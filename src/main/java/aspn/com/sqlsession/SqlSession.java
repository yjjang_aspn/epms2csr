package aspn.com.sqlsession;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import aspn.com.common.util.Entity;

@Repository("sqlSession")
public class SqlSession {
	
	private Logger logger = Logger.getLogger(getClass()); 

	@Autowired
	private org.apache.ibatis.session.SqlSession sqlSession;

//	@Autowired
//	@Resource(name="sqlSession2")
//	private SqlSession query;
	private Entity result = null;

	/**
	 * 
	 * @param sqlId
	 * @param resultType
	 * @param param
	 * @return
	 * @throws SQLException 
	 */	
	public Entity selectList(String sqlId) throws SQLException  {
		result = new Entity();
		
		List<Entity> list = sqlSession.selectList(sqlId);
		result.setValue("list", list);
		logger.debug("SELECT LIST : "+list.size()+" 개의 행이 검색되었습니다.");
		return result; 
	}
	public Entity selectList(String sqlId, Entity param) throws SQLException  {
		result = new Entity();

		List<Entity> list = sqlSession.selectList(sqlId, param);
		result.setValue("list", list);
		logger.debug("SELECT LIST : "+list.size()+" 개의 행이 검색되었습니다.");
		return result; 
	}

	public Entity selectListPaging(String sqlId, Entity param) throws SQLException  {
		result = new Entity();
		
		int offset = param.getInt("PAGESIZE") * (param.getInt("PAGENUM")-1);
		int limit = param.getInt("PAGESIZE");
		
		logger.debug("offset : "+offset);
		logger.debug("limit : "+limit);
		
		List<Entity> list = sqlSession.selectList(sqlId, param, new RowBounds(offset,limit));
		result.setValue("list", list);
		logger.debug("SELECT LIST : "+list.size()+" 개의 행이 검색되었습니다.");
		return result; 
	}

	public Entity selectOneRow(String sqlId) throws SQLException  {
		result = new Entity();
		
		List<Entity> list = sqlSession.selectList(sqlId);
		Entity entity = new Entity();
		if(list.size()>0) {
			entity = (Entity)list.get(0);
		} else {
			entity = new Entity();
		}
		result.setValue("entity", entity);
		logger.debug("SELECT ONE ROW : 1 개의 행이 검색되었습니다.");
		return result; 
	}
	public Entity selectOneRow(String sqlId, Entity param) throws SQLException  {
		result = new Entity();
		
		List<Entity> list = sqlSession.selectList(sqlId, param);
		Entity entity = new Entity();
		if(list.size()>0) {
			entity = (Entity)list.get(0);
		} else {
			entity = new Entity();
		}
		result.setValue("entity", entity);
		logger.debug("SELECT ONE ROW : "+entity.size()+" 개의 데이터가 검색되었습니다.");
		return result; 
	}

	/**
	 * 
	 * @param sqlId
	 * @param resultType
	 * @param param
	 * @return
	 * @throws SQLException 
	 */
	public Entity selectOne(String sqlId, String resultType) throws SQLException  {
		result = new Entity();
		
		if(resultType.equals("int")){
			int resultInt = ((Integer) sqlSession.selectOne(sqlId)).intValue();
			result.setValue("int", resultInt);
		}else if(resultType.equals("string")){
			String string = (String) sqlSession.selectOne(sqlId);
			result.setValue("string", string);
		}
		logger.debug("SELECT ONE : "+result.size()+" 개의 데이터가 검색되었습니다.");
		return result; 
	}
	public Entity selectOne(String sqlId, String resultType, Entity param) throws SQLException  {
		result = new Entity();
		
		if(resultType.equals("int")){
			int resultInt = ((Integer) sqlSession.selectOne(sqlId, param)).intValue();
			result.setValue("int", resultInt);
		}else if(resultType.equals("string")){
			String string = (String) sqlSession.selectOne(sqlId, param);
			result.setValue("string", string);
		}
		logger.debug("SELECT ONE : "+result.size()+" 개의 데이터가 검색되었습니다.");
		return result; 
	}
	
	public Object selectObject(String sqlId, Entity param, Object objClass) throws SQLException  {
		List<Object> list = sqlSession.selectList(sqlId, param);
		if(list.size()>0) {
			objClass = list.get(0);
		} else {
			objClass = new Object();
		}
		logger.debug("SELECT ONE ROW : 1 개의 행이 검색되었습니다.");
		System.out.println(objClass.toString());
		
		return objClass;
		
		/*
		List<Entity> list = sqlSession.selectList(sqlId, param);
		Entity entity = new Entity();
		if(list.size()>0) {
			entity = (Entity)list.get(0);
		} else {
			entity = new Entity();
		}
		
		String keyAttribute = null;
		String setMethodString = "set";
		String methodString = null;
		Iterator itr = entity.keySet().iterator();
		int j=0;
		while(itr.hasNext()){
			keyAttribute = (String) itr.next();
			methodString = setMethodString+keyAttribute.substring(0,1).toUpperCase()+keyAttribute.substring(1);
			j++;
			System.out.println("--------[ "+ j +" ]------");
			System.out.println(methodString);
			try {
				Method[] methods = objClass.getClass().getDeclaredMethods();
				for(int i=0;i<=methods.length-1;i++){
					if(methodString.equals(methods[i].getName())){
						System.out.println("invoke : "+methodString + " : " + entity.getString(keyAttribute));
						methods[i].invoke(objClass, entity.getString(keyAttribute));
					}
				}
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		System.out.println(objClass.toString());
		return objClass;
		*/
	}
	public Entity selectObjectList(String sqlId, Entity param, Object objClass) throws SQLException  {
		result = new Entity();

		List<Object> list = sqlSession.selectList(sqlId, param);
		result.setValue("list", list);
		logger.debug("SELECT LIST : "+list.size()+" 개의 행이 검색되었습니다.");
		for(int i=0; i<list.size(); i++){
			System.out.println("[" + (i+1) + "] " + list.get(i).toString());
		}
		return result; 
	}

    /**
     * INSERT 처리
     */
    public void insert(String sqlId, Entity param) throws SQLException  {
    	int insertResult = sqlSession.insert(sqlId, param);
    	if(insertResult > 0){
    		logger.debug("INSERT SUCCESS - ["+sqlId+"] : "+insertResult+" 개의 데이터가 정상처리 되었습니다.");
    	}else {
    		logger.debug("INSERT WARNING - ["+sqlId+"] : 갱신된 행이 존재하지 않습니다.");
    	}
    }
    public Entity insert(String sqlId, String resultType, Entity param) throws SQLException  {
    	result = new Entity();
    	int insertResult = sqlSession.insert(sqlId, param);
    	if(insertResult > 0){
    		if(resultType.equals("int")){
    			result.setValue("int", insertResult);
    		}else if (resultType.equals("boolean")){
    			result.setValue("boolean", true);
    		}
    		logger.debug("INSERT SUCCESS - ["+sqlId+"] : "+insertResult+" 개의 데이터가 정상처리 되었습니다.");
    	}else {
    		if(resultType.equals("int")){
    			result.setValue("int", insertResult);
    		}else if (resultType.equals("boolean")){
    			result.setValue("boolean", false);
    		}
    		logger.debug("INSERT WARNING - ["+sqlId+"] : 갱신된 행이 존재하지 않습니다.");
    	}
    	return result;
    }

    /**
     * UPDATE 처리
     */
    public void update(String sqlId, Entity param) throws SQLException  {
		int updateResult = sqlSession.update(sqlId, param);
    	if(updateResult > 0){
    		logger.debug("UPDATE SUCCESS - ["+sqlId+"] : "+updateResult+" 개의 데이터가 정상처리 되었습니다.");
    	}else {
    		logger.debug("UPDATE WARNING - ["+sqlId+"] : 갱신된 행이 존재하지 않습니다.");
    	}
    }
    public Entity update(String sqlId, String resultType, Entity param) throws SQLException  {
    	result = new Entity();
    	int updateResult = sqlSession.update(sqlId, param);
    	if(updateResult > 0){
    		if(resultType.equals("int")){
    			result.setValue("int", updateResult);
    		}else if (resultType.equals("boolean")){
    			result.setValue("boolean", true);
    		}
    		logger.debug("UPDATE SUCCESS - ["+sqlId+"] : "+updateResult+" 개의 데이터가 정상처리 되었습니다.");
    	}else {
    		if(resultType.equals("int")){
    			result.setValue("int", updateResult);
    		}else if (resultType.equals("boolean")){
    			result.setValue("boolean", false);
    		}
    		logger.debug("UPDATE WARNING - ["+sqlId+"] : 갱신된 행이 존재하지 않습니다.");
    	}
    	return result;
    }

    /**
     * DELETE 처리
     */
    public void delete(String sqlId, Entity param) throws SQLException  {
		int deleteResult = sqlSession.delete(sqlId, param);
    	if(deleteResult > 0){
    		logger.debug("DELETE SUCCESS - ["+sqlId+"] : "+deleteResult+" 개의 데이터가 정상처리 되었습니다.");
    	}else {
    		logger.debug("DELETE WARNING - ["+sqlId+"] : 갱신된 행이 존재하지 않습니다.");
    	}
    }
    public Entity delete(String sqlId, String resultType, Entity param) throws SQLException  {
    	result = new Entity();
    	int deleteResult = sqlSession.delete(sqlId, param);
    	if(deleteResult > 0){
    		if(resultType.equals("int")){
    			result.setValue("int", deleteResult);
    		}else if (resultType.equals("boolean")){
    			result.setValue("boolean", true);
    		}
    		logger.debug("DELETE SUCCESS - ["+sqlId+"] : "+deleteResult+" 개의 데이터가 정상처리 되었습니다.");
    	}else {
    		if(resultType.equals("int")){
    			result.setValue("int", deleteResult);
    		}else if (resultType.equals("boolean")){
    			result.setValue("boolean", false);
    		}
    		logger.debug("DELETE WARNING - ["+sqlId+"] : 갱신된 행이 존재하지 않습니다.");
    	}
    	return result;
    }
    
    
    /**
     * 
     * @param sqlId
     * @param param
     * @return
     * @throws SQLException
     */
    public Entity selectProcedure (String sqlId, Entity param) throws SQLException  {
    	result = new Entity();
    	sqlSession.insert(sqlId, param);
    	result = param;
    	return result;
    }

    /**
     * 
     * @param sqlId
     * @param param
     * @throws SQLException
     */
    public Entity insertProcedure(String sqlId, Entity param) throws SQLException  {
    	result = new Entity();
    	sqlSession.insert(sqlId, param);
    	logger.debug("RET_CODE : "+param.getString("RET_CODE"));
    	logger.debug("RET_MSG : "+param.getString("RET_MSG"));
    	
    	result.setValue("RET_CODE", param.getString("RET_CODE"));
    	result.setValue("RET_MSG", param.getString("RET_MSG"));
    	
    	if((param.getString("RET_NEW_ITEM_SN")).length() > 0){
    		result.setValue("RET_NEW_ITEM_SN", param.getString("RET_NEW_ITEM_SN"));
    	}
    	
    	return result;
    }

    /**
     * 
     * @param sqlId
     * @param param
     * @throws SQLException
     */
    public Entity updateProcedure (String sqlId, Entity param) throws SQLException  {
    	result = new Entity();
    	
    	sqlSession.update(sqlId, param);
    	logger.debug("RET_CODE : "+param.getString("RET_CODE"));
    	logger.debug("RET_MSG : "+param.getString("RET_MSG"));
    	
    	result.setValue("RET_CODE", param.getString("RET_CODE"));
    	result.setValue("RET_MSG", param.getString("RET_MSG"));
    	
    	return result;
    }

    /**
     * 
     * @param sqlId
     * @param param
     * @throws SQLException
     */
    public Entity deleteProcedure (String sqlId, Entity param) throws SQLException  {
    	result = new Entity();
    	
    	sqlSession.delete(sqlId, param);
    	logger.debug("RET_CODE : "+param.getString("RET_CODE"));
    	logger.debug("RET_MSG : "+param.getString("RET_MSG"));
    	
    	result.setValue("RET_CODE", param.getString("RET_CODE"));
    	result.setValue("RET_MSG", param.getString("RET_MSG"));
    	
    	return result;
    }
}
