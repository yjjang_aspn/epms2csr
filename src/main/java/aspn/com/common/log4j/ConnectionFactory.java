package aspn.com.common.log4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.springframework.util.ResourceUtils;

public class ConnectionFactory {
	private static interface Singleton {
		final ConnectionFactory INSTANCE = new ConnectionFactory();
	}

	private final DataSource dataSource;

	private ConnectionFactory() {
		
		Properties properties = new Properties();
		String jdbc_url = "";
		
		try {
			// 프로퍼티 객체 생성
	        Properties _config = new Properties();
	        // 프로퍼티 파일 로딩
	        _config.load(new java.io.BufferedInputStream(new FileInputStream(ResourceUtils.getFile("classpath:/config/config.properties"))));
	        
	        if(_config.getProperty("production_mode").equals("prod")){

	    		properties.setProperty("user", "epms");
	    		properties.setProperty("password", "last0015"); 
	    		jdbc_url = "jdbc:oracle:thin:@211.231.213.72:1521:orcl";
	        }
	        else if(_config.getProperty("production_mode").equals("dev")){

	    		properties.setProperty("user", "epms_test");
	    		properties.setProperty("password", "last0015"); 
	    		jdbc_url = "jdbc:oracle:thin:@20.10.10.72:1521:orcl";
	        }
	        else{

	    		properties.setProperty("user", "epms_test");
	    		properties.setProperty("password", "last0015"); 
	    		jdbc_url = "jdbc:oracle:thin:@211.231.213.72:1521:orcl";
	        }
	        
		} catch (IOException e) {
            throw new RuntimeException(e);
        }
		
		
		try {
			loadDriver("oracle.jdbc.driver.OracleDriver");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		GenericObjectPool pool = new GenericObjectPool();
		DriverManagerConnectionFactory connectionFactory = new DriverManagerConnectionFactory( jdbc_url, properties);
		new PoolableConnectionFactory(connectionFactory, pool, null, "SELECT 1", 3, false, false, Connection.TRANSACTION_READ_COMMITTED);
		this.dataSource = new PoolingDataSource(pool);
	}

	public static Connection getDatabaseConnection() throws SQLException {
		return Singleton.INSTANCE.dataSource.getConnection();
	}

	private static void loadDriver(String driver) throws SQLException {
		try {
			Class.forName(driver).newInstance();
		} catch (Exception e) {
			throw new SQLException("Unable to load driver: " + driver);
		}
	}
}