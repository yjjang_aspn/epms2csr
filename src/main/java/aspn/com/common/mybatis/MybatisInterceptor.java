package aspn.com.common.mybatis;

import java.util.List;
import java.util.Properties;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.log4j.Logger;
import org.springframework.util.StopWatch;

@Intercepts({ 
	@Signature(type = Executor.class, method = "update", args = { MappedStatement.class, Object.class})
	,@Signature(type = Executor.class, method = "query", args ={MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class})
})
public class MybatisInterceptor implements Interceptor {

	Logger logger = Logger.getLogger(MybatisInterceptor.class);

	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		// TODO Auto-generated method stub
		Object[] args = invocation.getArgs();
		String commandType = "";
		MappedStatement ms = (MappedStatement) args[0];
		logger.debug("[sqlId : " + ms.getId() + "]");
		
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		
		commandType = ms.getSqlCommandType().toString();
		Object result = invocation.proceed();
		
		stopWatch.stop();
		int cnt = 0;
		if(commandType.equals("SELECT")){
			List<?> list = (List<?>) result;
			cnt = list.size();
			logger.debug("[Query Result : " + cnt + "건이 조회되었습니다.]"+" ("+(stopWatch.getTotalTimeMillis() / 1000) % 60 + "s)");
		}else {
			cnt = (int) result;
			logger.debug("[Query Result : " + cnt + "건이 처리되었습니다.]"+" ("+(stopWatch.getTotalTimeMillis() / 1000) % 60 + "s)");
		}
		
		return result;
	}

	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}

	@Override
	public void setProperties(Properties properties) {
	}
}

