package aspn.com.common.mybatis;

import java.util.Scanner;

import net.sf.log4jdbc.Slf4jSpyLogDelegator;
import net.sf.log4jdbc.Spy;
import net.sf.log4jdbc.tools.LoggingType;

public class Log4JdbcCustomFormatter extends Slf4jSpyLogDelegator {
	private LoggingType loggingType = LoggingType.DISABLED;

	private String margin = "4";

	private String sqlPrefix = "----------------- SQL START -----------------";
	private String sqlSuffix = "----------------- SQL END -----------------";

	public int getMargin() {
		return this.margin.length();
	}

	public void setMargin(int n) {
		this.margin = String.format("%1$#" + n + "s", new Object[] { "" });
	}

	public String sqlOccured(Spy spy, String methodCall, String rawSql) {
		if (this.loggingType == LoggingType.DISABLED) {
			return "";
		}

		if (this.loggingType != LoggingType.MULTI_LINE) {
			rawSql = rawSql.replaceAll("\r", "");
			rawSql = rawSql.replaceAll("\n", "");
		}
		
		String sql = "";
		Scanner scanner = new Scanner(rawSql);
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			if(line.trim().length() > 0){
				sql += line+"\n";
			}
		}
		
		String fromClause = " from ";
		if (this.loggingType == LoggingType.MULTI_LINE) {
			String whereClause = " where ";
			String andClause = " and ";
			String subSelectClauseS = "\\(select";
			String subSelectClauseR = " (select";
			sql = sql.replaceAll(" from ", "\n" + this.margin + " from ");
			sql = sql.replaceAll(" where ", "\n" + this.margin + " where ");
			sql = sql.replaceAll(" and ", "\n" + this.margin + " and ");
			sql = sql.replaceAll("\\(select", "\n" + this.margin + " (select");
		}
		if ((this.loggingType == LoggingType.SINGLE_LINE_TWO_COLUMNS) && (sql.startsWith("select"))) {
			String from = sql.substring(sql.indexOf(" from ") + " from ".length());
			sql = from + "\t" + sql;
		}

//		getSqlOnlyLogger().debug(this.sqlSuffix);
		getSqlOnlyLogger().debug(sql);
//		getSqlOnlyLogger().debug(this.sqlSuffix);
		return sql;
	}

	public String sqlOccured(Spy spy, String methodCall, String[] sqls) {
		String s = "";
		for (int i = 0; i < sqls.length; i++) {
			s = s + sqlOccured(spy, methodCall, sqls[i]) + String.format("%n", new Object[0]);
		}
		return s;
	}

	public LoggingType getLoggingType() {
		return this.loggingType;
	}

	public void setLoggingType(LoggingType loggingType) {
		this.loggingType = loggingType;
	}

	public String getSqlPrefix() {
		return this.sqlPrefix;
	}

	public void setSqlPrefix(String sqlPrefix) {
		this.sqlPrefix = sqlPrefix;
	}

	public String getSqlSuffix() {
		return sqlSuffix;
	}

	public void setSqlSuffix(String sqlSuffix) {
		this.sqlSuffix = sqlSuffix;
	}
	
}