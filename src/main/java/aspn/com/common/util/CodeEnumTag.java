package aspn.com.common.util;
 
import java.util.HashMap;

import javax.servlet.jsp.JspWriter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.tags.RequestContextAwareTag;

import aspn.hello.sys.model.CodeVO;
import aspn.hello.sys.service.CodeService;

/**
 * 공통코드 Grid ComboData Custom Tag Class
 * @author 이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2017.07.04		이영탁			최초 생성
 *   
 * </pre>
 */
public class CodeEnumTag extends RequestContextAwareTag{
	
	@Autowired
	CodeService codeService; 
 
	private static final long serialVersionUID = 1L;
	private String codeGrp   = "";  //공통코드
	private String upperComcd = "";	//UPPER_COMCD : 상위코드
	private String companyId = "";	//COMPANY_ID : 회사
	private String location = "";	//LOCATION : 공장
	private String blankYn   = "N"; //option에 공백이 들어갈지 여부
	private String subAuthYn = "N"; //권한 사용여부
 
	public int doStartTagInternal() throws Exception{
		if (codeService == null) {
			// 1. WebApplicationContext를 얻는다.
			WebApplicationContext wac = getRequestContext().getWebApplicationContext();
			AutowireCapableBeanFactory beanFactory = wac.getAutowireCapableBeanFactory();
			// 2. 스프링 빈 주입
			beanFactory.autowireBean(this);
		}
		
		CodeVO codeVO = new CodeVO();
		codeVO.setComcdGrp(this.codeGrp);
		// UPPER_COMCD 있을 경우
		if(!(this.upperComcd.equals("") || this.upperComcd.isEmpty())){
			codeVO.setUPPER_COMCD(this.upperComcd);
		}
		// COMPANY_ID 있을 경우
		if(!(this.companyId.equals("") || this.companyId.isEmpty())){
			codeVO.setCOMPANY_ID(this.companyId);
		}
		// LOCATION 있을 경우
		if(!(this.location.equals("") || this.location.isEmpty())){
			codeVO.setLOCATION(this.location);
		}
		// 권한 사용이 Y일 경우 VO에 삽입
		if(this.subAuthYn.equals("Y")){
			codeVO.setSubAuthYn(this.subAuthYn);
			codeVO.setSubAuth((String)SessionUtil.getAttribute("ssSubAuth"));
		}
	 
		JspWriter out = pageContext.getOut(); 
		
		try {
			String enumStr = "";
			String enumKeysStr = "";
			for(HashMap<String, Object> info:codeService.getCodeDtlList(codeVO)){
				enumKeysStr += "|"+info.get("COMCD");
				enumStr += "|"+info.get("COMCD_NM");
			}
			if("Y".equals(blankYn)){
				out.println(" Enum='|"+enumStr+"' EnumKeys='|"+enumKeysStr+"' ");
			}else{
				out.println(" Enum='"+enumStr+"' EnumKeys='"+enumKeysStr+"' ");
			}
		}catch(Exception e){          
			e.printStackTrace(); 
		}
		return SKIP_BODY; 
	}
	
	public int doEndTag() {
		// 초기화
		this.setUpperComcd("");	//UPPER_COMCD : 상위코드
		this.setCompanyId("");	//COMPANY_ID : 회사
		this.setLocation("");	//LOCATION : 공장
		this.setBlankYn("N");	//option에 공백이 들어갈지 여부
		this.setSubAuthYn("N");	//권한 사용여부
		
		return SKIP_BODY;
	}

	public void setCodeGrp(String codeGrp) { 
		this.codeGrp = codeGrp; 
	}  
	public void setUpperComcd(String upperComcd) { 
		this.upperComcd = upperComcd; 
	}
	public void setCompanyId(String companyId) { 
		this.companyId = companyId; 
	}
	public void setLocation(String location) { 
		this.location = location; 
	}
	public void setBlankYn(String blankYn) { 
		this.blankYn =blankYn; 
	}
	public void setSubAuthYn(String subAuthYn) {
		this.subAuthYn = subAuthYn;
	}
}
