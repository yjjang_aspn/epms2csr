package aspn.com.common.util;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import aspn.hello.mem.model.MemberVO;

/**
 * Controller Support Util
 * @author 이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.07.14  이영탁          최초 생성
*
 * </pre>
 */

public class ContSupport
{
	@Autowired
	AspnMessageSource msgSource;
	
	public String getSaved(){
		return "<Grid><IO Result='0' HtmlMessage='"+msgSource.getMessage("msg.saved")+"' HtmlMessageTime ='700' /></Grid>";
	}
	
	public String getSaveFail(){
		return "<Grid><IO Result='0' HtmlMessage='"+msgSource.getMessage("msg.saveFail")+"'  /></Grid>";
	}

	//세션_사용자아이디
	public String getSsUserId() throws Exception{
		return (String)SessionUtil.getAttribute("ssUserId");
	}
	//세션_권한
	public String getSsAuth()throws Exception{
		return (String)SessionUtil.getAttribute("ssAuth");
	}
	//세션_보조권한
	public String getSsSubAuth()throws Exception{
		return (String)SessionUtil.getAttribute("ssSubAuth");
	}
	
	//세션_뷰권한
	public String getSsViewAuth()throws Exception{
		return (String)SessionUtil.getAttribute("ssViewAuth");
	}
	
	//세션_부서
	public String getSsDivision()throws Exception{
		return (String)SessionUtil.getAttribute("ssDivision");
	}
	
	//세션_회사코드
	public String getSsCompanyId()throws Exception{
		return (String)SessionUtil.getAttribute("ssCompanyId");
	}
	//세션_언어
	public String getSsLanguage()throws Exception{
		return (String)SessionUtil.getAttribute("lang");
	}
	//세션_코스트센터
	public String getSsKostl()throws Exception{
		return (String)SessionUtil.getAttribute("ssKostl");
	}
	//세션_사용자VO
	public MemberVO getSsMemberVO()throws Exception{
		return (MemberVO)SessionUtil.getAttribute("ssMemberVO");
	}

	//세션_위치(공장)
	public String getSsLocation()throws Exception{
		return (String)SessionUtil.getAttribute("ssLocation");
	}
	//세션_파트
	public String getSsPart()throws Exception{
		return (String)SessionUtil.getAttribute("ssPart");
	}
	//세션_사번
	public String getSsMemberId()throws Exception{
		return (String)SessionUtil.getAttribute("SsMemberId");
	}
	//세션_파트통합구분
	public String getSsRemarks()throws Exception{
		return (String)SessionUtil.getAttribute("ssRemarks");
	}
	//세션_출퇴근상태
	public String getSsCheckInout()throws Exception{
		return (String)SessionUtil.getAttribute("ssCheckInout");
	}
}
