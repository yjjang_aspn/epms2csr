package aspn.com.common.util;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;


/**
 -- ---------- ---------------------------------------------------------
 -- PGM 이름 : Entity
 -- PGM 내용 : VO를 담당할 객체
 -- ---------- ---------------------------------------------------------
*/
public class Entity extends HashMap implements Serializable {
	
	private static final Logger logger = Logger.getLogger(Entity.class); 

	private static final long serialVersionUID = 1L;
	private boolean isMultPart = false;
	private List<String> header = new ArrayList<String>();

	public Entity(){}
	
	public Entity(HashMap<String, Object> hashmap) throws Exception {
		this.parseHashmap(hashmap);
	}
	
	public Entity(HttpServletRequest req) throws Exception {
		this.parseRequest(req);
	}
	
	public List<String> getHeader(){
		return header;
	}
	
	@Override
	public Object put(Object key, Object value){
		if(value == null)
			value = "";
		super.put(key, value);
		if(key != null && !key.equals(""))
			header.add(key.toString());
		return value;
	}
	
	public void parseHashmap(HashMap<String, Object> map) throws Exception {
		String keyAttribute = null; 
		Iterator itr = map.keySet().iterator();
		
		while(itr.hasNext()){ 
			keyAttribute = (String) itr.next(); 
			
			setValue(keyAttribute, (map.get(keyAttribute) != null) ? map.get(keyAttribute).toString().trim() : "");
		}	
    }
	
	public void parseRequest(HttpServletRequest pm_oRequest) throws Exception {
        Enumeration im_oEnum = pm_oRequest.getParameterNames();
        String im_sParam = null;
        String[] im_sValues = null;

        //request 객체를 파싱하여 문자열로 만든다. - get 방식으로의 전송을 지원하기 위해
        String paramString = "";
        while (im_oEnum.hasMoreElements()) {
            im_sParam = (String) im_oEnum.nextElement();
            im_sValues = pm_oRequest.getParameterValues(im_sParam);
            if (im_sValues.length == 1) {
            	String _val = im_sValues[0];
            	if(im_sParam.equals("uploadData")){
            		String xml = XmlUtil.parseXML(_val);
            		Document doc = XmlUtil.convertNodesFromXml(xml);
//            		HashMap<String, Object> saveDataMap = XmlUtil.nodeToMap(doc, "Changes").get("I");
            		
            		HashMap<String, Object> saveDataMap = XmlUtil.nodeToMap(doc, "Changes");
        			List<HashMap<String, Object>> saveDataList = (List<HashMap<String, Object>>) saveDataMap.get("I");
            		
            		setValue("grid", saveDataList);
            	}else {
            		setValue(im_sParam, (_val != null) ? _val.toString().trim() : "");
            	}
            } else {
            	setValue(im_sParam, im_sValues);
            }
        }
        
    }

    /**
     * ResultSet 에서 컬럼 이름을 key로 해서 그 값을 Entity 에 저장하는 method
     *
     * @param ResultSet
     * @exception SQLException
     */
    public void parseResultSet(ResultSet rs) throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        int size = md.getColumnCount();

        for (int i = 1; i <= size; i++) {
            setValue(md.getColumnName(i), rs.getString(i));
        }
    }

    /**
     * ResultSet 에서 컬럼 이름과 인덱스를 key로 해서 그 값을 Entity 에 저장하는 method
     *
     * @param int
     * @param ResultSet
     * @exception SQLException
     */
    public void parseResultSet(int iIndex, ResultSet rs)
        throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        int size = md.getColumnCount();

        String sColumnName = null;
        String sColumnValue = null;

        for (int i = 1; i <= size; i++) {
            setValue( new StringBuffer(md.getColumnName(i)).append(iIndex).toString(), rs.getString(i) );
        }
    }

    public void setValue(String sKey, String sValue) {
        if (sValue != null) {
            put(sKey, sValue);
        }
    }

    public void setValue(String sKey, String[] sValues) {
        put(sKey, sValues);
    }

    public void setValue(String sKey, byte[] yValues) {
        String sValue = null;

        if (yValues != null) {
            sValue = new String(yValues);
        }
        put(sKey, sValue);
    }

    public void setValue(String sKey, byte yValue) {
        put(sKey, Byte.toString(yValue));
    }

    public void setValue(String sKey, char[] cValues) {
        String sValue = null;
        if (cValues != null) {
            sValue = new String(cValues);
        }
        put(sKey, sValue);
    }

    public void setValue(String sKey, char cValue) {
        put(sKey, String.valueOf(cValue));
    }

    public void setValue(String sKey, float fValue) {
        put(sKey, String.valueOf(fValue));
    }

    public void setValue(String sKey, boolean bValue) {
        put(sKey, String.valueOf(bValue));
    }

    public void setValue(String sKey, short tValue) {
        put(sKey, String.valueOf(tValue));
    }

    public void setValue(String sKey, int iValue) {
        put(sKey, String.valueOf(iValue));
    }

    public void setValue(String sKey, long lValue) {
        put(sKey, String.valueOf(lValue));
    }

    public void setValue(String sKey, double dValue) {
        put(sKey, String.valueOf(dValue));
    }

    public void setValue(String sKey, java.util.Date value) {
        String sValue = null;
        if (value != null) {
            ;
        }
        sValue = value.toString();
        put(sKey, sValue);
    }

    public void setValue(String sKey, Vector value) {
        put(sKey, value);
    }

    public void setValue(String sKey, List value) {
        put(sKey, value);
    }

    public void setValue(String sKey, Hashtable value) {
        put(sKey, value);
    }

    public void setValue(String sKey, Entity value) {
        put(sKey, value);
    }

    public void setValue(String sKey, HashMap value) {
    	put(sKey, value);
    }

    public void setValue(String sKey, Map value) {
        put(sKey, value);
    }

    public String getString() {
        String sValue = null;
        Object obj = null;

        try {
            obj = get("string");

            if (obj instanceof String) {
                sValue = (String) obj;
            } else if (obj instanceof String[]) {
                sValue = ((String[]) obj)[0];
            } else {
            	sValue = obj.toString();
            }
        } catch (Exception e) {
            sValue = "";
        }
        return sValue;
    }

    public String getString(String sKey) {
    	String sValue = null;
    	Object obj = null;
    	
    	try {
    		obj = get(sKey);
    		
    		if (obj instanceof String) {
    			sValue = (String) obj;
    		} else if (obj instanceof String[]) {
    			sValue = ((String[]) obj)[0];
    		} else {
    			sValue = obj.toString();
    		}
    	} catch (Exception e) {
    		sValue = "";
    	}
    	return sValue;
    }

    /**
     *
     * Entity 에 저장된 String[] 를 return 하는 method String 이 저장되어 있을 경우에는 length 가
     * 1인 String[] 를 return 한다.
     *
     * @param String
     *            sKey
     * @return String[]
     * @exception Exception
     */
    public String[] getStrings(String sKey) {
        String[] sValues = null;
        Object obj = null;

        try {
            obj = get(sKey);
            if(obj != null){
            	if (obj instanceof String) {
            		sValues = new String[1];
            		sValues[0] = (String) obj;
            	} else {
            		sValues = (String[]) obj;
            	}
            }else {
            	sValues = new String[1];
            	sValues[0] = (String) "";
            }
        } catch (Exception e) {
        }
        return sValues;
    }

    public Entity getStrings(Entity param, int idx) {
    	Entity resultParam = new Entity();
    	Object obj = null;
    	
    	Iterator it = param.keySet().iterator();
    	
    	String key = "";
    	while(it.hasNext()) {
    		key = it.next().toString();
    		try {
                obj = get(key);
                if (obj instanceof String[]) {
//                	System.out.println(key+"[] : "+this.getStrings(key)[idx]);
                    resultParam.setValue(key, this.getStrings(key)[idx]);
                }
                else {
//                	System.out.println(key+" : "+this.getString(key));
                    resultParam.setValue(key, this.getString(key));
                }
            } catch (Exception e) {
            	System.out.println(">>> "+e.getMessage());
            }
    	}
    	return resultParam;
    }
    
    public Entity getGridData(String gridName, Entity param, int idx) {
    	Entity resultParam = new Entity();
    	
    	String header[] = (String[]) this.get(gridName+"[0][]");
    	String value[] = (String[]) this.get(gridName+"["+(idx +1)+"][]");
    	
    	System.out.println("header= " + header.length + ": value =" + value.length) ;
    	for(int i = 0; i < header.length; i++){
    		resultParam.setValue(header[i], value[i]);
    	}
    	return resultParam;
    }
    
    public int getGridChkCount(String gridName, Entity param) {
    	int result = 0;
		Entity temp = param.getStrings(param, 0);
		Iterator it = temp.keySet().iterator();
    	while(it.hasNext()) {
    		String key = it.next().toString();
    		if(key.contains(gridName)){
    			result++;
    		};
    	}
    	result = result -1;
    	return result;
    }

    public byte getByte(String sKey) {
        byte yResult = (byte) 0;

        try {
            yResult = Byte.parseByte((String) get(sKey));
        } catch (Exception e) {
        }

        return yResult;
    }

    public byte[] getBytes(String sKey) {
        byte[] yResults = null;

        try {
            yResults = ((String) get(sKey)).getBytes();
        } catch (Exception e) {
        }

        return yResults;
    }

    public char getChar(String sKey) {
        char cResult = (char) 0;

        try {
            cResult = ((String) get(sKey)).charAt(0);
        } catch (Exception e) {
        }

        return cResult;
    }

    public char[] getChars(String sKey) {
        char[] cResults = null;

        try {
            cResults = ((String) get(sKey)).toCharArray();
        } catch (Exception e) {
        }

        return cResults;
    }

    public float getFloat(String sKey) {
        float fResult = 0;

        try {
            fResult = Float.parseFloat((String) get(sKey));
        } catch (Exception e) {
        	e.getStackTrace();
        }

        return fResult;
    }

    public boolean getBoolean(String sKey) {
        boolean bResult = false;

        try {
            bResult = Boolean.valueOf((String) get(sKey)).booleanValue(); 
        } catch (Exception e) {
        	e.getStackTrace();
        }

        return bResult;
    }

    public short getShort() {
        short tResult = 0;
        Object o = get("string");
        try {
        	o = ((String)o).replaceAll(",", "");
        	tResult = Short.parseShort((String) o);
        } catch (Exception e) {
        	e.getStackTrace();
        }

        return tResult;
    }

    public int getInt() {
        int iResult = 0;

        Object o = get("int");

        try {
        	o = ((String)o).replaceAll(",", "");
            iResult = Integer.parseInt((String) o);
        } catch (Exception e) {
        }

        try {
            Class classType = o.getClass();

            if (classType == BigDecimal.class) {
                iResult = Integer.parseInt(o.toString());
            }
        } catch (Exception e) {
        	e.getStackTrace();
        }

        return iResult;
    }

    public int getInt(String sKey) {
    	int iResult = 0;
    	
    	Object o = get(sKey);
    	
    	try {
    		o = ((String)o).replaceAll(",", "");
    		iResult = Integer.parseInt((String) o);
    	} catch (Exception e) {
    	}
    	
    	try {
    		Class classType = o.getClass();
    		
    		if (classType == BigDecimal.class) {
    			iResult = Integer.parseInt(o.toString());
    		}
    	} catch (Exception e) {
    		e.getStackTrace();
    	}
    	
    	return iResult;
    }

    public long getLong(String sKey) {
        long lResult = 0;
        Object o = get(sKey);

        try {
        	o = ((String)o).replaceAll(",", "");
            lResult = Long.parseLong((String) o);
        } catch (Exception e) {
        }

        try {
            Class classType = o.getClass();

            if (classType == BigDecimal.class) {
                lResult = Long.parseLong(o.toString());
            }
        } catch (Exception e) {
        	e.getStackTrace();
        }

        return lResult;
    }

    public double getDouble(String sKey) {
        double dResult = 0;
        Object o = get(sKey);

        try {
        	o = ((String)o).replaceAll(",", "");
            dResult = Double.parseDouble((String) o);
        } catch (Exception e) {
        	e.getStackTrace();
        }

        try {
            Class classType = o.getClass();

            if (classType == BigDecimal.class) {
                dResult = Double.parseDouble(o.toString());
            }
        } catch (Exception e) {
        	e.getStackTrace();
        }

        return dResult;
    }

    public java.util.Date getDate(String sKey) {
        java.util.Date result = null;

        try {
            String sDate = (String) get(sKey);

            SimpleDateFormat formatter = new SimpleDateFormat(
                    "yyyy-MM-dd hh:mm:ss");
            ParsePosition pos = new ParsePosition(0);
            result = formatter.parse(sDate, pos);
        } catch (Exception e) {
        	e.getStackTrace();
        }

        return result;
    }

    public Vector getVector(String sKey) {
        Vector vResult = null;

        try {
            vResult = (Vector) get(sKey);
        } catch (Exception e) {
        	e.getStackTrace();
        }

        return vResult;
    }

    public List getList() {
        List alResult = null;

        try {
            alResult = (List) get("list");
        } catch (Exception e) {
        	alResult = new ArrayList();
        }

        return alResult;
    }

    public List getList(String sKey) {
        List alResult = null;

        try {
            alResult = (List) get(sKey);
        } catch (Exception e) {
        	alResult = new ArrayList();
        }

        return alResult;
    }

    public Hashtable getHashtable(String sKey) {
    	Hashtable value = null;

    	try {
    		value = (Hashtable) get(sKey);

    		if (value == null) {
    			value = new Hashtable();
    		}
    	} catch (Exception e) {
    		value = new Hashtable();
    	}

    	return value;
    }

    public Map getMap() {
        Map value = null;

        try {
            value = (Map) get("map");

            if (value == null) {
                value = new HashMap();
            }
        } catch (Exception e) {
            value = new HashMap();
        }

        return value;
    }

    public Entity getEntity() {
        Entity value = null;

        try {
            value = (Entity) get("entity");

            if (value == null) {
                value = new Entity();
            }
        } catch (Exception e) {
            value = new Entity();
        }

        return value;
    }

    public HashMap getHashMap(String sKey) {
        HashMap value = null;

        try {
            value = (HashMap) get(sKey);

            if (value == null) {
                value = new HashMap();
            }
        } catch (Exception e) {
            value = new HashMap();
        }

        return value;
    }

//    public void remove(String sKey) {
//    	try {
//    		System.out.println("=====> "+sKey);
//    		remove(sKey.toUpperCase());
//		} catch (Exception e) {
//			System.out.println("11>>>> "+e.getMessage());
//		}
//    }

    public String getKey(String sValue) {
        String sResult = null;

        Set keySet = entrySet();
        Object[] lists = keySet.toArray();

        String sKey = null;
        Object value = null;

        for (int i = 0; i < lists.length; i++) {
            sKey = (String) (((Map.Entry) lists[i]).getKey());
            value = get(sKey);

            if (value instanceof String &&
                    ((String) value).trim().equals(sValue)) {
                sResult = (String) sKey;
                break;
            }
        }
        return sResult;
    }

    public String getKey(String sValue, String sKeyPrefix) {
        sKeyPrefix = sKeyPrefix.toUpperCase();

        String sResult = null;

        Set keySet = entrySet();
        Object[] lists = keySet.toArray();

        String sKey = null;
        Object value = null;

        for (int i = 0; i < lists.length; i++) {
            sKey = (String) (((Map.Entry) lists[i]).getKey());
            value = get(sKey);

            if (sKey.startsWith(sKeyPrefix) && value instanceof String &&
                    ((String) value).trim().equals(sValue)) {
                sResult = (String) sKey;

                break;
            }
        }

        return sResult;
    }
    
}
