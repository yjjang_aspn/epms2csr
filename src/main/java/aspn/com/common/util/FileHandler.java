package aspn.com.common.util;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.renderable.ParameterBlock;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.media.jai.JAI;
import javax.media.jai.PlanarImage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.sun.media.jai.codec.FileSeekableStream;
import com.sun.media.jai.codec.SeekableStream;

import aspn.hello.com.model.AttachVO;
import sun.misc.BASE64Encoder;



@SuppressWarnings("restriction")
public final class FileHandler {
	Logger log = Logger.getLogger(this.getClass());
	public static final int BUFF_SIZE = 2048;
	public static int WIDTH = 300;
	
	/**
	 * 
	 * @MethodName          : uploadFiles
	 * @Date                : 2014. 09. 01.
	 * @author              : psj
	 * @Description         : 파일 업로드
	 * @History             : 2014. 09. 01. 최초 작성
	 * @param request       : multipartRequest를 위한 리퀘스트 객체
	 * @param filefieldName : 파일 태그 필드명
	 * @return
	 * @throws Exception 
	 */
	public static List<AttachVO> uploadFiles(HttpServletRequest request, MultipartHttpServletRequest mRequest, String filefieldName) throws Exception{
    	
//		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		
		List<MultipartFile> files = mRequest.getFiles(filefieldName);
    	
		List<AttachVO> fileList = new ArrayList<AttachVO>();
		
//		int i = 1;
		
		for(MultipartFile file : files) {
			boolean thumbYn = false;
			boolean videoThumbYn = false;
			if(file.isEmpty() || file.getSize() == 0) continue;
			
			String fileRealname = file.getOriginalFilename();
			String ext = fileRealname.substring(fileRealname.lastIndexOf(".")+1, fileRealname.length());
			
			if("jpg".equals(ext.toLowerCase()) || "jpeg".equals(ext.toLowerCase())){
				
			}
			
			AttachVO fileInfo = fileUpload(file, request.getParameter("MODULE"));
			
				
			String[] arrFormat = {"gif", "jpg", "png", "jpeg", "bmp"};
//				String[] arrFormat = {"gif", "png", "bmp"};
			String[] arrFormat2 = {"avi", "wmv", "mpeg", "mpg", "mkv", "mp4", "tp", "ts", "asf", "asx", "flv", "mov", "3gp"};
					
			for(int i=0; i<arrFormat.length; i++){
				if(arrFormat[i].equals(fileInfo.getFORMAT().toLowerCase())){
					thumbYn = true;
				}
			}
			
			for(int i=0; i<arrFormat2.length; i++){
				if(arrFormat2[i].equals(fileInfo.getFORMAT().toLowerCase())){
					videoThumbYn = true;
				}
			}
			
			if(thumbYn){
				String orgPath = fileInfo.getPATH();
				String thumFileName = fileInfo.getNAME();
				
				thumbnailMake(orgPath, thumFileName, WIDTH, request.getParameter("MODULE"));	
			}
			else if(videoThumbYn){
				String orgPath = fileInfo.getPATH();
				String thumFileName = fileInfo.getNAME();
				
				videoThumbnailMake(orgPath, thumFileName, request.getParameter("MODULE"));
				
			}
			
			
			if(fileInfo != null) fileList.add(fileInfo);
			
		}
		
		return fileList;
		
	}
	
	protected static AttachVO writeFile(MultipartFile file, String middlePath) throws Exception {
		String fileClsNm = "";
		
		switch(Integer.parseInt(middlePath)){
			case 9 :
				fileClsNm = "profile";
				break;
			default :
				fileClsNm = "community";
		}
	   	// 경로 및 파일명
		String fileRealname = file.getOriginalFilename();
		String ext = fileRealname.substring(fileRealname.lastIndexOf(".")+1, fileRealname.length());
		
		//저장될 폴더 경로
		String yyFolder = new SimpleDateFormat("yyyy").format(new Date(System.currentTimeMillis()));
		String mmFolder = new SimpleDateFormat("MM").format(new Date(System.currentTimeMillis()));
		String fileDir = Config.getString("file.upload.dir") + File.separator + fileClsNm + File.separator + yyFolder + File.separator + mmFolder;
		
		InputStream stream = null;
		OutputStream bos = null;

		// 불가능 확장자 체크
		if(!ext.equalsIgnoreCase("EXE") && !ext.equalsIgnoreCase("JS")
				 && !ext.equalsIgnoreCase("JAVA") && !ext.equalsIgnoreCase("CLASS")
				 && !ext.equalsIgnoreCase("ASP") && !ext.equalsIgnoreCase("ASPX")
				 && !ext.equalsIgnoreCase("CER") && !ext.equalsIgnoreCase("CDX") && !ext.equalsIgnoreCase("ASA")
				 && !ext.equalsIgnoreCase("PHP") && !ext.equalsIgnoreCase("PHP3") && !ext.equalsIgnoreCase("HTML") && !ext.equalsIgnoreCase("HTM")
				 && !ext.equalsIgnoreCase("JSP") && !ext.equalsIgnoreCase("WAR")) {
			try {
				stream = file.getInputStream();
				File cFile = new File(fileDir);

				if (!cFile.isDirectory()){
					cFile.mkdir();
				}

				bos = new FileOutputStream(fileDir + File.separator + file.getOriginalFilename());

				int bytesRead = 0;
				byte[] buffer = new byte[BUFF_SIZE];

				while (bytesRead != -1) {
					bytesRead = stream.read(buffer, 0, BUFF_SIZE);
					if(bytesRead != -1) bos.write(buffer, 0, bytesRead);
				}
			} catch (FileNotFoundException fnfe) {
				fnfe.printStackTrace();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (bos != null) {
					try {
						bos.close();
					} catch (Exception ignore) {
						Logger.getLogger(FileHandler.class).debug(
								"IGNORED: " + ignore.getMessage());
					}
				}
				if (stream != null) {
					try {
						stream.close();
					} catch (Exception ignore) {
						Logger.getLogger(FileHandler.class).debug(
								"IGNORED: " + ignore.getMessage());
					}
				}
			}

			//파일 모델 생성
			AttachVO attachVO = new AttachVO();
			//
			attachVO.setNAME(fileRealname);
			attachVO.setPATH(fileDir);
			attachVO.setFORMAT(ext);
			attachVO.setFILE_SIZE(file.getSize());
			attachVO.setCNT_DOWNLOAD("0");
			attachVO.setCD_DEL("1");
			
			return attachVO;
		}else {
			
			throw new IllegalArgumentException("File type error!");
			//new Exception("File type error!");
			
		}
	}
	
	/**
	 * 
	 * @MethodName   : fileUpload
	 * @Date         : 2014. 09. 01.
	 * @author       : psj
	 * @Description  : 물리 파일 생성 및 파일모델 생성
	 * @History      : 2014. 09. 01. 최초 작성
	 * @param file   : MultipartFile 객체
	 * @return
	 * @throws IOException 
	 */
	public static AttachVO fileUpload(MultipartFile file, String middlePath) throws IOException {
		String fileClsNm = "";
		FileOutputStream fos = null;
		
		switch(Integer.parseInt(middlePath)){
			case 5 :
				fileClsNm = "asset";
				break;
			case 6 :
				fileClsNm = "mail";
				break;
			case 7 :
				fileClsNm = "paper";
				break;
			case 8 :
				fileClsNm = "expenses";
				break;
			case 9 :
				fileClsNm = "profile";
				break;
			case 10 :
				fileClsNm = "profileSeal"; //직인
				break;
			case 40 :
				fileClsNm = "vendor";
				break;
			case 41 :
				fileClsNm = "pr";
				break;
			case 42 :
				fileClsNm = "est";
				break;
			case 43 :
				fileClsNm = "estVendor";
				break;
			case 44 :
				fileClsNm = "contract";
				break;
			case 45 :
				fileClsNm = "po";
				break;
			case 80 :
				fileClsNm = "notice";
				break;
			case 81 :
				fileClsNm = "community";
				break;	
				
			//EPMS Start
			case 101 :
				fileClsNm = "epms_equipment";			//설비 사진
				break;
			case 102 :
				fileClsNm = "epms_qrcode";				//설비 QR코드
				break;
			case 103 :
				fileClsNm = "epms_equipment_manual";		//설비매뉴얼
				break;
			case 111 :
				fileClsNm = "epms_repair_request";		//정비요청
				break;
			case 112 :
				fileClsNm = "epms_repair_result";		//정비실적
				break;
			case 113 :
				fileClsNm = "epms_repair_analysis";		//원인분석
				break;	
			case 121 :
				fileClsNm = "epms_order";				//오더 매뉴얼
				break;	
			case 122 :
				fileClsNm = "epms_preventive_result";	//예방보전 실적
				break;	
			case 201 :
				fileClsNm = "epms_material";				//자재 사진
				break;
			//EPMS End
			
			default :
				fileClsNm = "etc";
		}
    	// 경로 및 파일명
		String fileRealname = file.getOriginalFilename();
		String ext = fileRealname.substring(fileRealname.lastIndexOf(".")+1, fileRealname.length());
		
		//저장될 폴더 경로
		String yyFolder = new SimpleDateFormat("yyyy").format(new Date(System.currentTimeMillis()));
		String mmFolder = new SimpleDateFormat("MM").format(new Date(System.currentTimeMillis()));
		String fileDir = Config.getString("file.upload.dir") + File.separator + fileClsNm + File.separator + yyFolder + File.separator + mmFolder;
		
		//시스템 파일명
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
//		sdf = new SimpleDateFormat();
		//수정날짜:2016.03.11, 수정자:KYH, 수정내용:동일한 파일명의 다른 파일을 덮어쓰는 문제발생 가능성이 있어 아래의 구문 활성화
		String timeM = String.valueOf(System.currentTimeMillis());
//		String fileName = fileRealname.substring(0, fileRealname.lastIndexOf(".")) +"_"+ timeM +"."+ ext;
		String fileName = fileRealname.substring(fileRealname.lastIndexOf(File.separator)+1, fileRealname.lastIndexOf(".")) +"_"+ timeM +"."+ ext;
		
		//저장될 경로에 폴더 존재여부 없으면 디렉토리 생성
		File dir = new File(fileDir);
		if(!dir.exists()) dir.mkdirs();
		
		//파일 객체 생성
		//수정날짜:2016.03.11, 수정자:KYH, 수정내용:file.getOriginalFilename()을 fileRealname으로 대체
		File newFile = new File(fileDir + "/" +fileName);  //file.getOriginalFilename()
		
		// 불가능 확장자 체크
		if(!ext.equalsIgnoreCase("EXE") && !ext.equalsIgnoreCase("JS")) {
			
			try{
				
				//물리파일 생성 시작
				byte[] bytes = file.getBytes();
				
				fos = new FileOutputStream(newFile);
				fos.write(bytes);
				//물리파일 생성 끝
			
			} catch (IOException ie) {
				
				ie.printStackTrace();
				new Exception("File writing error!");
			} finally {

				fos.close();
			}
			
			//파일 모델 생성
			AttachVO attachVO = new AttachVO();
			//
			attachVO.setNAME(fileName);
			attachVO.setPATH(fileDir);
			attachVO.setFORMAT(ext);
			attachVO.setFILE_SIZE(file.getSize());
			attachVO.setCNT_DOWNLOAD("0");
			attachVO.setCD_DEL("1");
			
			
			return attachVO;
			
		} else {
			
			throw new IllegalArgumentException("File type error!");
			//new Exception("File type error!");
			
		}
		
	}
	
	
	/**
     * @MethodName         : fileDownload
     * @Date               : 2014. 9. 1.
     * @author             : psj , yjjang
     * @Description        : 파일 다운로드
     * @History            : 2014. 9. 1. 최초 작성
     * @param fileinfo     : fileInfo Model
     * @param res          : response 객체
     * @throws IOException : 
     */
    public static void fileDownload(AttachVO attachVO, HttpServletResponse res) throws IOException {
    	
		String path = attachVO.getPATH() + File.separator +attachVO.getNAME();
		
		File file = new File(path);
		String fileName = attachVO.getFILE_NAME();
//		String fileName = attachVO.getNAME();
		
		res.reset();
		res.setContentType("application/x-msdownload;");
		res.setHeader("Content-Disposition", "attachment;filename=\"" + URLEncoder.encode(fileName, "UTF-8").replace("+", "%20") + "\";");
		res.setContentLength(attachVO.getFILE_SIZE().intValue());
		
		OutputStream out = res.getOutputStream();
		
		FileInputStream fis = null;
		
		try {
			
			if ( file.exists() ) {
				
				fis = new FileInputStream(file);
				
				FileCopyUtils.copy(fis, out);
			}
		
		} catch ( IOException ioEx ) {
			
			//ioEx.printStackTrace();  

			throw ioEx ;  // [2021.03.31][yjjang]
			
		} finally {
			
			if(fis != null){
				try{
					fis.close();
				}catch(IOException ex){
					ex.printStackTrace();
				}
			}
			
			out.flush();
		}
		
    }
    
    
	/**
     * 
     * @MethodName         : thumbFileDownload
     * @Date               : 2014. 12. 4.
     * @author             : Lee hyunho
     * @Description        : 파일 다운로드
     * @History            : 2014. 12. 4. 최초 작성
     * @param fileinfo     : fileInfo Model
     * @param res          : response 객체
     * @throws IOException : 
     */
    public static void thumbFileDownload(AttachVO attachVO,HttpServletResponse res) throws IOException{
    	String fileName = attachVO.getNAME();
		fileName = "thum_"+fileName.substring(0, fileName.lastIndexOf(".")) + ".png";
		String path = attachVO.getPATH() + File.separator +fileName;
		
		File file = new File(path);
		
		res.reset();
		res.setContentType("application/x-msdownload;");
		res.setHeader("Content-Disposition", "attachment;filename=\"" + URLEncoder.encode(fileName, "UTF-8").replace("+", "%20") + "\";");

		BufferedInputStream in = null;
		BufferedOutputStream out = null;
		try {
			in = new BufferedInputStream(new FileInputStream(file));
			out = new BufferedOutputStream(res.getOutputStream());

			FileCopyUtils.copy(in, out);
			out.flush();
		} catch (Exception ex) {
			
//			ex.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception ex) {
				}
			}
			if (out != null) {
				try {
					out.close();
				} catch (Exception ex) {
				}
			}
		}
		
//		OutputStream out = res.getOutputStream();
//		FileInputStream fis = null;
//		try{
//			fis = new FileInputStream(file);
//			FileCopyUtils.copy(fis, out);
//		}finally{
//			if(fis != null){
//				try{
//					fis.close();
//				}catch(IOException ex){
//					ex.printStackTrace();
//				}
//			}
//			out.flush();
//		}
    }    

    public static void nioFileDel(Path strFilePath){
		try {
			Files.delete(strFilePath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * @MethodName        : deleteFile
	 * @Date              : 2014. 09. 01.
	 * @author            : psj
	 * @Description       : 물리 파일 삭제
	 * @History           : 2014. 09. 01. 최초 작성
	 * @param strFileName : file Full Path
	 * @return
	 */
	public static boolean deleteFile(String strFilePath, String strFileName) {
		
		boolean rCode = true;
		String fileFullPath = strFilePath + File.separator + strFileName;
		
		File fileName = new File(fileFullPath);
		
		try {
			System.out.println("commonFile :: "+fileName.isFile());

			if(fileName.isFile()){
				if (!fileName.delete()) {
					
					System.out.println ("[" + System.currentTimeMillis() + "] [FileHandler] [commonFile] [ERROR] [파일 삭제 실패] file : " + fileFullPath);	
					
					rCode = false;
				
				}	
			}
			 
		} catch(Exception ex) {
			
			System.out.println("[" + System.currentTimeMillis() + "] [FileHandler] [commonFile] [Exception] file : " + fileFullPath);	
			
			ex.printStackTrace();
			
			rCode = false;
			
			System.gc();
		
		} finally {
			
			fileName = null;
		}
		
		return rCode;
	}
	
	public static boolean thumbDeleteFile(String strFilePath, String strFileName) {
		
		boolean rCode = true;
		String thumbFullPath = strFilePath + File.separator + strFileName;
		
		File thumbFileName = new File(thumbFullPath);
		
		try {
			System.out.println("thumbFile");
			if(thumbFileName.isFile()){
				if (!thumbFileName.delete()) {
					
					System.out.println ("[" + System.currentTimeMillis() + "] [FileHandler] [thumbFile] [ERROR] [파일 삭제 실패] file : " + thumbFullPath);	
					
					rCode = false;
				
				}	
			}
			
		} catch(Exception ex) {
			
			System.out.println("[" + System.currentTimeMillis() + "] [FileHandler] [thumbFile] [Exception] file : " + thumbFullPath);	
			
			ex.printStackTrace();
			
			rCode = false;
			
			System.gc();
		
		} finally {
			
			thumbFileName = null;
		}
		
		return rCode;
	}
	
	/**
	 * 
	 * @MethodName        : thumbnailMake
	 * @Date              : 2014. 09. 01.
	 * @author            : psj
	 * @Description       : 썸네일 생성
	 * @History           : 2014. 09. 01. 최초 작성
	 * @param fileUrl     : 파일 Path
	 * @param fileName    : 원본 파일 명
	 * @param imageWidth  : 썸네일이미지 폭
	 * @param imageHeight : 썸네일이미지 높이
	 * @return
	 * @throws IOException 
	 */
	public static boolean thumbnailMake(String fileUrl, String fileName, int imageWidth, String module) throws IOException  {
		boolean check = true;

		SeekableStream stream = null;
		
		try {
			File loadFile = new File(fileUrl + File.separator + fileName);
			stream = new FileSeekableStream(loadFile);
			ParameterBlock param = new ParameterBlock();
			param.add(stream);
			PlanarImage rOp = JAI.create("stream", param);
			
			int basicWidth = rOp.getWidth();
			int basicHeight = rOp.getHeight();
			
			BufferedImage bi;
			BufferedImage thumb;
			
			if("5".equals(module)){													// 자산 이미지 업로드시 썸네일이미지는 720 pixel로 고정
				/*
				int ThumbWidth = (basicWidth * 720) / basicHeight;
				
				bi = rOp.getAsBufferedImage();
				thumb = new BufferedImage(ThumbWidth, 720, BufferedImage.TYPE_INT_RGB);
				Graphics2D g = thumb.createGraphics();
				g.drawImage(bi, 0, 0, ThumbWidth, 720, null);
				*/
				
				int ThumbHeight = (basicHeight * 1280) / basicWidth;
				
				bi = rOp.getAsBufferedImage();
				thumb = new BufferedImage(1280, ThumbHeight, BufferedImage.TYPE_INT_RGB);
				Graphics2D g = thumb.createGraphics();
				g.drawImage(bi, 0, 0, 1280, ThumbHeight, null);
			} else {																// 그 외는 300 pixel로 고정
				int ThumbHeight = (basicHeight * imageWidth) / basicWidth;
				
				bi = rOp.getAsBufferedImage();
				thumb = new BufferedImage(imageWidth, ThumbHeight, BufferedImage.TYPE_INT_RGB);
				Graphics2D g = thumb.createGraphics();
				g.drawImage(bi, 0, 0, imageWidth, ThumbHeight, null);
			}
			

//			String thumbFileDir = fileUrl + File.separator + "thumbFile";
//			File thumbDir = new File(thumbFileDir);
//			if(!thumbDir.exists()) thumbDir.mkdirs();

			// 썸네일 이미지 생성
			File file = new File(fileUrl + File.separator + "thum_" + fileName.substring(0, fileName.lastIndexOf(".")) + ".png");
			ImageIO.write(thumb, "png", file);
				
		} catch (IOException e) {
			
			e.printStackTrace();
			check = false;
			
		} finally{
			stream.close();
		}
		
		return check;
	
	}
	
	/**
	 * 
	 * @MethodName        : videoThumbnailMake
	 * @Date              : 2019. 03. 11.
	 * @author            : psj
	 * @Description       : 동영상 썸네일 생성
	 * @History           : 2019. 03. 11. 최초 작성
	 * @param fileUrl     : 파일 Path
	 * @param fileName    : 원본 파일 명
	 * @param module      : 모듈
	 * @return
	 * @throws IOException 
	 */
	private static void videoThumbnailMake(String fileUrl, String fileName, String module) throws IOException {

		//썸네일 생성
		String str = null;
		String[] cmd = new String[] {Config.getString("file.video.thumb.path")
				, "-i", fileUrl + File.separator + fileName, "-an", "-ss"
				, "00:00:01", "-r", "1", "-vframes", "1", "-y"
				, fileUrl +  File.separator + "thum_" + fileName.substring(0, fileName.lastIndexOf(".")) + ".png"};
		Process process = null;
		
		@SuppressWarnings("unused")
		String ext = fileName.substring(fileName.lastIndexOf(".")+1,fileName.length()).toLowerCase();
		
		try{
//			if("mp4".equals(ext)){
		    	// 프로세스 빌더를 통하여 외부 프로그램 실행
		    	process = new ProcessBuilder(cmd).start();
			    // 외부 프로그램의 표준출력 상태 버퍼에 저장
			    BufferedReader stdOut = new BufferedReader( new InputStreamReader(process.getInputStream()) );
			    // 표준출력 상태를 출력
			    while( (str = stdOut.readLine()) != null ) {
			    	System.out.println(str);
			    }
//		    }
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 이미지 파일  -> base64String 으로 추출
	 * @param image
	 * @param type
	 * @return
	 * @throws IOException 
	 */
	public static String encodeToString(File file , String type) throws IOException {
		
		BufferedImage image = ImageIO.read(file);
	
		String imageString = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		try {
			ImageIO.write(image, type, bos);
			byte[] imageBytes = bos.toByteArray();

			BASE64Encoder encoder = new BASE64Encoder();
			imageString = encoder.encode(imageBytes);
			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return imageString;
	}
	
	
	/**
     * 
     * @MethodName         : zipFileDownload
     * @Date               : 2018. 06. 01
     * @author             : 김영환
     * @Description        : 압축파일  다운로드
     * @param res          : response 객체
     * @throws IOException : 
     */
    public static void zipFileDownload(String path, String fileName, HttpServletResponse res) throws IOException{
    	
		File file = new File(path + fileName);
		
		res.reset();
		res.setContentType("application/x-msdownload;");
		res.setHeader("Content-Disposition", "attachment;filename=\"" + URLEncoder.encode(fileName, "UTF-8").replace("+", "%20") + "\";");
		OutputStream out = res.getOutputStream();
		FileInputStream fis = null;
		try{
			fis = new FileInputStream(file);
			FileCopyUtils.copy(fis, out);
		}finally{
			if(fis != null){
				try{
					fis.close();
				}catch(IOException ex){
					ex.printStackTrace();
				}
			}
			out.flush();
		}
    }
	
	
}
