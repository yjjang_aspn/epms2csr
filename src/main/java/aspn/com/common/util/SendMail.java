package aspn.com.common.util;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

/**
 * 메일전송 Class
 * @author 이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.07.04  이영탁          최초 생성
*
 * </pre>
 */
public class SendMail {
  private static SendMail instance = new SendMail();

 public static SendMail getInstance() {
   return instance;
  }
   
 /**
  * @param       host      메일서버주소
  * @param       from      보내는 사람
  * @param       to        받는사람 - ","로 구분된 메일 주소
  * @param       subject   제목
  * @param       mailtext  내용
  * @param       attached  첨부파일
  * @result      boolean
  * @exception   javax.mail.internet.AddressException
  *              javax.mail.MessagingException
  *              sendEmail("localhost", "보내는메일 ", "받는메일", "메일입니다.","메일내용입니다.")
  */

	public int sendEmailMember(String user_name, String user_mail, String subject, String contents)
			throws javax.mail.internet.AddressException, javax.mail.MessagingException,
			java.io.UnsupportedEncodingException, java.io.IOException,
			java.net.MalformedURLException {
		int nResult = 0;
		try {
			System.out.println(user_name + "/" + user_mail);
			String host = "smtp.gmail.com";
			String senderemail = "jsun@aspn.co.kr";
			String _user_name = user_name;
			String email = user_mail;

			// 메일내용중 동적으로 바꿔야 하는부분
			contents = contents.replace("@USER_NAME", _user_name);
			contents = contents.replace("../", "http://----------/");
			System.out.println(contents);

			Properties props = new Properties();
			props.put("mail.smtp.host", host);
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.auth", "true");

			MyAuthenticator auth = new MyAuthenticator("hellocompany.aspn@gmail.com", "last0015");

			Session session = Session.getDefaultInstance(props, auth);
			Multipart mp = new MimeMultipart(); 
			// create a message
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(senderemail));
			// 받는사람
			InternetAddress[] toAddress = InternetAddress.parse(email);
			msg.setRecipients(Message.RecipientType.TO, toAddress);
			// 제목
			msg.setSubject(subject, "UTF-8");
			// 내용
			MimeBodyPart mbp1 = new MimeBodyPart();
			mbp1.setContent(contents, "text/html; charset=UTF-8");
			mp.addBodyPart(mbp1);
			// 메시지 add
			msg.setContent(mp);
			// header 에 날짜 삽입
			msg.setSentDate(new Date());
			// send the message
			Transport.send(msg);
			// 메일 전송 성공
			nResult = 1;
		} catch (javax.mail.internet.AddressException ae) {
			ae.printStackTrace();
			nResult = -1;
		} catch (javax.mail.MessagingException me) {
			me.printStackTrace();
			nResult = -2;
		} finally {
		}
		return nResult;
	}
	
	
}
