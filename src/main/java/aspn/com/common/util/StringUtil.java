/*
 * Project : SHANY_FIWP
 * Package : com.shany.common.util
 * Class   : StringUtil.java
 * Author  : hork lim
 * Date    : Oct 25, 2012
 * To-do   : TODO
 *
 */
package aspn.com.common.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import org.springframework.http.HttpStatus;

public class StringUtil {
	/*
	 * Exception 오브젝트에서 메서지를 읽어온다.
	 */
	public static String getExceptionTrace(Exception ex){
		String exception = null;
		if(ex != null){
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.printStackTrace(pw);
			exception = sw.toString();
		}else {
			exception = "No Exception";
		}

		return exception;
	}
	
	/*
	 * Output object field to string
	 */
	public static String Object2String(Object object)
			throws IllegalArgumentException, IllegalAccessException {
		Class<?> c = object.getClass();

		StringBuilder sb = new StringBuilder();
		sb.append(c.getName());
		sb.append(" - [");

		Field[] fields = c.getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
			if (field.getType() == String.class) {
				sb.append(field.getName());
				sb.append(" = ");
				sb.append(field.get(object));
				sb.append(", ");
			}else if(field.getType() == List.class){
				sb.append(list2String((List<?>) field.get(object)));
			}else{
				sb.append(field.get(object).toString());
			}
		}
		sb.deleteCharAt(sb.length() - 2);
		sb.append("]");
		return sb.toString();
	}
	
	/*
	 * Output List to String
	 */
	public static String list2String(List<?> list) throws IllegalArgumentException, IllegalAccessException{
		StringBuilder sb = new StringBuilder();
		
		sb.append("List Size = ");
		sb.append(list.size());
		sb.append("\n");
		
		for(Object object : list){
			sb.append(Object2String(object));
			sb.append("\n");
		}
		
		return sb.toString();
	}
	
	/*
	 * Output String Array
	 */
	
	public static String Array2String(Object[] objArray){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for(int i = 0 ; i < objArray.length ; i++){
			sb.append("Index ");
			sb.append(i);
			sb.append(" = ");
			sb.append(objArray[i]);
			sb.append(",");
		}
		sb.deleteCharAt(sb.length()-1);
		sb.append("]");
		return sb.toString();
	}
	
	/*
	 * Concat String Array
	 */
	public static String append(String... list){
		StringBuilder sb = new StringBuilder();
		for(String s : list){
			sb.append(s);
		}
		return sb.toString();
	}
	
	/*
	 * URL Encoder Encoding
	 */
	@SuppressWarnings("deprecation")
	public static String encode(String... list){
		StringBuilder sb = new StringBuilder();
		for(String s : list){
			sb.append(s);
		}
		return URLEncoder.encode(sb.toString());
	}
	
	/*
	 * Sub String
	 */
	public static String subString(int offset, int count,String...list){
		StringBuilder sb = new StringBuilder();
		for(String s : list){
			sb.append(s);
		}
		if(sb.toString().length()<offset + count){
			return sb.toString();
		}
		return sb.toString().substring(offset, offset + count);
	}
	
	/**
	 * 문자열 null 체크
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		return str == null || str.length() == 0;
	}
	
	//null을 str로
	public static String nullToStr(Object object)
	{
		if(object == null)
			return "";
		else
			return object.toString().trim();
	}
	
	/**
	 * LME 구리시세 리턴
	 * @param str
	 * @return
	 */
	/*
	public static String getLMECopper() {
		String str="";
		String url = "http://info.finance.naver.com/marketindex/materialDetail.nhn?marketindexCd=CMDT_CDY";
		
		// Create an instance of HttpClient.
	    HttpClient client = new HttpClient();

	    // Create a method instance.
	    GetMethod method = new GetMethod(url);
	    
	    // Provide custom retry handler is necessary
	    method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, 
	    		new DefaultHttpMethodRetryHandler(3, false));

	    try {
	      // Execute the method.
	      int statusCode = client.executeMethod(method);

	      if (statusCode != HttpStatus.SC_OK) {
	        System.err.println("Method failed: " + method.getStatusLine());
	      }

	      // Read the response body.
	      byte[] responseBody = method.getResponseBody();
	      
	      // 네이버 LME 정보에서 구리의 시세를 수집함.
	      String s1 = new String(responseBody, "euc-kr");  
	      
	      int index = s1.indexOf("<span>구리</span>");
	      str = s1.substring(index+29,index+50);
	      str= str.substring(4,12);
	      
	    } catch (HttpException e) {
	      System.err.println("Fatal protocol violation: " + e.getMessage());
	      e.printStackTrace();
	    } catch (IOException e) {
	      System.err.println("Fatal transport error: " + e.getMessage());
	      e.printStackTrace();
	    } finally {
	      // Release the connection.
	      method.releaseConnection();
	    }  
	    
	    return str;
	}
	*/
	
	/**
	  * 주어진 길이(iLength)만큼 주어진 문자(cPadder)를 strSource의 왼쪽에 붙혀서 보내준다.
	  * ex) lpad("abc", 5, '^') ==> "^^abc"
	  *     lpad("abcdefghi", 5, '^') ==> "abcde"
	  *     lpad(null, 5, '^') ==> "^^^^^"
	  *
	  * @param strSource
	  * @param iLength
	  * @param cPadder
	  */
	 public static String lpad(String strSource, int iLength, char cPadder){
		 StringBuffer sbBuffer = null;
		 if (!isEmpty(strSource)){
			 int iByteSize = getByteSize(strSource);
			 if (iByteSize > iLength){
				 return strSource.substring(0, iLength);
			 }else if (iByteSize == iLength){
				 return strSource;
			 }else{
				 int iPadLength = iLength - iByteSize;
				 sbBuffer = new StringBuffer();
				 for (int j = 0; j < iPadLength; j++){
					 sbBuffer.append(cPadder);
				 }
				 sbBuffer.append(strSource);
				 return sbBuffer.toString();
			 }
		 }
	  //int iPadLength = iLength;
		 sbBuffer = new StringBuffer();
		 for (int j = 0; j < iLength; j++){
			 sbBuffer.append(cPadder);
		 }
		 return sbBuffer.toString();
	 }

	 /**
	  * 주어진 길이(iLength)만큼 주어진 문자(cPadder)를 strSource의 오른쪽에 붙혀서 보내준다.
	  * ex) lpad("abc", 5, '^') ==> "abc^^"
	  *     lpad("abcdefghi", 5, '^') ==> "abcde"
	  *     lpad(null, 5, '^') ==> "^^^^^"
	  *
	  * @param strSource
	  * @param iLength
	  * @param cPadder
	  */
	 public static String rpad(String strSource, int iLength, char cPadder){
		 StringBuffer sbBuffer = null;
		 if (!isEmpty(strSource)){
			 int iByteSize = getByteSize(strSource);
			 if (iByteSize > iLength){
				 return strSource.substring(0, iLength);
			 }else if (iByteSize == iLength){
				 return strSource;
			 }else{
				 int iPadLength = iLength - iByteSize;
				 sbBuffer = new StringBuffer(strSource);
				 for (int j = 0; j < iPadLength; j++){
					 sbBuffer.append(cPadder);
				 }
				 return sbBuffer.toString();
			 }
		 }
		 sbBuffer = new StringBuffer();
		 for (int j = 0; j < iLength; j++){
			 sbBuffer.append(cPadder);
		 }
		 return sbBuffer.toString();
	 }
	 
	 /**
	  *  byte size를 가져온다.
	  *
	  * @param str String target
	  * @return int bytelength
	  */
	 public static int getByteSize(String str){
		 if (str == null || str.length() == 0)
			 return 0;
		 byte[] byteArray = null;
		 try{
			 byteArray = str.getBytes("UTF-8");
		 }catch (UnsupportedEncodingException ex){}
		 if (byteArray == null) return 0;
		 return byteArray.length;
	 }
	 /*
	 //sid 읽어오기
	 public static String urlCon(String id, String pwd) throws IOException, JSONException{

		String sid = "";
        
        URL url = new URL("http://59.25.98.149:5000/webapi/auth.cgi?api=SYNO.API.Auth&version=2&method=login&account="+id+"&passwd="+pwd+"&session=DownloadStation&format=cookie");
//        URL url = new URL("http://128.134.80.207:5000/webapi/auth.cgi?api=SYNO.API.Auth&version=2&method=login&account="+id+"&passwd="+pwd+"&session=DownloadStation&format=cookie");
        
        // 문자열로 URL 표현
//        System.out.println("URL :" + "http://59.25.98.149:5000/webapi/auth.cgi?api=SYNO.API.Auth&version=2&method=login&account="+id+"&passwd="+pwd+"&session=DownloadStation&format=cookie");
        
        // HTTP Connection 구하기 
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        
        // 요청 방식 설정 ( GET or POST or .. 별도로 설정하지않으면 GET 방식 )
        conn.setRequestMethod("GET"); 
        
        // 연결 타임아웃 설정 
        conn.setConnectTimeout(5000); // 3초 
        // 읽기 타임아웃 설정 
        conn.setReadTimeout(5000); // 3초 
        
        // 요청 방식 구하기
        //System.out.println("getRequestMethod():" + conn.getRequestMethod());
        // 응답 콘텐츠 유형 구하기
       // System.out.println("getContentType():" + conn.getContentType());
        // 응답 코드 구하기
        //System.out.println("getResponseCode():"    + conn.getResponseCode());
        // 응답 메시지 구하기
        //System.out.println("getResponseMessage():" + conn.getResponseMessage());
        
        
        // 응답 헤더의 정보를 모두 출력
//        for (Map.Entry<String, List<String>> header : conn.getHeaderFields().entrySet()) {
//            for (String value : header.getValue()) {
//                System.out.println(header.getKey() + " : " + value);
//            }
//        }
        
        // 응답 내용(BODY) 구하기        
        try (InputStream in = conn.getInputStream();
                ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            
            byte[] buf = new byte[1024 * 8];
            int length = 0;
            while ((length = in.read(buf)) != -1) {
                out.write(buf, 0, length);
            }
           sid = new String(out.toByteArray(), "UTF-8");
           
           JSONObject json = null;
           json = new JSONObject(sid);
           
           System.out.println( " json : " + json );
           
           if("true".equals(json.getString("success"))){
        	   JSONObject json1 = null;
        	   json1 = new JSONObject(json.getString("data"));
        	   sid = json1.getString("sid");
           }else{
        	   sid = "false";
           }
//           sid = sid.replace("{\"data\":{\"sid\":\"", "").replace("\"},\"success\":true}", "").replace("\n", "");
           System.out.println("sid="+sid);
        }
        
        // 접속 해제
        conn.disconnect();        
        return sid;        
        
    }
	*/
}
