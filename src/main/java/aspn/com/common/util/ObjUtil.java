package aspn.com.common.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;

import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;

import sun.misc.BASE64Decoder;

public class ObjUtil
{
	private static final String DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ssZ";
	private static final String MiliSEC_FORMAT = "yyyyMMddHHmmssS";
	private static final String SIMPLE_FORMAT = "yyyyMMddHHmmss";
	private static final String YMD_FORMAT = "yyyyMMdd";
	private static final String SLASH_FORMAT = "yyyy/MM/dd";
	private static final String MONTH_FORMAT = "yyyy/MM";

	public static final String getTimeString() throws Exception{

		return (getTimeString(new Date()));
	}

	public static final String getTimeString(Date date) throws Exception{

		return (new SimpleDateFormat(DEFAULT_FORMAT).format(date));
	}

	public static final String getMiliTimeString() throws Exception{

		return (getMiliTimeString(new Date()));
	}

	public static final String getMiliTimeString(Date date) throws Exception{

		return (new SimpleDateFormat(MiliSEC_FORMAT).format(date));
	}

	public static final String getSimpleTimeString() throws Exception{

		return (getSimpleTimeString(new Date()));
	}

	public static final String getSimpleTimeString(Date date) throws Exception{

		return (new SimpleDateFormat(SIMPLE_FORMAT).format(date));
	}

	public static final String getYmdTimeString(Date date) throws Exception{

		return (new SimpleDateFormat(YMD_FORMAT).format(date));
	}
	
	public static final String getSlashCurrentDate() throws Exception{

		return (new SimpleDateFormat(SLASH_FORMAT).format(new Date()));
	}
	
	public static final String getMonthFirstDay() throws Exception{

		return (new SimpleDateFormat(MONTH_FORMAT).format(new Date())+"/01");
	}

	public static final String getHmsTimeString(Date date) throws Exception{

		String dateStr = new SimpleDateFormat(SIMPLE_FORMAT).format(date);

		return (dateStr.substring(8));
	}

	public static final Date stringToDate(String date) throws Exception{

		return (new SimpleDateFormat(SIMPLE_FORMAT).parse(date));
	}

	public static String dateStrToString(String date) throws Exception{

		return date.replaceAll("-","").replaceAll(":", "").replaceAll(" ","").replaceAll("/","");
	}

	public static HashMap<String, String> getSplitFileName(String fileName) throws Exception{

		HashMap<String, String> fileInfo=new HashMap<String, String>();

		if (fileName.lastIndexOf(".") > -1) {

			String onlyFilename = fileName.substring(0, fileName.lastIndexOf("."));
			String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1);

			fileInfo.put("fileName", onlyFilename);
			fileInfo.put("fileExt", fileExt);

		}else{
			fileInfo.put("fileName", fileName);
			fileInfo.put("fileExt", "");
		}
		return fileInfo;
	}

	public static String streamToString(InputStream is) throws Exception{

		BufferedReader reader = null;
		String line ="";
		StringBuffer sb = new StringBuffer();
		try{
			InputStreamReader isr = new InputStreamReader(is);
			reader = new BufferedReader(new InputStreamReader(is));

			while((line = reader.readLine())!=null){
				sb.append(line);
			}
			System.out.println(isr.getEncoding());
			return sb.toString().trim();

		}finally{

			if(reader!=null){
				reader.close();
			}
		}
	}

	/**************************************************************************
	 *  Null값을 Default값으로 변경
	 **************************************************************************/
	public static String nullToStr(String sString, String sDefault)
	{
		if ( sString == null || sString.equals("null") || sString.equals("") ) {
			return sDefault;
		}

		return sString;
	}
	
	public static String emptyToStr(String sString, String sDefault)
	{
		if ( sString == null || sString.equals("")) {
			return sDefault;
		}

		return sString;
	}

	public static String nullToStr(Object object)
	{
		if(object == null)
			return "";
		else
			return object.toString().trim();
	}

	public static Object nullToObj(Object obj) {
		if(obj == null) {
			if(obj instanceof Integer ||obj instanceof Long)
				return -1;
			else
				return "";
		} else {
			return obj;
		}
	}

	public static int nullToInt(String sString, int nDefault)
	{
		int nReturn = nDefault;

		try {
			nReturn = Integer.parseInt(sString.trim());
		} catch ( Exception e ) {
		}

		return nReturn;
	}


	public static int nullToInt(String sString)
	{
		return nullToInt(sString, 0);
	}

	public static int nullToInt(Object obj)
	{
		return nullToInt(obj, 0);
	}

	public static int nullToInt(Object obj, int pDefault)
	{
		if(obj == null || obj.equals(""))
			return pDefault;
		else if(obj instanceof String)
			return Integer.parseInt((String)obj);
		else if(obj instanceof BigDecimal)
			return Integer.valueOf(((BigDecimal) obj).intValue());
		else
			return (Integer)obj;
	}

	public static String trim(Object obj)
	{
		String sValue = (String) obj;
		sValue = nullToStr(sValue);

		return sValue.trim();
	}

	/**
	 * convert the null value into the empty string.
	 *
	 * @param str String input string.
	 * @return String converted string.
	 */
	public static String nullToEmpty(String str) {
		String value = str;
		if (value == null) {
			value = "";
		} else {
			value.trim();
		}
		return value;
	}

	/**
	 * convert the null value into the empty string.
	 *
	 * @param str
	 *            String input string.
	 * @return String converted string.
	 */
	public static int nullToEmpty(int num) {
		return num;
	}

	/**
	 * convert the null value into the Object.
	 *
	 * @param obj
	 * @return
	 */
	public static String nullToEmpty(Object obj) {
		String value = (String) obj;
		if (value == null) {
			value = "";
		} else {
			value.trim();
		}
		return value.trim();
	}

	public static String getFileExt(String source) {
		int pos = source.lastIndexOf( "." );
		String ext = source.substring( pos + 1 );
		return ext;

	}

	public static String generateKey(int length)
	{
		String sKey="";
		Random random=new Random(System.nanoTime());
		long r1 = random.nextLong();
		long r2 = random.nextLong();
		String hash1 = Long.toHexString(r1);
		String hash2 = Long.toHexString(r2);
		sKey = hash1 + hash2;
		if(sKey.length()>length)
		{
			sKey=sKey.substring(0,length);
		}
		return sKey.toUpperCase();
	}


	public static String generateTimeKey() throws Exception
	{
		String genKey = getMiliTimeString();
		return genKey;
	}


	public static String generateRanTimeKey(int plusCnt) throws Exception
	{
		String genKey = getMiliTimeString();

		if(plusCnt > 0)
			genKey+=randomString(plusCnt,1);

		return genKey;
	}

	public static String generateRanTimeKey(int staIdx, int plusCnt) throws Exception
	{
		String genKey = getMiliTimeString();

		if(plusCnt > 0)
			genKey+=randomString(plusCnt,1);

		genKey = genKey.substring(staIdx, genKey.length());

		return genKey;
	}

	public static HashMap<String, Object> array2Map(String[] keys, String[] values) {

		HashMap<String, Object>  resultMap = new HashMap<String, Object> ();

		if(keys.length>0){
			int i=0;
			for(String tmpKey:keys){
				resultMap.put(tmpKey, values[i]);
				i++;
			}
		}
		return resultMap;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static HashMap listItem2Map(List<HashMap<String, String>> dataList, String seperator) {

		String resultStr[] = null;
		String resultKeyStr[] = null;
		HashMap resultMap = new HashMap();

		if(dataList.size() > 0){

			int keyCnt = dataList.get(0).keySet().size();
			resultStr = new String[keyCnt];
			resultKeyStr = new String[keyCnt];

			int i=0;
			for(HashMap tmpMap : dataList){

				Iterator itr = tmpMap.keySet().iterator();

				String key = "";
				int j=0;
				while(itr.hasNext()){

					key = itr.next().toString();

					if(i==0){
						resultStr[j] = seperator+ tmpMap.get(key);
						resultKeyStr[j] = key;
					}
					else
						resultStr[j] = resultStr[j]+seperator+ tmpMap.get(key);

					j++;
				}

				i++;
			}

			i=0;

			for(String key:resultKeyStr){

				resultMap.put(key, resultStr[i]);
				i++;
			}

		}
		return resultMap;

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static HashMap list2Map(List<HashMap<String, Object>> dataList, String seperator) {

		String resultStr[] = null;
		String resultKeyStr[] = null;
		HashMap resultMap = new HashMap();

		if(dataList.size() > 0){

			int keyCnt = dataList.get(0).keySet().size();
			resultStr = new String[keyCnt];
			resultKeyStr = new String[keyCnt];

			int i=0;
			for(HashMap tmpMap : dataList){

				Iterator itr = tmpMap.keySet().iterator();

				String key = "";
				String value = "";
				int j=0;
				while(itr.hasNext()){

					key = itr.next().toString();

					if(tmpMap.get(key) == null || " ".equals(tmpMap.get(key).toString())){
						value = "";
					}
					else{
						value = tmpMap.get(key).toString();
					}
					
					if(i==0){
						resultStr[j] = seperator+ value;
						resultKeyStr[j] = key;
					}
					else{
						resultStr[j] = resultStr[j]+seperator+ value;
					}
					
					j++;
				}

				i++;
			}

			i=0;

			for(String key:resultKeyStr){

				resultMap.put(key, resultStr[i]);
				i++;
			}

		}

		return resultMap;
	}
	
	@SuppressWarnings("rawtypes")
	public static String list2MapRelated(List<HashMap<String, Object>> dataList, String seperator, String value, String text) {
		/*
		* 1.RELATED_KEY 가 될 INX를 정의한다. ex) data= [0][2][3] 01, A, 서울 01, B, 경기
		* 02, A, 도교 02, B, 오사카
		* 
		* 2. 최종결과 ex) Enum01=|A|B EnumKeys01=|서울|경기 ex) Enum02=|A|B
		* EnumKeys02=|도교|오사카
		* 
		* 3. RELATED_KEY는 칼럼의 합이다. 쿼리에서 합쳐 줘야한다. 3-1) data= [col1] [col2]
		* [value] [text] 01, 01a, A, 서울 01, 01b, B, 경기 02, 02a, A, 도교 02, 02b,
		* B, 오사카 3-2) data= [0] [2] [3] 01_01a, A, 서울 01_01b, B, 경기 02_02a, A,
		* 도교 02_02b, B, 오사카
		*/
		StringBuffer enumFullStr = new StringBuffer(); // 최종 Enum
		StringBuffer tmpEnum = new StringBuffer(); // 임시 Enum
		StringBuffer tmpEmumKeys = new StringBuffer(); // 임시 EnumKeys
	
		String compareRelatedKey = ""; // 관련키
		String nextRelatedKey = ""; // 다음 관련키
		String textStr = "";
		String valueStr = "";
	
		int i = 0;
		for (HashMap dataMap : dataList) {
			compareRelatedKey = toString(dataMap.get("RELATED_KEY"));
			
			if(" ".equals(toString(dataMap.get(text)))){
				textStr = "";
			}
			else{
				textStr = toString(dataMap.get(text));
			}
			appendStrBuffer(tmpEnum, seperator);
			appendStrBuffer(tmpEnum, textStr);
			
			if(" ".equals(toString(dataMap.get(value)))){
				valueStr = "";
			}
			else{
				valueStr = toString(dataMap.get(value));
			}
			appendStrBuffer(tmpEmumKeys, seperator);
			appendStrBuffer(tmpEmumKeys, valueStr);
	
			try {
				// 다음 번 관련키와 현재 관련키와 비교하여 다른지 비교한다.
				nextRelatedKey = toString(((HashMap) dataList.get(++i)).get("RELATED_KEY"));
			
				if (!StringUtils.equals(compareRelatedKey, nextRelatedKey)) { // 다음 관련키가 다르면
					appendStrBuffer(enumFullStr, "Enum" + compareRelatedKey + "=");
					appendStrBuffer(enumFullStr, " '" + getStrBufferToString(tmpEnum) + "' ");
					appendStrBuffer(enumFullStr, "EnumKeys" + compareRelatedKey + "=");
					appendStrBuffer(enumFullStr, " '" + getStrBufferToString(tmpEmumKeys) + "' ");
				
					// 초기화 한다.
					setStrBufferLength(tmpEnum, 0); // new StringBuffer(); //임시 Enum
					setStrBufferLength(tmpEmumKeys, 0); // new StringBuffer(); // 임시 EnumKeys
					}
			} catch (IndexOutOfBoundsException e) {
				appendStrBuffer(enumFullStr, "Enum" + compareRelatedKey + "=");
				appendStrBuffer(enumFullStr, " '" + getStrBufferToString(tmpEnum) + "' ");
				appendStrBuffer(enumFullStr, "EnumKeys" + compareRelatedKey + "=");
				appendStrBuffer(enumFullStr, " '" + getStrBufferToString(tmpEmumKeys) + "' ");
			}
	
		}
	
		return getStrBufferToString(enumFullStr);
	}
	
	/**
	* append StringBuffer
	* @param sb
	* @param str
	* @return
	*/
	public static StringBuffer appendStrBuffer(final StringBuffer sb, final String str) {
		return sb.append( str );
	}
	
	/**
	 * get StringBuffer To String
	 * @param sb
	 * @return
	 */
	public static String getStrBufferToString(final StringBuffer sb) {
		return sb.toString();
	}
	
	/**
	 * get StringBuffer Length
	 * @param sb
	 * @return
	 */
	public static StringBuffer setStrBufferLength(final StringBuffer sb, final int len) {
		if (sb != null && sb.length() > 0) {
			sb.setLength(len);
		}
		return sb;
	}
	
	/**
	 * ToString
	 * <p>Gets the <code>toString</code> of an <code>Object</code> returning
     * an empty string ("") if <code>null</code> input.</p>
     * 
     * <pre>
     * ObjectUtils.toString(null)         = ""
     * ObjectUtils.toString("")           = ""
     * ObjectUtils.toString("bat")        = "bat"
     * ObjectUtils.toString(Boolean.TRUE) = "true"
     * </pre>
     * 
     * @see StringUtils#defaultString(String)
     * @see String#valueOf(Object)
     * @param obj  the Object to <code>toString</code>, may be null
     * @return the passed in Object's toString, or nullStr if <code>null</code> input
     * @since 2.0
	 */
	public static String toString(Object obj) {
		return obj == null ? "" : obj.toString();
	}
	
	/**
	 * ToString
     * <p>Gets the <code>toString</code> of an <code>Object</code> returning
     * a specified text if <code>null</code> input.</p>
     * 
     * <pre>
     * ObjectUtils.toString(null, null)           = null
     * ObjectUtils.toString(null, "null")         = "null"
     * ObjectUtils.toString("", "null")           = ""
     * ObjectUtils.toString("bat", "null")        = "bat"
     * ObjectUtils.toString(Boolean.TRUE, "null") = "true"
     * </pre>
     * 
     * @see StringUtils#defaultString(String,String)
     * @see String#valueOf(Object)
     * @param obj  the Object to <code>toString</code>, may be null
     * @param nullStr  the String to return if <code>null</code> input, may be null
     * @return the passed in Object's toString, or nullStr if <code>null</code> input
     * @since 2.0
     */
    public static String toString(Object obj, String nullStr) {
        return obj == null ? nullStr : obj.toString();
    }

	/**
	 * Pseudo-random number generator object for use with randomString().
	 * The Random class is not considered to be cryptographically secure, so
	 * only use these random Strings for low to medium security applications.
	 */
	private static Random randGen = new Random();

	/**
	 * Array of numbers and letters of mixed case. Numbers appear in the list
	 * twice so that there is a more equal chance that a number will be picked.
	 * We can use the array to get a random number or letter by picking a random
	 * array index.
	 */
	private static char[] numbersAndLetters = ("0123456789abcdefghijklmnopqrstuvwxyz" +
			"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();

	private static char[] numbers = ("0123456789").toCharArray();

	/**
	 * Returns a random String of numbers and letters (lower and upper case)
	 * of the specified length. The method uses the Random class that is
	 * built-in to Java which is suitable for low to medium grade security uses.
	 * This means that the output is only pseudo random, i.e., each number is
	 * mathematically generated so is not truly random.<p>
	 * <p/>
	 * The specified length must be at least one. If not, the method will return
	 * null.
	 *
	 * @param length the desired length of the random String to return.
	 * @return a random String of numbers and letters of the specified length.
	 */
	public static String randomString(int length, int flag) { // flag:1 -number 2:string+number
		if (length < 1) {
			return null;
		}
		// Create a char buffer to put random letters and numbers in.
		char[] randBuffer = new char[length];

		if(flag == 1){
			for (int i = 0; i < randBuffer.length; i++) {

				randBuffer[i] = numbers[randGen.nextInt(10)];
			}
		}else{
			for (int i = 0; i < randBuffer.length; i++) {

				randBuffer[i] = numbersAndLetters[randGen.nextInt(71)];
			}
		}
		return new String(randBuffer);
	}

	public static String lpad(String source, int length, char padder){
		byte[] originString = source.getBytes();
		int    diffLength   = length - originString.length;
		String returnValue  = "";
		String totalPadder  = "";

		for(int i=0; i<diffLength; i++){
			totalPadder = padder + totalPadder;
		}

		returnValue = totalPadder + source;
		return returnValue;
	}

	public static String rpad(String source, int length, char padder){
		byte[] originString = source.getBytes();
		int    diffLength   = length - originString.length;
		String returnValue  = "";
		String totalPadder  = "";

		for(int i=0; i<diffLength; i++){
			totalPadder = padder + totalPadder;
		}

		returnValue = source + totalPadder;

		return returnValue;
	}
	
	public static void writeJson(HttpServletResponse response, HashMap<String, Object> rtnMap) throws Exception {

		response.setContentType ("application/json; charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.print(rtnMap);
	}

	public static int setCurRange(String curPage) throws Exception {
		return StringUtils.isBlank(curPage) ? 0 	: Integer.parseInt(curPage);
	}

	public static int setEndRange(String endPage) throws Exception {
		return StringUtils.isBlank(endPage) ? Integer.parseInt(Config.getString("app.list.range")) 	: Integer.parseInt(endPage);
	}
	
	@SuppressWarnings("restriction")
	public static String mailDecord(String param) throws Exception {
		String rtnParam = "";
		
		if(param != "" && param != null){
			rtnParam = param;
			//System.out.println("param : "+param);
			if(param.startsWith("=?UTF-8")){
				param = param.substring(10) ;
			}else if(param.startsWith("=?ISO")){
				param = param.substring(15) ;
			}
			
			if (param.indexOf('?') != -1){ 
				param = param.substring( 0 ,param.indexOf('?') ) ;

				BASE64Decoder decoder = new BASE64Decoder () ;
				param = new String(decoder.decodeBuffer(param)) ;
				rtnParam = param  ;
			}
		}

		return rtnParam;
	}
	
	public static String mailFromDecord(String param) throws Exception {
		String rtnParam = param.trim();
		if(param.trim().indexOf("=?") > -1){
			rtnParam = MimeUtility.decodeWord(param); 
		}
		
		return rtnParam;
	}
	
	public static String ReadableByteCount(long bytes, boolean si) {
		int unit = si ? 1000 : 1024;
		if (bytes < unit) return bytes + " B";
		int exp = (int) (Math.log(bytes) / Math.log(unit));
		String pre = (si ? "KMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
		return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	}
	
	
	/**
	 * replace 특수문자까지 처리
	 * @param org
	 * @param var
	 * @param tgt
	 * @return
	 */
	public static String replaceStr(String org, String var, String tgt) {
		StringBuffer str = new StringBuffer("");
		int end = 0;
		int begin = 0;
		if (org == null || org.equals(""))
			return org;
		do {
			end = org.indexOf(var, begin);
			if (end == -1) {
				end = org.length();
				str.append(org.substring(begin, end));
				break;
			}
			str.append((new StringBuilder(String.valueOf(org.substring(begin, end)))).append(tgt)
					.toString());
			begin = end + var.length();
		} while (true);
		return str.toString();
	}

	/**
	 * 문자열 null 체크
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		return str == null || str.length() == 0;
	}
	
	/**
	 * 숫자형  데이터 콤마 찍어줌
	 * @param str
	 * @return
	 */
	public static String cutNum(String str) {
		if (str == null || "".equals(str.trim())) {
			return "0";
		} else {
			return cvtNumber(str);
		}
	}

	public static String cvtNumber(String target) {
		DecimalFormat df = new DecimalFormat("#,###.####");
		String retStr;
		try {
			BigDecimal dcmData = new BigDecimal(target);
			retStr = df.format(dcmData);
		} catch (Exception e) {
			retStr = target;
		}
		return retStr;
	}
	
	/**
	 * String을 구분자를 기준으로 여러개의 String으로 나눠서 배열로 바꿔주는 메소드
	 * <p>
	 * 구분자가 연속될 경우 하나의 구분자로 인식하고 연속된 구분자 간의 값은 무시됨
	 * @param strIn - 구분자로 나누고자 하는 String
	 * @param strSeparator - String을 나누는 기준이 되는 구분자
	 * @return String을 구분자에 대해 나눈 String[]
	 */
	public static String[] separateString(String strIn, String strSeparator) {
		String as[] = null;

		ArrayList<String> arraylist = new ArrayList<String>();
		for (StringTokenizer stringtokenizer = new StringTokenizer(strIn, strSeparator); stringtokenizer
				.hasMoreTokens(); arraylist.add(stringtokenizer.nextToken().trim()))
			;
		as = arraylist.toArray(new String[arraylist.size()]);

		return as;
	}
	
	
	/**
	 * String을 구분자를 기준으로 여러개의 String으로 나눠서 배열로 바꿔주는 메소드
	 * <p>
	 * 구분자가 연속될 경우 연속된 구분자 간에는 빈문자("")로 처리됨
	 * <p>
	 * J2SE 1.4 이상에서는 java.lang.String class의 split() 메소드를 통해 구현 가능
	 * @param strIn - 구분자로 나누고자 하는 String
	 * @param strSplitter - String을 나누는 기준이 되는 구분자
	 * @return String을 구분자에 대해 나눈 String[]
	 */
	public static String[] splitString(String strIn, String strSplitter) {
		String as[] = null;

		ArrayList<String> arraylist = new ArrayList<String>();
		int i = strSplitter.length();
		int j = 0;

		while ((j = strIn.indexOf(strSplitter)) > -1) {
			int k = j + i;
			arraylist.add(strIn.substring(0, j));
			strIn = strIn.substring(k);
		}
		arraylist.add(strIn);
		as = arraylist.toArray(new String[arraylist.size()]);

		return as;
	}
	
	/**
	 * 배열 값 세팅
	 * @param str
	 * @return
	 */
	public static String setArrString(String[] str, int i) {

		try {
			if (str == null) {
				return "";
			}

			if (str.length < i) {
				return "";
			}

			return str[i];
		} catch (Exception e) {
			return "";
		}
	}
	
	public static List<HashMap<String, Object>> ConvertObjListToMapList(List<Class> list){
		List<HashMap<String, Object>> resultList = new ArrayList<>();
		
		try {
			for(int i=0; i<=list.size(); i++){
				Map map = new HashMap();
				
				Object obj = list.get(i);
				map = ConverObjectToMap(obj);
				resultList.add((HashMap<String, Object>) map);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resultList; 
	}
	
	public static Map ConverObjectToMap(Object obj){
		
        try {
            //Field[] fields = obj.getClass().getFields(); //private field는 나오지 않음.
            Field[] fields = obj.getClass().getDeclaredFields();
            Map resultMap = new HashMap();
            for(int i=0; i<=fields.length-1;i++){
                fields[i].setAccessible(true);
                resultMap.put(fields[i].getName(), fields[i].get(obj));
            }
            return resultMap;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<String> nullCheck(Object srcObject, String... checkFields) throws Exception {
		List<String> nullList;
		if (srcObject == null) {
			return null;
		} else {
			 nullList = new ArrayList<>();
			Map describeMap = BeanUtils.describe(srcObject);
			for (String field : checkFields) {
				if (StringUtils.isEmpty((String) describeMap.get(field))) {
					nullList.add(field);
				}
			}
		}
		return nullList;
	}
}
