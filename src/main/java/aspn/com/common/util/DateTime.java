package aspn.com.common.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.SimpleTimeZone;

public final class DateTime {
	
	/**
	 * <p>
	 * <ul>
	 * <li> 날자 포맷이 YYYY-MM-DD 형태로 올바르게 지정되어 있는지를 검사 한다.
	 * <li> 날자가 정확히 입력이 되었는지 검사 한다.
	 * <li> public static void check(String s, String format) Method를 이용하여 처리
	 * </ul>
	 * </p>
	 * 
	 * @param s
	 *            [String]: 체크의 대상인 원본 문자열
	 * @throws <code>Exception</code> 포맷의 불일치 혹은 잘못된 날자 데이터인 경우 발생
	 */
	public static java.util.Date check(String s) throws Exception {
		return DateTime.check(s, "yyyy-MM-dd");
	}

	/**
	 * <p>
	 * <ul>
	 * <li> 지정한 날자 포맷으로 데이터가 표시되어 있는지를 확인 한다.
	 * <li> 날자가 정확히 입력이 되었는지 검사 한다.
	 * <li> public static void check(String s, String format) Method를 이용하여 처리
	 * </ul>
	 * </p>
	 * 
	 * @param s
	 *            [String]: 체크의 대상인 원본 문자열
	 * @param format
	 *            [String]: 포맷 검사에 사용할 포맷 스트링
	 * @throws <code>ParserException</code> 포맷의 불일치 혹은 잘못된 날자 데이터인 경우 발생
	 */
	public static java.util.Date check(String s, String format)
			throws java.text.ParseException {
		// -- 검사할 대상 문자열이 없는 경우
		if (s == null) {
			throw new NullPointerException("date string to check is null");
		}

		// -- 검사할 포맷이 지정되지 않은 경우
		if (format == null) {
			throw new NullPointerException(
					"format string to check date is null");
		}

		// -- Local을 고려한 포맷 객체를 생성한다.
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				format, java.util.Locale.KOREA);

		// -- 날자 관련 오브젝트를 생성한다.
		java.util.Date date = null;

		try {
			// -- 지정한 날자 포맷으로 문자열이 지정되었는지 검사 한다.
			date = formatter.parse(s);
		} catch (java.text.ParseException e) // -- 포맷에 맞지 않는 문자열이 지정된 경우
		{
			// throw new java.text.ParseException( e.getMessage() + " with
			// format \"" + format + "\"", e.getErrorOffset());
			throw new java.text.ParseException(" wrong date:\"" + s
					+ "\" with format \"" + format + "\"", 0);
		}

		// -- 포맷은 맞으나 날자로 사용할 수 없는 문자열이 지정된 경우 (ex: 2000-30-99)
		if (!formatter.format(date).equals(s)) {
			throw new java.text.ParseException("Out of bound date:\"" + s
					+ "\" with format \"" + format + "\"", 0);
		}
		return date;
	}

	/**
	 * <p>
	 * <ul>
	 * <li> 날자 스트링을 기본 포맷인 "yyyyMMdd"를 이용하여 올바로 작성된 것인지를 확인한다.
	 * <li> (ex: boolean b = DateTime.isValid("19990101");)
	 * </ul>
	 * </p>
	 * 
	 * @param s
	 *            [String]: "yyyyMMdd"의 형식을 검사할 대상 문자열
	 * @return [boolean]: true 날짜 형식이 맞고, 존재하는 날짜일 때 false 날짜 형식이 맞지 않거나, 존재하지
	 *         않는 날짜일 때
	 */
	public static boolean isValid(String s) throws Exception {
		return DateTime.isValid(s, "yyyyMMdd");
	}

	/**
	 * <p>
	 * <ul>
	 * <li> 날자 스트링을 사용자가 지정한 날자포맷을 이용하여 올바로 작성된 것인지를 확인한다.
	 * <li> (ex: boolean b = DateTime.isValid("19990101","yyyyMMdd");)
	 * </ul>
	 * </p>
	 * 
	 * @param s
	 *            [String]: 사용자가 지정한 날자 포맷을 이용하여 형식을 검사할 대상 문자열
	 * @param format
	 *            [String] : 형식 검사에 사용할 날자 포맷 문자열
	 * @return [boolean]: true 날짜 형식이 맞고, 존재하는 날짜일 때 false 날짜 형식이 맞지 않거나, 존재하지
	 *         않는 날짜일 때
	 */
	public static boolean isValid(String s, String format) {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				format, java.util.Locale.KOREA);
		java.util.Date date = null;
		try {
			date = formatter.parse(s);
		} catch (java.text.ParseException e) {
			return false;
		}

		if (!formatter.format(date).equals(s)) {
			return false;
		}

		return true;
	}

	/**
	 * <p>
	 * <ul>
	 * <li> 사용자가 지정한 패턴을 형태를 이용한 현 Date 정보의 리턴
	 * <li> (ex: String time = DateTime.getFormatString("yyyy-MM-dd HH:mm:ss");)
	 * </ul>
	 * </p>
	 * 
	 * @param pattern
	 *            [String]: 출력시 사용할 패턴
	 * @return [String]: 지정한 패턴에 맞춘 현재 Date정보
	 */
	public static String getFormatDateString(String pattern) {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				pattern, java.util.Locale.KOREA);
		String dateString = formatter.format(new java.util.Date());
		return dateString;
	}

	/**
	 * <p>
	 * <ul>
	 * <li> yyyy-mm-dd 포맷으로 지정 일자를 리턴 한다.
	 * </ul>
	 * </p>
	 * 
	 * @return [String]: yyyy-mm-dd 패턴에 맞춘 현재 Date정보
	 */
	public static String getDateString() {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				"yyyy-MM-dd", java.util.Locale.KOREA);
		return formatter.format(new java.util.Date());
	}

	/**
	 * <p>
	 * <ul>
	 * <li> 구분자 없이 날자를 yyyymmdd형태의 문자열로 리턴 한다.
	 * </ul>
	 * </p>
	 * 
	 * @return [String]: yyyymmdd 패턴에 맞춘 현재 Date정보
	 */
	public static String getShortDateString() {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				"yyyyMMdd", java.util.Locale.KOREA);
		return formatter.format(new java.util.Date());
	}

	/**
	 * <p>
	 * <ul>
	 * <li> 타임 스템프로 사용하기 위한 "yyyy-MM-dd-HH:mm:ss". 형태의 문자열 리턴
	 * </ul>
	 * </p>
	 * 
	 * @return [String]: "yyyy-MM-dd-HH:mm:ss" 패턴에 맞춘 현재 Date정보
	 */
	public static String getTimeStampString() {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				"yyyy-MM-dd-HH:mm:ss:SSS", java.util.Locale.KOREA);
		return formatter.format(new java.util.Date());
	}

	/**
	 * <p>
	 * <ul>
	 * <li> 현재 시간을 "HH:mm:ss".형태의 포맷 스트링으로 리턴한다.
	 * </ul>
	 * </p>
	 * 
	 * @return [String]: "HH:mm:ss" 패턴에 맞춘 현재 시간정보
	 */
	public static String getTimeString() {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				"HH:mm:ss", java.util.Locale.KOREA);
		return formatter.format(new java.util.Date());
	}

	/**
	 * <p>
	 * <ul>
	 * <li> 구분자 없이 시간 정보를 hhmmss형태의 문자열로 리턴 한다.
	 * </ul>
	 * </p>
	 * 
	 * @return [String]: "hhmmss" 패턴에 맞춘 현재 시간정보
	 */
	public static String getShortTimeString() {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				"HHmmss", java.util.Locale.KOREA);
		return formatter.format(new java.util.Date());
	}

	/**
	 * <p>
	 * <ul>
	 * <li> 현재 년도에 관련 정보를 정수로 리턴한다.
	 * </ul>
	 * </p>
	 * 
	 * @return [int]: 현재 년도에 관련 정보를 정수로 리턴한다.
	 */
	public static int getYear() {
		return getNumberByPattern("yyyy");
	}
	
	/**
	 * <p>
	 * <ul>
	 * <li> 현재 년도에 관련 정보를 정수로 리턴한다.
	 * </ul>
	 * </p>
	 * 
	 * @return [string]: 현재 년도에 관련 정보를 문자형으로 리턴한다.
	 */
	public static String getYearStr() {
		return getStringByPattern("yyyy");
	}

	/**
	 * <p>
	 * <ul>
	 * <li> 현재 월에 관련 정보를 정수로 리턴한다.
	 * </ul>
	 * </p>
	 * 
	 * @return [int]: 현재 월에 관련 정보를 정수로 리턴한다.
	 */
	public static String getMonth() {
		return getStringByPattern("MM");
	}

	/**
	 * <p>
	 * <ul>
	 * <li> 현재 날자에 관련 정보를 정수로 리턴한다.
	 * </ul>
	 * </p>
	 * 
	 * @return [int]: 현재 날자에 관련 정보를 정수로 리턴한다.
	 */
	public static String getDay() {
		return getStringByPattern("dd");
	}

	/**
	 * <p>
	 * <ul>
	 * <li> 지정한 패턴에 의하여 날자 혹은 월 년을 정수 형태를 리턴한다.
	 * </ul>
	 * </p>
	 * 
	 * @param patten
	 *            [String]: 지정할 패턴 문자열
	 * @return [int]: 지정한 패턴에 의하여 날자 혹은 월 년을 정수 형태를 리턴한다.
	 */
	public static int getNumberByPattern(String pattern) {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				pattern, java.util.Locale.KOREA);
		String dateString = formatter.format(new java.util.Date());
		return Integer.parseInt(dateString);
	}


	/**
	 * <p>
	 * <ul>
	 * <li> 지정한 패턴에 의하여 날자 혹은 월 년을 정수 형태를 리턴한다.
	 * </ul>
	 * </p>
	 * 
	 * @param patten
	 *            [String]: 지정할 패턴 문자열
	 * @return [int]: 지정한 패턴에 의하여 날자 혹은 월 년을 정수 형태를 리턴한다.
	 */
	public static String getStringByPattern(String pattern) {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				pattern, java.util.Locale.KOREA);
		String dateString = formatter.format(new java.util.Date());
		return dateString;
	}
	/**
	 * <p>
	 * <ul>
	 * <li> 날자데이터를 문자열로 입력 받아 해당하는 요일 정보를 리턴한다.
	 * <li> 이때 사용하는 날자 패턴은 기본 패턴인 (yyyyMMdd)를 이용한다.
	 * </ul>
	 * </p>
	 * 
	 * @param s
	 *            [String]: 해당 요일을 리턴받고자 하는 대상 일자를 표현한 문자열 (표준 포맷 "yyyyMMdd"형태
	 *            유지)
	 * @return [int]: 날짜 형식이 맞고, 존재하는 날짜일 때 요일을 리턴 0: 일요일
	 *         (java.util.Calendar.SUNDAY 와 비교) 1: 월요일
	 *         (java.util.Calendar.MONDAY 와 비교) 2: 화요일
	 *         (java.util.Calendar.TUESDAY 와 비교) 3: 수요일
	 *         (java.util.Calendar.WENDESDAY 와 비교) 4: 목요일
	 *         (java.util.Calendar.THURSDAY 와 비교) 5: 금요일
	 *         (java.util.Calendar.FRIDAY 와 비교) 6: 토요일
	 *         (java.util.Calendar.SATURDAY 와 비교)
	 * @throws <code>java.text.ParseException</code> 형식이 잘못 되었거나 존재하지 않는 날짜가 지정된
	 *             경우 <br>
	 *             <b> 예제 <b><br>
	 * 
	 * 예) String s = "20000529"; int dayOfWeek = whichDay(s, format); if
	 * (dayOfWeek == java.util.Calendar.MONDAY) System.out.println(" 월요일: " +
	 * dayOfWeek); if (dayOfWeek == java.util.Calendar.TUESDAY)
	 * System.out.println(" 화요일: " + dayOfWeek);
	 */
	public static int whichDay(String s) throws java.text.ParseException {
		return whichDay(s, "yyyyMMdd");
	}

	/**
	 * <p>
	 * <ul>
	 * <li> 날자데이터를 문자열로 입력 받아 해당하는 요일 정보를 리턴한다.
	 * <li> 이때 사용하는 날자 패턴은 사용자가 지정한 패턴을 이용한다.
	 * </ul>
	 * </p>
	 * 
	 * @param s
	 *            [String]: 해당 요일을 리턴받고자 하는 대상 일자를 표현한 문자열
	 * @param format
	 *            [String] : 지정한 날자의 형식등을 검사하기 위하여 지정한 사용자 패턴 스트링
	 * @return [int]: 날짜 형식이 맞고, 존재하는 날짜일 때 요일을 리턴 0: 일요일
	 *         (java.util.Calendar.SUNDAY 와 비교) 1: 월요일
	 *         (java.util.Calendar.MONDAY 와 비교) 2: 화요일
	 *         (java.util.Calendar.TUESDAY 와 비교) 3: 수요일
	 *         (java.util.Calendar.WENDESDAY 와 비교) 4: 목요일
	 *         (java.util.Calendar.THURSDAY 와 비교) 5: 금요일
	 *         (java.util.Calendar.FRIDAY 와 비교) 6: 토요일
	 *         (java.util.Calendar.SATURDAY 와 비교)
	 * @throws <code>java.text.ParseException</code> 형식이 잘못 되었거나 존재하지 않는 날짜가 지정된
	 *             경우 <br>
	 *             <b> 예제 <b><br>
	 * 
	 * 예) String s = "2000-05-29"; int dayOfWeek = whichDay(s, "yyyy-MM-dd"); if
	 * (dayOfWeek == java.util.Calendar.MONDAY) System.out.println(" 월요일: " +
	 * dayOfWeek); if (dayOfWeek == java.util.Calendar.TUESDAY)
	 * System.out.println(" 화요일: " + dayOfWeek);
	 */

	public static int whichDay(String s, String format)
			throws java.text.ParseException {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
				format, java.util.Locale.KOREA);
		java.util.Date date = check(s, format);

		java.util.Calendar calendar = formatter.getCalendar();
		calendar.setTime(date);
		return calendar.get(java.util.Calendar.DAY_OF_WEEK);
	}
	
	/**
	 * <p>
	 * <ul>
	 * <li> 날짜형을 지정된 형태의 String값으로 리턴한다.
	 * </ul>
	 * </p>
	 * 
	 * @param patten
	 *            [String]: 지정할 패턴 문자열
	 * @return [int]: 지정한 패턴에 의하여 날자 혹은 월 년을 정수 형태를 리턴한다.
	 */
	public static String convertDateToString(Date d, String pattern) {
		SimpleDateFormat date= new SimpleDateFormat(pattern);
		return date.format(d);			
	}
	
	/**
	 *	해당  
	 * @param Month
	 * @return
	 */
	public static String requestMmDay(String Month, String type){
		SimpleDateFormat dateFormat = new SimpleDateFormat(type);
		Calendar cal = Calendar.getInstance();
		
		cal.add(Calendar.YEAR, 0); 
		cal.add(Calendar.MONTH, -Integer.parseInt(Month));
		cal.add(Calendar.DATE, 0); 
		
		String ymd = dateFormat.format(cal.getTime());  

		return ymd;
	}
	
	
	/**
	 * 두 날짜간의 차이 구하기...
	 * @author 
	 * @since 2006. 1. 23.
	 * @param sDate
	 * @param eDate
	 * @return
	 */
	public static String diffDate(String sDate, String eDate) {
		long gap = 0;
		try {

			sDate = ObjUtil.replaceStr(sDate, "-", "");
			eDate = ObjUtil.replaceStr(eDate, "-", "");

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Date date1 = sdf.parse(sDate);
			Date date2 = sdf.parse(eDate);
			gap = date2.getTime() - date1.getTime();
			gap = gap / (24 * 60 * 60 * 1000);
		} catch (Exception e) {
			return "0";
		}
		return String.valueOf(gap);
	}

	/**
	 * 두 개월간의 차이 구하기...
	 * @author 
	 * @since 2006. 1. 23.
	 * @param sDate
	 * @param eDate
	 * @return
	 */
	public static int diffMonth(String sDate, String eDate) {

		int sYear = Integer.parseInt(sDate.substring(0, 4));
		int sMonth = Integer.parseInt(sDate.substring(4, 6));

		int eYear = Integer.parseInt(eDate.substring(0, 4));
		int eMonth = Integer.parseInt(eDate.substring(4, 6));

		int month_diff = (eYear - sYear) * 12 + (eMonth - sMonth);

		return month_diff;
	}

	/**
	 * 현재일로부터 일주일전 날짜 구하기
	 * @return
	 */
	public static String get7DayAgoDate(){
		Calendar cal = Calendar.getInstance(new SimpleTimeZone(0x1ee6280, "KST"));
		cal.add(Calendar.DATE, -7);
		
		Date weekago = cal.getTime();
		SimpleDateFormat fomatter = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
		
		return fomatter.format(weekago);
		
	}
}
