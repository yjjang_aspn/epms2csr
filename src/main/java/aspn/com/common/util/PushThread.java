package aspn.com.common.util;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.springframework.util.ResourceUtils;

import aspn.hello.com.model.DeviceVO;

public class PushThread extends Thread{
	private List<DeviceVO> pushList = new ArrayList<DeviceVO>();
	private List<DeviceVO> pushSendList = new ArrayList<DeviceVO>();
	private String pushTitle  = "";
	private String pushMenuType = "";
	private String pushMenuDetailNdx 	 = "";
	private String pushAlarm 	 = "";	// 알림음 유형 (01:일반, 02:긴급)
	private DeviceVO deviceVO = new DeviceVO();
	boolean go = true;
	
//	public void setParam(List<DeviceModel> pushList, String msgTitle, String actionUrl, String topNm) throws Exception{
//		this.pushList = pushList;
//		this.msgTitle = msgTitle;
//		this.actionUrl = actionUrl;
//		this.topNm = topNm;
//	}
	
	public void setParam(List<DeviceVO> pushList, String pushTitle, String pushMenuType, String pushMenuDetailNdx, String pushAlarm) throws Exception{
		this.pushList = pushList;
		this.pushTitle = pushTitle;
		this.pushMenuType = pushMenuType;
		this.pushMenuDetailNdx = pushMenuDetailNdx;
		this.pushAlarm = pushAlarm;
	}
	
	public void run(){		
		deviceVO.setPushTitle(pushTitle);
		deviceVO.setPushMenuType(pushMenuType);
		deviceVO.setPushMenuDetailNdx(pushMenuDetailNdx);
		deviceVO.setPushAlarm(pushAlarm);
		
		if(pushList.size() > 0 ){
			int baseI = 0;
			int overI = pushList.size() / 500;
			
			if(pushList.size() > 0){
				for(int i=0; i<pushList.size(); i++){
					if(i >= baseI && i <((baseI+1) * 500)){
						pushSendList.add(pushList.get(i));
					}
	
					if((i+1) % 500 == 0){		
						
						if(go){
							if(setPush(pushSendList).equals("Y")){
								go = true;
							};
						}
						
						pushSendList.clear();
						baseI++;
					}
					
					if(i == pushList.size() -1 && overI == baseI){	
						setPush(pushSendList);
					}
				}
			}else{	
				setPush(pushSendList);
			}
		}
	}

	public String setPush(List<DeviceVO> pushList){
		go = false;
		String tokenMember[] = new String[pushList.size()];
		String tokenPushId[] = new String[pushList.size()];
		String tokenPlatfm[] = new String[pushList.size()];
		int currentPosition = 0;
		String rtnContinue = "";

		
		if(pushList.size() > 0 ){
			for (DeviceVO deviceMd : pushList) {
				String tmpMember =  deviceMd.getUSER_ID();
				String tmpPushId =  deviceMd.getPUSH_ID();
				String tmpFlatfm =  deviceMd.getCD_PLATFORM();
				
				tokenMember[currentPosition] = tmpMember;
				tokenPushId[currentPosition] = tmpPushId;
				tokenPlatfm[currentPosition] = tmpFlatfm;
				
				currentPosition++;
			}
		}

		deviceVO.setTrgtMemberArray(tokenMember);
		deviceVO.setTrgtPushIdArray(tokenPushId);
		deviceVO.setTrgtPlatFmArray(tokenPlatfm);
		
		try {
			// 프로퍼티 객체 생성
            Properties _config = new Properties();
            // 프로퍼티 파일 로딩
            _config.load(new java.io.BufferedInputStream(new FileInputStream(ResourceUtils.getFile("classpath:/config/config.properties"))));
            
			PushHandler pushHd = new PushHandler();
			if(_config.getProperty("production_mode").equals("dev")){
				pushHd.setPushInfo(deviceVO, "dev"); // dev - 개발, real - 운영
			}else{
				pushHd.setPushInfo(deviceVO, "real"); // dev - 개발, real - 운영
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		rtnContinue = "Y";
		
		return rtnContinue;

	}
}
