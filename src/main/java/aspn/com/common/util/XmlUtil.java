package aspn.com.common.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlUtil {

	static Logger logger = LoggerFactory.getLogger(XmlUtil.class);
	
	public static boolean xmlPut(String fileName, String path, Object inClass, String encoding) throws Exception{
		
		OutputStreamWriter fileWriter=null;
		
		boolean retval = false;
		
		try {
			
			fileWriter = new OutputStreamWriter(new FileOutputStream(path+fileName, false), encoding);
			fileWriter.write("<?xml version=\"1.0\" encoding=\""+encoding+"\"?>\n");
			
			Serializer serializer = new Persister();

			serializer.write(inClass, fileWriter);
			
			retval = true;
		}catch(Exception e){
			e.printStackTrace();logger.error(e.getMessage());
		}finally{
			
			if(fileWriter !=null) {
				fileWriter.flush();
				fileWriter.close();
				fileWriter = null;
			}
		}
		return retval;
	}
	
	public static String xmlPut(Object inClass, String encoding) throws Exception{
		
		ByteArrayOutputStream out=null;
		OutputStreamWriter writer =null;
		String retString = "";
		
		try {
			out = new ByteArrayOutputStream();
			writer = new OutputStreamWriter(out,encoding);

			writer.write("<?xml version=\"1.0\" encoding=\""+encoding+"\"?>\n");
			Serializer serializer = new Persister();
			serializer.write(inClass,writer);
			retString = out.toString();
		}catch(Exception e){
			e.printStackTrace();logger.error(e.getMessage());
		}finally{
			
			if(out !=null) {
				out.close();
				out = null;
			}
			
			if(writer !=null) {
				writer.close();
				writer = null;
			}
		}
		
		return retString;
	}
	

	
	public static <T> Object xmlFileGet(String fileName, String path, Class<T> inClass, String encoding) throws Exception{
		
		InputStreamReader reader = null;
		Object result = null;

		try{
			Serializer serializer = new Persister();
	
			result = inClass.newInstance();
	
			File file = new File(XmlUtil.class.getClassLoader().getResource("").getPath()+path+fileName);
			
			reader = new InputStreamReader(new FileInputStream(file), encoding);
			
			result = serializer.read(inClass,  reader);
		}catch(Exception e){
			e.printStackTrace();logger.error(e.getMessage());
		}finally{
			
			if(reader !=null) {
				reader.close();
				reader = null;
			}
		}
		return result;
	}
	

	public static <T> Object xmlFileGet(String fileName, Class<T> inClass, String encoding) throws Exception{
		
		InputStreamReader reader = null;
		Object result = null;
		
		try{
			Serializer serializer = new Persister();
	
			File file = new File(XmlUtil.class.getClassLoader().getResource("").getPath()+fileName);
			
			reader = new InputStreamReader(new FileInputStream(file), encoding);
			
			result = serializer.read(inClass,  reader);
		
		}catch(Exception e){
			
			e.printStackTrace();logger.error(e.getMessage());
			
		}finally{
			
			if(reader !=null) {
				reader.close();
				reader = null;
			}
		}
		return result;
	}

	
	public static <T> Object xmlStrGet(StringBuffer xmlStr, Class<T> inClass, String encoding) throws Exception{
		
		InputStreamReader reader = null;
		ByteArrayInputStream in =null;
		Object result = null;
		
		try{
			byte[] readBytes = xmlStr.toString().getBytes();
			
			in = new ByteArrayInputStream(readBytes);
			
			reader = new InputStreamReader(in,encoding);
			
			Serializer serializer = new Persister();
			
			result = inClass.newInstance();
	
			result = serializer.read(inClass,  reader);
		
		}catch(Exception e){
			e.printStackTrace();logger.error(e.getMessage());
		}finally{
			
			if(reader !=null) {
				reader.close();
				reader = null;
			}
			
			if(in !=null) {
				in.close();
				in = null;
			}
		}
		return result;
	}
	
	public static <T> Object xmlStrGet(String xmlStr, Class<T> inClass, String encoding) throws Exception{

		InputStreamReader reader = null;
		ByteArrayInputStream in =null;
		Object result = null;
		
		try{
			byte[] readBytes = xmlStr.getBytes();
			
			in = new ByteArrayInputStream(readBytes);
			
			reader = new InputStreamReader(in,encoding);
			
			Serializer serializer = new Persister();
			
			result = inClass.newInstance();
			
			result = serializer.read(inClass,  reader);
		
		}catch(Exception e){
			e.printStackTrace();logger.error(e.getMessage());
		}finally{
			
			if(reader !=null) {
				reader.close();
				reader = null;
			}
			
			if(in !=null) {
				in.close();
				in = null;
			}
		}
		return result;
	}
	
	
	public static <T> Object xmlByteGet(byte[] xmlByteStream, Class<T> inClass, String encoding) throws Exception{

		InputStreamReader reader = null;
		ByteArrayInputStream in =null;
		Object result = null;
		
		try{
			
			in = new ByteArrayInputStream(xmlByteStream);
			
			reader = new InputStreamReader(in,encoding);
			
			Serializer serializer = new Persister();
			
			result = inClass.newInstance();
			
			result = serializer.read(inClass,  reader);
		
		}catch(Exception e){
			e.printStackTrace();logger.error(e.getMessage());
		}finally{
			
			if(reader !=null) {
				reader.close();
				reader = null;
			}
			
			if(in !=null) {
				in.close();
				in = null;
			}
		}
		return result;
	}
	
	public static String parseXML(String XML){
		if(XML==null){
			XML="";
		}
		if(XML.equals("")){
			
		}
		if(XML.charAt(0)=='&'){
			XML = XML.replaceAll("&lt;","<").replaceAll("&gt;",">").replaceAll("&quot;","\"").replaceAll("&apos;","'");
			XML = XML.replaceAll("&amp;","&");
		}

		return XML;
	}
	
	
	public static  org.w3c.dom.Document convertNodesFromXml(String xml) throws Exception {

		InputStream is = new ByteArrayInputStream(xml.getBytes("UTF-8"));
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.parse(is);
		
		return document;
	}

	public static org.w3c.dom.Element[] nodeToElement(org.w3c.dom.Document doc, String nodeName){
		if(doc==null){
			return null;
		}
		org.w3c.dom.NodeList nodeList = doc.getDocumentElement().getElementsByTagName(nodeName);
		if(nodeList.getLength()==0){
			return null;
		}
		nodeList = nodeList.item(0).getChildNodes();
		int len = nodeList.getLength();
		org.w3c.dom.Element[] E = new org.w3c.dom.Element[len];
		for(int i=0;i<len;i++){
			E[i] = (org.w3c.dom.Element) nodeList.item(i);
		}
		return E;
	}
	
	public static HashMap<String, Object> nodeToMap(org.w3c.dom.Document doc, String nodeName) {
		
		if(doc==null){
			return null;
		}
		
		NodeList nodeList = doc.getDocumentElement().getElementsByTagName(nodeName);
		
		if(nodeList.getLength()==0){
			return null;
		}
		
		return nodeToMap(nodeList, nodeName);
	}
	
	public static HashMap<String, Object> nodeToMap(NodeList nodeList, String nodeName) {
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		HashMap<String, Object> childMap = new HashMap<String, Object>();
		List<HashMap<String, Object>> childList = new ArrayList<HashMap<String, Object>>();

		for (int i = 0; i < nodeList.getLength(); i++) {

			Node currentNode = nodeList.item(i);
			
			childMap = new HashMap<String, Object>();
			
			if (currentNode.hasAttributes()) {
				for (int j = 0; j < currentNode.getAttributes().getLength(); j++) {
					
					Node item = currentNode.getAttributes().item(j);
					childMap.put(item.getNodeName(), item.getNodeValue());
				}
				childList.add(childMap);
			}
			if (currentNode.getFirstChild() != null && currentNode.getFirstChild().getNodeType() == Node.ELEMENT_NODE) {
				map.putAll(nodeToMap(currentNode.getChildNodes(),currentNode.getFirstChild().getLocalName()));
			} else if (currentNode.getFirstChild() != null && currentNode.getFirstChild().getNodeType() == Node.TEXT_NODE) {
				map.put(currentNode.getLocalName(), currentNode.getNodeValue());
			}
		}
		map.put(nodeName,childList);
		return map;
	}

	// treegrid attribute
	public static boolean isDeleted(org.w3c.dom.Element I){
		return I.getAttribute("Deleted").equals("1");
	}
	public static boolean isAdded(org.w3c.dom.Element I){
		return I.getAttribute("Added").equals("1");
	}
	public static boolean isChanged(org.w3c.dom.Element I){
		return I.getAttribute("Changed").equals("1");
	}
	// Moved only to another parent !
	public static boolean isMoved(org.w3c.dom.Element I){
		return I.getAttribute("Moved").equals("2");
	}
	
	
	
}
