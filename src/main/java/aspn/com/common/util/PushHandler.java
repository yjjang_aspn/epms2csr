package aspn.com.common.util;

import aspn.hello.com.model.DeviceVO;
import com.google.android.gcm.server.Message;
import javapns.Push;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class PushHandler {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	private PushNotificationPayload apnsPayload = null; //아이폰
	private Message.Builder fcmMessageBuilder = null; //안드로이드
	private List<String> fcmSendId = null;
	private List<String> apnsSendId = null;
	private String fcmApiKey = "";
	private String apnsApiKey = "";
	private String apnsApiPwd = "";
	private boolean production = true;

	public void setPushInfo(DeviceVO deviceMd, String production_mode) throws Exception{

		fcmSendId = new ArrayList<String>();
		apnsSendId = new ArrayList<String>();

		try{
			fcmApiKey = Config.getString("fcm.apikey");

			apnsApiKey = Config.getString("apns.path") + Config.getString("apns.cert");	//server_prod.xml과 server_dev.xml에 개발/운영 dist 파일명을 정확히 명시해야 함
			if(production_mode.equals("dev")){
				// 개발용 dist 파일은 production을 false로 해야 push 가능
				production = false;
				// 배포할때는 개발/운영 상관없이 무조건 true로 해야 push 가능
//				production = true;
			}else{
				production = true;
			}

			logger.debug("production_mode >> " + production_mode);
			logger.debug("apnsApiKey >> " + apnsApiKey);
			logger.debug("production >> " + production);

			apnsApiPwd = Config.getString("apns.pwd");

			// iOS-APNS 사용시 아래주석 해제 필요
//			for(int i=0; i<deviceMd.getTrgtMemberArray().length; i++){
//				if("A".equals(deviceMd.getTrgtPlatFmArray()[i].toString())){
//					fcmSend(fcmApiKey, deviceMd.getTrgtPushIdArray()[i].toString(),
//							deviceMd.getPushMenuType(), URLEncoder.encode(deviceMd.getPushTitle(), "UTF-8"), deviceMd.getPushMenuDetailNdx());
//				}else{
//					apnsSendId.add(deviceMd.getTrgtPushIdArray()[i].toString());
//				}
//			}
			
			// iOS-FCM 사용시 아래주석 처리 필요
			for(int i=0; i<deviceMd.getTrgtMemberArray().length; i++){
				fcmSend(deviceMd.getTrgtPlatFmArray()[i].toString(), fcmApiKey, deviceMd.getTrgtPushIdArray()[i].toString(),
						deviceMd.getPushMenuType(), deviceMd.getPushTitle(), deviceMd.getPushMenuDetailNdx(), deviceMd.getPushAlarm());
			}

			// 안드로인경우 푸쉬중복을 막기위해 처리
			if(fcmSendId.size() > 0 && "prod".equals(production_mode)){

				// FCM Push Message Setting
				fcmMessageBuilder = new Message.Builder();
				fcmMessageBuilder.delayWhileIdle(false);

				fcmMessageBuilder.addData("pushTitle", URLEncoder.encode(deviceMd.getPushTitle(), "UTF-8"));
				fcmMessageBuilder.addData("pushMenuType", deviceMd.getPushMenuType());
				fcmMessageBuilder.addData("pushMenuDetailNdx", deviceMd.getPushMenuDetailNdx());
				fcmMessageBuilder.addData("pushAlarm", deviceMd.getPushAlarm());
			}

			// iOS-APNS 사용시 아래주석 해제 필요
//			if(apnsSendId.size() > 0){
//				setApns(apnsSendId, deviceMd);
//			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}


	public void setApns(List<String> apnsSendId, DeviceVO deviceVO) throws Exception{

		// APNS Push Message Setting
		apnsPayload = PushNotificationPayload.complex();
		apnsPayload.addAlert(deviceVO.getPushTitle());
		apnsPayload.addBadge(-1);
		apnsPayload.addSound("default");
		
		apnsPayload.addCustomDictionary("pushTitle", ""); // 아이폰은 push title을 alert에 추가하여 보낸다.
		apnsPayload.addCustomDictionary("pushMenuType", deviceVO.getPushMenuType());
		apnsPayload.addCustomDictionary("pushMenuDetailNdx", deviceVO.getPushMenuDetailNdx());

		apnsSend(apnsApiKey, apnsApiPwd, true);
	}

	// 안드로이드 푸쉬 전송
	public void fcmSend(String targetPlat, String server_key, String tokenId, String pushMenuType, String pushTitle, String pushMenuDetailNdx, String pushAlarm) throws Exception{
		try {

			// Create URL instance.

			URL url = new URL(Config.getString("fcm.url"));

			// create connection.

			HttpURLConnection conn;

			conn = (HttpURLConnection) url.openConnection();
			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);

			// set method as POST or GET
			conn.setRequestMethod("POST");

			// pass FCM server key
			conn.setRequestProperty("Authorization", "key=" + server_key);

			// Specify Message Format
			conn.setRequestProperty("Content-Type", "application/json");

			// Create JSON Object & pass value

			JSONObject infoJson = new JSONObject();

			// 안드로이드/iOS 공통 파라미터
			infoJson.put("pushTitle", URLEncoder.encode(pushTitle, "UTF-8"));
			infoJson.put("pushMenuType", pushMenuType);
			infoJson.put("pushMenuDetailNdx", pushMenuDetailNdx);
			infoJson.put("pushAlarm", pushAlarm);

			JSONObject json = new JSONObject();
			
			json.put("to", tokenId.trim());
			json.put("data", infoJson);
			
			// iOS - FCM 사용시
			if("I".equals(targetPlat)){
				JSONObject notiJson = new JSONObject();
				
				// 필수파라미터(순서로 파싱하므로 키값 변경가능) > notification
				notiJson.put("body", pushTitle);
				notiJson.put("badge", "-1");	// 사용안하므로 0 or -1로 세팅
				
				// 긴급요청일 경우
				if("02".equals(pushAlarm)){
					notiJson.put("sound",  "siren.caf");
					notiJson.put("mutable_content", true);
				}
				
				json.put("notification", notiJson);
			}
			
//			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");

			wr.write(json.toString());

			wr.flush();

			int status = 0;

			if (null != conn) {
				status = conn.getResponseCode();
			}

			if (status != 0) {

				if (status == 200) {

					// SUCCESS message

					BufferedReader reader = new BufferedReader(
							new InputStreamReader(conn.getInputStream()));

					logger.debug("Android Notification Response : " + reader.readLine());

				} else if (status == 401) {

					// client side error

					logger.debug("Notification Response : TokenId : " + tokenId + " Error occurred :");

				} else if (status == 501) {

					// server side error

					logger.debug("Notification Response : [ errorCode=ServerError ] TokenId : " + tokenId);

				} else if (status == 503) {

					// server side error

					logger.debug("Notification Response : FCM Service is Unavailable  TokenId : " + tokenId);

				} else {
					logger.debug("ERROR :: " + status);
				}

			}

		} catch (MalformedURLException mlfexception) {

			// Prototcal Error

//			logger.debug("Error occurred while sending push Notification!.." + mlfexception.getMessage());

		} catch (IOException mlfexception) {

			// URL problem

//			logger.debug("Reading URL, Error occurred while sending push Notification!.." + mlfexception.getMessage());

		} catch (JSONException jsonexception) {

			// Message format error

//			logger.debug("Message Format, Error occurred while sending push Notification!.." + jsonexception.getMessage());

		} catch (Exception exception) {

			// General Error or exception.

//			logger.debug("Error occurred while sending push Notification!.." + exception.getMessage());
		}
	}

	// 아이폰 푸쉬 전송
	public List<String> apnsSend(String certificate, String password, boolean production) throws Exception{
		List<String> pushResult = new ArrayList<String>();

		int threads = apnsSendId.size();

		if(threads > 30)
			threads = 30;


		// certificate
		List<PushedNotification> notifications = Push.payload(apnsPayload, certificate, password,
				this.production, threads, apnsSendId);

		logger.debug("certificate ::>> " + certificate + " password ::>>>" + password + " notifications ::>>>" + notifications  + ", production ::>> " + this.production);


		if(notifications != null && notifications.size() > 0){

			int i=0;
			for(PushedNotification tmpResult: notifications){

				if(tmpResult.isSuccessful()){
//					logger.debug("tmpResult.isSuccessful() "  + tmpResult.isSuccessful());
				}else{
					pushResult.add(apnsSendId.get(i).toString());
//					logger.debug("error ::>> " + apnsSendId.get(i).toString());
				}
				i++;
			}
		}

		return pushResult;
	}
}
