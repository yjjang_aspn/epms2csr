package aspn.com.common.util;

import aspn.com.common.config.CodeConstants;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonUtil {

	static String resultCd = "resultCd";
	static String resultMsg = "resultMsg";
	static String resultCdTrue = "T";
	static String resultCdFalse = "F";
	static String MOBI_RES_CD = "RES_CD";
	static String MOBI_RES_MSG = "RES_MSG";
	static String MOBI_RES_ERR_DESC = "RES_ERR_DESC";

	/**
	 * JSON 에러 메세지 세팅
	 * @param resultMap
	 * @param msg
	 */
	public static void errorParam(Map<String, Object> resultMap, String msg) {
		resultMap.put(resultCd, resultCdFalse);
		resultMap.put(resultMsg, msg);
	}

	/**
	 * JSON 성공 메세지 세팅
	 * @param resultMap
	 * @param msg
	 */
	public static void successParam(Map<String, Object> resultMap, String msg) {
		resultMap.put(resultCd, resultCdTrue);
		resultMap.put(resultMsg, msg);
	}

	/**
	 * Mobile JSON Return Code Setting
	 * @author 강승상
	 * @since 2017.09.13
	 * @param resultMap
	 * @param resCode [0] response code, [1] system error message
	 * @see CodeConstants
	 */
	public static void setMobileNormalMessage(Map<String, Object> resultMap, String... resCode) {
		resultMap.put(MOBI_RES_CD, resCode[0]);
		resultMap.put(MOBI_RES_MSG, CodeConstants.ERROR_MSG.get(resCode[0])[1]);
		if (resCode.length == 2) {
			resultMap.put(MOBI_RES_ERR_DESC, resCode[1]);
		}
	}

	/**
	 * 모바일 - 예외메시지 처리
	 * @author 강승상
	 * @since 2017.09.19
	 * @param eMessage Exception.getMessage()
	 * @return json 객체
	 */
	public static Map<String, Object> getMobileExceptionMessage(String eMessage) {
		Map<String, Object> errorMap = new HashMap<>();

		errorMap.put(MOBI_RES_CD, CodeConstants.SYSTEM_ERROR);
		errorMap.put(MOBI_RES_MSG, CodeConstants.ERROR_MSG.get(CodeConstants.SYSTEM_ERROR)[1]);
		errorMap.put(MOBI_RES_ERR_DESC, eMessage);

		return errorMap;
	}

	/**
	 * 모바일 - 파라메터 null 체크
	 * @param resultMap
	 * @param srcObj 파라메터 객체
	 * @param checkParams null 체크 할 필드 명
	 * @return isNull
	 * @throws Exception
	 */
	public static boolean isMobileNullParam(Map<String, Object> resultMap, Object srcObj, String... checkParams) throws Exception {
		List<String> nullParam = ObjUtil.nullCheck(srcObj, checkParams);
		if (nullParam != null && !nullParam.isEmpty()) {
			setMobileNormalMessage(resultMap, CodeConstants.PARAM_NULL);
			resultMap.put(MOBI_RES_MSG, resultMap.get(MOBI_RES_MSG) + " / " + nullParam);
			return true;
		} else {
			return false;
		}
	}
}
