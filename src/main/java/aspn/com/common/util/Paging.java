package aspn.com.common.util;

import org.apache.commons.lang3.StringUtils;

public class Paging {
	private int nowPage ;	
	private int rowTotal ;  
	private int blockList ; 
	private int blockPage ; 
	private int totalPage ; 
	private int startPage ; 
	private int endPage ; 	
	private int startRow ; 	
	private int endRow ; 	
	private String pagingClass;
	
	private boolean isPrevPage ; 
	private boolean isNextPage ; 
	
	private StringBuffer pagingHtml ; 
	
	public Paging(int nowPage, int rowTotal){
		paging(nowPage, rowTotal, 10, 10);
	}
	
	public Paging(int nowPage, int rowTotal, int blockList, int blockPage){
		paging(nowPage, rowTotal, blockList, blockPage);
	}
	
	public void paging(int nowPage, int rowTotal, int blockList, int blockPage){
		this.nowPage = nowPage;
		this.rowTotal = rowTotal;
		this.blockList = blockList;
		this.blockPage = blockPage;
		
		/* 플래그 초기화 */
		isNextPage = false ;
		isPrevPage = false ;
		
		/* 전체페이지 = 전체게시글 / 한 페이지에 보여주는 게시물 갯수*/
		totalPage = (int)Math.ceil((double)rowTotal / blockList) ;
		/* 현재페이지가 전체페이지를 넘을 경우 강제적으로 현재 페이지값을 전체페이지값으로 변경 */
		if(nowPage > totalPage && totalPage != 0){
			nowPage = totalPage ;
		}
		
		/* 첫번째 게시물과 마지막 게시물을 구별 */
		startRow = (nowPage -1) * blockList ;
		endRow = startRow + blockList - 1 ;
		
		/* 시작페이지와 마지막 페이지 */
		startPage = ((nowPage - 1) / blockPage) * blockPage + 1 ;
		endPage = startPage + blockPage - 1 ;
		
		/* 마지막페이지가 전체페이지를 넘을 경우 강제적으로 마지막 페이지값을 전체페이지값으로 변경 */
		if(endPage > totalPage && totalPage != 0){
			endPage = totalPage ;
		}
		
		/* 마지막페이지가 전체페이지보다 작을 경우 다음페이징이 적용될 수 있도록 boolean 형 변수의 값 설정 */
		if(nowPage < totalPage){
			isNextPage = true ;
		}
		
		/* 시작페이지의 값이 1보다 클 경우, 이전 페이징이 적용될 수 있도록 boolean 형 변수의 값을 설정 */
		if(nowPage > 1){
			isPrevPage = true ;
		}
		pagingHtml = new StringBuffer() ;
		if(StringUtils.isBlank(this.pagingClass)) {
			pagingHtml.append("<div class=\"pagingWrap\">");
		} else {
			pagingHtml.append("<div class=\"" + this.pagingClass + "\">");
		}
		
		/* 이전 페이지 기능을 하는 이미지를 표출한다. */
		pagingHtml.append("<ul class=\"pagingLst\">");

		/* 페이지가 없을 경우 1 표시  */
		if(rowTotal <= blockList){
			pagingHtml.append("<li class=\"numPaging\"><span class=\"pagingNow\">1</span></li>");
		}else{
			pagingHtml.append("<li><a href=\"javascript:\" class=\"pageFirst\" page=\"1\"><img src=\"/images/eaccounting/page_first.gif\" alt=\"처음으로가기\"/></a></li>");
			if(isPrevPage){
				pagingHtml.append("<li><a href=\"javascript:\" page=\"" + (nowPage - 1) + "\"><img src=\"/images/eaccounting/page_pre.gif\" alt=\"이전페이지로가기\"/></a></li>");
			}else{
				pagingHtml.append("<li><a href=\"javascript:\" ><img src=\"/images/eaccounting/page_pre.gif\" alt=\"이전페이지로가기\" /></a></li>");
			}
			
			/* 페이지 번호찍기 */
			for(int i=startPage ; i<=endPage ; i++){
				/* 전체페이지를 넘어갈때 */
				if(i > totalPage){
					break ;
				}
				
				/* 현재페이지일때 */
				if(i == nowPage){
					pagingHtml.append("<li class=\"numPaging\"><span class=\"pagingNow\">" + i + "</span></li>");
				}else{
					pagingHtml.append("<li class=\"numPaging\"><a href=\"javascript:\" class=\"pageNum\" page=\"" + i + "\">" + i + "</a></li>");
				}
			}
			
			/* 다음 페이지 기능을 하는 이미지를 표출한다. */
			if(isNextPage){
				pagingHtml.append("<li><a href=\"javascript:\" page=\"" + (nowPage + 1) + "\"><img src=\"/images/eaccounting/page_next.gif\" alt=\"다음페이지로가기\"/></a></li>");
			}else{
				pagingHtml.append("<li><a href=\"javascript:\"><img src=\"/images/eaccounting/page_next.gif\" alt=\"다음페이지로가기\"/></a></li>");
			}
			pagingHtml.append("<li><a href=\"javascript:\" page=\"" + totalPage + "\"><img src=\"/images/eaccounting/page_end.gif\" alt=\"마지막으로가기\"/></a></li>");
		}
		
		pagingHtml.append("</ul>");
		pagingHtml.append("</div>");
	}
	
	public int getEndRow(int totalCnt){
		int cnt = totalCnt ;
		
		if(getEndRow() < totalCnt){
			cnt = getEndRow() + 1 ;
		}		
		return cnt ;
	}

	public int getNowPage() {
		return nowPage;
	}

	public void setNowPage(int nowPage) {
		this.nowPage = nowPage;
	}

	public int getRowTotal() {
		return rowTotal;
	}

	public void setRowTotal(int rowTotal) {
		this.rowTotal = rowTotal;
	}

	public int getBlockList() {
		return blockList;
	}

	public void setBlockList(int blockList) {
		this.blockList = blockList;
	}

	public int getBlockPage() {
		return blockPage;
	}

	public void setBlockPage(int blockPage) {
		this.blockPage = blockPage;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getStartPage() {
		return startPage;
	}

	public void setStartPage(int startPage) {
		this.startPage = startPage;
	}

	public int getEndPage() {
		return endPage;
	}

	public void setEndPage(int endPage) {
		this.endPage = endPage;
	}

	public int getStartRow() {
		return startRow;
	}

	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	public int getEndRow() {
		return endRow;
	}

	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}

	public boolean isPrevPage() {
		return isPrevPage;
	}

	public void setPrevPage(boolean isPrevPage) {
		this.isPrevPage = isPrevPage;
	}

	public boolean isNextPage() {
		return isNextPage;
	}

	public void setNextPage(boolean isNextPage) {
		this.isNextPage = isNextPage;
	}

	public StringBuffer getPagingHtml() {
		return pagingHtml;
	}

	public void setPagingHtml(StringBuffer pagingHtml) {
		this.pagingHtml = pagingHtml;
	}

	public String getPagingClass() {
		return pagingClass;
	}

	public void setPagingClass(String pagingClass) {
		this.pagingClass = pagingClass;
	}
}
