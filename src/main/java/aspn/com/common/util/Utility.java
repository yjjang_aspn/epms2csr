package aspn.com.common.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;

import aspn.hello.com.model.AbstractVO;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * 유틸 클래스
 * @author 이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.07.04  이영탁          최초 생성
*
 * </pre>
 */

public class Utility
{
	private static final String DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ssZ";
	private static final String MiliSEC_FORMAT = "yyyyMMddHHmmssS";
	private static final String SIMPLE_FORMAT = "yyyyMMddHHmmss";
	private static final String YMD_FORMAT = "yyyyMMdd";
	private static final String SLASH_FORMAT = "yyyy/MM/dd";
	private static final String MONTH_FORMAT = "yyyy/MM";

	private static final byte SECRET_KEY[] = { 1, 2, 9, 10, 49, 65, 107, 125, 43, 118, 48, 65, 38,
			57, 77, 67, 74, 111, 50, 87, 78, 80, 105, 108 };
	
	private static SecretKeySpec KEYSPEC = new SecretKeySpec(SECRET_KEY, "DESede");
	
	/**
	 * 문자열 null 체크
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		return str == null || str.length() == 0;
	}
	

	public static String encodeStr(String str) throws Exception {
		String result = null;
		Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
		cipher.init(1, KEYSPEC);
		byte plainText[] = str.getBytes("UTF-8");
		byte cipherText[] = cipher.doFinal(plainText);
		result = (new BASE64Encoder()).encode(cipherText);
		return result;
	}

	public static String decodeStr(String str) throws Exception {
		String result = null;
		Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
		cipher.init(2, KEYSPEC);
		byte base64bytes[] = (new BASE64Decoder()).decodeBuffer(str);
		byte decryptedText[] = cipher.doFinal(base64bytes);
		result = new String(decryptedText, "UTF-8");
		result = result.trim();
		return result;
	}	
	
	public static HashMap<String,String> getFunction(String trgtId, HttpSession session) {
		String[]               arrayFnct   = null;
		HashMap<String,String> returnValue = new HashMap<String,String>();

		List<HashMap<String,Object>> menuFnctList = (List<HashMap<String,Object>>)session.getAttribute("menuFnctList");

		String workCd = "";
		String menuCd = "";

		for(HashMap<String,Object> tmpFnct : menuFnctList){
			if(tmpFnct.get("trgtId") == null || tmpFnct.get("trgtId").equals("")) continue;

			if(session.getAttribute("popupYn") != null && session.getAttribute("popupYn").equals("N")){
				workCd = session.getAttribute("workCd").toString();
				menuCd = session.getAttribute("menuCd").toString();
			}else if(session.getAttribute("popupYn") != null && session.getAttribute("popupYn").equals("Y")){
				workCd = session.getAttribute("popupWorkCd").toString();
				menuCd = session.getAttribute("popupMenuCd").toString();
			}

			if(tmpFnct.get("workCd")!=null && workCd.equals(tmpFnct.get("workCd"))
					&& menuCd.equals(tmpFnct.get("menuCd"))
					&& trgtId.equals(tmpFnct.get("trgtId"))
			){
				arrayFnct = tmpFnct.get("menuFnctList").toString().split(",");

				for(int i=0; i<arrayFnct.length; i++){
					returnValue.put(arrayFnct[i], arrayFnct[i]);
				}
			}
		}

		return returnValue;
	}
	
	public static <T extends HashMap<String, Object>, C extends AbstractVO> List<C> convertMapToBean(List<T> list, Class<C> clazz) {
		
		List<C> beanList = new ArrayList<C>();
		for (HashMap<String, Object> source : list) {
			C bean = toBean(source, clazz);
			beanList.add(bean);
		}
		return beanList;
	}
	
	public static <C> C toBean(HashMap<String, Object> source, Class<C> targetClass) {
		C bean = null;
		try {
			bean = targetClass.newInstance();
			PropertyDescriptor[] targetPds = BeanUtils.getPropertyDescriptors(targetClass);
			for (PropertyDescriptor desc : targetPds) {
				Object value = source.get(desc.getName());
				if (value != null) {
					Method writeMethod = desc.getWriteMethod();
					if (writeMethod != null) {
						writeMethod.invoke(bean, new Object[] { value });
					}
				}
			}
			
		} catch (InstantiationException e) {
			new IllegalArgumentException("Cannot initiate class", e);
		} catch (IllegalAccessException e) {
			new IllegalStateException("Cannot access the property", e);
		} catch (InvocationTargetException e) {
			new IllegalArgumentException(e);
		}
		return bean;
	}
	
	@SuppressWarnings("unchecked")
	public static List<HashMap<String, Object>> getEditDataList(String uploadData) throws Exception{
		String xml= XmlUtil.parseXML(uploadData);
		org.w3c.dom.Document doc = XmlUtil.convertNodesFromXml(xml);
		HashMap<String, Object> dataMap = XmlUtil.nodeToMap(doc,"Changes");
		List<HashMap<String, Object>> dataList = (List<HashMap<String, Object>>)dataMap.get("I");
		return dataList;
	}
	
	
	public static String md5Convert(String newPwd) {
		String hash = "";
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] bytesOfMessage = trim(newPwd).getBytes("UTF-8");

			md.update(bytesOfMessage, 0, trim(newPwd).length());
			hash = new BigInteger(1, md.digest()).toString(16);
			hash = lpad(hash, 32, '0');

		} catch (Exception e) {
			e.printStackTrace();
		}

		return hash;
	}
	
	public static String lpad(String source, int length, char padder) {
		byte[] originString = source.getBytes();
		int diffLength = length - originString.length;
		String returnValue = "";
		String totalPadder = "";

		for (int i = 0; i < diffLength; i++) {
			totalPadder = padder + totalPadder;
		}

		returnValue = totalPadder + source;
		return returnValue;
	}
	
	public static String trim(Object obj) {
		String sValue = (String) obj;
		sValue = nullToStr(sValue);
		return sValue.trim();
	}

	public static String nullToStr(Object object) {
		if (object == null)
			return "";
		else
			return object.toString().trim();
	}
	
	
	static String resultCd = "resultCd";
	static String resultMsg = "resultMsg";
	static String resultCdTrue = "T";
	static String resultCdFalse = "F";
	
	/**
	 * JSON 에러 메세지 세팅
	 * @param resultMap
	 * @param msg
	 */
	public static void errorParam(HashMap<String, Object> resultMap, String msg) {
		resultMap.put(resultCd, resultCdFalse);
		resultMap.put(resultMsg, msg);
	}

	/**
	 * JSON 성공 메세지 세팅
	 * @param resultMap
	 * @param msg
	 */
	public static void successParam(HashMap<String, Object> resultMap, String msg) {
		resultMap.put(resultCd, resultCdTrue);
		resultMap.put(resultMsg, msg);
	}
	
	/**
	 * Conversion Method
	 * List<DefineClass Object> to List<Map Object>
	 */
	public static List<HashMap<String, Object>> getVoToMapList(List list) {
		List<HashMap<String, Object>> resultList = new ArrayList<HashMap<String,Object>>();
		
		for(Object obj: list) {
			
			// Object의 변수
			java.lang.reflect.Field[] fields = obj.getClass().getDeclaredFields();
			
			HashMap<String, Object> map = new HashMap<String, Object>();
			
			for(int i=0 ; i < fields.length ; i++ ) {
				
				// private 변수에 접근 허용
				fields[i].setAccessible(true);
				try {
					
					// 변수 명을 key로 value 저장.
					map.put(fields[i].getName(), fields[i].get(obj));
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			resultList.add(map);
		}
		
		return resultList;
	}
}
