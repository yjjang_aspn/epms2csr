package aspn.com.common.util;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.ResourceUtils;

/**
 * property를 조회하기 위한 클래스
 * @author 이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 * 수정일		수정자			수정내용
 * ------		------			--------
 * 2017.07.04	이영탁			최초 생성
 * 2017.08.17	강승상			Config Load방식 변경
 * </pre>
 */

public class Config {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());

    private static Properties properties = new Properties();

    public Config() {
        try {
        	// 프로퍼티 객체 생성
            Properties _config = new Properties();
            // 프로퍼티 파일 로딩
            _config.load(new java.io.BufferedInputStream(new FileInputStream(ResourceUtils.getFile("classpath:/config/config.properties"))));
            properties.loadFromXML(new FileInputStream(ResourceUtils.getFile("classpath:/config/properties/server_"+_config.getProperty("production_mode")+".xml")));
        } catch (IOException e) {
            e.printStackTrace();logger.error(e.getMessage());
        }
    }

    public static void loadConfiguration(String path) {
        try {
            File file = new File(path);
            InputStream input = new FileInputStream(file);
            properties.loadFromXML(input);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getString(String key) {
        String propertiesValue = properties.getProperty(key);
        if (propertiesValue == null) {
        	//throw new ConfigValueNotFoundException(key + " 에 해당하는 환경 변수값을 찾을 수 없음");
        	return "";
        } else {
            return propertiesValue;
        }
    }

    public static String getString(String key, String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }

    public static int getInt(String key) throws Exception {
        String value = properties.getProperty(key);
        if (value == null) {
        	throw new Exception(key);
        } else {
        	try {
        		return Integer.parseInt(properties.getProperty(key));
        	} catch(NumberFormatException e) {
        		throw new Exception(key);
        	}
        }
    }

    public static int getInt(String key, int defaultValue) {
        try {
            return Integer.parseInt(properties.getProperty(key));
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static boolean getBoolean(String key) {
        return stringToBoolean(properties.getProperty(key));
    }

    private static boolean stringToBoolean(String value) {
        if (value == null) {
            return false;
        }
        if (value.equals("true")) {
            return true;
        }
        if (value.equals("on")) {
            return true;
        }
        if (value.equals("yes")) {
            return true;
        }
        if (value.equals("1")) {
            return true;
        }
        if (value.equals("Y")) {
            return true;
        }
        return false;
    }

    public static long getLong(String key, long defaultValue) {
        try {
            return Long.parseLong(properties.getProperty(key));
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static Properties properties() {
        return (Properties) properties.clone();
    }

    /**
     * newline 으로 분리되는 설정값 리스트를 반환한다.
     *
     * @param key
     * @return \n으로 분리되는 설정값 리스트
     */
    public static List<String> getStrings(String key) {
        String propertyValue = getString(key);
        BufferedReader reader = null;
        StringReader stringReader = new StringReader(propertyValue);

        List<String> propertyValues = new ArrayList<String>();
        try {
            reader = new BufferedReader(stringReader);
            for (;;) {
                String line = readline(reader);
                if (line == null) {
                    break;
                } else if (Utility.isEmpty(line.trim())) {
                    continue;
                } else {
                    propertyValues.add(line.trim());
                }
            }
        } finally {
            if (reader != null) {
                close(reader);
            }
        }

        return propertyValues;
    }

    /**
     * Exception 안나
     * @param reader
     * @return
     */
    private static String readline(BufferedReader reader) {
        try {
            return reader.readLine();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public static void close(Object o) {
        if (o == null) {
            return;
        }

        if (o instanceof Closeable) {
            Closeable c = (Closeable) o;
            try {
                c.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void close(Object[] os) {
        for (Object o: os) {
            close(o);
        }
    }
}
