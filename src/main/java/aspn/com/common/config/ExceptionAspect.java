package aspn.com.common.config;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;


@Aspect
public class ExceptionAspect {

	private Logger logger = Logger.getLogger(ExceptionAspect.class);
	
	/**
	 * sqlSessionDao,commonDao logging Exception
	 * @param joinPoint
	 * @param e
	 * @throws Exception
	 * @throws Throwable
	 */
	@AfterThrowing(pointcut = "execution(* aspn..*.*(..))", throwing = "e")
	public void aspnException(JoinPoint joinPoint, Exception e) throws  Throwable {
		Signature signature = joinPoint.getSignature();
		String methodName = signature.getName();
		String stuff = signature.toString();
		String arguments = Arrays.toString(joinPoint.getArgs());
		logger.error("Exception", e);
	}

}
