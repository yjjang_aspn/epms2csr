package aspn.com.common.config;

import java.util.HashMap;

/**
 * 코드 상수 클래스
 * @author 이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2017.07.04		이영탁			최초 생성
 *   2018.05.21		서정민			EPMS 관련 코드 추가
 *   
 * </pre>
 */

public class CodeConstants {


	public static String COMPANY_ID = "ASPN"; // 회사코드

	/** 로그인 구분 */
	public static String USER_GUBUN_USER        = "1" ; // 일반 사용자
	public static String USER_GUBUN_CUSTOMER    = "2" ; // 거래처
	public static String USER_GUBUN_VENDOR      = "3" ; // 공급사

	/** 메뉴 그룹 */
	public static String HEADER_MENU_HELLOCOMPANY   = "HELLO"; //헬로컴퍼니
	public static String HEADER_MENU_ORDER          = "ORDER"; //주문
	public static String HEADER_MENU_PURCHASE       = "PR";    //구매
	public static String HEADER_MENU_EACCOUNTING    = "EACC";  //경비
	public static String HEADER_MENU_ASSET          = "ASSET"; //자산
	public static String HEADER_MENU_EPMS           = "EPMS";  //설비관리
	
	/** 공통 코드 */
	public static String STR_1         = "1" ;
	public static String COMM_YES      = "Y" ;  // Yes
	public static String COMM_NO       = "N" ;  // No
	
	/** Grid Upload Key Code  **/
	public static String GRID_UPLOAD_ADDED     =   "Added"   ;
	public static String GRID_UPLOAD_CHANGED   =   "Changed" ;
	public static String GRID_UPLOAD_DELETED   =   "Deleted" ;
	
	/** FLAG 코드 */
	public static String FLAG_SAVE     = "SAVE";     // 임시저장
	public static String FLAG_COMPLETE = "COMPLETE"; // 완료
	public static String FLAG_INSERT   = "INSERT";   // 추가
	public static String FLAG_UPDATE   = "UPDATE";   // 수정
	public static String FLAG_DELETE   = "DELETE";   // 삭제
	public static String FLAG_APPR     = "APPR";     // 승인
	public static String FLAG_REJECT   = "REJECT";   // 반려
	public static String FLAG_AUTOAPPR = "AUTOAPPR"; // 자동결재
	
	/** 파일 모듈 */
	public static String FILE_MODULE_PROFILE                 = "9";   // 프로필 사진
	public static String FILE_MODULE_EPMS_EQUIPMENT          = "101"; // 설비 사진
	public static String FILE_MODULE_EPMS_QRCODE             = "102"; // 설비 QR코드
	public static String FILE_MODULE_EPMS_EQUIPMENT_MANUAL   = "103"; // 설비 매뉴얼
	public static String FILE_MODULE_EPMS_REPAIR_REQUEST     = "111"; // 정비요청
	public static String FILE_MODULE_EPMS_REPPAIR_RESULT     = "112"; // 정비실적
	public static String FILE_MODULE_EPMS_REPAIR_ANALYSIS    = "113"; // 원인분석
	public static String FILE_MODULE_EPMS_ORDER              = "121"; // 오더 매뉴얼
	public static String FILE_MODULE_EPMS_PREVENTIVE_RESULT  = "122"; // 예방보전 실적
	public static String FILE_MODULE_EPMS_MATERIAL           = "201"; // 자재 사진
	
	/** 
	 * 파일 유형
	 * ATTACH : CD_FILE_TYPE
	 */
	public static String CD_FILE_TYPE_ETC   = "00"; // 기타
	public static String CD_FILE_TYPE_IMAGE = "01"; // 이미지
	public static String CD_FILE_TYPE_VIDEO = "02"; // 동영상
	public static String CD_FILE_TYPE_AUDIO = "03"; // 음성
	
	/** 
	 * 사용자 권한
	 * MEMBER : SUB_AUTH
	 */
	public static String AUTH_COMMON       	= "AUTH01";			// 직책권한 : 일반사용자
	public static String AUTH_CHIEF    		= "AUTH02";			// 직책권한 : 팀장
	public static String AUTH_REPAIR 		= "AUTH03";			// 직책권한 : 정비원
	public static String AUTH_FUNC01		= "FUNC01";			// 기능권한 : 설비 이미지관리(앱)
	public static String AUTH_FUNC02		= "FUNC02";			// 기능권한 : 자재 이미지관리(앱)
	public static String AUTH_FUNC03		= "FUNC03";			// 기능권한 : 정비확정취소
	public static String AUTH_FUNC04		= "FUNC04";			// 기능권한 : 긴급요청(수신)
	public static String AUTH_FAC5100		= "FAC5100";		// 조회권한 : 던킨 안양공장
	public static String AUTH_FAC7100		= "FAC7100";		// 조회권한 : 삼립 시화공장
	public static String AUTH_FAC1100		= "FAC1100";		// 조회권한 : 샤니 성남공장
	public static String AUTH_FAC1200		= "FAC1200";		// 조회권한 : 샤니 대구공장
	
	/** 
	 * 파트
	 * MEMBER : PART
	 */
	public static String PART_01 = "01"; // 기계
	public static String PART_02 = "02"; // 전기
	public static String PART_03 = "03"; // 시설
	
	/** 
	 * 공장파트 구분
	 * COMCD : REMARKS
	 */
	public static String REMARKS_SEPARATE   = "01"; // 파트구분
	public static String REMARKS_UNITED     = "02"; // 파트통합
	
	/** 
	 * 기기이력유형
	 * EPMS_EQUIPMENT_HISTORY : HISTORY_TYPE
	 */
	public static String EPMS_HISTORY_TYPE_REG      = "01";  // 설비등록
	public static String EPMS_HISTORY_TYPE_MOVE     = "02";  // 라인이동
	public static String EPMS_HISTORY_TYPE_110      = "110"; // 돌발정비
	public static String EPMS_HISTORY_TYPE_120      = "120"; // 일반정비
	public static String EPMS_HISTORY_TYPE_130      = "130"; // 개선정비
	public static String EPMS_HISTORY_TYPE_140      = "140"; // 사후정비
	public static String EPMS_HISTORY_TYPE_150      = "150"; // 예방정비
	public static String EPMS_HISTORY_TYPE_CBM      = "21";  // CBM
	public static String EPMS_HISTORY_TYPE_TBM      = "22";  // TBM
	
	/**
	 * 작업유형
	 * EPMS_WORK_LOG : WORK_TYPE
	 */
	public static String EPMS_WORK_TYPE_REPAIR      = "01"; // 정비실적
	public static String EPMS_WORK_TYPE_PREVENTIVE  = "02"; // 예방보전실적
	public static String EPMS_WORK_TYPE_ANALYSIS    = "03"; // 원인분석
	
	/** 
	 * 정비요청 요청유형
	 * EPMS_REPAIR_REQUEST : REQUEST_TYPE
	 */
	public static String EPMS_REQUEST_TYPE_COMMON         = "01"; // 일반
	public static String EPMS_REQUEST_TYPE_PREVENTIVE     = "02"; // 예방정비
	public static String EPMS_REQUEST_TYPE_HELP           = "03"; // 타파트 업무협조
	public static String EPMS_REQUEST_TYPE_PREVENTIVE_SAP = "09"; // SAP_예방보전(일일보전)
	
	/** 
	 * 정비요청 결재상태
	 * EPMS_REPAIR_REQUEST : APPR_STATUS
	 */
	public static String EPMS_APPR_STATUS_WAIT              = "01"; // 대기
	public static String EPMS_APPR_STATUS_APPR              = "02"; // 승인
	public static String EPMS_APPR_STATUS_REJECT            = "03"; // 반려
	public static String EPMS_APPR_STATUS_CANCEL_REPAIR     = "04"; // 정비취소
	public static String EPMS_APPR_STATUS_CANCEL_REQUEST    = "09"; // 요청취소
	
	/** 
	 * 정비실적 정비상태
	 * EPMS_REPAIR_RESULT : REPAIR_STATUS
	 */
	public static String EPMS_REPAIR_STATUS_RECEIVE     = "01"; // 접수
	public static String EPMS_REPAIR_STATUS_PLAN        = "02"; // 배정완료
	public static String EPMS_REPAIR_STATUS_COMPLETE    = "03"; // 정비완료
	public static String EPMS_REPAIR_STATUS_CANCEL      = "04"; // 정비취소
	
	/** 
	 * 정비실적 정비유형
	 * EPMS_REPAIR_RESULT : REPAIR_TYPE
	 */
	public static String EPMS_REPAIR_TYPE_EM    = "110"; // 돌발정비
	public static String EPMS_REPAIR_TYPE_CM	= "120"; // 일반정비
	public static String EPMS_REPAIR_TYPE_PM    = "150"; // 예방정비
	
	/** 
	 * 예방보전 업무구분
	 * EPMS_WORK : PREVENTIVE_TYPE
	 */
	public static String EPMS_PREVENTIVE_TYPE_REPAIR        = "01"; // 예방정비
	public static String EPMS_PREVENTIVE_TYPE_MAINTENANCE   = "02"; // 예방점검
	
	/** 
	 * 예방보전 판정
	 * EPMS_WORK : CHECK_DECISION
	 */
	public static String EPMS_CHECK_DICISION_PASS   = "01"; // O
	public static String EPMS_CHECK_DICISION_HOLD   = "02"; // △
	public static String EPMS_CHECK_DICISION_FAIL   = "03"; // X
	
	/** 
	 * 예방보전 점검구분
	 * EPMS_WORK : CHECK_TYPE
	 */
	public static String EPMS_CHECK_TYPE_CBM    = "01"; // CBM
	public static String EPMS_CHECK_TYPE_TBM    = "02"; // TBM
	
	/** 
	 * 예방보전 실적 상태
	 * EPMS_PREVENTIVE_RESULT : STATUS
	 */
	public static String EPMS_PREVENTIVE_STATUS_WAIT        = "01"; // 대기
	public static String EPMS_PREVENTIVE_STATUS_PROBLEM     = "02"; // 정비필요
	public static String EPMS_PREVENTIVE_STATUS_COMPLETE    = "03"; // 완료
	public static String EPMS_PREVENTIVE_STATUS_UNCHECK     = "04"; // 미점검
	
	/** 
	 * 자재 입출고 유형
	 * EPMS_MATERIAL_INOUT : MATERIAL_INOUT_TYPE
	 */
	public static String EPMS_MATERIAL_INOUT_TYPE_IN        = "01"; // 입고(발주)
	public static String EPMS_MATERIAL_INOUT_TYPE_OUT       = "02"; // 출고
	public static String EPMS_MATERIAL_INOUT_TYPE_IN_OPT    = "03"; // 임의입고
	public static String EPMS_MATERIAL_INOUT_TYPE_OUT_OPT   = "04"; // 임의출고
	
	/** 푸시메세지 유형 */
	public static String EPMS_PUSHMENUTYPE_REQUEST          = "REQUEST";            // 내요청 상세
	public static String EPMS_PUSHMENUTYPE_REQUEST_COMPLETE = "REQUEST_COMPLETE";   // 내요청 상세(완료)
	public static String EPMS_PUSHMENUTYPE_APPR             = "APPR";               // 결재 상세
	public static String EPMS_PUSHMENUTYPE_PLAN             = "PLAN";               // 계획및배정 상세
	public static String EPMS_PUSHMENUTYPE_REPAIR           = "REPAIR";             // 정비 상세
	public static String EPMS_PUSHMENUTYPE_REPAIR_COMPLETE  = "REPAIR_COMPLETE";    // 정비 상세(완료)
	public static String EPMS_PUSHMENUTYPE_REQUEST_CANCEL   = "REQUEST_CANCEL";     // NULL(요청취소시)
	public static String EPMS_PUSHMENUTYPE_NOTICE           = "NOTICE";             // 공지사항 
	public static String EPMS_PUSHMENUTYPE_ALARM            = "ALARM";              // 업무현황 알림 
	
	/** 푸시알람 유형 */
	public static String EPMS_PUSHALARM_GENERAL             = "01"; // 일반
	public static String EPMS_PUSHALARM_EMERGENCY           = "02"; // 긴급
	
	/** 결과 코드 */
	public static String RESULT_COMMON_ERROR       = "commonError";
	public static String RESULT_FILEUPLOAD_ERROR   = "fileUploadError";
	public static String RESULT_FILE_STREAM        = "fileStream";
	public static String RESULT_VALIDATION_ERROR   = "validationError";
	public static String RESULT_AUTH_ERROR         = "authError";
	public static String RESULT_ON_WORK_ERROR      = "onWorkError";

	/** 에러 코드 */
	public static String SUCCESS           = "0000";
	public static String NO_DATA_FOUND     = "1001";
	public static String FAILED_TO_SAVE    = "1002";
	public static String FAILED_TO_DELETE  = "1003";
	public static String ALEADY_DATA       = "1004";
	public static String INVALID_DATA      = "1005";
	public static String INVALID_SQL       = "1006";
	public static String FAILED_TO_MODIFY  = "1007";
	public static String NO_CERTIFIED      = "2000";
	public static String NO_ACCESS_RIGHT   = "2001";
	public static String INVALID_USER      = "2002";
	public static String INVALID_PASSWORD  = "2003";
	public static String INVALID_DEVICE    = "2004";
	public static String NO_REGIST_USER    = "2005";
	public static String DROP_MEMBER       = "2006";
	public static String DIS_SESSION       = "2007";
	public static String NO_AUTH     	   = "2008";
	public static String EXCEED_PASSWORD   = "2009";
	public static String NO_CONTENT 	   = "3001";
	public static String SUCCESS_REG       = "3002";
	public static String SUCCESS_UDT       = "3003";
	public static String SUCCESS_DEL       = "3004";
	public static String NO_MANAGER        = "3005";
	public static String NO_REPAIR         = "3006";
	public static String NO_WORK_LOG       = "3007";
	public static String NO_COMPLETE       = "3008";
	public static String NO_REPAIR_TIME    = "3009";
	public static String NO_WORK_LOG_TIME  = "3010";
	public static String CHECK_STATUS      = "4001";
	public static String PARAM_NULL        = "9995";
	public static String INVALID_METHOD    = "9996";
	public static String INVALID_URL       = "9997";
	public static String ERROR_ETC         = "9998";
	public static String SYSTEM_ERROR      = "9999";
	
	public static HashMap<Object, String[]> ERROR_MSG = new HashMap<Object, String[]>();
	static {
			ERROR_MSG.put(SUCCESS           , new String[] {"SUCCESS"         , "성공"});
			ERROR_MSG.put(NO_DATA_FOUND     , new String[] {"NO DATA FOUND"   , "데이터가 존재하지 않습니다."});
			ERROR_MSG.put(FAILED_TO_SAVE    , new String[] {"FAILED TO SAVE"  , "데이터 저장에 실패하였습니다."});
			ERROR_MSG.put(FAILED_TO_DELETE  , new String[] {"FAILED TO DELETE", "데이터 삭제에 실패하였습니다."});
			ERROR_MSG.put(ALEADY_DATA       , new String[] {"ALEADY DATA"     , "이미 등록된 데이터입니다."});
			ERROR_MSG.put(INVALID_DATA      , new String[] {"INVALID DATA"    , "잘못된데이터입니다."});
			ERROR_MSG.put(INVALID_SQL       , new String[] {"INVALID SQL"     , "잘못된 SQL문입니다."});
			ERROR_MSG.put(FAILED_TO_MODIFY  , new String[] {"FAILED_TO_MODIFY", "데이터 수정에 실패하였습니다."});
			
			ERROR_MSG.put(NO_CERTIFIED      , new String[] {"NO CERTIFIED"    , "검증되지 않은 앱입니다."});
			ERROR_MSG.put(NO_ACCESS_RIGHT   , new String[] {"NO ACCESS RIGHT" , "접근권한이 없습니다."});
			ERROR_MSG.put(INVALID_USER      , new String[] {"INVALID USER"    , "사용자 정보가 일치하지 않습니다."});
			ERROR_MSG.put(INVALID_PASSWORD  , new String[] {"INVALID PASSWORD", "비밀번호가 일치하지 않습니다."});
			ERROR_MSG.put(INVALID_DEVICE    , new String[] {"INVALID DEVICE"  , "분실된 단말기입니다. 관리팀에 연락해 주세요."});
			ERROR_MSG.put(NO_REGIST_USER    , new String[] {"NO REGIST USER"  , "사용자 정보가 등록 되 있지 않습니다. 회원가입을 해주십시오."});
			ERROR_MSG.put(DROP_MEMBER       , new String[] {"DROP_MEMBER"     , "탈퇴한 회원입니다."});
			ERROR_MSG.put(DIS_SESSION       , new String[] {"DIS_SESSION"     , "로그인이 필요합니다."});
			ERROR_MSG.put(NO_AUTH         	, new String[] {"NO_AUTH"    	  , "권한이 없습니다."});
			ERROR_MSG.put(EXCEED_PASSWORD   , new String[] {"EXCEED_PASSWORD" , "비밀번호 오류 누적 5회로 인해 로그인이 제한되었습니다. 관리팀에 연락해 주세요."});
			
			ERROR_MSG.put(NO_CONTENT        , new String[] {"NO_CONTENT"      , "게시물이 없습니다."});
			ERROR_MSG.put(SUCCESS_REG       , new String[] {"SUCCESS_REG"     , "등록 하였습니다."});
			ERROR_MSG.put(SUCCESS_UDT       , new String[] {"SUCCESS_UDT"     , "수정 하였습니다."});
			ERROR_MSG.put(SUCCESS_DEL       , new String[] {"SUCCESS_DEL"     , "삭제 하였습니다."});
			ERROR_MSG.put(NO_MANAGER        , new String[] {"NO_MANAGER"      , "담당자가 아닙니다."});
			ERROR_MSG.put(NO_REPAIR         , new String[] {"NO_REPAIR"       , "등록 가능한 정비실적이 없습니다."});
			ERROR_MSG.put(NO_WORK_LOG       , new String[] {"NO_WORK_LOG"     , "등록 가능한 개인실적이 없습니다."});
			ERROR_MSG.put(NO_COMPLETE       , new String[] {"NO_COMPLETE"     , "개인실적 완료여부 확인해 주시기 바랍니다."});
			ERROR_MSG.put(NO_REPAIR_TIME    , new String[] {"NO_REPAIR_TIME"  , "정비종료시간은 개인작업종료시간보다 이전일 수 없습니다."});
			ERROR_MSG.put(NO_WORK_LOG_TIME  , new String[] {"NO_WORK_LOG_TIME", "개인작업종료시간은 개인작업시작시간보다 이전일 수 없습니다."});
			
			ERROR_MSG.put(CHECK_STATUS      , new String[] {"CHECK_STATUS"    , "상태 값을 확인해 주시기 바랍니다."});
			
			ERROR_MSG.put(PARAM_NULL        , new String[] {"PARAM_NULL"      , "수신 파라미터가 NULL 입니다."});
			ERROR_MSG.put(INVALID_METHOD    , new String[] {"INVALID METHOD"  , "부적절한 Method 호출"});
			ERROR_MSG.put(INVALID_URL       , new String[] {"INVALID URL"     , "잘못된 주소입니다."});
			ERROR_MSG.put(ERROR_ETC         , new String[] {"ERROR / ETC."    , "기타 오류"});
			ERROR_MSG.put(SYSTEM_ERROR      , new String[] {"SYSTEM_ERROR"    , "시스템 오류 발생."});
	};
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/** 사용자권한 */
	public static String AUTH_GENERAL_USER		="01";	//일반사용자
	public static String AUTH_COMPLIMENT_ADMIN 	="02";	//칭찬관리자
	public static String AUTH_PROJECT_ADMIN 	="03";	//프로젝트관리자
	public static String AUTH_SNS_ADMIN 		="04";	//SNS관리자
	public static String AUTH_FINANCE_ADMIN 	="05";	//재무관리자
	public static String AUTH_FINANCE_MANAGER	="06";	//재무담당자
	public static String AUTH_ESTIMATE_ADMIN 	="07";	//평가관리자
	public static String AUTH_ASSETS_ADMIN 		="08";	//자산관리자
	public static String AUTH_CRM_ADMIN 		="09";	//CRM관리자
	public static String AUTH_EACC_ADMIN 		="10";	//경비관리자(EACC)
	public static String AUTH_EACC_USER 		="11";	//일반사용자(EACC)
	public static String AUTH_EACC_SYSTEM 		="12";	//경비관리자(EACC)
	
	public static String AUTH_CUSTOMER 			="100";	//고객
	public static String AUTH_PROVIDER 			="200";	//공급사
	public static String AUTH_SYSTEM_ADMIN 		="99";	//시스템관리자
	
	/** 파일 모듈 구분 */
	public static String FILE_MODULE_EXPN = "8"; 		// 경비 모듈 코드
//	public static String FILE_MODULE_PROFILE = "9";		// 프로필 사진 모듈 코드
	public static String FILE_MODULE_SIGN = "10";		// 직인 파일 모듈 코드
//	public static String FILE_MODULE_MATERIAL = "20"; 	// 자재관리 모듈 코드
	public static String FILE_MODULE_VENDOR = "40";		// 공급사 파일 모듈 번호
	public static String FILE_MODULE_PR = "41"; 		// 구매 요청서 파일 모듈번호
	public static String FILE_MODULE_EST = "42"; 		// 견적서 파일 모듈번호
	public static String FILE_MODULE_ESTVENDOR = "43"; 	// 공급사 견적서 파일 모듈번호
	public static String FILE_MODULE_CONTRACT = "44"; 	// 계약서 파일 모듈번호
	public static String FILE_MODULE_PO = "45";			// 발주서 파일 모듈 번호
	
	
	/** eacc 전표 유형 */
	public static String CARD 		= "1";	 // 법인카드
	public static String TAX 		= "2";	 // 세금계산서
	public static String ETC 		= "3";	 // 기타전표
	public static String TRAVEL		= "4";	 // 출장비

	/** eacc 계정 구분 */
	public static String ACC_EXPENSE	= "1";	// 경비
	public static String ACC_TRAVEL		= "2";	// 출장비
	
	/** 구매요청서 상태 */
	public static String PR_STAT_SAVE 		= "10";	 // 요청서 저장
	public static String PR_STAT_APPL 		= "20" ; // 결제상신
	public static String PR_STAT_OK   		= "30" ; // 결제승인
	public static String PR_STAT_RETURN 	= "40" ; // 결제반려
	public static String PR_STAT_CR 		= "50" ; // 견적서 생성
	
	/** 견적서 상태 */
	public static String EST_STAT_SAVE 		= "10";	 // 요청서 저장
	public static String EST_STAT_APPL 		= "20" ; // 견적서 상신
	public static String EST_STAT_OK   		= "30" ; // 견적서 승인
	public static String EST_STAT_RETURN 	= "40" ; // 견적서 반려
	public static String EST_STAT_SEND 		= "50" ; // 견적서 전송 
	public static String EST_STAT_SELECT 	= "60" ; // 견적서 선정
	public static String EST_STAT_CR 		= "70" ; // 계약서 생성
	
	/** 공급사 견적서 상태 */
	public static String EST_VENDOR_STAT_SAVE 		= "10";	 // 공급사 견적서 저장
	public static String EST_VENDOR_STAT_SEND 		= "20" ; // 공급사 견적서 전송
	public static String EST_VENDOR_STAT_READY   	= "30" ; // 공급사 견적서 업체 선정
	public static String EST_VENDOR_STAT_SUC 		= "40" ; // 공급사 업체 선정
	public static String EST_VENDOR_STAT_FAIL 		= "50" ; // 공급사 업체 탈락
	
	/** 계약서 상태 */
	public static String CONTRACT_CR 		= "10";	 // 계약서 생성
	public static String CONTRACT_SIGN_REQ 	= "20" ; // 공급사 전자 서명 요청
	public static String CONTRACT_SIGN_END  = "30" ; // 공급사 전자 서명 완료
	public static String CONTRACT_END	    = "40" ; // 계약 완료
	
	/** 계약 유형 */
	public static String CONTRACT_TYPE_1	= "1";	// 단가계약
	public static String CONTRACT_TYPE_2 	= "2" ; // 공사계약
	public static String CONTRACT_TYPE_3  	= "3" ; // 자재구매계약
	public static String CONTRACT_TYPE_4	= "4" ; // 용역계약
	public static String CONTRACT_TYPE_5	= "5" ; // 임대차 계약
	
	/** 발주서 상태 */
	public static String PO_ING 		= "10";	 // 발주진행
	public static String PO_APPL 		= "20" ; // 결재상신
	public static String PO_OK  		= "30" ; // 결재승인
	public static String PO_RETURN	    = "40" ; // 결재반려
	public static String PO_SEND	    = "50" ; // 발주서발송
	public static String PO_VIEW	    = "60" ; // 공급사조회
	public static String PO_ARRIVAL    	= "70" ; // 납품예정
	public static String PO_END		    = "80" ; // 발주마감
	
	/** 발주 유형 */
	public static String PO_TYPE1 		= "1" ; // 일반발주
	public static String PO_TYPE2 		= "2" ; // 단가발주
	public static String PO_TYPE3 		= "3" ; // 임대차발주

	/** 입고 상태 */
	public static String GR_ING			= "10";	// 입고 진행
	public static String GR_END			= "20";	// 입고 마감
	
	/** 결재문서 결재 상태  */
	public static String GW_SAVE	= "01";	// 결재 저장
	public static String GW_APPR	= "02";	// 결재 상신
	public static String GW_ING		= "03";	// 결재 진행중
	public static String GW_END		= "04";	// 결재 완료
	public static String GW_RETN	= "05";	// 결재 반려
	public static String GW_CANCEL	= "06";	// 결재 승인취소
	
	/** 전표 상태  */
	public static String BILL_READY			= "R";	// 임시전표 생성전
	public static String BILL_TMP			= "V";	// 임시전표
	public static String BILL_TMP_DELETE    = "X";	// 임시전표 삭제
	public static String BILL_REAL			= "S";	// 실전표
	public static String BILL_REVERSE		= "A";	// 역분개
	
	/** 개인 결재 상태(APPR_I_STS)  */
	public static String APPR_WAIT		= "00";  // 결재대기
	public static String APPR_END		= "01";  // 승인
	public static String APPR_RETN		= "02";  // 반려
	public static String APPR_CANCEL	= "03";	 // 승인취소
	
	/** 전표 상태  */
	public static String DOC_PREVENT		= "0";  //미작성
	public static String DOC_NO				= "1";  //미작성
	public static String DOC_EXIST			= "2";  //작성
	public static String DOC_REJECT			= "3";	//반려/승인취소

	/** 카드 분류 */
	public static String CARDKIND_PERSONAL	= "1";	// 개인 카드
	public static String CARDKIND_COMPANY	= "2";	// 법인 카드
	
	
}
