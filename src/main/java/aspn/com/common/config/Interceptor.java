package aspn.com.common.config;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.ThreadContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import aspn.com.common.util.SessionUtil;
import aspn.hello.com.service.ComService;
import aspn.hello.sys.mapper.MenuMapper;

/**
 * 인터셉터 클래스
 * @author 이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.07.04  이영탁          최초 생성
 *   2017.08.02  오명석          layout skip 추가
 *
 * </pre>
 */

public class Interceptor extends HandlerInterceptorAdapter {
	
	@Autowired
	ComService comService;
	
	@Autowired
	MenuMapper menuMapper;
	
	private final Logger logger = LoggerFactory.getLogger(Interceptor.class);
	Locale locales = null;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		HandlerMethod handlerMethod = (HandlerMethod) handler;
		/*
		 * user session check
		 */
		if(request.getSession().getAttribute("ssUserId") == null){
			ThreadContext.put("user", "");
		}else {
			ThreadContext.put("user", SessionUtil.getAttribute("ssUserId").toString());
		}
		
		boolean result= false;
		HttpSession	session = request.getSession();
		String uri = request.getRequestURL().toString();
		request.setAttribute("uri", uri);
		
		/*
		 * layout skip
		 */
		if(uri.indexOf("Layout.do") > -1){
			return true;
		}
		
		/*int idx = uri.indexOf("/", 10);
		String url = uri.substring(idx, uri.length());
		session.setAttribute("ssUrl", url);*/
		
		Enumeration<?> paramNames = request.getParameterNames();
		logger.debug("┌─[ Request ]───────────────────────");
		logger.debug("│ RequestURI : "+uri);
		logger.debug("│ Controller Class : "+handlerMethod.getMethod().toString().substring(handlerMethod.getMethod().toString().indexOf("aspn"),handlerMethod.getMethod().toString().indexOf("(")));
		logger.debug("│ Method : "+request.getMethod().toUpperCase());
		try{
			
			if(paramNames.hasMoreElements()){
				logger.debug("│");
				logger.debug("├─[ Param ]─────────────────────────");
			}
			
			while (paramNames.hasMoreElements()) {
				String paramName = (String) paramNames.nextElement();
				String[] paramValues = request.getParameterValues(paramName);

				for (String string : paramValues) {
					String value = string;
					logger.debug("│ "+paramName+ " : " + value);
					
					//다국어 파라메터가 있으면 세션에 저장함
					if(paramName.equals("lang")){
						session.setAttribute("lang", value);
						if (value.equals("ko")) {
							locales = Locale.KOREAN;
						} else {
							locales = Locale.ENGLISH;
						}							 
						Locale.setDefault(locales);
					}
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();logger.error(e.getMessage());
			logger.error("│ ※ "+getClass().getName()+" : "+e);
		}
		logger.debug("└───────────────────────────────────");
		
		
		//세션없이 허용되는 URL
		if(uri.indexOf("loginForm") > -1 
				|| uri.indexOf("webLogin") > -1 
				|| uri.indexOf("redirectLogin") > -1 
				|| uri.indexOf("file") > -1 || uri.indexOf("File") > -1 
				|| uri.indexOf("popCrmEditForm") > -1 || uri.indexOf("crmReservedInsert") > -1 
				|| uri.indexOf("/mobile/calendar") > -1 || uri.indexOf("/mobile/reserve") > -1
				|| uri.indexOf("mailCheckReceipt") > -1
				|| uri.indexOf("mobileSendPwdInit") > -1
				|| uri.indexOf("/mobile/keepAlive") > -1
				|| uri.indexOf("/mobile/getAppVersion") > -1
				|| uri.indexOf("/mobile/appCert") > -1
				|| uri.indexOf("/mobile/appLogin") > -1){
//				|| uri.contains("/mobile/")){
			result = true;
		}else{
			if((String)session.getAttribute("ssUserId") == null) {
				
				if(uri.contains("/mobile/")){
					request.getSession().invalidate();
					response.sendRedirect(request.getContextPath() + "/mobile/redirectLogin.do");   
					result = false; 
				}
				else{
					request.getSession().invalidate();
					response.sendRedirect(request.getContextPath() + "/redirectLogin.do");    
					result = false;
				}
			}else{	
				
				@SuppressWarnings("unchecked")
				List<HashMap<String, String>> ssList = (ArrayList<HashMap<String,String>>) session.getAttribute("ssHeaderMenuList");
				if( ssList == null) {
					//권한에 따른 메뉴 조회
					String ssAuth = (String)session.getAttribute("ssAuth");	
					List<HashMap<String, String>> headerMenuList = menuMapper.getHeaderMenuList(ssAuth);
					session.setAttribute("ssHeadMenuCnt", headerMenuList.size());
					session.setAttribute("ssHeaderMenuList", headerMenuList);
					session.setAttribute("ssLeftMenuList", menuMapper.getLeftMenuList(ssAuth));
					//메뉴 Function List 조회
					/*logger.info("ssAuth:: "+(String)session.getAttribute("ssAuth"));
					List<HashMap<String,Object>> menuFnctList = menuMapper.getMenuFnctList((String)session.getAttribute("ssAuth"));
					session.setAttribute("menuFnctList" , menuFnctList );*/
				}
				result = true;	
			} 
		}
		return result;
	}
	
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		ThreadContext.clearAll();
		super.afterCompletion(request, response, handler, ex);
	}
	
}
