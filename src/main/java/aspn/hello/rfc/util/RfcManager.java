package aspn.hello.rfc.util;

import java.io.FileInputStream; 
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import aspn.com.common.util.SessionUtil;
import aspn.com.common.util.StringUtil;

import com.sap.conn.jco.JCoContext;
import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.JCoField;
import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoParameterList;
import com.sap.conn.jco.JCoStructure;
import com.sap.conn.jco.JCoTable;
import com.sap.conn.jco.ext.Environment;

@Component
public class RfcManager {
	
	private static Logger logger = LoggerFactory.getLogger(RfcManager.class);	
	
	private static String ABAP_AS_POOLED = "SPP";
	
	private static JCOProvider provider = null;
	
	private static JCoDestination destination = null;

	private static String COMPANY_ID = "BR00";

	public static void loadProvider() {
		
		Properties properties = loadProperties();
		
		if(provider == null){

			provider = new JCOProvider();
			
			try {
				Environment.registerDestinationDataProvider(provider);
			} catch (IllegalStateException e) {
				logger.debug(e.toString());
			}
			
		}
		
		try {
//			provider.changePropertiesForABAP_AS(SessionUtil.getAttribute("ssCompanyId").toString(), properties);
			provider.changePropertiesForABAP_AS(COMPANY_ID, properties);
		} catch (Exception e) { 
			logger.error(e.toString());
		}
		
	}
	
	
	public static Properties loadConfig() {
		Properties config = new Properties();
		try {
			//TOMCAT
			config.load(new java.io.BufferedInputStream(new FileInputStream(ResourceUtils.getFile("classpath:/config/config.properties"))));
			//JUNIT
//			config.load(new java.io.BufferedInputStream(new FileInputStream(ResourceUtils.getFile("src/main/resources/config/config.properties"))));
		} catch (IOException e) { 
			logger.error(e.toString());
		}
		return config;
	}

	public static Properties loadProperties() {
		Properties prop = new Properties();
		try {
			//TOMCAT
//			prop.load(new java.io.BufferedInputStream(new FileInputStream(ResourceUtils.getFile("classpath:/config/properties/sap_"+loadConfig().getProperty("production_mode")+"_"+SessionUtil.getAttribute("ssCompanyId").toString()+".properties"))));
			prop.load(new java.io.BufferedInputStream(new FileInputStream(ResourceUtils.getFile("classpath:/config/properties/sap_"+loadConfig().getProperty("production_mode")+".properties"))));
			//JUNIT
//			prop.load(new java.io.BufferedInputStream(new FileInputStream(ResourceUtils.getFile("src/main/resources/config/"+loadConfig().getProperty("production_mode")+"_sap_conf.properties"))));
		} catch (IOException e) { 
			logger.error(e.toString());
		} catch (Exception e) { 
			logger.error(e.toString());
		}
		
		return prop;
	}

	public static JCoDestination getDestination() throws JCoException {
//		if (destination == null) {
//			destination = JCoDestinationManager.getDestination(ABAP_AS_POOLED);
//		}
		try {
//			destination = JCoDestinationManager.getDestination(SessionUtil.getAttribute("ssCompanyId").toString());
			destination = JCoDestinationManager.getDestination(COMPANY_ID);
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return destination;
	}

	public static JCoFunction getFunction(String functionName) {
		logger.debug("------------------------ JCO I/F Start ------------------------");
		logger.debug("* Find Client Pool.");
		logger.debug("* Create Client Pool.");
		logger.debug("  - Client                    : " + loadProperties().getProperty("jco.client.client"));
		logger.debug("  - User                      : " + loadProperties().getProperty("jco.client.user"));
		logger.debug("  - Language                  : " + loadProperties().getProperty("jco.client.langu"));
		logger.debug("  - Host                      : " + loadProperties().getProperty("jco.client.ashost"));
		logger.debug("  - System Number             : " + loadProperties().getProperty("jco.client.sysnr"));
		JCoFunction function = null;
		try {
			long sTime = 0;
			long eTime = 0;
			long cTime = 0;
			sTime = System.currentTimeMillis();
			try {
				getDestination().ping();
				eTime =System.currentTimeMillis();
			} catch (JCoException ex) {
				logger.debug("SAP I/F CONNECTION Exception "+StringUtil.getExceptionTrace(ex));
			}
			cTime = eTime - sTime;
			logger.debug("SAP I/F CONNECTION RESPONSE [TIME : " + (cTime / 1000) % 60 + "(s)]");
			
			logger.debug("* Function Info.");
            logger.debug("  - Function Name  : " + functionName);
           	function = getDestination().getRepository().getFunctionTemplate(functionName).getFunction();
		} catch (JCoException e) {
			logger.error("JCoException - ",e);
		} catch (NullPointerException ex) {
			logger.error("NullPointerException - ",ex);
		}
		return function;
	}

	public static void execute(JCoFunction function)  {
		JCoParameterList paramList = function.getImportParameterList();
		JCoParameterList table = function.getTableParameterList();
		if(paramList != null){
			logger.debug("* Create Input parameter");
			for (JCoField jCoField : paramList) {
				logger.debug("  - "+jCoField.getName()+"			: " + jCoField.getValue());
			}
		}
		if(table != null){
			table = function.getTableParameterList();
			int inputTableCnt    = table.getFieldCount();
			int inputTableColCnt = 0;
			int inputTableRowCnt = 0;
			String tableName = "";
			for(int i=0; i<inputTableCnt; i++) {
				tableName =table.getString(i);
				JCoTable inputTable = function.getTableParameterList().getTable(tableName);
				inputTableColCnt = inputTable.getNumColumns();
			    inputTableRowCnt = inputTable.getNumRows();
			    if(inputTableColCnt > 0 && inputTableRowCnt > 0){		
			        for(int j=0; j<inputTableRowCnt; j++){
			        	inputTable.setRow(j);
			        	for (JCoField jCoField : inputTable) {
				        	String name = jCoField.getName();
				        	String type = jCoField.getTypeAsString();
				        	String value = jCoField.getString();
				        	
				        	logger.debug(String.format("    Column Index["+j+"] Name[%s] Type[%s] Value - %s", name,type,value));
				        } 
			        }
			    }
			    
			}
		}
		try {
//			JCoContext.begin(destination);
				function.execute(getDestination());
				paramList =function.getExportParameterList();
			
			if(paramList != null){
				logger.debug("* Output Parameter Info.");
				
				if(paramList.getListMetaData().getTypeAsString(0).equals("STRUCTURE")){
					JCoStructure ret = function.getExportParameterList().getStructure(paramList.getListMetaData().getName(0));
					for (JCoField jCoField2 : ret) {
						logger.debug("  -> "+jCoField2.getName()+"			: " + jCoField2.getValue());
					}   
				}else {
					for (JCoField jCoField : paramList) {
						logger.debug("  ->> "+jCoField.getName()+"			: " + jCoField.getValue());
					}
				}
				
			}else{
				logger.debug("!!!!!!!!! Output Parameter Info 없음 !!!!!!!!!");
			}
			
			if(table != null){
				logger.debug("* Output Table Info.");
				table = function.getTableParameterList();
				int outputTableCnt    = table.getFieldCount();
				int outputTableColCnt = 0;
				int outputTableRowCnt = 0;
				String tableName = "";
				for(int i=0; i<outputTableCnt; i++) {
					tableName =table.getString(i);
					JCoTable outputTable = function.getTableParameterList().getTable(tableName);
					outputTableColCnt = outputTable.getNumColumns();
					outputTableRowCnt = outputTable.getNumRows();
					if(outputTableColCnt > 0 && outputTableRowCnt > 0){		
						logger.debug("  - Table[" + tableName + "] Count");
						logger.debug("    Row Count[" + outputTableRowCnt+ "] Column Count[" + outputTableColCnt + "]");
						for(int j=0; j<outputTableRowCnt; j++){
							outputTable.setRow(j);
							for (JCoField jCoField : outputTable) {
								String name = jCoField.getName();
								String type = jCoField.getTypeAsString();
								String value = jCoField.getString();
								logger.debug(String.format("    Column Index["+j+"] Name[%s] Type[%s] Value - %s", name,type,value));
							}  
						}
					}
				}
			}else{
				logger.debug("!!!!!!!!! Output Table Info 없음 !!!!!!!!!");
			}
//			JCoContext.end(destination);
		} catch (JCoException e){
			logger.error("JCoException - "+e.getMessage(), e);
		}finally {
			try {
				JCoContext.end(destination);
			} catch (JCoException ex) {
				ex.getStackTrace();
			}
		}
        logger.debug("------------------------ JCO I/F End ------------------------");
	}

	/*
	 * SAP 연결 Ping 테스트
	 */
	public static String ping() {
		String msg = null;
		try {
			getDestination().ping();
			msg = "Destination " + ABAP_AS_POOLED + " works";
		} catch (JCoException ex) {
			msg = StringUtil.getExceptionTrace(ex);
		}
		logger.debug(msg);
		return msg;
	}

	public static void main(String[] args) {
		RfcManager.ping();
	}
}

