package aspn.hello.rfc.model;

import java.io.Serializable;
import java.util.List;

/**
 * [SAP RFC - 플랜트별오더유형조회] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */

public class ZPM_ORDER_TYPE_IMPORT implements Serializable{
	 
	private List<ZPM_ORDER_TYPE_T_WERKS> zpm_order_type_t_werks; //플랜트

	public List<ZPM_ORDER_TYPE_T_WERKS> getZpm_order_type_t_werks() {
		return zpm_order_type_t_werks;
	}
	public void setZpm_order_type_t_werks(List<ZPM_ORDER_TYPE_T_WERKS> zpm_order_type_t_werks) {
		this.zpm_order_type_t_werks = zpm_order_type_t_werks;
	}
	
	
	
	

}
