package aspn.hello.rfc.model;

import java.io.Serializable;

/**
 * [SAP RFC - 자매마스터 조회 T_ITEM] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_MATERIAL_T_ITEM implements Serializable{

    private String MATNR;		//자재번호
    private String MAKTX;		//자재내역
    private String LVORM;		//삭제여부
    private String MTART;		//자재유형
    private String MTBEZ;		//자재유형내역
    private String MATKL;		//자재그룹
    private String WGBEZ;		//자재그룹내역
    private String GROES;		//자재규격
    private String MEINS;		//기본단위
    private String EKGRP;		//구매그룹
    private String EKNAM;       //구매그룹내역
    private String WERKS;       //플랜트
    private String LGPRO;       //저장위치
    private String LABST;       //현재고 : 20190225 추가 된 부분
    private String LGORT;       //저장위치 : 20190225 추가 된 부분
    
//    private String CODEGRUPPE;	//코드그룹
//    private String CODEGRUTXT;    //코드그룹명
//    private String CODE;       	//코드
//    private String CODETXT;       //코드명
    
    public String getMATNR() {
		return MATNR;
	}
	public void setMATNR(String mATNR) {
		MATNR = mATNR;
	}
	public String getMAKTX() {
		return MAKTX;
	}
	public void setMAKTX(String mAKTX) {
		MAKTX = mAKTX;
	}
	public String getLVORM() {
		return LVORM;
	}
	public void setLVORM(String lVORM) {
		LVORM = lVORM;
	}
	public String getMTART() {
		return MTART;
	}
	public void setMTART(String mTART) {
		MTART = mTART;
	}
	public String getMTBEZ() {
		return MTBEZ;
	}
	public void setMTBEZ(String mTBEZ) {
		MTBEZ = mTBEZ;
	}
	public String getMATKL() {
		return MATKL;
	}
	public void setMATKL(String mATKL) {
		MATKL = mATKL;
	}
	public String getWGBEZ() {
		return WGBEZ;
	}
	public void setWGBEZ(String wGBEZ) {
		WGBEZ = wGBEZ;
	}
	public String getGROES() {
		return GROES;
	}
	public void setGROES(String gROES) {
		GROES = gROES;
	}
	public String getMEINS() {
		return MEINS;
	}
	public void setMEINS(String mEINS) {
		MEINS = mEINS;
	}
	public String getEKGRP() {
		return EKGRP;
	}
	public void setEKGRP(String eKGRP) {
		EKGRP = eKGRP;
	}
	public String getEKNAM() {
		return EKNAM;
	}
	public void setEKNAM(String eKNAM) {
		EKNAM = eKNAM;
	}
	public String getWERKS() {
		return WERKS;
	}
	public void setWERKS(String wERKS) {
		WERKS = wERKS;
	}
	public String getLGPRO() {
		return LGPRO;
	}
	public void setLGPRO(String lGPRO) {
		LGPRO = lGPRO;
	}
	public String getLABST() {
		return LABST;
	}
	public void setLABST(String lABST) {
		LABST = lABST;
	}
	public String getLGORT() {
		return LGORT;
	}
	public void setLGORT(String lGORT) {
		LGORT = lGORT;
	}
    
    
}
