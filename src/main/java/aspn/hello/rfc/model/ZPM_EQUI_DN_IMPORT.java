package aspn.hello.rfc.model;

import java.io.Serializable;

/**
 * [SAP RFC통신] 설비마스터정보
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */

public class ZPM_EQUI_DN_IMPORT implements Serializable{
	
	private String I_YYYYMMDD = ""; // 다운로드 년월일

	public String getI_YYYYMMDD() {
		return I_YYYYMMDD;
	}

	public void setI_YYYYMMDD(String i_YYYYMMDD) {
		I_YYYYMMDD = i_YYYYMMDD;
	}

}
