package aspn.hello.rfc.model;

import java.util.List;

/**
 * [SAP RFC - 작업오더 생성 IMPORT] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_ORDER_CNF_IMPORT {
	
	private String I_TYPECODE;	 //오더유형
	private String I_AUFNR;      //오더번호
	private String I_KTEXT1;     //작업제목
	private String I_ILART;      //활동유형
	private String I_TPLNR;      //기능위치
	private String I_EQUNR;      //설비
	private String I_INGPR;      //보전계획자
	private String I_GSBER;      //사업영역
	private String I_PLANT;      //플랜트
	private String I_ARBPL;      //주요작업장
	private String I_GSTRP;      //기본시작일자
	private String I_GLTRP;      //기본종료일자
	private String I_PLWERKS;    //계획플랜트
	private String I_WERKS;      //정비플랜트
	private String I_METHOD;     //CRUD
	private String I_QMGRP;      //코딩그룹
	private String I_QMCOD;      //코딩
	private String I_OTGRP;      //ITEM그룹
	private String I_OTCOD;      //ITEM(대상)
	private String I_FEGRP;      //DAMAGE그룹
	private String I_FECOD;      //DAMAGE(손상)
	private String I_URGRP;      //CAUSE그룹
	private String I_URCOD;      //CAUSE(원인)
	private String I_MSAUS;      //고장여부
	private String I_AUSVN;      //오작동시작일자
	private String I_AUZTV;      //오작동시작시간
	private String I_AUSBS;      //오작동종료일자
	private String I_AUZTB;      //오작동종료시간
	private String I_EAUSZT;     //고장기간(H)
	private String I_MAUEH;      //고장시간단위
	
	private List<ZPM_ORDER_CNF_T_WORK>  zpm_order_cnf_t_work;
	private List<ZPM_ORDER_CNF_T_LINES> zpm_order_cnf_t_lines;
	private List<ZPM_ORDER_CNF_T_COMP>  zpm_order_cnf_t_comp;
	private List<ZPM_ORDER_CNF_T_TASK>  zpm_order_cnf_t_task;
	
	public String getI_TYPECODE() {
		return I_TYPECODE;
	}
	public void setI_TYPECODE(String i_TYPECODE) {
		I_TYPECODE = i_TYPECODE;
	}
	public String getI_AUFNR() {
		return I_AUFNR;
	}
	public void setI_AUFNR(String i_AUFNR) {
		I_AUFNR = i_AUFNR;
	}
	public String getI_KTEXT1() {
		return I_KTEXT1;
	}
	public void setI_KTEXT1(String i_KTEXT1) {
		I_KTEXT1 = i_KTEXT1;
	}
	public String getI_ILART() {
		return I_ILART;
	}
	public void setI_ILART(String i_ILART) {
		I_ILART = i_ILART;
	}
	public String getI_TPLNR() {
		return I_TPLNR;
	}
	public void setI_TPLNR(String i_TPLNR) {
		I_TPLNR = i_TPLNR;
	}
	public String getI_EQUNR() {
		return I_EQUNR;
	}
	public void setI_EQUNR(String i_EQUNR) {
		I_EQUNR = i_EQUNR;
	}
	public String getI_INGPR() {
		return I_INGPR;
	}
	public void setI_INGPR(String i_INGPR) {
		I_INGPR = i_INGPR;
	}
	public String getI_GSBER() {
		return I_GSBER;
	}
	public void setI_GSBER(String i_GSBER) {
		I_GSBER = i_GSBER;
	}
	public String getI_PLANT() {
		return I_PLANT;
	}
	public void setI_PLANT(String i_PLANT) {
		I_PLANT = i_PLANT;
	}
	public String getI_ARBPL() {
		return I_ARBPL;
	}
	public void setI_ARBPL(String i_ARBPL) {
		I_ARBPL = i_ARBPL;
	}
	public String getI_GSTRP() {
		return I_GSTRP;
	}
	public void setI_GSTRP(String i_GSTRP) {
		I_GSTRP = i_GSTRP;
	}
	public String getI_GLTRP() {
		return I_GLTRP;
	}
	public void setI_GLTRP(String i_GLTRP) {
		I_GLTRP = i_GLTRP;
	}
	public String getI_PLWERKS() {
		return I_PLWERKS;
	}
	public void setI_PLWERKS(String i_PLWERKS) {
		I_PLWERKS = i_PLWERKS;
	}
	public String getI_WERKS() {
		return I_WERKS;
	}
	public void setI_WERKS(String i_WERKS) {
		I_WERKS = i_WERKS;
	}
	public String getI_METHOD() {
		return I_METHOD;
	}
	public void setI_METHOD(String i_METHOD) {
		I_METHOD = i_METHOD;
	}
	public String getI_QMGRP() {
		return I_QMGRP;
	}
	public void setI_QMGRP(String i_QMGRP) {
		I_QMGRP = i_QMGRP;
	}
	public String getI_QMCOD() {
		return I_QMCOD;
	}
	public void setI_QMCOD(String i_QMCOD) {
		I_QMCOD = i_QMCOD;
	}
	public String getI_OTGRP() {
		return I_OTGRP;
	}
	public void setI_OTGRP(String i_OTGRP) {
		I_OTGRP = i_OTGRP;
	}
	public String getI_OTCOD() {
		return I_OTCOD;
	}
	public void setI_OTCOD(String i_OTCOD) {
		I_OTCOD = i_OTCOD;
	}
	public String getI_FEGRP() {
		return I_FEGRP;
	}
	public void setI_FEGRP(String i_FEGRP) {
		I_FEGRP = i_FEGRP;
	}
	public String getI_FECOD() {
		return I_FECOD;
	}
	public void setI_FECOD(String i_FECOD) {
		I_FECOD = i_FECOD;
	}
	public String getI_URGRP() {
		return I_URGRP;
	}
	public void setI_URGRP(String i_URGRP) {
		I_URGRP = i_URGRP;
	}
	public String getI_URCOD() {
		return I_URCOD;
	}
	public void setI_URCOD(String i_URCOD) {
		I_URCOD = i_URCOD;
	}
	public String getI_MSAUS() {
		return I_MSAUS;
	}
	public void setI_MSAUS(String i_MSAUS) {
		I_MSAUS = i_MSAUS;
	}
	public String getI_AUSVN() {
		return I_AUSVN;
	}
	public void setI_AUSVN(String i_AUSVN) {
		I_AUSVN = i_AUSVN;
	}
	public String getI_AUZTV() {
		return I_AUZTV;
	}
	public void setI_AUZTV(String i_AUZTV) {
		I_AUZTV = i_AUZTV;
	}
	public String getI_AUSBS() {
		return I_AUSBS;
	}
	public void setI_AUSBS(String i_AUSBS) {
		I_AUSBS = i_AUSBS;
	}
	public String getI_AUZTB() {
		return I_AUZTB;
	}
	public void setI_AUZTB(String i_AUZTB) {
		I_AUZTB = i_AUZTB;
	}
	public String getI_EAUSZT() {
		return I_EAUSZT;
	}
	public void setI_EAUSZT(String i_EAUSZT) {
		I_EAUSZT = i_EAUSZT;
	}
	public String getI_MAUEH() {
		return I_MAUEH;
	}
	public void setI_MAUEH(String i_MAUEH) {
		I_MAUEH = i_MAUEH;
	}
	public List<ZPM_ORDER_CNF_T_WORK> getZpm_order_cnf_t_work() {
		return zpm_order_cnf_t_work;
	}
	public void setZpm_order_cnf_t_work(List<ZPM_ORDER_CNF_T_WORK> zpm_order_cnf_t_work) {
		this.zpm_order_cnf_t_work = zpm_order_cnf_t_work;
	}
	public List<ZPM_ORDER_CNF_T_LINES> getZpm_order_cnf_t_lines() {
		return zpm_order_cnf_t_lines;
	}
	public void setZpm_order_cnf_t_lines(List<ZPM_ORDER_CNF_T_LINES> zpm_order_cnf_t_lines) {
		this.zpm_order_cnf_t_lines = zpm_order_cnf_t_lines;
	}
	public List<ZPM_ORDER_CNF_T_COMP> getZpm_order_cnf_t_comp() {
		return zpm_order_cnf_t_comp;
	}
	public void setZpm_order_cnf_t_comp(List<ZPM_ORDER_CNF_T_COMP> zpm_order_cnf_t_comp) {
		this.zpm_order_cnf_t_comp = zpm_order_cnf_t_comp;
	}
	public List<ZPM_ORDER_CNF_T_TASK> getZpm_order_cnf_t_task() {
		return zpm_order_cnf_t_task;
	}
	public void setZpm_order_cnf_t_task(List<ZPM_ORDER_CNF_T_TASK> zpm_order_cnf_t_task) {
		this.zpm_order_cnf_t_task = zpm_order_cnf_t_task;
	}
	
}
