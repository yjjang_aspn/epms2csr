package aspn.hello.rfc.model;

import java.util.List;

/**
 * [SAP RFC - 자매마스터 조회 T_ITEM] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_MATERIAL_EXPORT{

	
	private List<ZPM_MATERIAL_T_ITEM> zpm_material_t_item;	//자재마스터 I/F
	
	//결과정보
	private String E_RESULT;
	private String E_MESSAGE;
	
	public List<ZPM_MATERIAL_T_ITEM> getZpm_material_t_item() {
		return zpm_material_t_item;
	}
	public void setZpm_material_t_item(List<ZPM_MATERIAL_T_ITEM> zpm_material_t_item) {
		this.zpm_material_t_item = zpm_material_t_item;
	}
	public String getE_RESULT() {
		return E_RESULT;
	}
	public void setE_RESULT(String e_RESULT) {
		E_RESULT = e_RESULT;
	}
	public String getE_MESSAGE() {
		return E_MESSAGE;
	}
	public void setE_MESSAGE(String e_MESSAGE) {
		E_MESSAGE = e_MESSAGE;
	}
	
	
	
		
}
