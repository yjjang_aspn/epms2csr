package aspn.hello.rfc.model;

import java.util.List;

/**
 * [SAP RFC통신] 설비BOM다운로드
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */

public class ZPM_BOM_DN_EXPORT{

	
	private List<ZPM_BOM_DN_T_ITEM> zpm_bom_dn_t_item;	//설비BOM 수정건 다운로드
	private List<ZPM_BOM_DN_T_DEL> zpm_bom_dn_t_del;		//설비BOM 삭제건 다운로드
	
	//결과정보
	private String E_RESULT;
	private String E_MESSAGE;
	
	public List<ZPM_BOM_DN_T_ITEM> getZpm_bom_dn_t_item() {
		return zpm_bom_dn_t_item;
	}
	public void setZpm_bom_dn_t_item(List<ZPM_BOM_DN_T_ITEM> zpm_bom_dn_t_item) {
		this.zpm_bom_dn_t_item = zpm_bom_dn_t_item;
	}
	public List<ZPM_BOM_DN_T_DEL> getZpm_bom_dn_t_del() {
		return zpm_bom_dn_t_del;
	}
	public void setZpm_bom_dn_t_del(List<ZPM_BOM_DN_T_DEL> zpm_bom_dn_t_del) {
		this.zpm_bom_dn_t_del = zpm_bom_dn_t_del;
	}
	public String getE_RESULT() {
		return E_RESULT;
	}
	public void setE_RESULT(String e_RESULT) {
		E_RESULT = e_RESULT;
	}
	public String getE_MESSAGE() {
		return E_MESSAGE;
	}
	public void setE_MESSAGE(String e_MESSAGE) {
		E_MESSAGE = e_MESSAGE;
	}
	
	
	
	
	
}
