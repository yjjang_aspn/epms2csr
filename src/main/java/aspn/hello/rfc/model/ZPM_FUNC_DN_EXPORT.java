package aspn.hello.rfc.model;

import java.util.List;

/**
 * [SAP RFC - 기능위치 조회 EXPORT] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */

public class ZPM_FUNC_DN_EXPORT{

	
	private List<ZPM_FUNC_DN_T_FUNC> T_FUNC;	//기능위치 I/F
	
	private String E_RESULT;		//결과정보
	private String E_MESSAGE;		//결과코드
	
	public List<ZPM_FUNC_DN_T_FUNC> getT_FUNC() {
		return T_FUNC;
	}
	public void setT_FUNC(List<ZPM_FUNC_DN_T_FUNC> t_FUNC) {
		T_FUNC = t_FUNC;
	}
	public String getE_RESULT() {
		return E_RESULT;
	}
	public void setE_RESULT(String e_RESULT) {
		E_RESULT = e_RESULT;
	}
	public String getE_MESSAGE() {
		return E_MESSAGE;
	}
	public void setE_MESSAGE(String e_MESSAGE) {
		E_MESSAGE = e_MESSAGE;
	}
	
	
	
	
}
