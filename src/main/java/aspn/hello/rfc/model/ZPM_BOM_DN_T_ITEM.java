package aspn.hello.rfc.model;

import java.io.Serializable;

/**
 * [SAP RFC통신] 설비BOM다운로드
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */

public class ZPM_BOM_DN_T_ITEM implements Serializable{
	
	private static final long serialVersionUID = 8677222567863997157L;
	
    private String ZTYPE;			//OBJECT TYPE
    private String ZOBJECT;			//OBJECT
    private String ZOBJTXT;			//OBJECT내역
    private String WERKS;			//플랜트
    private String DATUV;			//효력시작일자
    private String BMENG;			//기준수량
    private String BMEIN;			//단위
    private String POSNR;			//항목
    private String POSTP;			//품목범주
    private String IDNRK;			//구성부품
    private String MAKTX;			//구성부품내역
    private String MENGE;			//구성부품수량
    private String MEINS;			//구성부품단위
    
	public String getZTYPE() {
		return ZTYPE;
	}
	public void setZTYPE(String zTYPE) {
		ZTYPE = zTYPE;
	}
	public String getZOBJECT() {
		return ZOBJECT;
	}
	public void setZOBJECT(String zOBJECT) {
		ZOBJECT = zOBJECT;
	}
	public String getZOBJTXT() {
		return ZOBJTXT;
	}
	public void setZOBJTXT(String zOBJTXT) {
		ZOBJTXT = zOBJTXT;
	}
	public String getWERKS() {
		return WERKS;
	}
	public void setWERKS(String wERKS) {
		WERKS = wERKS;
	}
	public String getDATUV() {
		return DATUV;
	}
	public void setDATUV(String dATUV) {
		DATUV = dATUV;
	}
	public String getBMENG() {
		return BMENG;
	}
	public void setBMENG(String bMENG) {
		BMENG = bMENG;
	}
	public String getBMEIN() {
		return BMEIN;
	}
	public void setBMEIN(String bMEIN) {
		BMEIN = bMEIN;
	}
	public String getPOSNR() {
		return POSNR;
	}
	public void setPOSNR(String pOSNR) {
		POSNR = pOSNR;
	}
	public String getPOSTP() {
		return POSTP;
	}
	public void setPOSTP(String pOSTP) {
		POSTP = pOSTP;
	}
	public String getIDNRK() {
		return IDNRK;
	}
	public void setIDNRK(String iDNRK) {
		IDNRK = iDNRK;
	}
	public String getMAKTX() {
		return MAKTX;
	}
	public void setMAKTX(String mAKTX) {
		MAKTX = mAKTX;
	}
	public String getMENGE() {
		return MENGE;
	}
	public void setMENGE(String mENGE) {
		MENGE = mENGE;
	}
	public String getMEINS() {
		return MEINS;
	}
	public void setMEINS(String mEINS) {
		MEINS = mEINS;
	}
    
}
