package aspn.hello.rfc.model;

import java.util.List;

/**
 * [SAP RFC - 작업확정취소 IMPORT] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_CNF_CANCEL_IMPORT {
	
	private List<ZPM_CNF_CANCEL_T_CNF> t_cnf;
	

	public List<ZPM_CNF_CANCEL_T_CNF> getT_cnf() {
		return t_cnf;
	}
	public void setT_cnf(List<ZPM_CNF_CANCEL_T_CNF> t_cnf) {
		this.t_cnf = t_cnf;
	}
	
	
	
}
