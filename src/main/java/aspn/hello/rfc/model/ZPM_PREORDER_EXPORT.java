package aspn.hello.rfc.model;

import java.util.List;

/**
 * [SAP RFC - 예방보전오더 생성 EXPORT] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_PREORDER_EXPORT {
	
	private String E_RESULT;
	private String E_MESSAGE;
	
	//예방보전오더 EXPORT TABLE
	private List<ZPM_PREORDER_T_AFKO> zpm_preorder_t_afko;
	
	public String getE_RESULT() {
		return E_RESULT;
	}
	public void setE_RESULT(String e_RESULT) {
		E_RESULT = e_RESULT;
	}
	public String getE_MESSAGE() {
		return E_MESSAGE;
	}
	public void setE_MESSAGE(String e_MESSAGE) {
		E_MESSAGE = e_MESSAGE;
	}
	public List<ZPM_PREORDER_T_AFKO> getZpm_preorder_t_afko() {
		return zpm_preorder_t_afko;
	}
	public void setZpm_preorder_t_afko(List<ZPM_PREORDER_T_AFKO> zpm_preorder_t_afko) {
		this.zpm_preorder_t_afko = zpm_preorder_t_afko;
	}
	
}
