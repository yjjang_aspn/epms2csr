package aspn.hello.rfc.model;

import java.io.Serializable;

/**
 * [SAP RFC - 자매마스터 조회 T_ITEM] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */

public class ZPM_MATKL_T_MATKL implements Serializable{
	
	private static final long serialVersionUID = 8677222567863997157L;
	
	private String MATKL;  //자재그룹
	private String WGBEZ;  //자재그룹내역
	
	public String getMATKL() {
		return MATKL;
	}
	public void setMATKL(String mATKL) {
		MATKL = mATKL;
	}
	public String getWGBEZ() {
		return WGBEZ;
	}
	public void setWGBEZ(String wGBEZ) {
		WGBEZ = wGBEZ;
	}
    
}
