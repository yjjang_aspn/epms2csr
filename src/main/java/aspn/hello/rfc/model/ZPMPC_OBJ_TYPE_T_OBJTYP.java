package aspn.hello.rfc.model;

import java.io.Serializable;

/**
 * [SAP RFC - 오브젝트유형조회] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPMPC_OBJ_TYPE_T_OBJTYP implements Serializable{

    private String EQART;		//기술오브젝트 유형
    private String EARTX;		//오브젝트유형텍스트
    
	public String getEQART() {
		return EQART;
	}
	public void setEQART(String eQART) {
		EQART = eQART;
	}
	public String getEARTX() {
		return EARTX;
	}
	public void setEARTX(String eARTX) {
		EARTX = eARTX;
	}
	
	@Override
	public String toString() {
		return "T_OBJTYP [EQART=" + EQART + ", EARTX=" + EARTX + "]";
	}
    
}
