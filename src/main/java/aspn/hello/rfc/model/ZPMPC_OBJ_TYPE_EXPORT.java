package aspn.hello.rfc.model;

import java.util.List;

/**
 * [SAP RFC - 오브젝트유형조회] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPMPC_OBJ_TYPE_EXPORT{

	
	private List<ZPMPC_OBJ_TYPE_T_OBJTYP> zpmpc_obj_type_t_objtyp;	//오브젝트유형조회
	
	//결과정보
	private String E_RESULT;
	private String E_MESSAGE;
	
	public List<ZPMPC_OBJ_TYPE_T_OBJTYP> getZpmpc_obj_type_t_objtyp() {
		return zpmpc_obj_type_t_objtyp;
	}
	public void setZpmpc_obj_type_t_objtyp(List<ZPMPC_OBJ_TYPE_T_OBJTYP> zpmpc_obj_type_t_objtyp) {
		this.zpmpc_obj_type_t_objtyp = zpmpc_obj_type_t_objtyp;
	}
	public String getE_RESULT() {
		return E_RESULT;
	}
	public void setE_RESULT(String e_RESULT) {
		E_RESULT = e_RESULT;
	}
	public String getE_MESSAGE() {
		return E_MESSAGE;
	}
	public void setE_MESSAGE(String e_MESSAGE) {
		E_MESSAGE = e_MESSAGE;
	}
	
	
	
}
