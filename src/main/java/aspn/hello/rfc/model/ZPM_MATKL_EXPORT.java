package aspn.hello.rfc.model;

import java.util.List;

/**
 * [SAP RFC - 자매마스터 조회 T_ITEM] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_MATKL_EXPORT{
	
	private List<ZPM_MATKL_T_MATKL> zpm_matkl_t_matkl;	//자재그룹 내역
	
	//결과정보
	private String E_RESULT;
	private String E_MESSAGE;
	
	public String getE_RESULT() {
		return E_RESULT;
	}
	public void setE_RESULT(String e_RESULT) {
		E_RESULT = e_RESULT;
	}
	public String getE_MESSAGE() {
		return E_MESSAGE;
	}
	public void setE_MESSAGE(String e_MESSAGE) {
		E_MESSAGE = e_MESSAGE;
	}
	
	public List<ZPM_MATKL_T_MATKL> getZpm_matkl_t_matkl() {
		return zpm_matkl_t_matkl;
	}
	public void setZpm_matkl_t_matkl(List<ZPM_MATKL_T_MATKL> zpm_matkl_t_matkl) {
		this.zpm_matkl_t_matkl = zpm_matkl_t_matkl;
	}
		
}
