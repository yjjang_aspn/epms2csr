package aspn.hello.rfc.model;

import java.io.Serializable;

/**
 * [SAP RFC통신] 설비BOM다운로드
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_BOM_DN_IMPORT implements Serializable{
	
	private String I_ZTYPE		= ""; // OBJECT TYPE
	private String I_OBJECT		= ""; // OBJECT
	private String I_WERKS		= ""; // 플랜트
	private String I_YYYYMMDD 	= ""; // 다운로드 년월일
	
	public String getI_ZTYPE() {
		return I_ZTYPE;
	}
	public void setI_ZTYPE(String i_ZTYPE) {
		I_ZTYPE = i_ZTYPE;
	}
	public String getI_OBJECT() {
		return I_OBJECT;
	}
	public void setI_OBJECT(String i_OBJECT) {
		I_OBJECT = i_OBJECT;
	}
	public String getI_WERKS() {
		return I_WERKS;
	}
	public void setI_WERKS(String i_WERKS) {
		I_WERKS = i_WERKS;
	}
	public String getI_YYYYMMDD() {
		return I_YYYYMMDD;
	}
	public void setI_YYYYMMDD(String i_YYYYMMDD) {
		I_YYYYMMDD = i_YYYYMMDD;
	}
	
	
	
}
