package aspn.hello.rfc.model;

/**
 * [SAP RFC통신] 유통기한 및 재고수량 전송 - ITAB
 * @author 김영환
 * @since 2019.04.02	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2019.04.02		김영환			최초 생성
 *
 * </pre>
 */

public class ZMM_SELL_BY_DATE_ITAB{
		
    private String BUDAT;   //전표의 전기일
    private String WERKS;   //플랜트
    private String LGORT;   //저장 위치
    private String MATNR;   //자재 번호
    private String MAKTX;   //자재명
    private String ADATE;   //유통기한
    private String MENGE;   //수량
    private String MEINS;   //기본 단위
    
	public String getBUDAT() {
		return BUDAT;
	}
	public void setBUDAT(String bUDAT) {
		BUDAT = bUDAT;
	}
	public String getWERKS() {
		return WERKS;
	}
	public void setWERKS(String wERKS) {
		WERKS = wERKS;
	}
	public String getLGORT() {
		return LGORT;
	}
	public void setLGORT(String lGORT) {
		LGORT = lGORT;
	}
	public String getMATNR() {
		return MATNR;
	}
	public void setMATNR(String mATNR) {
		MATNR = mATNR;
	}
	public String getMAKTX() {
		return MAKTX;
	}
	public void setMAKTX(String mAKTX) {
		MAKTX = mAKTX;
	}
	public String getADATE() {
		return ADATE;
	}
	public void setADATE(String aDATE) {
		ADATE = aDATE;
	}
	public String getMENGE() {
		return MENGE;
	}
	public void setMENGE(String mENGE) {
		MENGE = mENGE;
	}
	public String getMEINS() {
		return MEINS;
	}
	public void setMEINS(String mEINS) {
		MEINS = mEINS;
	}
    
}
