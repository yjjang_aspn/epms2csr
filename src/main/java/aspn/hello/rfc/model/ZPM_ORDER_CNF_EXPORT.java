package aspn.hello.rfc.model;

import java.util.List;

/**
 * [SAP RFC - 작업오더 생성 EXPORT] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_ORDER_CNF_EXPORT {
	
	private String E_AUFNR;
	private String E_RESULT;
	private String E_MESSAGE;
	private String E_RMZHL;
	
	//작업오더 IMPORT, EXPORT TABLE
	private List<ZPM_ORDER_CNF_T_COMP> zpm_order_cnf_t_comp;
	
	public String getE_AUFNR() {
		return E_AUFNR;
	}
	public void setE_AUFNR(String e_AUFNR) {
		E_AUFNR = e_AUFNR;
	}
	public String getE_RESULT() {
		return E_RESULT;
	}
	public void setE_RESULT(String e_RESULT) {
		E_RESULT = e_RESULT;
	}
	public String getE_MESSAGE() {
		return E_MESSAGE;
	}
	public void setE_MESSAGE(String e_MESSAGE) {
		E_MESSAGE = e_MESSAGE;
	}
	public String getE_RMZHL() {
		return E_RMZHL;
	}
	public void setE_RMZHL(String e_RMZHL) {
		E_RMZHL = e_RMZHL;
	}
	public List<ZPM_ORDER_CNF_T_COMP> getZpm_order_cnf_t_comp() {
		return zpm_order_cnf_t_comp;
	}
	public void setZpm_order_cnf_t_comp(List<ZPM_ORDER_CNF_T_COMP> zpm_order_cnf_t_comp) {
		this.zpm_order_cnf_t_comp = zpm_order_cnf_t_comp;
	}
	
}
