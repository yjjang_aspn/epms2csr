package aspn.hello.rfc.model;

/**
 * [SAP RFC통신] 자재 재고 TABLE
 * @author 김영환
 * @since 2019.02.19	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2019.02.19		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_STOCK_T_MARD {
	
	private String MATNR;	//자재번호
	private String MAKTX;   //자재내역
	private String MEINS;   //기본단위
	private String LABST;   //현재고
	private String WERKS;   //플랜트
	private String LGORT;   //저장위치
	
	public String getMATNR() {
		return MATNR;
	}
	public void setMATNR(String mATNR) {
		MATNR = mATNR;
	}
	public String getMAKTX() {
		return MAKTX;
	}
	public void setMAKTX(String mAKTX) {
		MAKTX = mAKTX;
	}
	public String getMEINS() {
		return MEINS;
	}
	public void setMEINS(String mEINS) {
		MEINS = mEINS;
	}
	public String getLABST() {
		return LABST;
	}
	public void setLABST(String lABST) {
		LABST = lABST;
	}
	public String getWERKS() {
		return WERKS;
	}
	public void setWERKS(String wERKS) {
		WERKS = wERKS;
	}
	public String getLGORT() {
		return LGORT;
	}
	public void setLGORT(String lGORT) {
		LGORT = lGORT;
	}
	
}
