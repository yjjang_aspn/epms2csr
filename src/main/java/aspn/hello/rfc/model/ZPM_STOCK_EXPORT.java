package aspn.hello.rfc.model;

import java.util.List;

/**
 * [SAP RFC통신] 자재 재고 EXPORT PARM
 * @author 김영환
 * @since 2019.02.19	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2019.02.19		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_STOCK_EXPORT {
	
	private String e_result;
	private String e_MESSAGE;
	
	//자재재고 EXPORT TABLE
	private List<ZPM_STOCK_T_MARD> zpm_stock_t_mard;
	
	public String getE_result() {
		return e_result;
	}
	public void setE_result(String e_result) {
		this.e_result = e_result;
	}
	public String getE_MESSAGE() {
		return e_MESSAGE;
	}
	public void setE_MESSAGE(String e_MESSAGE) {
		this.e_MESSAGE = e_MESSAGE;
	}
	public List<ZPM_STOCK_T_MARD> getZpm_stock_t_mard() {
		return zpm_stock_t_mard;
	}
	public void setZpm_stock_t_mard(List<ZPM_STOCK_T_MARD> zpm_stock_t_mard) {
		this.zpm_stock_t_mard = zpm_stock_t_mard;
	}
	
}
