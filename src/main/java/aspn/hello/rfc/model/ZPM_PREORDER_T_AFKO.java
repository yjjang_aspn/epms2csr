package aspn.hello.rfc.model;

/**
 * [SAP RFC - 예방보전작업오더 생성 IMPORT] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_PREORDER_T_AFKO {
	
	private String AUFNR;	//PM 오더 번호
	private String AUART;   //PM 오더 유형
	private String GLTRP;   //기본종료일
	private String GSTRP;   //기본시작일
	private String LTXA1;   //점검 항목
	private String EQUNR;   //설비코드
	private String INGPR;   //계획자 그룹
	private String INNAM;   //계획자 그룹 이름
	private String PAKET;   //주기
	private String TECO;    //TECO 여부
	
	public String getAUFNR() {
		return AUFNR;
	}
	public void setAUFNR(String aUFNR) {
		AUFNR = aUFNR;
	}
	public String getAUART() {
		return AUART;
	}
	public void setAUART(String aUART) {
		AUART = aUART;
	}
	public String getGLTRP() {
		return GLTRP;
	}
	public void setGLTRP(String gLTRP) {
		GLTRP = gLTRP;
	}
	public String getGSTRP() {
		return GSTRP;
	}
	public void setGSTRP(String gSTRP) {
		GSTRP = gSTRP;
	}
	public String getLTXA1() {
		return LTXA1;
	}
	public void setLTXA1(String lTXA1) {
		LTXA1 = lTXA1;
	}
	public String getEQUNR() {
		return EQUNR;
	}
	public void setEQUNR(String eQUNR) {
		EQUNR = eQUNR;
	}
	public String getINGPR() {
		return INGPR;
	}
	public void setINGPR(String iNGPR) {
		INGPR = iNGPR;
	}
	public String getINNAM() {
		return INNAM;
	}
	public void setINNAM(String iNNAM) {
		INNAM = iNNAM;
	}
	public String getPAKET() {
		return PAKET;
	}
	public void setPAKET(String pAKET) {
		PAKET = pAKET;
	}
	public String getTECO() {
		return TECO;
	}
	public void setTECO(String tECO) {
		TECO = tECO;
	}
	
}
