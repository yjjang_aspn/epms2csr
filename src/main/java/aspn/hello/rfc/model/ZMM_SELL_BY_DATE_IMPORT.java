package aspn.hello.rfc.model;

/**
 * [SAP RFC통신] 유통기한 및 재고수량 전송 - IMPORT
 * @author 김영환
 * @since 2019.04.02	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2019.04.02		김영환			최초 생성
 *
 * </pre>
 */

public class ZMM_SELL_BY_DATE_IMPORT {

    private String I_BUDAT;		//업데이트일자 (필수)
    private String F_WERKS;		//(FROM) 플랜트
    private String T_WERKS;		//(TO) 플랜트
    private String F_LGORT;		//(FROM) 저장 위치
    private String T_LGORT;		//(TO) 저장 위치
    
	public String getI_BUDAT() {
		return I_BUDAT;
	}
	public void setI_BUDAT(String i_BUDAT) {
		I_BUDAT = i_BUDAT;
	}
	public String getF_WERKS() {
		return F_WERKS;
	}
	public void setF_WERKS(String f_WERKS) {
		F_WERKS = f_WERKS;
	}
	public String getT_WERKS() {
		return T_WERKS;
	}
	public void setT_WERKS(String t_WERKS) {
		T_WERKS = t_WERKS;
	}
	public String getF_LGORT() {
		return F_LGORT;
	}
	public void setF_LGORT(String f_LGORT) {
		F_LGORT = f_LGORT;
	}
	public String getT_LGORT() {
		return T_LGORT;
	}
	public void setT_LGORT(String t_LGORT) {
		T_LGORT = t_LGORT;
	}
    
}
