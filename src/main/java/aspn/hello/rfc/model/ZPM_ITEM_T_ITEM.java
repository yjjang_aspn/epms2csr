package aspn.hello.rfc.model;

import java.io.Serializable;

/**
 * [SAP RFC - 자매마스터 조회 T_ITEM] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_ITEM_T_ITEM implements Serializable{
	
	private static final long serialVersionUID = 8677222567863997157L;
	
	private String KATALOGART;  //카탈로그
	private String KATALOGTXT;  //카탈로그내역
	
    private String CODEGRUPPE;	//코드그룹
    private String CODEGRUTXT;  //코드그룹명
    private String CODE;       	//코드
    private String CODETXT;     //코드명
    
	public String getKATALOGART() {
		return KATALOGART;
	}
	public void setKATALOGART(String kATALOGART) {
		KATALOGART = kATALOGART;
	}
	public String getKATALOGTXT() {
		return KATALOGTXT;
	}
	public void setKATALOGTXT(String kATALOGTXT) {
		KATALOGTXT = kATALOGTXT;
	}
	public String getCODEGRUPPE() {
		return CODEGRUPPE;
	}
	public void setCODEGRUPPE(String cODEGRUPPE) {
		CODEGRUPPE = cODEGRUPPE;
	}
	public String getCODEGRUTXT() {
		return CODEGRUTXT;
	}
	public void setCODEGRUTXT(String cODEGRUTXT) {
		CODEGRUTXT = cODEGRUTXT;
	}
	public String getCODE() {
		return CODE;
	}
	public void setCODE(String cODE) {
		CODE = cODE;
	}
	public String getCODETXT() {
		return CODETXT;
	}
	public void setCODETXT(String cODETXT) {
		CODETXT = cODETXT;
	}
	@Override
	public String toString() {
		return "ZPM_ITEM_T_ITEM [KATALOGART=" + KATALOGART + ", KATALOGTXT=" + KATALOGTXT + ", CODEGRUPPE=" + CODEGRUPPE
				+ ", CODEGRUTXT=" + CODEGRUTXT + ", CODE=" + CODE + ", CODETXT=" + CODETXT + "]";
	}
    
    
    
	
    
}
