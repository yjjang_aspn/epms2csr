package aspn.hello.rfc.model;

import java.io.Serializable;

/**
 * [SAP RFC - 기능위치 조회 T_FUNC] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_FUNC_DN_T_FUNC implements Serializable{

    private String OBJECT_TYPE;		//OBJECT구분
    private String OBJECT;			//OBJECT
    private String DESCRIPTION;		//OBJECT명칭
    private String PRED_TYPE;		//상위위치구분
    private String PREDECESSOR;		//상위위치
    private String HLEVEL;			//LEVEL
    private String INACT;			//비활성유무
    private String KOKRS;			//관리회계 영역
    private String KOSTL;			//코스트센터
    private String EQFNR;			//정렬필드
    
	public String getOBJECT_TYPE() {
		return OBJECT_TYPE;
	}

	public void setOBJECT_TYPE(String oBJECT_TYPE) {
		OBJECT_TYPE = oBJECT_TYPE;
	}

	public String getOBJECT() {
		return OBJECT;
	}

	public void setOBJECT(String oBJECT) {
		OBJECT = oBJECT;
	}

	public String getDESCRIPTION() {
		return DESCRIPTION;
	}

	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}

	public String getPRED_TYPE() {
		return PRED_TYPE;
	}

	public void setPRED_TYPE(String pRED_TYPE) {
		PRED_TYPE = pRED_TYPE;
	}

	public String getPREDECESSOR() {
		return PREDECESSOR;
	}

	public void setPREDECESSOR(String pREDECESSOR) {
		PREDECESSOR = pREDECESSOR;
	}

	public String getHLEVEL() {
		return HLEVEL;
	}

	public void setHLEVEL(String hLEVEL) {
		HLEVEL = hLEVEL;
	}

	public String getINACT() {
		return INACT;
	}

	public void setINACT(String iNACT) {
		INACT = iNACT;
	}

	public String getKOKRS() {
		return KOKRS;
	}

	public void setKOKRS(String kOKRS) {
		KOKRS = kOKRS;
	}

	public String getKOSTL() {
		return KOSTL;
	}

	public void setKOSTL(String kOSTL) {
		KOSTL = kOSTL;
	}

	public String getEQFNR() {
		return EQFNR;
	}

	public void setEQFNR(String eQFNR) {
		EQFNR = eQFNR;
	}

	@Override
	public String toString() {
		return "T_FUNC.java [OBJECT_TYPE="+ OBJECT_TYPE + 
				", OBJECT="+ OBJECT + 
				", DESCRIPTION="+ DESCRIPTION + 
				", PRED_TYPE="+ PRED_TYPE + 
				", PREDECESSOR="+ PREDECESSOR + 
				", HLEVEL="+ HLEVEL + 
				", INACT="+ INACT + 
				", KOKRS="+ KOKRS +
				", KOSTL="+ KOSTL + 
				", EQFNR="+ EQFNR + "]";
	}
	
}
