package aspn.hello.rfc.model;

/**
 * [SAP RFC - 작업오더 생성 IMPORT] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_ORDER_CNF_T_TASK {
	
	private String MNGRP;    //코드그룹
	private String MNCOD;    //타스크코드
	private String PARNR;    //직무책임자id
	private String PSTER;    //계획된시작일
	private String PSTUR;    //계획된시작시간
	private String PETER;    //계획된종료일
	private String PETUR;    //계획된종료시간
	public String getMNGRP() {
		return MNGRP;
	}
	public void setMNGRP(String mNGRP) {
		MNGRP = mNGRP;
	}
	public String getMNCOD() {
		return MNCOD;
	}
	public void setMNCOD(String mNCOD) {
		MNCOD = mNCOD;
	}
	public String getPARNR() {
		return PARNR;
	}
	public void setPARNR(String pARNR) {
		PARNR = pARNR;
	}
	public String getPSTER() {
		return PSTER;
	}
	public void setPSTER(String pSTER) {
		PSTER = pSTER;
	}
	public String getPSTUR() {
		return PSTUR;
	}
	public void setPSTUR(String pSTUR) {
		PSTUR = pSTUR;
	}
	public String getPETER() {
		return PETER;
	}
	public void setPETER(String pETER) {
		PETER = pETER;
	}
	public String getPETUR() {
		return PETUR;
	}
	public void setPETUR(String pETUR) {
		PETUR = pETUR;
	}
	
}
