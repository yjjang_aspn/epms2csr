package aspn.hello.rfc.model;

/**
 * [SAP RFC - 작업오더 생성 IMPORT] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_ORDER_CNF_T_COMP {
	
	private String MATERIAL;       //자재코드
	private String PLANT;          //플랜트
	private String STGE_LOC;       //저장위치
	private String ITEM_CAT;       //품목범주
	private String ITEM_NUMBER;    //품목번호
	private String ACTIVITY;       //공정번호
	private String QUANTITY;       //소요수량
	private String QUANTITY_UNIT;  //단위
	private String METHOD;         //CRUD
	private String VERPR;          //단가
	
	public String getMATERIAL() {
		return MATERIAL;
	}
	public void setMATERIAL(String mATERIAL) {
		MATERIAL = mATERIAL;
	}
	public String getPLANT() {
		return PLANT;
	}
	public void setPLANT(String pLANT) {
		PLANT = pLANT;
	}
	public String getSTGE_LOC() {
		return STGE_LOC;
	}
	public void setSTGE_LOC(String sTGE_LOC) {
		STGE_LOC = sTGE_LOC;
	}
	public String getITEM_CAT() {
		return ITEM_CAT;
	}
	public void setITEM_CAT(String iTEM_CAT) {
		ITEM_CAT = iTEM_CAT;
	}
	public String getITEM_NUMBER() {
		return ITEM_NUMBER;
	}
	public void setITEM_NUMBER(String iTEM_NUMBER) {
		ITEM_NUMBER = iTEM_NUMBER;
	}
	public String getACTIVITY() {
		return ACTIVITY;
	}
	public void setACTIVITY(String aCTIVITY) {
		ACTIVITY = aCTIVITY;
	}
	public String getQUANTITY() {
		return QUANTITY;
	}
	public void setQUANTITY(String qUANTITY) {
		QUANTITY = qUANTITY;
	}
	public String getQUANTITY_UNIT() {
		return QUANTITY_UNIT;
	}
	public void setQUANTITY_UNIT(String qUANTITY_UNIT) {
		QUANTITY_UNIT = qUANTITY_UNIT;
	}
	public String getMETHOD() {
		return METHOD;
	}
	public void setMETHOD(String mETHOD) {
		METHOD = mETHOD;
	}
	public String getVERPR() {
		return VERPR;
	}
	public void setVERPR(String vERPR) {
		VERPR = vERPR;
	}
	
}
