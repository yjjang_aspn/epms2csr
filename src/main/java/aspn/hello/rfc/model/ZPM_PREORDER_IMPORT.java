package aspn.hello.rfc.model;

import java.io.Serializable;

/**
 * [SAP RFC통신] 예방보전 오더 INPUT PARM
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_PREORDER_IMPORT implements Serializable{
	
	private String I_SPMON		= ""; // 다운로드 년월

	public String getI_SPMON() {
		return I_SPMON;
	}
	public void setI_SPMON(String i_SPMON) {
		I_SPMON = i_SPMON;
	}
	
}
