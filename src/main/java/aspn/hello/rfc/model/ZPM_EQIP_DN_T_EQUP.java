package aspn.hello.rfc.model;

import java.io.Serializable;

/**
 * [SAP RFC통신] 설비마스터정보
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */

public class ZPM_EQIP_DN_T_EQUP implements Serializable{
	
	private static final long serialVersionUID = 8677222567863997157L;
	
    private String EQUNR;			//설비번호
    private String DATSL;			//유효일
    private String EQTYP;			//설비범주
    private String EQKTX;			//설비내역
    private String EQART;			//오브젝트유형
    private String EARTX;			//오브젝트유형텍스트
    private String BRGEW;			//중량
    private String GEWEI;			//중량단위
    private String GROES;			//크기/치수
    private String INVNR;			//재고번호
    private String INBDT;			//가동시작일
    private String ANSWT;			//취득가액
    private String WAERS;			//통화키
    private String ANSDT;			//취득일
    private String HERST;			//제조사
    private String HERLD;			//제조국
    private String BAUJJ;			//설치년도
    private String BAUMM;			//설치월
    private String TYPBZ;			//모델번호
    private String MAPAR;			//제조자부품번호
    private String SERGE;			//제조자일련번호
    private String SWERK;			//유지보수플랜트
    private String BEBER;			//플랜트섹션
    private String ARBPL;			//작업장
    private String EQFNR;			//정렬필드
    private String BUKRS;			//회사코드
    private String ANLNR;			//자산
    private String GSBER;			//사업영역
    private String KOSTL;			//코스트센터
    private String IWERK;			//계획플랜트
    private String INGRP;			//계획자그룹
    private String GEWRK;			//주요작업장
    private String WERGW;			//작업장플랜트
    private String RBNR;			//Catalogprofile
    private String TPLNR;			//설치기능위치
    private String DATUM;			//설치/철거일자
    private String UZEIT;			//설치/철거시간
    private String INACT;			//비활성유무
	
	public String getEQUNR() {
		return EQUNR;
	}

	public void setEQUNR(String eQUNR) {
		EQUNR = eQUNR;
	}

	public String getDATSL() {
		return DATSL;
	}

	public void setDATSL(String dATSL) {
		DATSL = dATSL;
	}

	public String getEQTYP() {
		return EQTYP;
	}

	public void setEQTYP(String eQTYP) {
		EQTYP = eQTYP;
	}

	public String getEQKTX() {
		return EQKTX;
	}

	public void setEQKTX(String eQKTX) {
		EQKTX = eQKTX;
	}

	public String getEQART() {
		return EQART;
	}

	public void setEQART(String eQART) {
		EQART = eQART;
	}

	public String getEARTX() {
		return EARTX;
	}

	public void setEARTX(String eARTX) {
		EARTX = eARTX;
	}

	public String getBRGEW() {
		return BRGEW;
	}

	public void setBRGEW(String bRGEW) {
		BRGEW = bRGEW;
	}

	public String getGEWEI() {
		return GEWEI;
	}

	public void setGEWEI(String gEWEI) {
		GEWEI = gEWEI;
	}

	public String getGROES() {
		return GROES;
	}

	public void setGROES(String gROES) {
		GROES = gROES;
	}

	public String getINVNR() {
		return INVNR;
	}

	public void setINVNR(String iNVNR) {
		INVNR = iNVNR;
	}

	public String getINBDT() {
		return INBDT;
	}

	public void setINBDT(String iNBDT) {
		INBDT = iNBDT;
	}

	public String getANSWT() {
		return ANSWT;
	}

	public void setANSWT(String aNSWT) {
		ANSWT = aNSWT;
	}

	public String getWAERS() {
		return WAERS;
	}

	public void setWAERS(String wAERS) {
		WAERS = wAERS;
	}

	public String getANSDT() {
		return ANSDT;
	}

	public void setANSDT(String aNSDT) {
		ANSDT = aNSDT;
	}

	public String getHERST() {
		return HERST;
	}

	public void setHERST(String hERST) {
		HERST = hERST;
	}

	public String getHERLD() {
		return HERLD;
	}

	public void setHERLD(String hERLD) {
		HERLD = hERLD;
	}

	public String getBAUJJ() {
		return BAUJJ;
	}

	public void setBAUJJ(String bAUJJ) {
		BAUJJ = bAUJJ;
	}

	public String getBAUMM() {
		return BAUMM;
	}

	public void setBAUMM(String bAUMM) {
		BAUMM = bAUMM;
	}

	public String getTYPBZ() {
		return TYPBZ;
	}

	public void setTYPBZ(String tYPBZ) {
		TYPBZ = tYPBZ;
	}

	public String getMAPAR() {
		return MAPAR;
	}

	public void setMAPAR(String mAPAR) {
		MAPAR = mAPAR;
	}

	public String getSERGE() {
		return SERGE;
	}

	public void setSERGE(String sERGE) {
		SERGE = sERGE;
	}

	public String getSWERK() {
		return SWERK;
	}

	public void setSWERK(String sWERK) {
		SWERK = sWERK;
	}

	public String getBEBER() {
		return BEBER;
	}

	public void setBEBER(String bEBER) {
		BEBER = bEBER;
	}

	public String getARBPL() {
		return ARBPL;
	}

	public void setARBPL(String aRBPL) {
		ARBPL = aRBPL;
	}

	public String getEQFNR() {
		return EQFNR;
	}

	public void setEQFNR(String eQFNR) {
		EQFNR = eQFNR;
	}

	public String getBUKRS() {
		return BUKRS;
	}

	public void setBUKRS(String bUKRS) {
		BUKRS = bUKRS;
	}

	public String getANLNR() {
		return ANLNR;
	}

	public void setANLNR(String aNLNR) {
		ANLNR = aNLNR;
	}

	public String getGSBER() {
		return GSBER;
	}

	public void setGSBER(String gSBER) {
		GSBER = gSBER;
	}

	public String getKOSTL() {
		return KOSTL;
	}

	public void setKOSTL(String kOSTL) {
		KOSTL = kOSTL;
	}

	public String getIWERK() {
		return IWERK;
	}

	public void setIWERK(String iWERK) {
		IWERK = iWERK;
	}

	public String getINGRP() {
		return INGRP;
	}

	public void setINGRP(String iNGRP) {
		INGRP = iNGRP;
	}

	public String getGEWRK() {
		return GEWRK;
	}

	public void setGEWRK(String gEWRK) {
		GEWRK = gEWRK;
	}

	public String getWERGW() {
		return WERGW;
	}

	public void setWERGW(String wERGW) {
		WERGW = wERGW;
	}

	public String getRBNR() {
		return RBNR;
	}

	public void setRBNR(String rBNR) {
		RBNR = rBNR;
	}

	public String getTPLNR() {
		return TPLNR;
	}

	public void setTPLNR(String tPLNR) {
		TPLNR = tPLNR;
	}

	public String getDATUM() {
		return DATUM;
	}

	public void setDATUM(String dATUM) {
		DATUM = dATUM;
	}

	public String getUZEIT() {
		return UZEIT;
	}

	public void setUZEIT(String uZEIT) {
		UZEIT = uZEIT;
	}

	public String getINACT() {
		return INACT;
	}

	public void setINACT(String iNACT) {
		INACT = iNACT;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "T_EQIP.java [EQUNR="+ EQUNR + 
				", DATSL="+ DATSL + 
				", EQTYP="+ EQTYP + 
				", EQKTX="+ EQKTX + 
				", EQART="+ EQART + 
				", EARTX="+ EARTX + 
				", BRGEW="+ BRGEW + 
				", GEWEI="+ GEWEI + 
				", GROES="+ GROES + 
				", INVNR="+ INVNR + 
				", INBDT="+ INBDT + 
				", ANSWT="+ ANSWT + 
				", WAERS="+ WAERS + 
				", ANSDT="+ ANSDT + 
				", HERST="+ HERST + 
				", HERLD="+ HERLD + 
				", BAUJJ="+ BAUJJ + 
				", BAUMM="+ BAUMM + 
				", TYPBZ="+ TYPBZ + 
				", MAPAR="+ MAPAR + 
				", SERGE="+ SERGE + 
				", SWERK="+ SWERK + 
				", BEBER="+ BEBER + 
				", ARBPL="+ ARBPL + 
				", EQFNR="+ EQFNR + 
				", BUKRS="+ BUKRS + 
				", ANLNR="+ ANLNR + 
				", GSBER="+ GSBER + 
				", KOSTL="+ KOSTL + 
				", IWERK="+ IWERK + 
				", INGRP="+ INGRP + 
				", GEWRK="+ GEWRK + 
				", WERGW="+ WERGW + 
				", RBNR="+ RBNR + 
				", TPLNR="+ TPLNR + 
				", DATUM="+ DATUM + 
				", UZEIT="+ UZEIT + 
				", INACT="+ INACT + "]";
	}
	
}
