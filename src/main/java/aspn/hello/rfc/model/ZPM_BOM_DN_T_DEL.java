package aspn.hello.rfc.model;

import java.io.Serializable;

/**
 * [SAP RFC통신] 설비BOM다운로드
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */

public class ZPM_BOM_DN_T_DEL implements Serializable{
	
	private static final long serialVersionUID = 8677222567863997157L;
	
    private String ZTYPE;		//OBJECT TYPE
    private String ZOBJECT;		//OBJECT
    private String WERKS;		//플랜트
    private String UDATE;		//삭제일자
    
	public String getZTYPE() {
		return ZTYPE;
	}
	public void setZTYPE(String zTYPE) {
		ZTYPE = zTYPE;
	}
	public String getZOBJECT() {
		return ZOBJECT;
	}
	public void setZOBJECT(String zOBJECT) {
		ZOBJECT = zOBJECT;
	}
	public String getWERKS() {
		return WERKS;
	}
	public void setWERKS(String wERKS) {
		WERKS = wERKS;
	}
	public String getUDATE() {
		return UDATE;
	}
	public void setUDATE(String uDATE) {
		UDATE = uDATE;
	}
        
}
