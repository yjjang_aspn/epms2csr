package aspn.hello.rfc.model;

/**
 * [SAP RFC통신] 작업확정취소 T_CNF
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */

public class ZPM_CNF_CANCEL_T_CNF{
		
    private String AUFNR;
    private String VORNR;
    private String RMZHL;
    
	public String getAUFNR() {
		return AUFNR;
	}
	public void setAUFNR(String aUFNR) {
		AUFNR = aUFNR;
	}
	public String getVORNR() {
		return VORNR;
	}
	public void setVORNR(String vORNR) {
		VORNR = vORNR;
	}
	public String getRMZHL() {
		return RMZHL;
	}
	public void setRMZHL(String rMZHL) {
		RMZHL = rMZHL;
	}
}
