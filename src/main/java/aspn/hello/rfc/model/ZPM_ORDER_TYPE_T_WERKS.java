package  aspn.hello.rfc.model;

import java.io.Serializable;

/**
 * [SAP RFC통신] 플랜트별오더유형 조회
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */

public class ZPM_ORDER_TYPE_T_WERKS implements Serializable{
	
    private String WERKS;			//플랜트
    
	public String getWERKS() {
		return WERKS;
	}
	public void setWERKS(String wERKS) {
		WERKS = wERKS;
	}
	@Override
	public String toString() {
		return "T_WERKS [WERKS=" + WERKS + "]";
	}
	
	
}
