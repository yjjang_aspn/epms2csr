package aspn.hello.rfc.model;

import java.io.Serializable;

/**
 * [SAP RFC - 플랜트별오더유형조회] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */

public class ZPM_ORDER_TYPE_T_ORDTYP implements Serializable{
	
    private String WERKS;		//플랜트 : 삼립-시화:7100, 샤니-성남:1100, 샤니-대구:1200
    private String AUART;		//오더유형
    private String TXT;			//오더유형텍스트
    
	public String getWERKS() {
		return WERKS;
	}
	public void setWERKS(String wERKS) {
		WERKS = wERKS;
	}
	public String getAUART() {
		return AUART;
	}
	public void setAUART(String aUART) {
		AUART = aUART;
	}
	public String getTXT() {
		return TXT;
	}
	public void setTXT(String tXT) {
		TXT = tXT;
	}
	
	@Override
	public String toString() {
		return "T_ORDTYP [WERKS=" + WERKS + ", AUART=" + AUART + ", TXT=" + TXT + "]";
	}
    
}
