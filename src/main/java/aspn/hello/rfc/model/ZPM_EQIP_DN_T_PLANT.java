package  aspn.hello.rfc.model;

import java.io.Serializable;

/**
 * [SAP RFC통신] 설비마스터정보
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */

public class ZPM_EQIP_DN_T_PLANT implements Serializable{
	
	private static final long serialVersionUID = 8677222567863997157L;
	
    private String PLANT;				//플랜트

    public String getPLANT() {
		return PLANT;
	}
	public void setPLANT(String pLANT) {
		PLANT = pLANT;
	}
	
		
}
