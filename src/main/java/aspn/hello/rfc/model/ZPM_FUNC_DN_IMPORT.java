package aspn.hello.rfc.model;

/**
 * [SAP RFC - 기능위치 조회 IMPORT] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */

public class ZPM_FUNC_DN_IMPORT {
	
	private String I_OBJECT = "";	//기능 위치 코드

	public String getI_OBJECT() {
		return I_OBJECT;
	}
	public void setI_OBJECT(String i_OBJECT) {
		I_OBJECT = i_OBJECT;
	}
}
