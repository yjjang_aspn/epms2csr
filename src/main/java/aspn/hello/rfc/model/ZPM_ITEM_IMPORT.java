package aspn.hello.rfc.model;

/**
 * [SAP - 카테고리ITEM 조회  ZPM_ITEM_IMPORT] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_ITEM_IMPORT {
	
	private String I_GUBN 		= ""; // 코드구분
	private String I_CODEGRUPPE = ""; // 코드그룹
	
	public String getI_GUBN() {
		return I_GUBN;
	}
	public void setI_GUBN(String i_GUBN) {
		I_GUBN = i_GUBN;
	}
	public String getI_CODEGRUPPE() {
		return I_CODEGRUPPE;
	}
	public void setI_CODEGRUPPE(String i_CODEGRUPPE) {
		I_CODEGRUPPE = i_CODEGRUPPE;
	}
	
	@Override
	public String toString() {
		return "ZPM_ITEM_IMPORT [I_GUBN=" + I_GUBN + ", I_CODEGRUPPE=" + I_CODEGRUPPE + "]";
	}
	
	
	
}
