package aspn.hello.rfc.model;

import java.util.List;

/**
 * [SAP RFC - 플랜트별오더유형조회] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */

public class ZPM_ORDER_TYPE_EXPORT{

	
	private List<ZPM_ORDER_TYPE_T_ORDTYP> zpm_order_type_t_ordtyp;	//플랜트별오더유형
	
	//결과정보
	private String E_RESULT;
	private String E_MESSAGE;
	
	public List<ZPM_ORDER_TYPE_T_ORDTYP> getZpm_order_type_t_ordtyp() {
		return zpm_order_type_t_ordtyp;
	}
	public void setZpm_order_type_t_ordtyp(List<ZPM_ORDER_TYPE_T_ORDTYP> zpm_order_type_t_ordtyp) {
		this.zpm_order_type_t_ordtyp = zpm_order_type_t_ordtyp;
	}
	public String getE_RESULT() {
		return E_RESULT;
	}
	public void setE_RESULT(String e_RESULT) {
		E_RESULT = e_RESULT;
	}
	public String getE_MESSAGE() {
		return E_MESSAGE;
	}
	public void setE_MESSAGE(String e_MESSAGE) {
		E_MESSAGE = e_MESSAGE;
	}
	
	
	
	
}
