package aspn.hello.rfc.model;

import java.util.List;

/**
 * [SAP RFC - 작업확정취소 IMPORT] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_MATKL_IMPORT {
	
	private List<ZPM_MATKL_T_MATKL> t_matkl;

	public List<ZPM_MATKL_T_MATKL> getT_matkl() {
		return t_matkl;
	}

	public void setT_matkl(List<ZPM_MATKL_T_MATKL> t_matkl) {
		this.t_matkl = t_matkl;
	}
	
}
