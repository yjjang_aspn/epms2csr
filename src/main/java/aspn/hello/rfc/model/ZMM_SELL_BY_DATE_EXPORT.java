package aspn.hello.rfc.model;

import java.util.List;

/**
 * [SAP RFC통신] 유통기한 및 재고수량 전송 - EXPORT
 * @author 김영환
 * @since 2019.04.02	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2019.04.02		김영환			최초 생성
 *
 * </pre>
 */

public class ZMM_SELL_BY_DATE_EXPORT{
	
	private List<ZMM_SELL_BY_DATE_ITAB> zmm_sell_by_date_itab;		//유통기한전송 구조
	
	private String E_RESULT;		//결과코드
	private String E_MESSAGE;		//결과정보
	
	public String getE_RESULT() {
		return E_RESULT;
	}
	public void setE_RESULT(String e_RESULT) {
		E_RESULT = e_RESULT;
	}
	public String getE_MESSAGE() {
		return E_MESSAGE;
	}
	public void setE_MESSAGE(String e_MESSAGE) {
		E_MESSAGE = e_MESSAGE;
	}
	public List<ZMM_SELL_BY_DATE_ITAB> getZmm_sell_by_date_itab() {
		return zmm_sell_by_date_itab;
	}
	public void setZmm_sell_by_date_itab(List<ZMM_SELL_BY_DATE_ITAB> zmm_sell_by_date_itab) {
		this.zmm_sell_by_date_itab = zmm_sell_by_date_itab;
	}
	
}
