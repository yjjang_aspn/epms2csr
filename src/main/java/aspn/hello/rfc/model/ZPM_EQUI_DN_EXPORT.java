package aspn.hello.rfc.model;

import java.io.Serializable;
import java.util.List;

/**
 * [SAP RFC통신] 설비마스터정보
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */

public class ZPM_EQUI_DN_EXPORT implements Serializable{
	
	//기능위치 I/F
	private List<ZPM_EQIP_DN_T_EQUP> zpm_eqip_dn_t_equp;
	
	//결과정보
	private String e_result;
	private String e_message;
	
	public List<ZPM_EQIP_DN_T_EQUP> getZpm_eqip_dn_t_equp() {
		return zpm_eqip_dn_t_equp;
	}
	public void setZpm_eqip_dn_t_equp(List<ZPM_EQIP_DN_T_EQUP> zpm_eqip_dn_t_equp) {
		this.zpm_eqip_dn_t_equp = zpm_eqip_dn_t_equp;
	}
	public String getE_result() {
		return e_result;
	}
	public void setE_result(String e_result) {
		this.e_result = e_result;
	}
	public String getE_message() {
		return e_message;
	}
	public void setE_message(String e_message) {
		this.e_message = e_message;
	}
	
	
	
	
}
