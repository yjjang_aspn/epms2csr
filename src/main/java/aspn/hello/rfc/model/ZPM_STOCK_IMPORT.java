package aspn.hello.rfc.model;

import java.io.Serializable;

/**
 * [SAP RFC통신] 자재 재고 INPUT PARM
 * @author 김영환
 * @since 2019.02.19	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2019.02.19		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_STOCK_IMPORT implements Serializable{
	
	private String I_MATNR		= ""; // 자재번호
	private String I_WERKS		= ""; // 플랜트
	private String I_LGORT		= ""; // 저장위치
	private String I_YYYYMMDD	= ""; // 기준년월일
	
	public String getI_MATNR() {
		return I_MATNR;
	}
	public void setI_MATNR(String i_MATNR) {
		I_MATNR = i_MATNR;
	}
	public String getI_WERKS() {
		return I_WERKS;
	}
	public void setI_WERKS(String i_WERKS) {
		I_WERKS = i_WERKS;
	}
	public String getI_LGORT() {
		return I_LGORT;
	}
	public void setI_LGORT(String i_LGORT) {
		I_LGORT = i_LGORT;
	}
	public String getI_YYYYMMDD() {
		return I_YYYYMMDD;
	}
	public void setI_YYYYMMDD(String i_YYYYMMDD) {
		I_YYYYMMDD = i_YYYYMMDD;
	}
	
}
