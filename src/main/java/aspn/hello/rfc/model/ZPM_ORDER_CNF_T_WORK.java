package aspn.hello.rfc.model;

/**
 * [SAP RFC - 작업오더 생성 IMPORT] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_ORDER_CNF_T_WORK {
	
	private String VORNR;	//작업OP
	private String STEUS;   //제어키
	private String LTXA1;   //작업내역
	private String WERKS;   //정비플랜트
	private String ARBPL;   //작업장
	private String ARBEI;   //총작업시간
	private String DAUNO;   //작업기간[H]
	private String ISMNU;   //시간단위
	private String ANZZL;   //인원수
	private String LARNT;   //액티비티 유형
	private String INDET;   //계산키
	private String METHOD;  //CRUD
	private String ISDD;    //시작일자
	private String ISDZ;    //시작일자시간
	private String IEDD;    //종료일자
	private String IEDZ;    //종료일자시간
	private String PERNR;   //작업자ID
	private String LTXA2;   //확정내역
	private String AUERU;   //최종확정여부
	private String BUDAT;   //전기일자
	private String RMZHL;   //확정카운터
	
	public String getVORNR() {
		return VORNR;
	}
	public void setVORNR(String vORNR) {
		VORNR = vORNR;
	}
	public String getSTEUS() {
		return STEUS;
	}
	public void setSTEUS(String sTEUS) {
		STEUS = sTEUS;
	}
	public String getLTXA1() {
		return LTXA1;
	}
	public void setLTXA1(String lTXA1) {
		LTXA1 = lTXA1;
	}
	public String getWERKS() {
		return WERKS;
	}
	public void setWERKS(String wERKS) {
		WERKS = wERKS;
	}
	public String getARBPL() {
		return ARBPL;
	}
	public void setARBPL(String aRBPL) {
		ARBPL = aRBPL;
	}
	public String getARBEI() {
		return ARBEI;
	}
	public void setARBEI(String aRBEI) {
		ARBEI = aRBEI;
	}
	public String getDAUNO() {
		return DAUNO;
	}
	public void setDAUNO(String dAUNO) {
		DAUNO = dAUNO;
	}
	public String getISMNU() {
		return ISMNU;
	}
	public void setISMNU(String iSMNU) {
		ISMNU = iSMNU;
	}
	public String getANZZL() {
		return ANZZL;
	}
	public void setANZZL(String aNZZL) {
		ANZZL = aNZZL;
	}
	public String getLARNT() {
		return LARNT;
	}
	public void setLARNT(String lARNT) {
		LARNT = lARNT;
	}
	public String getINDET() {
		return INDET;
	}
	public void setINDET(String iNDET) {
		INDET = iNDET;
	}
	public String getMETHOD() {
		return METHOD;
	}
	public void setMETHOD(String mETHOD) {
		METHOD = mETHOD;
	}
	public String getISDD() {
		return ISDD;
	}
	public void setISDD(String iSDD) {
		ISDD = iSDD;
	}
	public String getISDZ() {
		return ISDZ;
	}
	public void setISDZ(String iSDZ) {
		ISDZ = iSDZ;
	}
	public String getIEDD() {
		return IEDD;
	}
	public void setIEDD(String iEDD) {
		IEDD = iEDD;
	}
	public String getIEDZ() {
		return IEDZ;
	}
	public void setIEDZ(String iEDZ) {
		IEDZ = iEDZ;
	}
	public String getPERNR() {
		return PERNR;
	}
	public void setPERNR(String pERNR) {
		PERNR = pERNR;
	}
	public String getLTXA2() {
		return LTXA2;
	}
	public void setLTXA2(String lTXA2) {
		LTXA2 = lTXA2;
	}
	public String getAUERU() {
		return AUERU;
	}
	public void setAUERU(String aUERU) {
		AUERU = aUERU;
	}
	public String getBUDAT() {
		return BUDAT;
	}
	public void setBUDAT(String bUDAT) {
		BUDAT = bUDAT;
	}
	public String getRMZHL() {
		return RMZHL;
	}
	public void setRMZHL(String rMZHL) {
		RMZHL = rMZHL;
	}
	
}
