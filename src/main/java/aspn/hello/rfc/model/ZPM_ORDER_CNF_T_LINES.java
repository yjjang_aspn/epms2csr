package aspn.hello.rfc.model;

/**
 * [SAP RFC - 작업오더 생성 IMPORT] VO Class
 * @author 김영환
 * @since 2018.11.08	
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.08		김영환			최초 생성
 *
 * </pre>
 */
public class ZPM_ORDER_CNF_T_LINES {
	
	private String TDLINE;	//텍스트라인

	public String getTDLINE() {
		return TDLINE;
	}
	public void setTDLINE(String tDLINE) {
		TDLINE = tDLINE;
	}
}
