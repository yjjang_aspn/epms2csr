package aspn.hello.rfc.service;

import java.util.List;

import aspn.hello.rfc.model.ZMM_SELL_BY_DATE_EXPORT;
import aspn.hello.rfc.model.ZMM_SELL_BY_DATE_IMPORT;
import aspn.hello.rfc.model.ZPMPC_OBJ_TYPE_EXPORT;
import aspn.hello.rfc.model.ZPM_BOM_DN_EXPORT;
import aspn.hello.rfc.model.ZPM_BOM_DN_IMPORT;
import aspn.hello.rfc.model.ZPM_CNF_CANCEL_EXPORT;
import aspn.hello.rfc.model.ZPM_CNF_CANCEL_IMPORT;
import aspn.hello.rfc.model.ZPM_EQIP_DN_T_PLANT;
import aspn.hello.rfc.model.ZPM_EQUI_DN_EXPORT;
import aspn.hello.rfc.model.ZPM_EQUI_DN_IMPORT;
import aspn.hello.rfc.model.ZPM_FUNC_DN_EXPORT;
import aspn.hello.rfc.model.ZPM_FUNC_DN_IMPORT;
import aspn.hello.rfc.model.ZPM_ITEM_EXPORT;
import aspn.hello.rfc.model.ZPM_ITEM_IMPORT;
import aspn.hello.rfc.model.ZPM_MATERIAL_EXPORT;
import aspn.hello.rfc.model.ZPM_MATERIAL_IMPORT;
import aspn.hello.rfc.model.ZPM_MATKL_EXPORT;
import aspn.hello.rfc.model.ZPM_ORDER_CNF_EXPORT;
import aspn.hello.rfc.model.ZPM_ORDER_CNF_IMPORT;
import aspn.hello.rfc.model.ZPM_ORDER_TYPE_EXPORT;
import aspn.hello.rfc.model.ZPM_ORDER_TYPE_T_WERKS;
import aspn.hello.rfc.model.ZPM_PREORDER_EXPORT;
import aspn.hello.rfc.model.ZPM_PREORDER_IMPORT;
import aspn.hello.rfc.model.ZPM_STOCK_EXPORT;
import aspn.hello.rfc.model.ZPM_STOCK_IMPORT;

/**
 * [SAP RFC통신] Service Class
 * @author 김영환
 * @since 2018.11.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.10		김영환			최초 생성
 *   2018.11.12		김영환			SAP 설비마스터 조회
 *   2018.11.14		김영환			SAP 자재 마스터 조회
 *   2018.11.18		김영환			SAP 플랜트별오더유형 조회
 *   2018.11.18		김영환			SAP 오브젝트유형  조회
 *   2018.11.18		김영환			SAP 카테고리ITEM 조회
 *   2018.11.20		김영환			SAP 설비BOM 조회
 *   2019.02.19		김영환			SAP 예방보전오더정보
 *   2019.04.02		김영환			SAP 유통기한 및 재고수량 전송
 *
 * </pre>
 */
public interface RfcService {

	/**
	 * SAP 기능위치 업데이트
	 *
	 * @author 김영환
	 * @since 2018.11.10
	 * @param ZPM_FUNC_DN_IMPORT zpm_func_dn_import
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	ZPM_FUNC_DN_EXPORT getFuncSiteInfo(ZPM_FUNC_DN_IMPORT zpm_func_dn_import) throws Exception;

	/**
	 * SAP 설비 마스터 조회
	 *
	 * @author 김영환
	 * @since 2018.11.12
	 * @param ZPMPC_EQIP_DN_IMPORT zpmpc_eqip_dn_import
	 * @return ZPM_EQUI_DN
	 */	
	ZPM_EQUI_DN_EXPORT getEquipInfo(ZPM_EQUI_DN_IMPORT zpm_equi_dn_import, List<ZPM_EQIP_DN_T_PLANT> t_plantList) throws Exception;

	/**
	 * SAP 자재 그룹
	 *
	 * @author 김영환
	 * @since 2018.10.01
	 * @return ZPM_ORDER_CNF_EXPORT
	 */
	ZPM_MATKL_EXPORT getMateriaGrouplInfo() throws IllegalArgumentException, IllegalAccessException, InstantiationException;
	
	/**
	 * SAP 자재 마스터 조회
	 *
	 * @author 김영환
	 * @since 2018.11.14
	 * @param ZPM_MATERIAL_IMPORT zpm_material_import
	 * @return ZPM_MATERIAL_EXPORT
	 */	
	ZPM_MATERIAL_EXPORT getMaterialInfo(ZPM_MATERIAL_IMPORT zpm_material_import) throws Exception;

	/**
	 * SAP 플랜트별오더유형  조회
	 *
	 * @author 김영환
	 * @since 2018.11.18
	 * @param List<ZPM_ORDER_TYPE_T_WERKS> t_werksList
	 * @return ZPM_ORDER_TYPE_EXPORT
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException
	 */
	ZPM_ORDER_TYPE_EXPORT getOrderTypeInfo(List<ZPM_ORDER_TYPE_T_WERKS> t_werksList)throws IllegalArgumentException, IllegalAccessException, InstantiationException;

	/**
	 * SAP 오브젝트유형  조회
	 *
	 * @author 김영환
	 * @since 2018.11.18
	 * @return ZPMPC_OBJ_TYPE_EXPORT
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException
	 */
	ZPMPC_OBJ_TYPE_EXPORT getObjTypeInfo() throws IllegalArgumentException, IllegalAccessException, InstantiationException;

	/**
	 * SAP 오브젝트유형 조회 
	 *
	 * @author 김영환
	 * @since 2018.11.14
	 * @param ZPM_ITEM_IMPORT zpm_item_import 
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	ZPM_ITEM_EXPORT getCategoryItemInfo(ZPM_ITEM_IMPORT zpm_item_import) throws IllegalArgumentException, IllegalAccessException, InstantiationException;

	/**
	 * SAP 설비BOM 다운로드 
	 *
	 * @author 김영환
	 * @since 2018.11.20
	 * @param ZPM_BOM_DN_IMPORT zpm_bom_dn_import
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	ZPM_BOM_DN_EXPORT updateEquipBom(ZPM_BOM_DN_IMPORT zpm_bom_dn_import)throws IllegalArgumentException, IllegalAccessException, InstantiationException;
	
	/**
	 * SAP 작업오더생성확정
	 *
	 * @author 김영환
	 * @since 2018.11.14
	 * @param ZPM_ORDER_CNF_IMPORT zpm_order_cnf_import
	 * @return ZPM_ORDER_CNF_EXPORT
	 * @throws IllegalAccessException
	 * @throws InstantiationException 
	 */	
	ZPM_ORDER_CNF_EXPORT setOrderCompInfo(ZPM_ORDER_CNF_IMPORT zpm_order_cnf_import) throws IllegalArgumentException, IllegalAccessException, InstantiationException;

	/**
	 * SAP 작업확정취소
	 *
	 * @author 김영환
	 * @since 2018.11.14
	 * @param ZPM_CNF_CANCEL_IMPORT zpm_cnf_cancel_import
	 * @return ZPM_CNF_CANCEL_EXPORT
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException
	 */	
	ZPM_CNF_CANCEL_EXPORT setOrderCancelInfo(ZPM_CNF_CANCEL_IMPORT zpm_cnf_cancel_import) throws IllegalArgumentException, IllegalAccessException,InstantiationException;

	/**
	 * SAP 예방보전오더정보
	 *
	 * @author 김영환
	 * @since 2019.02.19
	 * @param ZPM_PREORDER_IMPORT zpm_preorder_import
	 * @return ZPM_PREORDER_EXPORT
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException
	 */	
	ZPM_PREORDER_EXPORT getPreventiveOrderInfo(ZPM_PREORDER_IMPORT zpm_preorder_import) throws IllegalArgumentException, IllegalAccessException, InstantiationException;
	
	/**
	 * SAP 자재 재고 정보
	 *
	 * @author 김영환
	 * @since 2019.02.19
	 * @param ZPM_STOCK_IMPORT zpm_stock_import
	 * @return ZPM_STOCK_EXPORT
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException
	 */	
	ZPM_STOCK_EXPORT getMaterialStockInfo(ZPM_STOCK_IMPORT zpm_stock_import) throws IllegalArgumentException, IllegalAccessException, InstantiationException;
	
	/**
	 * SAP 유통기한 및 재고수량 정보
	 *
	 * @author 김영환
	 * @since 2019.04.02
	 * @param ZMM_SELL_BY_DATE_IMPORT zmm_sell_by_date_import
	 * @return ZMM_SELL_BY_DATE_EXPORT
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */	
	ZMM_SELL_BY_DATE_EXPORT getReportMaterialStockInfo(ZMM_SELL_BY_DATE_IMPORT zmm_sell_by_date_import) throws IllegalArgumentException, IllegalAccessException, InstantiationException;
	
}
