package aspn.hello.rfc.service;

import java.io.FileNotFoundException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoParameterList;
import com.sap.conn.jco.JCoTable;

import aspn.hello.com.mapper.AttachMapper;
import aspn.hello.epms.equipment.master.mapper.EquipmentMasterMapper;
import aspn.hello.rfc.model.ZMM_SELL_BY_DATE_EXPORT;
import aspn.hello.rfc.model.ZMM_SELL_BY_DATE_IMPORT;
import aspn.hello.rfc.model.ZMM_SELL_BY_DATE_ITAB;
import aspn.hello.rfc.model.ZPMPC_OBJ_TYPE_EXPORT;
import aspn.hello.rfc.model.ZPMPC_OBJ_TYPE_T_OBJTYP;
import aspn.hello.rfc.model.ZPM_BOM_DN_EXPORT;
import aspn.hello.rfc.model.ZPM_BOM_DN_IMPORT;
import aspn.hello.rfc.model.ZPM_BOM_DN_T_DEL;
import aspn.hello.rfc.model.ZPM_BOM_DN_T_ITEM;
import aspn.hello.rfc.model.ZPM_CNF_CANCEL_EXPORT;
import aspn.hello.rfc.model.ZPM_CNF_CANCEL_IMPORT;
import aspn.hello.rfc.model.ZPM_CNF_CANCEL_T_CNF;
import aspn.hello.rfc.model.ZPM_EQIP_DN_T_EQUP;
import aspn.hello.rfc.model.ZPM_EQIP_DN_T_PLANT;
import aspn.hello.rfc.model.ZPM_EQUI_DN_EXPORT;
import aspn.hello.rfc.model.ZPM_EQUI_DN_IMPORT;
import aspn.hello.rfc.model.ZPM_FUNC_DN_EXPORT;
import aspn.hello.rfc.model.ZPM_FUNC_DN_IMPORT;
import aspn.hello.rfc.model.ZPM_FUNC_DN_T_FUNC;
import aspn.hello.rfc.model.ZPM_ITEM_EXPORT;
import aspn.hello.rfc.model.ZPM_ITEM_IMPORT;
import aspn.hello.rfc.model.ZPM_ITEM_T_ITEM;
import aspn.hello.rfc.model.ZPM_MATERIAL_EXPORT;
import aspn.hello.rfc.model.ZPM_MATERIAL_IMPORT;
import aspn.hello.rfc.model.ZPM_MATERIAL_T_ITEM;
import aspn.hello.rfc.model.ZPM_MATKL_EXPORT;
import aspn.hello.rfc.model.ZPM_MATKL_T_MATKL;
import aspn.hello.rfc.model.ZPM_ORDER_CNF_EXPORT;
import aspn.hello.rfc.model.ZPM_ORDER_CNF_IMPORT;
import aspn.hello.rfc.model.ZPM_ORDER_CNF_T_COMP;
import aspn.hello.rfc.model.ZPM_ORDER_TYPE_EXPORT;
import aspn.hello.rfc.model.ZPM_ORDER_TYPE_T_ORDTYP;
import aspn.hello.rfc.model.ZPM_ORDER_TYPE_T_WERKS;
import aspn.hello.rfc.model.ZPM_PREORDER_EXPORT;
import aspn.hello.rfc.model.ZPM_PREORDER_IMPORT;
import aspn.hello.rfc.model.ZPM_PREORDER_T_AFKO;
import aspn.hello.rfc.model.ZPM_STOCK_EXPORT;
import aspn.hello.rfc.model.ZPM_STOCK_IMPORT;
import aspn.hello.rfc.model.ZPM_STOCK_T_MARD;
import aspn.hello.rfc.util.JCOReflection;
import aspn.hello.rfc.util.RfcManager;

/**
 * [SAP RFC통신] Business Implement Class
 * @author 김영환
 * @since 2018.11.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.10		김영환			최초 생성
 *   2018.11.12		김영환			SAP 설비마스터 조회
 *   2018.11.14		김영환			SAP 자재 마스터 조회
 *   2018.11.18		김영환			SAP 플랜트별오더유형 조회
 *   2018.11.18		김영환			SAP 오브젝트유형  조회
 *   2018.11.18		김영환			SAP 카테고리ITEM 조회
 *   2018.11.20		김영환			SAP 설비BOM 조회
 *   2018.10.01		김영환			SAP 자재 그룹
 *
 * </pre>
 */
@Service
public class RfcServiceImpl implements RfcService{
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	EquipmentMasterMapper equipmentMasterMapper;
	
	@Autowired
	AttachMapper attachMapper;

	/**
	 * 기능위치 정보 가져오기
	 *
	 * @author 김영환
	 * @since 2018.11.12
	 * @param ZPM_FUNC_DN_IMPORT zpm_func_dn_import
	 * @return ZPM_FUNC_DN_EXPORT
	 */
	@Override
	@SuppressWarnings("unchecked")
	public ZPM_FUNC_DN_EXPORT getFuncSiteInfo(ZPM_FUNC_DN_IMPORT zpm_func_dn_import) throws IllegalArgumentException, IllegalAccessException,InstantiationException {
		logger.info("SAP 기능위치정보 가져오기 - 시작");

		RfcManager.loadProvider();
		
		JCoFunction function = RfcManager.getFunction("ZPM_FUNC_DN");
		JCOReflection.importJCOParam(function.getImportParameterList(),	zpm_func_dn_import);

		RfcManager.execute(function);

		ZPM_FUNC_DN_EXPORT zpm_func_dn_export = new ZPM_FUNC_DN_EXPORT();
		JCoTable table = function.getTableParameterList().getTable("T_FUNC");
		zpm_func_dn_export
				.setT_FUNC((List<ZPM_FUNC_DN_T_FUNC>) JCOReflection
						.exportJCOTable(table, ZPM_FUNC_DN_T_FUNC.class));

		JCoParameterList exportParam = function.getExportParameterList();
		zpm_func_dn_export.setE_RESULT(exportParam.getValue("E_RESULT").toString());
		zpm_func_dn_export.setE_MESSAGE(exportParam.getValue("E_MESSAGE").toString());
		
		logger.info("SAP 기능위치정보  가져오기 - 끝 " );

		return zpm_func_dn_export;
	}

	/**
	 * SAP 설비마스터 조회
	 *
	 * @author 김영환
	 * @since 2018.11.12
	 * @param ZPM_EQUI_DN_IMPORT zpm_equi_dn_import
	 * @return ZPM_EQUI_DN_EXPORT
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ZPM_EQUI_DN_EXPORT getEquipInfo(ZPM_EQUI_DN_IMPORT zpm_equi_dn_import, List<ZPM_EQIP_DN_T_PLANT> t_plantList) throws IllegalArgumentException, IllegalAccessException,InstantiationException {
		logger.info("SAP 설비마스터정보 가져오기 - 시작");

		RfcManager.loadProvider();
		
		JCoFunction function = RfcManager.getFunction("ZPM_EQUI_DN");
		JCOReflection.importJCOParam(function.getImportParameterList(),	zpm_equi_dn_import);
		JCoTable input_table = function.getTableParameterList().getTable("T_PLANT");
		JCOReflection.importJCOTable(input_table,	t_plantList);

		RfcManager.execute(function);

		ZPM_EQUI_DN_EXPORT zpm_equi_dn_export = new ZPM_EQUI_DN_EXPORT();

		JCoTable output_table = function.getTableParameterList().getTable("T_EQUP");

		zpm_equi_dn_export
				.setZpm_eqip_dn_t_equp((List<ZPM_EQIP_DN_T_EQUP>) JCOReflection
						.exportJCOTable(output_table, ZPM_EQIP_DN_T_EQUP.class));

		JCoParameterList exportParam = function.getExportParameterList();
		zpm_equi_dn_export.setE_result(exportParam.getValue("E_RESULT").toString());
		zpm_equi_dn_export.setE_message(exportParam.getValue("E_MESSAGE").toString());
		
		logger.info("SAP 설비마스터  가져오기 - 끝 " );

		return zpm_equi_dn_export;
	}
	
	/**
	 * SAP 자재 그룹 조회
	 *
	 * @author 김영환
	 * @since 2018.10.01
	 * @param ZPM_MATKL_IMPORT zpm_matkl_import
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws InstantiationException 
	 */
	@Override
	public ZPM_MATKL_EXPORT getMateriaGrouplInfo() throws IllegalArgumentException, IllegalAccessException, InstantiationException {
		logger.info("SAP 자재그룹 가져오기 - 시작");

		RfcManager.loadProvider();
		JCoFunction function = RfcManager.getFunction("ZPM_MATKL");
		RfcManager.execute(function);
		
		ZPM_MATKL_EXPORT zpm_matkl_export = new ZPM_MATKL_EXPORT();
		
		JCoTable table = function.getTableParameterList().getTable("T_MATKL");

		zpm_matkl_export
				.setZpm_matkl_t_matkl((List<ZPM_MATKL_T_MATKL>) JCOReflection
						.exportJCOTable(table, ZPM_MATKL_T_MATKL.class));
				
		JCoParameterList exportParam = function.getExportParameterList();
		zpm_matkl_export.setE_MESSAGE(exportParam.getValue("E_MESSAGE").toString());
		zpm_matkl_export.setE_RESULT(exportParam.getValue("E_RESULT").toString());
		
		logger.info("SAP 자재그룹  가져오기 - 끝 " );

		return zpm_matkl_export;
		
	}

	/**
	 * SAP 자재 마스터 조회
	 *
	 * @author 김영환
	 * @since 2018.11.14
	 * @param ZPM_MATERIAL_IMPORT zpm_material_import
	 * @return ZPM_MATERIAL_EXPORT
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws FileNotFoundException
	 * @throws Exception
	 */
	@Override
	public ZPM_MATERIAL_EXPORT getMaterialInfo(ZPM_MATERIAL_IMPORT zpm_material_import) throws IllegalArgumentException, IllegalAccessException,InstantiationException, FileNotFoundException {
		logger.info("SAP 자재마스터 가져오기 - 시작");

		RfcManager.loadProvider();
		JCoFunction function = RfcManager.getFunction("ZPM_MATERIAL");
		JCOReflection.importJCOParam(function.getImportParameterList(),	zpm_material_import);
		RfcManager.execute(function);

		ZPM_MATERIAL_EXPORT zpm_material_export = new ZPM_MATERIAL_EXPORT();

		JCoTable table = function.getTableParameterList().getTable("T_ITEM");

		zpm_material_export
				.setZpm_material_t_item((List<ZPM_MATERIAL_T_ITEM>) JCOReflection
						.exportJCOTable(table, ZPM_MATERIAL_T_ITEM.class));
		
		JCoParameterList exportParam = function.getExportParameterList();
		zpm_material_export.setE_RESULT(exportParam.getValue("E_RESULT").toString());
		zpm_material_export.setE_MESSAGE(exportParam.getValue("E_MESSAGE").toString());
		
		logger.info("SAP 자재마스터  가져오기 - 끝 " );

		return zpm_material_export;
	}

	/**
	 * SAP 플랜트별오더유형 조회
	 *
	 * @author 김영환
	 * @since 2018.11.18
	 * @param ZPM_ORDER_TYPE_IMPORT zpm_order_type_import
	 * @return ZPM_ORDER_TYPE_EXPORT
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	@Override
	public ZPM_ORDER_TYPE_EXPORT getOrderTypeInfo(List<ZPM_ORDER_TYPE_T_WERKS> t_werksList) throws IllegalArgumentException, IllegalAccessException,InstantiationException {
		logger.info("SAP 플랜트별오더유형 가져오기 - 시작");

		RfcManager.loadProvider();	
		
		JCoFunction function = RfcManager.getFunction("ZPM_ORDER_TYPE");
		JCoTable input_table = function.getTableParameterList().getTable("T_WERKS");
		JCOReflection.importJCOTable(input_table, t_werksList);
		RfcManager.execute(function);

		ZPM_ORDER_TYPE_EXPORT zpm_order_type_export = new ZPM_ORDER_TYPE_EXPORT();
		JCoTable table = function.getTableParameterList().getTable("T_ORDTYP");

		zpm_order_type_export
				.setZpm_order_type_t_ordtyp((List<ZPM_ORDER_TYPE_T_ORDTYP>) JCOReflection
						.exportJCOTable(table, ZPM_ORDER_TYPE_T_ORDTYP.class));

		JCoParameterList exportParam = function.getExportParameterList();
		zpm_order_type_export.setE_RESULT(exportParam.getValue("E_RESULT").toString());
		zpm_order_type_export.setE_MESSAGE(exportParam.getValue("E_MESSAGE").toString());
		
		logger.info("SAP 플랜트별오더유형  가져오기 - 끝 " );

		return zpm_order_type_export;
	}

	/**
	 * SAP 오브젝트유형  조회
	 *
	 * @author 김영환
	 * @since 2018.11.18
	 * @return ZPMPC_OBJ_TYPE_EXPORT
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException
	 */
	@Override
	public ZPMPC_OBJ_TYPE_EXPORT getObjTypeInfo() throws IllegalArgumentException, IllegalAccessException, InstantiationException {
		logger.info("SAP 오브젝트유형 가져오기 - 시작");

		RfcManager.loadProvider();
		JCoFunction function = RfcManager.getFunction("ZPM_OBJ_TYPE");
		RfcManager.execute(function);

		ZPMPC_OBJ_TYPE_EXPORT zpmpc_obj_type_export = new ZPMPC_OBJ_TYPE_EXPORT();

		JCoTable table = function.getTableParameterList().getTable("T_OBJTYP");

		zpmpc_obj_type_export
				.setZpmpc_obj_type_t_objtyp((List<ZPMPC_OBJ_TYPE_T_OBJTYP>) JCOReflection
						.exportJCOTable(table, ZPMPC_OBJ_TYPE_T_OBJTYP.class));
		
		JCoParameterList exportParam = function.getExportParameterList();
		zpmpc_obj_type_export.setE_RESULT(exportParam.getValue("E_RESULT").toString());
		zpmpc_obj_type_export.setE_MESSAGE(exportParam.getValue("E_MESSAGE").toString());
		
		logger.info("SAP 오브젝트유형  가져오기 - 끝 " );

		return zpmpc_obj_type_export;
	}

	/**
	 * SAP 카테고리ITEM 조회/등록 
	 *
	 * @author 김영환
	 * @since 2018.11.18
	 * @param ZPM_ITEM_IMPORT zpm_item_import
	 * @return ZPM_ITEM_EXPORT
	 * @throws Exception
	 */
	@Override
	public ZPM_ITEM_EXPORT getCategoryItemInfo(ZPM_ITEM_IMPORT zpm_item_import) throws IllegalArgumentException, IllegalAccessException, InstantiationException {
		logger.info("SAP 카테고리ITEM 가져오기 - 시작");

		RfcManager.loadProvider();
		JCoFunction function = RfcManager.getFunction("ZPM_ITEM");
		JCOReflection.importJCOParam(function.getImportParameterList(),	zpm_item_import);
		RfcManager.execute(function);

		ZPM_ITEM_EXPORT zpm_item_export = new ZPM_ITEM_EXPORT();

		JCoTable table = function.getTableParameterList().getTable("T_ITEM");

		zpm_item_export
				.setZpm_item_t_item((List<ZPM_ITEM_T_ITEM>) JCOReflection
						.exportJCOTable(table, ZPM_ITEM_T_ITEM.class));

		JCoParameterList exportParam = function.getExportParameterList();
		zpm_item_export.setE_RESULT(exportParam.getValue("E_RESULT").toString());
		zpm_item_export.setE_MESSAGE(exportParam.getValue("E_MESSAGE").toString());
		
		logger.info("SAP 카테고리ITEM 가져오기 - 끝 " );

		return zpm_item_export;
		
	}

	/**
	 * SAP 설비BOM 조회
	 *
	 * @author 김영환
	 * @since 2018.11.20
	 * @param ZPM_BOM_DN_IMPORT zpm_bom_dn_import
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public ZPM_BOM_DN_EXPORT updateEquipBom(ZPM_BOM_DN_IMPORT zpm_bom_dn_import) throws IllegalArgumentException, IllegalAccessException, InstantiationException {
		
		logger.info("SAP 설비BOM 가져오기 - 시작");

		RfcManager.loadProvider();
		JCoFunction function = RfcManager.getFunction("ZPM_BOM_DN");
		JCOReflection.importJCOParam(function.getImportParameterList(),	zpm_bom_dn_import);
		RfcManager.execute(function);

		ZPM_BOM_DN_EXPORT zpm_bom_dn_export = new ZPM_BOM_DN_EXPORT();

		JCoTable table = function.getTableParameterList().getTable("T_ITEM");
		JCoTable table2 = function.getTableParameterList().getTable("T_DEL");

		zpm_bom_dn_export
				.setZpm_bom_dn_t_item((List<ZPM_BOM_DN_T_ITEM>) JCOReflection
						.exportJCOTable(table, ZPM_BOM_DN_T_ITEM.class));
		
		zpm_bom_dn_export
		.setZpm_bom_dn_t_del((List<ZPM_BOM_DN_T_DEL>) JCOReflection
				.exportJCOTable(table2, ZPM_BOM_DN_T_DEL.class));

		JCoParameterList exportParam = function.getExportParameterList();
		zpm_bom_dn_export.setE_RESULT(exportParam.getValue("E_RESULT").toString());
		zpm_bom_dn_export.setE_MESSAGE(exportParam.getValue("E_MESSAGE").toString());
		
		logger.info("SAP 설비BOM 가져오기- 끝 " );

		return zpm_bom_dn_export;

	}
	
	/**
	 * SAP 작업오더확정
	 *
	 * @author 김영환
	 * @since 2018.11.20
	 * @param ZPM_ORDER_CNF_IMPORT zpm_order_cnf_import
	 * @return ZPM_ORDER_CNF_EXPORT
	 * @throws InstantiationException 
	 * @throws Exception
	 */
	@Override
	public ZPM_ORDER_CNF_EXPORT setOrderCompInfo(ZPM_ORDER_CNF_IMPORT zpm_order_cnf_import) throws IllegalArgumentException, IllegalAccessException,InstantiationException {
		logger.info("SAP 작업오더생성확정 - 시작");

		RfcManager.loadProvider();
		
		JCoFunction function = RfcManager.getFunction("ZPM_ORDER_CNF");
		
		JCoParameterList importParam = function.getImportParameterList();
		JCOReflection.importJCOParam(importParam, zpm_order_cnf_import);
		
		JCoTable input_t_work = function.getTableParameterList().getTable("T_WORK");
		JCOReflection.importJCOTable(input_t_work, zpm_order_cnf_import.getZpm_order_cnf_t_work());
		
		JCoTable input_t_lines = function.getTableParameterList().getTable("T_LINES");
		JCOReflection.importJCOTable(input_t_lines, zpm_order_cnf_import.getZpm_order_cnf_t_lines());
		
		JCoTable input_t_comp = function.getTableParameterList().getTable("T_COMP");
		JCOReflection.importJCOTable(input_t_comp, zpm_order_cnf_import.getZpm_order_cnf_t_comp());
		
		JCoTable input_t_task = function.getTableParameterList().getTable("T_TASK");
		JCOReflection.importJCOTable(input_t_task, zpm_order_cnf_import.getZpm_order_cnf_t_task());
		
		RfcManager.execute(function);
		ZPM_ORDER_CNF_EXPORT zpm_order_cnf_export = new ZPM_ORDER_CNF_EXPORT();
		JCoTable output_table = function.getTableParameterList().getTable("T_COMP");

		zpm_order_cnf_export
				.setZpm_order_cnf_t_comp((List<ZPM_ORDER_CNF_T_COMP>) JCOReflection
						.exportJCOTable(output_table, ZPM_ORDER_CNF_T_COMP.class));
		
		JCoParameterList exportParam = function.getExportParameterList();
		
		zpm_order_cnf_export.setE_RMZHL(exportParam.getValue("E_RMZHL").toString());
		zpm_order_cnf_export.setE_AUFNR(exportParam.getValue("E_AUFNR").toString());
		zpm_order_cnf_export.setE_MESSAGE(exportParam.getValue("E_MESSAGE").toString());
		zpm_order_cnf_export.setE_RESULT(exportParam.getValue("E_RESULT").toString());
		
		return zpm_order_cnf_export;
	}

	/**
	 * SAP 작업확정취소
	 *
	 * @author 김영환
	 * @since 2018.11.20
	 * @param ZPM_CNF_CANCEL_IMPORT zpm_cnf_cancel_import
	 * @return ZPM_CNF_CANCEL_EXPORT
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws Exception
	 */
	@Override
	public ZPM_CNF_CANCEL_EXPORT setOrderCancelInfo(ZPM_CNF_CANCEL_IMPORT zpm_cnf_cancel_import) throws IllegalArgumentException, IllegalAccessException,InstantiationException {
		logger.info("SAP 작업확정취소 - 시작");

		RfcManager.loadProvider();
		
		JCoFunction function = RfcManager.getFunction("ZPM_CNF_CANCEL");
		
		JCoParameterList importParam = function.getImportParameterList();
		JCOReflection.importJCOParam(importParam, zpm_cnf_cancel_import);
		
		JCoTable input_t_work = function.getTableParameterList().getTable("T_CNF");
		JCOReflection.importJCOTable(input_t_work, zpm_cnf_cancel_import.getT_cnf());
		
		RfcManager.execute(function);
		
		JCoTable output_table = function.getTableParameterList().getTable("T_CNF");

		ZPM_CNF_CANCEL_EXPORT zpm_cnf_cancel_export = new ZPM_CNF_CANCEL_EXPORT();
		
		zpm_cnf_cancel_export
				.setT_cnf((List<ZPM_CNF_CANCEL_T_CNF>) JCOReflection
						.exportJCOTable(output_table, ZPM_CNF_CANCEL_T_CNF.class));
		
		JCoParameterList exportParam = function.getExportParameterList();
		zpm_cnf_cancel_export.setE_MESSAGE(exportParam.getValue("E_MESSAGE").toString());
		zpm_cnf_cancel_export.setE_RESULT(exportParam.getValue("E_RESULT").toString());
		
		return zpm_cnf_cancel_export;
	}

	/**
	 * SAP 예방보전오더정보
	 *
	 * @author 김영환
	 * @since 2019.02.19
	 * @param ZPM_PREORDER_IMPORT zpm_preorder_import
	 * @return ZPM_PREORDER_IMPORT
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException
	 * @throws InstantiationException 
	 */	
	@Override
	public ZPM_PREORDER_EXPORT getPreventiveOrderInfo(ZPM_PREORDER_IMPORT zpm_preorder_import) throws IllegalArgumentException, IllegalAccessException, InstantiationException {
		
		logger.info("SAP 예방보전오더 정보 가져오기 - 시작");
		RfcManager.loadProvider();
		
		JCoFunction function = RfcManager.getFunction("ZPM_PREORDER");
		JCOReflection.importJCOParam(function.getImportParameterList(),	zpm_preorder_import);

		RfcManager.execute(function);

		ZPM_PREORDER_EXPORT zpm_preorder_export = new ZPM_PREORDER_EXPORT();

		JCoTable output_table = function.getTableParameterList().getTable("T_AFKO");

		zpm_preorder_export
				.setZpm_preorder_t_afko((List<ZPM_PREORDER_T_AFKO>) JCOReflection
						.exportJCOTable(output_table, ZPM_PREORDER_T_AFKO.class));

		JCoParameterList exportParam = function.getExportParameterList();
		zpm_preorder_export.setE_RESULT(exportParam.getValue("E_RESULT").toString());
		zpm_preorder_export.setE_MESSAGE(exportParam.getValue("E_MESSAGE").toString());
		
		logger.info("SAP 예방보전오더 정보 가져오기 - 끝 " );

		return zpm_preorder_export;
		
	}
	
	/**
	 * SAP 자재 재고 정보 조회
	 *
	 * @author 김영환
	 * @since 2019.02.14
	 * @param ZPM_MATERIAL_IMPORT zpm_material_import
	 * @return ZPM_MATERIAL_EXPORT
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	@Override
	public ZPM_STOCK_EXPORT getMaterialStockInfo(ZPM_STOCK_IMPORT zpm_stock_import) throws IllegalArgumentException, IllegalAccessException,InstantiationException {
		
		logger.info("SAP 자재재고정보 가져오기 - 시작");

		RfcManager.loadProvider();
		JCoFunction function = RfcManager.getFunction("ZPM_STOCK");
		JCOReflection.importJCOParam(function.getImportParameterList(),	zpm_stock_import);
		RfcManager.execute(function);

		ZPM_STOCK_EXPORT zpm_stock_export = new ZPM_STOCK_EXPORT();

		JCoTable table = function.getTableParameterList().getTable("T_MARD");

		zpm_stock_export
				.setZpm_stock_t_mard((List<ZPM_STOCK_T_MARD>) JCOReflection
						.exportJCOTable(table, ZPM_STOCK_T_MARD.class));
		
		JCoParameterList exportParam = function.getExportParameterList();
		zpm_stock_export.setE_result(exportParam.getValue("E_RESULT").toString());
		zpm_stock_export.setE_MESSAGE(exportParam.getValue("E_MESSAGE").toString());
		
		logger.info("SAP 자재재고정보  가져오기 - 끝 " );

		return zpm_stock_export;
	}

	/**
	 * SAP 유통기한 및 재고수량 정보
	 *
	 * @author 김영환
	 * @since 2019.04.02
	 * @param ZMM_SELL_BY_DATE_IMPORT zmm_sell_by_date_import
	 * @return ZMM_SELL_BY_DATE_EXPORT
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */	
	@Override
	public ZMM_SELL_BY_DATE_EXPORT getReportMaterialStockInfo(ZMM_SELL_BY_DATE_IMPORT zmm_sell_by_date_import)
			throws IllegalArgumentException, IllegalAccessException, InstantiationException {
		
		logger.info("SAP 유통기한 및 재고수량 가져오기 - 시작");

		RfcManager.loadProvider();
		JCoFunction function = RfcManager.getFunction("ZMM_SELL_BY_DATE");
		JCOReflection.importJCOParam(function.getImportParameterList(),	zmm_sell_by_date_import);
		RfcManager.execute(function);

		ZMM_SELL_BY_DATE_EXPORT zmm_sell_by_date_export = new ZMM_SELL_BY_DATE_EXPORT();

		JCoTable table = function.getTableParameterList().getTable("ITAB");

		zmm_sell_by_date_export
				.setZmm_sell_by_date_itab((List<ZMM_SELL_BY_DATE_ITAB>) JCOReflection
						.exportJCOTable(table, ZMM_SELL_BY_DATE_ITAB.class));
		
		JCoParameterList exportParam = function.getExportParameterList();
		zmm_sell_by_date_export.setE_RESULT(exportParam.getValue("RESULT").toString()); 
//		zmm_sell_by_date_export.setE_MESSAGE(exportParam.getValue("E_MESSAGE").toString());
		
		logger.info("SAP 유통기한 및 재고수량 가져오기 - 종료");

		return zmm_sell_by_date_export;
	}
	
	
	
	
}
