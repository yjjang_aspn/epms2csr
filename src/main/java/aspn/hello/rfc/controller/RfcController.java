package aspn.hello.rfc.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * [SAP RFC통신] Controller Class
 * @author 김영환
 * @since 2018.11.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.10		김영환		최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/rfc")
public class RfcController {
	
}
