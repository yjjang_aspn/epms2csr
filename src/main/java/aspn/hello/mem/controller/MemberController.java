package aspn.hello.mem.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.JsonUtil;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.Entity;
import aspn.com.common.util.ObjUtil;
import aspn.com.common.util.SecretUtils;
import aspn.com.common.util.TreeGridUtil;
import aspn.com.common.util.Utility;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.mem.model.MemberVO;
import aspn.hello.mem.service.MemberService;
import aspn.hello.order.client.model.OrderClientVO;

/**
 * [사용자] Controller Class
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/mem/member")
public class MemberController extends ContSupport{
	Logger logger = LoggerFactory.getLogger(this.getClass());	

	/* INSTANCE VAR */
	@Autowired
	MemberService memberService;
	
	@Autowired
	AttachService attachService;
	
	/**
	 * [사용자관리] : 화면호출
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/memberDivList.do")
	public String memberDivList(Model model) {
		
		return "/mem/web/memDiv/memberDivList";
	}
	
	/**
	 * [사용자관리] : [사용자] 그리드 데이터
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/memberData.do")
	@ResponseBody
	public String memberData(MemberVO memberVO, Model model) throws Exception {
		
		memberVO.setCompanyId(getSsCompanyId());
		memberVO.setSsLocation(getSsLocation());
		
		List<HashMap<String, Object>> list = memberService.selectMemberList(memberVO);
		return TreeGridUtil.getGridListData(list);
	}
	
	/**
	 * [사용자관리] : [사용자] 그리드 레이아웃
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/memberLayout.do")
	public String memberLayout(MemberVO memberVO, Model model) throws Exception {
		
		//회사코드
		memberVO.setCompanyId(getSsCompanyId());
		
		//부서 목록
		List<HashMap<String, Object>> divisionList= memberService.selectDivisionOpt(memberVO);
		HashMap<String, Object> divisionOpt  = ObjUtil.list2Map(divisionList, "|");
		model.addAttribute("divisionOpt", divisionOpt);
		
		return "/mem/web/memDiv/memberLayout";
	}
	
	/**
	 * [사용자관리] : [사용자] 그리드 저장
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/memberEdit.do")
	@ResponseBody
	public String memberEdit(MemberVO memberVO, Model model) throws Exception {
		
		try{
			memberVO.setUserId(this.getSsUserId());
			memberService.memberEdit( Utility.getEditDataList(memberVO.getUploadData()), memberVO);
			
			return getSaved();
			
		} catch (Exception e) {
			logger.error("MemberController.memberEdit > " + e.toString());
			e.printStackTrace();
			return getSaveFail();
		}
	}
	
	/**
	 * [사용자관리] : ID 중복 조회 팝업 호출
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popMemberCheck.do")
	public String popMemberCheck(Model model) throws Exception {
		
		return "/mem/web/memDiv/popMemberCheck";
	}
	
	/**
	 * [사용자관리] : [ID 중복 조회 팝업] ID 중복 체크
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/fnAjaxMemberCheckAjax.do")
	@ResponseBody
	public int fnAjaxMemberCheckAjax(MemberVO memberVO, Model model) throws Exception {
		
		int userCnt = 0;
		if(!Utility.isEmpty(memberVO.getForMenu())) {  			//주문 > 고객관리에서 호출된 경우 실행 
			userCnt = memberService.clientCheckAjax(memberVO);
		} else {                                          		//시스템관리 > 사용자관리에서 호출되는 경우 실행
			userCnt = memberService.memberCheckAjax(memberVO);				
		}
		
		return userCnt;
	}
	
	/**
	 * [사용자관리] : 비밀번호 초기화
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/changePwAjax.do")
	@ResponseBody
	public Map<String, String> changePwAjax(MemberVO memberVO, ModelMap model) throws Exception {
		
		Map<String, String> resultMap = new HashMap<String, String>();
		String newPassWord = SecretUtils.encryptSha256(SecretUtils.encryptMd5(memberVO.getUSER_ID()));
		memberVO.setPASSWORD(newPassWord);
		resultMap.put("result", memberService.setPassword(memberVO)+"");
		
		return resultMap; 
	}
	
	
	/**
	 * [사용자권한] : 화면호출
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/memberPrivList.do")
	public String memberPrivList(MemberVO memberVO, Model model) throws Exception {
		
		return "/mem/web/memPriv/memberPrivList";
	}
	
	/**
	 * [사용자권한] : [사용자권한] 그리드 데이터
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/memberPrivData.do")
	public String memberPrivData(MemberVO memberVO, Model model) throws Exception {
		
		try {
			memberVO.setCompanyId(getSsCompanyId());
			memberVO.setSsLocation(getSsLocation());
			
			List<HashMap<String, Object>> memberList= memberService.selectMemberList(memberVO);
			List<HashMap<String, Object>> memberPrivList= memberService.selectMemberPrivList(memberVO);

			model.addAttribute("memberList", memberList);
			model.addAttribute("memberPrivList", memberPrivList);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Member Priv Data select Error !");
		}
		
		return "/mem/web/memPriv/memberPrivData";
	}
	
	/**
	 * [사용자권한] : [사용자권한] 그리드 레이아웃
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/memberPrivLayout.do")
	public String memberPrivLayout(MemberVO memberVO, Model model) throws Exception {
		
		memberVO.setCompanyId(getSsCompanyId());
		
		// 1. 직책권한
		List<HashMap<String, Object>> subAuthList = memberService.selectSubAuthList();
		model.addAttribute("subAuthList", subAuthList);
		
		// 2. 조회권한(공장)
		List<HashMap<String, Object>> locationList = memberService.selectLocationList(memberVO);
		model.addAttribute("locationList", locationList);
		
		// 3. 기능권한
		List<HashMap<String, Object>> funcAuthList = memberService.selectFuncAuthList();
		model.addAttribute("funcAuthList", funcAuthList);
	
		return "/mem/web/memPriv/memberPrivLayout";
	}
	
	/**
	 * [사용자권한] : [사용자권한] 그리드 저장
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/memberPrivEdit.do")
	@ResponseBody
	public String memberPrivEdit(MemberVO memberVO, Model model) throws Exception {
		
		try {
			memberVO.setUserId(this.getSsUserId());
			memberService.memberPrivEdit( Utility.getEditDataList(memberVO.getUploadData()), memberVO);
			
			return getSaved();
			
		} catch(Exception e) {
			logger.error("MemberController.memberPrivEdit > " + e.toString());
			e.printStackTrace();
			return getSaveFail();
		}
	}
	
	/**
	 * [사용자권한] : 공장파트관리(파트구분,파트통합) 팝업 호출
	 * 
	 * @author 김영환
	 * @since 2018.04.25 
	 * @param MemberVO memberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popLocationPartUnion.do")
	public String popLocationPartUnion(MemberVO memberVO, Model model) throws Exception {
		
		try {
			
			memberVO.setCompanyId(getSsCompanyId());
			
			//공장,파트관리 
			List<HashMap<String, Object>> list= memberService.selectLocationPartUnion(memberVO);
			model.addAttribute("list", list);	
			
		} catch (Exception e) {
			
			e.printStackTrace();
			logger.error("MemberController.popLocationPartUnion Error !" + e.toString());
		}
		
		return "/mem/web/memPriv/popLocationPartUnion";
	}
	
	/**
	 * [사용자권한] : [공장파트관리(파트구분,파트통합) 팝업] 정보 변경
	 * 
	 * @author 김영환
	 * @since 2018.04.25 
	 * @param MemberVO memberVO
	 * @return int
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateLocationPartUnion.do")
	@ResponseBody
	public int updateLocationPartUnion(MemberVO memberVO) throws Exception {
		
		int rtnVal = 0;
		
		try {
			rtnVal = memberService.updateLocationPartUnion(memberVO);
		}catch (Exception e) {
			
			e.printStackTrace();
			logger.error("MemberController.updateLocationPartUnion Error !" + e.toString());
		}
		
		return rtnVal;
	}
	
	/**
	 * [헤더메뉴] : 출퇴근 변경
	 * 
	 * @author 김영환
	 * @since 2018.05.18
	 * @param MemberVO memberVO
	 * @param HttpSession session
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkInOutAjax.do")
	@ResponseBody
	public Map<String, Object> onoffOfficeAjax(MemberVO memberVO, HttpSession session) throws Exception {

		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			
			//로그인 상태가 아닐 경우			
			if(getSsUserId().isEmpty()) {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
				return resultMap;
			}
			//출퇴근 변경할 아이디
			memberVO.setUSER_ID(memberVO.getUSER_ID().isEmpty()? getSsUserId() : memberVO.getUSER_ID());
			
			//등록자 아이디
			memberVO.setUserId(getSsUserId());
			
			//출퇴근 상태 업데이트
			memberService.updateCheckInOut(memberVO);
			
			//출퇴근 이력 관리
			memberService.insertCheckInOut(memberVO);
			
			//출퇴근 상태값만 세션에 재세팅
			session.setAttribute("ssCheckInout", memberVO.getCHECK_INOUT());

			//성공시 메시지(onOffStat - 1: 출근처리 메시지, 2: 퇴근처리 메시지)
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * [헤더메뉴] : 출퇴근이력 삭제
	 * 
	 * @author 김영환
	 * @since 2019.03.20
	 * @param MemberVO memberVO
	 * @param HttpSession session
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkInOutListDelAjax.do")
	@ResponseBody
	public Map<String, Object> checkInOutListDelAjax(MemberVO memberVO, HttpSession session) throws Exception {
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			
			//로그인 상태가 아닐 경우			
			if(getSsUserId().isEmpty()) {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
				return resultMap;
			}
						
			//등록자 아이디
			memberVO.setUserId(getSsUserId());
			
			//출퇴근 이력삭제 
			memberService.updateCheckInOutList(memberVO);
						
			//성공시 메시지(onOffStat - 1: 출근처리 메시지, 2: 퇴근처리 메시지)
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * [헤더메뉴] : 출퇴근  현황 리스트 팝업
	 * 
	 * @author 김영환
	 * @since 2018.05.18
	 * @param MemberVO memberVO
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popWorkerStatList.do")
	public String popWorkerStatList(MemberVO memberVO, Model model) throws Exception {
		model.addAttribute("startDt", DateTime.requestMmDay("1","yyyy-MM-dd"));
		model.addAttribute("endDt", DateTime.getDateString());
		return "/mem/web/popWorkerStatList"; 
	}
	
	/**
	 * [헤더메뉴] : 출퇴근  현황 리스트  Ajax
	 * 
	 * @author 김영환
	 * @since 2018.05.18
	 * @param MemberVO
	 * @return resultMap
	 * @throws Exception
	 */
	@RequestMapping(value = "/popWorkerStatListAjax.do")
	@ResponseBody
	public Map<String, Object> popWorkerStatListAjax( MemberVO memberVO) throws Exception {		
		Map<String, Object> resultMap = new HashMap<>();
		try {
			//로그인 상태가 아닐 경우			
			if(getSsUserId().isEmpty()) {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
				return resultMap;
			}

			memberVO.setCompanyId(getSsCompanyId());
			memberVO.setSsLocation(getSsLocation());
			memberVO.setSubAuth(getSsSubAuth());
			List<HashMap<String, Object>> list = memberService.selectWorkerStatList(memberVO);

			resultMap.put("auth", getSsSubAuth());
			resultMap.put("list", list);
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);

		} catch (Exception e) {
			e.printStackTrace();
			JsonUtil.errorParam(resultMap, CodeConstants.SYSTEM_ERROR);
		}
		return resultMap;
	}
	

	/**
	 * [헤더메뉴] : 계정 관리 팝업 호출
	 * 
	 * @author 김영환
	 * @since 2018.05.21
	 * @param MemberVO memberVO
	 * @param Model model
	 * @param String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popProfile.do")
	public String popProfile(MemberVO memberVO, Model model) throws Exception {

		HashMap<String, Object> memberInfo = new HashMap<String, Object>();
		
		memberVO.setDIVISION(getSsDivision());
		memberVO.setUSER_ID(getSsUserId());
		memberVO.setCOMPANY_ID(getSsCompanyId());
		
		memberInfo = memberService.getUserInfo(memberVO);
		model.addAttribute("memberInfo", memberInfo);
		model.addAttribute("timeStamp", DateTime.getTimeStampString());
		
		return "/mem/web/memInfo/popProfile";
	}
	
	/**
	 * [헤더메뉴] : [계정관리팝업] 계정 정보 수정
	 * 
	 * @author 김영환
	 * @since 2018.05.21
	 * @param HttpServletRequest request
	 * @param MemberVO memberVO
	 * @param HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateProfileAjax.do")
	@ResponseBody
	public HashMap<String, Object> editProfileAjax(HttpServletRequest request, MultipartHttpServletRequest mRequest, MemberVO memberVO) throws Exception {
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		try {
			
			Entity entity = new Entity(request);
			AttachVO attachVO = new AttachVO();
			
			// ATTACH_GRP_NO가 기존에 존재하지 않았으며, 수정후에는 이미지가 등록되는 경우
			 if("NY".equals(memberVO.getAttachExistYn())){			
				 String attachGrpNo = attachService.selectAttachGrpNo();
				 attachVO.setATTACH_GRP_NO(attachGrpNo);
				 memberVO.setATTACH_GRP_NO(attachGrpNo);
			 }else{
				 attachVO.setATTACH_GRP_NO(memberVO.getATTACH_GRP_NO());
			 }
			 attachVO.setDEL_SEQ(entity.getString("DEL_SEQ"));
			 attachVO.setMODULE(entity.getString("MODULE"));
			 
			 attachService.AttachEdit(request, mRequest, attachVO); 
			 
			 memberVO.setUserId(getSsUserId());
			 memberService.updateProfile(memberVO);
			 resultMap.put("resultCd", "T");
			
		} catch (Exception e) {
			e.printStackTrace();
			resultMap.put("resultCd", "F");
		}

		return resultMap;
	}
	
	/**
	 * [헤더메뉴] :  출퇴근 이력 팝업호출
	 * 
	 * @author 김영환
	 * @since 2018.05.18
	 * @param MemberVO memberVO
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popCheckInOutList.do")
	public String popCheckInOutList(MemberVO memberVO, Model model) throws Exception {
		
		try {
			memberVO.setUSER_ID(memberVO.getUSER_ID().isEmpty()? getSsUserId() : memberVO.getUSER_ID());
			
			//출퇴근 이력 조회
			List<HashMap<String, Object>> list = memberService.selectCheckInOutList(memberVO);
			model.addAttribute("list", list);
			model.addAttribute("startDt", DateTime.requestMmDay("1","yyyy-MM-dd"));
			model.addAttribute("endDt", DateTime.getDateString());
			model.addAttribute("userId", memberVO.getUSER_ID());

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "/mem/web/popCheckInOutList";
	}
	
	/**
	 * [헤더메뉴] :  출퇴근 이력 팝업호출
	 * 
	 * @author 김영환
	 * @since 2018.05.18
	 * @param MemberVO memberVO
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popCheckInOutListAjax.do")
	@ResponseBody
	public HashMap<String, Object> popCheckInOutListAjax(HttpServletRequest request, MemberVO memberVO) throws Exception {	
		
		HashMap<String, Object> resultMap = new HashMap<>();
		
		try {
			memberVO.setUSER_ID(memberVO.getUSER_ID().isEmpty()? getSsUserId() : memberVO.getUSER_ID());
			
			//출퇴근 이력 조회
			List<HashMap<String, Object>> list = memberService.selectCheckInOutList(memberVO);
			resultMap.put("CheckInOutList", list);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resultMap;
	}
	
	
	//////////////////////////
	// 사용하는지 검토 필요
	//////////////////////////	
	
	/**
	 * 부서별 사원정보 목록 그리드 데이터 조회
	 * @author 정순주
	 * @since 2017. 8. 14.
	 * @param session
	 * @param model
	 * @param memberVO
	 * @return
	 * @throws Exception
	 * @return String
	 */
	@RequestMapping(value = "/popDivisionMemberData.do")
	@ResponseBody
	public String popDivisionMemberData(HttpSession session, Model model, MemberVO memberVO) throws Exception {
		memberVO.setDIVISION(getSsDivision());
		memberVO.setUSER_ID(getSsUserId());
		List<HashMap<String, Object>> list= memberService.popDivisionMemberData(memberVO);
		return TreeGridUtil.getGridListData(list);
	}
	
	/**
	 * 부서별 직원정보 목록 그리드 레이아웃
	 * @author 정순주
	 * @since 2017. 8. 14.
	 * @param model
	 * @return
	 * @throws Exception
	 * @return String
	 */
	@RequestMapping(value = "/popDivisionMemberLayout.do")
	public String popDivisionMemberLayout(Model model) throws Exception {
		return "/mem/web/memDiv/popDivisionMemberLayout";
	}
	
	@RequestMapping(value = "/getmemberDataList.do")
	public String getmemberDataList(HttpServletRequest request, Model model, MemberVO memberVO) throws Exception {
		
		memberVO.setCompanyId(getSsCompanyId());
		memberVO.setSsLocation(getSsLocation());
		
		List<HashMap<String, Object>> list= memberService.selectMemberList(memberVO);
		return "/sys/member/admin/memberData";
	}
	
	/**
	 * 사원정보 조회 Pop-Up(예:주문>고객관리>내부담당자명 선택 시 사용)
	 * @author JHY
	 * @since 2016.09.12
	 * @param memberVO
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping(value = "/popSearchMemberList.do")
	public String popSearchMemberList(Model model, OrderClientVO orderClientVO, HttpSession session) throws Exception {
		
		model.addAttribute("subAuth",   getSsSubAuth());
		model.addAttribute("userId", getSsUserId());
		model.addAttribute("gridId", orderClientVO.getGRID_ID());
		model.addAttribute("colId",  orderClientVO.getCOL_ID());
		
		return "member/web/popSearchMemberList"; 
	}
	
	/**
	 * 사원정보 조회 Pop-Up 레이아웃(예:주문>고객관리>내부담당자명 선택 시 사용)
	 * @author JHY
	 * @since 2016.09.12
	 * @param memberVO
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping(value = "/popSearchMemberLayout.do")
	public String popSearchMemberLayout(HttpSession session, MemberVO memberVO, Model model) throws Exception {
		
		return "/sys/member/admin/popSearchMemberLayout";
	}
	
	/**
	 * 사원정보 조회 Pop-Up 데이터(예:주문>고객관리>내부담당자명 선택 시 사용)
	 * @author JHY
	 * @since 2016.09.12
	 * @param memberVO
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping(value = "/popSearchMemberData.do")
	@ResponseBody
	public String popSearchMemberData(HttpServletRequest request, MemberVO memberVO, Model model) throws Exception {
			
		List<HashMap<String, Object>> list = memberService.popMemberList(memberVO);
		return TreeGridUtil.getGridListData(list);
	}
}
