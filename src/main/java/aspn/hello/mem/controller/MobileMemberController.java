package aspn.hello.mem.controller;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.JsonUtil;
import aspn.com.common.util.ObjUtil;
import aspn.com.common.util.TreeGridUtil;
import aspn.com.common.util.Utility;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.mem.model.MemberVO;
import aspn.hello.mem.service.MemberService;
import aspn.hello.order.client.model.OrderClientVO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 사용자를 관리하기 위한 컨트롤러 클래스
 * @author 이영탁
 * @since 2017.07.26
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.07.26  이영탁          최초 생성
 *   2018.05.23  김영환          출퇴근 기능 추가
*
 * </pre>
 */

@Controller
@RequestMapping("/mobile/mem/member")
public class MobileMemberController extends ContSupport{
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private final
	MemberService memberService;
	
	private final
	AttachService attachService;

	@Autowired
	public MobileMemberController(MemberService memberService, AttachService attachService) {
		this.memberService = memberService;
		this.attachService = attachService;
	}

	/**
	 * @subject 유저 부서 리스트
	 * @content 부서별 항목 및 유저 항목을 표현하는 페이지
	 * @author 최규연
	 * @since 2014.12.16
	 * @return 
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/memberDivList.do")
	public String memberDivList(MemberVO member,Model model) {
		
		return "/mem/web/memDiv/memberDivList";
	}
	
	/**
	 * @subject 사용자 Grid Layout
	 * @content 그리드의 레이아웃 페이지
	 * @author 최규연
	 * @since 2014.12.17
	 * @return 
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/memberLayout.do")
	public String memberLayout(MemberVO member, Model model) throws Exception {
		return "/mem/web/memDiv/memberLayout";
	}
	
	/**
	 * @subject 사용자 Grid Data
	 * 	@content 그리드의 Data 페이지
	 * @author 최규연
	 * @since 2014.12.17
	 * @return String xml
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/memberData.do")
	@ResponseBody
	public String memberData(HttpServletRequest request, Model model, MemberVO memberVO, HttpSession session) throws Exception {
		
		/* 사용자 권한 처리 */
		String arr_auth[] = {"08"};
		for(int i=0; i<arr_auth.length; i++){
			if(getSsSubAuth().indexOf(arr_auth[i]) > -1){
				String showYn = "Y";
				memberVO.setShowYn(showYn);	
			}	
		}
		
		memberVO.setCompanyId(getSsCompanyId());
		memberVO.setSsLocation(getSsLocation());
		
		List<HashMap<String, Object>> list = memberService.selectMemberList(memberVO);
		return TreeGridUtil.getGridListData(list);
	}
	
	
	/**
	 * @subject 사용자 Grid Edit
	 * 	@content 그리드의 Data 입력, 수정, 삭제
	 * @author 최규연
	 * @since 2014.12.18
	 * @return 
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/admin/memberEdit.do")
	@ResponseBody
	public String memberEdit(Model model, MemberVO memberVO) throws Exception {
		try{
			memberService.memberEdit( Utility.getEditDataList(memberVO.getUploadData()) , memberVO  );
			return getSaved();
		}catch(Exception e){
			return getSaveFail();
		}
	}
	
	/**
	 * @subject 유저 중복 조회 팝업
	 * 	@content 유저 중복 조회 팝업생성
	 * @author 최규연
	 * @since 2014.12.16
	 * @return 
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/popMemberCheck.do")
	public String popMemberCheck(Model model) throws Exception {
		return "/mem/web/memDiv/popMemberCheck";
	}
	
	
	@RequestMapping(value = "/fnAjaxMemberCheckAjax.do")
	@ResponseBody
	public int fnAjaxMemberCheckAjax(Model model, MemberVO memberVO) throws Exception {
		int userCnt = 0;
		if(!Utility.isEmpty(memberVO.getForMenu())) {  //주문 > 고객관리에서 호출된 경우 실행 
			userCnt = memberService.clientCheckAjax(memberVO);
		} else {                                          //헬로컴퍼니 > 직원정보 > 사용자부서에서 호출되는 경우 실행
			userCnt = memberService.memberCheckAjax(memberVO);				
		}
		return userCnt;
	}
	
	/**
	 * @subject 사용자 권한 
	 * @content 부서별 사용자의 권한을 표현하는 페이지
	 * @author 최규연
	 * @since 2014.12.22
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping(value = "/memberPrivList.do")
	public String memberPrivList(MemberVO memberVO, Model model) throws Exception {
		model.addAttribute("menu", memberVO.getForMenu());
		return "/mem/web/memPriv/memberPrivList";
	}

	/**
	 * @subject 사용자 권한 Grid Layout
	 * 	@content 사용자 권한 그리드의 레이아웃 페이지
	 * @author 최규연
	 * @since 2014.12.22
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping(value = "/memberPrivLayout.do")
	public String memberPrivLayout(MemberVO memberVO, Model model) throws Exception {
		
		List<HashMap<String, Object>> subAuthList= memberService.selectSubAuthList();
		model.addAttribute("subAuthList", subAuthList);
		
		List<HashMap<String, Object>> locationList = memberService.selectLocationList(memberVO);
		model.addAttribute("locationList", locationList);
	
		return "/mem/web/memPriv/memberPrivLayout";
	}
	
	/**
	 * @subject 사용자 권한 Grid Data
	 * 	@content 사용자 권한 Data 페이지
	 * @author 최규연
	 * @since 2014.12.22
	 * @return list
	 * @throws Exception
	 */
	@RequestMapping(value = "/memberPrivData.do")
	public String memberPrivData(Model model, MemberVO memberVO) throws Exception {
		
		try {
			
			memberVO.setCompanyId(getSsCompanyId());
			memberVO.setSsLocation(getSsLocation());
			
			List<HashMap<String, Object>> memberList= memberService.selectMemberList(memberVO);
			List<HashMap<String, Object>> memberPrivList= memberService.selectMemberPrivList(memberVO);

			model.addAttribute("memberList", memberList);
			model.addAttribute("memberPrivList", memberPrivList);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Member Priv Data select Error !");
		}
		
		return "/mem/web/memPriv/memberPrivData";
	}
	
	/**
	 * @subject 사용자 권한 수정
	 * 	@content 사용자 권한 Data 입력, 수정, 삭제
	 * @author 최규연
	 * @since 2014.12.22
	 * @return 
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/memberPrivEdit.do")
	@ResponseBody
	public String memberPrivEdit(Model model, MemberVO memberVO) throws Exception {
		try{
			memberService.memberPrivEdit( Utility.getEditDataList(memberVO.getUploadData()), memberVO);
			return getSaved();
		}catch(Exception e){
			return getSaveFail();
		}
	}
	
	@RequestMapping(value = "/changePwAjax.do")
	@ResponseBody
	public Map<String, String> changePwAjax( MemberVO memberVO, ModelMap model, HttpSession session) throws Exception {
		Map<String, String> resultMap = new HashMap<String, String>();
		String newPassWord =  Utility.md5Convert(memberVO.getUserId());
		memberVO.setPASSWORD(newPassWord);
		resultMap.put("result", memberService.setPassword(memberVO)+"");
		return resultMap; 
	}
	
	@RequestMapping(value = "/memberPrivUserLayout.do")
	public String memberPrivUserLayout(Model model) throws Exception {
		return "/member/web/memberPrivUserLayout";
	}
	
	/**
	 * 부서별 사원정보 목록 그리드 데이터 조회
	 * @author 정순주
	 * @since 2017. 8. 14.
	 * @param session
	 * @param model
	 * @param memberVO
	 * @return
	 * @throws Exception
	 * @return String
	 */
	@RequestMapping(value = "/popDivisionMemberData.do")
	@ResponseBody
	public String popDivisionMemberData(HttpSession session, Model model, MemberVO memberVO) throws Exception {
		memberVO.setDIVISION(getSsDivision());
		memberVO.setUSER_ID(getSsUserId());
		List<HashMap<String, Object>> list= memberService.popDivisionMemberData(memberVO);
		return TreeGridUtil.getGridListData(list);
	}
	
	/**
	 * 부서별 직원정보 목록 그리드 레이아웃
	 * @author 정순주
	 * @since 2017. 8. 14.
	 * @param model
	 * @return
	 * @throws Exception
	 * @return String
	 */
	@RequestMapping(value = "/popDivisionMemberLayout.do")
	public String popDivisionMemberLayout(Model model) throws Exception {
		return "/mem/web/memDiv/popDivisionMemberLayout";
	}
	
	/**
	 * 계정 관리 팝업
	 * @author 정호윤
	 * @since 2017. 09. 12.
	 * @param memberVO
	 * @param String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popProfile.do")
	public String popProfile(HttpSession session, Model model, MemberVO memberVO) throws Exception {
		HashMap<String, Object> memberInfo = new HashMap<String, Object>();
		memberVO.setDIVISION(getSsDivision());
		memberVO.setUSER_ID(getSsUserId());
		memberVO.setCOMPANY_ID(getSsCompanyId());
		
		memberInfo = memberService.getUserInfo(memberVO);
		model.addAttribute("memberInfo", memberInfo);
		
		// 직인 파일이 있을 때
		if(memberInfo.containsKey("SEAL_ATTACH_GRP_NO")){
			AttachVO attachVO = new AttachVO();
			attachVO.setATTACH_GRP_NO(memberInfo.get("SEAL_ATTACH_GRP_NO").toString());
			List<HashMap<String, Object>> attachImgList = attachService.selectFileList(attachVO);
			
			model.addAttribute("attachImgList", attachImgList);
		}
		
		return "/mem/web/memInfo/popProfile";
	}
	
	/**
	 * 계정 정보 수정
	 * @author 정호윤
	 * @since 2017. 09. 12.
	 * @param memberVO
	 * @param HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateProfileAjax.do")
	@ResponseBody
	public HashMap<String, Object> updateProfileAjax(HttpSession session, HttpServletRequest request, MultipartHttpServletRequest mRequest, MemberVO memberVO)	throws Exception {
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		try {
			/* 직인 첨부파일 */
			if ("Y".equals(request.getParameter("sealAttachExistYn")) || !ObjUtil.isEmpty(request.getParameter("DEL_IMG_SEQ"))) {
				String attachGrpNo = memberVO.getSEAL_ATTACH_GRP_NO();
				// 파일 그룹 번호 세팅
				if(ObjUtil.isEmpty(memberVO.getSEAL_ATTACH_GRP_NO())){
					attachGrpNo = attachService.selectAttachGrpNo();
					memberVO.setSEAL_ATTACH_GRP_NO(attachGrpNo);
				}
				
				AttachVO attachVO = new AttachVO();
				attachVO.setDEL_SEQ(request.getParameter("DEL_IMG_SEQ"));
				attachVO.setCD_FILE_TYPE("1");
				attachVO.setATTACH_GRP_NO(attachGrpNo);
				attachVO.setATTACH("1");
				attachService.setFileName("imgFileList");
				attachService.AttachEdit(request, mRequest, attachVO);
			}
			
			if(!ObjUtil.isEmpty(request.getParameter("DEL_IMG_SEQ"))){
				memberVO.setSEAL_ATTACH_GRP_NO("");
			}
			
			memberService.updateProfile(memberVO);
			resultMap.put("resultCd", "T");
		} catch (Exception e) {
			e.printStackTrace();
			resultMap.put("resultCd", "F");
		}

		return resultMap;
	}
	
	@RequestMapping(value = "/getmemberDataList.do")
	public String getmemberDataList(HttpServletRequest request, Model model, MemberVO memberVO) throws Exception {
		
		memberVO.setCompanyId(getSsCompanyId());
		memberVO.setSsLocation(getSsLocation());
		
		List<HashMap<String, Object>> list= memberService.selectMemberList(memberVO);
		return "/sys/member/admin/memberData";
	}
	
	/**
	 * 사원정보 조회 Pop-Up(예:주문>고객관리>내부담당자명 선택 시 사용)
	 * @author JHY
	 * @since 2016.09.12
	 * @param memberVO
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping(value = "/popSearchMemberList.do")
	public String popSearchMemberList(Model model, OrderClientVO orderClientVO, HttpSession session) throws Exception {
		
		model.addAttribute("subAuth",   getSsSubAuth());
		model.addAttribute("userId", getSsUserId());
		model.addAttribute("gridId", orderClientVO.getGRID_ID());
		model.addAttribute("colId",  orderClientVO.getCOL_ID());
		
		return "member/web/popSearchMemberList"; 
	}
	
	/**
	 * 사원정보 조회 Pop-Up 레이아웃(예:주문>고객관리>내부담당자명 선택 시 사용)
	 * @author JHY
	 * @since 2016.09.12
	 * @param memberVO
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping(value = "/popSearchMemberLayout.do")
	public String popSearchMemberLayout(HttpSession session, MemberVO memberVO, Model model) throws Exception {
		
		return "/sys/member/admin/popSearchMemberLayout";
	}
	
	/**
	 * 사원정보 조회 Pop-Up 데이터(예:주문>고객관리>내부담당자명 선택 시 사용)
	 * @author JHY
	 * @since 2016.09.12
	 * @param memberVO
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping(value = "/popSearchMemberData.do")
	@ResponseBody
	public String popSearchMemberData(HttpServletRequest request, MemberVO memberVO, Model model) throws Exception {
			
		List<HashMap<String, Object>> list = memberService.popMemberList(memberVO);
		return TreeGridUtil.getGridListData(list);
	}

	
	/**
	 * 출퇴근 변경
	 * 
	 * @author 김영환
	 * @since 2018.05.23
	 * @param MemberVO memberVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateCheckInout.do")
	@ResponseBody
	public Map<String, Object> updateCheckInout(MemberVO memberVO) throws Exception {

		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			
			//로그인 상태가 아닐 경우			
			if(getSsUserId().isEmpty()) {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
				return resultMap;
			}
			
			memberVO.setUSER_ID(memberVO.getUSER_ID().isEmpty()? getSsUserId() : memberVO.getUSER_ID());
			
			//출퇴근 상태 업데이트
			memberService.updateCheckInOut(memberVO);
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * 출퇴근  현황 리스트  Ajax
	 * 
	 * @author 김영환
	 * @since 2018.05.23
	 * @param MemberVO memberVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/workerStatList.do")
	@ResponseBody
	public Map<String, Object> workerStatList( MemberVO memberVO) throws Exception {		
		Map<String, Object> resultMap = new HashMap<>();
		try {
			//로그인 상태가 아닐 경우			
			if(getSsUserId().isEmpty()) {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
				return resultMap;
			}

			memberVO.setCompanyId(getSsCompanyId());
			memberVO.setSsLocation(getSsLocation());
			memberVO.setSubAuth(getSsSubAuth());
			List<HashMap<String, Object>> list = memberService.selectWorkerStatList2(memberVO);
			
			if(list.isEmpty()) 
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.NO_DATA_FOUND);
			else 
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);

			resultMap.put("WORKER_LIST", list);
		} catch (Exception e) {
			e.printStackTrace();
			JsonUtil.errorParam(resultMap, CodeConstants.SYSTEM_ERROR);
		}
		return resultMap;
	}
}
