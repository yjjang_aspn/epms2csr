package aspn.hello.mem.controller;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.TreeGridUtil;
import aspn.com.common.util.Utility;
import aspn.hello.mem.model.DivisionVO;
import aspn.hello.mem.service.DivisionService;
import aspn.hello.order.client.model.OrderClientVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/mem/mobile/division")
public class MobileDivisionController extends ContSupport{
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/* INSTANCE VAR */
	@Autowired
	DivisionService divisionService;
	
	/* INSTANCE VAR */
	
	/**
	 * @subject 부서 Grid Layout
	 * 	@content 그리드의 레이아웃 페이지
	 * @author 최규연
	 * @since 2014.12.16
	 * @return 
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/divisionLayout.do")
	public String divisionLayout(Model model) {
		return "/mem/web/memDiv/divisionLayout";
	}
	
	/**
	 * @subject 부서 Grid Data
	 * 	@content 그리드의 Data 페이지
	 * @author 최규연
	 * @since 2014.12.16
	 * @return list
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/divisionData.do")
	@ResponseBody
	public String divisionData(DivisionVO divisionVO, Model model) throws Exception{
		List<HashMap<String, Object>> list= divisionService.selectDivisionList(divisionVO);
		return TreeGridUtil.getGridTreeData(list);
	}
	
	/**
	 * @subject 부서 Grid Edit
	 * 	@content 그리드의 Data 입력, 수정, 삭제
	 * @author 최규연
	 * @since 2014.12.18
	 * @return 
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/divisionEdit.do")
	@ResponseBody
	public String divisionEdit(HttpSession session, Model model, DivisionVO divisionVO) {
		try{
			divisionVO.setCompanyId(getSsCompanyId());
			divisionService.divisionEdit( Utility.getEditDataList(divisionVO.getUploadData()) , divisionVO  );
			return getSaved();
		}catch(Exception e){
			return getSaveFail();
		}
	}
	
	/**
	 * @subject 부서 Grid Layout
	 * 	@content 그리드의 레이아웃 페이지
	 * @author 최규연
	 * @since 2014.12.16
	 * @return 
	 * @throws Exception
	 */
	
	@RequestMapping(value = "/divisionNotEditLayout.do")
	public String divisionNoneEditLayout(Model model) {
		return "/mem/web/memPriv/divisionNotEditLayout";
	}
	
	
	@RequestMapping(value = "/divisDepartmentData.do")
	@ResponseBody
	public String divisDepartmentData(HttpSession session,DivisionVO divisionVO, Model model) throws Exception{
		
		List<HashMap<String, Object>> list= divisionService.divisDepartmentData();
		return TreeGridUtil.getGridTreeData(list);
	}
	
	/****************************
	//부서정보 조회 Pop-Up
	 * @author JHY
	 * @since  2016.09.12
	 * @throws Exception
	 ****************************/
	@RequestMapping(value = "/popSearchDivisionList.do")
	public String popSearchDivisionList(HttpSession session, DivisionVO divisionMode, Model model, OrderClientVO orderClientVO) throws Exception{

		model.addAttribute("auth",   getSsAuth());
		model.addAttribute("userId", getSsUserId());
		model.addAttribute("gridId", orderClientVO.getGRID_ID());
		model.addAttribute("colId",  orderClientVO.getCOL_ID());
		return "/division/popSearchDivisionList";
	}
	
	/****************************
	//부서정보 조회 Pop-Up Layout
	 * @author JHY
	 * @since  2016.09.12
	 * @throws Exception
	 ****************************/
	@RequestMapping(value = "/popSearchDivisionLayout.do")
	public String popSearchDivisionLayout(HttpServletRequest request, DivisionVO divisionMode, Model model) {		
		try {
			System.out.println("실행");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Pop Search Division List Error !");
		}
		return "/sys/division/popSearchDivisionLayout";
	}
	
	/****************************
	//부서정보 조회 Pop-Up Data
	 * @author JHY
	 * @since  2016.09.12
	 * @throws Exception
	 ****************************/
	@RequestMapping(value = "/popSearchDivisionData.do")
	public String popSearchDivisionData(HttpServletRequest request, DivisionVO divisionMode, Model model) throws Exception{
		List<HashMap<String, Object>> list = divisionService.popDivisionList(divisionMode);
		return TreeGridUtil.getGridListData(list);
	}
}
