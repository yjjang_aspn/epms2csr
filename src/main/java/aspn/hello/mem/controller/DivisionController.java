package aspn.hello.mem.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.TreeGridUtil;
import aspn.com.common.util.Utility;
import aspn.hello.mem.model.DivisionVO;
import aspn.hello.mem.service.DivisionService;
import aspn.hello.order.client.model.OrderClientVO;

/**
 * [부서] Controller Class
 * @author 서정민
 * @since 2018.05.08
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.08		서정민			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/mem/division")
public class DivisionController extends ContSupport{
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/* INSTANCE VAR */
	@Autowired
	DivisionService divisionService;
	
	/**
	 * [부서] 그리드 데이터
	 * 
	 * @author 서정민
	 * @since 2018.05.08
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/divisionData.do")
	@ResponseBody
	public String divisionData(DivisionVO divisionVO, Model model) throws Exception{
		
		divisionVO.setCompanyId(getSsCompanyId());
		List<HashMap<String, Object>> list= divisionService.selectDivisionList(divisionVO);
		return TreeGridUtil.getGridTreeData(list);
	}
	
	/**
	 * [부서] 그리드 레이아웃(수정가능)
	 * 
	 * @author 서정민
	 * @since 2018.05.08
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/divisionLayout.do")
	public String divisionLayout(Model model) {
		
		return "/mem/web/memDiv/divisionLayout";
	}
	
	/**
	 * [부서] 그리드 레이아웃(수정불가)
	 * 
	 * @author 서정민
	 * @since 2018.05.08
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/divisionNotEditLayout.do")
	public String divisionNoneEditLayout(Model model) {
		
		return "/mem/web/memPriv/divisionNotEditLayout";
	}
	
	/**
	 * [부서] 그리드 저장
	 * 
	 * @author 서정민
	 * @since 2018.05.08
	 * @param DivisionVO divisionVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/divisionEdit.do")
	@ResponseBody
	public String divisionEdit(DivisionVO divisionVO, Model model) {
		
		try{
			divisionVO.setUserId(this.getSsUserId());
			divisionService.divisionEdit( Utility.getEditDataList(divisionVO.getUploadData()), divisionVO);
			
			return getSaved();
			
		} catch (Exception e) {
			logger.error("DivisionController.divisionEdit > " + e.toString());
			e.printStackTrace();
			return getSaveFail();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//////////////////////////
	// 사용하는지 검토 필요
	//////////////////////////	
	
	@RequestMapping(value = "/divisDepartmentData.do")
	@ResponseBody
	public String divisDepartmentData(HttpSession session,DivisionVO divisionVO, Model model) throws Exception{
		
		List<HashMap<String, Object>> list= divisionService.divisDepartmentData();
		return TreeGridUtil.getGridTreeData(list);
	}
	
	/****************************
	//부서정보 조회 Pop-Up
	 * @author JHY
	 * @since  2016.09.12
	 * @throws Exception
	 ****************************/
	@RequestMapping(value = "/popSearchDivisionList.do")
	public String popSearchDivisionList(HttpSession session, DivisionVO divisionMode, Model model, OrderClientVO orderClientVO) throws Exception{

		model.addAttribute("auth",   getSsAuth());
		model.addAttribute("userId", getSsUserId());
		model.addAttribute("gridId", orderClientVO.getGRID_ID());
		model.addAttribute("colId",  orderClientVO.getCOL_ID());
		return "/division/popSearchDivisionList";
	}
	
	/****************************
	//부서정보 조회 Pop-Up Layout
	 * @author JHY
	 * @since  2016.09.12
	 * @throws Exception
	 ****************************/
	@RequestMapping(value = "/popSearchDivisionLayout.do")
	public String popSearchDivisionLayout(HttpServletRequest request, DivisionVO divisionMode, Model model) {		
		try {
			System.out.println("실행");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Pop Search Division List Error !");
		}
		return "/sys/division/popSearchDivisionLayout";
	}
	
	/****************************
	//부서정보 조회 Pop-Up Data
	 * @author JHY
	 * @since  2016.09.12
	 * @throws Exception
	 ****************************/
	@RequestMapping(value = "/popSearchDivisionData.do")
	public String popSearchDivisionData(HttpServletRequest request, DivisionVO divisionMode, Model model) throws Exception{
		List<HashMap<String, Object>> list = divisionService.popDivisionList(divisionMode);
		return TreeGridUtil.getGridListData(list);
	}
}
