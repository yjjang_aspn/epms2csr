/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package aspn.hello.mem.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.mem.model.MemberVO;

/**
 * [사용자] Service Class
 * 
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *   
 * </pre>
 */

public interface MemberService {
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 사용자 목록 조회
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectMemberList(MemberVO memberVO) throws Exception;
	
	/**
	 * 부서 목록 조회(Grid combo)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectDivisionOpt(MemberVO memberVO) throws Exception;

	/**
	 * 사용자 그리드 저장
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return void
	 * @throws Exception
	 */
	public void memberEdit(List<HashMap<String, Object>> saveDataList, MemberVO memberVO);
	
	/**
	 * 고객 로그인 ID 중복체크 ajax
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return void
	 * @throws Exception
	 */
	public int clientCheckAjax(MemberVO memberVO);
	
	/**
	 * 사용자 로그인 ID 중복체크 ajax
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return void
	 * @throws Exception
	 */
	public int memberCheckAjax(MemberVO memberVO);
	
	/**
	 * 비밀번호 설정
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return void
	 * @throws Exception
	 */
	public int setPassword(MemberVO memberVO) throws Exception;
	
	
	/**
	 * 사용자권한 목록 조회
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectMemberPrivList(MemberVO memberVO) throws Exception;
	
	/**
	 * 직책 권한 조회(SUB_AUTH)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectSubAuthList() throws Exception;
	
	/**
	 * 조회 권한(공장) 조회(LOCATION)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectLocationList(MemberVO memberVO) throws Exception;
	
	/**
	 * 기능 권한 조회(FUNC_AUTH)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectFuncAuthList() throws Exception;
	
	/**
	 * 사용자권한 그리드 저장
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public void memberPrivEdit(List<HashMap<String, Object>> saveDataList, MemberVO memberVO);
	
	/**
	 * 공장파트관리 목록 조회(파트구분,파트통합)
	 * 
	 * @author 김영환
	 * @since 2018.04.25
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> selectLocationPartUnion(MemberVO memberVO);
	
	/**
	 * 공장파트관리(파트구분,파트통합) 정보 변경
	 * 
	 * @author 김영환
	 * @since 2018.04.25
	 * @param MemberVO memberVO
	 * @return int
	 */
	public int updateLocationPartUnion(MemberVO memberVO);
	
	/**
	 * 출퇴근 상태변경
	 * 
	 * @author 김영환
	 * @since 2018.05.18
	 * @param MemberVO memberVO
	 * @return void
	 */
	public void updateCheckInOut(MemberVO memberVO);
	
	/**
	 * 출퇴근  현황 목록 조회
	 * 
	 * @author 김영환
	 * @since 2018.05.18
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, String>>
	 */
	public List<HashMap<String, Object>> selectWorkerStatList(MemberVO memberVO);
	
	/**
	 * 계정 정보 수정
	 * 
	 * @author 김영환
	 * @since 2018.05.21
	 * @param MemberVO memberVO
	 * @param HashMap<String, Object>
	 * @throws Exception
	 */
	public void updateProfile(MemberVO memberVO) throws Exception;
	
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 모바일 - 사용자 목록 조회
	 * 
	 * @author 서정민
	 * @since 2018.06.05
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectMemberList2(MemberVO memberVO) throws Exception;
	
	/**
	 * 모바일 - 사용자 상세 조회
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param MemberVO memberVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public HashMap<String, Object> getUserInfo2(MemberVO memberVO) throws Exception;
	
	/**
	 * 모바일 - 파트 정보 조회
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> getPartList(MemberVO memberVO) throws Exception;

	/**
	 * 모바일 - 사용자 비밀번호 변경
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param MemberVO memberVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public void changeUserPassword(MemberVO memberVO) throws Exception;
	
	/**
	 * 모바일 - 출퇴근  현황 목록 조회
	 * 
	 * @author 김영환
	 * @since 2018.05.18
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, String>>
	 */
	public List<HashMap<String, Object>> selectWorkerStatList2(MemberVO memberVO);
	
	
	
	
	
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 사용하는지 검토 필요
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 부서별 사원정보 목록 그리드 데이터 조회
	 * @author 정순주
	 * @since 2017. 8. 14.
	 * @param memberVO
	 * @return
	 * @throws Exception
	 * @return List<HashMap<String,Object>>
	 */
	public List<HashMap<String, Object>> popDivisionMemberData(MemberVO memberVO)  throws Exception;

	

	/****************************
	//사원정보 조회 Pop-Up
	 * @author JHY
	 * @since  2016.09.12
	 * @param  memberVO
	 * @throws Exception
	 ****************************/
	public List<HashMap<String, Object>> popMemberList(MemberVO memberVO) throws Exception;
	
	/**
	 * @subject 사용자 권한 수정
	 * 	@content 그리드의 Data 입력, 수정, 삭제
	 * @author 최규연
	 * @since 2014.12.22
	 * @return 
	 * @throws Exception
	 */
	public void memberAttachEdit(MemberVO memberVO)throws Exception;
	
	/**
	 * 계정 관리 팝업 - 사용자 상세 조회
	 * @author 정호윤
	 * @since 2017.09.12
	 * @param memberMd
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public HashMap<String, Object> getUserInfo(MemberVO memberMd) throws Exception;

	/**
	 * 사용자 상세 조회 (Model 객체)
	 * @author 정호윤
	 * @since 2017.09.12
	 * @param memberMd
	 * @return MemberVO
	 * @throws Exception
	 */
	public MemberVO getUserInfoByModel(MemberVO memberVO) throws Exception;
	
	/**
	 * 사용자 프로필 정보 변경
	 *
	 * @author 김영환
	 * @param repairResultVO 
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return HashMap<String, Object>
	 */
	void updateMemberProfileAttach(MemberVO memberVO);

	/**
	 * [헤더메뉴] : 출퇴근 이력조회
	 * 
	 * @author 김영환
	 * @since 2018.12.11
	 * @param MemberVO memberVO
	 * @param HttpSession session
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectCheckInOutList(MemberVO memberVO) throws Exception;

	/**
	 * [헤더메뉴] : 출퇴근 이력 등록
	 * 
	 * @author 김영환
	 * @since 2018.12.11
	 * @param MemberVO memberVO
	 * @param HttpSession session
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	public void insertCheckInOut(MemberVO memberVO);

	/**
	 * [헤더메뉴] : 출퇴근이력 삭제
	 * 
	 * @author 김영환
	 * @since 2018.12.11
	 * @param MemberVO memberVO
	 * @param HttpSession session
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	public void updateCheckInOutList(MemberVO memberVO);
	
}
