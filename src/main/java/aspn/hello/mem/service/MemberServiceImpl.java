/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package aspn.hello.mem.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.SecretUtils;
import aspn.com.common.util.Utility;
import aspn.hello.mem.mapper.MemberMapper;
import aspn.hello.mem.model.MemberVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

/**
 * [사용자] Business Implement Class
 * 
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *   
 * </pre>
 */

@Service("memberService")
public class MemberServiceImpl extends EgovAbstractServiceImpl implements MemberService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MemberServiceImpl.class);
	
	@Autowired
	private MemberMapper memberMapper;
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 사용자 목록 조회
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectMemberList(MemberVO memberVO) throws Exception {
		
		return memberMapper.selectMemberList(memberVO);
	}
	
	/**
	 * 부서 목록 조회(Grid combo)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectDivisionOpt(MemberVO memberVO) throws Exception{
		return memberMapper.selectDivisionOpt(memberVO);
	}
	
	/**
	 * 사용자 그리드 저장
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param MemberVO memberVO
	 * @return void
	 * @throws Exception
	 */
	public void memberEdit(List<HashMap<String, Object>> saveDataList, MemberVO memberVO){
		if(saveDataList != null){
			
			int dupCnt=0;
			int busiCnt = 0;
			
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				
				@SuppressWarnings("unchecked")
				MemberVO memberMd = Utility.toBean(tmpSaveData, memberVO.getClass());
				
				if(tmpSaveData.containsKey("Added")) {
					
					//공백 제거
					memberMd.setUSER_ID(memberMd.getUSER_ID().replaceAll(" ",""));
					memberMd.setEMAIL(memberMd.getEMAIL().replaceAll(" ",""));
					
					//패스워드 암호화(초기값: ID)
					String password = SecretUtils.encryptSha256(SecretUtils.encryptMd5(memberMd.getUSER_ID()));
					memberMd.setPASSWORD(password);
					
					//ID 중복 여부 확인
					dupCnt = memberMapper.getMemberDuplicate(memberMd);
					//ID가 없으면 등록
					if(dupCnt==0){
						memberMapper.memberAdd(memberMd);
					}
					memberMapper.businessAdd(memberMd);
				
				}
				else if (tmpSaveData.containsKey("Changed")) {
					memberMapper.memberChange(memberMd);
					memberMapper.businessChange(memberMd);
				}
				else if(tmpSaveData.containsKey("Deleted")) {
					
					//BUSINESS 카운트 조회
					busiCnt = memberMapper.getBusiCount(memberMd);
					
					if(busiCnt < 2){
						//MEMBER 삭제
						memberMapper.memberDelete(memberMd);
					}
					//BUSINESS 삭제
					memberMapper.businessDelete(memberMd);
				}
			}
		}
	}
	
	/**
	 * 고객 로그인 ID 중복체크 ajax
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return int
	 * @throws Exception
	 */
	public int clientCheckAjax(MemberVO memberVO) {
		return memberMapper.clientCheckAjax(memberVO);
	}
	
	/**
	 * 사용자 로그인 ID 중복체크 ajax
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return int
	 * @throws Exception
	 */
	public int memberCheckAjax(MemberVO memberVO) {
		return memberMapper.memberCheckAjax(memberVO);
	}
	
	/**
	 * 비밀번호 설정
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return int
	 * @throws Exception
	 */
	public int setPassword(MemberVO memberVO) throws Exception {
		return memberMapper.setPassword(memberVO);
	}
	
	
	/**
	 * 사용자권한 목록 조회
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectMemberPrivList(MemberVO memberVO) throws Exception {
		
		return memberMapper.selectMemberPrivList(memberVO);
	}
	
	/**
	 * 직책 권한 조회(SUB_AUTH)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectSubAuthList() throws Exception {
		
		return memberMapper.selectSubAuthList();
	}
	
	/**
	 * 조회 권한(공장) 조회(LOCATION)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectLocationList(MemberVO memberVO) throws Exception {
		
		return memberMapper.selectLocationList(memberVO);
	}
	
	/**
	 * 기능 권한 조회(FUNC_AUTH)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectFuncAuthList() throws Exception {
		
		return memberMapper.selectFuncAuthList();
	}
	
	/**
	 * 사용자권한 그리드 저장
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 */
	public void memberPrivEdit(List<HashMap<String, Object>> saveDataList, MemberVO memberVO){
		if(saveDataList != null){
			
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				
				MemberVO memberVO1 = Utility.toBean(tmpSaveData, memberVO.getClass());

				String tmpAuth  ="";
				if (tmpSaveData.containsKey("Changed")) {
					Iterator<String> iterator = tmpSaveData.keySet().iterator();
					
					while(iterator.hasNext()){

						String key = iterator.next().toString();

						if(tmpSaveData.get(key).equals("1")){
							
							// 1. 직책권한
							if(key.equals(CodeConstants.AUTH_COMMON)){
								tmpAuth = setGubunComma(tmpAuth);
								tmpAuth += "AUTH01";
							}else if(key.equals(CodeConstants.AUTH_CHIEF)){
								tmpAuth = setGubunComma(tmpAuth);
								tmpAuth += "AUTH02";
							}else if(key.equals(CodeConstants.AUTH_REPAIR)){
								tmpAuth = setGubunComma(tmpAuth);
								tmpAuth += "AUTH03";
							}
							
							// 2. 조회권한
							if(key.indexOf("FAC") > -1){
								
								tmpAuth = setGubunComma(tmpAuth);
								tmpAuth += key;
								
							}
							
							// 3. 기능권한
							if(key.indexOf("FUNC") > -1){
								
								if(key.equals(CodeConstants.AUTH_FUNC01)){
									tmpAuth = setGubunComma(tmpAuth);
									tmpAuth += "FUNC01";
								}else if(key.equals(CodeConstants.AUTH_FUNC02)){
									tmpAuth = setGubunComma(tmpAuth);
									tmpAuth += "FUNC02";
								}else if(key.equals(CodeConstants.AUTH_FUNC03)){
									tmpAuth = setGubunComma(tmpAuth);
									tmpAuth += "FUNC03";
								}else if(key.equals(CodeConstants.AUTH_FUNC04)){
									tmpAuth = setGubunComma(tmpAuth);
									tmpAuth += "FUNC04";
								}		
								
							}
							
						}
					}
					
					tmpAuth = setGubunComma(tmpAuth);
					
					memberVO1.setUserId(memberVO.getUserId());
					memberVO1.setSUB_AUTH(tmpAuth);
					//사용자권한 수정(SUBAUTH)
					memberMapper.memberPrivChange(memberVO1);
					
					//계정 생성 이력 추가
					memberMapper.memberAuthHistoryAdd(memberVO1);
				} 
			}
		}
	}
	
	public String setGubunComma(String tmpAuth){
		
		if(!"".equals(tmpAuth)){
		tmpAuth += ",";
		}

		return tmpAuth;
	}
	
	/**
	 * 공장파트관리 목록 조회(파트구분,파트통합)
	 * 
	 * @author 김영환
	 * @since 2018.04.25
	 * @param MemberVO memberVO
	 * @return HashMap<String, Object>
	 */
	public List<HashMap<String, Object>> selectLocationPartUnion(MemberVO memberVO) {
		return memberMapper.selectLocationPartUnion(memberVO);
	}

	/**
	 * 공장파트관리(파트구분,파트통합) 정보변경
	 * 
	 * @author 김영환
	 * @since 2018.04.25
	 * @param MemberVO memberVO
	 * @return int
	 */
	public int updateLocationPartUnion(MemberVO memberVO) {
		int rtnVal = 0;
		String[] comcdArr = memberVO.getComcdArr().split(",");
		String[] remarksArr = memberVO.getRemarksArr().split(",");
		
		for(int i=0; i < comcdArr.length; i++){
			rtnVal = 0;
			memberVO.setCOMCD(comcdArr[i].trim());
			memberVO.setREMARKS(remarksArr[i].trim());
			
			rtnVal = memberMapper.updateLocationPartUnion(memberVO);
		}
		
		return rtnVal;
	}
	
	/**
	 * 출퇴근 상태변경
	 * 
	 * @author 김영환
	 * @since 2018.05.18
	 * @param MemberVO memberVO
	 * @return void
	 */
	public void updateCheckInOut(MemberVO memberVO) {
		memberMapper.updateCheckInOut(memberVO);
	}
	
	/**
	 * 출퇴근  현황 목록 조회
	 * 
	 * @author 김영환
	 * @since 2018.05.18
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, String>>
	 */
	public List<HashMap<String, Object>> selectWorkerStatList(MemberVO memberVO) {
		return memberMapper.selectWorkerStatList(memberVO);
	}
	
	/**
	 * 계정 정보 수정
	 * 
	 * @author 김영환
	 * @since 2018.05.21
	 * @param MemberVO memberVO
	 * @return void
	 * @throws Exception
	 */
	@Override
	public void updateProfile(MemberVO memberVO) throws Exception {
		
//		if(!"".equals(memberVO.getNewPw())){
//			String newPassWord =  Utility.md5Convert(memberVO.getNewPw());
//			memberVO.setPASSWORD(newPassWord);
//			memberMapper.setPassword(memberVO);
//		}
		// 비밀번호 설정
		if(!"".equals(memberVO.getPASSWORD())){
			memberMapper.setPassword(memberVO);
		}
		
		if(!"".equals(memberVO.getATTACH_GRP_NO())){		
			// 프로필사진 파일번호 수정
			memberMapper.updateMemberProfileAttach(memberVO);
		}
		
	}
	
	/**
	 * [헤더메뉴] : 출퇴근 이력조회
	 * 
	 * @author 김영환
	 * @since 2018.12.11
	 * @param MemberVO memberVO
	 * @param HttpSession session
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectCheckInOutList(MemberVO memberVO) throws Exception {
		return memberMapper.selectCheckInOutList(memberVO);
	}
	
	/**
	 * [헤더메뉴] : 출퇴근 이력 등록
	 * 
	 * @author 김영환
	 * @since 2018.12.11
	 * @param MemberVO memberVO
	 * @param HttpSession session
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@Override
	public void insertCheckInOut(MemberVO memberVO) {
		memberMapper.insertCheckInOut(memberVO);
	}

	/**
	 * 사용자 프로필 정보 변경
	 *
	 * @author 김영환
	 * @param repairResultVO 
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return HashMap<String, Object>
	 */
	@Override
	public void updateMemberProfileAttach(MemberVO memberVO) {
		memberMapper.updateMemberProfileAttach(memberVO);
	}
	
	/**
	 * 출퇴근 이력 삭제
	 * @author 김영환
	 * @since 2019.03.20
	 * @param memberMd
	 * @return MemberVO
	 * @throws Exception
	 */
	@Override
	public void updateCheckInOutList(MemberVO memberVO) {
		memberMapper.updateCheckInOutList(memberVO);
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 모바일 - 사용자 목록 조회
	 * 
	 * @author 서정민
	 * @since 2018.06.05
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectMemberList2(MemberVO memberVO) throws Exception {
		
		return memberMapper.selectMemberList2(memberVO);
	}
	
	/**
	 * 모바일 - 사용자 상세 조회
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param MemberVO memberVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public HashMap<String, Object> getUserInfo2(MemberVO memberVO) throws Exception{
		return memberMapper.getUserInfo2(memberVO);
	}
	
	/**
	 * 모바일 - 사용자 상세 조회
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> getPartList(MemberVO memberVO) throws Exception{
		return memberMapper.getPartList(memberVO);
	}
	
	/**
	 * 모바일 - 사용자 비밀번호 변경
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param MemberVO memberVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public void changeUserPassword(MemberVO memberVO) throws Exception {
		memberMapper.setPassword2(memberVO);
	}
	
	
	/**
	 * 모바일 - 출퇴근  현황 목록 조회
	 * 
	 * @author 김영환
	 * @since 2018.05.18
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, String>>
	 */
	public List<HashMap<String, Object>> selectWorkerStatList2(MemberVO memberVO) {
		return memberMapper.selectWorkerStatList2(memberVO);
	}
	
	
	
	
	
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 사용하는지 검토 필요
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 부서별 사원정보 목록 그리드 데이터 조회
	 * @author 정순주
	 * @since 2017. 8. 14.
	 * @param memberModel
	 * @return
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> popDivisionMemberData(MemberVO memberModel) throws Exception {
		
		return memberMapper.popDivisionMemberData(memberModel);
	}

	

	/****************************
	//사원정보 조회 Pop-Up
	 * @author JHY
	 * @since  2016.09.12
	 * @param  memberModel
	 * @throws Exception
	 ****************************/
	@Override
	public List<HashMap<String, Object>> popMemberList(MemberVO memberVO) throws Exception {
		// TODO Auto-generated method stub
		return memberMapper.popMemberList(memberVO);
	}
	
	public void memberAttachEdit(MemberVO memberVO)throws Exception{
		memberMapper.memberAttachEdit(memberVO);
	}
	
	/**
	 * 계정 관리 팝업 - 사용자 상세 조회
	 * @author 정호윤
	 * @since 2017.09.12
	 * @param memberMd
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public HashMap<String, Object> getUserInfo(MemberVO memberVO) throws Exception{
		return memberMapper.getUserInfo(memberVO);
	}
	
	/**
	 * 사용자 상세 조회 (Model 객체)
	 * @author 정호윤
	 * @since 2017.09.12
	 * @param memberMd
	 * @return MemberVO
	 * @throws Exception
	 */
	public MemberVO getUserInfoByModel(MemberVO memberVO) throws Exception{
		return memberMapper.getUserInfoByModel(memberVO);
	}

}
