package aspn.hello.mem.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.mem.model.DivisionVO;

/**
 * [부서] Service Class
 * 
 * @author 서정민
 * @since 2018.05.08
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.08		서정민			최초 생성
 *   
 * </pre>
 */

public interface DivisionService {

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 부서 목록 조회
	 * 
	 * @author 서정민
	 * @since 2018.05.08
	 * @param DivisionVO divisionVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectDivisionList(DivisionVO divisionVO) throws Exception;
	
	/**
	 * 부서 그리드 저장
	 * 
	 * @author 서정민
	 * @since 2018.05.08
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param DivisionVO divisionVO
	 * @return void
	 * @throws Exception
	 */
	public void divisionEdit(List<HashMap<String, Object>> saveDataList, DivisionVO divisionVO) throws Exception;

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - 부서 목록 조회
	 * 
	 * @author 서정민
	 * @since 2018.06.05
	 * @param 
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectDivisionList2(DivisionVO divisionVO) throws Exception;
	
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 사용하는지 검토 필요
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public List<HashMap<String, Object>> divisDepartmentData()  throws Exception;

	/****************************
	//부서정보 조회 Pop-Up
	 * @author JHY
	 * @since  2016.09.12
	 * @throws Exception
	 ****************************/
	public List<HashMap<String, Object>> popDivisionList(DivisionVO divisionMode) throws Exception;
}
