package aspn.hello.mem.service;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.util.Utility;
import aspn.hello.mem.mapper.DivisionMapper;
import aspn.hello.mem.model.DivisionVO;

/**
 * [부서] Business Implement Class
 * 
 * @author 서정민
 * @since 2018.05.08
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.08		서정민			최초 생성
 *   
 * </pre>
 */

@Service("divisionService")
public class DivisionServiceImpl implements DivisionService {
	
	@Autowired
	DivisionMapper divisionMapper;
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 부서 목록 조회
	 * 
	 * @author 서정민
	 * @since 2018.05.08
	 * @param DivisionVO divisionVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectDivisionList(DivisionVO divisionVO) throws Exception {
		return divisionMapper.selectDivisionList(divisionVO);
	}
	
	/**
	 * 부서 그리드 저장
	 * 
	 * @author 서정민
	 * @since 2018.05.08
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return void
	 * @throws Exception
	 */
	public void divisionEdit(List<HashMap<String, Object>> saveDataList, DivisionVO divisionVO) throws Exception {
		
		HashMap<String, String> oParentDivisionMap = new HashMap<String, String>();	//상위부서코드 맵
		HashMap<String, String> oTreeMap = new HashMap<String, String>();			//구조체 맵
		
		if(saveDataList != null){
			
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				
				DivisionVO divisionMd = Utility.toBean(tmpSaveData, divisionVO.getClass());
				divisionMd.setUserId(divisionVO.getUserId());
				
				if(tmpSaveData.containsKey("Added")) {
					
					String SEQ_DIVISION = divisionMapper.selectDivisionSeq();
					divisionMd.setDIVISION(SEQ_DIVISION);
					
					if(divisionMd.getPARENT_DIVISION().isEmpty() && !divisionMd.getDEPTH().equals("1")){
						String index = Integer.toString(Integer.parseInt(divisionMd.getDEPTH())-1);
						divisionMd.setPARENT_DIVISION(oParentDivisionMap.get(index));
						divisionMd.setTREE(oTreeMap.get(index));
					}
					divisionMapper.insertDivision(divisionMd);
					
					//상위부서코드 및 구조체 맵 세팅
					oParentDivisionMap.put(divisionMd.getDEPTH(), SEQ_DIVISION);
					oTreeMap.put(divisionMd.getDEPTH(), divisionMd.getTREE()+"$"+SEQ_DIVISION);
					
				}
				else if (tmpSaveData.containsKey("Changed")) {
					divisionMapper.updateDivision(divisionMd);
				} 
				else if(tmpSaveData.containsKey("Deleted")) {
					divisionMapper.deleteDivision(divisionMd);
				}
			}
		}
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - 부서 목록 조회
	 * 
	 * @author 서정민
	 * @since 2018.06.05
	 * @param 
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectDivisionList2(DivisionVO divisionVO) throws Exception {
		return divisionMapper.selectDivisionList2(divisionVO);
	}
	
	
	
	
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 사용하는지 검토 필요
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public List<HashMap<String, Object>> divisDepartmentData() throws Exception {
		return divisionMapper.divisDepartmentData();
	}

	/****************************
	//부서정보 조회 Pop-Up
	 * @author JHY
	 * @since  2016.09.12
	 * @throws Exception
	 ****************************/
	@Override
	public List<HashMap<String, Object>> popDivisionList(DivisionVO divisionMode) throws Exception {
		// TODO Auto-generated method stub
		return divisionMapper.popDivisionList(divisionMode);
	}
}
