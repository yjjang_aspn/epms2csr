package aspn.hello.mem.model;

import aspn.hello.com.model.AbstractVO;

/**
 * [사용자] VO Class
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *   2018.06.07		김영환			사용자 프로필 이미지 URL 추가
 *   
 * </pre>
 */

public class MemberVO extends AbstractVO{

	private static final long serialVersionUID = 1L;
	
	/** MEMBER DB model  */
	private String USER_ID 			= "";  	//회원아이디
	private String PASSWORD			= "";  	//비밀번호
	private String CATEGORY 		= "";   //카테고리 코드
	private String NAME 			= "";   //이름 
	private String EMAIL 			= "";   //이메일
	private String EMAIL_HEADER 	= "";   //이메일 헤더
	private String CD_COUNTRY 		= "";   //국가
	private String PHONE 			= "";   //휴대폰번호
	private String ZIPCODE 			= "";   //우편번호
	private String ADDR1 			= "";   //주소1
	private String ADDR2			= "";   //주소2
	private String SEX_FM           = "";   //성별
	private String CD_MEMBER_TYPE   = "";   //회원유형
	private String CD_MEMBER_STATUS = "";   //회원상태
	private String AUTH 			= "";   //회원권한
	private String PUSH_YN          = "";   //푸쉬수신여부
	private String CNT_VIEW         = "";   //조회수
	private String CNT_RECOM        = "";   //추천수
	private String DATE_C           = "";   //등록일
	private String DATE_E           = "";   //수정일
	private String CD_DEL           = "";   //삭제여부
	private String DIVISION			= "";	//부서번호
	private String PARENT_DIVISION	= "";	//상위부서번호
	private String JOB_GRADE		= "";	//직급
	private String JOB_POSITION		= "";	//직책
	private String MEMO				= "";	//주요업무
	private String MEMBER_SEQ		= "";	//사용자시퀀스
	private String COMPANY_ID		= "";	//
	private String TELEPHONE		= "";	//사내전화번호
	private String KEYWORD			= "";	//
	private String DIVISION_NM		= "";	//부서명
	private String USER_NM			= "";	//유저명
	private String JOB_GRADE_NM		= "";	//직급명
	private String SUB_AUTH			= "";	//보조권한
	private String KOSTL			= "";	//코스트센터 코드
	private String KTEXT			= "";	//코스트센터 명
	private String WHERE_DIVISION	= "";	
	private String ATTACH_GRP_NO	= "";	//첨부파일
	private String ATTACH			= "";
	private String SEAL_ATTACH_GRP_NO = ""; //직인
	private String CHECK_INOUT		= ""; //출퇴근 01: 출근  , 02: 퇴근
	private String USER_THUMBURL	= ""; //사용자 프로필 이미지 URL

	private String LOCATION			= ""; //위치(공장ID)
	private String PART				= ""; //파트ID
	private String MEMBER_ID		= ""; //사번
	
	/** BUSINESS DB model  */
	private String BUSINESS			= ""; //업무번호
	
	/** DIVISION DB model  */
	private String TREE				= ""; //구조체
	
	/** COMCD DB model  */
	private String REMARKS			= ""; //파트통합구분
	private String COMCD			= ""; //공통코드
	
	/** COMCD DB model  */
	private String TYPE				= ""; //타입
	private String REG_ID			= ""; //등록자
	private String REG_DT			= ""; //등록일시
	private String DEL_YN			= ""; //삭제여부
	
	/** parameter */
	private String ORG_DIVISION		= "";	//부서코드(변경할 경우 기존부서코드)
	private String password			= "";	//password
	private String userGubun		= "";	//사용자구분
	private String userIp			= "";	//사용자ip
	private String resCd			= "";	//응답코드
	private String resMsg			= "";	//응답메시지
	private String encId			= "";
	private String showYn			= "";
	private String forMenu			= "";
	private String newPw			= "";	//신규패스워드
	private String divNo			= "";
	private String curLvl			= "";
	private String logoutYn			= "";
	private String comcdArr			= ""; 	//공통코드목록
	private String remarksArr		= ""; 	//비고목록
	private String failCnt			= "";	//비밀번호 오류횟수
	private String startDt			= "";	//출퇴근 이력 조회 시작일
	private String endDt			= "";	//출퇴근 이력 조회 종료일
	
	
	
	public String getCHECK_INOUT() {
		return CHECK_INOUT;
	}
	public void setCHECK_INOUT(String cHECK_INOUT) {
		CHECK_INOUT = cHECK_INOUT;
	}
	public String getUSER_THUMBURL() {
		return USER_THUMBURL;
	}
	public void setUSER_THUMBURL(String uSER_THUMBURL) {
		USER_THUMBURL = uSER_THUMBURL;
	}
	public String getLogoutYn() {
		return logoutYn;
	}
	public void setLogoutYn(String logoutYn) {
		this.logoutYn = logoutYn;
	}
	public String getATTACH() {
		return ATTACH;
	}
	public void setATTACH(String aTTACH) {
		ATTACH = aTTACH;
	}
	public String getATTACH_GRP_NO() {
		return ATTACH_GRP_NO;
	}
	public void setATTACH_GRP_NO(String aTTACH_GRP_NO) {
		ATTACH_GRP_NO = aTTACH_GRP_NO;
	}
	public String getSEAL_ATTACH_GRP_NO() {
		return SEAL_ATTACH_GRP_NO;
	}
	public void setSEAL_ATTACH_GRP_NO(String sEAL_ATTACH_GRP_NO) {
		SEAL_ATTACH_GRP_NO = sEAL_ATTACH_GRP_NO;
	}
	public String getCurLvl() {
		return curLvl;
	}
	public void setCurLvl(String curLvl) {
		this.curLvl = curLvl;
	}
	public String getDivNo() {
		return divNo;
	}
	public void setDivNo(String divNo) {
		this.divNo = divNo;
	}
	public String getWHERE_DIVISION() {
		return WHERE_DIVISION;
	}
	public void setWHERE_DIVISION(String wHERE_DIVISION) {
		WHERE_DIVISION = wHERE_DIVISION;
	}
	public String getNewPw() {
		return newPw;
	}
	public void setNewPw(String newPw) {
		this.newPw = newPw;
	}
	public String getForMenu() {
		return forMenu;
	}
	public void setForMenu(String forMenu) {
		this.forMenu = forMenu;
	}
	public String getShowYn() {
		return showYn;
	}
	public void setShowYn(String showYn) {
		this.showYn = showYn;
	}
	public String getKOSTL() {
		return KOSTL;
	}
	public void setKOSTL(String kOSTL) {
		KOSTL = kOSTL;
	}
	public String getKTEXT() {
		return KTEXT;
	}
	public void setKTEXT(String kTEXT) {
		KTEXT = kTEXT;
	}
	public String getSUB_AUTH() {
		return SUB_AUTH;
	}
	public void setSUB_AUTH(String sUB_AUTH) {
		SUB_AUTH = sUB_AUTH;
	}
	public String getJOB_GRADE_NM() {
		return JOB_GRADE_NM;
	}
	public void setJOB_GRADE_NM(String jOB_GRADE_NM) {
		JOB_GRADE_NM = jOB_GRADE_NM;
	}
	public String getUSER_NM() {
		return USER_NM;
	}
	public void setUSER_NM(String uSER_NM) {
		USER_NM = uSER_NM;
	}
	public String getDIVISION_NM() {
		return DIVISION_NM;
	}
	public void setDIVISION_NM(String dIVISION_NM) {
		DIVISION_NM = dIVISION_NM;
	}
	public String getResMsg() {
		return resMsg;
	}
	public void setResMsg(String resMsg) {
		this.resMsg = resMsg;
	}
	public String getEncId() {
		return encId;
	}
	public void setEncId(String encId) {
		this.encId = encId;
	}
	public String getUserGubun() {
		return userGubun;
	}
	public void setUserGubun(String userGubun) {
		this.userGubun = userGubun;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getResCd() {
		return resCd;
	}
	public void setResCd(String resCd) {
		this.resCd = resCd;
	}
	public String getUserIp() {
		return userIp;
	}
	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}
	
	public String getUSER_ID() {
		return USER_ID;
	}
	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
		setUserId(uSER_ID);
	}
	public String getPASSWORD() {
		return PASSWORD;
	}
	public void setPASSWORD(String pASSWORD) {
		PASSWORD = pASSWORD;
	}
	public String getCATEGORY() {
		return CATEGORY;
	}
	public void setCATEGORY(String cATEGORY) {
		CATEGORY = cATEGORY;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}
	public String getEMAIL_HEADER() {
		return EMAIL_HEADER;
	}
	public void setEMAIL_HEADER(String eMAIL_HEADER) {
		EMAIL_HEADER = eMAIL_HEADER;
	}
	public String getCD_COUNTRY() {
		return CD_COUNTRY;
	}
	public void setCD_COUNTRY(String cD_COUNTRY) {
		CD_COUNTRY = cD_COUNTRY;
	}
	public String getPHONE() {
		return PHONE;
	}
	public void setPHONE(String pHONE) {
		PHONE = pHONE;
	}
	public String getZIPCODE() {
		return ZIPCODE;
	}
	public void setZIPCODE(String zIPCODE) {
		ZIPCODE = zIPCODE;
	}
	public String getADDR1() {
		return ADDR1;
	}
	public void setADDR1(String aDDR1) {
		ADDR1 = aDDR1;
	}
	public String getADDR2() {
		return ADDR2;
	}
	public void setADDR2(String aDDR2) {
		ADDR2 = aDDR2;
	}
	public String getSEX_FM() {
		return SEX_FM;
	}
	public void setSEX_FM(String sEX_FM) {
		SEX_FM = sEX_FM;
	}
	public String getCD_MEMBER_TYPE() {
		return CD_MEMBER_TYPE;
	}
	public void setCD_MEMBER_TYPE(String cD_MEMBER_TYPE) {
		CD_MEMBER_TYPE = cD_MEMBER_TYPE;
	}
	public String getCD_MEMBER_STATUS() {
		return CD_MEMBER_STATUS;
	}
	public void setCD_MEMBER_STATUS(String cD_MEMBER_STATUS) {
		CD_MEMBER_STATUS = cD_MEMBER_STATUS;
	}
	public String getAUTH() {
		return AUTH;
	}
	public void setAUTH(String aUTH) {
		AUTH = aUTH;
	}
	public String getPUSH_YN() {
		return PUSH_YN;
	}
	public void setPUSH_YN(String pUSH_YN) {
		PUSH_YN = pUSH_YN;
	}
	public String getCNT_VIEW() {
		return CNT_VIEW;
	}
	public void setCNT_VIEW(String cNT_VIEW) {
		CNT_VIEW = cNT_VIEW;
	}
	public String getCNT_RECOM() {
		return CNT_RECOM;
	}
	public void setCNT_RECOM(String cNT_RECOM) {
		CNT_RECOM = cNT_RECOM;
	}
	public String getDATE_C() {
		return DATE_C;
	}
	public void setDATE_C(String dATE_C) {
		DATE_C = dATE_C;
	}
	public String getDATE_E() {
		return DATE_E;
	}
	public void setDATE_E(String dATE_E) {
		DATE_E = dATE_E;
	}
	public String getCD_DEL() {
		return CD_DEL;
	}
	public void setCD_DEL(String cD_DEL) {
		CD_DEL = cD_DEL;
	}
	public String getDIVISION() {
		return DIVISION;
	}
	public void setDIVISION(String dIVISION) {
		DIVISION = dIVISION;
	}
	public String getPARENT_DIVISION() {
		return PARENT_DIVISION;
	}
	public void setPARENT_DIVISION(String pARENT_DIVISION) {
		PARENT_DIVISION = pARENT_DIVISION;
	}
	public String getJOB_GRADE() {
		return JOB_GRADE;
	}
	public void setJOB_GRADE(String jOB_GRADE) {
		JOB_GRADE = jOB_GRADE;
	}
	public String getJOB_POSITION() {
		return JOB_POSITION;
	}
	public void setJOB_POSITION(String jOB_POSITION) {
		JOB_POSITION = jOB_POSITION;
	}
	public String getMEMO() {
		return MEMO;
	}
	public void setMEMO(String mEMO) {
		MEMO = mEMO;
	}
	public String getMEMBER_SEQ() {
		return MEMBER_SEQ;
	}
	public void setMEMBER_SEQ(String mEMBER_SEQ) {
		MEMBER_SEQ = mEMBER_SEQ;
	}
	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}
	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
		setCompanyId(cOMPANY_ID);
	}
	public String getTELEPHONE() {
		return TELEPHONE;
	}
	public void setTELEPHONE(String tELEPHONE) {
		TELEPHONE = tELEPHONE;
	}
	public String getKEYWORD() {
		return KEYWORD;
	}
	public void setKEYWORD(String kEYWORD) {
		KEYWORD = kEYWORD;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getPART() {
		return PART;
	}
	public void setPART(String pART) {
		PART = pART;
	}
	public String getMEMBER_ID() {
		return MEMBER_ID;
	}
	public void setMEMBER_ID(String mEMBER_ID) {
		MEMBER_ID = mEMBER_ID;
	}
	public String getBUSINESS() {
		return BUSINESS;
	}
	public void setBUSINESS(String bUSINESS) {
		BUSINESS = bUSINESS;
	}
	public String getTREE() {
		return TREE;
	}
	public void setTREE(String tREE) {
		TREE = tREE;
	}
	public String getREMARKS() {
		return REMARKS;
	}
	public void setREMARKS(String rEMARKS) {
		REMARKS = rEMARKS;
	}
	public String getComcdArr() {
		return comcdArr;
	}
	public void setComcdArr(String comcdArr) {
		this.comcdArr = comcdArr;
	}
	public String getRemarksArr() {
		return remarksArr;
	}
	public void setRemarksArr(String remarksArr) {
		this.remarksArr = remarksArr;
	}
	public String getCOMCD() {
		return COMCD;
	}
	public void setCOMCD(String cOMCD) {
		COMCD = cOMCD;
	}
	public String getORG_DIVISION() {
		return ORG_DIVISION;
	}
	public void setORG_DIVISION(String oRG_DIVISION) {
		ORG_DIVISION = oRG_DIVISION;
	}
	public String getFailCnt() {
		return failCnt;
	}
	public void setFailCnt(String failCnt) {
		this.failCnt = failCnt;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getREG_ID() {
		return REG_ID;
	}
	public void setREG_ID(String rEG_ID) {
		REG_ID = rEG_ID;
	}
	public String getREG_DT() {
		return REG_DT;
	}
	public void setREG_DT(String rEG_DT) {
		REG_DT = rEG_DT;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getStartDt() {
		return startDt;
	}
	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}
	public String getEndDt() {
		return endDt;
	}
	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}
	@Override
	public String toString() {
		return "MemberVO{" +
				"USER_ID='" + USER_ID + '\'' +
				", PASSWORD='" + PASSWORD + '\'' +
				", CATEGORY='" + CATEGORY + '\'' +
				", NAME='" + NAME + '\'' +
				", EMAIL='" + EMAIL + '\'' +
				", EMAIL_HEADER='" + EMAIL_HEADER + '\'' +
				", CD_COUNTRY='" + CD_COUNTRY + '\'' +
				", PHONE='" + PHONE + '\'' +
				", ZIPCODE='" + ZIPCODE + '\'' +
				", ADDR1='" + ADDR1 + '\'' +
				", ADDR2='" + ADDR2 + '\'' +
				", SEX_FM='" + SEX_FM + '\'' +
				", CD_MEMBER_TYPE='" + CD_MEMBER_TYPE + '\'' +
				", CD_MEMBER_STATUS='" + CD_MEMBER_STATUS + '\'' +
				", AUTH='" + AUTH + '\'' +
				", PUSH_YN='" + PUSH_YN + '\'' +
				", CNT_VIEW='" + CNT_VIEW + '\'' +
				", CNT_RECOM='" + CNT_RECOM + '\'' +
				", DATE_C='" + DATE_C + '\'' +
				", DATE_E='" + DATE_E + '\'' +
				", CD_DEL='" + CD_DEL + '\'' +
				", DIVISION='" + DIVISION + '\'' +
				", PARENT_DIVISION='" + PARENT_DIVISION + '\'' +
				", JOB_GRADE='" + JOB_GRADE + '\'' +
				", JOB_POSITION='" + JOB_POSITION + '\'' +
				", MEMO='" + MEMO + '\'' +
				", MEMBER_SEQ='" + MEMBER_SEQ + '\'' +
				", COMPANY_ID='" + COMPANY_ID + '\'' +
				", TELEPHONE='" + TELEPHONE + '\'' +
				", KEYWORD='" + KEYWORD + '\'' +
				", DIVISION_NM='" + DIVISION_NM + '\'' +
				", USER_NM='" + USER_NM + '\'' +
				", JOB_GRADE_NM='" + JOB_GRADE_NM + '\'' +
				", SUB_AUTH='" + SUB_AUTH + '\'' +
				", KOSTL='" + KOSTL + '\'' +
				", KTEXT='" + KTEXT + '\'' +
				", WHERE_DIVISION='" + WHERE_DIVISION + '\'' +
				", ATTACH_GRP_NO='" + ATTACH_GRP_NO + '\'' +
				", ATTACH='" + ATTACH + '\'' +
				", password='" + password + '\'' +
				", userGubun='" + userGubun + '\'' +
				", userIp='" + userIp + '\'' +
				", resCd='" + resCd + '\'' +
				", resMsg='" + resMsg + '\'' +
				", encId='" + encId + '\'' +
				", showYn='" + showYn + '\'' +
				", forMenu='" + forMenu + '\'' +
				", newPw='" + newPw + '\'' +
				", divNo='" + divNo + '\'' +
				", curLvl='" + curLvl + '\'' +
				"} " + super.toString();
	}
	
}
