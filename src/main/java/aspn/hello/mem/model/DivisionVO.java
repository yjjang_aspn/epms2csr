package aspn.hello.mem.model;

import aspn.hello.com.model.AbstractVO;

/**
 * [부서] VO Class
 * @author 서정민
 * @since 2018.05.08
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.08		서정민			최초 생성
 *   
 * </pre>
 */

public class DivisionVO extends AbstractVO{

	private static final long serialVersionUID = 1L;
	
	/** 컨텐츠 DB model  */
	private String DIVISION				= ""; //부서ID
	private String DIVISION_UID			= ""; //부서코드
	private String NAME					= ""; //부서명
	private String PARENT_DIVISION		= ""; //상위부서ID
	private String DIVISION_TYPE		= ""; //부서유형
	private String TREE					= ""; //구조체
	private String DEPTH				= ""; //레벨
	private String SEQ_DSP				= ""; //출력순서
	private String COMPANY_ID			= ""; //회사코드
	private String LOCATION				= ""; //공장코드
	
	public String getDIVISION() {
		return DIVISION;
	}
	public void setDIVISION(String dIVISION) {
		DIVISION = dIVISION;
	}
	public String getDIVISION_UID() {
		return DIVISION_UID;
	}
	public void setDIVISION_UID(String dIVISION_UID) {
		DIVISION_UID = dIVISION_UID;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getPARENT_DIVISION() {
		return PARENT_DIVISION;
	}
	public void setPARENT_DIVISION(String pARENT_DIVISION) {
		PARENT_DIVISION = pARENT_DIVISION;
	}
	public String getDIVISION_TYPE() {
		return DIVISION_TYPE;
	}
	public void setDIVISION_TYPE(String dIVISION_TYPE) {
		DIVISION_TYPE = dIVISION_TYPE;
	}
	public String getTREE() {
		return TREE;
	}
	public void setTREE(String tREE) {
		TREE = tREE;
	}
	public String getDEPTH() {
		return DEPTH;
	}
	public void setDEPTH(String dEPTH) {
		DEPTH = dEPTH;
	}
	public String getSEQ_DSP() {
		return SEQ_DSP;
	}
	public void setSEQ_DSP(String sEQ_DSP) {
		SEQ_DSP = sEQ_DSP;
	}
	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}
	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
}
