/*
 * Copyright 2011 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package aspn.hello.mem.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.mem.model.MemberVO;
import aspn.hello.sys.model.CodeVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [사용자] Mapper Class
 * 
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *   
 * </pre>
 */

@Mapper("memberMapper")
public interface MemberMapper {

	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 사용자 목록 조회 
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectMemberList(MemberVO memberVO);
	
	/**
	 * 부서 목록 조회(Grid combo)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectDivisionOpt(MemberVO memberVO) throws Exception;
	
	/**
	 * 사용자 중복조회
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return int
	 * @throws Exception
	 */
	public int getMemberDuplicate(MemberVO memberVO);
	
	/**
	 * 사용자 등록(MEMBER)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return int
	 * @throws Exception
	 */
	public int memberAdd(MemberVO memberVO);
	
	/**
	 * 사용자 등록(BUSINESS)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return int
	 * @throws Exception
	 */
	public int businessAdd(MemberVO memberVO);
	
	/**
	 * 사용자 수정(MEMBER)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return int
	 * @throws Exception
	 */
	public int memberChange(MemberVO memberVO);
	
	/**
	 * 사용자 수정(BUSINESS)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return int
	 * @throws Exception
	 */
	public int businessChange(MemberVO memberVO);
	
	/**
	 * BUSINESS 카운트 조회
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return int
	 * @throws Exception
	 */
	public int getBusiCount(MemberVO memberVO);
	
	/**
	 * 사용자 삭제(MEMBER)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return int
	 * @throws Exception
	 */
	public int memberDelete(MemberVO memberVO);
	
	/**
	 * 사용자 삭제(BUSINESS)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return int
	 * @throws Exception
	 */
	public int businessDelete(MemberVO memberVO);
	
	/**
	 * 고객 로그인 ID 중복체크 ajax
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return int
	 * @throws Exception
	 */
	public int clientCheckAjax(MemberVO memberVO);

	/**
	 * 사용자 로그인 ID 중복체크 ajax
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return int
	 * @throws Exception
	 */
	public int memberCheckAjax(MemberVO memberVO);
	
	/**
	 * 비밀번호 설정
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return int
	 * @throws Exception
	 */
	public int setPassword(MemberVO memberVO)throws Exception;
	
	/**
	 * 프로필사진 파일번호 수정
	 *
	 * @author 김영환
	 * @since 2018.05.21
	 * @param MemberVO memberVO
	 * @return void
	 */
	public void updateMemberProfileAttach(MemberVO memberVO);
	
	/**
	 * 사용자권한 목록 조회 
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectMemberPrivList(MemberVO memberVO);
	
	/**
	 * 직책 권한 조회(SUB_AUTH)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectSubAuthList();
	
	/**
	 * 조회 권한(공장) 조회(LOCATION)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectLocationList(MemberVO memberVO);
	
	/**
	 * 기능 권한 조회(FUNC_AUTH)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectFuncAuthList();
	
	/**
	 * 사용자권한 수정(SUBAUTH)
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return int
	 * @throws Exception
	 */
	public int memberPrivChange(MemberVO memberVO);
	
	/**
	 * 사용자권한 수정시 이력 등록
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO memberVO
	 * @return int
	 * @throws Exception
	 */
	public int memberAuthHistoryAdd(MemberVO memberVO);
		
	/**
	 * 공장파트관리 목록 조회(파트구분,파트통합)
	 * 
	 * @author 김영환
	 * @since 2018.04.25
	 * @param MemberVO memberVO
	 * @return HashMap<String, Object>
	 */
	public List<HashMap<String, Object>> selectLocationPartUnion(MemberVO memberVO);

	/**
	 * 공장파트관리(파트구분,파트통합) 정보변경
	 * 
	 * @author 김영환
	 * @since 2018.04.25
	 * @param MemberVO memberVO
	 * @return int
	 */
	public int updateLocationPartUnion(MemberVO memberVO);	
	
	/**
	 * 출퇴근 상태변경
	 * 
	 * @author 김영환
	 * @since 2018.05.18
	 * @param MemberVO memberVO
	 * @return void
	 */
	public void updateCheckInOut(MemberVO memberVO);
	
	/**
	 * 출퇴근  현황 목록 조회
	 * 
	 * @author 김영환
	 * @since 2018.05.18
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, String>>
	 */
	public List<HashMap<String, Object>> selectWorkerStatList(MemberVO memberVO);

	/**
	 * 출퇴근 이력 삭제
	 * @author 김영환
	 * @since 2019.03.20
	 * @param memberMd
	 * @return MemberVO
	 * @throws Exception
	 */
	public void updateCheckInOutList(MemberVO memberVO);
		
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 모바일 사용자 목록 조회 
	 * 
	 * @author 서정민
	 * @since 2018.06.05
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectMemberList2(MemberVO memberVO);
	
	/**
	 * 모바일 - 사용자 상세 조회
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param MemberVO memberVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public HashMap<String, Object> getUserInfo2(MemberVO memberMd);
	
	/**
	 * 모바일 - 파트 정보 조회
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> getPartList(MemberVO memberMd);
	
	/**
	 * 모바일 - 사용자 비밀번호 변경
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param MemberVO memberVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public int setPassword2(MemberVO memberVO);	
		
		
	/**
	 * 모바일 - 출퇴근  현황 목록 조회
	 * 
	 * @author 김영환
	 * @since 2018.05.18
	 * @param MemberVO memberVO
	 * @return List<HashMap<String, String>>
	 */
	public List<HashMap<String, Object>> selectWorkerStatList2(MemberVO memberVO);
		
	/**
	 * [헤더메뉴] : 출퇴근 이력조회
	 * 
	 * @author 김영환
	 * @since 2018.12.11
	 * @param MemberVO memberVO
	 * @param HttpSession session
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectCheckInOutList(MemberVO memberVO);
	
	/**
	 * [헤더메뉴] : 출퇴근 이력 등록
	 * 
	 * @author 김영환
	 * @since 2018.12.11
	 * @param MemberVO memberVO
	 * @param HttpSession session
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	public void insertCheckInOut(MemberVO memberVO);
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 사용하는지 검토 필요
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 계정 관리 팝업 - 사용자 상세 조회
	 * @author 정호윤
	 * @since 2017.09.12
	 * @param memberMd
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public HashMap<String, Object> getUserInfo(MemberVO memberMd);

	/**********************
	//거래처(고객)정보 조회
	 * @author KYH
	 * @since  2016.08.22
	 * @param  memberMd
	 * @throws Exception
	 **********************/
	public MemberVO getClientInfo(MemberVO memberMd);

	/**********************
	//공급사정보 조회
	 * @author KYH
	 * @since  2016.08.23
	 * @param  memberMd
	 * @throws Exception
	 **********************/
	public MemberVO getVendorInfo(MemberVO memberMd);

	public List<HashMap<String,String>> getDeptUser(MemberVO memberMd);
	
	
	public void memberAttachEdit(MemberVO memberMd)throws Exception;
	
	public int memberInfoModify(MemberVO memberMd);
	
	
	public String getUpdateTime(MemberVO memberMd);

	//사용자 생일 정보
	public List<HashMap<String, Object>> getUserBirthDay();

	
	public String getMemberName(MemberVO memberMd);
	
	/**
	 * 부서별 사원정보 목록 그리드 데이터 조회
	 * @author 정순주
	 * @since 2017. 8. 14.
	 * @param memberMd
	 * @return
	 * @return List<HashMap<String,Object>>
	 */
	public List<HashMap<String, Object>> popDivisionMemberData(MemberVO memberMd);

	public void updateMyInfo(MemberVO memberVO);
	
	//아이디 찾기
	public List<HashMap<String, Object>> findId(MemberVO param);
	
	//임시 비밀번호 업데이트
	public void tempPasswordUpdate(MemberVO param);
	
	//
	public MemberVO findPassword(MemberVO param);

	/****************************
	//사원정보 조회 Pop-Up
	 * @author JHY
	 * @since  2016.09.12
	 * @param  memberVO
	 * @throws Exception
	 ****************************/
	public List<HashMap<String, Object>> popMemberList(MemberVO memberVO) throws Exception;

	/**
	 * 사용자 상세 조회 (Model 객체)
	 * @author 정호윤
	 * @since 2017.09.12
	 * @param memberMd
	 * @return MemberVO
	 * @throws Exception
	 */
	public MemberVO getUserInfoByModel(MemberVO memberVO) throws Exception;
	
}
