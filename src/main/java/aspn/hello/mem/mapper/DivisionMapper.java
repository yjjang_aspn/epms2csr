package aspn.hello.mem.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.mem.model.DivisionVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [부서] Mapper Class
 * 
 * @author 서정민
 * @since 2018.05.08
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.08		서정민			최초 생성
 *   
 * </pre>
 */

@Mapper("divisionMapper")
public interface DivisionMapper {
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 부서 목록 조회 
	 * 
	 * @author 서정민
	 * @since 2018.05.08
	 * @param DivisionVO divisionVO
	 * @return String
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectDivisionList(DivisionVO divisionVO) throws Exception;
	
	/**
	 * 부서 SEQ 조회 
	 * 
	 * @author 서정민
	 * @since 2018.05.08
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	public String selectDivisionSeq() throws Exception;
	
	/**
	 * 부서 등록
	 * 
	 * @author 서정민
	 * @since 2018.05.08
	 * @param DivisionVO divisionModel
	 * @return String
	 * @throws Exception
	 */
	public int insertDivision(DivisionVO divisionModel);
	
	/**
	 * 부서 수정
	 * 
	 * @author 서정민
	 * @since 2018.05.08
	 * @param DivisionVO divisionModel
	 * @return String
	 * @throws Exception
	 */
	public int updateDivision(DivisionVO divisionModel);

	/**
	 * 부서 삭제
	 * 
	 * @author 서정민
	 * @since 2018.05.08
	 * @param DivisionVO divisionModel
	 * @return String
	 * @throws Exception
	 */
	public int deleteDivision(DivisionVO divisionModel);
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - 부서 목록 조회 
	 * 
	 * @author 서정민
	 * @since 2018.06.05
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectDivisionList2(DivisionVO divisionVO) throws Exception;
	
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 사용하는지 검토 필요
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public List<HashMap<String, Object>> divisDepartmentData() throws Exception;

	/****************************
	//부서정보 조회 Pop-Up
	 * @author JHY
	 * @since  2016.09.12
	 * @throws Exception
	 ****************************/
	public List<HashMap<String, Object>> popDivisionList(DivisionVO divisionMode) throws Exception;

}
