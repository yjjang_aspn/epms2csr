/*
 * Copyright 2011 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package aspn.hello.com.mapper;

import aspn.hello.com.model.ComVO;
import aspn.hello.com.model.ExceptionLogVO;
import aspn.hello.mem.model.MemberVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.cache.annotation.Cacheable;

/**
 * 인증에 관한 데이터처리 매퍼 클래스
 *
 * @author  이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see 
 * 
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2017.07.04		이영탁			최초 생성
 *   
 * </pre>
 */
@Mapper("comMapper")
public interface ComMapper {

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	public MemberVO getUserInfo(MemberVO memberVO);

	 /**
	 * 거래처(고객)정보 조회
	 * @author 이영탁
	 * @since 2017.07.04
	 * @param CodeVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public MemberVO getClientInfo(MemberVO memberVO);

	 /**
	 * 거래처(고객)정보 조회
	 * @author 이영탁
	 * @since 2017.07.04
	 * @param CodeVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public MemberVO getVendorInfo(MemberVO memberVO);
	
	/**
	 * 비밀번호 실패횟수 조회
	 * @author 서정민
	 * @since 2018.11.05
	 * @param CodeVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public int getLoginFailCnt(MemberVO memberVO);
	
	 /**
	 * 비밀번호 실패횟수 세팅
	 * @author 서정민
	 * @since 2018.11.05
	 * @param CodeVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public void setFailCnt(MemberVO memberVO);
	
	/**
	 * 비밀번호 실패회수 초기화
	 * @author 서정민
	 * @since 2018.11.05
	 * @param CodeVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public void setResetCnt(MemberVO memberVO);

	/**
	 * 에러발생 로그 DB저장
	 * @author 오명석
	 * @since 2017.08.02
	 * @param exceptionLogVO
	 * @return
	 * @throws Exception
	 */
	public void setExceptionLog(ExceptionLogVO exceptionLogVO);
	
	/**
	 * 메인 정비현황목록 정보 가져오기
	 * @author 김영환
	 * @param comVO 
	 * @since 2018.06.14
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public HashMap<String, Object> selectRepairInfo(ComVO comVO);
	
	/**
	 * 메인 예방보전 정보 가져오기
	 * @author 김영환
	 * @since 2018.06.14
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public HashMap<String, Object> selectPreventiveInfo(ComVO comVO);
	
	/**
	 * 공통코드 정보 가져오기
	 * @author 김영환
	 * @since 2018.06.18
	 * @param String COMCD_GRP
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectComCodeList(ComVO comVO);
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - APP 버전정보 JSON
	 * @author 서정민
	 * @since 2018.05.23
	 * @param 
	 * @return List<HashMap<String,Object>>
	 * @throws Exception
	 */
	public HashMap<String,Object> getAppVerJson();

	/**
	 * 모바일 - 부서/사용자 조회
	 * @param companyId 회사 코드
	 * @return 부서/사용자 맵 리스트
	 */
	public List<HashMap<String,Object>> getMemberList(String companyId);
	

	public String cacheTest();

}
