package aspn.hello.com.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import aspn.hello.com.model.AttachVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("attachMapper")
public interface AttachMapper {
	
	public String getFileGrpNo() throws Exception;
	
	public int insertFileGrp(AttachVO attachVO) throws Exception;
	
	public int insertAttach(AttachVO attachVO) throws Exception;
	
	public List<AttachVO> selectDelFileList(String fileGrpNo) throws Exception;
	
	public AttachVO selectOneFile(AttachVO attachVO) throws Exception;
	
	public AttachVO selectUserFile(AttachVO attachVO) throws Exception;
	
	public List<HashMap<String, Object>> selectFileList(AttachVO attachVO) throws Exception;
	
	public int deleteFile(AttachVO attachVO) throws Exception;
	
	public String selectAttachGrpNo() throws Exception;

	public void updateClientFile(AttachVO attachVO);

	public void removeUpdateFile(AttachVO attachVO);

	public String getAttach(String grpNo) throws Exception;

	public void CopyAttach(@Param("attachGrpNo")String attachGrpNo, @Param("beforeAttachGrpNo")String beforeAttachGrpNo) throws Exception;
	
}
