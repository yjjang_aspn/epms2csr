package aspn.hello.com.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.com.model.DeviceVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [단말기] Mapper Class
 * 
 * @author 서정민
 * @since 2018.04.11
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.11		서정민			최초 생성
 *   
 * </pre>
 */
@Mapper("deviceMapper")
public interface DeviceMapper {
	
	/**
	 * 단말기 정보 등록
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param DeviceVO deviceVO
	 * @return int
	 * @throws Exception
	 */
	public int setDevice(DeviceVO deviceMd);
	
	/**
	 * 단말기 정보 삭제
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param DeviceVO deviceVO
	 * @return int
	 * @throws Exception
	 */
	public int deleteDevice(DeviceVO deviceMd);
	
	/**
	 * 푸시대상자 목록 - 내 정비요청 상세
	 * 
	 * @author 서정민
	 * @since 2018.05.16
	 * @param HashMap<String, Object> pushParam
	 * @return List<DeviceVO>
	 * @throws Exception
	 */
	public List<DeviceVO> selectRequestPushList(HashMap<String, Object> pushParam);
	
	/**
	 * 푸시대상자 목록 - 결재 상세
	 * 
	 * @author 서정민
	 * @since 2018.05.16
	 * @param HashMap<String, Object> pushParam
	 * @return List<DeviceVO>
	 * @throws Exception
	 */
	public List<DeviceVO> selectApprPushList(HashMap<String, Object> pushParam);
	
	/**
	 * 푸시대상자 목록 - 계획 및 배정 상세
	 * 
	 * @author 서정민
	 * @since 2018.05.16
	 * @param HashMap<String, Object> pushParam
	 * @return List<DeviceVO>
	 * @throws Exception
	 */
	public List<DeviceVO> selectPlanPushList(HashMap<String, Object> pushParam);
	
	/**
	 * 푸시대상자 목록 - 정비 상세
	 * 
	 * @author 서정민
	 * @since 2018.05.16
	 * @param HashMap<String, Object> pushParam
	 * @return List<DeviceVO>
	 * @throws Exception
	 */
	public List<DeviceVO> selectRepairPushList(HashMap<String, Object> pushParam);

	/**
	 * 푸시대상자 목록 - 긴급요청 알림(긴급요청(수신) 권한 있는 사람 중 파트정비원)
	 * 
	 * @author 서정민
	 * @since 2018.07.09
	 * @param HashMap<String, Object> pushParam
	 * @return List<DeviceVO>
	 * @throws Exception
	 */
	public List<DeviceVO> selectEmergencyPartPushList(HashMap<String, Object> pushParam);

	/**
	 * 푸시대상자 목록 - 긴급요청 알림(긴급요청(수신) 권한 있는 사람 중 파트정비원 제외 인원)
	 * 
	 * @author 서정민
	 * @since 2018.07.09
	 * @param HashMap<String, Object> pushParam
	 * @return List<DeviceVO>
	 * @throws Exception
	 */
	public List<DeviceVO> selectEmergencyNotPartPushList(HashMap<String, Object> pushParam);
	
	/**
	 * 푸시대상자 목록 - 긴급요청 알림(파트정비원 제외 전체인원)
	 * 
	 * @author 서정민
	 * @since 2018.07.09
	 * @param HashMap<String, Object> pushParam
	 * @return List<DeviceVO>
	 * @throws Exception
	 */
	public List<DeviceVO> selectEmergencyAllPushList(HashMap<String, Object> pushParam);
	
	/**
	 * 푸시대상자 목록 - 공지사항
	 * 
	 * @author 서정민
	 * @since 2018.05.16
	 * @param HashMap<String, Object> pushParam
	 * @return List<DeviceVO>
	 * @throws Exception
	 */
	public List<DeviceVO> selectNoticePushList(HashMap<String, Object> pushParam);
	
	/**
	 * 푸시대상자 목록 - 업무현황 알림
	 * 
	 * @author 서정민
	 * @since 2018.07.09
	 * @param HashMap<String, Object> pushParam
	 * @return List<DeviceVO>
	 * @throws Exception
	 */
	public List<DeviceVO> selectAlarmPushList(HashMap<String, Object> pushParam);
	
}
