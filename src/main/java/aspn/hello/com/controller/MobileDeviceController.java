package aspn.hello.com.controller;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.config.SFAConstants;
import aspn.com.common.util.JsonUtil;
import aspn.hello.com.model.DeviceVO;
import aspn.hello.com.service.DeviceService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * [단말기] Controller Class
 * @author 서정민
 * @since 2018.04.11
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.11		서정민			최초 생성
 *   
 * </pre>
 */

@Controller
@RequestMapping("/mobile/device")
public class MobileDeviceController {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private final DeviceService deviceService;

	@Autowired
	public MobileDeviceController(DeviceService deviceService) {
		this.deviceService = deviceService;
	}

	/**
	 * 모바일 디바이스 정보 등록
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param DeviceVO deviceVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/setDevice.do")
	@ResponseBody
	public Map<String, Object> setDevice(DeviceVO deviceVO) throws Exception {
		Map<String, Object> resultMap = new HashMap<>();
		
		try {

			if("".equals(deviceVO.getDEVICE_NM())){
				
				resultMap.put("RES_CD", CodeConstants.PARAM_NULL);
				resultMap.put("RES_MSG", CodeConstants.ERROR_MSG.get(CodeConstants.PARAM_NULL)[1]);
				
				return resultMap;
			}
			deviceService.setDevice(deviceVO);
			resultMap.put("RES_CD", CodeConstants.SUCCESS);
			resultMap.put("RES_MSG", CodeConstants.ERROR_MSG.get(CodeConstants.SUCCESS)[1]);
			
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			resultMap.put("RES_CD", CodeConstants.ERROR_ETC);
			resultMap.put("RES_MSG", CodeConstants.ERROR_MSG.get(CodeConstants.ERROR_ETC)[1]);
		}
		return resultMap;
	}
}
