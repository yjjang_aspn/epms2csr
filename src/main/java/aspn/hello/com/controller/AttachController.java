package aspn.hello.com.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.sap.conn.jco.util.Codecs.Base64;

import aspn.com.common.config.SFAConstants;
import aspn.com.common.util.CompressionUtil;
import aspn.com.common.util.Config;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.Entity;
import aspn.com.common.util.FileHandler;
import aspn.com.common.util.JsUtil;
import aspn.com.common.util.ObjUtil;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.epms.equipment.master.model.EquipmentMasterVO;
import aspn.hello.epms.equipment.master.service.EquipmentMasterService;
import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.epms.material.master.service.MaterialMasterService;
import aspn.hello.epms.repair.request.model.RepairRequestVO;
import aspn.hello.epms.repair.request.service.RepairRequestService;
import aspn.hello.epms.repair.result.model.RepairResultVO;
import aspn.hello.epms.repair.result.service.RepairResultService;
import aspn.hello.mem.model.MemberVO;
import aspn.hello.mem.service.MemberService;

/**
 * 첨부파일 컨트롤러 클래스
 * @author 이영탁
 * @since 2017.09.06
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.09.06  이영탁          최초 생성
*
 * </pre>
 */

@Controller
@RequestMapping("/attach")
public class AttachController extends ContSupport{
	
	public static int WIDTH = 550;
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	AttachService attachService;
	
	@Autowired
	MemberService memberService;

	@Autowired
	EquipmentMasterService equipmentMasterService;
	
	@Autowired
	MaterialMasterService materialMasterService;
	
	@Autowired
	RepairRequestService repairRequestService;
	
	@Autowired
	RepairResultService repairResultService;
	
	/* INSTANCE VAR */
	
	@RequestMapping(value = "/fileEdit.do")
	@ResponseBody
	public void FileEdit(HttpServletRequest request, MultipartHttpServletRequest mRequest, HttpServletResponse response, Model model, AttachVO attachVO, HttpSession session) {
		
		try {
			MemberVO memberVO = new MemberVO();
			memberVO.setUSER_ID(getSsUserId());
			memberVO = memberService.getUserInfoByModel(memberVO);
			memberVO.setUserId(memberVO.getUSER_ID());
			
			// 이미지 파일 등록/삭제 처리
			if ("Y".equals(request.getParameter("attachExistYn")) || !ObjUtil.isEmpty(request.getParameter("DEL_SEQ"))) {
				attachVO.setDEL_SEQ(request.getParameter("DEL_SEQ"));
				attachService.AttachEdit(request, mRequest, attachVO);
			}

			model.addAttribute("attachVO", attachVO);
			
			if("Y".equals(attachVO.getMyInfo())){
				
				memberVO.setATTACH_GRP_NO(attachVO.getATTACH_GRP_NO());
				if("0".equals(request.getParameter("fileCnt"))){
					memberVO.setATTACH_GRP_NO("");
				}
				memberService.memberAttachEdit(memberVO);

				session.setAttribute("ssMemberVO", memberVO);
				session.setMaxInactiveInterval(Integer.parseInt(Config.getString("session.time")));
				
				String callUrl = "/attach/fileUserUdateForm.do";
				String param = "?ATTACH_GRP_NO="+attachVO.getATTACH_GRP_NO()+"&MODULE="+attachVO.getMODULE()+"&flag=edit";
				JsUtil.goAlertURL("정상적으로 파일 등록/수정 처리 되었습니다.", callUrl+param, response);
			}else {
				if("8".equals(attachVO.getMODULE())){ //법인카드 증빙
					String callUrl = "/attach/fileUploadForm.do";
					String param = "?ATTACH_GRP_NO="+attachVO.getATTACH_GRP_NO()+"&MODULE="+attachVO.getMODULE()+"&flag=edit";
					JsUtil.goAlertURL("정상적으로 파일 등록/수정 처리 되었습니다.", callUrl+param, response);
				}else {
					String callUrl = "/attach/fileEditForm.do";
					String param = "?ATTACH_GRP_NO="+attachVO.getATTACH_GRP_NO()+"&MODULE="+attachVO.getMODULE()+"&flag=edit";
					JsUtil.goAlertURL("정상적으로 파일 등록/수정 처리 되었습니다.", callUrl+param, response);
				}
				
			}
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			try {
				if("Y".equals(attachVO.getMyInfo())){
					JsUtil.goAlertURL("첨부파일 등록/수정에 실패하였습니다. 다시 시도 해주십시오.","/attach/fileUserUdateForm.do", response);
				}else {
					JsUtil.goAlertURL("첨부파일 등록/수정에 실패하였습니다. 다시 시도 해주십시오.","/attach/fileEditForm.do", response);
				}
				
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	
	/**
	 * 
	 * @MethodName  : fileDownload
	 * @Date        : 2014. 9. 1.
	 * @author      : PSJ
	 * @Description : 파일 다운로드
	 * @History     : 2014. 9. 1. 최초 작성
	 * @param res   : HttpServletResponse 객체
	 */
	@RequestMapping(value="/fileDownload.do")
	public void fileDownload(HttpServletRequest request, HttpServletResponse response, AttachVO attachVO) {
		
		try {
			
			/**
			@SuppressWarnings("rawtypes")
			Enumeration eHeader = request.getHeaderNames();
			
			while (eHeader.hasMoreElements()) {
			    String hName = (String)eHeader.nextElement();
			    String hValue = request.getHeader(hName);
			    System.out.println(hName + " : " + hValue + "");
			}
			**/
			
			attachVO = attachService.selectOneFile(attachVO);
			
			// File Full Path Download
			if ( attachVO.getPATH() != null && attachVO.getNAME() != null ) {
				
				FileHandler.fileDownload(attachVO, response);
				
			} else {
				
				// 파일정보가 조회 되지 않았을때 처리
				logger.debug( "# AttachController.fileDownload >>> 파일정보가 조회 되지 않았을때 처리 ~~" );
			}
			
		} catch(Exception e) {
			
			//e.printStackTrace();  // [2021.03.31][yjjang]
			
			logger.info( "[yjjang] AttachController.fileDownload >>> " );
			logger.error( "" + e.toString() );
			
			//throw e;
		}
		
	}
	
	
	/**
	 * 
	 * @MethodName  : thumbFileDownload
	 * @Date        : 2014. 12. 4.
	 * @author      : Lee hyunho
	 * @Description : 썸네일파일 다운로드
	 * @History     : 2014. 12. 4. 최초 작성
	 * @param res   : HttpServletResponse 객체
	 */
	@RequestMapping(value="/thumbFileDownload.do")
	public void thumbFileDownload(HttpServletResponse respose, AttachVO attachVO) {
		
		try {
			
			//System.out.println(ATTACH);
			attachVO = attachService.selectOneFile(attachVO);
			
			if(attachVO != null){
				//파일 풀패스가 있을때 다운로드
				if(attachVO.getPATH() != null && attachVO.getNAME() != null) {
					
					FileHandler.thumbFileDownload(attachVO, respose);
					
				} else {
					//파일정보가 조회 되지 않았을때 처리
				}
			}
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
			logger.error("AttachController.fileDownload > " + e.toString());
		}
	}
	
	/**
	 * 
	 * @MethodName          : fileDownloadForm
	 * @Date                : 2014. 11. 17.
	 * @author              : lee hyunho
	 * @Description         : 파일 다운로드 폼
	 * @History             : 2014. 11. 17. 최초 작성
	 * @param attachVO   : 파일 모델
	 * @param model         : 모델 리턴
	 */
	@RequestMapping(value="/fileEditForm.do")
	public String fileEditForm(AttachVO attachVO, Model model) {
		
		try {
			String attachGrpNo 		   = "";
			List<HashMap<String, Object>> fileList = attachService.selectFileList(attachVO);
					
			if("".equals(attachVO.getATTACH_GRP_NO())){
				attachGrpNo = attachService.selectAttachGrpNo();
			}
			
			model.addAttribute("attachGrpNo", attachGrpNo);
			model.addAttribute("fileList", fileList);
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
			logger.error("AttachController.fileDownloadForm > " + e.toString());
		}
		
		return "/com/attach/fileEditForm";
		
	}
	
	@RequestMapping(value="/fileUserUdateForm.do")
	public String fileUserUdateForm(HttpServletRequest request,AttachVO attachVO, Model model) {
		
		try {
			String attachGrpNo = "";
			List<HashMap<String, Object>> fileList = attachService.selectFileList(attachVO);
					
			if("".equals(attachVO.getATTACH_GRP_NO())){
				attachGrpNo = attachService.selectAttachGrpNo();
			}
			
			model.addAttribute("attachGrpNo", attachGrpNo);
			model.addAttribute("fileList", fileList);
		} catch(Exception e) {
			
			e.printStackTrace();
			
			logger.error("AttachController.fileDownloadForm > " + e.toString());
		}
		
		return "/com/attach/fileUserUdateForm";
		
	}
	
	@RequestMapping(value = "/appUserFile.do")
	@ResponseBody
	public HashMap<String, Object> AppUserFile(HttpServletRequest request, MultipartHttpServletRequest mRequest, HttpServletResponse response, Model model, AttachVO attachVO) {

		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		try {
			//개별 사진을 우선 조회 한다.
			MemberVO memberVO = new MemberVO();
			memberVO.setUserId(attachVO.getUserId());
			HashMap<String, Object> memberInfo = memberService.getUserInfo(memberVO);
			
			String attachGrpNo = "";
//			if(memberVO.getATTACH_GRP_NO() == null || "".equals(memberVO.getATTACH_GRP_NO())){
			if(memberInfo.get("ATTACH_GRP_NO") == null || "".equals(memberInfo.get("ATTACH_GRP_NO"))){
				attachGrpNo = attachService.selectAttachGrpNo();
				attachVO.setATTACH_GRP_NO(attachGrpNo);
				memberVO.setATTACH_GRP_NO(attachGrpNo);
			}else{
				attachVO.setATTACH_GRP_NO(memberVO.getATTACH_GRP_NO());
				attachVO.setDEL_SEQ(memberVO.getATTACH());
			}
			
			//첨부파일 생성
			attachService.AttachEdit(request, mRequest, attachVO);
			
			//생성된 첨부파일 seq user정보에 mapping
			memberService.memberAttachEdit(memberVO);
			
			attachVO = attachService.selectUserFile(attachVO);
			String param = "ATTACH_GRP_NO="+attachVO.getATTACH_GRP_NO()+"&ATTACH="+attachVO.getATTACH()+"&EditTime="+DateTime.getTimeStampString();
			
			rtnMap.put("resCd", "0000");
			rtnMap.put("resMsg", "성공");
			rtnMap.put("fileUrl", "/thumbFileDownload.do?"+param);
			
		} catch (Exception e) {
			
			e.printStackTrace();

			rtnMap.put("resCd", "1002");
			rtnMap.put("resMsg", SFAConstants.ERROR_MSG.get("1002")[1]);
		}
		
		return rtnMap;
	}
	
	@RequestMapping(value = "/appEditFile.do")
	@ResponseBody
	public HashMap<String, Object> AppEditFile(HttpServletRequest request, MultipartHttpServletRequest mRequest, HttpServletResponse response, Model model, AttachVO attachVO) {

		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		try {
			//첨부파일 생성
			attachService.AttachEdit(request, mRequest, attachVO);

			rtnMap.put("resCd", "0000");
			rtnMap.put("resMsg", "성공");
			
		} catch (Exception e) {
			
			e.printStackTrace();

			rtnMap.put("resCd", "1002");
			rtnMap.put("resMsg", SFAConstants.ERROR_MSG.get("1002")[1]);
		}
		
		return rtnMap;
	}

	@RequestMapping(value = "/getFileNo.do")
	@ResponseBody
	public String getFileNo(HttpServletRequest request, HttpServletResponse response, Model model, AttachVO attachVO) throws Exception {
		String attachGrpNo = "";
		attachGrpNo = attachService.selectAttachGrpNo();
		return attachGrpNo;
	}
	
	/**
	 * 
	 * @author kim yeo
	 * @since 2015.01.27
	 * @param expnSlipModel
	 * @param model
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping(value = "/expnFileDataAjax.do")
	@ResponseBody
	public AttachVO expnFileDataAjax(AttachVO attachVO, Model model) throws Exception {
		
		AttachVO data = attachService.selectOneFile(attachVO);
		return data;
	}
	
	@RequestMapping(value = "/editor/imageUpload.do")
    public void communityImageUpload(HttpServletRequest request, HttpServletResponse response, @RequestParam MultipartFile upload) throws Exception {
 
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		OutputStream out = null;
		PrintWriter printWriter = null;

		
		try {
			String targetFolder = request.getParameter("folder");
			String grpNo = request.getParameter("ATTACH_GRP_NO");
			String fileName = upload.getOriginalFilename();

			// 수정날짜:2016.03.11 ,수정내용:동일한 파일명의 다른 파일을 덮어쓰는 문제발생 가능성이 있어 아래의 구문
			// 활성화
			String ext = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
			String timeM = String.valueOf(System.currentTimeMillis());
//			fileName = fileName.substring(0, fileName.lastIndexOf(".")) + "_" + timeM + "." + ext;
			fileName = fileName.substring(fileName.lastIndexOf(File.separator)+1, fileName.lastIndexOf(".")) + "_" + timeM + "." + ext;
			String file_types = "|gif |jpg |png |jpeg |GIF |JPG |PNG |JPEG";

			byte[] bytes = upload.getBytes();

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
			String folder = sdf.format(date);
			String fileDir = Config.getString("file.upload.dir") + File.separator + folder + File.separator
					+ targetFolder;

			File dir = new File(fileDir);
			if (!dir.exists())dir.mkdirs();

			if (file_types.indexOf(ext.toLowerCase()) == -1) {

				printWriter = response.getWriter();
				printWriter.println("<script type='text/javascript'>window.parent.alert('이미지 파일(gif,jpg,png,jpeg)만 업로드 가능합니다. 다시 시도해 주세요.');"
						+ "window.parent.close();</script>");
//						+ "window.parent.CKEDITOR.tools.callFunction(154,'');</script>");
				printWriter.flush();
				
				return;

			}

			String uploadPath = fileDir + "/" + fileName;// 저장경로

			out = new FileOutputStream(new File(uploadPath));
			out.write(bytes);
			String callback = request.getParameter("CKEditorFuncNum");

			printWriter = response.getWriter();

			AttachVO attachVO = attachService.setEditorFile(grpNo, fileDir, fileName, upload.getSize());
			
			//base64
			File orgFile = new File(uploadPath);
			
			String fileToBase64 = null;
			
			@SuppressWarnings("resource")
			FileInputStream fileInputStreamReader = new FileInputStream(orgFile);
			
			fileInputStreamReader.read(bytes);
			fileToBase64 = new String(Base64.encode(bytes));
			
			logger.debug("fileToBase64:>" + fileToBase64);
			
//			String fileUrl = "/attach/fileDownload.do?ATTACH_GRP_NO=" + attachVO.getATTACH_GRP_NO()
//					+ "&ATTACH=" + attachVO.getATTACH() + "&"; //fileUrl 마지막 &제거하지 말것
			
			String fileUrl = "data:image/png;base64," + fileToBase64; //fileUrl 마지막 &제거하지 말것
			
			printWriter.println("<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction('" + callback
					+ "','" + fileUrl + "','이미지를 업로드 하였습니다.');window.parent.fnSetAttahGrp('"
					+ callback + "','"+ attachVO.getATTACH_GRP_NO() + "','" + attachVO.getATTACH() + "');</script>");
			printWriter.flush();
			
			
			FileHandler.thumbnailMake(fileDir, fileName, WIDTH,"8");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			
		}catch (IOException e) {
			e.printStackTrace();
			
		}finally {
			try {
				if (out != null) {
					out.close();
				}
				if (printWriter != null) {
					printWriter.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return;
	}
	
	@RequestMapping(value="/fileUploadForm.do")
	public String fileUploadForm(AttachVO attachVO, Model model) {
		
		try {
			String attachGrpNo 		   = "";
			List<HashMap<String, Object>> fileList = attachService.selectFileList(attachVO);
			if("".equals(attachVO.getATTACH_GRP_NO())){
				attachGrpNo = attachService.selectAttachGrpNo();
			}
			model.addAttribute("attachGrpNo", attachGrpNo);
			model.addAttribute("fileList", fileList);
			
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("AttachController.fileUploadForm > " + e.toString());
		}
		return "/com/attach/fileUploadForm";
		
	}

	/**
	 * 모달 첨부파일 다운로드 화면(eacc 전용)
	 * @author 정순주
	 * @since 2017. 8. 29.
	 * @param attachVO
	 * @param model
	 * @return String
	 */
	@RequestMapping(value="/modalFileDownForm.do")
	public String modalFileDownForm(AttachVO attachVO, Model model) {
		
		try {
			String attachGrpNo 		   = "";
			List<HashMap<String, Object>> fileList = attachService.selectFileList(attachVO);
					
			if("".equals(attachVO.getATTACH_GRP_NO())){
				attachGrpNo = attachService.selectAttachGrpNo();
			}
			
			model.addAttribute("attachGrpNo", attachGrpNo);
			model.addAttribute("fileList", fileList);
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
			logger.error("AttachController.fileDownloadForm > " + e.toString());
		}
		
		return "/hello/eacc/common/modalFileDownForm";
		
	}
	
	/**
	 * 첨부파일 상세보기
	 * 
	 * @author 서정민
	 * @since 2018.04.17
	 * @param AttachVO attachVO
	 * @param Model model
	 * @return String
	 */
	@RequestMapping(value="/popFileManageForm.do")
	public String popFileManageForm(AttachVO attachVO, Model model) {
		
		try {
			List<HashMap<String, Object>> fileList = attachService.selectFileList(attachVO);
			model.addAttribute("fileList", fileList);
			
		} catch(Exception e) {
			logger.error("AttachController.popFileManageForm > " + e.toString());
			e.printStackTrace();
		}
		
		return "/com/attach/popFileManageForm";
		
	}
	
	/**
	 * 첨부파일 등록/수정
	 * 
	 * @author 서정민
	 * @since 2018.04.17
	 * @param HttpServletRequest request
	 * @param AttachVO attachVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateAttachInfo.do")
	@ResponseBody
	public Map<String, Object> updateAttachInfo(HttpServletRequest request, MultipartHttpServletRequest mRequest, AttachVO attachVO) throws Exception {
		
		Map<String, Object> resultMap = new HashMap<>();
		Entity entity = new Entity(request);
		
		try {
			attachVO.setUserId(getSsUserId());
				
			// ATTACH_GRP_NO가 기존에 존재하지 않았으며, 수정후에는 이미지가 등록되는 경우
			 if("NY".equals(attachVO.getAttachExistYn())){			
				 String attachGrpNo = attachService.selectAttachGrpNo();
				 attachVO.setATTACH_GRP_NO(attachGrpNo);
			 }
			 attachService.AttachEdit(request, mRequest, attachVO); 
			
			//프로필 이미지 
			if(attachVO.getFlag().equals("profile")){
				MemberVO memberVO = new MemberVO();
				memberVO.setUSER_ID(entity.getString("ATTACH_USER_ID"));
				memberVO.setATTACH_GRP_NO(attachVO.getATTACH_GRP_NO());
				
				memberService.updateMemberProfileAttach(memberVO);
				resultMap.put("RES_CD", "S");
			} 
			//설비 이미지 
			else if(attachVO.getFlag().equals("equipment")){
				EquipmentMasterVO equipmentMasterVO = new EquipmentMasterVO();
				equipmentMasterVO.setFlag("equipment");
				equipmentMasterVO.setEQUIPMENT(entity.getString("ATTACH_EQUIPMENT"));
				equipmentMasterVO.setATTACH_GRP_NO(attachVO.getATTACH_GRP_NO());
				equipmentMasterVO.setUserId(getSsUserId());
				
				equipmentMasterService.updateEquipmentAttach(equipmentMasterVO);
				resultMap.put("RES_CD", "S");
			}
			//설비 매뉴얼
			else if(attachVO.getFlag().equals("equipment_manual")){
				EquipmentMasterVO equipmentMasterVO = new EquipmentMasterVO();
				equipmentMasterVO.setFlag("equipment_manual");
				equipmentMasterVO.setEQUIPMENT(entity.getString("ATTACH_EQUIPMENT"));
				equipmentMasterVO.setATTACH_GRP_NO3(attachVO.getATTACH_GRP_NO());
				equipmentMasterVO.setUserId(getSsUserId());
				
				equipmentMasterService.updateEquipmentAttach(equipmentMasterVO);
				resultMap.put("RES_CD", "S");
			}
			//자재 첨부
			else if(attachVO.getFlag().equals("material")){
				MaterialMasterVO materialMasterVO = new MaterialMasterVO();
				materialMasterVO.setMATERIAL(entity.getString("ATTACH_MATERIAL"));
				materialMasterVO.setATTACH_GRP_NO(attachVO.getATTACH_GRP_NO());
				materialMasterVO.setUserId(getSsUserId());
				 
				materialMasterService.updateMaterialImgModify(materialMasterVO);
				resultMap.put("RES_CD", "S");
			}
			//정비요청 첨부
			else if(attachVO.getFlag().equals("repair_request")){
				RepairRequestVO repairRequestVO = new RepairRequestVO();
				repairRequestVO.setREPAIR_REQUEST(entity.getString("ATTACH_REPAIR_REQUEST"));
				repairRequestVO.setATTACH_GRP_NO(attachVO.getATTACH_GRP_NO());
				repairRequestVO.setUserId(getSsUserId());
				 
				repairRequestService.updateRepairRequestAttach(repairRequestVO);
				resultMap.put("RES_CD", "S");
			}
			//정비실적 첨부
			else if(attachVO.getFlag().equals("repair_result")){
				RepairResultVO repairResultVO = new RepairResultVO();
				repairResultVO.setREPAIR_RESULT(entity.getString("ATTACH_REPAIR_RESULT"));
				repairResultVO.setATTACH_GRP_NO(attachVO.getATTACH_GRP_NO());
				repairResultVO.setUserId(getSsUserId());
				 
				repairResultService.updateRepairResultAttach(repairResultVO);
				resultMap.put("RES_CD", "S");
			}
			 
		} catch (Exception e) {
			resultMap.put("RES_CD", "F");
			logger.error("AttachController.updateAttachInfo Error > " + e.toString());
			e.printStackTrace();
		}
		return resultMap;
	}
	
	/**
	 * 
	 * @MethodName  : zipFileDownload
	 * @Date        : 2018. 06. 01
	 * @author      : 김영환
	 * @Description : 압축파일 다운로드
	 * @param res   : HttpServletResponse 객체
	 * @throws IOException 
	 */
	@RequestMapping(value="/zipFileDownload.do")
	public void zipFileDownload(HttpServletResponse response, HttpServletRequest request, AttachVO attachVO) throws IOException {
		String fileDir = Config.getString("file.upload.dir") + File.separator + "zip" + File.separator;
		
		String zipName = "ePMS_" + (attachVO.getZipFileName() != "" ? attachVO.getZipFileName() : "download") + ".zip";
		try {
			List<HashMap<String, Object>> attachList = attachService.selectFileList(attachVO);
			
			List<File> filelist = new ArrayList<File>();
			CompressionUtil cu = new CompressionUtil();
			for (HashMap<String, Object> item : attachList) {
				String path = item.get("PATH") + File.separator + item.get("NAME");
				filelist.add(new File(path));
				
			}
			
			File dir = new File(fileDir);
			if(!dir.exists()) dir.mkdirs();
			
			File zippedFile = new File ( fileDir, zipName); 
			cu.zip(filelist, new FileOutputStream(zippedFile));
		} catch (Exception e) {
			e.printStackTrace();
			
			logger.error("AttachController.zipFileDownload > " + e.toString());
		} finally {
			FileHandler.zipFileDownload(fileDir, zipName, response);
		}
		
	}
}
