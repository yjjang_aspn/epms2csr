package aspn.hello.com.controller;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.FileHandler;
import aspn.com.common.util.JsonUtil;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.mem.model.MemberVO;
import aspn.hello.mem.service.MemberService;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * [첨부파일] Controller Class
 * @author 서정민
 * @since 2018.04.12
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.12		서정민			최초 생성
 *   
 * </pre>
 */

@Controller
@RequestMapping("/mobile/attach")
public class MobileAttachController extends ContSupport{

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	AttachService attachService;
	
	@Autowired
	MemberService memberService;

	/**
	 * 모바일 - 파일 다운로드(썸네일)
	 * 
	 * @author 서정민
	 * @since 2018.04.12
	 * @param HttpServletResponse response
	 * @param AttachVO attachVO
	 * @return void
	 * @throws Exception
	 */
	@RequestMapping(value="/thumbFileDownload.do")
	public void thumbFileDownload(HttpServletResponse response, AttachVO attachVO) throws Exception {

		try {
			// 파일 조회
			attachVO = attachService.selectOneFile(attachVO);

			if(attachVO != null){
				// 파일 풀패스가 있을때 다운로드
				if(attachVO.getPATH() != null && attachVO.getNAME() != null) {

					FileHandler.thumbFileDownload(attachVO, response);
				}
			}
		} catch(Exception e) {
			logger.error("MobileComController.fileDownload > " + e.toString());
			e.printStackTrace();
		}
	}

	/**
	 * 모바일 - 파일 다운로드
	 * 
	 * @author 서정민
	 * @since 2018.04.12
	 * @param HttpServletResponse response
	 * @param AttachVO attachVO
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/fileDownload.do")
	public void fileDownload(HttpServletResponse response, AttachVO attachVO) throws Exception {

		try {
			// 파일조회
			attachVO = attachService.selectOneFile(attachVO);

			// 파일 풀패스가 있을때 다운로드
			if(attachVO.getPATH() != null && attachVO.getNAME() != null) {

				FileHandler.fileDownload(attachVO, response);
			}
		} catch(Exception e) {
			logger.error("AttachController.fileDownload > " + e.toString());
			e.printStackTrace();
		}
	}
	
	/**
	 * 프로필 사진 변경
	 * 
	 * @author 서정민
	 * @since 2018.04.12
	 * @param HttpServletRequest request
	 * @param AttachVO attachVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/changeProfile.do")
	@ResponseBody
	public Map<String, Object> changeProfile(HttpServletRequest request, MultipartHttpServletRequest mRequest, AttachVO attachVO) throws Exception {
		
		Map<String, Object> resultMap = new HashMap<>();

		try {
			// Validation
			if (StringUtils.isEmpty(attachVO.getUserId())
					|| StringUtils.isEmpty(attachVO.getMODULE())) {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.PARAM_NULL);
			}
			else {
				// 유저정보 조회
				MemberVO memberVO = new MemberVO();
				String attachGrpNo = "";
				String fileUrl = "";

				memberVO.setUSER_ID(attachVO.getUserId());
				memberVO.setUserId(attachVO.getUserId());
				memberVO = memberService.getUserInfoByModel(memberVO);


				if (StringUtils.isEmpty(memberVO.getATTACH_GRP_NO())) {
					attachGrpNo = attachService.selectAttachGrpNo();
					attachVO.setATTACH_GRP_NO(attachGrpNo);
					memberVO.setATTACH_GRP_NO(attachGrpNo);
				} else {
					attachVO.setATTACH_GRP_NO(memberVO.getATTACH_GRP_NO());
					attachVO.setCD_DEL("Y");
					attachVO.setDEL_SEQ("1");
				}

				// 이미지 수정
				attachService.AttachEdit(request, mRequest, attachVO);
				memberService.memberAttachEdit(memberVO);

				// 이미지 url 세팅
				attachVO = attachService.selectUserFile(attachVO);
				fileUrl = String.format("/mobile/attach/thumbFileDownload.do?ATTACH_GRP_NO=%s&ATTACH=%s&EditTime=%s", attachVO.getATTACH_GRP_NO(), attachVO.getATTACH(), DateTime.getTimeStampString());
				resultMap.put("fileUrl", fileUrl);
				
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}

	/**
	 * 증빙자료 업로드
	 * @author 강승상
	 * @since 2017.09.14
	 * @param pRequest multiPart
	 * @param pAttachVO userId, companyId, MODULE
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/appUploadFile.do")
	@ResponseBody
	public Map<String, Object> appUploadFile(HttpServletRequest pRequest, MultipartHttpServletRequest mRequest, AttachVO pAttachVO) throws Exception {
		Map<String, Object> resultMap = new HashMap<>();
		try {
			// Validation Check
			if (StringUtils.isEmpty(pAttachVO.getUserId())
					|| StringUtils.isEmpty(pAttachVO.getCompanyId())
					|| StringUtils.isEmpty(pAttachVO.getMODULE())) {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.PARAM_NULL);
			} else {
				attachService.AttachEdit(pRequest, mRequest, pAttachVO);
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return JsonUtil.getMobileExceptionMessage(e.getMessage());
		}
		return resultMap;
	}

	
}
