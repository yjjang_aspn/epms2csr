package aspn.hello.com.controller;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.APKCertExtractor;
import aspn.com.common.util.Config;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.JsonUtil;
import aspn.com.common.util.PushThread;
import aspn.hello.com.model.DeviceVO;
import aspn.hello.com.service.ComService;
import aspn.hello.com.service.DeviceService;
import aspn.hello.epms.equipment.master.model.EquipmentMasterVO;
import aspn.hello.epms.equipment.master.service.EquipmentMasterService;
import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.epms.material.master.service.MaterialMasterService;
import aspn.hello.epms.preventive.result.model.PreventiveResultVO;
import aspn.hello.epms.preventive.result.service.PreventiveResultService;
import aspn.hello.epms.repair.appr.model.RepairApprVO;
import aspn.hello.epms.repair.appr.service.RepairApprService;
import aspn.hello.epms.repair.plan.model.RepairPlanVO;
import aspn.hello.epms.repair.plan.service.RepairPlanService;
import aspn.hello.epms.repair.request.model.RepairRequestVO;
import aspn.hello.epms.repair.request.service.RepairRequestService;
import aspn.hello.epms.repair.result.model.RepairResultVO;
import aspn.hello.epms.repair.result.service.RepairResultService;
import aspn.hello.mem.model.DivisionVO;
import aspn.hello.mem.model.MemberVO;
import aspn.hello.mem.service.DivisionService;
import aspn.hello.mem.service.MemberService;
import aspn.hello.sys.model.CategoryVO;
import aspn.hello.sys.service.CategoryService;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.DigestUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * [공통-모바일] Controller Class
 * @author 서정민
 * @since 2018.04.11
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.11		서정민			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/mobile")
public class MobileComController extends ContSupport {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	ComService comService;
	
	@Autowired
	DivisionService divisionService;
	
	@Autowired
	MemberService memberService;
	
	@Autowired
	DeviceService deviceService;
	
	@Autowired
	CategoryService categoryService;
	
	@Autowired
	EquipmentMasterService equipmentMasterService;

	@Autowired
	MaterialMasterService materialMasterService;
	
	@Autowired
	RepairRequestService repairRequestService;
	
	@Autowired
	RepairApprService repairApprService;
	
	@Autowired
	RepairPlanService repairPlanService;
	
	@Autowired
	RepairResultService repairResultService;
	
	@Autowired
	PreventiveResultService preventiveResultService;
	
	
	/**
	 * APP 버전 체크
	 * 
	 * @author 서정민
	 * @since 2018.05.23
	 * @param 
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAppVersion.do")
	@ResponseBody
	public Map<String, Object> getAppVersion() throws Exception {
		
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			
			HashMap<String, Object> appVerJsonDtl = comService.getAppVerJson();
			
			// APP 버전정보 JSON 파일 위치
			resultMap.put("RES_JSON_LOCATION", appVerJsonDtl.get("COMCD_NM").toString());
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * 안드로이드 무결성 검증
	 * 
	 * @author 서정민
	 * @since 2018.05.23
	 * @param HttpServletRequest request
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/appCert.do")
	@ResponseBody
	public HashMap<String, Object> appCert(HttpServletRequest request) throws Exception {
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		try {
			String cert_key = APKCertExtractor.execute("D:\\HelloEPMS\\app\\appVer\\epms_dunkin.apk");
			
			if(cert_key.equals(request.getParameter("cert"))){
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}else{
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.NO_CERTIFIED);
			}
			
		}  catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * 모바일 로그인
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param MemberVO memberVO
	 * @param HttpSession session
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/appLogin.do")
	@ResponseBody
	public Map<String, Object> appLogin(MemberVO memberVO, HttpSession session) throws Exception {
		
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			
			memberVO = comService.setLogin(memberVO);
			JsonUtil.setMobileNormalMessage(resultMap, memberVO.getResCd());
			
			if(CodeConstants.SUCCESS.equals(memberVO.getResCd()) ) {
				session.removeAttribute("ssHeaderMenuList");
				session.setAttribute("ssMemberVO", memberVO);
				session.setAttribute("ssCheckInout", memberVO.getCHECK_INOUT());
				
				session.setAttribute("ssUserId", memberVO.getUSER_ID());
				session.setAttribute("ssAuth", memberVO.getAUTH());
				session.setAttribute("ssUserNm", memberVO.getUSER_NM());
				session.setAttribute("ssDivision", memberVO.getDIVISION());
				session.setAttribute("ssDivisionNm", memberVO.getDIVISION_NM());
				session.setAttribute("ssJobGradeNm", memberVO.getJOB_GRADE_NM());
				session.setAttribute("ssSubAuth", memberVO.getSUB_AUTH());
				session.setAttribute("ssLocation", memberVO.getLOCATION());
				session.setAttribute("ssPart", memberVO.getPART());
				session.setAttribute("ssMemberId", memberVO.getMEMBER_ID());
				session.setAttribute("ssRemarks", memberVO.getREMARKS());
//				session.setAttribute("ssViewAuth", memberVO.getVIEW_AUTH());
				session.setAttribute("ssCompanyId", memberVO.getCOMPANY_ID());
				session.setAttribute("ssKostl", memberVO.getKOSTL());
				session.setMaxInactiveInterval(Integer.parseInt(Config.getString("session.time")));
				
				// 1. 메뉴 및 출퇴근 권한
				// 1-1. 팀장일 경우
				if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
					resultMap.put("AUTH_MENU", CodeConstants.AUTH_CHIEF);
					resultMap.put("AUTH_INOUT", "Y");
				}
				// 1-2. 정비원일 경우
				else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
					resultMap.put("AUTH_MENU", CodeConstants.AUTH_REPAIR);
					resultMap.put("AUTH_INOUT", "Y");
				}
				// 1-3. 일반사용자일 경우
				else{
					resultMap.put("AUTH_MENU", CodeConstants.AUTH_COMMON);
					resultMap.put("AUTH_INOUT", "Y");
				}
				
				// 2. 설비이미지관리 권한
				if(getSsSubAuth().indexOf(CodeConstants.AUTH_FUNC01)>-1){
					resultMap.put("AUTH_FUNC01", "Y");
				}
				else{
					resultMap.put("AUTH_FUNC01", "N");
				}
				
				// 3. 자재이미지관리 권한
				if(getSsSubAuth().indexOf(CodeConstants.AUTH_FUNC02)>-1){
					resultMap.put("AUTH_FUNC02", "Y");
				}
				else{
					resultMap.put("AUTH_FUNC02", "N");
				}
				
				// 4. 출퇴근 상태
				resultMap.put("CHECK_INOUT", memberVO.getCHECK_INOUT());
				
				// 5. 외부링크 URL
				// 프로퍼티 객체 생성
	            Properties _config = new Properties();
	            // 프로퍼티 파일 로딩
	            _config.load(new java.io.BufferedInputStream(new FileInputStream(ResourceUtils.getFile("classpath:/config/config.properties"))));
	            if(_config.getProperty("external_link") != null){
	            	resultMap.put("EXTERNAL_LINK", _config.getProperty("external_link").toString());
	            }
				
				// 6. 뱃지 카운트 및 ToDo목록
				// 6-1. 결재 : 뱃지 카운트
				RepairApprVO repairApprVO = new RepairApprVO();
				repairApprVO.setSubAuth(getSsSubAuth());     //세션_조회권한
				repairApprVO.setSsLocation(getSsLocation()); //세션_공장위치
				repairApprVO.setSsRemarks(getSsRemarks());   //세션_파트 분리/통합 여부
				repairApprVO.setSsPart(getSsPart());         //세션_파트
				
				repairApprVO.setLOCATION("ALL");             //조회 공장
				
				// 팀장일 경우
				if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
					repairApprVO.setAuthType(CodeConstants.AUTH_CHIEF);
				}
				// 정비원일 경우
				else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
					repairApprVO.setAuthType(CodeConstants.AUTH_REPAIR);
					
					// 공무(기계, 전기, 시설) 일 경우
					if("BR00_01".equals(getSsPart()) || "BR00_02".equals(getSsPart()) || "BR00_03".equals(getSsPart())){
						repairApprVO.setPART("BR00_01,BR00_02,BR00_03");
					}
					// 생산 일 경우
					else if("BR00_04".equals(getSsPart())){
						repairApprVO.setPART("BR00_04");
					}
					else {
						repairApprVO.setPART(getSsPart());
					}
					repairApprVO.setPART(addStringToQuoto(repairApprVO.getPART()));
				}
				resultMap.put("APPR_CNT", repairApprService.selectRepairApprCnt(repairApprVO));
				
				// 6-2. 담당자배정 : 뱃지 카운트
				RepairPlanVO repairPlanVO = new RepairPlanVO();
				repairPlanVO.setSsLocation(getSsLocation()); //세션_공장위치
				repairPlanVO.setSubAuth(getSsSubAuth());	 //세션_조회권한
				repairPlanVO.setSsRemarks(getSsRemarks());   //세션_파트 분리/통합 여부
				repairPlanVO.setSsPart(getSsPart());         //세션_파트

				repairPlanVO.setLOCATION("ALL");             //조회 공장
				
				// 팀장일 경우
				if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
					repairPlanVO.setAuthType(CodeConstants.AUTH_CHIEF);
				}
				// 정비원일 경우
				else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
					repairPlanVO.setAuthType(CodeConstants.AUTH_REPAIR);
					
					// 공무(기계, 전기, 시설) 일 경우
					if("BR00_01".equals(getSsPart()) || "BR00_02".equals(getSsPart()) || "BR00_03".equals(getSsPart())){
						repairPlanVO.setPART("BR00_01,BR00_02,BR00_03");
					}
					// 생산 일 경우
					else if("BR00_04".equals(getSsPart())){
						repairPlanVO.setPART("BR00_04");
					}
					else {
						repairPlanVO.setPART(getSsPart());
					}
					repairPlanVO.setPART(addStringToQuoto(repairPlanVO.getPART()));
				}
				resultMap.put("PLAN_CNT", repairPlanService.selectRepairPlanCnt(repairPlanVO));
				
				// 6-3. 정비 : 뱃지 카운트 및 ToDo목록
				RepairResultVO repairResultVO = new RepairResultVO();
				repairResultVO.setCompanyId(getSsCompanyId());
				repairResultVO.setSsLocation(getSsLocation());	//세션_공장위치
				repairResultVO.setSubAuth(getSsSubAuth());		//세션_조회권한
				repairResultVO.setSsRemarks(getSsRemarks());	//세션_파트 분리/통합 여부
				repairResultVO.setSsPart(getSsPart());			//세션_파트
				repairResultVO.setUserId(getSsUserId());		//세션_아이디
				
				repairResultVO.setLOCATION("ALL");             	//조회 공장
				repairResultVO.setAPPR_STATUS("('02')");		//승인
				repairResultVO.setREPAIR_STATUS("('02')");		//담당자배정
				repairResultVO.setREQUEST_TYPE("ALL");			//오더 유형
				repairResultVO.setFlag("JOIN");					//참여건 조회

				// 팀장일 경우
				if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
					repairResultVO.setAuthType(CodeConstants.AUTH_CHIEF);
				}
				// 정비원일 경우
				else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
					repairResultVO.setAuthType(CodeConstants.AUTH_REPAIR);
					
					// 공무(기계, 전기, 시설) 일 경우
					if("BR00_01".equals(getSsPart()) || "BR00_02".equals(getSsPart()) || "BR00_03".equals(getSsPart())){
						repairResultVO.setPART("BR00_01,BR00_02,BR00_03");
					}
					// 생산 일 경우
					else if("BR00_04".equals(getSsPart())){
						repairResultVO.setPART("BR00_04");
					}
					else {
						repairResultVO.setPART(getSsPart());
					}
					repairResultVO.setPART(addStringToQuoto(repairResultVO.getPART()));
				}
				
				// 6-3-1. 정비 뱃지카운트
				resultMap.put("REPAIR_CNT", repairResultService.selectRepairResultCnt(repairResultVO));
				
				// 6-3-2. 팀장 또는 정비원일 경우, 정비목록 호출
				if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1 || (getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart()))){
					
					// 정비현황 목록 (EPMS_REPAIR_RESULT)
					List<HashMap<String, Object>> result_list = repairResultService.selectRepairResultList2(repairResultVO);
					
					// 정비실적에  등록된 정비원 조회 (EPMS_WORK_LOG)
					List<HashMap<String, Object>> work_log_list = new ArrayList<HashMap<String, Object>>();
					if (result_list != null){
						//정비실적현황 목록의 정비ID가져오기
						List<String> work_id = new ArrayList<String>();
						for (int i=0; i < result_list.size() ; i++ ){
							if(result_list.get(i).get("REPAIR_RESULT") != null){
								work_id.add(String.valueOf(result_list.get(i).get("REPAIR_RESULT")));
							}
						}
						repairResultVO.setWorkIdArr(work_id);
						
						//정비실적에 등록된 정비원 조회
						repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //01:정비실적, 02:고장원인분석

						work_log_list = repairResultService.selectWorkMemberList(repairResultVO);
					}
					
					if(result_list.isEmpty() == false) {
						
						List<HashMap<String, Object>> tmp_list = new ArrayList<HashMap<String, Object>>();
						String REPAIR_RESULT = "";	//정비ID (EPMS_REPAIR_RESULT)
						String WORK_ID = "";		//작업ID (EPMS_WORK_LOG)
						
						for(HashMap<String, Object> result : result_list){
							
							tmp_list = new ArrayList<HashMap<String, Object>>();
							REPAIR_RESULT = String.valueOf(result.get("REPAIR_RESULT"));
							
							for(HashMap<String, Object> item : work_log_list){

								WORK_ID = String.valueOf(item.get("WORK_ID"));
								
								if(REPAIR_RESULT.equals(WORK_ID)){
									tmp_list.add(item);
								}
							}
							result.put("WORK_LOG", tmp_list);
						}
					}
					resultMap.put("REPAIR_LIST", result_list);	
				}
				
				// 6-4. 예방보전 : 뱃지 카운트 및 ToDo목록
//				PreventiveResultVO preventiveResultVO = new PreventiveResultVO();
//				preventiveResultVO.setSubAuth(getSsSubAuth());
//				preventiveResultVO.setUserId(getSsUserId());
//				preventiveResultVO.setCompanyId(getSsCompanyId());
//				preventiveResultVO.setSsPart(getSsPart());
//				
//				resultMap.put("PREVENTIVE_CNT", preventiveResultService.preventiveResultCnt(preventiveResultVO));
//				resultMap.put("PREVENTIVE_LIST", preventiveResultService.preventiveResultList2(preventiveResultVO));
				
				// 6-5. 일반사용자일 경우, 내요청현황 목록 호출 (팀장 또는 정비원이 아닐 경우)
				if(!(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1 || (getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())))){
					
					RepairRequestVO repairRequestVO = new RepairRequestVO();
					repairRequestVO.setCompanyId(getSsCompanyId());
					repairRequestVO.setSsLocation(getSsLocation());
					repairRequestVO.setSubAuth(getSsSubAuth());		//세션_조회권한
					repairRequestVO.setUserId(getSsUserId());
					
					repairRequestVO.setLOCATION("ALL");			//조회 공장
					repairRequestVO.setAPPR_STATUS("ALL");		//결재상태
					repairRequestVO.setREPAIR_STATUS("ALL");	//정비상태
					repairRequestVO.setFlag("MY");				//ALL:전체, MY:내요청
					repairRequestVO.setStartDt(DateTime.requestMmDay("1","yyyyMMdd"));
					repairRequestVO.setEndDt(DateTime.getShortDateString());
					
					// 내 정비요청현황 목록 (EPMS_REPAIR_REQUEST)
					List<HashMap<String, Object>> request_list = repairRequestService.selectRepairRequestList2(repairRequestVO);
					
					// 정비실적에  등록된 정비원 조회 (EPMS_WORK_LOG)
					List<HashMap<String, Object>> work_log_list = new ArrayList<HashMap<String, Object>>();
					if (request_list != null){
						//정비실적현황 목록의 정비ID가져오기
						List<String> work_id = new ArrayList<String>();
						for (int i=0; i < request_list.size() ; i++ ){
							if(request_list.get(i).get("REPAIR_RESULT") != null){
								work_id.add(String.valueOf(request_list.get(i).get("REPAIR_RESULT")));
							}
						}
						repairResultVO.setWorkIdArr(work_id);
						
						//정비실적에 등록된 정비원 조회
						repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //01:정비실적, 02:고장원인분석

						work_log_list = repairResultService.selectWorkMemberList(repairResultVO);
					}
					
					if(request_list.isEmpty() == false) {
						
						List<HashMap<String, Object>> tmp_list = new ArrayList<HashMap<String, Object>>();
						String REPAIR_RESULT = "";	//정비ID (EPMS_REPAIR_RESULT)
						String WORK_ID = "";		//작업ID (EPMS_WORK_LOG)
						
						for(HashMap<String, Object> result : request_list){
							
							tmp_list = new ArrayList<HashMap<String, Object>>();
							REPAIR_RESULT = String.valueOf(result.get("REPAIR_RESULT"));
							
							for(HashMap<String, Object> item : work_log_list){

								WORK_ID = String.valueOf(item.get("WORK_ID"));
								
								if(REPAIR_RESULT.equals(WORK_ID)){
									tmp_list.add(item);
								}
							}
							result.put("WORK_LOG", tmp_list);
						}
					}
					resultMap.put("REQUEST_LIST", request_list);	
				}				
				
				// 7. 유저정보
				HashMap<String, Object> userInfo = memberService.getUserInfo2(memberVO);
				resultMap.put("USER_INFO", userInfo);
				
				// 8. 파트정보
				memberVO.setCompanyId(memberVO.getCOMPANY_ID());
				List<HashMap<String, Object>> partList = memberService.getPartList(memberVO);
				resultMap.put("PART_LIST", partList);
				
				// 9-1. 부서 목록
				DivisionVO divisionVo = new DivisionVO();
				divisionVo.setCompanyId(memberVO.getCOMPANY_ID());
				List<HashMap<String, Object>> divisionList= divisionService.selectDivisionList2(divisionVo);
				resultMap.put("DIVISION_LIST", divisionList);
				
				// 9-2. 사용자
				memberVO.setCompanyId(memberVO.getCOMPANY_ID());
				memberVO.setSsLocation(memberVO.getLOCATION());
				List<HashMap<String, Object>> memberList = memberService.selectMemberList2(memberVO);
				resultMap.put("MEMBER_LIST", memberList);
				
				// 10-1. 설비유형(라인) 목록
				CategoryVO categoryVO = new CategoryVO();
				categoryVO.setSubAuth(memberVO.getSUB_AUTH());
				categoryVO.setCATEGORY_TYPE("LINE");
				List<HashMap<String, Object>> lineList = categoryService.selectCategoryList2(categoryVO);
				resultMap.put("LINE_LIST", lineList);
				
				// 10-2. 설비마스터  목록
				EquipmentMasterVO equipmentMasterVO = new EquipmentMasterVO();
				equipmentMasterVO.setSubAuth(memberVO.getSUB_AUTH());
				equipmentMasterVO.setCATEGORY_TYPE("LINE");
				List<HashMap<String, Object>> equipmentList = equipmentMasterService.selectEquipmentMasterList2(equipmentMasterVO);
				resultMap.put("EQUIPMENT_LIST", equipmentList);
				
				// 11-1. 자재유형 목록
				categoryVO = new CategoryVO();
				categoryVO.setSubAuth(memberVO.getSUB_AUTH());
				categoryVO.setCATEGORY_TYPE("MATERIAL");
				List<HashMap<String, Object>> materialTypeList = categoryService.selectCategoryList2(categoryVO);
				resultMap.put("MATERIAL_TYPE_LIST", materialTypeList);
				
				// 11-2. 자재마스터 목록
				MaterialMasterVO materialMasterVO = new MaterialMasterVO();
				materialMasterVO.setSubAuth(memberVO.getSUB_AUTH());
				materialMasterVO.setCATEGORY_TYPE("MATERIAL");
				List<HashMap<String, Object>> materialList = materialMasterService.selectMaterialMasterList2(materialMasterVO);
				resultMap.put("MATERIAL_LIST", materialList);
				
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * 모바일 로그아웃
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param HttpSession session
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/appLogout.do")
	@ResponseBody
	public Map<String, Object> appLogout(HttpSession session) throws Exception {
		
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			
			// 단말기정보 삭제
			DeviceVO deviceVO = new DeviceVO();
			deviceVO.setUserId(getSsUserId());
			deviceService.deleteDevice(deviceVO);
			
			session.invalidate();

			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
     * Interceptor에서 세션 정보 없을 경우 호출
	 * 
	 * @author 서정민
	 * @since 2018.05.29
	 * @param HttpSession session
	 * @return Map<String, Object>
	 * @throws Exception
     */
    @RequestMapping(value="/redirectLogin.do")
	@ResponseBody
    public Map<String, Object> redirectLogin() throws Exception
    {
    	Map<String, Object> resultMap = new HashMap<>();
    	JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.DIS_SESSION);
    	
        return resultMap;
    }
	
	/**
	 * 세션 재생성
	 * 
	 * @author 서정민
	 * @since 2018.05.29
	 * @param HttpSession session
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/keepAlive.do")
	@ResponseBody
	public Map<String, Object> keepAlive(MemberVO memberVO, HttpSession session) throws Exception {
		
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			
			memberVO = comService.setLogin(memberVO);
			JsonUtil.setMobileNormalMessage(resultMap, memberVO.getResCd());
			
			if(CodeConstants.SUCCESS.equals(memberVO.getResCd()) ) {
				session.removeAttribute("ssHeaderMenuList");
				session.setAttribute("ssMemberVO", memberVO);
				session.setAttribute("ssCheckInout", memberVO.getCHECK_INOUT());
				
				session.setAttribute("ssUserId", memberVO.getUSER_ID());
				session.setAttribute("ssAuth", memberVO.getAUTH());
				session.setAttribute("ssUserNm", memberVO.getUSER_NM());
				session.setAttribute("ssDivision", memberVO.getDIVISION());
				session.setAttribute("ssDivisionNm", memberVO.getDIVISION_NM());
				session.setAttribute("ssJobGradeNm", memberVO.getJOB_GRADE_NM());
				session.setAttribute("ssSubAuth", memberVO.getSUB_AUTH());
				session.setAttribute("ssLocation", memberVO.getLOCATION());
				session.setAttribute("ssPart", memberVO.getPART());
				session.setAttribute("ssMemberId", memberVO.getMEMBER_ID());
				session.setAttribute("ssRemarks", memberVO.getREMARKS());
//				session.setAttribute("ssViewAuth", memberVO.getVIEW_AUTH());
				session.setAttribute("ssCompanyId", memberVO.getCOMPANY_ID());
				session.setAttribute("ssKostl", memberVO.getKOSTL());
				session.setMaxInactiveInterval(Integer.parseInt(Config.getString("session.time")));
				
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * 홈 이동
	 * 
	 * @author 서정민
	 * @since 2018.05.24
	 * @param MemberVO memberVO
	 * @param HttpSession session
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/main.do")
	@ResponseBody
	public Map<String, Object> main(MemberVO memberVO, HttpSession session) throws Exception {
		
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			// 1. 출퇴근 상태
			memberVO.setUSER_ID(getSsUserId());
			HashMap<String, Object> userInfo = memberService.getUserInfo2(memberVO);
			resultMap.put("CHECK_INOUT", userInfo.get("CHECK_INOUT").toString());
			
			// 2. 뱃지 카운트 및 ToDo목록
			// 2-1. 결재 : 뱃지 카운트
			RepairApprVO repairApprVO = new RepairApprVO();
			repairApprVO.setSubAuth(getSsSubAuth());     //세션_조회권한
			repairApprVO.setSsLocation(getSsLocation()); //세션_공장위치
			repairApprVO.setSsRemarks(getSsRemarks());   //세션_파트 분리/통합 여부
			repairApprVO.setSsPart(getSsPart());         //세션_파트

			repairApprVO.setLOCATION("ALL");             //조회 공장
			
			// 팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				repairApprVO.setAuthType(CodeConstants.AUTH_CHIEF);
			}
			// 정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
				repairApprVO.setAuthType(CodeConstants.AUTH_REPAIR);
				
				// 공무(기계, 전기, 시설) 일 경우
				if("BR00_01".equals(getSsPart()) || "BR00_02".equals(getSsPart()) || "BR00_03".equals(getSsPart())){
					repairApprVO.setPART("BR00_01,BR00_02,BR00_03");
				}
				// 생산 일 경우
				else if("BR00_04".equals(getSsPart())){
					repairApprVO.setPART("BR00_04");
				}
				else {
					repairApprVO.setPART(getSsPart());
				}
				repairApprVO.setPART(addStringToQuoto(repairApprVO.getPART()));
			}
			resultMap.put("APPR_CNT", repairApprService.selectRepairApprCnt(repairApprVO));
			
			// 2-2. 담당자배정 : 뱃지 카운트
			RepairPlanVO repairPlanVO = new RepairPlanVO();
			repairPlanVO.setSsLocation(getSsLocation()); //세션_공장위치
			repairPlanVO.setSubAuth(getSsSubAuth());	 //세션_조회권한
			repairPlanVO.setSsRemarks(getSsRemarks());   //세션_파트 분리/통합 여부
			repairPlanVO.setSsPart(getSsPart());         //세션_파트
			
			repairPlanVO.setLOCATION("ALL");             //조회 공장
			
			// 팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				repairPlanVO.setAuthType(CodeConstants.AUTH_CHIEF);
			}
			// 정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
				repairPlanVO.setAuthType(CodeConstants.AUTH_REPAIR);
				
				// 공무(기계, 전기, 시설) 일 경우
				if("BR00_01".equals(getSsPart()) || "BR00_02".equals(getSsPart()) || "BR00_03".equals(getSsPart())){
					repairPlanVO.setPART("BR00_01,BR00_02,BR00_03");
				}
				// 생산 일 경우
				else if("BR00_04".equals(getSsPart())){
					repairPlanVO.setPART("BR00_04");
				}
				else {
					repairPlanVO.setPART(getSsPart());
				}
				repairPlanVO.setPART(addStringToQuoto(repairPlanVO.getPART()));
			}
			resultMap.put("PLAN_CNT", repairPlanService.selectRepairPlanCnt(repairPlanVO));
			
			// 2-3. 정비 : 뱃지 카운트 및 ToDo목록
			RepairResultVO repairResultVO = new RepairResultVO();
			repairResultVO.setCompanyId(getSsCompanyId());
			repairResultVO.setSsLocation(getSsLocation());	//세션_공장위치
			repairResultVO.setSubAuth(getSsSubAuth());		//세션_조회권한
			repairResultVO.setSsRemarks(getSsRemarks());	//세션_파트 분리/통합 여부
			repairResultVO.setSsPart(getSsPart());			//세션_파트
			repairResultVO.setUserId(getSsUserId());		//세션_아이디
			
			repairResultVO.setLOCATION("ALL");				//조회 공장
			repairResultVO.setAPPR_STATUS("('02')");		//승인
			repairResultVO.setREPAIR_STATUS("('02')");		//담당자배정
			repairResultVO.setREQUEST_TYPE("ALL");			//오더 유형
			repairResultVO.setFlag("JOIN");					//참여건 조회

			// 팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				repairResultVO.setAuthType(CodeConstants.AUTH_CHIEF);
			}
			// 정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
				repairResultVO.setAuthType(CodeConstants.AUTH_REPAIR);
				
				// 공무(기계, 전기, 시설) 일 경우
				if("BR00_01".equals(getSsPart()) || "BR00_02".equals(getSsPart()) || "BR00_03".equals(getSsPart())){
					repairResultVO.setPART("BR00_01,BR00_02,BR00_03");
				}
				// 생산 일 경우
				else if("BR00_04".equals(getSsPart())){
					repairResultVO.setPART("BR00_04");
				}
				else {
					repairResultVO.setPART(getSsPart());
				}
				repairResultVO.setPART(addStringToQuoto(repairResultVO.getPART()));
			}
			
			// 2-3-1. 정비 뱃지카운트
			resultMap.put("REPAIR_CNT", repairResultService.selectRepairResultCnt(repairResultVO));
			
			// 2-3-2. 팀장 또는 정비원일 경우, 정비목록 호출
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1 || (getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart()))){
				
				// 정비현황 목록 (EPMS_REPAIR_RESULT)
				List<HashMap<String, Object>> result_list = repairResultService.selectRepairResultList2(repairResultVO);
				
				// 정비실적에  등록된 정비원 조회 (EPMS_REPAIR_RESULT)
				List<HashMap<String, Object>> work_log_list = new ArrayList<HashMap<String, Object>>();
				if (result_list != null){
					//정비실적현황 목록의 정비ID가져오기
					List<String> work_id = new ArrayList<String>();
					for (int i=0; i < result_list.size() ; i++ ){
						if(result_list.get(i).get("REPAIR_RESULT") != null){
							work_id.add(String.valueOf(result_list.get(i).get("REPAIR_RESULT")));
						}
					}
					repairResultVO.setWorkIdArr(work_id);
					
					//정비실적에 등록된 정비원 조회
					repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //01:정비실적, 02:고장원인분석

					work_log_list = repairResultService.selectWorkMemberList(repairResultVO);
				}
				
				if(result_list.isEmpty() == false) {
					
					List<HashMap<String, Object>> tmp_list = new ArrayList<HashMap<String, Object>>();
					String REPAIR_RESULT = "";	//정비ID (EPMS_REPAIR_RESULT)
					String WORK_ID = "";		//작업ID (EPMS_WORK_LOG)
					
					for(HashMap<String, Object> result : result_list){
						
						tmp_list = new ArrayList<HashMap<String, Object>>();
						REPAIR_RESULT = String.valueOf(result.get("REPAIR_RESULT"));
						
						for(HashMap<String, Object> item : work_log_list){

							WORK_ID = String.valueOf(item.get("WORK_ID"));
							
							if(REPAIR_RESULT.equals(WORK_ID)){
								tmp_list.add(item);
							}
						}
						result.put("WORK_LOG", tmp_list);
					}
				}
				resultMap.put("REPAIR_LIST", result_list);
			}
			
			
			// 2-4. 예방보전 : 뱃지 카운트 및 ToDo목록
//			PreventiveResultVO preventiveResultVO = new PreventiveResultVO();
//			preventiveResultVO.setSubAuth(getSsSubAuth());
//			preventiveResultVO.setUserId(getSsUserId());
//			preventiveResultVO.setCompanyId(getSsCompanyId());
//			preventiveResultVO.setSsPart(getSsPart());
//			
//			resultMap.put("PREVENTIVE_CNT", preventiveResultService.preventiveResultCnt(preventiveResultVO));
//			resultMap.put("PREVENTIVE_LIST", preventiveResultService.preventiveResultList2(preventiveResultVO));
			
			
			// 2-5. 일반사용자일 경우, 내요청현황 목록 호출 (팀장 또는 정비원이 아닐 경우)
			if(!(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1 || (getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())))){
				
				RepairRequestVO repairRequestVO = new RepairRequestVO();
				repairRequestVO.setCompanyId(getSsCompanyId());
				repairRequestVO.setSsLocation(getSsLocation());
				repairRequestVO.setSubAuth(getSsSubAuth());		//세션_조회권한
				repairRequestVO.setUserId(getSsUserId());
				
				repairRequestVO.setLOCATION("ALL");			//조회 공장
				repairRequestVO.setAPPR_STATUS("ALL");		//결재상태
				repairRequestVO.setREPAIR_STATUS("ALL");	//정비상태
				repairRequestVO.setFlag("MY");				//ALL:전체, MY:내요청
				repairRequestVO.setStartDt(DateTime.requestMmDay("1","yyyyMMdd"));
				repairRequestVO.setEndDt(DateTime.getShortDateString());
				
				// 내 정비요청현황 목록 (EPMS_REPAIR_REQUEST)
				List<HashMap<String, Object>> request_list = repairRequestService.selectRepairRequestList2(repairRequestVO);
				
				// 정비실적에  등록된 정비원 조회 (EPMS_WORK_LOG)
				List<HashMap<String, Object>> work_log_list = new ArrayList<HashMap<String, Object>>();
				if (request_list != null){
					//정비실적현황 목록의 정비ID가져오기
					List<String> work_id = new ArrayList<String>();
					for (int i=0; i < request_list.size() ; i++ ){
						if(request_list.get(i).get("REPAIR_RESULT") != null){
							work_id.add(String.valueOf(request_list.get(i).get("REPAIR_RESULT")));
						}
					}
					repairResultVO.setWorkIdArr(work_id);
					
					//정비실적에 등록된 정비원 조회
					repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //01:정비실적, 02:고장원인분석

					work_log_list = repairResultService.selectWorkMemberList(repairResultVO);
				}
				
				if(request_list.isEmpty() == false) {
					
					List<HashMap<String, Object>> tmp_list = new ArrayList<HashMap<String, Object>>();
					String REPAIR_RESULT = "";	//정비ID (EPMS_REPAIR_RESULT)
					String WORK_ID = "";		//작업ID (EPMS_WORK_LOG)
					
					for(HashMap<String, Object> result : request_list){
						
						tmp_list = new ArrayList<HashMap<String, Object>>();
						REPAIR_RESULT = String.valueOf(result.get("REPAIR_RESULT"));
						
						for(HashMap<String, Object> item : work_log_list){

							WORK_ID = String.valueOf(item.get("WORK_ID"));
							
							if(REPAIR_RESULT.equals(WORK_ID)){
								tmp_list.add(item);
							}
						}
						result.put("WORK_LOG", tmp_list);
					}
				}
				resultMap.put("REQUEST_LIST", request_list);
			}
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
				
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}

	/**
	 * 모바일 - 비밀번호 변경
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param MemberVO memberVO 
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/changePassword.do")
	@ResponseBody
	public Map<String, Object> changePassword(MemberVO memberVO) throws Exception {
		
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			
			if (StringUtils.isEmpty(memberVO.getUserId())
					|| StringUtils.isEmpty(memberVO.getPassword())
					|| StringUtils.isEmpty(memberVO.getNewPw())) {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.PARAM_NULL);
			} else {
				
				memberService.changeUserPassword(memberVO);
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * 모바일 - Oracle IN절 입력 시 Quoto처리 
	 * 
	 * @author 김영환
	 * 
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	public String addStringToQuoto(String originString){
		
		if("".equals(originString) || originString == null) {
			return "\'\'";
		} 
		else if("ALL".equals(originString)) {
			return "ALL";
		} 
		else {
			String[] tempArray = originString.split(",");
			StringBuffer tempBuffer = new StringBuffer();
			
			if(tempArray.length == 1) {
				tempBuffer.append("(\'"+tempArray[0]+"\')");
			} else {
				for(int i=0; i < tempArray.length; i++){
					if(i == 0)							tempBuffer.append("(\'"+tempArray[i]+"\', ");	// 시작값
					else if( i == tempArray.length-1)	tempBuffer.append("\'"+tempArray[i]+"\')");		// 마지막값
					else 								tempBuffer.append("\'"+tempArray[i]+"\', ");	// 중간값
				}
			}
			
			return tempBuffer.toString();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	/**
	 * 모바일 - 부서/사용자 목록 조회
	 * @author 강승상
	 * @since 2017.09.21
	 * @param companyId 회사 코드
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getMemberList.do")
	@ResponseBody
	public Map<String, Object> memberList(@RequestParam("companyId") String _companyId) throws Exception {
		Map<String, Object> resultMap = new HashMap<>();
		try {
			if (StringUtils.isEmpty(_companyId)) {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.PARAM_NULL);
			} else {
				resultMap.put("MEMBER_LIST", comService.getMemberList(_companyId));
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return resultMap;
	}

	@RequestMapping(value = "/testPush.do")
	public String testPush() throws Exception {
		try {

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return "/testPush";
	}

	@RequestMapping(value = "/testPushAjax.do")
	@ResponseBody
	public Map<String, Object> testPushAjax(String PUSH_ID, String MEMBER, String CD_PLATFORM, String title, String actionUrl, String topName) throws Exception {
		Map<String, Object> resultMap = new HashMap<>();
		try {
			PushThread pt = new PushThread();
			DeviceVO vo = new DeviceVO();
			vo.setPUSH_ID(PUSH_ID);
			vo.setUSER_ID(MEMBER);
			vo.setCD_PLATFORM(CD_PLATFORM);

			List<DeviceVO> deviceVOList = new ArrayList<>();
			deviceVOList.add(vo);

//			pt.setParam(deviceVOList, title, actionUrl, "TMP_MENUID", topName);
			pt.start();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return resultMap;
	}
	
}
