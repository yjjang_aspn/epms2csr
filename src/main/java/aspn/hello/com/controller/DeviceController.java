package aspn.hello.com.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.hello.com.model.DeviceVO;
import aspn.hello.com.service.DeviceService;

/**
 * 단말기  컨트롤러 클래스
 * @author 이영탁
 * @since 2017.09.06
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.09.06  이영탁          최초 생성
*
 * </pre>
 */

@Controller
@RequestMapping("/device")
public class DeviceController {
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/* INSTANCE VAR */
	@Autowired
	DeviceService deviceService;
	/* INSTANCE VAR */
	
	@RequestMapping(value = "/setDevice.do")
	@ResponseBody
	public HashMap<String, Object> setDevice(HttpServletRequest request, HttpServletResponse response, DeviceVO deviceVO, Model model) throws Exception {
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();

		deviceService.setDevice(deviceVO);
		
		rtnMap.put("resCd", deviceVO.getResCd());
		rtnMap.put("resMsg", deviceVO.getResMsg());
		
		return rtnMap;
	}
}
