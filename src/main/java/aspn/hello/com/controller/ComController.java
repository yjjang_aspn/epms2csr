package aspn.hello.com.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.Config;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.JsonUtil;
import aspn.com.common.util.SecretUtils;
import aspn.hello.com.model.ComVO;
import aspn.hello.com.service.ComService;
import aspn.hello.epms.preventive.result.service.PreventiveResultService;
import aspn.hello.mem.model.MemberVO;
import aspn.hello.mem.service.MemberService;

/**
 * 공통  컨트롤러 클래스
 * @author 이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see
 * 
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2017.07.04		이영탁			최초 생성
 *   
 * </pre>
 */
@Controller
public class ComController extends ContSupport{
	Logger logger = LoggerFactory.getLogger(this.getClass());	

	@Autowired
	ComService comService;
	
	@Autowired
	PreventiveResultService preventiveResultService;
	
	@Autowired
	MemberService memberService;
	
	/**
	 * 로그인 화면이동
	 * @author 이영탁
	 * @since 2017.07.04
	 * @param session
	 * @return "/com/loginForm"
	 * @throws Exception
	 */
	/*@RequestMapping(value = "/loginForm.do")
	public String loginForm(HttpSession session, Model model, HttpServletRequest request) throws Exception {
		
		String userAgent = request.getHeader("USER-Agent");
		if (userAgent.indexOf("iPhone") > -1 || userAgent.indexOf("iPad") > -1 || userAgent.indexOf("Android") > -1) 
		{
			model.addAttribute("deviceInfo", "mobile");
		}
		
		//session.invalidate();
		return "/com/web/loginForm";
	}*/
	
	/**
     * 페이지 redirect
     * @return returnPage 리턴페이지URL
     * @exception Exception
     */
    @RequestMapping(value="/redirectLogin.do")
    public String redirectLogin() throws Exception
    {
        return "/com/web/redirectLogin";
    }
	
	/**
	 * 로그인 처리
	 * @param request
	 * @param response
	 * @param memberVO
	 * @param model
	 * @param session
	 * @return "redirect:/pr/vendor/vendorInputDetailFrm.do
	 * @return "redirect:/order/main/orderMain.do"
	 * @return "redirect:/order/main/orderMain.do"
	 * @return "/com/web/loginForm"
	 * @throws Exception
	 */
	@RequestMapping(value = "/webLogin.do")
	public String webLogin(HttpServletRequest request, HttpServletResponse response, MemberVO memberVO, Model model, HttpSession session ) throws Exception {
		
		@SuppressWarnings("unused")
		String userIp = memberVO.getUserIp();

		String userAgent = request.getHeader("USER-Agent");
		String[] mobileType = {"iPhone", "iPad", "iPod", "Android", "BlackBerry", "Windows CE", "SAMSUNG", "LG", "MOT", "Nokia", "SonyEricsson", "IEMobile"};
		for(int i=0; i<mobileType.length; i++){
			if (userAgent.indexOf(mobileType[i]) > -1){
				model.addAttribute("deviceInfo", "mobile");
			}
		}
		
		if(memberVO.getLogoutYn().equals("Y")){
			return  "/com/web/loginForm";
		}
		
		if(null == this.getSsUserId() ){
			if(!memberVO.getUserId().equals("")){
				
				memberVO = comService.setLogin(memberVO);
				
				if(CodeConstants.SUCCESS.equals(memberVO.getResCd()) ) {			
		
					session.removeAttribute("ssHeaderMenuList");
					session.setAttribute("ssMemberVO", memberVO);
					session.setAttribute("ssCheckInout", memberVO.getCHECK_INOUT());
					
					session.setAttribute("ssUserId", memberVO.getUSER_ID());
					session.setAttribute("ssAuth", memberVO.getAUTH());
					session.setAttribute("ssUserNm", memberVO.getUSER_NM());
					session.setAttribute("ssDivision", memberVO.getDIVISION());
					session.setAttribute("ssDivisionNm", memberVO.getDIVISION_NM());
					session.setAttribute("ssJobGradeNm", memberVO.getJOB_GRADE_NM());
					session.setAttribute("ssSubAuth", memberVO.getSUB_AUTH());
					session.setAttribute("ssLocation", memberVO.getLOCATION());
					session.setAttribute("ssPart", memberVO.getPART());
					session.setAttribute("ssMemberId", memberVO.getMEMBER_ID());
					session.setAttribute("ssRemarks", memberVO.getREMARKS());
//						session.setAttribute("ssViewAuth", memberVO.getVIEW_AUTH());
					session.setAttribute("ssCompanyId", memberVO.getCOMPANY_ID());
					session.setAttribute("ssKostl", memberVO.getKOSTL());
					session.setAttribute("ssUserThumbUrl", memberVO.getUSER_THUMBURL());
					session.setMaxInactiveInterval(Integer.parseInt(Config.getString("session.time")));
					
					// 공급사 로그인시 페이지
					if (CodeConstants.USER_GUBUN_VENDOR.equals(memberVO.getUserGubun())) {
						return "redirect:/pr/vendor/vendorInputDetailFrm.do";
					} else if(CodeConstants.USER_GUBUN_CUSTOMER.equals(memberVO.getUserGubun())) {
						return "redirect:/order/main/orderMain.do";				
					} else {
						return "redirect:/main.do";
					}		
					
				} else {
					String returnMsg = memberVO.getResMsg();
					
					if(CodeConstants.INVALID_PASSWORD.equals(memberVO.getResCd()) ) {
						returnMsg = returnMsg + " 암호를 다시 입력하십시오.\\n(남은 재시도 횟수 : "+ (5 - Integer.parseInt(memberVO.getFailCnt())) + "" +")";
					}
					
					model.addAttribute("resMsg", returnMsg);
					return  "/com/web/loginForm";
				}
			}else{
				
				return  "/com/web/loginForm";
			}
		}else{			
			return  "redirect:/main.do";
		}
	}
	
	/**
	 * 메인화면 이동
	 * @return "/com/web/main"
	 * @throws Exception
	 */
	@RequestMapping(value = "/main.do")
	public String mainMenu(MemberVO memberVO, Model model) throws Exception {
		HashMap<String, Object> memberInfo = new HashMap<String, Object>();
		memberVO.setUSER_ID(getSsUserId());
		memberInfo = memberService.getUserInfo(memberVO);
		
		// 1.비밀번호 초기화일 경우(ID와 비밀번호 동일)
		String pwd_reset = null;
		String secret_user_id = SecretUtils.encryptSha256(SecretUtils.encryptMd5(memberVO.getUSER_ID()));
		
		if(memberInfo.get("PASSWORD").equals(secret_user_id)){
			pwd_reset = "Y";
		}else {
			pwd_reset = "N";
		}
		model.addAttribute("pwd_reset", pwd_reset);
		
		// 2. 비밀번호 변경 180일 초과일 경우
		String pwd_change = null;
		String DATE_P = String.valueOf(memberInfo.get("DATE_P"));
		String pwdChgDt = DateTime.diffDate(DATE_P, DateTime.getShortDateString());
		
		if(Integer.parseInt(pwdChgDt) > 180) {
			pwd_change = "Y";
		}
		else{
			pwd_change = "N";
		}
		model.addAttribute("pwd_change", pwd_change);
		
		return  "/com/web/main";
		
	}
	
	/**
	 * 대시보드 이동
	 * @return "/com/web/dashBoard"
	 * @throws Exception
	 */
	@RequestMapping(value = "/dashBoard.do")
	public String dashBoard( Model model) throws Exception {
		return  "/com/web/dashBoard";
		
	}
	
	
	
	/**
	 * 헤더 메뉴 조회
	 * @param request
	 * @param response
	 * @param menuMd
	 * @param model
	 * @return "/com/web/headerMenuBar"
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/headerMenuBar.do")
	public String headerMenuBar(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		model.addAttribute("headerMenuList", (ArrayList<HashMap<String, String>>) session.getAttribute("ssHeaderMenuList"));
		return  "/com/web/headerMenuBar";
		
	}

	
	/**
	 * 로그아웃
	 * @param 
	 * @param model
	 * @return "logout"
	 * @exception Exception
	 */
	@RequestMapping(value = "/logout.do")
	public String logout(HttpSession  session) throws Exception {

		session.invalidate();
		return "redirect:/";
	}

	/** 
	 * 공통 Layout page
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/**/*Layout.do")
	public String loadMain(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		String contextPath = request.getContextPath();
		String uri = (String)request.getAttribute("javax.servlet.include.request_uri");
//		logger.info("contextPath : " + contextPath);
//		logger.info("uri : " + uri);


		if(uri == null || uri.trim().equals("")) {
			uri = request.getRequestURI();
		}

//		if(logger.isDebugEnabled()) {
//			logger.info("URI : " + uri);
//			logger.info("ContectPath : " + contextPath);
//		}

		int begin = 0;
		if(!((contextPath == null) || ("".equals(contextPath)))) {
			begin = contextPath.length();
		}

//		if(logger.isDebugEnabled()) {
//			logger.info("Begin : " + begin);
//		}

		int end;
		if(uri.indexOf(";") != -1) {
			end = uri.indexOf(";");
		} else if(uri.indexOf("?") != -1) {
			end = uri.indexOf("?");
		} else {
			end = uri.length();
		}

		String fileName = uri.substring(begin, end);
		if(fileName.indexOf(".") != -1) {
		        fileName = fileName.substring(0, fileName.lastIndexOf("."));
		}
//		System.out.println("---------------------------- fileName1 : " + fileName);
    	
        return fileName;
	}
	
	/**
	 * 메인 상단바 정보(정비/예방보전 카운트)
	 * @author 김영환
	 * @since 2018.06.14
	 * @throws Exception
	 */
	@RequestMapping(value = "/dashboardInfo.do")
	@ResponseBody
	public Map<String, Object> dashboardInfo(ComVO comVO, HttpServletRequest request) throws Exception {
		Map<String, Object> resultMap = new HashMap<>();
		try {
			
//			comVO.setSsLocation(getSsLocation());
			comVO.setSubAuth(getSsSubAuth());
			comVO.setSsPart(getSsPart());
			
			//팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				comVO.setAuthType(CodeConstants.AUTH_CHIEF);
			}
			//정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
				comVO.setAuthType(CodeConstants.AUTH_REPAIR);
			}
			
			// 1. 정비현황 : 정비요청, 정비완료, 진행중, 반려, 정비취소
			HashMap<String, Object> repairInfo= comService.selectRepairInfo(comVO);
			resultMap.put("repairInfo", repairInfo);
			
			// 2019.03.25 던킨 요청으로 사용안함
			// 2: 예방보전현황 : 점검 완료, 미점검 완료, 진행중
//			HashMap<String, Object> preventiveInfo= comService.selectPreventiveInfo(comVO);
//			resultMap.put("preventiveInfo", preventiveInfo);
			

		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
}
