package aspn.hello.com.model;

import java.io.Serializable;

/**
 * [공통] VO Class
 * @author 이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2017.07.04		이영탁			최초 생성
 *   2018.02.08		jmseo		설비관리 변수 추가 : subAuth
 *   2017.02.20		jychoi		시작일, 마감일, 수정가능 변수 추가 : startDt, endDt, editYn
 *   2017.02.26		jychoi		버튼선택, 첨부파일 변수 추가 : flag, attachExistYn
 *   
 * </pre>
 */

public class AbstractVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String uploadData   		= ""; //트리그리드데이터
	private String userId				= ""; //사용자Id
	private String companyId			= ""; //회사
	private String subAuth				= ""; //조회권한
	private String startDt				= ""; //시작일
	private String endDt				= ""; //마감일
	private String ssLocation			= ""; //세션 위치
	private String ssPart				= ""; //세션 파트
	private String ssRemarks			= ""; //세션 파트구분
	private String editYn				= ""; //수정가능 여부(Y:가능,N:불가)
	private String attachExistYn		= ""; //첨부파일 유무
	private String flag					= ""; //버튼 선택(1:등록(수정),2:삭제)
	private String searchDt				= ""; //검색 일
	private String srcType				= ""; //검색 구분
	
	public String getUploadData() {
		return uploadData;
	}

	public void setUploadData(String uploadData) {
		this.uploadData = uploadData;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getSubAuth() {
		return subAuth;
	}

	public void setSubAuth(String subAuth) {
		this.subAuth = subAuth;
	}

	public String getStartDt() {
		return startDt;
	}

	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}

	public String getEndDt() {
		return endDt;
	}

	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}

	public String getSsLocation() {
		return ssLocation;
	}

	public void setSsLocation(String ssLocation) {
		this.ssLocation = ssLocation;
	}

	public String getSsPart() {
		return ssPart;
	}

	public void setSsPart(String ssPart) {
		this.ssPart = ssPart;
	}

	public String getSsRemarks() {
		return ssRemarks;
	}

	public void setSsRemarks(String ssRemarks) {
		this.ssRemarks = ssRemarks;
	}

	public String getEditYn() {
		return editYn;
	}

	public void setEditYn(String editYn) {
		this.editYn = editYn;
	}

	public String getAttachExistYn() {
		return attachExistYn;
	}

	public void setAttachExistYn(String attachExistYn) {
		this.attachExistYn = attachExistYn;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getSearchDt() {
		return searchDt;
	}

	public void setSearchDt(String searchDt) {
		this.searchDt = searchDt;
	}

	public String getSrcType() {
		return srcType;
	}

	public void setSrcType(String srcType) {
		this.srcType = srcType;
	}

	@Override
	public String toString() {
		return "AbstractVO{" +
				"uploadData='"  	+ uploadData + '\'' +
				", userId='" 		+ userId + '\'' +
				", companyId='" 	+ companyId + '\'' +
				", subAuth='" 		+ subAuth + '\'' +
				", startDt='" 		+ startDt + '\'' +
				", endDt='" 		+ endDt + '\'' +
				", editYn='" 		+ editYn + '\'' +
				", attachExistYn='" + attachExistYn + '\'' +
				", flag='" 			+ flag + '\'' +
				'}';
	}
}
