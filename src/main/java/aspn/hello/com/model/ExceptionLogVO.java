package aspn.hello.com.model;

import aspn.hello.mem.model.MemberVO;

/**
 * 에러로깅 VO
 * @author 오명석
 * @since 2017.08.02
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      				수정자          수정내용
 *  ---------------    		------------   ---------------------------
 *   2017.08.02  		오명석          최초 생성
*
 * </pre>
 */

public class ExceptionLogVO extends MemberVO{

	private static final long serialVersionUID = 1L;
	
	private String logger   = "";    //발생경로
	private String message   = "";      //발생메세지
	public String getLogger() {
		return logger;
	}
	public void setLogger(String logger) {
		this.logger = logger;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	

}
