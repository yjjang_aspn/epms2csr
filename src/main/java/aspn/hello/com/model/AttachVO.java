package aspn.hello.com.model;

import java.util.Arrays;

/**
 * 
 * @FileName    : FileInfo.java
 * @Project     : kr.co.aspn
 * @Date        : 2014. 09. 12.
 * @author      : PSJ
 * @Description : 파일정보 모델 객체 정의
 * @History     : 최초작성 ( 2014. 09. 12 )
 */
public class AttachVO extends AbstractVO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String ATTACH_GRP_NO = "";  //파일그룹번호
	private String ATTACH        = "";  //파일번호
	private String MODULE        = "";  //모듈아이디(5:자산, 8:경비, 10:고객관리)
	private String CONTENTS      = "";  //콘텐츠아이디
	private String CD_FILE_TYPE  = "";  //파일구분
	private String NAME          = "";  //파일명
	private String PATH          = "";  //파일경로
	private String FORMAT        = "";  //파일포멧
	private Long FILE_SIZE       = 0L;  //파일크기
	private int SEQ_DSP          = 0;   //출력순서
	private String MEMO          = "";  //설명
	private String CNT_DOWNLOAD  = "";  //다운로드횟수
	private String DATE_C        = ""; //등록일
	private String DATE_E        = "";  //수정일
	private String CD_DEL        = "";  //삭제
	private String FILE_SIZE_TXT = "";  //파일크기 변환
	private String DEL_SEQ		 = "";  
	
	private String flag			 = "";	//동작 여부
	private String myInfo		 = "";	//계정관리여부
	private String [] arrDelSeq;
	private String fileGb		 = "";
	private String fileExp		 = "";

	private String FILE_URL		 = "";
	private String FILE_NAME	 = "";	//계정관리여부
	private String zipFileName	 = "";	// 압축 파일 이름
	
	/** MATERIAL MASTER DB model  */
	private String MATERIAL			= ""; //자재번호

	
	
	public String getZipFileName() {
		return zipFileName;
	}

	public void setZipFileName(String zipFileName) {
		this.zipFileName = zipFileName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String[] getArrDelSeq() {
		return arrDelSeq;
	}

	public void setArrDelSeq(String[] arrDelSeq) {
		this.arrDelSeq = arrDelSeq;
	}


	public String getFileGb() {
		return fileGb;
	}

	public void setFileGb(String fileGb) {
		this.fileGb = fileGb;
	}

	public String getFileExp() {
		return fileExp;
	}

	public void setFileExp(String fileExp) {
		this.fileExp = fileExp;
	}

	public String getDEL_SEQ() {
		return DEL_SEQ;
	}

	public void setDEL_SEQ(String dEL_SEQ) {
		DEL_SEQ = dEL_SEQ;
	}

	public String getATTACH_GRP_NO() {
		return ATTACH_GRP_NO;
	}

	public void setATTACH_GRP_NO(String aTTACH_GRP_NO) {
		ATTACH_GRP_NO = aTTACH_GRP_NO;
	}

	public String getATTACH() {
		return ATTACH;
	}

	public void setATTACH(String aTTACH) {
		ATTACH = aTTACH;
	}

	public String getMODULE() {
		return MODULE;
	}

	public void setMODULE(String mODULE) {
		MODULE = mODULE;
	}

	public String getCONTENTS() {
		return CONTENTS;
	}

	public void setCONTENTS(String cONTENTS) {
		CONTENTS = cONTENTS;
	}

	public String getCD_FILE_TYPE() {
		return CD_FILE_TYPE;
	}

	public void setCD_FILE_TYPE(String cD_FILE_TYPE) {
		CD_FILE_TYPE = cD_FILE_TYPE;
	}

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public String getPATH() {
		return PATH;
	}

	public void setPATH(String pATH) {
		PATH = pATH;
	}

	public String getFORMAT() {
		return FORMAT;
	}

	public void setFORMAT(String fORMAT) {
		FORMAT = fORMAT;
	}

	public Long getFILE_SIZE() {
		return FILE_SIZE;
	}

	public void setFILE_SIZE(Long fILE_SIZE) {
		FILE_SIZE = fILE_SIZE;
	}

	public int getSEQ_DSP() {
		return SEQ_DSP;
	}

	public void setSEQ_DSP(int sEQ_DSP) {
		SEQ_DSP = sEQ_DSP;
	}

	public String getMEMO() {
		return MEMO;
	}

	public void setMEMO(String mEMO) {
		MEMO = mEMO;
	}

	public String getCNT_DOWNLOAD() {
		return CNT_DOWNLOAD;
	}

	public void setCNT_DOWNLOAD(String cNT_DOWNLOAD) {
		CNT_DOWNLOAD = cNT_DOWNLOAD;
	}

	public String getDATE_C() {
		return DATE_C;
	}

	public void setDATE_C(String dATE_C) {
		DATE_C = dATE_C;
	}

	public String getDATE_E() {
		return DATE_E;
	}

	public void setDATE_E(String dATE_E) {
		DATE_E = dATE_E;
	}

	public String getCD_DEL() {
		return CD_DEL;
	}

	public void setCD_DEL(String cD_DEL) {
		CD_DEL = cD_DEL;
	}

	public String getFILE_SIZE_TXT() {
		return FILE_SIZE_TXT;
	}

	public void setFILE_SIZE_TXT(String fILE_SIZE_TXT) {
		FILE_SIZE_TXT = fILE_SIZE_TXT;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getFILE_NAME() {
		return FILE_NAME;
	}

	public void setFILE_NAME(String fILE_NAME) {
		FILE_NAME = fILE_NAME;
	}

	public void setFileInfo(String fileGrpNo, String realFileNm, String fileDir, long fileSize,
			String fileForMat, int i) {
		
		this.setATTACH_GRP_NO(fileGrpNo);
		this.setMODULE("6");
		this.setNAME(realFileNm);
		this.setPATH(fileDir);
		this.setFILE_SIZE(fileSize);
		this.setFORMAT(realFileNm.substring(realFileNm.lastIndexOf(".") + 1, realFileNm.length()));
		this.setSEQ_DSP(i + 1);
		this.setCD_DEL("1");
		
	}

	public String getMyInfo() {
		return myInfo;
	}

	public void setMyInfo(String myInfo) {
		this.myInfo = myInfo;
	}

	public String getFILE_URL() {
		return FILE_URL;
	}

	public void setFILE_URL(String FILE_URL) {
		this.FILE_URL = FILE_URL;
	}
	
	public String getMATERIAL() {
		return MATERIAL;
	}
	public void setMATERIAL(String mATERIAL) {
		MATERIAL = mATERIAL;
	}

	@Override
	public String toString() {
		return "AttachVO{" +
				"ATTACH_GRP_NO='" + ATTACH_GRP_NO + '\'' +
				", ATTACH='" + ATTACH + '\'' +
				", MODULE='" + MODULE + '\'' +
				", CONTENTS='" + CONTENTS + '\'' +
				", CD_FILE_TYPE='" + CD_FILE_TYPE + '\'' +
				", NAME='" + NAME + '\'' +
				", PATH='" + PATH + '\'' +
				", FORMAT='" + FORMAT + '\'' +
				", FILE_SIZE=" + FILE_SIZE +
				", SEQ_DSP=" + SEQ_DSP +
				", MEMO='" + MEMO + '\'' +
				", CNT_DOWNLOAD='" + CNT_DOWNLOAD + '\'' +
				", DATE_C='" + DATE_C + '\'' +
				", DATE_E='" + DATE_E + '\'' +
				", CD_DEL='" + CD_DEL + '\'' +
				", FILE_SIZE_TXT='" + FILE_SIZE_TXT + '\'' +
				", DEL_SEQ='" + DEL_SEQ + '\'' +
				", flag='" + flag + '\'' +
				", myInfo='" + myInfo + '\'' +
				", arrDelSeq=" + Arrays.toString(arrDelSeq) +
				", fileGb='" + fileGb + '\'' +
				", fileExp='" + fileExp + '\'' +
				", FILE_URL='" + FILE_URL + '\'' +
				"} " + super.toString();
	}
	
	
}