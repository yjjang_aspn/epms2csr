package aspn.hello.com.model;

/**
 * [메인 헤더 및 공통] VO Class
 * @author 김영환
 * @since 2018.11.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.11.10		김영환			최초 생성
 *   
 * </pre>
 */

public class ComVO extends AbstractVO{

	private static final long serialVersionUID = 1L;
	
	/** Preventive result */
	private String MANAGER			= "";   // 담당자
	
	private String PARAM_DATE 		= "";	// 검색날짜
	private String PARAM_PART 		= "";	// 검색파트
	private String PARAM_LOCATION 	= "";	// 검색공장
	
	/** COMCD */
	private String UPPER_COMCD		= "";	// 상위코드
	private String COMPANY_ID		= "";	// 회사코드
	private String LOCATION			= "";	// 공장코드
	private String REMARKS			= "";   // 비고(공장파트구분 01:파트구분,02:파트통합)
	
	/** parameter model */
	private String authType			= "";	// 권한여부
	
	public String getMANAGER() {
		return MANAGER;
	}
	public void setMANAGER(String mANAGER) {
		MANAGER = mANAGER;
	}
	public String getPARAM_DATE() {
		return PARAM_DATE;
	}
	public void setPARAM_DATE(String pARAM_DATE) {
		PARAM_DATE = pARAM_DATE;
	}
	public String getPARAM_PART() {
		return PARAM_PART;
	}
	public void setPARAM_PART(String pARAM_PART) {
		PARAM_PART = pARAM_PART;
	}
	public String getPARAM_LOCATION() {
		return PARAM_LOCATION;
	}
	public void setPARAM_LOCATION(String pARAM_LOCATION) {
		PARAM_LOCATION = pARAM_LOCATION;
	}
	public String getUPPER_COMCD() {
		return UPPER_COMCD;
	}
	public void setUPPER_COMCD(String uPPER_COMCD) {
		UPPER_COMCD = uPPER_COMCD;
	}
	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}
	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getREMARKS() {
		return REMARKS;
	}
	public void setREMARKS(String rEMARKS) {
		REMARKS = rEMARKS;
	}
	public String getAuthType() {
		return authType;
	}
	public void setAuthType(String authType) {
		this.authType = authType;
	}
	
}
