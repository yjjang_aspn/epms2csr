package aspn.hello.com.model;

/**
 * [단말기] VO Class
 * @author 서정민
 * @since 2018.04.11
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.11		서정민			최초 생성
 *   
 * </pre>
 */
public class DeviceVO extends AbstractVO{

private static final long serialVersionUID = 1L;
	
	/** DEVICE DB  */
	private String DEVICE			= "";	//단말기번호 
	private String USER_ID           = "";	//회원아이디 
	private String DEVICE_NM        = "";   //단말기명
	private String DEVICE_VER       = "";   //단말기 버젼
	private String PUSH_ID          = "";   //푸쉬아이디
	private String APP_VER          = "";   //앱 버젼
	private String CD_PLATFORM      = "";   //플랫폼
	private String CD_LOSS          = "";   //분실여부
	private String REG_ID           = "";   //등록자
	private String REG_DT           = "";   //등록일
	private String UPD_ID           = "";   //수정자
	private String UPD_DT           = "";   //수정일
	private String DEL_YN 			= "";  	//삭제여부
	private String SEQ_DEVICE		= "";  	//단말기시퀀스
	
	/** EPMS_REPAIR_REQUEST DB  */
	private String REPAIR_REQUEST   = "";   //정비요청ID
	
	/** EPMS_REPAIR_RESULT DB  */
	private String REPAIR_RESULT    = "";   //정비실적ID
	
	/** parameter model */
	private String bpush_userId		= "";  	//바이두 푸쉬 유저아이디
	private String bpush_channelId	= "";  	//바이두 푸쉬 채널아이디
	private String mainMsg			= "";	//전달 메세지
	private String actionUrl		= "";	//푸쉬 확인 후 전송될 url
	private String menuId			= "";	//푸쉬 확인 시 메뉴 id
	private String topName			= "";	//푸쉬 확인 시 표현될 상단 명칭
	private String pushTitle		= ""; 	// 고도화 >> 푸시 제목
	private String pushMenuType		= "";	// 고도화 >> 푸시 메뉴 타입(0-공지사항, 1-반려/승인취소, 2-결재요청)
	private String pushMenuDetailNdx = "";  // 고도화 >> 상세 NDX 값
	private String pushAlarm		= "";  // 알림음 유형(01:일반, 02:긴급)
	private String resCd			= "";
	private String resMsg			= "";
	
	/** chat model */
	private String message			= "";	//메시지 내용
	private String roomName			= "";	//메시지 방 이름
	private String fromUserId		= "";	//메시지 보내는 사람

	private String[] trgtMemberArray = null;
	private String[] trgtPushIdArray = null;
	private String[] trgtPlatFmArray = null;	
	private String[] trgtBPushIdArray = null;
	private String[] trgtBChanneldArray = null;
	
	public String getDEVICE() {
		return DEVICE;
	}
	public void setDEVICE(String dEVICE) {
		DEVICE = dEVICE;
	}
	public String getUSER_ID() {
		return USER_ID;
	}
	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}
	public String getDEVICE_NM() {
		return DEVICE_NM;
	}
	public void setDEVICE_NM(String dEVICE_NM) {
		DEVICE_NM = dEVICE_NM;
	}
	public String getDEVICE_VER() {
		return DEVICE_VER;
	}
	public void setDEVICE_VER(String dEVICE_VER) {
		DEVICE_VER = dEVICE_VER;
	}
	public String getPUSH_ID() {
		return PUSH_ID;
	}
	public void setPUSH_ID(String pUSH_ID) {
		PUSH_ID = pUSH_ID;
	}
	public String getAPP_VER() {
		return APP_VER;
	}
	public void setAPP_VER(String aPP_VER) {
		APP_VER = aPP_VER;
	}
	public String getCD_PLATFORM() {
		return CD_PLATFORM;
	}
	public void setCD_PLATFORM(String cD_PLATFORM) {
		CD_PLATFORM = cD_PLATFORM;
	}
	public String getCD_LOSS() {
		return CD_LOSS;
	}
	public void setCD_LOSS(String cD_LOSS) {
		CD_LOSS = cD_LOSS;
	}
	public String getREG_ID() {
		return REG_ID;
	}
	public void setREG_ID(String rEG_ID) {
		REG_ID = rEG_ID;
	}
	public String getREG_DT() {
		return REG_DT;
	}
	public void setREG_DT(String rEG_DT) {
		REG_DT = rEG_DT;
	}
	public String getUPD_ID() {
		return UPD_ID;
	}
	public void setUPD_ID(String uPD_ID) {
		UPD_ID = uPD_ID;
	}
	public String getUPD_DT() {
		return UPD_DT;
	}
	public void setUPD_DT(String uPD_DT) {
		UPD_DT = uPD_DT;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getSEQ_DEVICE() {
		return SEQ_DEVICE;
	}
	public void setSEQ_DEVICE(String sEQ_DEVICE) {
		SEQ_DEVICE = sEQ_DEVICE;
	}
	public String getREPAIR_REQUEST() {
		return REPAIR_REQUEST;
	}
	public void setREPAIR_REQUEST(String rEPAIR_REQUEST) {
		REPAIR_REQUEST = rEPAIR_REQUEST;
	}
	public String getREPAIR_RESULT() {
		return REPAIR_RESULT;
	}
	public void setREPAIR_RESULT(String rEPAIR_RESULT) {
		REPAIR_RESULT = rEPAIR_RESULT;
	}
	public String getBpush_userId() {
		return bpush_userId;
	}
	public void setBpush_userId(String bpush_userId) {
		this.bpush_userId = bpush_userId;
	}
	public String getBpush_channelId() {
		return bpush_channelId;
	}
	public void setBpush_channelId(String bpush_channelId) {
		this.bpush_channelId = bpush_channelId;
	}
	public String getMainMsg() {
		return mainMsg;
	}
	public void setMainMsg(String mainMsg) {
		this.mainMsg = mainMsg;
	}
	public String getActionUrl() {
		return actionUrl;
	}
	public void setActionUrl(String actionUrl) {
		this.actionUrl = actionUrl;
	}
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getTopName() {
		return topName;
	}
	public void setTopName(String topName) {
		this.topName = topName;
	}
	public String getPushTitle() {
		return pushTitle;
	}
	public void setPushTitle(String pushTitle) {
		this.pushTitle = pushTitle;
	}
	public String getPushMenuType() {
		return pushMenuType;
	}
	public void setPushMenuType(String pushMenuType) {
		this.pushMenuType = pushMenuType;
	}
	public String getPushMenuDetailNdx() {
		return pushMenuDetailNdx;
	}
	public void setPushMenuDetailNdx(String pushMenuDetailNdx) {
		this.pushMenuDetailNdx = pushMenuDetailNdx;
	}
	public String getPushAlarm() {
		return pushAlarm;
	}
	public void setPushAlarm(String pushAlarm) {
		this.pushAlarm = pushAlarm;
	}
	public String getResCd() {
		return resCd;
	}
	public void setResCd(String resCd) {
		this.resCd = resCd;
	}
	public String getResMsg() {
		return resMsg;
	}
	public void setResMsg(String resMsg) {
		this.resMsg = resMsg;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public String getFromUserId() {
		return fromUserId;
	}
	public void setFromUserId(String fromUserId) {
		this.fromUserId = fromUserId;
	}
	public String[] getTrgtMemberArray() {
		return trgtMemberArray;
	}
	public void setTrgtMemberArray(String[] trgtMemberArray) {
		this.trgtMemberArray = trgtMemberArray;
	}
	public String[] getTrgtPushIdArray() {
		return trgtPushIdArray;
	}
	public void setTrgtPushIdArray(String[] trgtPushIdArray) {
		this.trgtPushIdArray = trgtPushIdArray;
	}
	public String[] getTrgtPlatFmArray() {
		return trgtPlatFmArray;
	}
	public void setTrgtPlatFmArray(String[] trgtPlatFmArray) {
		this.trgtPlatFmArray = trgtPlatFmArray;
	}
	public String[] getTrgtBPushIdArray() {
		return trgtBPushIdArray;
	}
	public void setTrgtBPushIdArray(String[] trgtBPushIdArray) {
		this.trgtBPushIdArray = trgtBPushIdArray;
	}
	public String[] getTrgtBChanneldArray() {
		return trgtBChanneldArray;
	}
	public void setTrgtBChanneldArray(String[] trgtBChanneldArray) {
		this.trgtBChanneldArray = trgtBChanneldArray;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}

