package aspn.hello.com.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ToString {
    public static String toString(Object o) {
        if (o == null) {
            return "null";
        } else {
            return ToStringBuilder.reflectionToString(o, ToStringStyle.MULTI_LINE_STYLE);
        }
    }
    
    @Override
	public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
