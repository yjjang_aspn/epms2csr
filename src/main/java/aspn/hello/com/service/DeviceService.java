package aspn.hello.com.service;

import java.util.HashMap;

import aspn.hello.com.model.DeviceVO;

/**
 * [단말기정보] Service Class
 * 
 * @author 서정민
 * @since 2018.04.11
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.11		서정민			최초 생성
 *   
 * </pre>
 */
public interface DeviceService {
	
	/**
	 * 단말기 정보 등록
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param DeviceVO deviceVO
	 * @return void
	 * @throws Exception
	 */
	public void setDevice(DeviceVO deviceVO);
	
	/**
	 * 단말기 정보 삭제
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param DeviceVO deviceVO
	 * @return void
	 * @throws Exception
	 */
	public void deleteDevice(DeviceVO deviceVO);
	
	/**
	 * 푸시메시지 전송
	 * 
	 * @author 서정민
	 * @since 2018.05.16
	 * @param HashMap<String, Object> pushParam
	 * @return void
	 * @throws Exception
	 */
	public void sendPush(HashMap<String, Object> pushParam) throws Exception;

}
