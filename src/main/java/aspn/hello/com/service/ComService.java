/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package aspn.hello.com.service;

import aspn.hello.com.model.ComVO;
import aspn.hello.com.model.ExceptionLogVO;
import aspn.hello.mem.model.MemberVO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 인증 Service Class
 * @author 이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2017.07.04		이영탁			최초 생성
 *   
 * </pre>
 */

public interface ComService {
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 로그인 
	 * @author 이영탁
	 * @since 2017.07.04
	 * @param memberVO
	 * @return MemberVO
	 * @throws Exception
	 */
	MemberVO setLogin(MemberVO memberVO) throws Exception;

	/**
	 * 에러발생 로그 DB저장
	 * @author 오명석
	 * @since 2017.08.02
	 * @param exceptionLogVO
	 * @return
	 * @throws Exception
	 */
	void setExceptionLog(ExceptionLogVO exceptionLogVO) throws Exception;
	
	/**
	 * 메인 정비현황목록 정보 가져오기
	 * @author 김영환
	 * @param comVO 
	 * @since 2018.11.10
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	HashMap<String, Object> selectRepairInfo(ComVO comVO);
	
	/**
	 * 메인 예방보전 정보 가져오기
	 * @author 김영환
	 * @since 2018.11.10
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	HashMap<String, Object> selectPreventiveInfo(ComVO comVO);
	
	/**
	 * 공통코드 정보 가져오기
	 * @author 김영환
	 * @since 2018.06.18
	 * @param String COMCD_GRP
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectComCodeList(ComVO comVO);
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - APP 버전정보 JSON
	 * @author 서정민
	 * @since 2018.05.23
	 * @param 
	 * @return List<HashMap<String, Object>> 
	 */
	HashMap<String, Object> getAppVerJson();
	
	/**
	 * 모바일 - 부서/사용자 통합 조회
	 * @author 강승상
	 * @since 2017.09.21
	 * @param companyId
	 * @return 부서/사용자 맵 리스트
	 */
	List<HashMap<String, Object>> getMemberList(String companyId);

	
}
