package aspn.hello.com.service;

import aspn.com.common.util.Config;
import aspn.com.common.util.FileHandler;
import aspn.com.common.util.ObjUtil;
import aspn.hello.com.mapper.AttachMapper;
import aspn.hello.com.model.AttachVO;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("attachService")
public class AttachServiceImpl implements AttachService {
	
	@Autowired
	AttachMapper attachMapper;
	
	public String FILE_NAME =  "fileList";

	public Map<String,Object> fileUpload(HttpServletRequest request, MultipartHttpServletRequest mRequest) throws Exception {
		
		List<AttachVO> fileList = FileHandler.uploadFiles(request,mRequest,Config.getString("app.fileInfo.fieldName"));
		
		String fileGrpNo = attachMapper.getFileGrpNo();
		Map<String,Object> map = new HashMap<String, Object>();
		/*//get files
		for(int i=0; i<fileList.size(); i++) {
			
			
			fileList.get(i).setFileGrpNo(fileGrpNo);
			
			//insertFileGrp 파일그룹테이블에는 최초 한번 insert
			if(i==0){
				
				attachMapper.insertFileGrp(fileList.get(i));
				
			}
			
			attachMapper.insertFile(fileList.get(i));
			
		}*/
		
		return map;
		
	}
	
	public boolean fileUpload(HttpServletRequest request, String fileGrpNo) {
		
		/*List<AttachVO> fileList = FileHandler.uploadFiles(request,Config.getString("app.fileInfo.fieldName"));
		
		//get files
		for(AttachVO attachVO : fileList) {
			
			attachVO.setFileGrpNo(fileGrpNo);
			
			//insertFileGrp 파일그룹테이블에는 최초 한번 insert
			attachMapper.insertFile(attachVO);
			
		}*/
		
		return true;
		
	}
	
	public boolean deleteFile(String fileGrpNo, String arrfileNo) {
		
		boolean result = false;
		
		/*StringTokenizer token = new StringTokenizer(arrfileNo,",");
		
		while(token.hasMoreTokens()){
			
			//삭제할 파일 로우 select, filepath set
			FileInfoModel fileInfoModel = commonDao.selectOneFile(fileGrpNo,token);
			String fileFullPath = fileInfoModel.getFileLoc() + File.separator + fileInfoModel.getFileNm();
			
			//db 로우 삭제
			commonDao.deleteFile(fileGrpNo,token);
			
			//물리 파일 삭제
			FileHandler.deleteFile(fileFullPath);
			
		}
		
		if(commonDao.countFile(fileGrpNo) == 0){
			//파일그룹 로우 삭제
			commonDao.deleteFileGrp(fileGrpNo);
		}*/
		
		return result;
		
	}
	
	public List<HashMap<String, Object>> selectFileList(AttachVO attachVO) throws Exception {

		return attachMapper.selectFileList(attachVO);
	}
	
	public AttachVO selectOneFile(AttachVO attachVO) throws Exception {
		return attachMapper.selectOneFile(attachVO);
	}
	
	public AttachVO selectUserFile(AttachVO attachVO) throws Exception {
		return attachMapper.selectUserFile(attachVO);
	}
	
	public String selectAttachGrpNo() throws Exception {
		return attachMapper.selectAttachGrpNo();
	}
	
	public List<AttachVO> AttachEdit(HttpServletRequest request, MultipartHttpServletRequest mRequest, AttachVO attachVO) throws Exception {
		
		//insert 공급사 성공
		if(!"".equals(attachVO.getDEL_SEQ()) && !"0".equals(attachVO.getDEL_SEQ())){
			String [] arrDelSeq = StringUtils.split(attachVO.getDEL_SEQ(),",");
			attachVO.setArrDelSeq(arrDelSeq);

			List<HashMap<String, Object>> fileList = attachMapper.selectFileList(attachVO);
			for(HashMap<String, Object> fileInfo:fileList) {
				
//				Path path = Paths.get(fileInfo.getPATH(), fileInfo.getNAME());
//				Path pathThum = Paths.get(fileInfo.getPATH(), "thum_"+fileInfo.getNAME());
//				FileHandler.nioFileDel(path);
//				FileHandler.nioFileDel(pathThum);

				//객체 정보를 가지고 물리 파일을 삭제
				boolean delFileYn = FileHandler.deleteFile(String.valueOf(fileInfo.get("PATH")), String.valueOf(fileInfo.get("NAME")));
				if(delFileYn){
					String fileName = String.valueOf(fileInfo.get("NAME"));
					FileHandler.thumbDeleteFile(String.valueOf(fileInfo.get("PATH")), "thum_"+fileName.substring(0, fileName.lastIndexOf(".")) + ".png");
				}
				
				AttachVO attachVO1 = new AttachVO();
				attachVO1.setATTACH_GRP_NO(String.valueOf(fileInfo.get("ATTACH_GRP_NO")));
				attachVO1.setATTACH(String.valueOf(fileInfo.get("ATTACH")));
				
				//ATTACH 로 DB 로우 삭제 
				attachMapper.deleteFile(attachVO1);
			}
		}
		
		// 파일업로드
		// 파일 확장자인 format이 추출 되는 부분
		List<AttachVO> fileList = FileHandler.uploadFiles(request, mRequest, FILE_NAME);
		
		//file 존재여부 체크
		if( fileList != null && fileList.size() > 0 ) {
			String arr_fileGbp[] = null;
			if(!StringUtils.isEmpty(attachVO.getFileGb())){
				arr_fileGbp = attachVO.getFileGb().split(",");
			}

			String arr_fileExp[] = null;
			if(!StringUtils.isEmpty(attachVO.getFileExp())){
				arr_fileExp = attachVO.getFileExp().split(",");
			}
			
			String attachGrpNo = attachVO.getATTACH_GRP_NO();
			String module = attachVO.getMODULE();
			
			/*file Insert start*/
			for(int i = 0; i < fileList.size(); i++) {
				
				attachVO = fileList.get(i);

//				if(i == 0){
//					String orgPath = attachVO.getPATH();
//					String thumFileName = attachVO.getNAME();
//					
//					FileHandler fh = new FileHandler();
//					fh.thumbnailMake(orgPath, thumFileName, 300);
//				}
				
				attachVO.setATTACH_GRP_NO(attachGrpNo);
				attachVO.setMODULE(module);
				
				if(arr_fileExp == null){
					attachVO.setSEQ_DSP(1);
				}else{
					attachVO.setSEQ_DSP(Integer.parseInt(arr_fileExp[i], 10));
				}
				
//				if(arr_fileGbp != null ){
//					attachVO.setCD_FILE_TYPE(arr_fileGbp[i]);
//				}
				
				//이미지 확장자
				String[] arrFormat = {"gif", "jpg", "png", "jpeg", "bmp"};	
				//동영상 확장자
				String[] arrFormat2 = {"avi", "wmv", "mpeg", "mpg", "mkv", "mp4", "tp", "ts", "asf", "asx", "flv", "mov", "3gp"};
				//오디오 확장자
				String[] arrFormat3 = {"mp3", "ogg", "wma", "wav", "au", "rm", "mid", "flac", "m4a", "amr"};	
				
				String cd_file_type = "";	//00:기타, 01:이미지, 02:동영상, 03:오디오
				for(int j=0; j<arrFormat.length; j++){
					if(arrFormat[j].equals(attachVO.getFORMAT().toLowerCase())){
						cd_file_type = "01";
						break;
					}
				}
				for(int j=0; j<arrFormat2.length; j++){
					if(arrFormat2[j].equals(attachVO.getFORMAT().toLowerCase())){
						cd_file_type = "02";
						break;
					}
				}
				for(int j=0; j<arrFormat3.length; j++){
					if(arrFormat3[j].equals(attachVO.getFORMAT().toLowerCase())){
						cd_file_type = "03";
						break;
					}
				}
				if("".equals(cd_file_type)){
					cd_file_type = "00";
				}
				attachVO.setCD_FILE_TYPE(cd_file_type);
				
				attachMapper.insertAttach(attachVO);
			}
			/*file Insert end*/
		}
		
		return fileList;
	}
	
	public List<AttachVO> AttachListEdit(HttpServletRequest request, MultipartHttpServletRequest mRequest, AttachVO attachVO, String fileName) throws Exception {
		
		//insert 공급사 성공
		if(!"".equals(attachVO.getDEL_SEQ()) && !"0".equals(attachVO.getDEL_SEQ())){
			String [] arrDelSeq = StringUtils.split(attachVO.getDEL_SEQ(),",");
			attachVO.setArrDelSeq(arrDelSeq);
			
			List<HashMap<String, Object>> fileList = attachMapper.selectFileList(attachVO);
			for(HashMap<String, Object> fileInfo:fileList) {
				
//				Path path = Paths.get(fileInfo.getPATH(), fileInfo.getNAME());
//				Path pathThum = Paths.get(fileInfo.getPATH(), "thum_"+fileInfo.getNAME());
//				FileHandler.nioFileDel(path);
//				FileHandler.nioFileDel(pathThum);
				
				//객체 정보를 가지고 물리 파일을 삭제
				boolean delFileYn = FileHandler.deleteFile(String.valueOf(fileInfo.get("PATH")), String.valueOf(fileInfo.get("NAME")));
				if(delFileYn){
					FileHandler.thumbDeleteFile(String.valueOf(fileInfo.get("PATH")), "thum_"+String.valueOf(fileInfo.get("NAME")));
				}
				
				AttachVO attachVO1 = new AttachVO();
				attachVO1.setATTACH_GRP_NO(String.valueOf(fileInfo.get("ATTACH_GRP_NO")));
				attachVO1.setATTACH(String.valueOf(fileInfo.get("ATTACH")));
				
				//ATTACH 로 DB 로우 삭제 
				attachMapper.deleteFile(attachVO1);
			}
		}
		
		// 파일업로드
		// 파일 확장자인 format이 추출 되는 부분
		List<AttachVO> fileList = FileHandler.uploadFiles(request, mRequest, fileName);
		
		//file 존재여부 체크
		if( fileList != null && fileList.size() > 0 ) {
			String arr_fileGbp[] = null;
			if(!StringUtils.isEmpty(attachVO.getFileGb())){
				arr_fileGbp = attachVO.getFileGb().split(",");
			}
			
			String arr_fileExp[] = null;
			if(!StringUtils.isEmpty(attachVO.getFileExp())){
				arr_fileExp = attachVO.getFileExp().split(",");
			}
			
			String attachGrpNo = attachVO.getATTACH_GRP_NO();
			String module = attachVO.getMODULE();
			
			/*file Insert start*/
			for(int i = 0; i < fileList.size(); i++) {
				
				attachVO = fileList.get(i);
				
//				if(i == 0){
//					String orgPath = attachVO.getPATH();
//					String thumFileName = attachVO.getNAME();
//					
//					FileHandler fh = new FileHandler();
//					fh.thumbnailMake(orgPath, thumFileName, 300);
//				}
				
				attachVO.setATTACH_GRP_NO(attachGrpNo);
				attachVO.setMODULE(module);
				
				if(arr_fileExp == null){
					attachVO.setSEQ_DSP(1);
				}else{
					attachVO.setSEQ_DSP(Integer.parseInt(arr_fileExp[i], 10));
				}
				
//				if(arr_fileGbp != null ){
//					attachVO.setCD_FILE_TYPE(arr_fileGbp[i]);
//				}
				
				//이미지 확장자
				String[] arrFormat = {"gif", "jpg", "png", "jpeg", "bmp"};	
				//동영상 확장자
				String[] arrFormat2 = {"avi", "wmv", "mpeg", "mpg", "mkv", "mp4", "tp", "ts", "asf", "asx", "flv", "mov", "3gp"};
				//오디오 확장자
				String[] arrFormat3 = {"mp3", "ogg", "wma", "wav", "au", "rm", "mid", "flac", "m4a"};	
				
				String cd_file_type = "";	//00:기타, 01:이미지, 02:동영상, 03:오디오
				for(int j=0; j<arrFormat.length; j++){
					if(arrFormat[j].equals(attachVO.getFORMAT().toLowerCase())){
						cd_file_type = "01";
						break;
					}
				}
				for(int j=0; j<arrFormat2.length; j++){
					if(arrFormat2[j].equals(attachVO.getFORMAT().toLowerCase())){
						cd_file_type = "02";
						break;
					}
				}
				for(int j=0; j<arrFormat3.length; j++){
					if(arrFormat3[j].equals(attachVO.getFORMAT().toLowerCase())){
						cd_file_type = "03";
						break;
					}
				}
				if("".equals(cd_file_type)){
					cd_file_type = "00";
				}
				attachVO.setCD_FILE_TYPE(cd_file_type);
				
				attachMapper.insertAttach(attachVO);
			}
			/*file Insert end*/
		}
		
		return fileList;
	}
	
	@Override
	public void updateClientFile(HttpServletRequest request,
			AttachVO attachVO) {
		attachMapper.updateClientFile(attachVO);
		
	}

	/**
	 * 첨부파일 네임 값 지정
	 */
	@Override
	public void setFileName(String fileNm) throws Exception {
		FILE_NAME = fileNm;
	}

	/**
	 * CKEditor 이미지 업로드
	 * @author 윤선철
	 * @since 2017.08.17
	 * @param NoticeVO
	 * @return 
	 * @throws Exception
	 */
	@Override
	public AttachVO setEditorFile(String attachGrpNo, String fileDir, String fileName, long fileSize) throws Exception {
		
		AttachVO attachVO = new AttachVO();

		String ext = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
		String grpNo = attachGrpNo;
		
		if("".equals(attachGrpNo)){
			grpNo = attachMapper.selectAttachGrpNo();
		}
		
		attachVO.setATTACH_GRP_NO(grpNo);
		attachVO.setATTACH(attachMapper.getAttach(grpNo));
		attachVO.setMODULE("88");		//98일경우 EDITOR로 등록한 파일
		attachVO.setNAME(fileName);
		attachVO.setPATH(fileDir);
		attachVO.setFORMAT(ext);
		attachVO.setFILE_SIZE(fileSize);
		attachVO.setCD_FILE_TYPE("0");
		attachVO.setCNT_DOWNLOAD("0");
		attachVO.setCD_DEL("1");
		
		attachMapper.insertAttach(attachVO);
		
		return attachVO;
	}

	@Override
	public void CopyAttach(String attachGrpNo, String beforeAttachGrpNo) throws Exception {
		attachMapper.CopyAttach(attachGrpNo, beforeAttachGrpNo);
	}
}
