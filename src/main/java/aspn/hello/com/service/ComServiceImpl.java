/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package aspn.hello.com.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.SecretUtils;
import aspn.com.common.util.Utility;
import aspn.hello.com.mapper.ComMapper;
import aspn.hello.com.model.ComVO;
import aspn.hello.com.model.ExceptionLogVO;
import aspn.hello.mem.model.MemberVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

/**
 * 인증  Business Implement Class
 * @author 이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2017.07.04		이영탁			최초 생성
 *   2018.04.11		서정민			마스터 패스워드 생성
 *   
 * </pre>
 */

@Service("comService")
public class ComServiceImpl extends EgovAbstractServiceImpl implements ComService {

	private static final Logger logger = LoggerFactory.getLogger(ComServiceImpl.class);

	/** CodeDAO */
	@Autowired
	private ComMapper comMapper;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	/**
	 * 로그인 정보 확인
	 * @author 이영탁
	 * @since 2017.07.04
	 * @param memberVO
	 * @return MemberVO
	 * @throws Exception
	 */
	public MemberVO setLogin(MemberVO memberVO) {
		
		String inputPwd     = memberVO.getPassword();
		String userGubun    = memberVO.getUserGubun();
		
		// 마스터 패스워드
		String masterPwd = "aspn" +DateTime.getShortDateString();
		
		masterPwd = SecretUtils.encryptSha256(SecretUtils.encryptMd5(masterPwd));

		// [admin] "a49b20a85868bebc6acf47eedddd61845d1ac06aafba7b7ba832baeb65b8d173"
		// [1]     "08428467285068b426356b9b0d0ae1e80378d9137d5e559e5f8377dbd6dde29f"
		
		
		if (Utility.isEmpty(userGubun)) {
			userGubun = CodeConstants.USER_GUBUN_USER;
		}
		
		if("1".equals(userGubun)) {         //일반사용자
			memberVO = comMapper.getUserInfo(memberVO);    //원래 소스
		} else if("2".equals(userGubun)) {  //거래처(고객)
			memberVO = comMapper.getClientInfo(memberVO);
		} else if("3".equals(userGubun)) {  //공급사
			memberVO = comMapper.getVendorInfo(memberVO);	
			if(memberVO != null) {
				memberVO.setAUTH("15");
			}
		} 

		
		if(memberVO == null) {
			
			memberVO = this.setResultPacket(CodeConstants.NO_REGIST_USER, memberVO);
			
		} else {

			int loginFailCnt = 0;
		
			loginFailCnt = comMapper.getLoginFailCnt(memberVO);
			
			if(loginFailCnt >= 5 ){
				
				memberVO = this.setResultPacket(CodeConstants.EXCEED_PASSWORD, memberVO);
			}
			else {  // && !inputPwd.equals(masterPwd)
				
				logger.debug("# ComService.setLogin >> inputPwd [" + inputPwd + "] = [" + memberVO.getPASSWORD() + "] ");
				
				if(memberVO.getPASSWORD() == null || !memberVO.getPASSWORD().equals(inputPwd) ) {
					//비번이 맟지않을시
					comMapper.setFailCnt(memberVO);
					loginFailCnt = comMapper.getLoginFailCnt(memberVO);
					
					memberVO = this.setResultPacket(CodeConstants.INVALID_PASSWORD, memberVO);
				} else if("2".equals(memberVO.getCD_DEL())) {
					//관리권한
					memberVO = this.setResultPacket(CodeConstants.DROP_MEMBER, memberVO);
				} else {
					//로그인 성공시
					comMapper.setResetCnt(memberVO);
					memberVO = this.setResultPacket(CodeConstants.SUCCESS, memberVO);
				}
			}
			
			memberVO.setFailCnt(Integer.toString(loginFailCnt));
			
		}

		return memberVO;
	}
	
	/**
	 * 인증결과를 VO에 추가
	 * @author 이영탁
	 * @since 2017.07.05
	 * @param String retCd 리턴코드
	 * @param memberVO
	 * @return memberVO
	 */
	MemberVO setResultPacket(String retCd, MemberVO memberVO){
		if(memberVO == null){
			memberVO = new MemberVO();
		}
		
		memberVO.setResCd(retCd);
		memberVO.setResMsg(CodeConstants.ERROR_MSG.get(retCd)[1]);
		
		return memberVO;
	}

	/**
	 * 에러발생 로그 DB저장
	 * @author 오명석
	 * @since 2017.08.02
	 * @param exceptionLogVO
	 * @return
	 * @throws Exception
	 */
	public void setExceptionLog(ExceptionLogVO exceptionLogVO) throws Exception {
		comMapper.setExceptionLog(exceptionLogVO);
	}
	
	/**
	 * 메인 정비현황목록 정보 가져오기
	 * @author 김영환
	 * @since 2018.06.14
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> selectRepairInfo(ComVO comVO) {
		return comMapper.selectRepairInfo(comVO);
	}
	
	/**
	 * 메인 예방보전 정보 가져오기
	 * @author 김영환
	 * @since 2018.06.14
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public HashMap<String, Object> selectPreventiveInfo(ComVO comVO) {
		return comMapper.selectPreventiveInfo(comVO);
	}
	
	/**
	 * 공통코드 정보 가져오기
	 * @author 김영환
	 * @since 2018.06.18
	 * @param String COMCD_GRP
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectComCodeList(ComVO comVO) {
		return comMapper.selectComCodeList(comVO);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - APP 버전정보 JSON
	 * @author 서정민
	 * @since 2018.05.23
	 * @param 
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> getAppVerJson() {
		return comMapper.getAppVerJson();
	}
	
	/* 모바일 - 부서/사용자 목록 조회 */
	@Override
	public List<HashMap<String, Object>> getMemberList(String companyId) {
		return comMapper.getMemberList(companyId);
	}

}
