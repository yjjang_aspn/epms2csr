package aspn.hello.com.service;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartHttpServletRequest;

import aspn.hello.com.model.AttachVO;

public interface AttachService {
	
	public List<HashMap<String, Object>> selectFileList(AttachVO attachVO) throws Exception;

	public AttachVO selectOneFile(AttachVO attachVO) throws Exception;

	/**
	 * 첨부파일 group no 조회
	 * @return
	 */
	public String selectAttachGrpNo() throws Exception;
	
	/**
	 * 첨부파일 등록/수정
	 * @param attachVO
	 * @return integer 성공 : 1
	 * @throws Exception 
	 */
	public List<AttachVO> AttachEdit(HttpServletRequest request, MultipartHttpServletRequest mRequest, AttachVO attachVO) throws Exception;

	public AttachVO selectUserFile(AttachVO attachVO) throws Exception;

	public void updateClientFile(HttpServletRequest request,
			AttachVO attachVO);
	
	/**
	 * 첨부 파일 name 값 지정
	 * @param fileNm
	 * @throws Exception
	 */
	public void setFileName(String fileNm) throws Exception;
	
	/**
	 * CKEditor 이미지 업로드
	 * @author 윤선철
	 * @since 2017.08.17
	 * @param NoticeVO
	 * @return 
	 * @throws Exception
	 */
	public AttachVO setEditorFile(String attachGrpNo, String fileDir, String fileName, long fileSize) throws Exception;

	/**
	 * 첨부파일 복사 (재임시전표생성시 사용)
	 * @param attachGrpNo
	 * @param string
	 * @throws Exception
	 */
	public void CopyAttach(String attachGrpNo, String string) throws Exception;

	public List<AttachVO> AttachListEdit(HttpServletRequest request, MultipartHttpServletRequest mRequest, AttachVO attachVO, String fileName) throws Exception;

}
