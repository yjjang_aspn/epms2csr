package aspn.hello.com.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.config.SFAConstants;
import aspn.com.common.util.PushThread;
import aspn.hello.com.mapper.DeviceMapper;
import aspn.hello.com.model.DeviceVO;

/**
 * [단말기정보] Business Implement Class
 * 
 * @author 서정민
 * @since 2018.04.11
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.11		서정민			최초 생성
 *   
 * </pre>
 */

@Service("deviceService")
public class DeviceServiceImpl implements DeviceService {
	
	@Autowired
	DeviceMapper deviceMapper;
	
	/**
	 * 단말기 정보 등록
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param DeviceVO deviceVO
	 * @return void
	 * @throws Exception
	 */
	public void setDevice(DeviceVO deviceVO){
		
		deviceMapper.setDevice(deviceVO);
	}
	
	/**
	 * 단말기 정보 삭제
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param DeviceVO deviceVO
	 * @return void
	 * @throws Exception
	 */
	public void deleteDevice(DeviceVO deviceVO){
		
		deviceMapper.deleteDevice(deviceVO);
	}
	
	/**
	 * 푸시메시지 전송
	 * 
	 * @author 서정민
	 * @since 2018.05.16
	 * @param HashMap<String, Object> pushParam
	 * @return void
	 * @throws Exception
	 */
	public void sendPush(HashMap<String, Object> pushParam) throws Exception {
	    
		try{
			List<DeviceVO> pushList = null;
			
			// 1. 일반알람일 경우
			if(CodeConstants.EPMS_PUSHALARM_GENERAL.equals(pushParam.get("PUSHALARM").toString())){
				// 내 요청 상세
				if(CodeConstants.EPMS_PUSHMENUTYPE_REQUEST.equals(pushParam.get("PUSHMENUTYPE").toString())){
					pushList = deviceMapper.selectRequestPushList(pushParam);
				}
				// 내 요청 상세
				else if(CodeConstants.EPMS_PUSHMENUTYPE_REQUEST_COMPLETE.equals(pushParam.get("PUSHMENUTYPE").toString())){
					pushList = deviceMapper.selectRequestPushList(pushParam);
				}
				// 결재 상세
				else if(CodeConstants.EPMS_PUSHMENUTYPE_APPR.equals(pushParam.get("PUSHMENUTYPE").toString())){
					pushList = deviceMapper.selectApprPushList(pushParam);
				}
				// 계획 및 배정 상세
				else if(CodeConstants.EPMS_PUSHMENUTYPE_PLAN.equals(pushParam.get("PUSHMENUTYPE").toString())){
					pushList = deviceMapper.selectPlanPushList(pushParam);
				}
				// 정비 상세
				else if(CodeConstants.EPMS_PUSHMENUTYPE_REPAIR.equals(pushParam.get("PUSHMENUTYPE").toString())){
					pushList = deviceMapper.selectRepairPushList(pushParam);
				}
				// 정비 상세(완료)
				else if(CodeConstants.EPMS_PUSHMENUTYPE_REPAIR_COMPLETE.equals(pushParam.get("PUSHMENUTYPE").toString())){
					pushList = deviceMapper.selectRepairPushList(pushParam);
				}
				// 요청 취소
				else if(CodeConstants.EPMS_PUSHMENUTYPE_REQUEST_CANCEL.equals(pushParam.get("PUSHMENUTYPE").toString())){
					pushList = deviceMapper.selectApprPushList(pushParam);
				}
				// 공지사항
				else if(CodeConstants.EPMS_PUSHMENUTYPE_NOTICE.equals(pushParam.get("PUSHMENUTYPE").toString())){
					pushList = deviceMapper.selectNoticePushList(pushParam);
				}
				// 업무현황 알림
				else if(CodeConstants.EPMS_PUSHMENUTYPE_ALARM.equals(pushParam.get("PUSHMENUTYPE").toString())){
					pushList = deviceMapper.selectAlarmPushList(pushParam);
				}
	  
				if(pushList.size() > 0){
					PushThread pt = new PushThread();
					pt.setParam(pushList, pushParam.get("PUSHTITLE").toString(), pushParam.get("PUSHMENUTYPE").toString(), pushParam.get("PUSHMENUDETAILNDX").toString(), pushParam.get("PUSHALARM").toString());
					pt.start();
				}
			}
			// 2. 긴급알람일 경우(긴급정비요청) : 긴급요청(수신) 권한 있는 사람에게만 푸시 전송 
			else if(CodeConstants.EPMS_PUSHALARM_EMERGENCY.equals(pushParam.get("PUSHALARM").toString())){
				
				/*
				 * 2차 개발 : 긴급요청(수신) 권한 있는 사람에게만 푸시 전송 
				 */
				// 2-1. 긴급요청(수신) 권한 있는 사람 중 파트정비원 전송
				pushList = deviceMapper.selectEmergencyPartPushList(pushParam);
				if(pushList.size() > 0){
					PushThread pt = new PushThread();
					pt.setParam(pushList, pushParam.get("PUSHTITLE").toString(), pushParam.get("PUSHMENUTYPE").toString(), pushParam.get("PUSHMENUDETAILNDX").toString(), pushParam.get("PUSHALARM").toString());
					pt.start();
				}
				
				// 2-2. 긴급요청(수신) 권한 있는 사람 중 파트정비원 제외 인원 전송
				List<DeviceVO> pushList2 = deviceMapper.selectEmergencyNotPartPushList(pushParam);
				if(pushList2.size() > 0){
					PushThread pt = new PushThread();
					pt.setParam(pushList2, pushParam.get("PUSHTITLE").toString(), "REQUEST", pushParam.get("PUSHMENUDETAILNDX").toString(), pushParam.get("PUSHALARM").toString());
					pt.start();
				}
				
				/* 1차 개발 : 공장 전체 인원에게 푸시 전송
				 * 
				// 2-1. 파트정비원 전송
				// 결재 상세
				if(CodeConstants.EPMS_PUSHMENUTYPE_APPR.equals(pushParam.get("PUSHMENUTYPE").toString())){
					pushList = deviceMapper.selectApprPushList(pushParam);
				}
				// 계획 및 배정 상세
				else if(CodeConstants.EPMS_PUSHMENUTYPE_PLAN.equals(pushParam.get("PUSHMENUTYPE").toString())){
					pushList = deviceMapper.selectPlanPushList(pushParam);
				}
				// 정비 상세
				else if(CodeConstants.EPMS_PUSHMENUTYPE_REPAIR.equals(pushParam.get("PUSHMENUTYPE").toString())){
					pushList = deviceMapper.selectPlanPushList(pushParam);
				}
				if(pushList.size() > 0){
					PushThread pt = new PushThread();
					pt.setParam(pushList, pushParam.get("PUSHTITLE").toString(), pushParam.get("PUSHMENUTYPE").toString(), pushParam.get("PUSHMENUDETAILNDX").toString(), pushParam.get("PUSHALARM").toString());
					pt.start();
				}
				
				// 2-2. 파트정비원 제외 공장 전체 인원 전송
				List<DeviceVO> pushList2 = deviceMapper.selectEmergencyAllPushList(pushParam);
				if(pushList2.size() > 0){
					PushThread pt = new PushThread();
					pt.setParam(pushList2, pushParam.get("PUSHTITLE").toString(), "REQUEST", pushParam.get("PUSHMENUDETAILNDX").toString(), pushParam.get("PUSHALARM").toString());
					pt.start();
				}
				*/
			}
			
	      
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
