/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package aspn.hello.sys.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.util.SessionUtil;
import aspn.com.common.util.Utility;
import aspn.hello.rfc.model.ZPMPC_OBJ_TYPE_EXPORT;
import aspn.hello.rfc.model.ZPMPC_OBJ_TYPE_T_OBJTYP;
import aspn.hello.rfc.model.ZPM_ITEM_EXPORT;
import aspn.hello.rfc.model.ZPM_ITEM_T_ITEM;
import aspn.hello.rfc.model.ZPM_ORDER_TYPE_EXPORT;
import aspn.hello.rfc.model.ZPM_ORDER_TYPE_T_ORDTYP;
import aspn.hello.sys.mapper.CodeMapper;
import aspn.hello.sys.model.CodeVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

/**
 * [공통코드] Business Implement Class
 * 
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *   2018.11.18		김영환			SAP 플랜트별오더유형 조회
 *   2018.11.18		김영환			SAP 오브젝트유형  조회
 *   2018.11.18		김영환			SAP 카테고리ITEM 조회
 *   
 * </pre>
 */

@Service("codeService")
public class CodeServiceImpl extends EgovAbstractServiceImpl implements CodeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CodeServiceImpl.class);

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/** CodeDAO */
	@Autowired
	private CodeMapper codeMapper;
	


	/**
	 * 공통코드 목록조회
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param CodeVO codeVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> getCodeList(CodeVO codeVO) throws Exception {
		return codeMapper.getCodeList(codeVO);
	}
	
	/**
	 * 공통코드 그리드 저장
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param List<HashMap<String, Object>> saveDataList
	 * @return void
	 * @throws Exception
	 */
	public void codeEdit(List<HashMap<String, Object>> saveDataList, CodeVO codeVO) throws Exception{
		
		if(saveDataList != null){
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				CodeVO codeVO1 = Utility.toBean(tmpSaveData, codeVO.getClass());
				codeVO1.setUserId(codeVO.getUserId());
				if (tmpSaveData.containsKey("Changed")) {
					codeMapper.updateCode(codeVO1);
				} else if(tmpSaveData.containsKey("Added")) {
					codeMapper.insertCode(codeVO1);
				} else if(tmpSaveData.containsKey("Deleted")) {
					codeMapper.deleteCode(codeVO1);
				}
			}
		}
	}
	
	/**
	 * 공통코드상세 목록조회
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param CodeVO codeVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> getCodeDtlList(CodeVO codeVO) throws Exception {
		return codeMapper.getCodeDtlList(codeVO);
	}
	
	/**
	 * 공통코드상세 그리드 저장
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param List<HashMap<String, Object>> saveDataList
	 * @return void
	 * @throws Exception
	 */
	public void codeDtlEdit(List<HashMap<String, Object>> saveDataList, CodeVO codeVO) throws Exception{
		
		if(saveDataList != null){
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				@SuppressWarnings("unchecked")
				CodeVO codeVO1 = Utility.toBean(tmpSaveData, codeVO.getClass());
				codeVO1.setUserId(codeVO.getUserId());
				if (tmpSaveData.containsKey("Changed")) {
					codeMapper.updateCodeDtl(codeVO1);
				} else if(tmpSaveData.containsKey("Added")) {
					codeMapper.insertCodeDtl(codeVO1);
				} else if(tmpSaveData.containsKey("Deleted")) {
					codeMapper.deleteCodeDtl(codeVO1);
				}
			}
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// SAP
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * SAP 플랜트별오더유형  등록 
	 *
	 * @author 김영환
	 * @since 2018.11.14
	 * @param ZPM_ORDER_TYPE_EXPORT tables
	 * @param ZPM_ORDER_TYPE_IMPORT zpm_order_type_import
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> updateOrderTypeInfo(ZPM_ORDER_TYPE_EXPORT tables) throws Exception {
		
		CodeVO codeVO = new CodeVO();
		String ssCompanyId = (String) SessionUtil.getAttribute("ssCompanyId");
		String ssUserId = (String) SessionUtil.getAttribute("ssUserId");
		
		//플랜트별오더유형  등록 
		if(tables != null){
			for (ZPM_ORDER_TYPE_T_ORDTYP t_ordtyp : tables.getZpm_order_type_t_ordtyp()) {
				
//				WERKS : 5100
//				AUART : ZM01, ZM02
//				TXT : 비알코리아(주)-예방보전오더, 비알코리아(주)-돌발고장오더
				
				codeVO = new CodeVO();
				
				codeVO.setCOMPANY_ID(ssCompanyId);
				codeVO.setLOCATION(t_ordtyp.getWERKS());
				codeVO.setREG_ID(ssUserId);
				codeVO.setCOMCD_GRP("ORDER_TYPE");
				codeVO.setCOMCD(t_ordtyp.getAUART());
				codeVO.setCOMCD_NM(t_ordtyp.getTXT());
				codeVO.setCOMCD_DESC(t_ordtyp.getTXT());
				
				//SAP:플랜트별오더유형  등록 
				String COMCD_ORD = codeMapper.selectComcdOrd(codeVO);
				codeVO.setCOMCD_ORD(COMCD_ORD);
//				if("5100".equals(codeVO.getLOCATION())){
					codeMapper.insertCodeDtl(codeVO);
//				}
								
			}
		}
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("E_RESULT", tables.getE_RESULT());
		map.put("E_MESSAGE", tables.getE_MESSAGE());
		logger.info("E_RESULT:" + tables.getE_RESULT());
		logger.info("E_MESSAGE:" + tables.getE_MESSAGE());
		
		return map;
	}

	/**
	 * SAP 오브젝트유형 등록 
	 *
	 * @author 김영환
	 * @since 2018.11.14
	 * @param ZPMPC_OBJ_TYPE_EXPORT tables
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> updateObjTypeInfo(ZPMPC_OBJ_TYPE_EXPORT tables) throws Exception {
		
		CodeVO codeVO = new CodeVO();
		String ssCompanyId = (String) SessionUtil.getAttribute("ssCompanyId");
//		String ssLocation = (String) SessionUtil.getAttribute("ssLocation");
		String ssUserId = (String) SessionUtil.getAttribute("ssUserId");
		
		//오브젝트유형 등록
		if(tables != null){
			for (ZPMPC_OBJ_TYPE_T_OBJTYP t_objtyp : tables.getZpmpc_obj_type_t_objtyp()) {
				
//				EQART(기술오브젝트 유형)   : 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8100, 8200, 8300, 8400, 9000
//				EARTX(오브젝트 유형 텍스트) : 배합설비, 정형설비, 부출설비, 냉각설비, 포장설비, 이송설비, 출하설비, 싸이로설비, 유틸리티설비, 공조설비, 환경설비, 기타설비
				
				codeVO = new CodeVO();
				
				codeVO.setCOMPANY_ID(ssCompanyId);
				codeVO.setLOCATION("5100");
				codeVO.setREG_ID(ssUserId);
				codeVO.setCOMCD_GRP("OBJ_TYPE");
				codeVO.setCOMCD(t_objtyp.getEQART());
				codeVO.setCOMCD_NM(t_objtyp.getEARTX());
				codeVO.setCOMCD_DESC(t_objtyp.getEARTX());
				
				//SAP:오브젝트유형 공통코드순서 조회 
				String COMCD_ORD = codeMapper.selectComcdOrd(codeVO);
				codeVO.setCOMCD_ORD(COMCD_ORD);
				codeMapper.insertCodeDtl(codeVO);
				
			}
		}		
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("E_RESULT", tables.getE_RESULT());
		map.put("E_MESSAGE", tables.getE_MESSAGE());
		logger.info("E_RESULT:" + tables.getE_RESULT());
		logger.info("E_MESSAGE:" + tables.getE_MESSAGE());
		
		return map;
	}

	/**
	 * SAP 카테고리ITEM 등록 
	 *
	 * @author 김영환
	 * @since 2018.11.19
	 * @param ZPM_ITEM_EXPORT tables
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> updateCategoryItem(ZPM_ITEM_EXPORT tables) throws Exception {
		CodeVO codeVO = new CodeVO();
		
		String ssCompanyId = (String) SessionUtil.getAttribute("ssCompanyId");
//		String ssLocation = (String) SessionUtil.getAttribute("ssLocation");
		String ssUserId = (String) SessionUtil.getAttribute("ssUserId");
		
		int COMCD_GRP_ORD = 0;
		int COMCD_ORD = 0;
		String COMCD_GRP = "";
		String UPPER_COMCD = "";
		
		//오브젝트유형 등록
		if(tables != null){
			for (ZPM_ITEM_T_ITEM zpm_item_t_item : tables.getZpm_item_t_item()) {
				
				// 코드그룹의 첫번째일경우
				if( !COMCD_GRP.equals(zpm_item_t_item.getKATALOGART()) || !UPPER_COMCD.equals(zpm_item_t_item.getCODEGRUPPE())){
					
					if(!COMCD_GRP.equals(zpm_item_t_item.getKATALOGART())){
						COMCD_GRP_ORD = 1;	
					}
					else{
						COMCD_GRP_ORD++;
					}
					
					// 코드그룹 생성
					CodeVO codeVO2 = new CodeVO();
					codeVO2.setCOMCD_GRP("CATALOG_"+zpm_item_t_item.getKATALOGART());
					codeVO2.setCOMPANY_ID(ssCompanyId);
					codeVO2.setLOCATION("5100");
					codeVO2.setREG_ID(ssUserId);
					
					codeVO2.setCOMCD_GRP_NM(zpm_item_t_item.getKATALOGTXT().trim());
					codeVO2.setCOMCD_GRP_DESC(zpm_item_t_item.getKATALOGTXT().trim());
					codeVO2.setCOMCD(zpm_item_t_item.getCODEGRUPPE());
					codeVO2.setCOMCD_NM(zpm_item_t_item.getCODEGRUTXT().trim());
					codeVO2.setCOMCD_ORD(Integer.toString(COMCD_GRP_ORD));
					codeVO2.setDEL_YN("N");
					
					codeMapper.insertCodeDtl(codeVO2);
					
					// 코드 생성
					COMCD_ORD = 1;
					codeVO.setCOMCD(zpm_item_t_item.getCODE());
					codeVO.setCOMCD_GRP("CATALOG_"+zpm_item_t_item.getKATALOGART());
					codeVO.setUPPER_COMCD(zpm_item_t_item.getCODEGRUPPE());
					
					codeVO.setCOMCD_ORD(Integer.toString(COMCD_ORD));
					codeVO.setCOMPANY_ID(ssCompanyId);
					codeVO.setLOCATION("");
					codeVO.setREG_ID(ssUserId);
					codeVO.setCOMCD_NM(zpm_item_t_item.getCODETXT().trim());
					codeVO.setDEL_YN("N");
					
					codeMapper.insertCodeDtl(codeVO);
					
				}
				else{
					// 코드 생성
					COMCD_ORD++;
					codeVO.setCOMCD(zpm_item_t_item.getCODE());
					codeVO.setCOMCD_GRP("CATALOG_"+zpm_item_t_item.getKATALOGART());
					codeVO.setUPPER_COMCD(zpm_item_t_item.getCODEGRUPPE());
					
					codeVO.setCOMCD_ORD(Integer.toString(COMCD_ORD));
					codeVO.setCOMPANY_ID(ssCompanyId);
					codeVO.setLOCATION("5100");
					codeVO.setREG_ID(ssUserId);
					codeVO.setCOMCD_NM(zpm_item_t_item.getCODETXT().trim());
					codeVO.setDEL_YN("N");
					
					codeMapper.insertCodeDtl(codeVO);
					
				}
				
				COMCD_GRP = zpm_item_t_item.getKATALOGART();
				UPPER_COMCD = zpm_item_t_item.getCODEGRUPPE();
				
			}
		}		
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("E_RESULT", tables.getE_RESULT());
		map.put("E_MESSAGE", tables.getE_MESSAGE());
		logger.info("E_RESULT:" + tables.getE_RESULT());
		logger.info("E_MESSAGE:" + tables.getE_MESSAGE());
		
		return map;
	}
	
}
