/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package aspn.hello.sys.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.sys.model.CategoryVO;

/**
 * [카테고리] Service Class
 * 
 * @author 서정민
 * @since 2018.02.08
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.08		서정민			최초 생성
 *   
 * </pre>
 */
public interface CategoryService {

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 카테고리 목록조회
	 * 
	 * @author 서정민
	 * @since 2018.02.08
	 * @param CategoryVO categoryVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectCategoryList(CategoryVO categoryVO) throws Exception;
	
	/**
	 * 카테고리 그리드 저장
	 * 
	 * @author 서정민
	 * @since 2018.02.08
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param CategoryVO categoryVO
	 * @return void
	 * @throws Exception
	 */
	public void categoryEdit(List<HashMap<String, Object>> saveDataList, CategoryVO categoryVO) throws Exception;
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - 카테고리 목록조회
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param CategoryVO categoryVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectCategoryList2(CategoryVO categoryVO) throws Exception;
	
}
