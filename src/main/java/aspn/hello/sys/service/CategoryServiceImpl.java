/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package aspn.hello.sys.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.Utility;
import aspn.hello.sys.mapper.CategoryMapper;
import aspn.hello.sys.model.CategoryVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

/**
 * [카테고리] Business Implement Class
 * 
 * @author 서정민
 * @since 2018.02.08
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.08		서정민			최초 생성
 *   
 * </pre>
 */

@Service("categoryService")
public class CategoryServiceImpl extends EgovAbstractServiceImpl implements CategoryService {

	private static final Logger logger = LoggerFactory.getLogger(CodeServiceImpl.class);

	/** CategoryDAO */
	@Autowired
	private CategoryMapper categoryMapper;

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 카테고리 목록조회
	 * 
	 * @author 서정민
	 * @since 2018.02.08
	 * @param MemberVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectCategoryList(CategoryVO categoryVO) throws Exception {
		return categoryMapper.selectCategoryList(categoryVO);
	}
	
	
	/**
	 * 카테고리 그리드 저장
	 * 
	 * @author 서정민
	 * @since 2018.02.08
	 * @param List<HashMap<String, Object>> saveDataList
	 * @return void
	 * @throws Exception
	 */
	public void categoryEdit(List<HashMap<String, Object>> saveDataList, CategoryVO categoryVO) throws Exception {
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("# [YJJANG] CategoryService.categoryEdit called ...");
		}
		
		HashMap<String, String> oParentCategoryMap = new HashMap<String, String>();
		HashMap<String, String> oTreeMap = new HashMap<String, String>();  // Check for
		
		
		if ( saveDataList != null && saveDataList.size() > 0 ) {
			
			for ( HashMap<String, Object> tmpSaveData : saveDataList ) {
				
				CategoryVO toCategoryVO = Utility.toBean(tmpSaveData, categoryVO.getClass());
				
				toCategoryVO.setREG_ID(categoryVO.getUserId());
				toCategoryVO.setUPD_ID(categoryVO.getUserId());
				
				
				/**  Grid Upload Key >>> "Added"  **/
				if ( tmpSaveData.containsKey( CodeConstants.GRID_UPLOAD_ADDED ) ) {
				
					// 카테고리 SEQ 조회
					String SEQ_CATEGORY = categoryMapper.selectCategorySeq();
					
					toCategoryVO.setCATEGORY(SEQ_CATEGORY);  // 카테고리ID
					
					
					// Set for PARENT_CATEGORY , TREE
					if ( toCategoryVO.getPARENT_CATEGORY().isEmpty() 
							&& !CodeConstants.STR_1.equals( toCategoryVO.getDEPTH() ) ) {
						
						String index = Integer.toString( Integer.parseInt(toCategoryVO.getDEPTH()) - 1 );
						
						toCategoryVO.setPARENT_CATEGORY( oParentCategoryMap.get(index) );
						toCategoryVO.setTREE(oTreeMap.get(index));
					}
					
					// [YJJANG] Check for CATEGORY_UID(카테고리코드) , SUB_ROOT(공장구분) 

					// 카테고리 등록
					categoryMapper.insertCategory( toCategoryVO );
					
					
					oParentCategoryMap.put(toCategoryVO.getDEPTH(), SEQ_CATEGORY);
					
					if ( CodeConstants.STR_1.equals(toCategoryVO.getDEPTH()) ) {
						
						oTreeMap.put(toCategoryVO.getDEPTH(), SEQ_CATEGORY);
					}
					else {
						oTreeMap.put(toCategoryVO.getDEPTH(), toCategoryVO.getTREE()+"$"+SEQ_CATEGORY);
					}

				} 
				/**  Grid Upload Key >>> "Changed"  **/
				else if ( tmpSaveData.containsKey( CodeConstants.GRID_UPLOAD_CHANGED ) ) {
					
					// 카테고리 수정
					categoryMapper.updateCategory( toCategoryVO );
					
				}
				/** Grid Upload Key >>> "Deleted"  **/
				else if ( tmpSaveData.containsKey( CodeConstants.GRID_UPLOAD_DELETED ) ) {
					
					categoryMapper.deleteCategory( toCategoryVO );
				}
			
			}
			// for(HashMap<String, Object> tmpSaveData:saveDataList)
			
		}
		// if ( saveDataList != null && saveDataList.size() > 0 )
		
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - 카테고리 목록조회
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param MemberVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectCategoryList2(CategoryVO categoryVO) throws Exception {
		return categoryMapper.selectCategoryList2(categoryVO);
	}
	
}
