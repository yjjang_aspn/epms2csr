package aspn.hello.sys.service;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 강승상
 * @version 1.0
 * <p>
 * <pre>
 * << History >>
 * 수정일		수정자		수정내용
 * ---------    --------    ---------------------
 * 2017-08-31	강승상		최초 생성
 * </pre>
 * @since 2017-08-31
 */
public class SQLService {

	//TODO : JDBCTemplate Logging

	private JdbcTemplate template;

	public void setDataSource(BasicDataSource dataSource) {
		this.template = new JdbcTemplate(dataSource);
	}

	/**
	 * 쿼리 결과 반환
	 * @param sql
	 * @return
	 */
	public List<Map<String, Object>> doQuery(String sql) throws Exception{
		List<Map<String, Object>> resultMaptList = new ArrayList<>();
		try {
			resultMaptList = template.queryForList(removeNewLine(sql));
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> errorMap = new HashMap<>();
			errorMap.put("ERROR", e.getMessage());
			resultMaptList.add(errorMap);
		}

		return resultMaptList;
	}

	public Map<String, Object> doExecute(String sql) {
		int result = 0;
		Map<String, Object> resultMap = new HashMap<>();
		try {
			result = template.update(removeNewLine(sql));
			resultMap.put("AFFECTED", result);
		} catch (Exception e) {
			e.printStackTrace();
			resultMap.put("ERROR", e.getMessage());
		}
		return resultMap;
	}

	private String removeNewLine(String str) {
		System.out.println("str / " + str);
		System.out.println("str / " + str.replaceAll("\n", " ").replaceAll("\r", " "));
		return str;
	}
}
