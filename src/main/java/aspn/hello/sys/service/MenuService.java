/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package aspn.hello.sys.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aspn.hello.sys.model.MenuVO;


/**
 * 메뉴 Service Class
 * @author 이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.07.04  이영탁          최초 생성
*
 * </pre>
 */

public interface MenuService {

	/**
	 * 헤더 메뉴 리스트
	 * @author 이영탁
	 * @since 2017.07.05
	 * @param List<HashMap<String, String>>
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, String>> getHeaderMenuList(String auth) throws Exception;
	
	/**
	 * 왼쪽 메뉴 리스트
	 * @author 이영탁
	 * @since 2017.07.05
	 * @param List<HashMap<String, String>>
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, String>> getLeftMenuList(String auth) throws Exception;
	
	/**
	 * 메뉴관리 리스트
	 * @author 이영탁
	 * @since 2017.07.12
	 * @param menuVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> getMenuMgntList(MenuVO menuVO) throws Exception;
	
	/**
	 * 메뉴관리 그리드 CRUD 처리
	 * @author 이영탁
	 * @since 2017.07.12
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param MenuVO menuVO
	 * @return void
	 * @throws Exception
	 */
	public void menuMgntEdit(List<HashMap<String, Object>> saveDataList, MenuVO menuVO) throws Exception;
	
	/**
	 * 메뉴 리스트(메뉴권한)
	 * @author 이영탁
	 * @since 2017.07.17
	 * @param menuVO
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> getMenuList(MenuVO menuVO) throws Exception;
	
	/**
	 * 메뉴(권한) 그리드 CRUD 처리
	 * @author 이영탁
	 * @since 2017.07.17
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param MenuVO menuVO
	 * @return void
	 * @throws Exception
	 */
	public void menuEdit(List<HashMap<String, Object>> saveDataList, MenuVO menuVO) throws Exception;

	/**
	 * GNB 메뉴 정보 조회
	 * @author 정순주
	 * @since 2017. 7. 31.
	 * @param menuVo
	 * @return
	 * @return Map<String,Object>
	 * @throws Exception 
	 */
	public Map<String, Object> getGnbMenuInfo(MenuVO menuVo) throws Exception;
	
	/**
	 * 메뉴 id조회
	 * @param menuKey
	 * @return
	 * @throws Exception
	 */
	public String getMenuId(MenuVO menuVo) throws Exception;
	
}
