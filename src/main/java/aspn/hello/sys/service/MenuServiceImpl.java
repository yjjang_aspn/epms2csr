package aspn.hello.sys.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.util.Utility;
import aspn.hello.sys.mapper.MenuMapper;
import aspn.hello.sys.model.MenuVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

/**
 * 메뉴  Business Implement Class
 * @author 이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.07.04  이영탁          최초 생성
*
 * </pre>
 */

@Service("menuService")
public class MenuServiceImpl extends EgovAbstractServiceImpl implements MenuService {

	private static final Logger logger = LoggerFactory.getLogger(MenuServiceImpl.class);

	@Autowired
	private MenuMapper menuMapper;
	
	// gnb 메뉴 정보
    private final Map<String, Object> gnbMenu = new HashMap<String, Object>();
	
	
	/**
	 * 헤더 메뉴 리스트
	 * @author 이영탁
	 * @since 2017.07.05
	 * @param String auth
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, String>> getHeaderMenuList(String auth) throws Exception{
		
		return menuMapper.getHeaderMenuList(auth);  
	}
	
	/**
	 * 왼쪽 메뉴 리스트
	 * @author 이영탁
	 * @since 2017.07.05
	 * @param String auth
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, String>> getLeftMenuList(String auth) throws Exception{
		
		return menuMapper.getLeftMenuList(auth);  
	}
	
	/**
	 * 메뉴관리 리스트
	 * @author 이영탁
	 * @since 2017.07.12
	 * @param menuVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> getMenuMgntList(MenuVO menuVO) throws Exception{
		return menuMapper.getMenuMgntList(menuVO);  
	}
	
	/**
	 * 메뉴관리 그리드 CRUD 처리
	 * @author 이영탁
	 * @since 2017.07.12
	 * @param List<HashMap<String, Object>> saveDataList
	 * @return void
	 * @throws Exception
	 */
	public void menuMgntEdit(List<HashMap<String, Object>> saveDataList, MenuVO menuVO) throws Exception{
		
		if(saveDataList != null){
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				MenuVO menuVO1 = Utility.toBean(tmpSaveData, menuVO.getClass());
//				menuVO1.setMENU_NM(menuVO1.getFIRST_MENU_NM()+menuVO1.getSECOND_MENU_NM()+menuVO1.getTHIRD_MENU_NM());
				menuVO1.setUserId(menuVO.getUserId());
				if (tmpSaveData.containsKey("Changed")) {
					// 메뉴관리 업데이트
					menuMapper.updateMenuMgnt(menuVO1);
					// 권한별 메뉴 업데이트 (메뉴코드 변경시)
					menuMapper.updateMenu2(menuVO1);
				} else if(tmpSaveData.containsKey("Added")) {
					// 메뉴관리 등록
					menuMapper.insertMenuMgnt(menuVO1);
				} else if(tmpSaveData.containsKey("Deleted")) {
					// 메뉴관리 삭제
					menuMapper.deleteMenuMgnt(menuVO1);
				}
			}
		}
	}
	
	
	/**
	 * 메뉴 리스트
	 * @author 이영탁
	 * @since 2017.07.17
	 * @param menuVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> getMenuList(MenuVO menuVO) throws Exception{
		return menuMapper.getMenuList(menuVO);  
	}
	
	/**
	 * 메뉴(권한) 그리드 CRUD 처리
	 * @author 이영탁
	 * @since 2017.07.17
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param MenuVO menuVO
	 * @return void
	 * @throws Exception
	 */
	public void menuEdit(List<HashMap<String, Object>> saveDataList, MenuVO menuVO) throws Exception{
		if(saveDataList != null){
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				MenuVO menuVO1 = Utility.toBean(tmpSaveData, menuVO.getClass());
				if (tmpSaveData.containsKey("Changed")) {
					menuMapper.updateMenu(menuVO1);
				} else if(tmpSaveData.containsKey("Added")) {
					menuMapper.insertMenu(menuVO1);
				} else if(tmpSaveData.containsKey("Deleted")) {
					menuMapper.deleteMenu(menuVO1);
				}
			}
		}
	}

	/**
	 * GNB 메뉴 정보 조회
	 * @author 정순주
	 * @since 2017. 7. 31.
	 * @param menuMd
	 * @return
	 * @throws Exception
	 * @return Map<String,Object>
	 */
	@Override
	public Map<String, Object> getGnbMenuInfo(MenuVO menuVo) throws Exception {
		
		gnbMenu.clear();

//		String[] arr_auth = StringUtils.split(menuVo.getUser_auth(), ",");
//		menuVo.setArrAuth(arr_auth);

		//고객이나 공급사가 로그인 시 좌측에 메뉴 목록을 바로 보여주기 위해 선언
//		if(arr_auth.length == 1) {
//			if("14".equals(arr_auth[0].toString())) {         //고객
//				menuVo.setHEAD_MENU_GROUP("ORDER");
//			} else if("15".equals(arr_auth[0].toString())) {  //공급사
//				menuVo.setHEAD_MENU_GROUP("PR");						
//			}
//		}

		// 메인 메뉴 리스트
		List<MenuVO> map = menuMapper.getMainMenuList(menuVo);

		// 메인 메뉴 리스트
		List<MenuVO> subMap = menuMapper.getSubMenuList(menuVo);

		gnbMenu.clear();
		gnbMenu.put("list", map);
		gnbMenu.put("subMenuList", subMap);
//		gnbMenu.put("menuHeader", menuVo.getHEAD_MENU_GROUP());

		return gnbMenu;
	}
	
	/**
	 * 메뉴 id조회
	 * @param menuKey
	 * @return
	 * @throws Exception
	 */
	public String getMenuId(MenuVO menuVO) throws Exception {
		return menuMapper.getMenuId(menuVO);
	}

}
