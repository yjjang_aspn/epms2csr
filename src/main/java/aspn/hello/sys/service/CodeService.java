/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package aspn.hello.sys.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aspn.hello.rfc.model.ZPMPC_OBJ_TYPE_EXPORT;
import aspn.hello.rfc.model.ZPM_ITEM_EXPORT;
import aspn.hello.rfc.model.ZPM_ORDER_TYPE_EXPORT;
import aspn.hello.sys.model.CodeVO;

/**
 * [공통코드] Service Class
 * 
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *   2018.11.18		김영환			SAP 플랜트별오더유형 조회
 *   2018.11.18		김영환			SAP 오브젝트유형  조회
 *   2018.11.18		김영환			SAP 카테고리ITEM 조회
 *   
 * </pre>
 */

public interface CodeService {

	/**
	 * 공통코드 목록조회
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param CodeVO codeVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> getCodeList(CodeVO codeVO) throws Exception;
	
	/**
	 * 공통코드 그리드 저장
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param CodeVO codeVO
	 * @return void
	 * @throws Exception
	 */
	public void codeEdit(List<HashMap<String, Object>> saveDataList, CodeVO codeVO) throws Exception;
	
	/**
	 * 공통코드상세 목록조회
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param CodeVO codeVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> getCodeDtlList(CodeVO codeVO) throws Exception;

	/**
	 * 공통코드상세 그리드 저장
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param CodeVO codeVO
	 * @return void
	 * @throws Exception
	 */
	public void codeDtlEdit(List<HashMap<String, Object>> saveDataList, CodeVO codeVO) throws Exception;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// SAP
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * SAP 플랜트별오더유형  등록 
	 *
	 * @author 김영환
	 * @since 2018.11.14
	 * @param ZPM_ORDER_TYPE_EXPORT tables
	 * @param ZPM_ORDER_TYPE_IMPORT zpm_order_type_import
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public Map<String, Object> updateOrderTypeInfo(ZPM_ORDER_TYPE_EXPORT tables) throws Exception;
	
	
	/**
	 * SAP 오브젝트유형 등록 
	 *
	 * @author 김영환
	 * @since 2018.11.14
	 * @param ZPMPC_OBJ_TYPE_EXPORT zpmpc_obj_type_export
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public Map<String, Object> updateObjTypeInfo(ZPMPC_OBJ_TYPE_EXPORT zpmpc_obj_type_export) throws Exception;

	/**
	 * SAP 카테고리ITEM 등록 
	 *
	 * @author 김영환
	 * @since 2018.11.19
	 * @param ZPM_ITEM_EXPORT tables
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	Map<String, Object> updateCategoryItem(ZPM_ITEM_EXPORT zpm_item_export)throws Exception;
		
}
