/*
 * Copyright 2011 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package aspn.hello.sys.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.sys.model.CategoryVO;
import aspn.hello.sys.model.CodeVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [공통코드] Mapper Class
 * 
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *   2018.11.18		김영환			SAP 플랜트별오더유형 조회
 *   2018.11.18		김영환			SAP 오브젝트유형  조회
 *   2018.11.18		김영환			SAP 카테고리ITEM 조회
 *   
 * </pre>
 */

@Mapper("codeMapper")
public interface CodeMapper {

	/**
	 * 공통코드 목록조회
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param CodeVO codeVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> getCodeList(CodeVO codeVO) throws Exception;

	/**
	 * 공통코드  등록
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param CodeVO codeVO
	 * @return int
	 * @throws Exception
	 */
	public int insertCode(CodeVO codeVO) throws Exception;
	
	/**
	 * 공통코드 수정
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param CodeVO codeVO
	 * @return int
	 * @throws Exception
	 */
	public int updateCode(CodeVO codeVO) throws Exception;
	
	/**
	 * 공통코드  삭제
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param CodeVO codeVO
	 * @return int
	 * @throws Exception
	 */
	public int deleteCode(CodeVO codeVO) throws Exception;
	
	/**
	 * 공통코드상세 목록조회
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param CodeVO codeVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> getCodeDtlList(CodeVO codeVO) throws Exception;
	
	/**
	 * 공통코드상세  등록
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param CodeVO codeVO
	 * @return int
	 * @throws Exception
	 */
	public int insertCodeDtl(CodeVO codeVO) throws Exception;
	
	/**
	 * 공통코드상세 수정
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param CodeVO codeVO
	 * @return int
	 * @throws Exception
	 */
	public int updateCodeDtl(CodeVO codeVO) throws Exception;
	
	/**
	 * 공통코드상세  삭제
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param CodeVO codeVO
	 * @return int
	 * @throws Exception
	 */
	public int deleteCodeDtl(CodeVO codeVO) throws Exception;

	/**
	 * 코드상세 목록조회
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param HashMap<String, String> param
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, String>> getCodeList(HashMap<String, String> param) throws Exception;

	/**
	 * SAP ComcdOrd 조회 
	 * 
	 * @author 김영환
	 * @since 2018.11.20
	 * @param CodeVO codeVO
	 * @return String
	 * @throws Exception
	 */
	public String selectComcdOrd(CodeVO codeVO);

}
