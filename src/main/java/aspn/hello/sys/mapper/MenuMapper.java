/*
 * Copyright 2011 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package aspn.hello.sys.mapper;


import java.util.HashMap;
import java.util.List;

import aspn.hello.sys.model.MenuVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * 인증에 관한 데이터처리 매퍼 클래스
 *
 * @author  이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see <pre>
 *  == 개정이력(Modification Information) ==
 *
 *          수정일          수정자           수정내용
 *  ----------------    ------------    ---------------------------
 *   2017.07.04       이영탁          최초 생성
 *
 * </pre>
 */
@Mapper("menuMapper")
public interface MenuMapper {

	/**
	 * 헤더 메뉴 리스트
	 * @author 이영탁
	 * @since 2017.07.05
	 * @param String auth
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, String>> getHeaderMenuList(String auth) throws Exception;
	
	/**
	 * 왼쪽 메뉴 리스트
	 * @author 이영탁
	 * @since 2017.07.05
	 * @param String auth
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, String>> getLeftMenuList(String auth) throws Exception;
	
	/**
	 * 메뉴기능 리스트
	 * @author 이영탁
	 * @since 2017.07.12
	 * @param menuVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> getMenuFnctList(String auth) throws Exception;
	
	/**
	 * 메뉴관리 리스트
	 * @author 이영탁
	 * @since 2017.07.12
	 * @param menuVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> getMenuMgntList(MenuVO menuVO) throws Exception;
	
		
	/**
	 * 메뉴관리 업데이트
	 * @author 이영탁
	 * @since 2017.07.14
	 * @param menuVO
	 * @return int
	 * @throws Exception
	 */
	public int updateMenuMgnt(MenuVO menuVO) throws Exception;
	
	/**
	 * 메뉴관리 등록
	 * @author 이영탁
	 * @since 2017.07.14
	 * @param menuVO
	 * @return int
	 * @throws Exception
	 */
	public int insertMenuMgnt(MenuVO menuVO) throws Exception;
	
	/**
	 * 메뉴관리 삭제
	 * @author 이영탁
	 * @since 2017.07.14
	 * @param menuVO
	 * @return int
	 * @throws Exception
	 */
	public int deleteMenuMgnt(MenuVO menuVO) throws Exception;
	
	/**
	 * 권한별 메뉴 리스트
	 * @author 이영탁
	 * @since 2017.07.12
	 * @param menuVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> getMenuList(MenuVO menuVO) throws Exception;
		
	/**
	 * 권한별 메뉴 업데이트 (권한별 메뉴관리)
	 * @author 이영탁
	 * @since 2017.07.17
	 * @param menuVO
	 * @return int
	 * @throws Exception
	 */
	public int updateMenu(MenuVO menuVO) throws Exception;
	
	/**
	 * 권한별 메뉴 업데이트 (메뉴관리에서 메뉴코드 변경시)
	 * @author 이영탁
	 * @since 2017.07.17
	 * @param menuVO
	 * @return int
	 * @throws Exception
	 */
	public int updateMenu2(MenuVO menuVO) throws Exception;
	
	/**
	 * 권한별 메뉴 등록
	 * @author 이영탁
	 * @since 2017.07.17
	 * @param menuVO
	 * @return int
	 * @throws Exception
	 */
	public int insertMenu(MenuVO menuVO) throws Exception;
	
	/**
	 * 권한별 메뉴 삭제
	 * @author 이영탁
	 * @since 2017.07.17
	 * @param menuVO
	 * @return int
	 * @throws Exception
	 */
	public int deleteMenu(MenuVO menuVO) throws Exception;
	
	/**
	 * 메뉴별 기능(버튼)적용 대상  그리드 데이타 
	 * @author 이영탁
	 * @since 2017.07.17
	 * @param menuVO
	 * @return List<HashMap<String, String>
	 * @throws Exception
	 */
	public List<HashMap<String, String>> getTrgtGridList(MenuVO menuVO) throws Exception;
	
		
	/**
	 * 메뉴별 기능(버튼)적용 대상  그리드 업데이트
	 * @author 이영탁
	 * @since 2017.07.17
	 * @param menuVO
	 * @return int
	 * @throws Exception
	 */
	public int updateTrgtGrid(MenuVO menuVO) throws Exception;
	
	/**
	 *메뉴별 기능(버튼)적용 대상  그리드 등록
	 * @author 이영탁
	 * @since 2017.07.17
	 * @param menuVO
	 * @return int
	 * @throws Exception
	 */
	public int insertTrgtGrid(MenuVO menuVO) throws Exception;
	
	/**
	 * 메뉴별 기능(버튼)적용 대상  그리드 삭제
	 * @author 이영탁
	 * @since 2017.07.17
	 * @param menuVO
	 * @return int
	 * @throws Exception
	 */
	public int deleteTrgtGrid(MenuVO menuVO) throws Exception;
	
	/**
	 * 기능(버튼)관리 그리드 데이타
	 * @author 이영탁
	 * @since 2017.07.17 
	 * @param menuVO
	 * @return List<HashMap<String, String>>
	 * @throws Exception
	 */
	public List<HashMap<String, String>> getFnctList(MenuVO menuVO) throws Exception;
	
		
	/**
	 * 기능(버튼)관리 그리드 업데이트
	 * @author 이영탁
	 * @since 2017.07.17
	 * @param menuVO
	 * @return int
	 * @throws Exception
	 */
	public int updateFnct(MenuVO menuVO) throws Exception;
	
	/**
	 * 기능(버튼)관리 그리드 등록
	 * @author 이영탁
	 * @since 2017.07.17
	 * @param menuVO
	 * @return int
	 * @throws Exception
	 */
	public int insertFnct(MenuVO menuVO) throws Exception;
	
	/**
	 * 기능(버튼)관리 그리드 삭제
	 * @author 이영탁
	 * @since 2017.07.17
	 * @param menuVO
	 * @return int
	 * @throws Exception
	 */
	public int deleteFnct(MenuVO menuVO) throws Exception;

	/**
	 * 헤더메뉴가 1개인경우 헤더 메뉴 리스트 조회 
	 * @author 정순주
	 * @since 2017. 7. 31.
	 * @param menuVo
	 * @return List<MenuVO>
	 * @throws Exception
	 */
	public List<MenuVO> getMainMenuList(MenuVO menuVo) throws Exception;

	/**
	 * 헤더메뉴가 1개인경우 서브메뉴 리스트 조회
	 * @author 정순주
	 * @since 2017. 7. 31.
	 * @param menuVo
	 * @return List<MenuVO>
	 * @throws Exception
	 */
	public List<MenuVO> getSubMenuList(MenuVO menuVo) throws Exception;
	
	/**
	 * 메뉴 id조회
	 * @param menuKey
	 * @return
	 * @throws Exception
	 */
	public String getMenuId(MenuVO menuVo) throws Exception;
}
