package aspn.hello.sys.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.TreeGridUtil;
import aspn.com.common.util.Utility;
import aspn.hello.rfc.model.ZPMPC_OBJ_TYPE_EXPORT;
import aspn.hello.rfc.model.ZPM_ITEM_EXPORT;
import aspn.hello.rfc.model.ZPM_ITEM_IMPORT;
import aspn.hello.rfc.model.ZPM_ORDER_TYPE_EXPORT;
import aspn.hello.rfc.model.ZPM_ORDER_TYPE_IMPORT;
import aspn.hello.rfc.model.ZPM_ORDER_TYPE_T_WERKS;
import aspn.hello.rfc.service.RfcService;
import aspn.hello.sys.model.CodeVO;
import aspn.hello.sys.service.CodeService;


/**
 * [공통코드] Controller Class
 * 
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *   2018.11.18		김영환			SAP 플랜트별오더유형 조회
 *   2018.11.18		김영환			SAP 오브젝트유형  조회
 *   2018.11.18		김영환			SAP 카테고리ITEM 조회
 *   
 * </pre>
 */

@Controller
@RequestMapping("/sys/code")
public class CodeController extends ContSupport{
	Logger logger = LoggerFactory.getLogger(this.getClass());	

	@Autowired
	CodeService codeService;
	
	@Autowired
	RfcService rfcService;
	
	/**
	 * [공통코드관리] : 화면호출
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @return "/sys/web/code/codeMgntList"
	 * @throws Exception
	 */
	@RequestMapping(value = "/codeMgntList.do")
	public String codeMgntList() throws Exception {

		return "/sys/web/code/codeMgntList";
	}
	
	/**
	 * [공통코드관리] : [공통코드] 그리드 데이터
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param CodeVO codeVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/getCodeData.do")
	@ResponseBody
	public String getCodeData(CodeVO codeVO, Model model) throws Exception {
		List<HashMap<String, Object>> list = codeService.getCodeList(codeVO);
		return TreeGridUtil.getGridListData(list);
	}
	
	/**
	 * [공통코드관리] : [공통코드] 그리드 레이아웃
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/getCodeLayout.do")
	public String getCodeLayout(Model model) throws Exception {
		
		model.addAttribute("userId", this.getSsUserId());
		
		return "/sys/web/code/codeLayout";
	}
	/**
	 * [공통코드관리] : [공통코드] 그리드 저장
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MemberVO codeVO
	 * @param model Model
	 * @return String xml
	 * @throws Exception
	 */
	@RequestMapping(value = "/codeEdit.do")
	@ResponseBody
	public String codeEdit(CodeVO codeVO, Model model) throws Exception {
		try{
			codeVO.setUserId(this.getSsUserId());
			codeService.codeEdit( Utility.getEditDataList(codeVO.getUploadData()), codeVO);
			return getSaved();
		}catch(Exception e){
			return getSaveFail();
		}
	}
	
	/**
	 * [공통코드관리] : [공통코드상세] 그리드 데이터
	 * @author 서정민
	 * @since 2018.05.09
	 * @param CodeVO codeVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/getCodeDtlData.do")
	@ResponseBody
	public String getCodeDtlData(CodeVO codeVO, Model model) throws Exception {
		List<HashMap<String, Object>> list = codeService.getCodeDtlList(codeVO);
		return TreeGridUtil.getGridListData(list);
	}
	
	/**
	 * [공통코드관리] : [공통코드상세] 그리드 레이아웃
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/getCodeDtlLayout.do")
	public String getCodeDtlLayout(Model model) throws Exception {
		
		model.addAttribute("userId", this.getSsUserId());
		
		return "/sys/web/code/codeDtlLayout";
	}
	
	/**
	 * [공통코드관리] : [공통코드상세] 그리드 저장
	 * @author 이영탁
	 * @since 2017.07.24
	 * @param MemberVO codeVO
	 * @param model Model
	 * @return String xml
	 * @throws Exception
	 */
	@RequestMapping(value = "/codeDtlEdit.do")
	@ResponseBody
	public String codeDtlEdit(CodeVO codeVO, Model model) throws Exception {
		try{
			codeVO.setUserId(this.getSsUserId());
			codeService.codeDtlEdit( Utility.getEditDataList(codeVO.getUploadData()), codeVO);
			return getSaved();
		}catch(Exception e){
			return getSaveFail();
		}
	}
	
	/**
	 * [사용자권한] : 공장파트관리(파트구분,파트통합) 팝업 호출
	 * 
	 * @author 김영환
	 * @since 2018.11.18 
	 * @param CodeVO codeVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popSyncCode.do")
	public String popSyncCode(CodeVO codeVO, Model model) {
		
		return "/sys/web/code/popSyncCode";
		
	}
	
	/**
	 * SAP 플랜트별오더유형 조회/등록 
	 *
	 * @author 김영환
	 * @since 2018.11.18
	 * @param ZPM_ORDER_TYPE_IMPORT zpm_order_type_import
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateOrderTypeInfoAjax.do")
	@ResponseBody
	public Map<String, Object> updateOrderTypeInfo() throws Exception {

		ZPM_ORDER_TYPE_T_WERKS t_werksvo = new ZPM_ORDER_TYPE_T_WERKS();
		t_werksvo.setWERKS("5100");		//던킨:안양공장-5100
		
		List<ZPM_ORDER_TYPE_T_WERKS> t_werksList = new ArrayList<ZPM_ORDER_TYPE_T_WERKS>();
		t_werksList.add(0, t_werksvo);
		
		ZPM_ORDER_TYPE_IMPORT zpm_order_type_import = new ZPM_ORDER_TYPE_IMPORT();
		zpm_order_type_import.setZpm_order_type_t_werks(t_werksList);
		
		//SAP 플랜트별오더유형  조회
		ZPM_ORDER_TYPE_EXPORT tables = rfcService.getOrderTypeInfo(t_werksList);
		Map<String, Object> map = codeService.updateOrderTypeInfo(tables);
		
		return map;
	}
	
	/**
	 * SAP 오브젝트유형 조회/등록 
	 *
	 * @author 김영환
	 * @since 2018.11.18
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateObjTypeInfoAjax.do")
	@ResponseBody
	public Map<String, Object> updateObjTypeInfo() throws Exception {
	
		ZPMPC_OBJ_TYPE_EXPORT zpmpc_obj_type_export  = new ZPMPC_OBJ_TYPE_EXPORT();
		
		//SAP 오브젝트유형  조회
		zpmpc_obj_type_export = rfcService.getObjTypeInfo();
		Map<String, Object> map = codeService.updateObjTypeInfo(zpmpc_obj_type_export);
		
		return map;

	}
	
	/**
	 * SAP 카테고리ITEM 조회/등록 
	 *
	 * @author 김영환
	 * @since 2018.11.18
	 * @param ZPM_ITEM_IMPORT zpm_item_import
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateCategoryItemAjax.do")
	@ResponseBody
	public Map<String, Object> updateCategoryItemAjax() throws Exception {
		
		ZPM_ITEM_IMPORT zpm_item_import = new ZPM_ITEM_IMPORT();
		ZPM_ITEM_EXPORT zpm_item_export  = new ZPM_ITEM_EXPORT();
		
		//SAP 카테고리ITEM 조회
		zpm_item_export = rfcService.getCategoryItemInfo(zpm_item_import);
		Map<String, Object> map = codeService.updateCategoryItem(zpm_item_export);
		
		return map;

	}

}
