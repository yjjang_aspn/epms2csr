package aspn.hello.sys.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.util.BizException;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.SessionUtil;
import aspn.com.common.util.TreeGridUtil;
import aspn.com.common.util.Utility;
import aspn.hello.sys.model.MenuVO;
import aspn.hello.sys.service.MenuService;

/**
 * 메뉴 관련  컨트롤러 클래스
 * @author 이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.07.04  이영탁          최초 생성
*
 * </pre>
 */

@Controller
@RequestMapping("/sys/menu")
public class MenuController extends ContSupport{
	Logger logger = LoggerFactory.getLogger(this.getClass());	

	@Autowired
	MenuService menuService;
	
	/**
	 * 헤더 메뉴 조회
	 * @author 이영탁
	 * @since 2017.07.05
	 * @param request
	 * @param response
	 * @param menuMd
	 * @param model
	 * @return "/com/web/headerMenuBar"
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/headerMenuBar.do")
	public String headerMenuBar(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		model.addAttribute("headerMenuList", (ArrayList<HashMap<String, String>>) session.getAttribute("ssHeaderMenuList"));
		return  "/com/web/headerMenuBar";
		
	}
	
	/**
	 * 헤더 메뉴 조회 - 헤더 메뉴가 한개인 경우 처리
	 * @author 정순주
	 * @since 2017. 7. 31.
	 * @param request
	 * @param response
	 * @param menuMd
	 * @param model
	 * @return
	 * @throws Exception
	 * @return String
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value = "/headerMenuBar2.do")
	public String header_menuBar2(HttpServletRequest request, HttpServletResponse response, MenuVO menuVo, HttpSession session,
			Model model) throws Exception {

		// 헤더 메뉴
		Map<String, Object> map = new HashMap<String, Object>();

		int headMenuCnt = (int) session.getAttribute("ssHeadMenuCnt");
		String auth = (String) SessionUtil.getAttribute("ssAuth");
		menuVo.setAuth(auth);
		
		if (headMenuCnt == 1) {
			
			map = menuService.getGnbMenuInfo(menuVo);

			// 메인메뉴
			List<MenuVO> list = (List<MenuVO>) map.get("list");

			// 서브메뉴
			List<MenuVO> subMenuList = (List<MenuVO>) map.get("subMenuList");

			model.addAttribute("list", list);
			model.addAttribute("subMenuList", subMenuList);
		} else {
			throw new BizException("메뉴 정보를 조회 할 수 없습니다.");
		}

		if (map == null) {
			throw new BizException("메뉴 정보를 조회 할 수 없습니다.");
		}

		return "/com/web/headerMenuBar2";
	}
	
	/**
	 * 왼쪽 메뉴 조회
	 * @author 이영탁
	 * @since 2017.07.05
	 * @param request
	 * @param response
	 * @param menuMd
	 * @param model
	 * @return "/com/web/headerMenuBar"
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/leftMenuBar.do")
	public String leftMenuBar(HttpServletRequest request, MenuVO menuVO, Model model, HttpSession session)
			throws Exception {
		
		model.addAttribute("leftMenuList", (ArrayList<HashMap<String, String>>) session.getAttribute("ssLeftMenuList"));
		return "/com/web/leftMenuBar";
	}
	
	/**
	 * 메뉴 관리 목록 
	 * @author 이영탁
	 * @since 2017.07.05
	 * @return "/com/web/headerMenuBar"
	 * @throws Exception
	 */
	@RequestMapping(value = "/menuMgntList.do")
	public String getMenuMgntList() throws Exception {		
		
		return "/sys/web/menuMgnt/menuMgntList"; 
	}
	
	/**
	 * 메뉴관리 목록 그리드 데이타
	 * @author 이영탁
	 * @since 2017.07.05
	 * @param menuMd
	 * @param model
	 * @return xml
	 * @throws Exception
	 */
	@RequestMapping(value = "/getMenuMgntData.do")
	@ResponseBody
	public String getMenuMgntData(MenuVO menuVO, Model model) throws Exception {
//		model.addAttribute("menuMgntList", menuService.getMenuMgntList(menuVO));
//		return "/sys/web/menuMgnt/menuMgntData";
		
		List<HashMap<String, Object>> menuMgntList = menuService.getMenuMgntList(menuVO);
		return TreeGridUtil.getGridTreeData(menuMgntList);
	}
	
	/**
	 * 메뉴관리 목록 그리드 레이아웃
	 * @author 이영탁
	 * @since 2017.07.05
	 * @return "/sys/web/menuMgnt/menuMgntLayout"
	 * @throws Exception
	 */
	@RequestMapping(value = "/getMenuMgntLayout.do")
	public String getMenuMgntLayout(Model model) throws Exception {
		
		model.addAttribute("userId", this.getSsUserId());
		
		return "/sys/web/menuMgnt/menuMgntLayout";
	}
	
	/**
	 * 메뉴관리 Grid CRUD 처리
	 * @author 이영탁
	 * @since 2017.07.05
	 * @param menuVO MenuVO
	 * @param model Model
	 * @return xml
	 * @throws Exception
	 */
	@RequestMapping(value = "/menuMgntEdit.do")
	@ResponseBody
	public String menuMgntEdit(HttpServletRequest request, MenuVO menuVO, Model model) throws Exception {
		
		menuVO.setUserId(getSsUserId());
		
		try {
			menuService.menuMgntEdit( Utility.getEditDataList(menuVO.getUploadData()), menuVO );
			return getSaved();
		}catch(Exception e){
			e.printStackTrace();
			return getSaveFail();
		}
	}
	
	/**
	 * 메뉴 권한 조회 화면 
	 * @author 이영탁
	 * @since 2017.07.05
	 * @return "/sys/web/menuAuth/menuAuthList"
	 * @throws Exception
	 */
	@RequestMapping(value = "/menuAuthList.do")
	public String menuAuthList() throws Exception {

		return "/sys/web/menuAuth/menuAuthList";
	}

	/**
	 * 메뉴 목록 그리드 데이타
	 * @author 이영탁
	 * @since 2017.07.05
	 * @param menuVO
	 * @param model
	 * @return xml
	 * @throws Exception
	 */
	@RequestMapping(value = "/getMenuData.do")
	@ResponseBody
	public String getMenuData(MenuVO menuVO, Model model) throws Exception {
		List<HashMap<String, Object>> list = menuService.getMenuList(menuVO);
		return TreeGridUtil.getGridTreeData(list);
	}

	/**
	 * 메뉴 목록 그리드 레이아웃
	 * @author 이영탁
	 * @since 2017.07.05
	 * @return "/sys/web/menuMgnt/menuLayout"
	 * @throws Exception
	 */
	@RequestMapping(value = "/getMenuLayout.do")
	public String getMenuLayout() throws Exception {

		return "/sys/web/menuAuth/menuLayout";
	}

	/**
	 * 메뉴 Grid CRUD 처리
	 * @author 이영탁
	 * @since 2017.07.05
	 * @param menuVO MenuVO
	 * @param model Model
	 * @return xml
	 * @throws Exception
	 */
	@RequestMapping(value = "/menuEdit.do")
	@ResponseBody
	public String menuEdit(MenuVO menuVO, Model model) throws Exception {
		try{
			menuService.menuEdit( Utility.getEditDataList(menuVO.getUploadData()), menuVO  );
			return getSaved();
		}catch(Exception e){
			return getSaveFail();
		}
	}

	

}
