package aspn.hello.sys.controller;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.Utility;
import aspn.hello.sys.model.CategoryVO;
import aspn.hello.sys.service.CategoryService;


/**
 * [카테고리] Controller Class
 * 
 * @author 서정민
 * @since 2018.02.08
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.08		서정민			최초 생성
 *   
 * </pre>
 */

@Controller
@RequestMapping("/sys/category")
public class CategoryController extends ContSupport{
	Logger logger = LoggerFactory.getLogger(this.getClass());	

	@Autowired
	CategoryService categoryService;
	
	/**
	 * [카테고리] 그리드 데이터
	 * 
	 * @author 서정민
	 * @since 2018.02.08
	 * @param CategoryVO categoryVO
	 * @param Model model
	 * @return String xml
	 * @throws Exception
	 */
	@RequestMapping(value = "/categoryData.do")
	public String categoryData(CategoryVO categoryVO, Model model) throws Exception {

		try {
			//카테고리 목록
			categoryVO.setSubAuth(getSsSubAuth());
			List<HashMap<String, Object>> list = categoryService.selectCategoryList(categoryVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return "/sys/web/category/categoryData";
	}
	
	/**
	 * [카테고리] 그리드 레이아웃(수정가능)
	 * 
	 * @author 서정민
	 * @since 2018.02.08
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/categoryLayout.do")
	public String categoryLayout(Model model) throws Exception{
		
		return "/sys/web/category/categoryLayout";
	}
	
	/**
	 * [카테고리] 그리드 레이아웃(수정불가)
	 * 
	 * @author 서정민
	 * @since 2017.02.08
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/categoryNotEditLayout.do")
	public String categoryNotEditLayout(CategoryVO categoryVO, Model model) throws Exception{
		
		return "/sys/web/category/categoryNotEditLayout";
	}

	/**
	 * [카테고리] 그리드 저장
	 * 
	 * @author 서정민
	 * @since 2018.02.08
	 * @param CategoryVO categoryVO
	 * @param Model model
	 * @return String xml
	 * @throws Exception
	 */
	@RequestMapping(value = "/categoryEdit.do")
	@ResponseBody
	public String categoryEdit(CategoryVO categoryVO, Model model) throws Exception {
		
		try {
			
			categoryVO.setUserId(this.getSsUserId());
			
			categoryService.categoryEdit( Utility.getEditDataList(categoryVO.getUploadData()) , categoryVO);
			
			return getSaved();
			
		} catch(Exception e) {
			return getSaveFail();
		}
		
	}
	

}
