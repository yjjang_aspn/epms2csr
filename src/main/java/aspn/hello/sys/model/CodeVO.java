package aspn.hello.sys.model;

import aspn.hello.com.model.AbstractVO;

/**
 * 공통코드VO 클래스
 * @author 이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2017.07.04		이영탁		최초 생성
 *   2018.03.02		김영환		권한여부 추가 : subAuthYn
 *
 * </pre>
 */

@SuppressWarnings("serial")
public class CodeVO extends AbstractVO{

	/** COMCD DB model  */
	private String COMCD_GRP = "";
	private String COMCD_GRP_NM = "";
	private String COMCD_GRP_DESC = "";
	private String COMCD = "";
	private String COMCD_NM = "";
	private String COMCD_DESC = "";
	private String COMCD_ORD = "";
	private String UPPER_COMCD = "";
	private String COMPANY_ID = "";
	private String LOCATION = "";
	private String REG_ID = "";
	private String REG_DT = "";
	private String UPD_ID = "";
	private String UPD_DT = "";
	private String DEL_YN = "";
	
	/** PARAMETER */
	private String comcdGrp = "";
	private String subAuthYn = "";
	private String REMARKS = "";
	
	public String getCOMCD_GRP_DESC() {
		return COMCD_GRP_DESC;
	}
	public void setCOMCD_GRP_DESC(String cOMCD_GRP_DESC) {
		COMCD_GRP_DESC = cOMCD_GRP_DESC;
	}
	public String getCOMCD_GRP_NM() {
		return COMCD_GRP_NM;
	}
	public void setCOMCD_GRP_NM(String cOMCD_GRP_NM) {
		COMCD_GRP_NM = cOMCD_GRP_NM;
	}
	public String getComcdGrp() {
		return comcdGrp;
	}
	public void setComcdGrp(String comcdGrp) {
		this.comcdGrp = comcdGrp;
	}
	public String getCOMCD_GRP() {
		return COMCD_GRP;
	}
	public void setCOMCD_GRP(String cOMCD_GRP) {
		COMCD_GRP = cOMCD_GRP;
	}
	public String getCOMCD() {
		return COMCD;
	}
	public void setCOMCD(String cOMCD) {
		COMCD = cOMCD;
	}
	public String getCOMCD_NM() {
		return COMCD_NM;
	}
	public void setCOMCD_NM(String cOMCD_NM) {
		COMCD_NM = cOMCD_NM;
	}
	public String getCOMCD_DESC() {
		return COMCD_DESC;
	}
	public void setCOMCD_DESC(String cOMCD_DESC) {
		COMCD_DESC = cOMCD_DESC;
	}
	public String getCOMCD_ORD() {
		return COMCD_ORD;
	}
	public void setCOMCD_ORD(String cOMCD_ORD) {
		COMCD_ORD = cOMCD_ORD;
	}
	public String getUPPER_COMCD() {
		return UPPER_COMCD;
	}
	public void setUPPER_COMCD(String uPPER_COMCD) {
		UPPER_COMCD = uPPER_COMCD;
	}
	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}
	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getREG_ID() {
		return REG_ID;
	}
	public void setREG_ID(String rEG_ID) {
		REG_ID = rEG_ID;
	}
	public String getREG_DT() {
		return REG_DT;
	}
	public void setREG_DT(String rEG_DT) {
		REG_DT = rEG_DT;
	}
	public String getUPD_ID() {
		return UPD_ID;
	}
	public void setUPD_ID(String uPD_ID) {
		UPD_ID = uPD_ID;
	}
	public String getUPD_DT() {
		return UPD_DT;
	}
	public void setUPD_DT(String uPD_DT) {
		UPD_DT = uPD_DT;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getSubAuthYn() {
		return subAuthYn;
	}
	public void setSubAuthYn(String subAuthYn) {
		this.subAuthYn = subAuthYn;
	}
	public String getREMARKS() {
		return REMARKS;
	}
	public void setREMARKS(String rEMARKS) {
		REMARKS = rEMARKS;
	}
	
}
