package aspn.hello.sys.model;

import aspn.hello.com.model.AbstractVO;

/**
 * [카테고리] VO Class
 * @author jmseo
 * @since 2018.02.08
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.08		jmseo		최초 생성
 *   
 * </pre>
 */

@SuppressWarnings("serial")
public class CategoryVO extends AbstractVO{

	/** CATEGORY DB  */
	private String CATEGORY   		= "";	// 1.카테고리ID
	private String CATEGORY_UID		= "";	// 2.카테고리코드
	private String CATEGORY_NAME	= "";	// 2.카테고리명
	private String CATEGORY_TYPE	= "";	// 3.카테고리 유형
	private String PARENT_CATEGORY	= "";	// 4.부모카테고리
	private String TREE   			= "";	// 5.구조체
	private String DEPTH	  		= "";	// 6.레벨
	private String SEQ_DSP			= "";	// 7.출력순서
	private String COMPANY_ID		= "";	// 8.회사코드
	private String LOCATION			= "";	// 9.공장코드
	private String SUB_ROOT			= "";	//10.공장구분(설비관리)
	private String REG_ID			= "";	//11.등록자
	private String REG_DT			= "";	//12.등록일
	private String UPD_ID			= "";	//13.수정자
	private String UPD_DT			= "";	//14.수정일
	private String DEL_YN			= "";	//15.삭제여부
	
	/** query result */
	private String CUR_LVL			= "";
	private String NEXT_LVL			= "";
	private String PRE_LVL			= "";
	
	public String getCATEGORY() {
		return CATEGORY;
	}
	public void setCATEGORY(String cATEGORY) {
		CATEGORY = cATEGORY;
	}
	public String getCATEGORY_UID() {
		return CATEGORY_UID;
	}
	public void setCATEGORY_UID(String cATEGORY_UID) {
		CATEGORY_UID = cATEGORY_UID;
	}
	public String getCATEGORY_NAME() {
		return CATEGORY_NAME;
	}
	public void setCATEGORY_NAME(String cATEGORY_NAME) {
		CATEGORY_NAME = cATEGORY_NAME;
	}
	public String getCATEGORY_TYPE() {
		return CATEGORY_TYPE;
	}
	public void setCATEGORY_TYPE(String cATEGORY_TYPE) {
		CATEGORY_TYPE = cATEGORY_TYPE;
	}
	public String getPARENT_CATEGORY() {
		return PARENT_CATEGORY;
	}
	public void setPARENT_CATEGORY(String pARENT_CATEGORY) {
		PARENT_CATEGORY = pARENT_CATEGORY;
	}
	public String getTREE() {
		return TREE;
	}
	public void setTREE(String tREE) {
		TREE = tREE;
	}
	public String getDEPTH() {
		return DEPTH;
	}
	public void setDEPTH(String dEPTH) {
		DEPTH = dEPTH;
	}
	public String getSEQ_DSP() {
		return SEQ_DSP;
	}
	public void setSEQ_DSP(String sEQ_DSP) {
		SEQ_DSP = sEQ_DSP;
	}
	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}
	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getSUB_ROOT() {
		return SUB_ROOT;
	}
	public void setSUB_ROOT(String sUB_ROOT) {
		SUB_ROOT = sUB_ROOT;
	}
	public String getREG_ID() {
		return REG_ID;
	}
	public void setREG_ID(String rEG_ID) {
		REG_ID = rEG_ID;
	}
	public String getREG_DT() {
		return REG_DT;
	}
	public void setREG_DT(String rEG_DT) {
		REG_DT = rEG_DT;
	}
	public String getUPD_ID() {
		return UPD_ID;
	}
	public void setUPD_ID(String uPD_ID) {
		UPD_ID = uPD_ID;
	}
	public String getUPD_DT() {
		return UPD_DT;
	}
	public void setUPD_DT(String uPD_DT) {
		UPD_DT = uPD_DT;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getCUR_LVL() {
		return CUR_LVL;
	}
	public void setCUR_LVL(String cUR_LVL) {
		CUR_LVL = cUR_LVL;
	}
	public String getNEXT_LVL() {
		return NEXT_LVL;
	}
	public void setNEXT_LVL(String nEXT_LVL) {
		NEXT_LVL = nEXT_LVL;
	}
	public String getPRE_LVL() {
		return PRE_LVL;
	}
	public void setPRE_LVL(String pRE_LVL) {
		PRE_LVL = pRE_LVL;
	}
	
}
