package aspn.hello.sys.model;

import aspn.hello.com.model.AbstractVO;

/**
 * 메뉴VO 클래스
 * @author 이영탁
 * @since 2017.07.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.07.04  이영탁          최초 생성
*
 * </pre>
 */
public class MenuVO extends AbstractVO{

	private static final long serialVersionUID = 1L;
	
	/** MENU DB model  */
	private String USER_PRIV	= "";
	private String WORK_CD		= "";
	private String MENU_CD		= "";
	private String USE_YN		= "";
	private String REG_ID		= "";
	private String TRGT_ID		= "";
	private String FNCT_CD		= "";
	private String MENU_LVL		= "";
	private String UPPR_MENU_CD	= "";
	private String MENU_NM		= "";
	private String FIRST_MENU_NM	= "";
	private String SECOND_MENU_NM	= "";
	private String THIRD_MENU_NM	= "";
	private String MENU_URL		= "";
	private String POPUP_YN		= "";
	private String SORT_ORD		= "";
	private String WORK_GRP		= "";
	private String DEL_YN		= "";
	
	/** PARAMETER */
	private String ORG_MENU_CD	= "";
	private String userPriv = "";
	private String workCd = "";
	private String menuCd = "";
	private String auth	  = "";
	
	public String getUSER_PRIV() {
		return USER_PRIV;
	}
	public void setUSER_PRIV(String uSER_PRIV) {
		USER_PRIV = uSER_PRIV;
	}
	public String getWORK_CD() {
		return WORK_CD;
	}
	public void setWORK_CD(String wORK_CD) {
		WORK_CD = wORK_CD;
	}
	public String getMENU_CD() {
		return MENU_CD;
	}
	public void setMENU_CD(String mENU_CD) {
		MENU_CD = mENU_CD;
	}
	public String getUSE_YN() {
		return USE_YN;
	}
	public void setUSE_YN(String uSE_YN) {
		USE_YN = uSE_YN;
	}
	public String getREG_ID() {
		return REG_ID;
	}
	public void setREG_ID(String rEG_ID) {
		REG_ID = rEG_ID;
	}
	public String getTRGT_ID() {
		return TRGT_ID;
	}
	public void setTRGT_ID(String tRGT_ID) {
		TRGT_ID = tRGT_ID;
	}
	public String getFNCT_CD() {
		return FNCT_CD;
	}
	public void setFNCT_CD(String fNCT_CD) {
		FNCT_CD = fNCT_CD;
	}
	public String getMENU_LVL() {
		return MENU_LVL;
	}
	public void setMENU_LVL(String mENU_LVL) {
		MENU_LVL = mENU_LVL;
	}
	public String getUPPR_MENU_CD() {
		return UPPR_MENU_CD;
	}
	public void setUPPR_MENU_CD(String uPPR_MENU_CD) {
		UPPR_MENU_CD = uPPR_MENU_CD;
	}
	public String getMENU_NM() {
		return MENU_NM;
	}
	public void setMENU_NM(String mENU_NM) {
		MENU_NM = mENU_NM;
	}
	public String getFIRST_MENU_NM() {
		return FIRST_MENU_NM;
	}
	public void setFIRST_MENU_NM(String fIRST_MENU_NM) {
		FIRST_MENU_NM = fIRST_MENU_NM;
	}
	public String getSECOND_MENU_NM() {
		return SECOND_MENU_NM;
	}
	public void setSECOND_MENU_NM(String sECOND_MENU_NM) {
		SECOND_MENU_NM = sECOND_MENU_NM;
	}
	public String getTHIRD_MENU_NM() {
		return THIRD_MENU_NM;
	}
	public void setTHIRD_MENU_NM(String tHIRD_MENU_NM) {
		THIRD_MENU_NM = tHIRD_MENU_NM;
	}
	public String getMENU_URL() {
		return MENU_URL;
	}
	public void setMENU_URL(String mENU_URL) {
		MENU_URL = mENU_URL;
	}
	public String getPOPUP_YN() {
		return POPUP_YN;
	}
	public void setPOPUP_YN(String pOPUP_YN) {
		POPUP_YN = pOPUP_YN;
	}
	public String getSORT_ORD() {
		return SORT_ORD;
	}
	public void setSORT_ORD(String sORT_ORD) {
		SORT_ORD = sORT_ORD;
	}
	public String getWORK_GRP() {
		return WORK_GRP;
	}
	public void setWORK_GRP(String wORK_GRP) {
		WORK_GRP = wORK_GRP;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getORG_MENU_CD() {
		return ORG_MENU_CD;
	}
	public void setORG_MENU_CD(String oRG_MENU_CD) {
		ORG_MENU_CD = oRG_MENU_CD;
	}
	public String getUserPriv() {
		return userPriv;
	}
	public void setUserPriv(String userPriv) {
		this.userPriv = userPriv;
	}
	public String getWorkCd() {
		return workCd;
	}
	public void setWorkCd(String workCd) {
		this.workCd = workCd;
	}
	public String getMenuCd() {
		return menuCd;
	}
	public void setMenuCd(String menuCd) {
		this.menuCd = menuCd;
	}
	public String getAuth() {
		return auth;
	}
	public void setAuth(String auth) {
		this.auth = auth;
	}
	
	@Override
	public String toString() {
		return "MenuVO [USER_PRIV=" + USER_PRIV + ", WORK_CD=" + WORK_CD + ", MENU_CD=" + MENU_CD + ", USE_YN=" + USE_YN
				+ ", REG_ID=" + REG_ID + ", TRGT_ID=" + TRGT_ID + ", FNCT_CD=" + FNCT_CD + ", MENU_LVL=" + MENU_LVL
				+ ", UPPR_MENU_CD=" + UPPR_MENU_CD + ", MENU_NM=" + MENU_NM + ", FIRST_MENU_NM=" + FIRST_MENU_NM
				+ ", SECOND_MENU_NM=" + SECOND_MENU_NM + ", THIRD_MENU_NM=" + THIRD_MENU_NM + ", MENU_URL=" + MENU_URL
				+ ", POPUP_YN=" + POPUP_YN + ", SORT_ORD=" + SORT_ORD + ", WORK_GRP=" + WORK_GRP + ", userPriv="
				+ userPriv + ", workCd=" + workCd + ", menuCd=" + menuCd + "]";
	}
	
}
