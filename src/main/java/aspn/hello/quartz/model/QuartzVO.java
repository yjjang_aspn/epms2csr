package aspn.hello.quartz.model;

import aspn.hello.com.model.AbstractVO;

public class QuartzVO extends AbstractVO{

	private static final long serialVersionUID = 1L;
	
	private String retCode = "";
	private String retMsg = "";
	
	public String getRetCode() {
		return retCode;
	}
	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}
	public String getRetMsg() {
		return retMsg;
	}
	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}
	
	
}
