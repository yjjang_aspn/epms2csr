package aspn.hello.quartz;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StopWatch;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.ObjUtil;
import aspn.com.common.util.Utility;
import aspn.hello.com.service.DeviceService;
import aspn.hello.epms.repair.request.model.RepairRequestVO;
import aspn.hello.epms.repair.request.service.RepairRequestService;
import aspn.hello.quartz.model.QuartzVO;
import aspn.hello.quartz.service.QuartzService;
import aspn.hello.rfc.model.ZPM_PREORDER_EXPORT;
import aspn.hello.rfc.model.ZPM_PREORDER_IMPORT;
import aspn.hello.rfc.model.ZPM_PREORDER_T_AFKO;
import aspn.hello.rfc.service.RfcService;

@Component(value="quartz")
public class Quartz extends ContSupport{

	Logger logger = LoggerFactory.getLogger(this.getClass());	
	
	@Autowired
	private QuartzService quartzService;

	@Autowired
	DeviceService deviceService;

	@Autowired
	RfcService rfcService;
	
	@Autowired
	RepairRequestService repairRequestService;
	
	
	/**
	 * 예방점검 결과 업데이트
	 * 예방점검타입이 예방점검(02)인경우 계획일이 지나면 미점검 체크후 다음 계획일 데이터 생성
	 * 매일 00시
	 * @author 이영탁
	 * @since 2018.04.27
	 * @param
	 * @return void
	 * @throws Exception
	 */
	// 2019.03.25 던킨 요청으로 사용안함
//	@Scheduled(cron="0 0 0 * * *")
//	public void preventiveResultCheck() throws Exception{
//		
//		// 프로퍼티 객체 생성
//        Properties _config = new Properties();
//        // 프로퍼티 파일 로딩
//        _config.load(new java.io.BufferedInputStream(new FileInputStream(ResourceUtils.getFile("classpath:/config/config.properties"))));
//        
//        if(_config.getProperty("batch_mode").equals("true")){
//        	
//        	logger.debug("================== 예방점검 업데이트 start ==================");
//    		StopWatch stopWatch = new StopWatch();
//    		if(true) {
//    			try {
//    				stopWatch.start();
//    				QuartzVO quartzVO= new QuartzVO();
//    				quartzService.updatePreventiveResult(quartzVO);
//    				
//    				if(quartzVO.getRetCode().equals("S")){
//    					logger.debug("처리완료");
//    				}else{
//    					logger.debug("처리시 오류발생: "+quartzVO.getRetMsg());
//    				}
//    				
//    			} catch(Exception e) {
//    				logger.error(e.getMessage());
//    			}
//    		}
//    		stopWatch.stop();
//    		logger.debug("# 메소드종료[runtime: " + (stopWatch.getTotalTimeMillis()/1000) % 60 + "(s)]");
//    		logger.debug("================== 예방점검 업데이트 end ====================");
//    		
//        }
//	}
	
	/**
	 * 업무현황 푸시알림
	 * 매일 08시
	 * @author 서정민
	 * @since 2018.07.09
	 * @param
	 * @return void
	 * @throws Exception
	 */
	@Scheduled(cron="0 0 8 * * *")
	public void sendPushWorkingSatus() throws Exception{
		
		// 프로퍼티 객체 생성
        Properties _config = new Properties();
        // 프로퍼티 파일 로딩
        _config.load(new java.io.BufferedInputStream(new FileInputStream(ResourceUtils.getFile("classpath:/config/config.properties"))));
        
        if(_config.getProperty("batch_mode").equals("true")){
        	
        	logger.debug("================== 업무현황 푸시알림 start ==================");
    		StopWatch stopWatch = new StopWatch();
			try {
				stopWatch.start();
				
				List<HashMap<String, Object>> pushList = quartzService.selectWorkingStatusList();
				
				if(pushList.size()>0){
					for(HashMap<String, Object> item : pushList){
						// Push 정보
						HashMap<String, Object> pushParam = new HashMap<String, Object>();
						String pushTitle = "";
						// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REQUEST_CANCEL-요청취소, NOTICE-공지사항, ALARM-업무현황알림
						pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_ALARM);
						
						// Push Title
						pushTitle = "[ " + DateTime.getMonth() + "월 " + DateTime.getDay() + "일 " + item.get("NAME").toString() + "님의 업무현황 ]"
								  + System.getProperty("line.separator") + "▷ 정비 : " + item.get("REPAIR_CNT").toString() + "건";
						pushParam.put("PUSHTITLE", pushTitle);
						// 상세화면 IDX 세팅
						pushParam.put("PUSHMENUDETAILNDX", "");
						// 알림음 유형 세팅(01:일반, 02:긴급)
						pushParam.put("PUSHALARM", "01");
						// USER_ID세팅
						pushParam.put("USER_ID", item.get("USER_ID"));
						
						deviceService.sendPush(pushParam);
					}
				}
				logger.debug("처리완료");
				
			} catch(Exception e) {
				logger.error(e.getMessage());
			}
    		stopWatch.stop();
    		logger.debug("# 메소드종료[runtime: " + (stopWatch.getTotalTimeMillis()/1000) % 60 + "(s)]");
    		logger.debug("================== 업무현황 푸시알림 end ====================");
    		
        }
	}
	
	/**
	 * SAP 예방보전 오더 생성
	 * 매일 03시
	 * @author 서정민
	 * @since 2019.04.30
	 * @param
	 * @return void
	 * @throws Exception
	 */
	@Scheduled(cron="0 0 3 * * *")
	public void preventiveOrderInsert() throws Exception{
		
		// 프로퍼티 객체 생성
        Properties _config = new Properties();
        // 프로퍼티 파일 로딩
        _config.load(new java.io.BufferedInputStream(new FileInputStream(ResourceUtils.getFile("classpath:/config/config.properties"))));
        
        if(_config.getProperty("batch_mode").equals("true")){
        	
        	logger.debug("================== SAP 예방보전 오더 생성 start ==================");
    		StopWatch stopWatch = new StopWatch();
			try {
				stopWatch.start();				
				
				// SAP에서 보내준 예방보전 있는지 카운트 
				int sap_preventive_request_cnt = 0;
				String E_RESULT = null;
				String E_MESSAGE = null;

				// SAP의 예방보전오더정보 I/F
				ZPM_PREORDER_IMPORT zpm_preorder_import = new ZPM_PREORDER_IMPORT();
				zpm_preorder_import.setI_SPMON(DateTime.getShortDateString().substring(0,6));
				
				ZPM_PREORDER_EXPORT tables = rfcService.getPreventiveOrderInfo(zpm_preorder_import);
				
				RepairRequestVO repairRequestVO = new RepairRequestVO();
				
				if(tables != null){
					
					for (ZPM_PREORDER_T_AFKO zpm_preorder_t_afko : tables.getZpm_preorder_t_afko()) {
						
						repairRequestVO = new RepairRequestVO();
						repairRequestVO.setEQUIPMENT_UID(zpm_preorder_t_afko.getEQUNR());	//설비코드
						
						//설비정보 상세 조회 : 파트구분 통합여부 / 위치정보
						HashMap<String, Object> equipmentDtl = repairRequestService.selectEquipmentDtl(repairRequestVO);
						
						if(equipmentDtl !=null){
							
							//SAP에서 EQUIPMENT_UID값을 return하기 때문에 EQUIPMENT은 조회후 셋팅
							repairRequestVO.setEQUIPMENT(equipmentDtl.get("EQUIPMENT").toString());
							
							//파트구분 공장일 경우
							if(CodeConstants.REMARKS_SEPARATE.equals(String.valueOf(equipmentDtl.get("REMARKS")))){
								repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
							//파트통합 공장일 경우
							} else if(CodeConstants.REMARKS_UNITED.equals(String.valueOf(equipmentDtl.get("REMARKS")))){
								repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
							}

							repairRequestVO.setCompanyId("BR00");	//"sap(예방보전)"계정 소속회사
							repairRequestVO.setSsLocation("AY");	//"sap(예방보전)"계정 소속공장
							repairRequestVO.setUserId("sap");		//"sap(예방보전)"계정 ID
							repairRequestVO.setDIVISION("10");		//"sap(예방보전)"계정 부서 : "기타"
//							repairRequestVO.setCompanyId(getSsCompanyId());
//							repairRequestVO.setSsLocation(getSsLocation());
//							repairRequestVO.setUserId(getSsUserId());
//							repairRequestVO.setDIVISION(getSsDivision());
							
							//PM 오더 유형이 SAP_예방보전(계획보전)일경우
							if("DM01".equals(zpm_preorder_t_afko.getAUART())){ 
								repairRequestVO.setREQUEST_TYPE(CodeConstants.EPMS_REQUEST_TYPE_PREVENTIVE_SAP);	//요청유형 세팅
								repairRequestVO.setE_AUFNR(zpm_preorder_t_afko.getAUFNR());                         //PM 오더 번호
								repairRequestVO.setREPAIR_TYPE("150");												//정비유형
							}
							else{
								continue;
							}
							
							//처리파트
							//INGPR - D01 (기계파트), D02 (전기파트), D03 (시설파트)
							String part = zpm_preorder_t_afko.getINGPR().replaceAll("D", "").trim();
							
							// 파트 지정일 경우
							if(part.equals("01") || part.equals("02") || part.equals("03")){
								repairRequestVO.setPART(repairRequestVO.getCompanyId()+"_"+part);
						    // 파트가 지정되어 넘어 오지 않는 경우 : SAP에서 받은 대로 넣어줌
							} else {
								repairRequestVO.setPART(zpm_preorder_t_afko.getINGPR());
							}
													
							repairRequestVO.setMANAGER("");						                                //담당자
							repairRequestVO.setREQUEST_DESCRIPTION(zpm_preorder_t_afko.getLTXA1());				//정비요청 세팅(점검내용과 동일)
							repairRequestVO.setDATE_PLAN(zpm_preorder_t_afko.getGSTRP());						//계획일
							repairRequestVO.setDATE_ASSIGN(zpm_preorder_t_afko. getGSTRP());					//배정일
							
							// 기존에 등록된 오더인지 확인
							HashMap<String, Object> repair_result_aufnr = repairRequestService.selectRepairResultAufnr(repairRequestVO);
							// 기존에 등록된 오더가 아니고, TECO!="X" > 신규등록
							if(repair_result_aufnr == null){
								if(!"X".equals(zpm_preorder_t_afko.getTECO())){
									// 예방정비 요청
									repairRequestService.insertRepairRequest(repairRequestVO);
									// SAP에서 보내준 예방보전 건수 카운트
									sap_preventive_request_cnt++;
								}
							}
							// 기존에 등록된 오더 중 완료건이 아니고(E_RESULT!="S"), TECO=="X" 인 경우 > SAP에서 마감(TECO)친 경우, 오더 삭제
							else {
								if(!"S".equals(String.valueOf(repair_result_aufnr.get("E_RESULT")))
										&& "X".equals(zpm_preorder_t_afko.getTECO())){
									repairRequestVO.setREPAIR_REQUEST(String.valueOf(repair_result_aufnr.get("REPAIR_REQUEST")));
									repairRequestVO.setREPAIR_RESULT(String.valueOf(repair_result_aufnr.get("REPAIR_RESULT")));
									repairRequestService.updateRepairTeco(repairRequestVO);
								}
							}
							
						}
						
					}
				}
				
				if("S".equals(tables.getE_RESULT())){	// SAP과 정상적 통신을 했을 경우
					if(sap_preventive_request_cnt > 0){ // SAP과 통신후 일일 보전이 생성 되었을 경우
						E_RESULT = "S";
						E_MESSAGE = String.valueOf(sap_preventive_request_cnt) + "건의 '계획보전'이 생성되었습니다.";
					}else if( sap_preventive_request_cnt == 0){ // SAP과 정상적 통신은 했지만 생성된 일일 보전이 없을경우
						E_RESULT = "S";
						E_MESSAGE = "생성할 '계획보전'이 없습니다.";
					}
				}
				else{									// SAP과 정상적 통신을 못 했을경우
					E_RESULT = tables.getE_RESULT();
					E_MESSAGE = tables.getE_MESSAGE();
				}
				logger.debug("E_RESULT:" + E_RESULT);
				logger.debug("E_MESSAGE:" + E_MESSAGE);
					
			} catch(Exception e) {
				logger.error(e.getMessage());
			}
    		stopWatch.stop();
    		logger.debug("# 메소드종료[runtime: " + (stopWatch.getTotalTimeMillis()/1000) % 60 + "(s)]");
    		logger.debug("================== SAP 예방보전 오더 생성 end ====================");
    		
        }
	}
	
}
