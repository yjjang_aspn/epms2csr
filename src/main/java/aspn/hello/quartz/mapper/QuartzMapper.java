package aspn.hello.quartz.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.quartz.model.QuartzVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("quartzMapper")
public interface QuartzMapper {

	/**
	 * 예방보전 결과 업데이트 프로시저 호출
	 * 
	 * @author 이영탁
	 * @since 2018.04.27
	 * @param
	 * @return void
	 * @throws Exception
	 */
	public void  updatePreventiveResult(QuartzVO quartzVO) throws Exception;

	/**
	 * 정비원 업무현황 조회
	 * 
	 * @author 서정민
	 * @since 2018.07.09
	 * @param
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectWorkingStatusList() throws Exception;
}
