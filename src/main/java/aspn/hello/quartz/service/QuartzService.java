package aspn.hello.quartz.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.quartz.model.QuartzVO;

public interface QuartzService {
	

	/**
	 * 예방점검 결과 업데이트
	 * 예방점검타입이 예방점검(02)인경우 계획일이 지나면 미점검 체크후 다음 계획일 데이터 생성
	 * 
	 * @author 이영탁
	 * @since 2018.04.27
	 * @param
	 * @return void
	 * @throws Exception
	 */
	public boolean updatePreventiveResult(QuartzVO quartzVO)  throws Exception;
	
	/**
	 * 정비원 업무현황 조회
	 * 
	 * @author 서정민
	 * @since 2018.07.09
	 * @param 
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectWorkingStatusList() throws Exception;
}
