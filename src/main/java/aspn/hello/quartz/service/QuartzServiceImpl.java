package aspn.hello.quartz.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.hello.quartz.mapper.QuartzMapper;
import aspn.hello.quartz.model.QuartzVO;
import aspn.hello.quartz.service.QuartzService;

@Service
public class QuartzServiceImpl implements QuartzService{
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	
	@Autowired
	QuartzMapper quartzMapper;

	/**
	 * 예방점검 결과 업데이트
	 * 예방점검타입이 예방점검(02)인경우 계획일이 지나면 미점검 체크후 다음 계획일 데이터 생성
	 * 
	 * @author 이영탁
	 * @since 2018.04.27
	 * @param
	 * @return void
	 * @throws Exception
	 */
	public boolean updatePreventiveResult(QuartzVO quartzVO)  throws Exception{
		
		boolean result = false;
		
		//예방보전 결과 업데이트 프로시저 호출
		quartzMapper.updatePreventiveResult(quartzVO);
		
		if(quartzVO.getRetCode().equals("S")){
			result = true;
		}		
		
		return result;
		
	}
	
	/**
	 * 정비원 업무현황 조회
	 * 
	 * @author 서정민
	 * @since 2018.07.09
	 * @param 
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectWorkingStatusList() throws Exception {
		return quartzMapper.selectWorkingStatusList();
	}
	
}
