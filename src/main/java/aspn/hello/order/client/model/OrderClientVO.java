package aspn.hello.order.client.model;

import java.util.List;

import aspn.com.common.util.Utility;
import aspn.hello.com.model.AbstractVO;
import aspn.hello.com.model.AttachVO;

public class OrderClientVO extends AbstractVO {

	private static final long serialVersionUID = 1L;

	//공통
	private String GRID_ID                       = "";   //Grid ID
	private String COL_ID                        = "";   //COL ID
	private String CLIENT                        = "";   //고객코드(SEQ)
	private String NAME                          = "";   //고객·파트너명
	private String COOPERATION_ID                = "";   //사업자등록번호
	private String DATE_C                        = "";   //등록일
	private String DATE_E                        = "";   //수정일
	private String CD_DEL                        = "";   //삭제여부
	private String DIVISION			             = "";   //부서코드(SEQ)
	private String MEMBER 		                 = "";   //등록자ID/사번/로그인ID
	private String ATTACH_GRP_NO                 = "";   //첨부파일(회사로고 등) 
	private String REGISTER                      = "";   //등록자ID
	private String REGISTER_NAME                 = "";   //등록자이름
	private String CLIENT_UID					 = "";   //로그인ID

	//고객관리
	private String TYPE1                         = "";   //업종
	private String TYPE2                         = "";   //업태
	private String CEO                           = "";   //대표자명
	private String ADDRESS                       = "";   //주소
	private String HOMEPAGE                      = "";   //홈페이지 주소
	private String COMPANY_ID	                 = "";   //회사ID
	private String CLIENT_TYPE	                 = "";   //고객구분
	private String GRP1	                         = "";   //고객유형1
	private String GRP2	                         = "";   //고객유형2
	private String SUPERVISOR                    = "";   //내부담당자(대표영업사원)코드
	private String SUPERVISORNM                  = "";   //내부담당자(대표영업사원)명
	private String FI_CLIENT                     = "";   //세금계산서발행처코드
	private String FI_CLIENTNM                   = "";   //세금계산서발행처명
	private String CD_WAREHOUSE                  = "";   //출고/창고코드
	private String CD_WAREHOUSENM                = "";   //출고/창고명
	private String HO_CLIENT                     = "";   //위탁점코드
	private String HO_CLIENTNM                   = "";   //위탁점명
	private String CD_CREDIT                     = "";   //여신체크
	private String CONTRACT_BEGIN_DATE           = "";   //계약시작일자
	private String CONTRACT_END_DATE             = "";   //계약만료일자
	private String MEMO				             = "";   //메모
	private String PASSWD                        = "";   //비밀번호
	private String PRICE			             = "";   //여신금액

	//파트너관리
	private String PARTNER                       = "";   //파트너코드(SEQ)
	private String ADDR1                         = "";   //주소1
	private String PHONE                         = "";   //유선전화
	private String MOBILE                        = "";   //핸드폰번호
	private String EMAIL                         = "";   //이메일
	private String CEO_NAME                      = "";   //대표자명
	private String CD_PARTNER_TYPE               = "";   //파트너구분

	//납품처관리
	private String DELIVERY                      = "";   //납품처SEQ
	private String RCV_NAME                      = "";   //수령인
	private String RCV_PHONE                     = "";   //전화번호
	private String RCV_MOBILE                    = "";   //휴대폰번호
	private String ADDR2                         = "";   //주소2
	private String USE_YN                        = "";   //사용여부

	//클레임관리
	private String NO	                         = "";   //순번
	private String CLAIM                         = "";   //클레임SEQ
	private String CD_CLAIM_TYPE                 = "";   //클레임유형
	private String CONTENTS                      = "";   //내용
	private String ACCEPT_USERID                 = "";   //접수자ID 
	private String CD_CLAIM_STATUS               = "";   //처리상태
	private String ATTACH_GRP_IMG                = "";   //첨부파일(이미지)
	private String ATTACH_FLG		             = "";   //첨부파일여부
	private String ATTACH_IMG_FLG                = "";   //첨부파일여부(이미지)
	private String DEL_IMG_SEQ     	             = "";   //삭제 선택 첨부파일 번호(이미지)
	private String COMMENT_CNT		             = "";   //댓글수
	private String ACCEPT_USERNAME	             = "";   //접수자명
	private String SEQ							 = "";	 //답글순서

	//처리내역
	private String CLAIM_RESPONSE	             = "";   //처리내역SEQ
	private String DIVISION_NAME	             = "";   //부서명
	private List<AttachVO> responseAttachList = null; //처리내역(댓글) 첨부파일 리스트

	//클레임레포트
	private String REQUEST         	             = "";   //금일요청
	private String PROCESS      	             = "";   //금일처리
	private String ENDING       	             = "";   //당일접수처리
	private String ACCEPT    	                 = "";   //접수중
	private String PROGRESS	     	             = "";   //진행중
	private String APPROVE_STANDBY               = "";   //승인대기중
	
	//지도
	private String LNG							 = "";	 // 경도
	private String LAT							 = "";	 // 위도
	
	//공통          
	public String getGRID_ID() {
		return GRID_ID;
	}
	public void setGRID_ID(String gRID_ID) {
		GRID_ID = gRID_ID;
	}
	public String getCOL_ID() {
		return COL_ID;
	}
	public void setCOL_ID(String cOL_ID) {
		COL_ID = cOL_ID;
	}
	public String getCLIENT() {
		return CLIENT;
	}
	public void setCLIENT(String cLIENT) {
		CLIENT = cLIENT;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getCOOPERATION_ID() {
		return COOPERATION_ID;
	}
	public void setCOOPERATION_ID(String cOOPERATION_ID) {
		COOPERATION_ID = cOOPERATION_ID;
	}
	public String getDATE_C() {
		return DATE_C;
	}
	public void setDATE_C(String dATE_C) {
		DATE_C = dATE_C;
	}
	public String getDATE_E() {
		return DATE_E;
	}
	public void setDATE_E(String dATE_E) {
		DATE_E = dATE_E;
	}
	public String getCD_DEL() {
		return CD_DEL;
	}
	public void setCD_DEL(String cD_DEL) {
		CD_DEL = cD_DEL;
	}
	public String getDIVISION() {
		return DIVISION;
	}
	public void setDIVISION(String dIVISION) {
		DIVISION = dIVISION;
	}
	public String getMEMBER() {
		return MEMBER;
	}
	public void setMEMBER(String mEMBER) {
		MEMBER = mEMBER;
	}
	public String getCLIENT_UID() {
		return CLIENT_UID;
	}
	public void setCLIENT_UID(String cLIENT_UID) {
		CLIENT_UID = cLIENT_UID;
	}
	
	//고객관리
	public String getTYPE1() {
		return TYPE1;
	}
	public void setTYPE1(String tYPE1) {
		TYPE1 = tYPE1;
	}
	public String getTYPE2() {
		return TYPE2;
	}
	public void setTYPE2(String tYPE2) {
		TYPE2 = tYPE2;
	}
	public String getCEO() {
		return CEO;
	}
	public void setCEO(String cEO) {
		CEO = cEO;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public String getATTACH_GRP_NO() {
		return ATTACH_GRP_NO;
	}
	public void setATTACH_GRP_NO(String aTTACH_GRP_NO) {
		ATTACH_GRP_NO = aTTACH_GRP_NO;
	}
	public String getHOMEPAGE() {
		return HOMEPAGE;
	}
	public void setHOMEPAGE(String hOMEPAGE) {
		HOMEPAGE = hOMEPAGE;
	}
	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}
	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}
	public String getCLIENT_TYPE() {
		return CLIENT_TYPE;
	}
	public void setCLIENT_TYPE(String cLIENT_TYPE) {
		CLIENT_TYPE = cLIENT_TYPE;
	}
	public String getGRP1() {
		return GRP1;
	}
	public void setGRP1(String gRP1) {
		GRP1 = gRP1;
	}
	public String getGRP2() {
		return GRP2;
	}
	public void setGRP2(String gRP2) {
		GRP2 = gRP2;
	}
	public String getSUPERVISOR() {
		return SUPERVISOR;
	}
	public void setSUPERVISOR(String sUPERVISOR) {
		SUPERVISOR = sUPERVISOR;
	}
	public String getSUPERVISORNM() {
		return SUPERVISORNM;
	}
	public void setSUPERVISORNM(String sUPERVISORNM) {
		SUPERVISORNM = sUPERVISORNM;
	}
	public String getFI_CLIENT() {
		return FI_CLIENT;
	}
	public void setFI_CLIENT(String fI_CLIENT) {
		FI_CLIENT = fI_CLIENT;
	}
	public String getFI_CLIENTNM() {
		return FI_CLIENTNM;
	}
	public void setFI_CLIENTNM(String fI_CLIENTNM) {
		FI_CLIENTNM = fI_CLIENTNM;
	}
	public String getCD_WAREHOUSE() {
		return CD_WAREHOUSE;
	}
	public void setCD_WAREHOUSE(String cD_WAREHOUSE) {
		CD_WAREHOUSE = cD_WAREHOUSE;
	}
	public String getCD_WAREHOUSENM() {
		return CD_WAREHOUSENM;
	}
	public void setCD_WAREHOUSENM(String cD_WAREHOUSENM) {
		CD_WAREHOUSENM = cD_WAREHOUSENM;
	}
	public String getHO_CLIENT() {
		return HO_CLIENT;
	}
	public void setHO_CLIENT(String hO_CLIENT) {
		HO_CLIENT = hO_CLIENT;
	}
	public String getHO_CLIENTNM() {
		return HO_CLIENTNM;
	}
	public void setHO_CLIENTNM(String hO_CLIENTNM) {
		HO_CLIENTNM = hO_CLIENTNM;
	}
	public String getCD_CREDIT() {
		return CD_CREDIT;
	}
	public void setCD_CREDIT(String cD_CREDIT) {
		CD_CREDIT = cD_CREDIT;
	}
	public String getCONTRACT_BEGIN_DATE() {
		return CONTRACT_BEGIN_DATE;
	}
	public void setCONTRACT_BEGIN_DATE(String cONTRACT_BEGIN_DATE) {
		CONTRACT_BEGIN_DATE = cONTRACT_BEGIN_DATE;
	}
	public String getCONTRACT_END_DATE() {
		return CONTRACT_END_DATE;
	}
	public void setCONTRACT_END_DATE(String cONTRACT_END_DATE) {
		CONTRACT_END_DATE = cONTRACT_END_DATE;
	}
	public String getMEMO() {
		return MEMO;
	}
	public void setMEMO(String mEMO) {
		MEMO = mEMO;
	}
	public String getPASSWD() {
		return PASSWD;
	}
	public void setPASSWD(String pASSWD) {
		PASSWD = pASSWD;
	}
	public String getPRICE() {
		return PRICE;
	}
	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}

	//파트너관리
	public String getPARTNER() {
		return PARTNER;
	}
	public void setPARTNER(String pARTNER) {
		PARTNER = pARTNER;
	}
	public String getADDR1() {
		return ADDR1;
	}
	public void setADDR1(String aDDR1) {
		ADDR1 = aDDR1;
	}
	public String getPHONE() {
		return PHONE;
	}
	public void setPHONE(String pHONE) {
		PHONE = pHONE;
	}
	public String getMOBILE() {
		return MOBILE;
	}
	public void setMOBILE(String mOBILE) {
		MOBILE = mOBILE;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}
	public String getCEO_NAME() {
		return CEO_NAME;
	}
	public void setCEO_NAME(String cEO_NAME) {
		CEO_NAME = cEO_NAME;
	}
	public String getCD_PARTNER_TYPE() {
		return CD_PARTNER_TYPE;
	}
	public void setCD_PARTNER_TYPE(String cD_PARTNER_TYPE) {
		CD_PARTNER_TYPE = cD_PARTNER_TYPE;
	}

	//납품처관리
	public String getDELIVERY() {
		return DELIVERY;
	}
	public void setDELIVERY(String dELIVERY) {
		DELIVERY = dELIVERY;
	}
	public String getRCV_NAME() {
		return RCV_NAME;
	}
	public void setRCV_NAME(String rCV_NAME) {
		RCV_NAME = rCV_NAME;
	}
	public String getRCV_PHONE() {
		return RCV_PHONE;
	}
	public void setRCV_PHONE(String rCV_PHONE) {
		RCV_PHONE = rCV_PHONE;
	}
	public String getRCV_MOBILE() {
		return RCV_MOBILE;
	}
	public void setRCV_MOBILE(String rCV_MOBILE) {
		RCV_MOBILE = rCV_MOBILE;
	}
	public String getADDR2() {
		return ADDR2;
	}
	public void setADDR2(String aDDR2) {
		ADDR2 = aDDR2;
	}
	public String getUSE_YN() {
		return USE_YN;
	}
	public void setUSE_YN(String uSE_YN) {
		USE_YN = uSE_YN;
	}

	//클레임관리
	public String getNO() {
		return NO;
	}
	public void setNO(String nO) {
		NO = nO;
	}
	public String getCLAIM() {
		return CLAIM;
	}
	public void setCLAIM(String cLAIM) {
		CLAIM = cLAIM;
	}
	public String getCD_CLAIM_TYPE() {
		return CD_CLAIM_TYPE;
	}
	public void setCD_CLAIM_TYPE(String cD_CLAIM_TYPE) {
		CD_CLAIM_TYPE = cD_CLAIM_TYPE;
	}
	public String getCONTENTS() {
		return CONTENTS;
	}
	public void setCONTENTS(String cONTENTS) {
		CONTENTS = cONTENTS;
	}
	public String getREGISTER() {
		return REGISTER;
	}
	public void setREGISTER(String rEGISTER) {
		REGISTER = rEGISTER;
	}
	public String getREGISTER_NAME() {
		return REGISTER_NAME;
	}
	public void setREGISTER_NAME(String rEGISTER_NAME) {
		REGISTER_NAME = rEGISTER_NAME;
	}
	public String getACCEPT_USERID() {
		return ACCEPT_USERID;
	}
	public void setACCEPT_USERID(String aCCEPT_USERID) {
		ACCEPT_USERID = aCCEPT_USERID;
	}
	public String getCD_CLAIM_STATUS() {
		return CD_CLAIM_STATUS;
	}
	public void setCD_CLAIM_STATUS(String cD_CLAIM_STATUS) {
		CD_CLAIM_STATUS = cD_CLAIM_STATUS;
	}
	public String getATTACH_GRP_IMG() {
		return ATTACH_GRP_IMG;
	}
	public void setATTACH_GRP_IMG(String aTTACH_GRP_IMG) {
		ATTACH_GRP_IMG = aTTACH_GRP_IMG;
	}
	public String getATTACH_FLG() {
		return ATTACH_FLG;
	}
	public void setATTACH_FLG(String aTTACH_FLG) {
		ATTACH_FLG = aTTACH_FLG;
	}
	public String getATTACH_IMG_FLG() {
		return ATTACH_IMG_FLG;
	}
	public void setATTACH_IMG_FLG(String aTTACH_IMG_FLG) {
		ATTACH_IMG_FLG = aTTACH_IMG_FLG;
	}
	public String getDEL_IMG_SEQ() {
		return DEL_IMG_SEQ;
	}
	public void setDEL_IMG_SEQ(String dEL_IMG_SEQ) {
		DEL_IMG_SEQ = dEL_IMG_SEQ;
	}
	public String getCOMMENT_CNT() {
		return COMMENT_CNT;
	}
	public void setCOMMENT_CNT(String cOMMENT_CNT) {
		COMMENT_CNT = cOMMENT_CNT;
	}
	public String getACCEPT_USERNAME() {
		return ACCEPT_USERNAME;
	}
	public void setACCEPT_USERNAME(String aCCEPT_USERNAME) {
		ACCEPT_USERNAME = aCCEPT_USERNAME;
	}
	public String getSEQ() {
		return SEQ;
	}
	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}
	
	//처리내역
	public String getCLAIM_RESPONSE() {
		return CLAIM_RESPONSE;
	}
	public void setCLAIM_RESPONSE(String cLAIM_RESPONSE) {
		CLAIM_RESPONSE = cLAIM_RESPONSE;
	}
	public String getDIVISION_NAME() {
		return DIVISION_NAME;
	}
	public void setDIVISION_NAME(String dIVISION_NAME) {
		DIVISION_NAME = dIVISION_NAME;
	}
	
	//처리내역 첨부파일 리스트
	public List<AttachVO> getResponseAttachList() {
		return responseAttachList;
	}
	public void setResponseAttachList(List<AttachVO> responseAttachList) {
		this.responseAttachList = responseAttachList;
	}

	//클레임레포트
	public String getREQUEST() {
		return REQUEST;
	}
	public void setREQUEST(String rEQUEST) {
		REQUEST = rEQUEST;
	}
	public String getPROCESS() {
		return PROCESS;
	}
	public void setPROCESS(String pROCESS) {
		PROCESS = pROCESS;
	}
	public String getENDING() {
		return ENDING;
	}
	public void setENDING(String eNDING) {
		ENDING = eNDING;
	}
	public String getACCEPT() {
		return ACCEPT;
	}
	public void setACCEPT(String aCCEPT) {
		ACCEPT = aCCEPT;
	}
	public String getPROGRESS() {
		return PROGRESS;
	}
	public void setPROGRESS(String pROGRESS) {
		PROGRESS = pROGRESS;
	}
	public String getAPPROVE_STANDBY() {
		return APPROVE_STANDBY;
	}
	public void setAPPROVE_STANDBY(String aPPROVE_STANDBY) {
		APPROVE_STANDBY = aPPROVE_STANDBY;
	}
	public String getLNG() {
		return LNG;
	}
	public void setLNG(String lNG) {
		LNG = lNG;
	}
	public String getLAT() {
		return LAT;
	}
	public void setLAT(String lAT) {
		LAT = lAT;
	}
	//클레임 유효성(유형, 내용) 검사 메소드
	public boolean isRegValidation() {

		if(Utility.isEmpty(CD_CLAIM_TYPE)) {
			return false;
		}

		if(Utility.isEmpty(CONTENTS)) {
			return false;
		}

		return true;
	}
}
