package aspn.hello.epms.preventive.work.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.preventive.work.model.PreventiveWorkVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [오더마스터 관리] Mapper Class
 * 
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *   
 * </pre>
 */

@Mapper("preventiveWorkMapper")
public interface PreventiveWorkMapper {
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 오더마스터 목록 조회
	 * @author 서정민
	 * @since 2018.05.09
	 * @param preventiveWorkVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectPreventiveWorkList(PreventiveWorkVO preventiveWorkVO);
	
	/**
	 * 오더마스터 SEQ 조회 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param PreventiveWorkVO preventiveWorkVO
	 * @return int
	 * @throws Exception
	 */
	public String selectWorkSeq() throws Exception;
	
	/**
	 * 오더마스터 등록
	 * @author 서정민
	 * @since 2018.05.09
	 * @param PreventiveWorkVO preventiveWorkVO
	 * @return int
	 * @throws Exception
	 */
	public int insertPreventiveWork(PreventiveWorkVO preventiveWorkVO) throws Exception;
	
	/**
	 * 오더마스터 수정
	 * @author 서정민
	 * @since 2018.05.09
	 * @param PreventiveWorkVO preventiveWorkVO
	 * @return int
	 * @throws Exception
	 */
	public int updatePreventiveWork(PreventiveWorkVO preventiveWorkVO) throws Exception;
}
