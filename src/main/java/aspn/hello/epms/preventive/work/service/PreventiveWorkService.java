package aspn.hello.epms.preventive.work.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.preventive.work.model.PreventiveWorkVO;

/**
 * [오더마스터 관리] Service Class
 * 
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *   
 * </pre>
 */

public interface PreventiveWorkService {
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 오더마스터 목록 조회
	 * @author 서정민
	 * @since 2018.05.09
	 * @param PreventiveWorkVO preventiveWorkVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectPreventiveWorkList(PreventiveWorkVO preventiveWorkVO) throws Exception;
	
	/**
	 * 오더마스터 그리드 저장
	 * @author 서정민
	 * @since 2018.05.09
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param PreventiveWorkVO preventiveWorkVO
	 * @return void
	 * @throws Exception
	 */
	public void preventiveWorkListEdit(List<HashMap<String, Object>> saveDataList, PreventiveWorkVO preventiveWorkVO) throws Exception;
	
}
