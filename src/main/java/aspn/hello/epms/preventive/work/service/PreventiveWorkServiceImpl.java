package aspn.hello.epms.preventive.work.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.util.Utility;
import aspn.hello.epms.preventive.work.mapper.PreventiveWorkMapper;
import aspn.hello.epms.preventive.work.model.PreventiveWorkVO;

/**
 * [오더마스터] Business Implement Class
 * 
 * @author 이영탁
 * @since 2018.03.27
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.27		이영탁		최초 생성
 *   
 * </pre>
 */

@Service
public class PreventiveWorkServiceImpl implements PreventiveWorkService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	PreventiveWorkMapper preventiveWorkMapper;

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 오더마스터 목록 조회
	 * @author 서정민
	 * @since 2018.05.09
	 * @param PreventiveWorkVO preventiveWorkVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectPreventiveWorkList(PreventiveWorkVO preventiveWorkVO) throws Exception {
		return preventiveWorkMapper.selectPreventiveWorkList(preventiveWorkVO);
	}
	
	/**
	 * 오더마스터 그리드 저장
	 * @author 서정민
	 * @since 2018.05.09
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param PreventiveWorkVO preventiveWorkVO
	 * @return void
	 * @throws Exception
	 */
	public void preventiveWorkListEdit(List<HashMap<String, Object>> saveDataList, PreventiveWorkVO preventiveWorkVO) throws Exception{
		
		if(saveDataList != null){
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				PreventiveWorkVO preventiveWorkVO1 = Utility.toBean(tmpSaveData, preventiveWorkVO.getClass());
				preventiveWorkVO1.setREG_ID(preventiveWorkVO.getREG_ID());
				preventiveWorkVO1.setUPD_ID(preventiveWorkVO.getUPD_ID());
				
				if(tmpSaveData.containsKey("Added")) {
					String SEQ_WORK = preventiveWorkMapper.selectWorkSeq();
					preventiveWorkVO1.setWORK(SEQ_WORK);
					
					preventiveWorkMapper.insertPreventiveWork(preventiveWorkVO1);
					
				} else if (tmpSaveData.containsKey("Changed")) {
					preventiveWorkMapper.updatePreventiveWork(preventiveWorkVO1);
					
				} else if(tmpSaveData.containsKey("Deleted")) {
					
				}
			}
		}
	}

}
