package aspn.hello.epms.preventive.work.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.Utility;
import aspn.hello.com.service.AttachService;
import aspn.hello.epms.preventive.work.model.PreventiveWorkVO;
import aspn.hello.epms.preventive.work.service.PreventiveWorkService;

/**
 * [오더마스터 관리] Controller Class
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/preventive/work")
public class PreventiveWorkController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	PreventiveWorkService preventiveWorkService;

	@Autowired
	AttachService attachService;

	/**
	 * [오더마스터관리] : 화면호출
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventiveWorkMng.do")
	public String preventiveWorkMng(Model model) throws Exception{
		
		return "/epms/preventive/work/preventiveWorkMng";
	}
	
	/**
	 * [오더마스터관리] : [오더] 그리드 데이터
	 *
	 * @author 이영탁
	 * @since 2018.05.09
	 * @param PreventiveWorkVO preventiveWorkVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventiveWorkListData.do")
	public String preventiveWorkListData(PreventiveWorkVO preventiveWorkVO, Model model) throws Exception{
		
		try {
			//오더마스터 목록
			preventiveWorkVO.setSubAuth(getSsSubAuth());
			List<HashMap<String, Object>> list= preventiveWorkService.selectPreventiveWorkList(preventiveWorkVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		}
		return "/epms/preventive/work/preventiveWorkListData";
	}
	
	
	/**
	 * [오더마스터관리] : [오더] 그리드 레이아웃
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventiveWorkMngLayout.do")
	public String preventiveWorkMngLayout(Model model) throws Exception{

		return "/epms/preventive/work/preventiveWorkMngLayout";
	}
	
	/**
	 * [오더마스터관리] : [오더] 그리드 저장
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param PreventiveWorkVO preventiveWorkVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventiveWorkEdit.do")
	@ResponseBody
	public String equipmentListEdit(PreventiveWorkVO preventiveWorkVO, Model model) throws Exception {
		
		try{
			preventiveWorkVO.setUserId(this.getSsUserId());
			preventiveWorkService.preventiveWorkListEdit( Utility.getEditDataList(preventiveWorkVO.getUploadData()) , preventiveWorkVO);
			
			return getSaved();
			
		}catch(Exception e){
			
			e.printStackTrace();
			return getSaveFail();
		}
		
	}

}
