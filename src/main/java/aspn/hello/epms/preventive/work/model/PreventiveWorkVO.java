package aspn.hello.epms.preventive.work.model;

import aspn.hello.com.model.AbstractVO;

/**
 * [오더마스터 관리] VO Class
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *   
 * </pre>
 */

public class PreventiveWorkVO extends AbstractVO{
	
	private static final long serialVersionUID = 1L;
	
	/** EPMS_WORK DB **/
	private String WORK				= ""; //작업ID
	private String COMPANY_ID		= ""; //회사코드
	private String WORK_TYPE		= ""; //작업유형
	private String PREVENTIVE_TYPE	= ""; //업무구분(01:예방정비,02:예방점검)
	private String PART				= ""; //파트
	private String CHECK_TYPE		= ""; //점검구분(01:CBM,02:TBM)
	private String CHECK_DETAIL		= ""; //점검항목
	private String CHECK_STANDARD	= ""; //판단기준
	private String CHECK_TOOL		= ""; //진단장비
	private String CHECK_CYCLE		= ""; //주기
	private String MANAGER			= ""; //담당자
	private String REG_ID			= ""; //등록자
	private String REG_DT			= ""; //등록일
	private String UPD_ID			= ""; //수정자
	private String UPD_DT			= ""; //수정일
	private String DEL_YN			= ""; //삭제여부
	
	/** CATEGORY DB  */
	private String CATEGORY   		= ""; //카테고리 코드
	private String NAME   			= ""; //카테고리 이름
	private String CATEGORY_TYPE	= ""; //카테고리 구분
	private String PARENT_CATEGORY	= ""; //부모카테고리
	private String TREE   			= ""; //구조체
	private String DEPTH	  		= ""; //레벨
	private String SEQ_DSP			= ""; //출력순서
	private String SUB_ROOT			= ""; //공장코드(설비관리)
	
	
	public String getWORK() {
		return WORK;
	}
	public void setWORK(String wORK) {
		WORK = wORK;
	}
	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}
	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}
	public String getWORK_TYPE() {
		return WORK_TYPE;
	}
	public void setWORK_TYPE(String wORK_TYPE) {
		WORK_TYPE = wORK_TYPE;
	}
	public String getPREVENTIVE_TYPE() {
		return PREVENTIVE_TYPE;
	}
	public void setPREVENTIVE_TYPE(String pREVENTIVE_TYPE) {
		PREVENTIVE_TYPE = pREVENTIVE_TYPE;
	}
	public String getPART() {
		return PART;
	}
	public void setPART(String pART) {
		PART = pART;
	}
	public String getCHECK_TYPE() {
		return CHECK_TYPE;
	}
	public void setCHECK_TYPE(String cHECK_TYPE) {
		CHECK_TYPE = cHECK_TYPE;
	}
	public String getCHECK_DETAIL() {
		return CHECK_DETAIL;
	}
	public void setCHECK_DETAIL(String cHECK_DETAIL) {
		CHECK_DETAIL = cHECK_DETAIL;
	}
	public String getCHECK_STANDARD() {
		return CHECK_STANDARD;
	}
	public void setCHECK_STANDARD(String cHECK_STANDARD) {
		CHECK_STANDARD = cHECK_STANDARD;
	}
	public String getCHECK_TOOL() {
		return CHECK_TOOL;
	}
	public void setCHECK_TOOL(String cHECK_TOOL) {
		CHECK_TOOL = cHECK_TOOL;
	}
	public String getCHECK_CYCLE() {
		return CHECK_CYCLE;
	}
	public void setCHECK_CYCLE(String cHECK_CYCLE) {
		CHECK_CYCLE = cHECK_CYCLE;
	}
	public String getMANAGER() {
		return MANAGER;
	}
	public void setMANAGER(String mANAGER) {
		MANAGER = mANAGER;
	}
	public String getREG_ID() {
		return REG_ID;
	}
	public void setREG_ID(String rEG_ID) {
		REG_ID = rEG_ID;
	}
	public String getREG_DT() {
		return REG_DT;
	}
	public void setREG_DT(String rEG_DT) {
		REG_DT = rEG_DT;
	}
	public String getUPD_ID() {
		return UPD_ID;
	}
	public void setUPD_ID(String uPD_ID) {
		UPD_ID = uPD_ID;
	}
	public String getUPD_DT() {
		return UPD_DT;
	}
	public void setUPD_DT(String uPD_DT) {
		UPD_DT = uPD_DT;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getCATEGORY() {
		return CATEGORY;
	}
	public void setCATEGORY(String cATEGORY) {
		CATEGORY = cATEGORY;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getCATEGORY_TYPE() {
		return CATEGORY_TYPE;
	}
	public void setCATEGORY_TYPE(String cATEGORY_TYPE) {
		CATEGORY_TYPE = cATEGORY_TYPE;
	}
	public String getPARENT_CATEGORY() {
		return PARENT_CATEGORY;
	}
	public void setPARENT_CATEGORY(String pARENT_CATEGORY) {
		PARENT_CATEGORY = pARENT_CATEGORY;
	}
	public String getTREE() {
		return TREE;
	}
	public void setTREE(String tREE) {
		TREE = tREE;
	}
	public String getDEPTH() {
		return DEPTH;
	}
	public void setDEPTH(String dEPTH) {
		DEPTH = dEPTH;
	}
	public String getSEQ_DSP() {
		return SEQ_DSP;
	}
	public void setSEQ_DSP(String sEQ_DSP) {
		SEQ_DSP = sEQ_DSP;
	}
	public String getSUB_ROOT() {
		return SUB_ROOT;
	}
	public void setSUB_ROOT(String sUB_ROOT) {
		SUB_ROOT = sUB_ROOT;
	}

	
}
