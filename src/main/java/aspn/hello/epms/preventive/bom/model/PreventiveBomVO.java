package aspn.hello.epms.preventive.bom.model;

import aspn.hello.com.model.AbstractVO;

/**
 * [예방보전BOM] VO Class
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *   
 * </pre>
 */

public class PreventiveBomVO extends AbstractVO{
	
	private static final long serialVersionUID = 1L;
	
	/** EPMS_PREVENTIVE_BOM DB **/
	private String PREVENTIVE_BOM	= ""; // 예방보전ID
	private String WORK				= ""; // 작업목록ID
	private String EQUIPMENT		= ""; // 설비ID
	private String REG_ID			= ""; // 등록자
	private String REG_DT			= ""; // 등록일
	private String UPD_ID			= ""; // 수정자
	private String UPD_DT			= ""; // 수정일
	private String DEL_YN			= ""; // 삭제여부
	
	/** EPMS_PREVENTIVE_BOM DB **/
	private String PREVENTIVE_RESULT= ""; // 예방보전ID
	private String DATE_PLAN		= ""; // 계획일
	private String DATE_ASSIGN		= ""; // 배정일
	private String DATE_CHECK		= ""; // 점검일
	private String STATUS			= ""; // 상태
	private String MANAGER			= ""; // 담당자
	private String CHECK_DECISION	= ""; // 판정
	private String CHECK_VALUE		= ""; // 측정치
	private String CHECK_DESCRIPTION= ""; // 점검내용
	private String MATERIAL_INOUT	= ""; // 자재입출고ID
	private String ATTACH_GRP_NO	= ""; // 파일그룹번호
	private String REPAIR_REQUEST	= ""; // 정비요청ID
	private String SUCCESS_YN		= ""; // 주기전 고장여부
	
	/** EPMS_WORK DB **/
	private String WORK_TYPE 		= ""; // 작업유형
	private String CHECK_CYCLE		= ""; // 주기
	
	/** CATEGORY DB **/
	private String TREE				= ""; // 구조체
	
	/** parameter */
	private String works			= ""; // 선택된 오더들
	private String authType			= ""; //조회권한

	public String getPREVENTIVE_BOM() {
		return PREVENTIVE_BOM;
	}

	public void setPREVENTIVE_BOM(String pREVENTIVE_BOM) {
		PREVENTIVE_BOM = pREVENTIVE_BOM;
	}

	public String getWORK() {
		return WORK;
	}

	public void setWORK(String wORK) {
		WORK = wORK;
	}

	public String getEQUIPMENT() {
		return EQUIPMENT;
	}

	public void setEQUIPMENT(String eQUIPMENT) {
		EQUIPMENT = eQUIPMENT;
	}

	public String getREG_ID() {
		return REG_ID;
	}

	public void setREG_ID(String rEG_ID) {
		REG_ID = rEG_ID;
	}

	public String getREG_DT() {
		return REG_DT;
	}

	public void setREG_DT(String rEG_DT) {
		REG_DT = rEG_DT;
	}

	public String getUPD_ID() {
		return UPD_ID;
	}

	public void setUPD_ID(String uPD_ID) {
		UPD_ID = uPD_ID;
	}

	public String getUPD_DT() {
		return UPD_DT;
	}

	public void setUPD_DT(String uPD_DT) {
		UPD_DT = uPD_DT;
	}

	public String getDEL_YN() {
		return DEL_YN;
	}

	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}

	public String getPREVENTIVE_RESULT() {
		return PREVENTIVE_RESULT;
	}

	public void setPREVENTIVE_RESULT(String pREVENTIVE_RESULT) {
		PREVENTIVE_RESULT = pREVENTIVE_RESULT;
	}

	public String getDATE_PLAN() {
		return DATE_PLAN;
	}

	public void setDATE_PLAN(String dATE_PLAN) {
		DATE_PLAN = dATE_PLAN;
	}

	public String getDATE_ASSIGN() {
		return DATE_ASSIGN;
	}

	public void setDATE_ASSIGN(String dATE_ASSIGN) {
		DATE_ASSIGN = dATE_ASSIGN;
	}

	public String getDATE_CHECK() {
		return DATE_CHECK;
	}

	public void setDATE_CHECK(String dATE_CHECK) {
		DATE_CHECK = dATE_CHECK;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getMANAGER() {
		return MANAGER;
	}

	public void setMANAGER(String mANAGER) {
		MANAGER = mANAGER;
	}

	public String getCHECK_DECISION() {
		return CHECK_DECISION;
	}

	public void setCHECK_DECISION(String cHECK_DECISION) {
		CHECK_DECISION = cHECK_DECISION;
	}

	public String getCHECK_VALUE() {
		return CHECK_VALUE;
	}

	public void setCHECK_VALUE(String cHECK_VALUE) {
		CHECK_VALUE = cHECK_VALUE;
	}

	public String getCHECK_DESCRIPTION() {
		return CHECK_DESCRIPTION;
	}

	public void setCHECK_DESCRIPTION(String cHECK_DESCRIPTION) {
		CHECK_DESCRIPTION = cHECK_DESCRIPTION;
	}

	public String getMATERIAL_INOUT() {
		return MATERIAL_INOUT;
	}

	public void setMATERIAL_INOUT(String mATERIAL_INOUT) {
		MATERIAL_INOUT = mATERIAL_INOUT;
	}

	public String getATTACH_GRP_NO() {
		return ATTACH_GRP_NO;
	}

	public void setATTACH_GRP_NO(String aTTACH_GRP_NO) {
		ATTACH_GRP_NO = aTTACH_GRP_NO;
	}

	public String getREPAIR_REQUEST() {
		return REPAIR_REQUEST;
	}

	public void setREPAIR_REQUEST(String rEPAIR_REQUEST) {
		REPAIR_REQUEST = rEPAIR_REQUEST;
	}

	public String getSUCCESS_YN() {
		return SUCCESS_YN;
	}

	public void setSUCCESS_YN(String sUCCESS_YN) {
		SUCCESS_YN = sUCCESS_YN;
	}

	public String getWORK_TYPE() {
		return WORK_TYPE;
	}

	public void setWORK_TYPE(String wORK_TYPE) {
		WORK_TYPE = wORK_TYPE;
	}

	public String getCHECK_CYCLE() {
		return CHECK_CYCLE;
	}

	public void setCHECK_CYCLE(String cHECK_CYCLE) {
		CHECK_CYCLE = cHECK_CYCLE;
	}

	public String getTREE() {
		return TREE;
	}

	public void setTREE(String tREE) {
		TREE = tREE;
	}

	public String getWorks() {
		return works;
	}

	public void setWorks(String works) {
		this.works = works;
	}

	public String getAuthType() {
		return authType;
	}

	public void setAuthType(String authType) {
		this.authType = authType;
	}
}
