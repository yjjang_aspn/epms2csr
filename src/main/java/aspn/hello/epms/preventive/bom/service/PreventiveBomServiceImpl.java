package aspn.hello.epms.preventive.bom.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.util.Utility;
import aspn.hello.epms.preventive.bom.mapper.PreventiveBomMapper;
import aspn.hello.epms.preventive.bom.model.PreventiveBomVO;

/**
 * [예방보전BOM] Business Implement Class
 * 
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *   
 * </pre>
 */

@Service
public class PreventiveBomServiceImpl implements PreventiveBomService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	PreventiveBomMapper preventiveBomMapper;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 예방보전BOM 목록 조회
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param PreventiveBomVO preventiveBomVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectPreventiveBomList(PreventiveBomVO preventiveBomVO) throws Exception {
				
		return preventiveBomMapper.selectPreventiveBomList(preventiveBomVO);
	}
	/**
	 * 예방보전BOM 그리드 저장
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param PreventiveBomVO preventiveBomVO
	 * @return void
	 * @throws Exception
	 */
	@Override
	public void preventiveBomEdit(List<HashMap<String, Object>> saveDataList, PreventiveBomVO preventiveBomVO) throws Exception{
				
		if(saveDataList != null){
			
			String works = preventiveBomVO.getWorks();
			String[] works_arr;
			works_arr = works.split(",");
			
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				PreventiveBomVO preventiveBomVO1 = Utility.toBean(tmpSaveData, preventiveBomVO.getClass());
				
				preventiveBomVO1.setUserId(preventiveBomVO.getUserId());
				
				// BOM매핑 팝업에서 BOM 등록
				if(!works.equals("")){ 
				
					if(tmpSaveData.containsKey("Changed")) {
						
						for(int i=0; i<works_arr.length; i++){
							// 작업ID 세팅
							preventiveBomVO1.setWORK(works_arr[i]);
							
							// 예방보전BOM 등록 여부 확인
							if(preventiveBomMapper.existPreventiveBom(preventiveBomVO1) == 0){	
								
								// 예방보전BOM 시퀀스 세팅
								String SEQ_PREVENTIVE_BOM = preventiveBomMapper.selectPreventiveBomSeq();
								preventiveBomVO1.setPREVENTIVE_BOM(SEQ_PREVENTIVE_BOM);
								
								// 예방보전BOM 등록
								preventiveBomMapper.insertPreventiveBom(preventiveBomVO1);
								// 예방보전실적 등록
								preventiveBomMapper.insertPreventiveResult(preventiveBomVO1);
								
							}
						}
						
					} 
				} 
				// 예방보전 BOM Grid에서 등록
				else
				{
					if(tmpSaveData.containsKey("Changed")) {
					
						// 담당자가 '(공통)'일 경우
						if("(Common)".equals(preventiveBomVO1.getMANAGER())){
							preventiveBomVO1.setMANAGER("");
						}
						
						// 비활성일 경우
						if("Y".equals(preventiveBomVO1.getDEL_YN())){
							preventiveBomVO1.setMANAGER("");
							preventiveBomVO1.setDATE_PLAN("");
						}
						
						// 예방보전BOM 수정(주기,담당자,활성여부)
						preventiveBomMapper.updatePreventiveBom(preventiveBomVO1);
						
						// 예방보전실적 수정(담당자 및 차기계획일)
						preventiveBomMapper.updatePreventiveResult(preventiveBomVO1);
						
						
					} else if (tmpSaveData.containsKey("Added")) {
						
					} else if(tmpSaveData.containsKey("Deleted")) {
						
					}
					
				}
			}
		}
	}

	/**
	 * 정비파트원 목록-전체
	 * 
	 * @author 서정민
	 * @since 2018.08.20
	 * @param PreventiveBomVO preventiveBomVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairMemberList(PreventiveBomVO preventiveBomVO){
		return preventiveBomMapper.selectRepairMemberList(preventiveBomVO);
	}
	
	/**
	 * 정비파트원 목록-전체:RELATED
	 * 
	 * @author 서정민
	 * @since 2018.08.20
	 * @param PreventiveBomVO preventiveBomVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairMemberList2(PreventiveBomVO preventiveBomVO){
		return preventiveBomMapper.selectRepairMemberList2(preventiveBomVO);
	}
	
}
