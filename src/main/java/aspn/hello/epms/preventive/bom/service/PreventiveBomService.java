package aspn.hello.epms.preventive.bom.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.preventive.bom.model.PreventiveBomVO;

/**
 * [예방보전BOM] Service Class
 * 
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09 	서정민			최초 생성
 *   
 * </pre>
 */

public interface PreventiveBomService {
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 예방보전BOM 목록 조회
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param PreventiveBomVO preventiveBomVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectPreventiveBomList(PreventiveBomVO preventiveBomVO) throws Exception;
	
	/**
	 * 예방보전BOM 그리드 저장
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param PreventiveBomVO preventiveBomVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public void preventiveBomEdit(List<HashMap<String, Object>> saveDataList, PreventiveBomVO preventiveBomVO) throws Exception;

	/**
	 * 정비파트원 목록-전체
	 * 
	 * @author 서정민
	 * @since 2018.08.20
	 * @param PreventiveBomVO preventiveBomVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairMemberList(PreventiveBomVO preventiveBomVO);

	/**
	 * 정비파트원 목록-전체:RELATED
	 * 
	 * @author 서정민
	 * @since 2018.08.20
	 * @param PreventiveBomVO preventiveBomVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairMemberList2(PreventiveBomVO preventiveBomVO);
	
}
