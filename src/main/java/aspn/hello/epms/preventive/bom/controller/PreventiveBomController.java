package aspn.hello.epms.preventive.bom.controller;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.ObjUtil;
import aspn.com.common.util.Utility;
import aspn.hello.epms.preventive.bom.model.PreventiveBomVO;
import aspn.hello.epms.preventive.bom.service.PreventiveBomService;
import aspn.hello.epms.repair.plan.model.RepairPlanVO;
import aspn.hello.epms.repair.plan.service.RepairPlanService;

/**
 * [예방보전BOM] Controller Class
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/preventive/bom")
public class PreventiveBomController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	PreventiveBomService preventiveBomService;
	
	@Autowired
	RepairPlanService repairPlanService;
	
	@Value("#{config['equipment_hierarchy']}")
	private String equipment_hierarchy;


	/**
	 * [예방보전BOM관리] : 화면호출
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventiveBomMng.do")
	public String preventiveBomMng() throws Exception{
		
		return "/epms/preventive/bom/preventiveBomMng";
	}
	
	/**
	 * [예방보전BOM관리] : [오더] 그리드 레이아웃
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventiveWorkListLayout.do")
	public String preventiveWorkListLayout() throws Exception{
				
		return "/epms/preventive/bom/preventiveWorkListLayout";
	}
	
	/**
	 * [예방보전BOM관리]: [예방보전BOM] 그리드 데이터
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param PreventiveBomVO preventiveBomVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventiveBomListData.do")
	public String preventiveBomListData(PreventiveBomVO preventiveBomVO, Model model) throws Exception{
		
		try {
			
			//예방보전BOM 목록
			List<HashMap<String, Object>> list= preventiveBomService.selectPreventiveBomList(preventiveBomVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return "/epms/preventive/bom/preventiveBomListData";
	}
	
	/**
	 * [예방보전BOM조회] : [예방보전BOM] 그리드 레이아웃
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventiveBomListLayout.do")
	public String preventiveBomListLayout(PreventiveBomVO preventiveBomVO, RepairPlanVO repairPlanVO, Model model) throws Exception{
		
		try {
			
			preventiveBomVO.setSubAuth(getSsSubAuth());	 	//세션_조회권한
			preventiveBomVO.setCompanyId(getSsCompanyId());

			//정비파트원 목록
			List<HashMap<String, Object>> list3 = preventiveBomService.selectRepairMemberList(preventiveBomVO);
			HashMap<String, Object> repairMemberListOpt  = ObjUtil.list2Map(list3, "|");
			model.addAttribute("repairMemberListOpt", repairMemberListOpt);
			
			//정비파트원 목록(공장별,파트별):RELATED
			List<HashMap<String, Object>> list4 = preventiveBomService.selectRepairMemberList2(preventiveBomVO);
			String relatedCombo = ObjUtil.list2MapRelated(list4, "|", "CODE", "NAME");
			model.addAttribute("relatedCombo", relatedCombo);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		return "/epms/preventive/bom/preventiveBomListLayout";
	}
	
	
	/**
	 * [예방보전BOM관리] : 예방보전BOM Grid CRUD
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param PreventiveBomVO preventiveBomVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventiveBomEdit.do")
	@ResponseBody
	public String preventiveBomEdit(PreventiveBomVO preventiveBomVO, Model model) throws Exception {
		
		try{
			preventiveBomVO.setUserId(this.getSsUserId());
			
			preventiveBomService.preventiveBomEdit( Utility.getEditDataList(preventiveBomVO.getUploadData()) , preventiveBomVO);
			return getSaved();
			
		}catch(Exception e){
			logger.error("PreventiveBomController.preventiveBomListEdit Error !");
			e.printStackTrace();

			return getSaveFail();
			
		}
	}
	
	/**
	 * [예방보전BOM관리] : 팝업 설비목록 그리드 레이아웃
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popEquipmentListLayout.do")
	public String popEquipmentListLayout(Model model) throws Exception{
		
		model.addAttribute("equipment_hierarchy", equipment_hierarchy);
		
		return "/epms/preventive/bom/popEquipmentListLayout";
	}

}
