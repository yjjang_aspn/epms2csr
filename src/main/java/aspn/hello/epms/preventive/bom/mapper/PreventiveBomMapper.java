package aspn.hello.epms.preventive.bom.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.preventive.bom.model.PreventiveBomVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [예방보전BOM] Mapper Class
 * 
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *   
 * </pre>
 */

@Mapper("preventiveBomMapper")
public interface PreventiveBomMapper {
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 예방보전BOM 목록 조회
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param PreventiveBomVO preventiveBomVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectPreventiveBomList(PreventiveBomVO preventiveBomVO);
	
	/**
	 * 예방보전BOM 등록 여부 확인
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param PreventiveBomVO preventiveBomVO
	 * @return int
	 * @throws Exception
	 */
	public int existPreventiveBom(PreventiveBomVO preventiveBomVO) throws Exception;
	
	/**
	 * 예방보전BOM SEQ 조회
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	public String selectPreventiveBomSeq() throws Exception;
	
	/**
	 * 예방보전BOM 등록
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param PreventiveBomVO preventiveBomVO
	 * @return int
	 * @throws Exception
	 */
	public int insertPreventiveBom(PreventiveBomVO preventiveBomVO) throws Exception;
	
	/**
	 * 예방보전실적 등록
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param PreventiveBomVO preventiveBomVO
	 * @return int
	 * @throws Exception
	 */
	public int insertPreventiveResult(PreventiveBomVO preventiveBomVO) throws Exception;
	
	/**
	 * 예방보전BOM 수정 (주기,담당자,활성여부)
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param PreventiveBomVO preventiveBomVO
	 * @return int
	 * @throws Exception
	 */
	public int updatePreventiveBom(PreventiveBomVO preventiveBomVO) throws Exception;
	
	/**
	 * 예방보전실적 수정 (담당자 및 차기계획일)
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param PreventiveBomVO preventiveBomVO
	 * @return int
	 * @throws Exception
	 */
	public int updatePreventiveResult(PreventiveBomVO preventiveBomVO) throws Exception;
	
	/**
	 * 정비파트원 목록-전체
	 * 
	 * @author 서정민
	 * @since 2018.08.20
	 * @param PreventiveBomVO preventiveBomVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairMemberList(PreventiveBomVO preventiveBomVO);
	
	/**
	 * 정비파트원 목록-전체:RELATED
	 * 
	 * @author 서정민
	 * @since 2018.08.20
	 * @param PreventiveBomVO preventiveBomVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairMemberList2(PreventiveBomVO preventiveBomVO);
	
}
