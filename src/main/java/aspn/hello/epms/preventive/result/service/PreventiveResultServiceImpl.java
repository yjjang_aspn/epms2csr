package aspn.hello.epms.preventive.result.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.DateTime;
import aspn.hello.epms.equipment.history.mapper.EquipmentHistoryMapper;
import aspn.hello.epms.equipment.history.model.EquipmentHistoryVO;
import aspn.hello.epms.material.master.mapper.MaterialMasterMapper;
import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.epms.preventive.bom.mapper.PreventiveBomMapper;
import aspn.hello.epms.preventive.result.mapper.PreventiveResultMapper;
import aspn.hello.epms.preventive.result.model.PreventiveResultVO;
import aspn.hello.epms.repair.result.mapper.RepairResultMapper;
import aspn.hello.epms.repair.result.model.RepairResultVO;
import net.sf.json.JSONArray;

/**
 * [예방보전실적] Business Implement Class
 * 
 * @author 김영환
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		김영환			최초 생성
 *   2018.06.11     김영환                  187-220Line 수정(DATE_ALERT 기능추가)
 * 
 *      </pre>
 */

@Service
public class PreventiveResultServiceImpl implements PreventiveResultService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	PreventiveResultMapper preventiveResultMapper;

	@Autowired
	RepairResultMapper repairResultMapper;

	@Autowired
	EquipmentHistoryMapper equipmentHistoryMapper;

	@Autowired
	PreventiveBomMapper preventiveBomMapper;
	
	@Autowired
	MaterialMasterMapper materialMasterMapper;
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 예방보전실적 - 목록 조회 
	 *
	 * @author 김영환
	 * @since 2018.05.09
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> preventiveResultList(PreventiveResultVO preventiveResultVO) throws Exception {
		return preventiveResultMapper.preventiveResultList(preventiveResultVO);
	}

	/**
	 * 예방보전실적 - 상세 
	 *
	 * @author 김영환
	 * @since 2018.05.09
	 * @param PreventivePlanVO preventivePlanVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> selectPreventiveResultDtl(PreventiveResultVO preventiveResultVO) throws Exception {
		return preventiveResultMapper.selectPreventiveResultDtl(preventiveResultVO);
	}

	/**
	 * 예방보전실적 - 사용자재 목록
	 *
	 * @author 김영환
	 * @since 2018.05.09
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectPreventiveUsedMaterialList(PreventiveResultVO preventiveResultVO)
			throws Exception {
		return preventiveResultMapper.selectPreventiveUsedMaterialList(preventiveResultVO);
	}

	/**
	 * 예방보전실적 - 공통코드 판정 목록
	 *
	 * @author 김영환
	 * @since 2018.05.09
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectCheckDecisionList() throws Exception {
		return preventiveResultMapper.selectCheckDecisionList();
	}
	

	/**
	 * 예방보전실적 - 수정
	 *
	 * @author 김영환
	 * @since 2018.05.09
	 * @param PreventivePlanVO preventivePlanVO
	 * @return int
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> updatePreventiveResult(PreventiveResultVO preventiveResultVO) throws Exception {
		int rtnVal = 0;
		int cnt = preventiveResultMapper.selectPreventiveResultCheck(preventiveResultVO);
		int SEQ_MATERIAL_INOUT = 0;
		preventiveResultVO.setDATE_CHECK(DateTime.getDateString());
		
		if (cnt == 1) {

			// 1-1. 자재 내역 등록
			if (!("".equals(preventiveResultVO.getMaterialArr()) || preventiveResultVO.getMaterialArr() == null)) {

				List<HashMap<String, Object>> materialArr = JSONArray.fromObject(convertStandardJSONString(preventiveResultVO.getMaterialArr()));

				// 입출고 헤더 등록부터
				RepairResultVO repairResultVO = new RepairResultVO();
				repairResultVO.setUserId(preventiveResultVO.getUserId());

				// 입출고 SEQ 체크(SEQ_MATERIAL_INOUT 체크)

				SEQ_MATERIAL_INOUT = repairResultMapper.selectMaterialInoutSeq(repairResultVO);
				repairResultVO.setMATERIAL_INOUT(String.valueOf(SEQ_MATERIAL_INOUT));

				// 입출고 날짜 담기
				repairResultVO.setDATE_INOUT(preventiveResultVO.getDATE_CHECK());

				// 입출고유형 (02:출고)
				repairResultVO.setMATERIAL_INOUT_TYPE(CodeConstants.EPMS_MATERIAL_INOUT_TYPE_OUT);
				
				// 입출고 등록(헤더) MATERIAL_INOUT INSERT
				repairResultMapper.insertMaterialInout(repairResultVO);
				
				// 사용자재 등록
				for (Map<String, Object> item : materialArr) {
					
					RepairResultVO repairResultVO1 = new RepairResultVO();
					
					// 로그인 계정 정보 담기
					repairResultVO1.setUserId(preventiveResultVO.getUserId());
					// 입출고 헤더 PK (MATERIAL_INOUT) 담기
					repairResultVO1.setMATERIAL_INOUT(String.valueOf(SEQ_MATERIAL_INOUT));
					// 입출고 날짜 담기
					repairResultVO1.setDATE_INOUT(preventiveResultVO.getDATE_CHECK());
					
					//실적유형, ID 등록
					repairResultVO1.setTARGET_TYPE("PREVENTIVE");
					repairResultVO1.setTARGET_ID(preventiveResultVO.getPREVENTIVE_RESULT());

					// BOM코드 담기
					repairResultVO1.setEQUIPMENT_BOM(String.valueOf(item.get("EQUIPMENT_BOM")));
					// 자재코드 담기
					repairResultVO1.setMATERIAL(String.valueOf(item.get("MATERIAL")));
					// 수량 담기
					repairResultVO1.setQNTY(String.valueOf(item.get("QNTY")));
					
					//반영 전 현재 안전재고/재고 수량 조회
					MaterialMasterVO materialMasterVO = new MaterialMasterVO();
					materialMasterVO.setMATERIAL(repairResultVO1.getMATERIAL());
					
					HashMap<String, Object> before_stockMap = materialMasterMapper.selectMaterialMasterStock(materialMasterVO);
					int before_stock = Integer.parseInt(String.valueOf(before_stockMap.get("STOCK")));
					int before_stock_optimal = Integer.parseInt(String.valueOf(before_stockMap.get("STOCK_OPTIMAL")));
					
					String STOCK_FLAG = "N";	// 기존 재고보다 안전재고 미만일 경우 (STOCK_FLAG:'N')
					if(before_stock >= before_stock_optimal){
						STOCK_FLAG = "Y";		// 기존 재고가 안전재고 이상일 경우 (STOCK_FLAG:'Y')
					}

					// 수량이 0보다 크다면 입출고 내역 등록 (20170126 수정 : 수량이 0도 등록)
					if (Integer.parseInt(repairResultVO1.getQNTY()) >= 0 || "".equals(repairResultVO1.getQNTY())) {

						// 출고 등록(아이템) MATERIAL_INOUT_ITEM INSERT
						rtnVal = repairResultMapper.insertMaterialInoutItem(repairResultVO1);

						if (rtnVal > 0) {
							rtnVal = 0;

							// 자재 재고수량 반영(출고) MATERIAL UPDATE
							// 던킨의 경우 자재 동기화시 재고 반영
//							rtnVal = repairResultMapper.updateMaterialStockOut(repairResultVO1);
						}
					}
					
					// 기존 재고가 안전재고 이상이었는데, 반영 후 안전재고 미만이 될 경우 DATE_ALERT 등록
					HashMap<String, Object> after_stockMap = materialMasterMapper.selectMaterialMasterStock(materialMasterVO);
					int after_stock = Integer.parseInt(String.valueOf(after_stockMap.get("STOCK")));
					int after_stock_optimal = Integer.parseInt(String.valueOf(after_stockMap.get("STOCK_OPTIMAL")));
					
					if(STOCK_FLAG == "Y" && after_stock_optimal > after_stock){
						materialMasterMapper.updateMaterialMasterDateAlert(materialMasterVO);
					}
				}
			}

			// 2-1. 실적등록
			// 2-1-1. 예방보전실적 업데이트
			// STATUS 세팅 : 판정이 X(03)일 경우 02(정비필요), 그외 03(정비완료)
			preventiveResultVO.setSTATUS(preventiveResultVO.getCHECK_DECISION().equals(CodeConstants.EPMS_CHECK_DICISION_FAIL) ? CodeConstants.EPMS_PREVENTIVE_STATUS_PROBLEM : CodeConstants.EPMS_PREVENTIVE_STATUS_COMPLETE);
			// 자재 입출고 헤더값 세팅
			preventiveResultVO.setMATERIAL_INOUT(String.valueOf(SEQ_MATERIAL_INOUT));
			// 정비실적등록 - 정비완료 REPAIR_RESULT UPDATE
			rtnVal = preventiveResultMapper.updatePreventiveResult(preventiveResultVO);

			// 2-1-2. 기기이력 등록 (예방보전실적)
			EquipmentHistoryVO equipmentHistoryVO = new EquipmentHistoryVO();

			equipmentHistoryVO.setEQUIPMENT(preventiveResultVO.getEQUIPMENT());
			if (preventiveResultVO.getCHECK_TYPE().equals(CodeConstants.EPMS_CHECK_TYPE_CBM)) {
				equipmentHistoryVO.setHISTORY_TYPE(CodeConstants.EPMS_HISTORY_TYPE_CBM); // CBM
			} else if (preventiveResultVO.getCHECK_TYPE().equals(CodeConstants.EPMS_CHECK_TYPE_TBM)) {
				equipmentHistoryVO.setHISTORY_TYPE(CodeConstants.EPMS_HISTORY_TYPE_TBM); // TBM
			}
			equipmentHistoryVO.setHISTORY_DESCRIPTION(preventiveResultVO.getCHECK_DESCRIPTION());
			equipmentHistoryVO.setDETAIL_ID(preventiveResultVO.getPREVENTIVE_RESULT());
			equipmentHistoryVO.setDATE_HISTORY(preventiveResultVO.getDATE_CHECK());
			equipmentHistoryVO.setUserId(preventiveResultVO.getUserId());

			equipmentHistoryMapper.insertEquipmentHistory(equipmentHistoryVO);

			// 2-1-3. 다음 예방보전 오더 생성
			
			HashMap<String, Object> preventiveResultDtl = preventiveResultMapper.selectPreventiveResultDtl(preventiveResultVO);
//			PreventiveResultVO preventiveResultVO1 = Utility.toBean(preventiveResultDtl, preventiveResultVO.getClass());
			PreventiveResultVO preventiveResultVO1 = new PreventiveResultVO();
			
			// 차기계획일 세팅
			// 업무유형이 예방정비일 경우 (SYSDATE + CHECK_CYCLE )
			if (String.valueOf(preventiveResultDtl.get("PREVENTIVE_TYPE")).equals(CodeConstants.EPMS_PREVENTIVE_TYPE_REPAIR)) {
				preventiveResultVO1.setDATE_PLAN("");
			}
			// 업무유형이 예방점검일 경우 (DATE_PLAN + CHECK_CYCLE)
			else if (String.valueOf(preventiveResultDtl.get("PREVENTIVE_TYPE")).equals(CodeConstants.EPMS_PREVENTIVE_TYPE_MAINTENANCE)) {
				preventiveResultVO1.setDATE_PLAN(String.valueOf(preventiveResultDtl.get("DATE_PLAN")));
			}

			if(preventiveResultDtl.get("DEFAULT_MANAGER") == null){
				preventiveResultVO1.setMANAGER("");
			}
			else{
				preventiveResultVO1.setMANAGER(String.valueOf(preventiveResultDtl.get("DEFAULT_MANAGER")));
			}
			preventiveResultVO1.setCHECK_CYCLE(String.valueOf(preventiveResultDtl.get("CHECK_CYCLE")));
			preventiveResultVO1.setUserId(preventiveResultVO.getUserId());
			preventiveResultVO1.setPREVENTIVE_BOM(String.valueOf(preventiveResultDtl.get("PREVENTIVE_BOM")));

			preventiveResultMapper.insertPreventiveResult(preventiveResultVO1);
			
		} else {
			rtnVal = 0;
		}

		return preventiveResultMapper.selectPreventiveResultDtl(preventiveResultVO);
	}

	/**
	 * 예방보전실적 - 정비요청 정보 가져오기
	 *
	 * @author 김영환
	 * @since 2018.05.09
	 * @param PreventivePlanVO preventivePlanVO
	 * @return int
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> selectRepairPlanDtl(PreventiveResultVO preventiveResultVO) {
		return preventiveResultMapper.selectRepairPlanDtl(preventiveResultVO);
	}
	
	/**
	 * 예방보전실적등록/조회 - 팝업 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.05.29
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectPopPreventiveResultList(PreventiveResultVO preventiveResultVO) {
		return preventiveResultMapper.selectPopPreventiveResultList(preventiveResultVO);
	}
	
	/**
	 *  특수문자 변환 처리
	 *
	 * @author 김영환
	 * @since 2018.11.29
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public static String convertStandardJSONString(String data_json) {
		
        data_json = data_json.replaceAll("&amp;", "&");
        data_json = data_json.replaceAll("&quot;", "\"");
        data_json = data_json.replaceAll("&gt;", ">");
        data_json = data_json.replaceAll("&lt;", "<");
        
        return data_json;
    }
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - 예방보전실적 건수 (메인화면 뱃지카운트)
	 *
	 * @author 서정민
	 * @since 2018.05.24
	 * @param PreventivePlanVO preventivePlanVO
	 * @return int
	 * @throws Exception
	 */
	@Override
	public int preventiveResultCnt(PreventiveResultVO preventiveResultVO) throws Exception {
		return preventiveResultMapper.preventiveResultCnt(preventiveResultVO);
	}
	
	/**
	 * 모바일 - 예방보전실적 목록 조회 
	 *
	 * @author 김영환
	 * @since 2018.05.09
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> preventiveResultList2(PreventiveResultVO preventiveResultVO) throws Exception {
		return preventiveResultMapper.preventiveResultList2(preventiveResultVO);
	}
	
}
