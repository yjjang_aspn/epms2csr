package aspn.hello.epms.preventive.result.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.JsonUtil;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.com.service.DeviceService;
import aspn.hello.epms.preventive.result.model.PreventiveResultVO;
import aspn.hello.epms.preventive.result.service.PreventiveResultService;
import aspn.hello.epms.repair.request.model.RepairRequestVO;
import aspn.hello.epms.repair.request.service.RepairRequestService;
import aspn.hello.epms.repair.result.model.RepairResultVO;
import aspn.hello.epms.repair.result.service.RepairResultService;

/**
 * [예방보전실적등록/조회] Mobile Controller Class
 * @author 김영환
 * @since 2018.05.14
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.14		김영환			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/mobile/epms/preventive/result")
public class MobilePreventiveResultController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	PreventiveResultService preventiveResultService;
	
	@Autowired
	AttachService attachService;
	

	@Autowired
	DeviceService deviceService;
	
	@Autowired
	RepairResultService repairResultService;
	
	@Autowired
	RepairRequestService repairRequestService;
	
	
	/**
	 * 예방보전실적 - 목록 조회
	 * @author 김영환
	 * @since 2018.05.14
	 * @param PreventiveResultVO preventiveResultVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventiveResultList.do")
	@ResponseBody
	public Map<String, Object>  preventiveResultList( PreventiveResultVO preventiveResultVO) throws Exception{
		Map<String, Object> resultMap = new HashMap<>();
		try {
			if(getSsUserId().isEmpty()) {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
				return resultMap;
			}
			preventiveResultVO.setSubAuth(getSsSubAuth());
			preventiveResultVO.setUserId(getSsUserId());
			preventiveResultVO.setCompanyId(getSsCompanyId());
			preventiveResultVO.setSsPart(getSsPart());

			//예방보전 배정목록
			List<HashMap<String, Object>> list= preventiveResultService.preventiveResultList2(preventiveResultVO);
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			
			resultMap.put("RESULT_LIST", list);
			
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	
	/**
	 * 예방보전실적 - 상세정보
	 * 
	 * @author 김영환
	 * @since 2018.05.14 
	 * @param PreventiveResultVO preventiveResultVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventiveResultDtl.do")
	@ResponseBody
	public Map<String, Object> preventiveResultDtl(PreventiveResultVO preventiveResultVO) throws Exception{
		Map<String, Object> resultMap = new HashMap<>();
		try {
			if (JsonUtil.isMobileNullParam(resultMap, preventiveResultVO, "PREVENTIVE_RESULT")) {
				return resultMap;
			} 
			
			preventiveResultVO.setCompanyId(getSsCompanyId());
			preventiveResultVO.setSsLocation(getSsLocation());
			
			//예방보전 상세목록
			HashMap<String, Object> preventiveResultDtl = preventiveResultService.selectPreventiveResultDtl(preventiveResultVO);
			

			if(preventiveResultDtl == null) {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.NO_DATA_FOUND);
			} else {
				resultMap.put("RESULT_DTL", preventiveResultDtl);
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
				
				// 사용자재 등록용 설비 BOM 목록
				RepairResultVO repairResultVO = new RepairResultVO();
				repairResultVO.setEQUIPMENT(String.valueOf(preventiveResultDtl.get("EQUIPMENT")));
				List<HashMap<String, Object>> equipment_bom_list = repairResultService.selectEquipmentBomList(repairResultVO);
				if(equipment_bom_list.isEmpty() == false) {
					resultMap.put("EQUIPMENT_BOM_LIST", equipment_bom_list);
				}
				
				// 정비원 등록용 정비원 목록
				repairResultVO.setCompanyId(getSsCompanyId());
				repairResultVO.setREMARKS(String.valueOf(preventiveResultDtl.get("REMARKS")));		//01:파트구분, 02:파트통합
				repairResultVO.setPART(String.valueOf(preventiveResultDtl.get("PART")));				//파트
				repairResultVO.setSUB_ROOT(String.valueOf(preventiveResultDtl.get("LOCATION")));		//공장코드
				
				repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_PREVENTIVE); // 작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)
				repairResultVO.setWORK_ID(String.valueOf(preventiveResultDtl.get("PREVENTIVE_RESULT")));
				
				List<HashMap<String, Object>> repair_member_list = repairResultService.selectRepairMemberList(repairResultVO);
				if(repair_member_list.isEmpty() == false) {
					resultMap.put("REPAIR_MEMBER_LIST", repair_member_list);
				}
				
				// 파트 등록용 파트 목록
				repairResultVO = new RepairResultVO();
				repairResultVO.setCompanyId(getSsCompanyId());
				List<HashMap<String, Object>> part_list = repairResultService.selectPartOptList2(repairResultVO);
				if(part_list != null) {
					resultMap.put("PART_LIST", part_list);
				}
			}
			
			
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * 예방보전실적 - 등록 
	 * 
	 * @author 김영환
	 * @since 2018.05.14
	 * @param HttpServletRequest request
	 * @param PreventiveResultVO preventiveResultVO
	 * @param RepairRequestVO repairRequestVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatePreventiveResult.do")
	@ResponseBody
	public Map<String, Object> updatePreventiveResult(HttpServletRequest request, MultipartHttpServletRequest mRequest, PreventiveResultVO preventiveResultVO, RepairRequestVO repairRequestVO) {
		Map<String, Object> resultMap = new HashMap<>();
		try {
			AttachVO attachVO = new AttachVO();
			attachVO.setDEL_SEQ(preventiveResultVO.getDEL_SEQ());
			attachVO.setMODULE(preventiveResultVO.getMODULE()); 
			
			// 1. 첨부파일 등록(예방점검실적 및 정비요청)
			if("".equals(preventiveResultVO.getATTACH_GRP_NO()) || preventiveResultVO.getATTACH_GRP_NO() == null){
				if("Y".equals(preventiveResultVO.getAttachExistYn())){
					String attachGrpNo = attachService.selectAttachGrpNo();
					attachVO.setATTACH_GRP_NO(attachGrpNo);
					preventiveResultVO.setATTACH_GRP_NO(attachGrpNo);
					repairRequestVO.setATTACH_GRP_NO(attachGrpNo);
					attachService.AttachEdit(request, mRequest, attachVO); 
				}
			}
			else{
				attachVO.setATTACH_GRP_NO(preventiveResultVO.getATTACH_GRP_NO());
				attachService.AttachEdit(request, mRequest, attachVO); 
			}
			
			// 2. CBM 판정 X일 경우 정비요청하기
			if(CodeConstants.EPMS_CHECK_TYPE_CBM.equals(preventiveResultVO.getCHECK_TYPE()) && CodeConstants.EPMS_CHECK_DICISION_FAIL.equals(preventiveResultVO.getCHECK_DECISION())){
				
				//파트구분 공장일 경우
				if(CodeConstants.REMARKS_SEPARATE.equals(repairRequestVO.getREMARKS())){
					//팀장일 경우 요청후 자동결재
					if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
						repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
					//정비원일 경우
					} else if (getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1){
						if(getSsSubAuth().indexOf(repairRequestVO.getLOCATION())>-1 && getSsPart().equals(preventiveResultVO.getREQUEST_PART()) ){
//						if(getSsLocation().equals(repairRequestVO.getLOCATION()) && getSsPart().equals(preventiveResultVO.getREQUEST_PART())){
							repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
						}
					}
				//파트통합 공장일 경우
				} else if(CodeConstants.REMARKS_UNITED.equals(repairRequestVO.getREMARKS())){
					//팀장, 정비원일 경우 요청후 자동결재
					if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
						repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
					} else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1) {
						if(getSsSubAuth().indexOf(repairRequestVO.getLOCATION())>-1){
//						if(getSsLocation().equals(repairRequestVO.getLOCATION())){
							repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
						}
					}
				}
				
				repairRequestVO.setCompanyId(getSsCompanyId());
				repairRequestVO.setSsLocation(getSsLocation());
				repairRequestVO.setUserId(getSsUserId());
				repairRequestVO.setDIVISION(getSsDivision());
				repairRequestVO.setREQUEST_TYPE(CodeConstants.EPMS_REQUEST_TYPE_PREVENTIVE);								//요청유형 세팅
				repairRequestVO.setPART(preventiveResultVO.getREQUEST_PART());					//처리파트 세팅
				repairRequestVO.setMANAGER(preventiveResultVO.getREQUEST_MANAGER());			//담당자 세팅
				repairRequestVO.setREQUEST_DESCRIPTION(preventiveResultVO.getCHECK_DESCRIPTION());	//정비요청 세팅(점검내용과 동일)
				repairRequestVO.setDATE_PLAN(DateTime.getDateString());			//배정일
				
				// 예방정비 요청
				HashMap<String, Object> repairRequest = repairRequestService.insertRepairRequest(repairRequestVO);
				// 정비요청ID(예방정비) 세팅
				preventiveResultVO.setREPAIR_REQUEST(String.valueOf(repairRequest.get("REPAIR_REQUEST")));
			
				if(repairRequest != null) {
					// Push 정보
					HashMap<String, Object> pushParam = repairRequest;
					String pushTitle = "";
					
					// 1. 자동결재일 경우 (접수 or 배정)
					if(CodeConstants.FLAG_AUTOAPPR.equals(repairRequestVO.getFlag())){
						
						// 1-1. 접수까지 수행된 경우 : 처리파트 전체 정비원에게 푸시 전송
						if(CodeConstants.EPMS_REPAIR_STATUS_RECEIVE.equals(pushParam.get("REPAIR_STATUS").toString())){
							// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
							pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_PLAN);
							// Push Title
							pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비(예방정비)를 요청하였습니다.(담당자 미배정)";
							pushParam.put("PUSHTITLE", pushTitle);
							// 상세화면 IDX 세팅
							pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
							// 알림음 유형 세팅(01:일반, 02:긴급)
							pushParam.put("PUSHALARM", "01");
						}
						
						// 1-2. 배정까지 수행된 경우 : 정비 담당자에게 푸시 전송
						else if(CodeConstants.EPMS_REPAIR_STATUS_PLAN.equals(pushParam.get("REPAIR_STATUS").toString())){
							// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
							pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REPAIR);
							// Push Title
							pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비(예방정비)를 요청하였습니다.(담당자 : " + pushParam.get("MANAGER_NAME").toString() + ")";
							pushParam.put("PUSHTITLE", pushTitle);
							// 상세화면 IDX 세팅
							pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
							// 알림음 유형 세팅(01:일반, 02:긴급)
							pushParam.put("PUSHALARM", "01");
						}
						
						deviceService.sendPush(pushParam);
					}
					// 2. 자동결재 아닐 경우  : 처리파트 전체 정비원에게 푸시 전송
					else{
						// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
						pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_APPR);
						// Push Title
						pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비(예방정비)를 요청하였습니다.";
						pushParam.put("PUSHTITLE", pushTitle);
						// 상세화면 IDX 세팅
						pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
						// 알림음 유형 세팅(01:일반, 02:긴급)
						pushParam.put("PUSHALARM", "01");
						
						deviceService.sendPush(pushParam);
					}
				}
			}
			
			// 3. 예방보전실적 등록	
			preventiveResultVO.setCompanyId(getSsCompanyId());
			preventiveResultVO.setSsLocation(getSsLocation());
			preventiveResultVO.setUserId(getSsUserId());
			preventiveResultService.updatePreventiveResult(preventiveResultVO);
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			
		}catch (Exception e) {
			logger.error("PreventiveResultController.updatePreventiveResultReg Error !");
			e.printStackTrace();
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
}
