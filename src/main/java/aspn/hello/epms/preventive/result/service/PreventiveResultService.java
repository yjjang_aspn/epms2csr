package aspn.hello.epms.preventive.result.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.preventive.result.model.PreventiveResultVO;

/**
 * [예방보전실적] Service Class
 * 
 * @author 김영환
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09 	김영환			최초 생성
 *   
 * </pre>
 */

public interface PreventiveResultService {
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	/**
	 * 예방보전실적 - 목록 조회 
	 *
	 * @author 김영환
	 * @since 2018.05.09
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> preventiveResultList(PreventiveResultVO preventiveResultVO) throws Exception;

	/**
	 * 예방보전실적 - 상세 
	 *
	 * @author 김영환
	 * @since 2018.05.09
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public HashMap<String, Object> selectPreventiveResultDtl(PreventiveResultVO preventiveResultVO) throws Exception;

	/**
	 * 예방보전실적 - 사용자재 목록
	 *
	 * @author 김영환
	 * @since 2018.05.09
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectPreventiveUsedMaterialList(PreventiveResultVO preventiveResultVO) throws Exception;

	/**
	 * 예방보전실적 - 공통코드 판정 목록
	 *
	 * @author 김영환
	 * @since 2018.05.09
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectCheckDecisionList() throws Exception;

	/**
	 * 예방보전실적 - 수정
	 *
	 * @author 김영환
	 * @since 2018.05.09
	 * @param PreventivePlanVO preventivePlanVO
	 * @return int
	 * @throws Exception
	 */
	public HashMap<String, Object> updatePreventiveResult(PreventiveResultVO preventiveResultVO) throws Exception;

	/**
	 * 예방보전실적 - 정비요청 정보 가져오기
	 *
	 * @author 김영환
	 * @since 2018.05.09
	 * @param PreventivePlanVO preventivePlanVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public HashMap<String, Object> selectRepairPlanDtl(PreventiveResultVO preventiveResultVO);
	
	/**
	 * 예방보전실적등록/조회 - 팝업 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.05.29
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectPopPreventiveResultList(PreventiveResultVO preventiveResultVO);
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	/**
	 * 모바일 - 예방보전실적 건수 (메인화면 뱃지카운트)
	 *
	 * @author 서정민
	 * @since 2018.05.24
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public int preventiveResultCnt(PreventiveResultVO preventiveResultVO) throws Exception;

	/**
	 * 모바일 - 예방보전실적 목록 조회 
	 *
	 * @author 김영환
	 * @since 2018.05.09
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> preventiveResultList2(PreventiveResultVO preventiveResultVO) throws Exception;
}
