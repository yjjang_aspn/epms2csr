package aspn.hello.epms.preventive.result.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.ObjUtil;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.com.service.DeviceService;
import aspn.hello.epms.preventive.plan.model.PreventivePlanVO;
import aspn.hello.epms.preventive.plan.service.PreventivePlanService;
import aspn.hello.epms.preventive.result.model.PreventiveResultVO;
import aspn.hello.epms.preventive.result.service.PreventiveResultService;
import aspn.hello.epms.repair.request.model.RepairRequestVO;
import aspn.hello.epms.repair.request.service.RepairRequestService;
import aspn.hello.epms.repair.result.model.RepairResultVO;
import aspn.hello.epms.repair.result.service.RepairResultService;
import aspn.hello.mem.model.MemberVO;

/**
 * [예방보전실적] Controller Class
 * @author 김영환
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		김영환			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/preventive/result")
public class PreventiveResultController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	PreventiveResultService preventiveResultService;
	
	@Autowired
	PreventivePlanService preventivePlanService;
	
	@Autowired
	AttachService attachService;
	
	@Autowired
	DeviceService deviceService;
	
	@Autowired
	RepairRequestService repairRequestService;
	
	@Autowired
	RepairResultService repairResultService;
	
	/**
	 * 예방보전실적등록/조회
	 * @author 김영환
	 * @since 2018.05.09
	 * @param PreventivePlanVO preventiveResultVO
	 * @param Model model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventiveResultMng.do")
	public String preventiveResultMng( PreventivePlanVO preventivePlanVO, Model model) throws Exception{
		
		MemberVO memberVO = getSsMemberVO();
//		preventivePlanVO.setLOCATION(memberVO.getLOCATION());
		preventivePlanVO.setSUB_ROOT(memberVO.getLOCATION());
		preventivePlanVO.setPART(memberVO.getPART());
		
		model.addAttribute("part", memberVO.getPART());
		model.addAttribute("location", memberVO.getLOCATION());
		model.addAttribute("startDt", DateTime.requestMmDay("1","yyyy-MM-dd"));
		model.addAttribute("endDt", DateTime.getDateString());
		return "/epms/preventive/result/preventiveResultMng";
	}
	
	/**
	 * 예방보전실적등록/조회 - 그리드 레이아웃
	 * @author 김영환
	 * @since 2018.05.09
	 * @param PreventivePlanVO preventiveResultVO
	 * @param Model model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventiveResultListLayout.do")
	public String preventiveResultListLayout(PreventivePlanVO preventivePlanVO, Model model) throws Exception{
		
		preventivePlanVO.setSubAuth(getSsSubAuth());	 //세션_조회권한
		
		//정비파트원 목록
		List<HashMap<String, Object>> list4 = preventivePlanService.repairMemberList(preventivePlanVO);
		HashMap<String, Object> repairMemberListOpt  = ObjUtil.list2Map(list4, "|");
		model.addAttribute("repairMemberListOpt", repairMemberListOpt);
		
		//정비파트원 목록(공장별,파트별):RELATED
		List<HashMap<String, Object>> list5 = preventivePlanService.repairMemberList2(preventivePlanVO);
		String relatedCombo = ObjUtil.list2MapRelated(list5, "|", "CODE", "NAME");
		model.addAttribute("relatedCombo", relatedCombo);
		
		return "/epms/preventive/result/preventiveResultListLayout";
	}
	
	/**
	 * 예방보전실적등록/조회 - 그리드 데이터
	 * @author 김영환
	 * @since 2018.05.09
	 * @param PreventiveResultVO preventiveResultVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventiveResultListData.do")
	public String preventiveResultListData( PreventiveResultVO preventiveResultVO, Model model) throws Exception{
		try {
			preventiveResultVO.setSubAuth(getSsSubAuth());
			preventiveResultVO.setCompanyId(getSsCompanyId());
			preventiveResultVO.setSsPart(getSsPart());
			
			//팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				preventiveResultVO.setAuthType(CodeConstants.AUTH_CHIEF);
			}
			//정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
				preventiveResultVO.setAuthType(CodeConstants.AUTH_REPAIR);
			}
			
			//예방보전 배정목록
			List<HashMap<String, Object>> list= preventiveResultService.preventiveResultList(preventiveResultVO);
			model.addAttribute("userId", getSsUserId());
			model.addAttribute("list", list);
			
			if("01".equals(getSsRemarks())){
				model.addAttribute("locationPart", getSsLocation()+"_"+getSsPart());
			}else{
				model.addAttribute("locationPart", getSsLocation());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "/epms/preventive/result/preventiveResultListData";
	}
	
	/**
	 * 예방보전실적 상세정보 팝업 화면
	 * 
	 * @author 김영환
	 * @since 2018.05.09 
	 * @param PreventiveResultVO preventiveResultVO
	 * @param  Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popPreventiveResultForm.do")
	public String popPreventiveResultForm(PreventiveResultVO preventiveResultVO, Model model) throws Exception{
		
		preventiveResultVO.setCompanyId(getSsCompanyId());
		preventiveResultVO.setSsLocation(getSsLocation());
		
		//예방보전실적 상세 조회
		HashMap<String, Object> preventiveResultDtl = preventiveResultService.selectPreventiveResultDtl(preventiveResultVO);
		model.addAttribute("preventiveResultDtl", preventiveResultDtl);
		
		//예방보전실적에 등록된 자재 목록
		List<HashMap<String, Object>> usedMaterial1 = preventiveResultService.selectPreventiveUsedMaterialList(preventiveResultVO);
		model.addAttribute("usedMaterial1", usedMaterial1);
		
		//X 판정일때 정비요청 정보 가져오기
		if(CodeConstants.EPMS_CHECK_DICISION_FAIL.equals(preventiveResultDtl.get("CHECK_DECISION"))){
			PreventiveResultVO preventiveResultVO1= new PreventiveResultVO();
			preventiveResultVO1.setREPAIR_REQUEST(String.valueOf(preventiveResultDtl.get("REPAIR_REQUEST")));
			
			HashMap<String, Object> repairRequestDtl = preventiveResultService.selectRepairPlanDtl(preventiveResultVO1);	
			if (repairRequestDtl != null)
				model.addAttribute("repairRequestDtl", repairRequestDtl);
			
			if("03".equals(preventiveResultDtl.get("STATUS"))){
				RepairResultVO repairResultVO = new RepairResultVO();
				repairResultVO.setCompanyId(getSsCompanyId());
				repairResultVO.setSsLocation(getSsLocation());
				repairResultVO.setREPAIR_REQUEST(String.valueOf(preventiveResultDtl.get("REPAIR_REQUEST")));
				
				//정비실적 상세 조회
				HashMap<String, Object> repairResultDtl = repairResultService.selectRepairResultDtl(repairResultVO);
				model.addAttribute("repairResultDtl", repairResultDtl);
				
				if(!"".equals(String.valueOf(repairResultDtl.get("REPAIR_RESULT")))){
					//정비실적에 등록된 자재 목록
					List<HashMap<String, Object>> usedMaterial2 = repairResultService.selectRepairUsedMaterialList(repairResultVO);
					model.addAttribute("usedMaterial2", usedMaterial2);
					
					//정비실적에 등록된 정비원 조회
					repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //01:정비실적, 02:고장원인분석
					repairResultVO.setWORK_ID(String.valueOf(repairResultDtl.get("REPAIR_RESULT")));
					List<HashMap<String, Object>> workMemberList = repairResultService.selectWorkMemberList(repairResultVO);
					model.addAttribute("repairRegMember", workMemberList);
					
					//정비사진
					if(!("".equals(repairResultDtl.get("ATTACH_GRP_NO2")) || repairResultDtl.get("ATTACH_GRP_NO2") == null)){
						AttachVO attachVO = new AttachVO();
						attachVO.setATTACH_GRP_NO(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO2")));
						List<HashMap<String, Object>> attachList2 = attachService.selectFileList(attachVO);
						model.addAttribute("attachList2", attachList2);
					}
					
					//고장유형 목록
					List<HashMap<String, Object>> troubleList1 = repairResultService.selectTroubleList1();
					model.addAttribute("troubleList1", troubleList1);
					
					//조치 목록
					List<HashMap<String, Object>> troubleList2 = repairResultService.selectTroubleList2();
					model.addAttribute("troubleList2", troubleList2);
				}
				
			}
		}
		
		//첨부파일
		if(Integer.parseInt(String.valueOf(preventiveResultDtl.get("FILE_CNT"))) > 0){
			AttachVO attachVO = new AttachVO();
			attachVO.setATTACH_GRP_NO(String.valueOf(preventiveResultDtl.get("ATTACH_GRP_NO")));
			List<HashMap<String, Object>> attachList1 = attachService.selectFileList(attachVO);
			model.addAttribute("attachList1", attachList1);
		}
		
		return "/epms/preventive/result/popPreventiveResultForm";
	}
	
	/**
	 * 예방보전실적 상세정보 등록 팝업 화면
	 * 
	 * @author 김영환
	 * @since 2018.05.09 
	 * @param PreventiveResultVO preventiveResultVO
	 * @param  Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popPreventiveResultRegForm.do")
	public String popPreventiveResultRegForm(PreventiveResultVO preventiveResultVO, Model model) throws Exception{
		
		preventiveResultVO.setCompanyId(getSsCompanyId());
		preventiveResultVO.setSsLocation(getSsLocation());
		
		//예방보전실적 상세 조회
		HashMap<String, Object> preventiveResultDtl = preventiveResultService.selectPreventiveResultDtl(preventiveResultVO);
		model.addAttribute("preventiveResultDtl", preventiveResultDtl);
		
		//예방보전실적에 등록된 자재 목록
		List<HashMap<String, Object>> usedMaterial = preventiveResultService.selectPreventiveUsedMaterialList(preventiveResultVO);
		model.addAttribute("usedMaterial", usedMaterial);
		
		//판정 목록
		List<HashMap<String, Object>> checkDecisionList = preventiveResultService.selectCheckDecisionList();
		model.addAttribute("checkDecisionList", checkDecisionList);
		
		model.addAttribute("remarks", getSsRemarks());
		model.addAttribute("userPart", getSsPart());
		model.addAttribute("userId", getSsUserId());
		if("01".equals(getSsRemarks())){
			model.addAttribute("locationPart", getSsLocation()+"_"+getSsPart());
		}else{
			model.addAttribute("locationPart", getSsLocation());
		}
		
		return "/epms/preventive/result/popPreventiveResultRegForm";
	}
	
	/**
	 * 예방보전실적등록/조회 - 실적 임시저장,정비완료 
	 * 
	 * @author 김영환
	 * @since 2018.05.10
	 * @param HttpServletRequest request
	 * @param RepairResultVO repairResultVO
	 * @param Model model
	 * @return int
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventiveResultRegAjax.do")
	@ResponseBody
	public HashMap<String, Object> repairResultRegAjax(HttpServletRequest request, MultipartHttpServletRequest mRequest, PreventiveResultVO preventiveResultVO, RepairRequestVO repairRequestVO, Model model) {
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		try {
			
			// 1. 첨부파일 등록(예방점검실적 및 정비요청)
			AttachVO attachVO = new AttachVO();
			attachVO.setDEL_SEQ(preventiveResultVO.getDEL_SEQ());
			attachVO.setMODULE(preventiveResultVO.getMODULE());
			
			if("".equals(preventiveResultVO.getATTACH_GRP_NO()) || preventiveResultVO.getATTACH_GRP_NO() == null){
				if("Y".equals(preventiveResultVO.getAttachExistYn())){
					String attachGrpNo = attachService.selectAttachGrpNo();
					attachVO.setATTACH_GRP_NO(attachGrpNo);
					preventiveResultVO.setATTACH_GRP_NO(attachGrpNo);
					repairRequestVO.setATTACH_GRP_NO(attachGrpNo);
					attachService.AttachEdit(request, mRequest, attachVO); 
				}
			}
			else{
				attachVO.setATTACH_GRP_NO(preventiveResultVO.getATTACH_GRP_NO());
 				attachService.AttachEdit(request, mRequest, attachVO); 
			}
			
			// 2. CBM 판정 X일 경우 정비요청하기
			if(CodeConstants.EPMS_CHECK_TYPE_CBM.equals(preventiveResultVO.getCHECK_TYPE()) && CodeConstants.EPMS_CHECK_DICISION_FAIL.equals(preventiveResultVO.getCHECK_DECISION())){
				
				//파트구분 공장일 경우
				if(CodeConstants.REMARKS_SEPARATE.equals(repairRequestVO.getREMARKS())){
					//팀장일 경우 요청후 자동결재
					if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
						repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
					//정비원일 경우
					} else if (getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1){
						if(getSsSubAuth().indexOf(repairRequestVO.getLOCATION())>-1 && getSsPart().equals(preventiveResultVO.getREQUEST_PART()) ){
//						if(getSsLocation().equals(repairRequestVO.getLOCATION()) && getSsPart().equals(preventiveResultVO.getREQUEST_PART())){
							repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
						}
					}
				//파트통합 공장일 경우
				} else if(CodeConstants.REMARKS_UNITED.equals(repairRequestVO.getREMARKS())){
					//팀장, 정비원일 경우 요청후 자동결재
					if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
						repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
					} else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1) {
						if(getSsSubAuth().indexOf(repairRequestVO.getLOCATION())>-1){
//						if(getSsLocation().equals(repairRequestVO.getLOCATION())){
							repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
						}
					}
				}

				repairRequestVO.setCompanyId(getSsCompanyId());
				repairRequestVO.setSsLocation(getSsLocation());
				repairRequestVO.setUserId(getSsUserId());
				repairRequestVO.setDIVISION(getSsDivision());
				repairRequestVO.setREQUEST_TYPE(CodeConstants.EPMS_REQUEST_TYPE_PREVENTIVE);	//요청유형 세팅
				repairRequestVO.setPART(preventiveResultVO.getREQUEST_PART());					//처리파트 세팅
				repairRequestVO.setMANAGER(preventiveResultVO.getREQUEST_MANAGER());			//담당자 세팅
				repairRequestVO.setREQUEST_DESCRIPTION(preventiveResultVO.getCHECK_DESCRIPTION());	//정비요청 세팅(점검내용과 동일)
				repairRequestVO.setDATE_PLAN(DateTime.getDateString());							//배정일
				
				// 예방정비 요청
				HashMap<String, Object> repairRequest = repairRequestService.insertRepairRequest(repairRequestVO);
				// 정비요청 ID(예방정비) 세팅
				preventiveResultVO.setREPAIR_REQUEST(String.valueOf(repairRequest.get("REPAIR_REQUEST")));
				
				if(repairRequest != null) {
					// Push 정보
					HashMap<String, Object> pushParam = repairRequest;
					String pushTitle = "";
					
					// 1. 자동결재일 경우 (접수 or 배정)
					if(CodeConstants.FLAG_AUTOAPPR.equals(repairRequestVO.getFlag())){
						
						// 1-1. 접수까지 수행된 경우 : 처리파트 전체 정비원에게 푸시 전송
						if(CodeConstants.EPMS_REPAIR_STATUS_RECEIVE.equals(pushParam.get("REPAIR_STATUS").toString())){
							// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
							pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_PLAN);
							// Push Title
							pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비(예방정비)를 요청하였습니다.(담당자 미배정)";
							pushParam.put("PUSHTITLE", pushTitle);
							// 상세화면 IDX 세팅
							pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
							// 알림음 유형 세팅(01:일반, 02:긴급)
							pushParam.put("PUSHALARM", "01");
						}
						
						// 1-2. 배정까지 수행된 경우 : 정비 담당자에게 푸시 전송
						else if(CodeConstants.EPMS_REPAIR_STATUS_PLAN.equals(pushParam.get("REPAIR_STATUS").toString())){
							// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
							pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REPAIR);
							// Push Title
							pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비(예방정비)를 요청하였습니다.(담당자 : " + pushParam.get("MANAGER_NAME").toString() + ")";
							pushParam.put("PUSHTITLE", pushTitle);
							// 상세화면 IDX 세팅
							pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
							// 알림음 유형 세팅(01:일반, 02:긴급)
							pushParam.put("PUSHALARM", "01");
						}
						
						deviceService.sendPush(pushParam);
					}
					// 2. 자동결재 아닐 경우  : 처리파트 전체 정비원에게 푸시 전송
					else{
						// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
						pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_APPR);
						// Push Title
						pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비(예방정비)를 요청하였습니다.";
						pushParam.put("PUSHTITLE", pushTitle);
						// 상세화면 IDX 세팅
						pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
						// 알림음 유형 세팅(01:일반, 02:긴급)
						pushParam.put("PUSHALARM", "01");
						
						deviceService.sendPush(pushParam);
					}
				}
			}
			
			// 3. 예방보전실적 등록	
			preventiveResultVO.setCompanyId(getSsCompanyId());
			preventiveResultVO.setSsLocation(getSsLocation());
			preventiveResultVO.setUserId(getSsUserId());
			rtnMap = preventiveResultService.updatePreventiveResult(preventiveResultVO);
			
		}catch (Exception e) {
			logger.error("PreventiveResultController.preventiveResultRegAjax Error !");
			e.printStackTrace();
		}
		
		return rtnMap;
	}
	
	/**
	 * 예방보전실적등록/조회 - 팝업 그리드 레이아웃
	 * @author 김영환
	 * @since 2018.05.09
	 * @param PreventivePlanVO preventiveResultVO
	 * @param Model model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/popPreventiveResultListLayout.do")
	public String popPreventiveResultListLayout(PreventivePlanVO preventivePlanVO, Model model) throws Exception{
		return "/epms/preventive/result/popPreventiveResultListLayout";
	}
	
	/**
	 * 예방보전실적등록/조회 - 팝업 그리드 데이터
	 * @author 김영환
	 * @since 2018.05.09
	 * @param PreventiveResultVO preventiveResultVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/popPreventiveResultListData.do")
	public String popPreventiveResultListData( PreventiveResultVO preventiveResultVO, Model model) throws Exception{
		try {
			preventiveResultVO.setSubAuth(getSsSubAuth());
			preventiveResultVO.setCompanyId(getSsCompanyId());
			preventiveResultVO.setSsLocation(getSsLocation());
			
			List<HashMap<String, Object>> list= preventiveResultService.selectPopPreventiveResultList(preventiveResultVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "/epms/report/preventivePart/popPreventiveResultListData";
	}
	
	
	
	
	
	
	
	
}
