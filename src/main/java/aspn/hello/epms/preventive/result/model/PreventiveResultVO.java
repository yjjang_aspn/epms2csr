package aspn.hello.epms.preventive.result.model;

import aspn.hello.com.model.AbstractVO;

/**
 * [예방보전실적 등록/조회] VO Class
 * @author 김영환
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		김영환			최초 생성
 *   
 * </pre>
 */

public class PreventiveResultVO extends AbstractVO{
	
private static final long serialVersionUID = 1L;

	/** PREVENTIVE_RESULT DB model */
	private String PREVENTIVE_RESULT		=""; //예방보전실적 ID
	private String PREVENTIVE_BOM			=""; //예방보전BOM ID
	private String PREVENTIVE_TYPE			=""; //업무구분 
	private String PREVENTIVE_TYPE_NAME		=""; //웁무구분
	private String DATE_CHECK				=""; //점검일
	private String STATUS					=""; //상태
	private String STATUS_NAME				=""; //상태
	private String CHECK_DECISION			=""; //판정
	private String CHECK_VALUE				=""; //측정치
	private String CHECK_DESCRIPTION		=""; //점검내용
	private String SUCCESS_YN				=""; //주기전 고장여부
	private String CHECK_TYPE				=""; //점검구분
	private String CHECK_TYPE_NAME			=""; //점검구분
	private String REG_ID					=""; //등록자
	private String DEFAULT_MANAGER			=""; //기본 담당자
	private String requestReport			=""; //레포트 예방보전 현황에서  파트, 정비원 구분 
	
	
	private String REQUEST_PART				=""; //정비요청 부서
	
	
	private String materialUsedArrJSON		= ""; //사용 자재목록 JSON
	private String materialArrJSON			= ""; //신규 자재목록 JSON
	private String REQUEST_MANAGER			= ""; //정비요청 매니저 
	
	
	/** EQUIPMENT DB model  */
	private String EQUIPMENT				= ""; //설비 ID
	private String EQUIPMENT_UID			= ""; //설비코드
	private String EQUIPMENT_NAME			= ""; //설비명
	private String LOCATION					= ""; //회사 ID
	private String GRADE					= ""; //설비등급
	private String ASSET_NO					= ""; //자산번호
	private String MODEL					= ""; //모델명
	private String DIMENSION				= ""; //크기/치수 
	private String WEIGHT					= ""; //총중량
	private String CD_WEIGHT				= ""; //중량단위
	private String ACQUISITION_VALUE		= ""; //취드가액
	private String CURRENCY					= ""; //통화
	private String COUNTRY					= ""; //제조국
	private String DATE_ACQUISITION			= ""; //취득일
	private String DATE_INSTALL				= ""; //설치일
	private String DATE_OPERATE				= ""; //가동일
	private String DATE_VALID				= ""; //유효일(효력시작일)
	private String LINE						= ""; //기등위치
	private String FIELD					= ""; //정렬필드
	private String MANUFACTURER				= ""; //자산제조사
	private String COST_CENTER				= ""; //코스트센터
	private String INFORMATION1				= ""; //에너지정보-전기
	private String INFORMATION2				= ""; //에너지정보-가스
	private String DESCRIPTION				= ""; //설비설명
	private String DATE_C					= ""; //등록일
	private String DATE_E					= ""; //수정일
	private String MEMBER					= ""; //등록자
	private String CD_DEL					= ""; //삭제여부
	private String CD_DEL1					= ""; //삭제여부
	private String CD_DEL2					= ""; //삭제여부
	private String CD_DEL3					= ""; //삭제여부
	private String SEQ_EQUIPMENT			= ""; //설비시퀀스
	
	
	/** MATERIAL_GRP DB model  */
	private String MATERIAL_GRP				= ""; //장치 ID
	private String MATERIAL_GRP_UID			= ""; //장치코드
	private String MATERIAL_GRP_NAME		= ""; //장치명
	private String SEQ_MATERIAL_GRP			= ""; //장치시퀀스
	
	
	/** EQUIPMENT_BOM DB model  */
	private String EQUIPMENT_BOM			= ""; //설비BOM ID
	private String SEQ_EQUIPMENT_BOM		= ""; //설비BOM시퀀스
	
	
	/** EQUIPMENT_HISTORY DB model  */
	private String EQUIPMENT_HISTORY		= ""; //설비이력 ID
	private String HISTORY_TYPE				= ""; //이력유형(01:일반정비, 02:돌발정비, 03:예방정비, 04:CBM, 05:TBM, 10:마스터등록, 11:라인이동)
	private String DATE_HISTORY				= ""; //이력날짜
	private String HISTORY_DESCRIPTION		= ""; //이력날짜
	private String DETAIL_ID				= ""; //상세정보ID(정비실적ID, 예방보전실적ID)
	private String SEQ_EQUIPMENT_HISTORY	= ""; //설비이력시퀀스
	
	
	/** REPAIR_REQUEST DB model  */
	private String REPAIR_REQUEST			= ""; //정비요청 ID
	private String REQUEST_TYPE				= ""; //정비요청유형(01:일반, 02:예방정비, 03:타파트 업무협조)
	private String REPAIR_REQUEST_ORG		= ""; //원정비요청 ID
	private String APPR_STATUS				= ""; //결재상태(01:대기, 02:승인, 03:반려)
	private String REQUEST_DESCRIPTION		= ""; //고장내용
	private String REQUEST_DIV				= ""; //요청부서
	private String DATE_REQUEST				= ""; //요청일
	private String REQUEST_MEMBER			= ""; //요청자
	private String DATE_APPR				= ""; //결재일
	private String APPR_MEMBER				= ""; //결재자
	private String REJECT_DESCRIPTION		= ""; //반려사유
	private String DATE_CANCEL				= ""; //정비취소일
	private String CANCEL_MEMBER			= ""; //취소자
	private String SEQ_REPAIR_REQUEST		= ""; //정비요청시퀀스
	
	
	/** REPAIR_RESULT DB model  */
	private String REPAIR					= ""; //정비 ID
	private String REPAIR_TYPE				= ""; //정비요청유형(110:돌발정비, 120:일상정비, 130:보전정비, 140:사후정비, 150:예방정비)
	private String REPAIR_STATUS			= ""; //정비상태(01:접수, 02:배정완료, 03:정비완료)
	private String DATE_ASSIGN				= ""; //배정일
	private String DATE_PLAN				= ""; //계획일
	private String DATE_REPAIR				= ""; //정비완료일
	private String TROUBLE_TYPE1			= ""; //고장현상ID
	private String TROUBLE_DESCRIPTION1		= ""; //고장현상
	private String TROUBLE_TYPE2			= ""; //조치ID
	private String TROUBLE_DESCRIPTION2		= ""; //조치
	private String MANAGER					= ""; //배정자
	private String MANAGER_NAME				= ""; //배정자 이름
	private String REPAIR_MEMBER			= ""; //정비원ID
	private String OUTSOURCING				= ""; //외주
	private String MATERIAL_UNREG			= ""; //미등록자재
	private String TROUBLE_TIME1			= ""; //고장시작시간
	private String TROUBLE_TIME2			= ""; //고장종료시간
	private String REPAIR_DESCRIPTION		= ""; //정비내용
	private String SEQ_REPAIR				= ""; //정비시퀀스
	private String E_AUFNR					= ""; //SAP-오더번호
	private String E_RESULT					= ""; //SAP-성공여부
	
	/** REPAIR_ANALYSIS DB model  */
	private String REPAIR_ANALYSIS			= ""; //고장원인분석ID
	private String DATE_ANALYSIS			= ""; //분석일
	private String LOSS						= ""; //피해현황
	private String REASON1					= ""; //1차원인
	private String MEASURE1					= ""; //1차대책
	private String REASON2					= ""; //2차원인
	private String MEASURE2					= ""; //2차대책
	private String REASON3					= ""; //3차원인
	private String MEASURE3					= ""; //3차대책
	private String SAVE_YN					= ""; //저장여부
	private String SEQ_REPAIR_ANALYSIS		= ""; //고장원인분석시퀀스
	
	
	/** MAINTENANCE_BOM DB model  */
	private String MAINTENANCE_BOM			= ""; //예방보전 ID
	private String PART						= ""; //파트(01:기계, 02:전기, 03:시설)
	private String MAINTENANCE_TYPE			= ""; //보전구분(01:CBM, 02:TBM)
	private String CHECK_ITEM				= ""; //점검항목
	private String CHECK_STANDARD			= ""; //판단기분
	private String CHECK_TOOL				= ""; //진단장비
	private String CHECK_TYPE1				= ""; //점검방식(01:OSI, 02:SDI)
	private String MAINTENANCE_STANDARD		= ""; //보전표준(01:점검표준, 02:정비표준, 03:알람 및 이물오염)
	private String CHECK_TYPE2				= ""; //점검구분(01:정기)
	private String CHECK_CYCLE				= ""; //주기
	private String SEQ_MAINTENANCE_BOM		= ""; //예방보전시퀀스
	
	
	/** MAINTENANCE_RESULT DB model  */
	private String MAINTENANCE				= ""; //예방보전실적 ID
	private String MAINTENANCE_STATUS		= ""; //점검상태(01:미점검, 02:정비필요, 03:완료)
	private String DATE_MAINTENANCE			= ""; //점검완료일
	private String MEASURED_VALUE			= ""; //측정치
	private String MAINTENANCE_DECISION		= ""; //판정(01:Y, 02:N, 03:NY)
	private String MAINTENANCE_DESCRIPTION	= ""; //점검내용
	private String SEQ_MAINTENANCE			= ""; //예방보전실적시퀀스
	
	
	/** WORK_LOG DB model  */
	private String WORK_LOG					= ""; //작업이력 ID
	private String WORK_TYPE				= ""; //작업유형(01:정비, 02:예방보전)
	private String WORK_ID					= ""; //작업대상ID
	private String SEQ_WORK_LOG			= ""; //예방보전실적시퀀스
	
	
	/** MATERIAL DB model  */
	private String MATERIAL					= ""; //자재 ID
	private String CD_MATERIAL_TYPE			= ""; //자재구분
	private String CD_MATERIAL_GRP1			= ""; //대그룹
	private String CD_MATERIAL_GRP2			= ""; //중그룹
	private String CD_MATERIAL_GRP3			= ""; //소그룹
	private String MATERIAL_UID				= ""; //자재코드
	private String MATERIAL_NAME			= ""; //자재명
	private String FUNCTION					= ""; //기능
	private String SPEC						= ""; //규격/사양
	private String MEMO						= ""; //메모
	private String CD_UNIT					= ""; //단위
	private String STOCK					= ""; //재고
	private String STOCK_OPTIMAL			= ""; //적정재고
	
	
	/** MATERIAL_INOUT DB model  */
	private String MATERIAL_INOUT			= ""; //입출고 ID
	private String CD_MATERIAL_INOUT_TYPE	= ""; //입출고유형(01:입고,02:출고,03:임의입고,04:임의출고)
	private String DATE_INOUT				= ""; //입출고일
	
	
	/** MATERIAL_INOUT_ITEM DB model  */
	private String QNTY						= ""; //수량
	private String PRICE					= ""; //가격
	private String SEQ_MATERIAL_INOUT_ITEM	= ""; //자재입출고내역 시퀀스
	

	/** ATTACH DB model  */
	private String MODULE					= ""; //모듈 아이디
	private String ATTACH_GRP_NO			= ""; //파일그룹번호
	private String DEL_SEQ					= ""; //파일삭제번호
	
	
	/** CATEGORY DB model*/
	private String CATEGORY					= ""; //카테고리 ID
	private String CATEGORY_TYPE			= ""; //카테고리 구분
	private String TREE						= ""; //구조체
	private String DEPTH					= ""; //깊이
	
	
	/** OPT_COMCD_TB DB model*/
	private String COMCD_GRP				= ""; //공통코드그룹
	private String COMCD					= ""; //공통코드
	private String COMCD_NM					= ""; //공통코드명
	private String COMCD_DESC				= ""; //공통코드설명
	private String COMCD_ORD				= ""; //공통코드순서
	private String REMARKS					= ""; //비고(공장파트구분 01:파트구분,02:파트통합)
	private String SEQ_EQUIPMENT_TROUBLE1	= ""; //고장현상시퀀스
	private String SEQ_EQUIPMENT_TROUBLE2	= ""; //고장원인시퀀스

	
	/** query result DB model */
	private String member_name				= ""; //담당자명
	private String location_name			= ""; //공장명
	private String category_name			= ""; //카테고리명
	private String category_org				= ""; //기존카테고리
	private String category_org_name		= ""; //기존카테고리명
	private String cd_material_type_name	= ""; //자재구분명(1:전기,2:기계)
	private String cd_unit_name				= ""; //단위명
	private String FILE_CNT					= ""; //첨부파일 개수
	private String history_cnt				= ""; //설비이력 개수
	private String stock_condition			= ""; //재고상태(1:적정,2:미만)
	private String request_div_name			= ""; //요청부서명
	private String request_member_name		= ""; //요청자명
	private String appr_member_name			= ""; //결재자명
	private String cancel_member_name		= ""; //취소자명
	private String part_name				= ""; //파트명
	private String appr_status_name			= ""; //결재상태명
	private String repair_status_name		= ""; //정비상태명
	private String repair_type_name			= ""; //정비유형명
	private String repair_member_name		= ""; //정비원명
	private String analysis_member_name		= ""; //정비원명
	private String material_used_name		= ""; //사용자재명
	private String maintenance_plan			= ""; //일별 예방 점검 횟수
	private String incomplete				= ""; //미완료 건
	private String complete					= ""; //완료 건
	private String progress					= ""; //실시율
	private String file_name				= ""; //파일명
	private String trouble_time_hour		= ""; //고장시간(시간)
	private String trouble_time_minute		= ""; //고장시간(분)
	private String location_nm				= ""; //위치 명
	private String date_request_day			= ""; //요청일(날짜)
	private String date_request_time		= ""; //요청일(시간)
	private String repair_total_cnt			= ""; //정비총횟수
	private String repair_cnt				= ""; //정비횟수(돌발만)
	private String maintenance_name			= ""; //보전 구분 명
	private String locationpart				= ""; //공장파트구분(2100_01,2100_02,...)
	
	/** parameter model */
	private String attachExistYn			= ""; //첨부파일 유무
	private String flag						= ""; //버튼 선택(1:등록(수정),2:삭제)
	private String appr_flag				= ""; //정비요청 자동결재 상태
	private String memberArr				= ""; //정비원목록(정비실적 등록,수정시)
	private String equipmentNameArr			= ""; //설비명목록(qr코드 인쇄시)
	private String equipmentUidArr			= ""; //설비코드목록(qr코드 인쇄시)
	private String modelArr					= ""; //모델명목록(qr코드 인쇄시)
	private String manufacturerArr			= ""; //제조사목록(qr코드 인쇄시)
	private String equipmentBomArr			= ""; //bom목록(정비실적 등록,수정시)
	private String materialArr				= ""; //자재목록(정비실적 등록,수정시)
	private String qntyArr					= ""; //수량목록(정비실적 등록,수정시)
	private String equipmentBomUsedArr		= ""; //bom목록(정비실적 수정시)
	private String materialUsedArr			= ""; //자재목록(정비실적 수정시)
	private String qntyUsedArr				= ""; //수량목록(정비실적 수정시)
	private String regTerm					= ""; //일괄등록 간격
	private String period					= ""; //고장분석기준 : 기간
	private String frequency				= ""; //고장분석기준 : 횟수
	private String editYn					= ""; //수정가능 여부(Y:가능,N:불가)
	private String srcType					= ""; //검색구분
	private String CHECK_INOUT				= ""; //출퇴근 여부
	private String GRID_FLAG				= "";
	private String AuthType					= ""; //권한여부
	
	public String getRequestReport() {
		return requestReport;
	}
	public void setRequestReport(String requestReport) {
		this.requestReport = requestReport;
	}
	public String getGRID_FLAG() {
		return GRID_FLAG;
	}
	public void setGRID_FLAG(String gRID_FLAG) {
		GRID_FLAG = gRID_FLAG;
	}
	public String getDEFAULT_MANAGER() {
		return DEFAULT_MANAGER;
	}
	public void setDEFAULT_MANAGER(String dEFAULT_MANAGER) {
		DEFAULT_MANAGER = dEFAULT_MANAGER;
	}
	public String getMANAGER_NAME() {
		return MANAGER_NAME;
	}
	public void setMANAGER_NAME(String mANAGER_NAME) {
		MANAGER_NAME = mANAGER_NAME;
	}
	public String getREQUEST_PART() {
		return REQUEST_PART;
	}
	public void setREQUEST_PART(String rEQUEST_PART) {
		REQUEST_PART = rEQUEST_PART;
	}
	public String getREG_ID() {
		return REG_ID;
	}
	public void setREG_ID(String rEG_ID) {
		REG_ID = rEG_ID;
	}
	public String getREQUEST_MANAGER() {
		return REQUEST_MANAGER;
	}
	public void setREQUEST_MANAGER(String rEQUEST_MANAGER) {
		REQUEST_MANAGER = rEQUEST_MANAGER;
	}
	public String getMaterialUsedArrJSON() {
		return materialUsedArrJSON;
	}
	public void setMaterialUsedArrJSON(String materialUsedArrJSON) {
		this.materialUsedArrJSON = materialUsedArrJSON;
	}
	public String getMaterialArrJSON() {
		return materialArrJSON;
	}
	public void setMaterialArrJSON(String materialArrJSON) {
		this.materialArrJSON = materialArrJSON;
	}
	public String getDEL_SEQ() {
		return DEL_SEQ;
	}
	public void setDEL_SEQ(String dEL_SEQ) {
		DEL_SEQ = dEL_SEQ;
	}
	public String getATTACH_GRP_NO() {
		return ATTACH_GRP_NO;
	}
	public void setATTACH_GRP_NO(String aTTACH_GRP_NO) {
		ATTACH_GRP_NO = aTTACH_GRP_NO;
	}
	public String getFILE_CNT() {
		return FILE_CNT;
	}
	public void setFILE_CNT(String fILE_CNT) {
		FILE_CNT = fILE_CNT;
	}
	public String getPREVENTIVE_TYPE() {
		return PREVENTIVE_TYPE;
	}
	public void setPREVENTIVE_TYPE(String pREVENTIVE_TYPE) {
		PREVENTIVE_TYPE = pREVENTIVE_TYPE;
	}
	public String getPREVENTIVE_TYPE_NAME() {
		return PREVENTIVE_TYPE_NAME;
	}
	public void setPREVENTIVE_TYPE_NAME(String pREVENTIVE_TYPE_NAME) {
		PREVENTIVE_TYPE_NAME = pREVENTIVE_TYPE_NAME;
	}
	public String getSTATUS_NAME() {
		return STATUS_NAME;
	}
	public void setSTATUS_NAME(String sTATUS_NAME) {
		STATUS_NAME = sTATUS_NAME;
	}
	public String getCHECK_TYPE() {
		return CHECK_TYPE;
	}
	public void setCHECK_TYPE(String cHECK_TYPE) {
		CHECK_TYPE = cHECK_TYPE;
	}
	public String getCHECK_TYPE_NAME() {
		return CHECK_TYPE_NAME;
	}
	public void setCHECK_TYPE_NAME(String cHECK_TYPE_NAME) {
		CHECK_TYPE_NAME = cHECK_TYPE_NAME;
	}
	public String getPREVENTIVE_RESULT() {
		return PREVENTIVE_RESULT;
	}
	public void setPREVENTIVE_RESULT(String pREVENTIVE_RESULT) {
		PREVENTIVE_RESULT = pREVENTIVE_RESULT;
	}
	public String getPREVENTIVE_BOM() {
		return PREVENTIVE_BOM;
	}
	public void setPREVENTIVE_BOM(String pREVENTIVE_BOM) {
		PREVENTIVE_BOM = pREVENTIVE_BOM;
	}
	public String getDATE_CHECK() {
		return DATE_CHECK;
	}
	public void setDATE_CHECK(String dATE_CHECK) {
		DATE_CHECK = dATE_CHECK;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getCHECK_DECISION() {
		return CHECK_DECISION;
	}
	public void setCHECK_DECISION(String cHECK_DECISION) {
		CHECK_DECISION = cHECK_DECISION;
	}
	public String getCHECK_VALUE() {
		return CHECK_VALUE;
	}
	public void setCHECK_VALUE(String cHECK_VALUE) {
		CHECK_VALUE = cHECK_VALUE;
	}
	public String getCHECK_DESCRIPTION() {
		return CHECK_DESCRIPTION;
	}
	public void setCHECK_DESCRIPTION(String cHECK_DESCRIPTION) {
		CHECK_DESCRIPTION = cHECK_DESCRIPTION;
	}
	public String getSUCCESS_YN() {
		return SUCCESS_YN;
	}
	public void setSUCCESS_YN(String sUCCESS_YN) {
		SUCCESS_YN = sUCCESS_YN;
	}
	public String getEQUIPMENT() {
		return EQUIPMENT;
	}
	public void setEQUIPMENT(String eQUIPMENT) {
		EQUIPMENT = eQUIPMENT;
	}
	public String getEQUIPMENT_UID() {
		return EQUIPMENT_UID;
	}
	public void setEQUIPMENT_UID(String eQUIPMENT_UID) {
		EQUIPMENT_UID = eQUIPMENT_UID;
	}
	public String getEQUIPMENT_NAME() {
		return EQUIPMENT_NAME;
	}
	public void setEQUIPMENT_NAME(String eQUIPMENT_NAME) {
		EQUIPMENT_NAME = eQUIPMENT_NAME;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getGRADE() {
		return GRADE;
	}
	public void setGRADE(String gRADE) {
		GRADE = gRADE;
	}
	public String getASSET_NO() {
		return ASSET_NO;
	}
	public void setASSET_NO(String aSSET_NO) {
		ASSET_NO = aSSET_NO;
	}
	public String getMODEL() {
		return MODEL;
	}
	public void setMODEL(String mODEL) {
		MODEL = mODEL;
	}
	public String getDIMENSION() {
		return DIMENSION;
	}
	public void setDIMENSION(String dIMENSION) {
		DIMENSION = dIMENSION;
	}
	public String getWEIGHT() {
		return WEIGHT;
	}
	public void setWEIGHT(String wEIGHT) {
		WEIGHT = wEIGHT;
	}
	public String getCD_WEIGHT() {
		return CD_WEIGHT;
	}
	public void setCD_WEIGHT(String cD_WEIGHT) {
		CD_WEIGHT = cD_WEIGHT;
	}
	public String getACQUISITION_VALUE() {
		return ACQUISITION_VALUE;
	}
	public void setACQUISITION_VALUE(String aCQUISITION_VALUE) {
		ACQUISITION_VALUE = aCQUISITION_VALUE;
	}
	public String getCURRENCY() {
		return CURRENCY;
	}
	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}
	public String getCOUNTRY() {
		return COUNTRY;
	}
	public void setCOUNTRY(String cOUNTRY) {
		COUNTRY = cOUNTRY;
	}
	public String getDATE_ACQUISITION() {
		return DATE_ACQUISITION;
	}
	public void setDATE_ACQUISITION(String dATE_ACQUISITION) {
		DATE_ACQUISITION = dATE_ACQUISITION;
	}
	public String getDATE_INSTALL() {
		return DATE_INSTALL;
	}
	public void setDATE_INSTALL(String dATE_INSTALL) {
		DATE_INSTALL = dATE_INSTALL;
	}
	public String getDATE_OPERATE() {
		return DATE_OPERATE;
	}
	public void setDATE_OPERATE(String dATE_OPERATE) {
		DATE_OPERATE = dATE_OPERATE;
	}
	public String getDATE_VALID() {
		return DATE_VALID;
	}
	public void setDATE_VALID(String dATE_VALID) {
		DATE_VALID = dATE_VALID;
	}
	public String getLINE() {
		return LINE;
	}
	public void setLINE(String lINE) {
		LINE = lINE;
	}
	public String getFIELD() {
		return FIELD;
	}
	public void setFIELD(String fIELD) {
		FIELD = fIELD;
	}
	public String getMANUFACTURER() {
		return MANUFACTURER;
	}
	public void setMANUFACTURER(String mANUFACTURER) {
		MANUFACTURER = mANUFACTURER;
	}
	public String getCOST_CENTER() {
		return COST_CENTER;
	}
	public void setCOST_CENTER(String cOST_CENTER) {
		COST_CENTER = cOST_CENTER;
	}
	public String getINFORMATION1() {
		return INFORMATION1;
	}
	public void setINFORMATION1(String iNFORMATION1) {
		INFORMATION1 = iNFORMATION1;
	}
	public String getINFORMATION2() {
		return INFORMATION2;
	}
	public void setINFORMATION2(String iNFORMATION2) {
		INFORMATION2 = iNFORMATION2;
	}
	public String getDESCRIPTION() {
		return DESCRIPTION;
	}
	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
	public String getDATE_C() {
		return DATE_C;
	}
	public void setDATE_C(String dATE_C) {
		DATE_C = dATE_C;
	}
	public String getDATE_E() {
		return DATE_E;
	}
	public void setDATE_E(String dATE_E) {
		DATE_E = dATE_E;
	}
	public String getMEMBER() {
		return MEMBER;
	}
	public void setMEMBER(String mEMBER) {
		MEMBER = mEMBER;
	}
	public String getCD_DEL() {
		return CD_DEL;
	}
	public void setCD_DEL(String cD_DEL) {
		CD_DEL = cD_DEL;
	}
	public String getCD_DEL1() {
		return CD_DEL1;
	}
	public void setCD_DEL1(String cD_DEL1) {
		CD_DEL1 = cD_DEL1;
	}
	public String getCD_DEL2() {
		return CD_DEL2;
	}
	public void setCD_DEL2(String cD_DEL2) {
		CD_DEL2 = cD_DEL2;
	}
	public String getCD_DEL3() {
		return CD_DEL3;
	}
	public void setCD_DEL3(String cD_DEL3) {
		CD_DEL3 = cD_DEL3;
	}
	public String getSEQ_EQUIPMENT() {
		return SEQ_EQUIPMENT;
	}
	public void setSEQ_EQUIPMENT(String sEQ_EQUIPMENT) {
		SEQ_EQUIPMENT = sEQ_EQUIPMENT;
	}
	public String getMATERIAL_GRP() {
		return MATERIAL_GRP;
	}
	public void setMATERIAL_GRP(String mATERIAL_GRP) {
		MATERIAL_GRP = mATERIAL_GRP;
	}
	public String getMATERIAL_GRP_UID() {
		return MATERIAL_GRP_UID;
	}
	public void setMATERIAL_GRP_UID(String mATERIAL_GRP_UID) {
		MATERIAL_GRP_UID = mATERIAL_GRP_UID;
	}
	public String getMATERIAL_GRP_NAME() {
		return MATERIAL_GRP_NAME;
	}
	public void setMATERIAL_GRP_NAME(String mATERIAL_GRP_NAME) {
		MATERIAL_GRP_NAME = mATERIAL_GRP_NAME;
	}
	public String getSEQ_MATERIAL_GRP() {
		return SEQ_MATERIAL_GRP;
	}
	public void setSEQ_MATERIAL_GRP(String sEQ_MATERIAL_GRP) {
		SEQ_MATERIAL_GRP = sEQ_MATERIAL_GRP;
	}
	public String getEQUIPMENT_BOM() {
		return EQUIPMENT_BOM;
	}
	public void setEQUIPMENT_BOM(String eQUIPMENT_BOM) {
		EQUIPMENT_BOM = eQUIPMENT_BOM;
	}
	public String getSEQ_EQUIPMENT_BOM() {
		return SEQ_EQUIPMENT_BOM;
	}
	public void setSEQ_EQUIPMENT_BOM(String sEQ_EQUIPMENT_BOM) {
		SEQ_EQUIPMENT_BOM = sEQ_EQUIPMENT_BOM;
	}
	public String getEQUIPMENT_HISTORY() {
		return EQUIPMENT_HISTORY;
	}
	public void setEQUIPMENT_HISTORY(String eQUIPMENT_HISTORY) {
		EQUIPMENT_HISTORY = eQUIPMENT_HISTORY;
	}
	public String getHISTORY_TYPE() {
		return HISTORY_TYPE;
	}
	public void setHISTORY_TYPE(String hISTORY_TYPE) {
		HISTORY_TYPE = hISTORY_TYPE;
	}
	public String getDATE_HISTORY() {
		return DATE_HISTORY;
	}
	public void setDATE_HISTORY(String dATE_HISTORY) {
		DATE_HISTORY = dATE_HISTORY;
	}
	public String getHISTORY_DESCRIPTION() {
		return HISTORY_DESCRIPTION;
	}
	public void setHISTORY_DESCRIPTION(String hISTORY_DESCRIPTION) {
		HISTORY_DESCRIPTION = hISTORY_DESCRIPTION;
	}
	public String getDETAIL_ID() {
		return DETAIL_ID;
	}
	public void setDETAIL_ID(String dETAIL_ID) {
		DETAIL_ID = dETAIL_ID;
	}
	public String getSEQ_EQUIPMENT_HISTORY() {
		return SEQ_EQUIPMENT_HISTORY;
	}
	public void setSEQ_EQUIPMENT_HISTORY(String sEQ_EQUIPMENT_HISTORY) {
		SEQ_EQUIPMENT_HISTORY = sEQ_EQUIPMENT_HISTORY;
	}
	public String getREPAIR_REQUEST() {
		return REPAIR_REQUEST;
	}
	public void setREPAIR_REQUEST(String rEPAIR_REQUEST) {
		REPAIR_REQUEST = rEPAIR_REQUEST;
	}
	public String getREQUEST_TYPE() {
		return REQUEST_TYPE;
	}
	public void setREQUEST_TYPE(String rEQUEST_TYPE) {
		REQUEST_TYPE = rEQUEST_TYPE;
	}
	public String getREPAIR_REQUEST_ORG() {
		return REPAIR_REQUEST_ORG;
	}
	public void setREPAIR_REQUEST_ORG(String rEPAIR_REQUEST_ORG) {
		REPAIR_REQUEST_ORG = rEPAIR_REQUEST_ORG;
	}
	public String getAPPR_STATUS() {
		return APPR_STATUS;
	}
	public void setAPPR_STATUS(String aPPR_STATUS) {
		APPR_STATUS = aPPR_STATUS;
	}
	public String getREQUEST_DESCRIPTION() {
		return REQUEST_DESCRIPTION;
	}
	public void setREQUEST_DESCRIPTION(String rEQUEST_DESCRIPTION) {
		REQUEST_DESCRIPTION = rEQUEST_DESCRIPTION;
	}
	public String getREQUEST_DIV() {
		return REQUEST_DIV;
	}
	public void setREQUEST_DIV(String rEQUEST_DIV) {
		REQUEST_DIV = rEQUEST_DIV;
	}
	public String getDATE_REQUEST() {
		return DATE_REQUEST;
	}
	public void setDATE_REQUEST(String dATE_REQUEST) {
		DATE_REQUEST = dATE_REQUEST;
	}
	public String getREQUEST_MEMBER() {
		return REQUEST_MEMBER;
	}
	public void setREQUEST_MEMBER(String rEQUEST_MEMBER) {
		REQUEST_MEMBER = rEQUEST_MEMBER;
	}
	public String getDATE_APPR() {
		return DATE_APPR;
	}
	public void setDATE_APPR(String dATE_APPR) {
		DATE_APPR = dATE_APPR;
	}
	public String getAPPR_MEMBER() {
		return APPR_MEMBER;
	}
	public void setAPPR_MEMBER(String aPPR_MEMBER) {
		APPR_MEMBER = aPPR_MEMBER;
	}
	public String getREJECT_DESCRIPTION() {
		return REJECT_DESCRIPTION;
	}
	public void setREJECT_DESCRIPTION(String rEJECT_DESCRIPTION) {
		REJECT_DESCRIPTION = rEJECT_DESCRIPTION;
	}
	public String getDATE_CANCEL() {
		return DATE_CANCEL;
	}
	public void setDATE_CANCEL(String dATE_CANCEL) {
		DATE_CANCEL = dATE_CANCEL;
	}
	public String getCANCEL_MEMBER() {
		return CANCEL_MEMBER;
	}
	public void setCANCEL_MEMBER(String cANCEL_MEMBER) {
		CANCEL_MEMBER = cANCEL_MEMBER;
	}
	public String getSEQ_REPAIR_REQUEST() {
		return SEQ_REPAIR_REQUEST;
	}
	public void setSEQ_REPAIR_REQUEST(String sEQ_REPAIR_REQUEST) {
		SEQ_REPAIR_REQUEST = sEQ_REPAIR_REQUEST;
	}
	public String getE_AUFNR() {
		return E_AUFNR;
	}
	public void setE_AUFNR(String e_AUFNR) {
		E_AUFNR = e_AUFNR;
	}
	public String getE_RESULT() {
		return E_RESULT;
	}
	public void setE_RESULT(String e_RESULT) {
		E_RESULT = e_RESULT;
	}
	public String getREPAIR_ANALYSIS() {
		return REPAIR_ANALYSIS;
	}
	public void setREPAIR_ANALYSIS(String rEPAIR_ANALYSIS) {
		REPAIR_ANALYSIS = rEPAIR_ANALYSIS;
	}
	public String getDATE_ANALYSIS() {
		return DATE_ANALYSIS;
	}
	public void setDATE_ANALYSIS(String dATE_ANALYSIS) {
		DATE_ANALYSIS = dATE_ANALYSIS;
	}
	public String getLOSS() {
		return LOSS;
	}
	public void setLOSS(String lOSS) {
		LOSS = lOSS;
	}
	public String getREASON1() {
		return REASON1;
	}
	public void setREASON1(String rEASON1) {
		REASON1 = rEASON1;
	}
	public String getMEASURE1() {
		return MEASURE1;
	}
	public void setMEASURE1(String mEASURE1) {
		MEASURE1 = mEASURE1;
	}
	public String getREASON2() {
		return REASON2;
	}
	public void setREASON2(String rEASON2) {
		REASON2 = rEASON2;
	}
	public String getMEASURE2() {
		return MEASURE2;
	}
	public void setMEASURE2(String mEASURE2) {
		MEASURE2 = mEASURE2;
	}
	public String getREASON3() {
		return REASON3;
	}
	public void setREASON3(String rEASON3) {
		REASON3 = rEASON3;
	}
	public String getMEASURE3() {
		return MEASURE3;
	}
	public void setMEASURE3(String mEASURE3) {
		MEASURE3 = mEASURE3;
	}
	public String getSAVE_YN() {
		return SAVE_YN;
	}
	public void setSAVE_YN(String sAVE_YN) {
		SAVE_YN = sAVE_YN;
	}
	public String getSEQ_REPAIR_ANALYSIS() {
		return SEQ_REPAIR_ANALYSIS;
	}
	public void setSEQ_REPAIR_ANALYSIS(String sEQ_REPAIR_ANALYSIS) {
		SEQ_REPAIR_ANALYSIS = sEQ_REPAIR_ANALYSIS;
	}
	public String getREPAIR() {
		return REPAIR;
	}
	public void setREPAIR(String rEPAIR) {
		REPAIR = rEPAIR;
	}
	public String getREPAIR_TYPE() {
		return REPAIR_TYPE;
	}
	public void setREPAIR_TYPE(String rEPAIR_TYPE) {
		REPAIR_TYPE = rEPAIR_TYPE;
	}
	public String getREPAIR_STATUS() {
		return REPAIR_STATUS;
	}
	public void setREPAIR_STATUS(String rEPAIR_STATUS) {
		REPAIR_STATUS = rEPAIR_STATUS;
	}
	public String getDATE_ASSIGN() {
		return DATE_ASSIGN;
	}
	public void setDATE_ASSIGN(String dATE_ASSIGN) {
		DATE_ASSIGN = dATE_ASSIGN;
	}
	public String getDATE_PLAN() {
		return DATE_PLAN;
	}
	public void setDATE_PLAN(String dATE_PLAN) {
		DATE_PLAN = dATE_PLAN;
	}
	public String getDATE_REPAIR() {
		return DATE_REPAIR;
	}
	public void setDATE_REPAIR(String dATE_REPAIR) {
		DATE_REPAIR = dATE_REPAIR;
	}
	public String getTROUBLE_TYPE1() {
		return TROUBLE_TYPE1;
	}
	public void setTROUBLE_TYPE1(String tROUBLE_TYPE1) {
		TROUBLE_TYPE1 = tROUBLE_TYPE1;
	}
	public String getTROUBLE_DESCRIPTION1() {
		return TROUBLE_DESCRIPTION1;
	}
	public void setTROUBLE_DESCRIPTION1(String tROUBLE_DESCRIPTION1) {
		TROUBLE_DESCRIPTION1 = tROUBLE_DESCRIPTION1;
	}
	public String getTROUBLE_TYPE2() {
		return TROUBLE_TYPE2;
	}
	public void setTROUBLE_TYPE2(String tROUBLE_TYPE2) {
		TROUBLE_TYPE2 = tROUBLE_TYPE2;
	}
	public String getTROUBLE_DESCRIPTION2() {
		return TROUBLE_DESCRIPTION2;
	}
	public void setTROUBLE_DESCRIPTION2(String tROUBLE_DESCRIPTION2) {
		TROUBLE_DESCRIPTION2 = tROUBLE_DESCRIPTION2;
	}
	public String getMANAGER() {
		return MANAGER;
	}
	public void setMANAGER(String mANAGER) {
		MANAGER = mANAGER;
	}
	public String getREPAIR_MEMBER() {
		return REPAIR_MEMBER;
	}
	public void setREPAIR_MEMBER(String rEPAIR_MEMBER) {
		REPAIR_MEMBER = rEPAIR_MEMBER;
	}
	public String getOUTSOURCING() {
		return OUTSOURCING;
	}
	public void setOUTSOURCING(String oUTSOURCING) {
		OUTSOURCING = oUTSOURCING;
	}
	public String getMATERIAL_UNREG() {
		return MATERIAL_UNREG;
	}
	public void setMATERIAL_UNREG(String mATERIAL_UNREG) {
		MATERIAL_UNREG = mATERIAL_UNREG;
	}
	public String getTROUBLE_TIME1() {
		return TROUBLE_TIME1;
	}
	public void setTROUBLE_TIME1(String tROUBLE_TIME1) {
		TROUBLE_TIME1 = tROUBLE_TIME1;
	}
	public String getTROUBLE_TIME2() {
		return TROUBLE_TIME2;
	}
	public void setTROUBLE_TIME2(String tROUBLE_TIME2) {
		TROUBLE_TIME2 = tROUBLE_TIME2;
	}
	public String getREPAIR_DESCRIPTION() {
		return REPAIR_DESCRIPTION;
	}
	public void setREPAIR_DESCRIPTION(String rEPAIR_DESCRIPTION) {
		REPAIR_DESCRIPTION = rEPAIR_DESCRIPTION;
	}
	public String getSEQ_REPAIR() {
		return SEQ_REPAIR;
	}
	public void setSEQ_REPAIR(String sEQ_REPAIR) {
		SEQ_REPAIR = sEQ_REPAIR;
	}
	public String getMAINTENANCE_BOM() {
		return MAINTENANCE_BOM;
	}
	public void setMAINTENANCE_BOM(String mAINTENANCE_BOM) {
		MAINTENANCE_BOM = mAINTENANCE_BOM;
	}
	public String getPART() {
		return PART;
	}
	public void setPART(String pART) {
		PART = pART;
	}
	public String getMAINTENANCE_TYPE() {
		return MAINTENANCE_TYPE;
	}
	public void setMAINTENANCE_TYPE(String mAINTENANCE_TYPE) {
		MAINTENANCE_TYPE = mAINTENANCE_TYPE;
	}
	public String getCHECK_ITEM() {
		return CHECK_ITEM;
	}
	public void setCHECK_ITEM(String cHECK_ITEM) {
		CHECK_ITEM = cHECK_ITEM;
	}
	public String getCHECK_STANDARD() {
		return CHECK_STANDARD;
	}
	public void setCHECK_STANDARD(String cHECK_STANDARD) {
		CHECK_STANDARD = cHECK_STANDARD;
	}
	public String getCHECK_TOOL() {
		return CHECK_TOOL;
	}
	public void setCHECK_TOOL(String cHECK_TOOL) {
		CHECK_TOOL = cHECK_TOOL;
	}
	public String getCHECK_TYPE1() {
		return CHECK_TYPE1;
	}
	public void setCHECK_TYPE1(String cHECK_TYPE1) {
		CHECK_TYPE1 = cHECK_TYPE1;
	}
	public String getMAINTENANCE_STANDARD() {
		return MAINTENANCE_STANDARD;
	}
	public void setMAINTENANCE_STANDARD(String mAINTENANCE_STANDARD) {
		MAINTENANCE_STANDARD = mAINTENANCE_STANDARD;
	}
	public String getCHECK_TYPE2() {
		return CHECK_TYPE2;
	}
	public void setCHECK_TYPE2(String cHECK_TYPE2) {
		CHECK_TYPE2 = cHECK_TYPE2;
	}
	public String getCHECK_CYCLE() {
		return CHECK_CYCLE;
	}
	public void setCHECK_CYCLE(String cHECK_CYCLE) {
		CHECK_CYCLE = cHECK_CYCLE;
	}
	public String getSEQ_MAINTENANCE_BOM() {
		return SEQ_MAINTENANCE_BOM;
	}
	public void setSEQ_MAINTENANCE_BOM(String sEQ_MAINTENANCE_BOM) {
		SEQ_MAINTENANCE_BOM = sEQ_MAINTENANCE_BOM;
	}
	public String getWORK_LOG() {
		return WORK_LOG;
	}
	public void setWORK_LOG(String wORK_LOG) {
		WORK_LOG = wORK_LOG;
	}
	public String getWORK_TYPE() {
		return WORK_TYPE;
	}
	public void setWORK_TYPE(String wORK_TYPE) {
		WORK_TYPE = wORK_TYPE;
	}
	public String getWORK_ID() {
		return WORK_ID;
	}
	public void setWORK_ID(String wORK_ID) {
		WORK_ID = wORK_ID;
	}
	public String getSEQ_WORK_LOG() {
		return SEQ_WORK_LOG;
	}
	public void setSEQ_WORK_LOG(String sEQ_WORK_LOG) {
		SEQ_WORK_LOG = sEQ_WORK_LOG;
	}
	public String getMAINTENANCE() {
		return MAINTENANCE;
	}
	public void setMAINTENANCE(String mAINTENANCE) {
		MAINTENANCE = mAINTENANCE;
	}
	public String getMAINTENANCE_STATUS() {
		return MAINTENANCE_STATUS;
	}
	public void setMAINTENANCE_STATUS(String mAINTENANCE_STATUS) {
		MAINTENANCE_STATUS = mAINTENANCE_STATUS;
	}
	public String getDATE_MAINTENANCE() {
		return DATE_MAINTENANCE;
	}
	public void setDATE_MAINTENANCE(String dATE_MAINTENANCE) {
		DATE_MAINTENANCE = dATE_MAINTENANCE;
	}
	public String getMEASURED_VALUE() {
		return MEASURED_VALUE;
	}
	public void setMEASURED_VALUE(String mEASURED_VALUE) {
		MEASURED_VALUE = mEASURED_VALUE;
	}
	public String getMAINTENANCE_DECISION() {
		return MAINTENANCE_DECISION;
	}
	public void setMAINTENANCE_DECISION(String mAINTENANCE_DECISION) {
		MAINTENANCE_DECISION = mAINTENANCE_DECISION;
	}
	public String getMAINTENANCE_DESCRIPTION() {
		return MAINTENANCE_DESCRIPTION;
	}
	public void setMAINTENANCE_DESCRIPTION(String mAINTENANCE_DESCRIPTION) {
		MAINTENANCE_DESCRIPTION = mAINTENANCE_DESCRIPTION;
	}
	public String getSEQ_MAINTENANCE() {
		return SEQ_MAINTENANCE;
	}
	public void setSEQ_MAINTENANCE(String sEQ_MAINTENANCE) {
		SEQ_MAINTENANCE = sEQ_MAINTENANCE;
	}
	public String getMATERIAL() {
		return MATERIAL;
	}
	public void setMATERIAL(String mATERIAL) {
		MATERIAL = mATERIAL;
	}
	public String getCD_MATERIAL_TYPE() {
		return CD_MATERIAL_TYPE;
	}
	public void setCD_MATERIAL_TYPE(String cD_MATERIAL_TYPE) {
		CD_MATERIAL_TYPE = cD_MATERIAL_TYPE;
	}
	public String getCD_MATERIAL_GRP1() {
		return CD_MATERIAL_GRP1;
	}
	public void setCD_MATERIAL_GRP1(String cD_MATERIAL_GRP1) {
		CD_MATERIAL_GRP1 = cD_MATERIAL_GRP1;
	}
	public String getCD_MATERIAL_GRP2() {
		return CD_MATERIAL_GRP2;
	}
	public void setCD_MATERIAL_GRP2(String cD_MATERIAL_GRP2) {
		CD_MATERIAL_GRP2 = cD_MATERIAL_GRP2;
	}
	public String getCD_MATERIAL_GRP3() {
		return CD_MATERIAL_GRP3;
	}
	public void setCD_MATERIAL_GRP3(String cD_MATERIAL_GRP3) {
		CD_MATERIAL_GRP3 = cD_MATERIAL_GRP3;
	}
	public String getMATERIAL_UID() {
		return MATERIAL_UID;
	}
	public void setMATERIAL_UID(String mATERIAL_UID) {
		MATERIAL_UID = mATERIAL_UID;
	}
	public String getMATERIAL_NAME() {
		return MATERIAL_NAME;
	}
	public void setMATERIAL_NAME(String mATERIAL_NAME) {
		MATERIAL_NAME = mATERIAL_NAME;
	}
	public String getFUNCTION() {
		return FUNCTION;
	}
	public void setFUNCTION(String fUNCTION) {
		FUNCTION = fUNCTION;
	}
	public String getSPEC() {
		return SPEC;
	}
	public void setSPEC(String sPEC) {
		SPEC = sPEC;
	}
	public String getMEMO() {
		return MEMO;
	}
	public void setMEMO(String mEMO) {
		MEMO = mEMO;
	}
	public String getCD_UNIT() {
		return CD_UNIT;
	}
	public void setCD_UNIT(String cD_UNIT) {
		CD_UNIT = cD_UNIT;
	}
	public String getSTOCK() {
		return STOCK;
	}
	public void setSTOCK(String sTOCK) {
		STOCK = sTOCK;
	}
	public String getSTOCK_OPTIMAL() {
		return STOCK_OPTIMAL;
	}
	public void setSTOCK_OPTIMAL(String sTOCK_OPTIMAL) {
		STOCK_OPTIMAL = sTOCK_OPTIMAL;
	}
	public String getMATERIAL_INOUT() {
		return MATERIAL_INOUT;
	}
	public void setMATERIAL_INOUT(String mATERIAL_INOUT) {
		MATERIAL_INOUT = mATERIAL_INOUT;
	}
	public String getCD_MATERIAL_INOUT_TYPE() {
		return CD_MATERIAL_INOUT_TYPE;
	}
	public void setCD_MATERIAL_INOUT_TYPE(String cD_MATERIAL_INOUT_TYPE) {
		CD_MATERIAL_INOUT_TYPE = cD_MATERIAL_INOUT_TYPE;
	}
	public String getDATE_INOUT() {
		return DATE_INOUT;
	}
	public void setDATE_INOUT(String dATE_INOUT) {
		DATE_INOUT = dATE_INOUT;
	}
	public String getQNTY() {
		return QNTY;
	}
	public void setQNTY(String qNTY) {
		QNTY = qNTY;
	}
	public String getPRICE() {
		return PRICE;
	}
	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}
	public String getSEQ_MATERIAL_INOUT_ITEM() {
		return SEQ_MATERIAL_INOUT_ITEM;
	}
	public void setSEQ_MATERIAL_INOUT_ITEM(String sEQ_MATERIAL_INOUT_ITEM) {
		SEQ_MATERIAL_INOUT_ITEM = sEQ_MATERIAL_INOUT_ITEM;
	}
	public String getMODULE() {
		return MODULE;
	}
	public void setMODULE(String mODULE) {
		MODULE = mODULE;
	}
	public String getCATEGORY() {
		return CATEGORY;
	}
	public void setCATEGORY(String cATEGORY) {
		CATEGORY = cATEGORY;
	}
	
	public String getCATEGORY_TYPE() {
		return CATEGORY_TYPE;
	}
	public void setCATEGORY_TYPE(String cATEGORY_TYPE) {
		CATEGORY_TYPE = cATEGORY_TYPE;
	}
	public String getTREE() {
		return TREE;
	}
	public void setTREE(String tREE) {
		TREE = tREE;
	}
	public String getDEPTH() {
		return DEPTH;
	}
	public void setDEPTH(String dEPTH) {
		DEPTH = dEPTH;
	}
	public String getCOMCD_GRP() {
		return COMCD_GRP;
	}
	public void setCOMCD_GRP(String cOMCD_GRP) {
		COMCD_GRP = cOMCD_GRP;
	}
	public String getCOMCD() {
		return COMCD;
	}
	public void setCOMCD(String cOMCD) {
		COMCD = cOMCD;
	}
	public String getCOMCD_NM() {
		return COMCD_NM;
	}
	public void setCOMCD_NM(String cOMCD_NM) {
		COMCD_NM = cOMCD_NM;
	}
	public String getCOMCD_DESC() {
		return COMCD_DESC;
	}
	public void setCOMCD_DESC(String cOMCD_DESC) {
		COMCD_DESC = cOMCD_DESC;
	}
	public String getCOMCD_ORD() {
		return COMCD_ORD;
	}
	public void setCOMCD_ORD(String cOMCD_ORD) {
		COMCD_ORD = cOMCD_ORD;
	}
	public String getREMARKS() {
		return REMARKS;
	}
	public void setREMARKS(String rEMARKS) {
		REMARKS = rEMARKS;
	}
	public String getSEQ_EQUIPMENT_TROUBLE1() {
		return SEQ_EQUIPMENT_TROUBLE1;
	}
	public void setSEQ_EQUIPMENT_TROUBLE1(String sEQ_EQUIPMENT_TROUBLE1) {
		SEQ_EQUIPMENT_TROUBLE1 = sEQ_EQUIPMENT_TROUBLE1;
	}
	public String getSEQ_EQUIPMENT_TROUBLE2() {
		return SEQ_EQUIPMENT_TROUBLE2;
	}
	public void setSEQ_EQUIPMENT_TROUBLE2(String sEQ_EQUIPMENT_TROUBLE2) {
		SEQ_EQUIPMENT_TROUBLE2 = sEQ_EQUIPMENT_TROUBLE2;
	}
	public String getMember_name() {
		return member_name;
	}
	public void setMember_name(String member_name) {
		this.member_name = member_name;
	}
	public String getLocation_name() {
		return location_name;
	}
	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public String getCategory_org() {
		return category_org;
	}
	public void setCategory_org(String category_org) {
		this.category_org = category_org;
	}
	public String getCategory_org_name() {
		return category_org_name;
	}
	public void setCategory_org_name(String category_org_name) {
		this.category_org_name = category_org_name;
	}
	public String getCd_material_type_name() {
		return cd_material_type_name;
	}
	public void setCd_material_type_name(String cd_material_type_name) {
		this.cd_material_type_name = cd_material_type_name;
	}
	public String getCd_unit_name() {
		return cd_unit_name;
	}
	public void setCd_unit_name(String cd_unit_name) {
		this.cd_unit_name = cd_unit_name;
	}
	public String getHistory_cnt() {
		return history_cnt;
	}
	public void setHistory_cnt(String history_cnt) {
		this.history_cnt = history_cnt;
	}
	public String getStock_condition() {
		return stock_condition;
	}
	public void setStock_condition(String stock_condition) {
		this.stock_condition = stock_condition;
	}
	public String getRequest_div_name() {
		return request_div_name;
	}
	public void setRequest_div_name(String request_div_name) {
		this.request_div_name = request_div_name;
	}
	public String getRequest_member_name() {
		return request_member_name;
	}
	public void setRequest_member_name(String request_member_name) {
		this.request_member_name = request_member_name;
	}
	public String getAppr_member_name() {
		return appr_member_name;
	}
	public void setAppr_member_name(String appr_member_name) {
		this.appr_member_name = appr_member_name;
	}
	public String getCancel_member_name() {
		return cancel_member_name;
	}
	public void setCancel_member_name(String cancel_member_name) {
		this.cancel_member_name = cancel_member_name;
	}
	public String getPart_name() {
		return part_name;
	}
	public void setPart_name(String part_name) {
		this.part_name = part_name;
	}
	public String getAppr_status_name() {
		return appr_status_name;
	}
	public void setAppr_status_name(String appr_status_name) {
		this.appr_status_name = appr_status_name;
	}
	public String getRepair_status_name() {
		return repair_status_name;
	}
	public void setRepair_status_name(String repair_status_name) {
		this.repair_status_name = repair_status_name;
	}
	public String getRepair_type_name() {
		return repair_type_name;
	}
	public void setRepair_type_name(String repair_type_name) {
		this.repair_type_name = repair_type_name;
	}
	public String getRepair_member_name() {
		return repair_member_name;
	}
	public void setRepair_member_name(String repair_member_name) {
		this.repair_member_name = repair_member_name;
	}
	public String getAnalysis_member_name() {
		return analysis_member_name;
	}
	public void setAnalysis_member_name(String analysis_member_name) {
		this.analysis_member_name = analysis_member_name;
	}
	public String getMaterial_used_name() {
		return material_used_name;
	}
	public void setMaterial_used_name(String material_used_name) {
		this.material_used_name = material_used_name;
	}
	public String getMaintenance_plan() {
		return maintenance_plan;
	}
	public void setMaintenance_plan(String maintenance_plan) {
		this.maintenance_plan = maintenance_plan;
	}
	public String getIncomplete() {
		return incomplete;
	}
	public void setIncomplete(String incomplete) {
		this.incomplete = incomplete;
	}
	public String getComplete() {
		return complete;
	}
	public void setComplete(String complete) {
		this.complete = complete;
	}
	public String getProgress() {
		return progress;
	}
	public void setProgress(String progress) {
		this.progress = progress;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getTrouble_time_hour() {
		return trouble_time_hour;
	}
	public void setTrouble_time_hour(String trouble_time_hour) {
		this.trouble_time_hour = trouble_time_hour;
	}
	public String getTrouble_time_minute() {
		return trouble_time_minute;
	}
	public void setTrouble_time_minute(String trouble_time_minute) {
		this.trouble_time_minute = trouble_time_minute;
	}
	public String getLocation_nm() {
		return location_nm;
	}
	public void setLocation_nm(String location_nm) {
		this.location_nm = location_nm;
	}
	public String getDate_request_day() {
		return date_request_day;
	}
	public void setDate_request_day(String date_request_day) {
		this.date_request_day = date_request_day;
	}
	public String getDate_request_time() {
		return date_request_time;
	}
	public void setDate_request_time(String date_request_time) {
		this.date_request_time = date_request_time;
	}
	public String getRepair_total_cnt() {
		return repair_total_cnt;
	}
	public void setRepair_total_cnt(String repair_total_cnt) {
		this.repair_total_cnt = repair_total_cnt;
	}
	public String getRepair_cnt() {
		return repair_cnt;
	}
	public void setRepair_cnt(String repair_cnt) {
		this.repair_cnt = repair_cnt;
	}
	public String getMaintenance_name() {
		return maintenance_name;
	}
	public void setMaintenance_name(String maintenance_name) {
		this.maintenance_name = maintenance_name;
	}
	public String getLocationpart() {
		return locationpart;
	}
	public void setLocationpart(String locationpart) {
		this.locationpart = locationpart;
	}
	public String getAttachExistYn() {
		return attachExistYn;
	}
	public void setAttachExistYn(String attachExistYn) {
		this.attachExistYn = attachExistYn;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getMemberArr() {
		return memberArr;
	}
	public void setMemberArr(String memberArr) {
		this.memberArr = memberArr;
	}
	public String getEquipmentNameArr() {
		return equipmentNameArr;
	}
	public void setEquipmentNameArr(String equipmentNameArr) {
		this.equipmentNameArr = equipmentNameArr;
	}
	public String getEquipmentUidArr() {
		return equipmentUidArr;
	}
	public void setEquipmentUidArr(String equipmentUidArr) {
		this.equipmentUidArr = equipmentUidArr;
	}
	public String getModelArr() {
		return modelArr;
	}
	public void setModelArr(String modelArr) {
		this.modelArr = modelArr;
	}
	public String getManufacturerArr() {
		return manufacturerArr;
	}
	public void setManufacturerArr(String manufacturerArr) {
		this.manufacturerArr = manufacturerArr;
	}
	public String getEquipmentBomArr() {
		return equipmentBomArr;
	}
	public void setEquipmentBomArr(String equipmentBomArr) {
		this.equipmentBomArr = equipmentBomArr;
	}
	public String getMaterialArr() {
		return materialArr;
	}
	public void setMaterialArr(String materialArr) {
		this.materialArr = materialArr;
	}
	public String getQntyArr() {
		return qntyArr;
	}
	public void setQntyArr(String qntyArr) {
		this.qntyArr = qntyArr;
	}
	public String getEquipmentBomUsedArr() {
		return equipmentBomUsedArr;
	}
	public void setEquipmentBomUsedArr(String equipmentBomUsedArr) {
		this.equipmentBomUsedArr = equipmentBomUsedArr;
	}
	public String getMaterialUsedArr() {
		return materialUsedArr;
	}
	public void setMaterialUsedArr(String materialUsedArr) {
		this.materialUsedArr = materialUsedArr;
	}
	public String getQntyUsedArr() {
		return qntyUsedArr;
	}
	public void setQntyUsedArr(String qntyUsedArr) {
		this.qntyUsedArr = qntyUsedArr;
	}
	public String getRegTerm() {
		return regTerm;
	}
	public void setRegTerm(String regTerm) {
		this.regTerm = regTerm;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getEditYn() {
		return editYn;
	}
	public void setEditYn(String editYn) {
		this.editYn = editYn;
	}
	public String getSrcType() {
		return srcType;
	}
	public void setSrcType(String srcType) {
		this.srcType = srcType;
	}
	public String getCHECK_INOUT() {
		return CHECK_INOUT;
	}
	public void setCHECK_INOUT(String cHECK_INOUT) {
		CHECK_INOUT = cHECK_INOUT;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getAppr_flag() {
		return appr_flag;
	}
	public void setAppr_flag(String appr_flag) {
		this.appr_flag = appr_flag;
	}
	public String getAuthType() {
		return AuthType;
	}
	public void setAuthType(String authType) {
		AuthType = authType;
	}
	
}
