package aspn.hello.epms.preventive.plan.model;

import aspn.hello.com.model.AbstractVO;

/**
 * [예방보전Plan] VO Class
 * @author 이영탁
 * @since 2018.04.26
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.26			이영탁			최초 생성
 *   2018.05.09			김영환			VO정리
 *   
 * </pre>
 */

public class PreventivePlanVO extends AbstractVO{
	
private static final long serialVersionUID = 1L;

	/** MEMBER DB model  */	
	private String LOCATION					= ""; //공장
	private String PART						= ""; //파트
	
	/** COMCD DB model  */
	private String REMARKS					= ""; //비고(01:파트구분,02:파트통합)
	
	/** EPMS_PREVENTIVE_RESULT DB model  */
	private String DATE_ASSIGN				= ""; //배정일
	private String MANAGER					= ""; //담당자
	private String PREVENTIVE_RESULT		= ""; //예방보전ID
	private String PREVENTIVE_STATUS		= ""; //예방보전 상태
	private String CHECK_DECISION			= ""; //판정
	private String CHECK_TYPE				= ""; //점검구분
	
	/** parameter */
	private String authType			= "";	//조회권한
	
	/** EPMS_EQUIPMENT DB model */
	private String SUB_ROOT			= "";	//조회권한
	
	public String getCHECK_DECISION() {
		return CHECK_DECISION;
	}
	public void setCHECK_DECISION(String cHECK_DECISION) {
		CHECK_DECISION = cHECK_DECISION;
	}
	public String getCHECK_TYPE() {
		return CHECK_TYPE;
	}
	public void setCHECK_TYPE(String cHECK_TYPE) {
		CHECK_TYPE = cHECK_TYPE;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getPART() {
		return PART;
	}
	public void setPART(String pART) {
		PART = pART;
	}
	public String getREMARKS() {
		return REMARKS;
	}
	public void setREMARKS(String rEMARKS) {
		REMARKS = rEMARKS;
	}
	public String getDATE_ASSIGN() {
		return DATE_ASSIGN;
	}
	public void setDATE_ASSIGN(String dATE_ASSIGN) {
		DATE_ASSIGN = dATE_ASSIGN;
	}
	public String getMANAGER() {
		return MANAGER;
	}
	public void setMANAGER(String mANAGER) {
		MANAGER = mANAGER;
	}
	public String getPREVENTIVE_RESULT() {
		return PREVENTIVE_RESULT;
	}
	public void setPREVENTIVE_RESULT(String pREVENTIVE_RESULT) {
		PREVENTIVE_RESULT = pREVENTIVE_RESULT;
	}
	public String getPREVENTIVE_STATUS() {
		return PREVENTIVE_STATUS;
	}
	public void setPREVENTIVE_STATUS(String pREVENTIVE_STATUS) {
		PREVENTIVE_STATUS = pREVENTIVE_STATUS;
	}
	public String getAuthType() {
		return authType;
	}
	public void setAuthType(String authType) {
		this.authType = authType;
	}
	public String getSUB_ROOT() {
		return SUB_ROOT;
	}
	public void setSUB_ROOT(String sUB_ROOT) {
		SUB_ROOT = sUB_ROOT;
	}
	
}
