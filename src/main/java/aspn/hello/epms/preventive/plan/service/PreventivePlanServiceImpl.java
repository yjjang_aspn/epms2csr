package aspn.hello.epms.preventive.plan.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.util.Utility;
import aspn.hello.epms.preventive.plan.mapper.PreventivePlanMapper;
import aspn.hello.epms.preventive.plan.model.PreventivePlanVO;

/**
 * [예방보전일정관리/배정] Business Implement Class
 * 
 * @author 이영탁
 * @since 2018.04.26
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.26		이영탁			최초 생성
 *   2018.05.09		김영환			일별 예방점검 리스트 조회 수정
 *   
 * </pre>
 */

@Service
public class PreventivePlanServiceImpl implements PreventivePlanService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	PreventivePlanMapper preventivePlanMapper;
		
	/**
	 * 위치 목록(Enum)
	 *
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> locationListOpt(PreventivePlanVO preventivePlanVO){
		
		return preventivePlanMapper.locationListOpt(preventivePlanVO);
		
	}
	
	/**
	 * 파트 목록(Enum)
	 *
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> partListOpt(PreventivePlanVO preventivePlanVO){
		
		return preventivePlanMapper.partListOpt(preventivePlanVO);
		
	}
	
	/**
	 * 월별 예방점검 결과 조회 ajax
	 *
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> monthlyPreventivePlanList(PreventivePlanVO preventivePlanVO){
		
		return preventivePlanMapper.monthlyPreventivePlanList(preventivePlanVO);
	}
	
	/**
	 * 일별 예방점검 리스트 조회 
	 *
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> preventivePlanList(PreventivePlanVO preventivePlanVO){
		
		return preventivePlanMapper.preventivePlanList(preventivePlanVO);
	}
	
	/**
	 * 설비BOM 목록(Enum)
	 *
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> equipmentBomListOpt(PreventivePlanVO preventivePlanVO){
		
		return preventivePlanMapper.equipmentBomListOpt(preventivePlanVO);
		
	}
	
	/**
	 * 정비파트원 목록(전체)
	 *
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> repairMemberList(PreventivePlanVO preventivePlanVO){
		
		return preventivePlanMapper.repairMemberList(preventivePlanVO);
		
	}
	
	/**
	 * 정비파트원 목록(전체):RELATED
	 *
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> repairMemberList2(PreventivePlanVO preventivePlanVO){
		
		return preventivePlanMapper.repairMemberList2(preventivePlanVO);
		
	}
	/**
	 * 예방보전일정관리/배정 
	 * 예방점검항목 저장버튼  (관리자메뉴)
	 *
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param PreventivePlanVO preventivePlanVO
	 * @return void
	 */
	@Override
	public void preventivePlanListEdit(List<HashMap<String, Object>> saveDataList,PreventivePlanVO preventivePlanVO){
		int rtnVal=0;
		
		if(saveDataList != null){
			
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				
				PreventivePlanVO preventivePlanVO1 = Utility.toBean(tmpSaveData, preventivePlanVO.getClass());
				
				//로그인 계정 정보 담기
				preventivePlanVO1.setUserId(preventivePlanVO.getUserId());			
					
				if(tmpSaveData.containsKey("Changed")) {
					
					// 담당자가 '(공통)'일 경우
					if("(Common)".equals(preventivePlanVO1.getMANAGER())){
						preventivePlanVO1.setMANAGER("");
					}
					
					//예방보전 점검 항목 수정
					rtnVal = preventivePlanMapper.preventivePlanUpdate(preventivePlanVO1);
					
					if(rtnVal >0){
						rtnVal=0;
					}
				} else if(tmpSaveData.containsKey("Added")) {

				} else if(tmpSaveData.containsKey("Deleted")) {
						
				}	
			}
		}
		
	}
	
	/**
	 * 해당 LOCATION, PART에 해당하는 정비원 목록
	 *
	 * @author 김영환
	 * @since 2018.05.28
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairMemberList(PreventivePlanVO preventivePlanVO) {
		return preventivePlanMapper.selectRepairMemberList(preventivePlanVO);
	}

	/**
	 * 공통코드 - 정비유형목록 조회
	 *
	 * @author 김영환
	 * @since 2019.02.25
	 * @param List<HashMap<String, Object>> 
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRequestTypeList(PreventivePlanVO preventivePlanVO) {
		return preventivePlanMapper.selectRequestTypeList(preventivePlanVO);
	}
		
}
