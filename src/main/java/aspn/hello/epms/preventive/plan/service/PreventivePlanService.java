package aspn.hello.epms.preventive.plan.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.preventive.plan.model.PreventivePlanVO;

/**
 * [예방보전일정관리/배정] Service Class
 * 
 * @author 이영탁
 * @since 2018.04.26
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.26 	이영탁			최초 생성
 *   2018.05.09		김영환			일별 예방점검 리스트 조회 수정
 *   
 * </pre>
 */

public interface PreventivePlanService {
	
	/**
	 * 위치 목록(Enum)
	 *
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> locationListOpt(PreventivePlanVO preventivePlanVO);
	
	/**
	 * 파트 목록(Enum)
	 *
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> partListOpt(PreventivePlanVO preventivePlanVO);

	/**
	 * 월별 예방점검 결과 조회 ajax
	 *
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> monthlyPreventivePlanList(PreventivePlanVO preventivePlanVO);
	
	/**
	 * 일별 예방점검 리스트 조회 
	 *
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> preventivePlanList(PreventivePlanVO preventivePlanVO);
	
	/**
	 * 설비BOM 목록(Enum)
	 *
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> equipmentBomListOpt(PreventivePlanVO preventivePlanVO);
	
	/**
	 * 정비파트원 목록(전체)
	 *
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> repairMemberList(PreventivePlanVO preventivePlanVO);
	
	/**
	 * 정비파트원 목록(전체):RELATED
	 *
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> repairMemberList2(PreventivePlanVO preventivePlanVO);
	
	/**
	 * 예방보전일정관리/배정 
	 * 예방점검항목 저장버튼  (관리자메뉴)
	 *
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param List<HashMap<String, Object>> 
	 * @param PreventivePlanVO preventivePlanVO
	 * @return void
	 */
	public void preventivePlanListEdit(List<HashMap<String, Object>> saveDataList, PreventivePlanVO preventivePlanVO);

	/**
	 * 해당 LOCATION, PART에 해당하는 정비원 목록
	 *
	 * @author 김영환
	 * @since 2018.05.28
	 * @param List<HashMap<String, Object>> 
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> selectRepairMemberList(PreventivePlanVO preventivePlanVO);

	/**
	 * 공통코드 - 정비유형목록 조회
	 *
	 * @author 김영환
	 * @since 2019.02.25
	 * @param List<HashMap<String, Object>> 
	 * @param PreventivePlanVO preventivePlanVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> selectRequestTypeList(PreventivePlanVO preventivePlanVO);

}
