package aspn.hello.epms.preventive.plan.controller;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.ObjUtil;
import aspn.com.common.util.Utility;
import aspn.hello.epms.preventive.plan.model.PreventivePlanVO;
import aspn.hello.epms.preventive.plan.service.PreventivePlanService;
import aspn.hello.mem.model.MemberVO;
import aspn.hello.quartz.model.QuartzVO;
import aspn.hello.quartz.service.QuartzService;

/**
 * [예방보전일정관리/배정] Controller Class
 * @author 이영탁
 * @since 2018.04.26
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.26		이영탁			최초 생성
 *   2018.05.09		김영환			일별 예방점검 리스트 조회 수정
 *   2018.05.24		김영환			예방점검 오더 생성(preventivePlanMakeOrder)
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/preventive/plan")
public class PreventivePlanController extends ContSupport {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	PreventivePlanService preventivePlanService;
	
	@Autowired
	private QuartzService quartzService;

	/**
	 * 예방보전일정관리/배정
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param preventivePlanVO
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventivePlanMng.do")
	public String preventivePlanMng( PreventivePlanVO preventivePlanVO, Model model){
		String startDt = "";
		String endDt = "";
		
		try {
			
			if(preventivePlanVO.getStartDt() == null || "".equals(preventivePlanVO.getStartDt())){
				String todate = DateTime.getShortDateString();
				todate = todate.substring(0,6) + "01";
				//해당 연도 추출
				int year = Integer.parseInt(todate.substring(0, 4));
				//해당 월 추출
				int month = Integer.parseInt(todate.substring(4,6));
				String txtYear = "";
				String txtMonth = "";
				txtYear  = Integer.toString(year);
				//월이 10월보다 작을 경우 월앞에 '0'추가
				if(month<10){
					txtMonth = "0" + Integer.toString(month);
				}else{
					txtMonth = Integer.toString(month);
				}
				//해당 월의 마지막 날짜 구하기
				Calendar cal = Calendar.getInstance();
				cal.set(year, month-1, 1);
				int lastDate = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
				//해달 월 마지막 일자 최종
				endDt = txtYear + txtMonth + Integer.toString(lastDate);
				startDt = todate;
			}
			
			preventivePlanVO.setSubAuth(getSsSubAuth());
			preventivePlanVO.setUserId(getSsUserId());
			preventivePlanVO.setCompanyId(getSsCompanyId());
			preventivePlanVO.setLOCATION("5100");
			preventivePlanVO.setPART(getSsPart());
			
			//위치 정보
			List<HashMap<String, Object>> locationList = preventivePlanService.locationListOpt(preventivePlanVO);

			//파트 정보
			List<HashMap<String, Object>> partListOpt = preventivePlanService.partListOpt(preventivePlanVO);
			
			//해당 파트로 먼저선택
			if(preventivePlanVO.getPART().equals("") || preventivePlanVO.getPART() == null){
				String part1 = "";
				preventivePlanVO.setPART(part1);
			}
			
			
			String part = preventivePlanVO.getPART();
			String location = preventivePlanVO.getLOCATION();
			
			model.addAttribute("searchDt2", DateTime.getYearStr()+ "-" +DateTime.getMonth());
			model.addAttribute("locationList", locationList);
			model.addAttribute("partListOpt", partListOpt);
			model.addAttribute("startDt", startDt);
			model.addAttribute("endDt", endDt);
			model.addAttribute("part", part);
			model.addAttribute("location", location);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("PreventivePlanController.preventivePlanMng Error !" + e.toString());
		}
		
		return "/epms/preventive/plan/preventivePlanMng";
	}
	
	
	/**
	 * 월별 예방점검 결과 조회 ajax
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param preventivePlanVO
	 * @param model
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventivePlanAjax.do")
	@ResponseBody
	public HashMap<String, Object> preventivePlanAjax(PreventivePlanVO preventivePlanVO, Model model){
		
		HashMap<String, Object> map= new HashMap<String, Object>();
		
		try {
			
			preventivePlanVO.setSubAuth(getSsSubAuth());
			preventivePlanVO.setUserId(getSsUserId());
			
			//팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				preventivePlanVO.setAuthType(CodeConstants.AUTH_CHIEF);
			}
			//정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
				preventivePlanVO.setAuthType(CodeConstants.AUTH_REPAIR);
			}
			
			//월별 예방 점검 계획 횟수 조회 (COUNT)
			List<HashMap<String, Object>> monthlyPreventivePlan = preventivePlanService.monthlyPreventivePlanList(preventivePlanVO);
			
			map.put("monthlyPreventivePlan", monthlyPreventivePlan);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("PreventivePlanController.preventivePlanAjax Error !" + e.toString());
		}
		
		return map;
	}
	
	/**
	 * 일별 예방점검 리스트 조회 
	 * @author 이영탁
	 * @since 2018.04.26
	 * @param preventivePlanVO
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventivePlanListData.do")
	public String preventivePlanListData(PreventivePlanVO preventivePlanVO, Model model){
		
		try {
			preventivePlanVO.setSubAuth(getSsSubAuth());
			String authType="";
			
			//팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				authType=CodeConstants.AUTH_CHIEF;
				preventivePlanVO.setAuthType(authType);
			}//정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1){
				authType=CodeConstants.AUTH_REPAIR;
				preventivePlanVO.setAuthType(authType);
			}
			
			model.addAttribute("authType", authType);
			model.addAttribute("user_location", getSsLocation());
			model.addAttribute("user_part", getSsSubAuth());
			
			//예방보전 배정목록
			List<HashMap<String, Object>> list= preventivePlanService.preventivePlanList(preventivePlanVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("PreventivePlanController.preventivePlanListData Error !" + e.toString());
		}
		
		return "/epms/preventive/plan/preventivePlanListData";
	}
	
	/**
	 * 예방보전일정관리/배정
	 * 일별 예방점검 리스트 조회  Grid Layout
	 * @author 이영탁
	 * @since 2016.11.01
	 * @param preventivePlanVO
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/preventivePlanListLayout.do")
	public String preventivePlanListLayout(PreventivePlanVO preventivePlanVO, Model model){
		
		try{
			
			preventivePlanVO.setSubAuth(getSsSubAuth());	 				 //세션_조회권한
			preventivePlanVO.setSUB_ROOT(preventivePlanVO.getLOCATION());	 //세션_조회권한
			
			//정비파트원 목록
			List<HashMap<String, Object>> list4 = preventivePlanService.repairMemberList(preventivePlanVO);
			HashMap<String, Object> repairMemberListOpt  = ObjUtil.list2Map(list4, "|");
			model.addAttribute("repairMemberListOpt", repairMemberListOpt);
			
			//정비파트원 목록(공장별,파트별):RELATED
			List<HashMap<String, Object>> list5 = preventivePlanService.repairMemberList2(preventivePlanVO);
			String relatedCombo = ObjUtil.list2MapRelated(list5, "|", "CODE", "NAME");
			model.addAttribute("relatedCombo", relatedCombo);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("PreventivePlanController.preventivePlanListLayout Error !" + e.toString());
		}
		
		return "/epms/preventive/plan/preventivePlanListLayout";
	}
	
	/**
	 *  예방보전일정관리/배정
	 *  예방점검항목 저장버튼  (관리자메뉴)
	 * @author 이영탁
	 * @since 2016.11.01
	 * @param preventivePlanVO
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventivePlanListEdit.do")
	@ResponseBody
	public String preventivePlanListEdit(PreventivePlanVO preventivePlanVO, Model model){
		
		try{
			preventivePlanVO.setUserId(getSsUserId());
			preventivePlanService.preventivePlanListEdit( Utility.getEditDataList(preventivePlanVO.getUploadData()) , preventivePlanVO);
			return getSaved();
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("PreventivePlanController.preventivePlanListEdit Error !" + e.toString());
			return getSaveFail();
		}
	}
	
	/**
	 * 예방점검 결과 업데이트
	 * 예방점검타입이 예방점검(02)인경우 계획일이 지나면 미점검 체크후 다음 계획일 데이터 생성
	 * 
	 * @author 김영환
	 * @since 2018.05.24
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventivePlanMakeOrder.do")
	@ResponseBody
	public HashMap<String, Object> preventivePlanMakeOrder(){
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		logger.debug("================== 예방점검 업데이트 start ==================");
		StopWatch stopWatch = new StopWatch();
		if(true) {
			try {
				stopWatch.start();
				QuartzVO quartzVO= new QuartzVO();
				quartzService.updatePreventiveResult(quartzVO);
				
				if(quartzVO.getRetCode().equals("S")){
					logger.debug("처리완료");
					rtnMap.put("RetCode", quartzVO.getRetCode());
				}else{
					logger.debug("처리시 오류발생: "+quartzVO.getRetMsg());
				}
				
			} catch(Exception e) {
				logger.error(e.getMessage());
			}
		}
		stopWatch.stop();
		logger.debug("# 메소드종료[runtime: " + (stopWatch.getTotalTimeMillis()/1000) % 60 + "(s)]");
		logger.debug("================== 예방점검 업데이트 end ====================");
		return rtnMap;
	}
	
	/**
	 * 정비원 리스트
	 * 
	 * @author 김영환
	 * @since 2018.05.28
	 * @param PreventivePlanVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/managerListAjax.do")
	@ResponseBody
	public HashMap<String, Object> managerListAjax(PreventivePlanVO preventivePlanVO){
		
		HashMap<String, Object> map= new HashMap<String, Object>();
		
		try {
			preventivePlanVO.setCompanyId(getSsCompanyId());
						
			//월별 예방 점검 계획 횟수 조회 (COUNT)
			List<HashMap<String, Object>> managerListOpt = preventivePlanService.selectRepairMemberList(preventivePlanVO);
			map.put("managerListOpt", managerListOpt);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("PreventivePlanController.managerListAjax Error !" + e.toString());
		}
		
		return map;
	}
	
}
