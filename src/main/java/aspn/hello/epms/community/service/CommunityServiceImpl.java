package aspn.hello.epms.community.service;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ObjUtil;
import aspn.hello.epms.community.mapper.CommunityMapper;
import aspn.hello.epms.community.model.CommunityVO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * [게시판] Implement Class
 * 
 * @author 서정민
 * @since 2019.02.19
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2019.02.19		서정민			최초 생성
 *
 * </pre>
 */

@Service
public class CommunityServiceImpl implements CommunityService {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/** NoticeMapper */
	@Autowired
	private CommunityMapper communityMapper;
	

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 게시판 목록 조회
	 *
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectCommunityList(CommunityVO communityVO){
		return communityMapper.selectCommunityList(communityVO);
	}
	
	/**
	 * 게시판 상세 조회
	 *
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> selectCommunityDtl(CommunityVO communityVO){
		return communityMapper.selectCommunityDtl(communityVO);
	}	
	
	/**
	 * 게시판 등록
	 *
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return HashMap<String, Object>
	 */
	@Override
	public HashMap<String, Object> insertCommunity(CommunityVO communityVO) {
		
		//게시판 SEQ 조회(SEQ_COMMUNITY)
		int SEQ_COMMUNITY = communityMapper.selectCommunitySeq(communityVO);			
		communityVO.setCOMMUNITY(Integer.toString(SEQ_COMMUNITY));
		
		//게시판 등록
		communityMapper.insertCommunity(communityVO);
		
			
		return communityMapper.selectCommunityDtl(communityVO);
	}
	
	/**
	 * 게시판 수정
	 *
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return HashMap<String, Object>
	 */
	@Override
	public HashMap<String, Object> updateCommunity(CommunityVO communityVO) {
				
		HashMap<String, Object> communityChk = communityMapper.selectCommunityDtl(communityVO);
		
		if(communityChk != null){
			
			//게시판 수정
			communityMapper.updateCommunity(communityVO);
		}
			
		return communityMapper.selectCommunityDtl(communityVO);
	}
	
	/**
	 * 게시판 삭제
	 *
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return HashMap<String, Object>
	 */
	@Override
	public HashMap<String, Object> deleteCommunity(CommunityVO communityVO) {
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		HashMap<String, Object> communityChk = communityMapper.selectCommunityDtl(communityVO);
		
		if(communityChk != null){
			
			//게시판 삭제
			communityMapper.deleteCommunity(communityVO);
			
			resultMap.put("delYn", "Y");
		}
		else{
			resultMap.put("delYn", "N");
		}
		
		resultMap.put("communityDtl", communityMapper.selectCommunityDtl(communityVO));
			
		return resultMap;
	}
		
	/**
	 * 세션 권한 공장 목록
	 * 
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> locationListOpt(CommunityVO communityVO) {
		return communityMapper.locationListOpt(communityVO);
	}
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 모바일 - 게시판 목록 조회
	 *
	 * @author 서정민
	 * @since 2019.02.25
	 * @param CommunityVO communityVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectCommunityList2(CommunityVO communityVO){
		return communityMapper.selectCommunityList2(communityVO);
	}
	
	/**
	 * 모바일 - 게시판 상세 조회
	 *
	 * @author 서정민
	 * @since 2019.02.25
	 * @param CommunityVO communityVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> selectCommunityDtl2(CommunityVO communityVO){
		return communityMapper.selectCommunityDtl2(communityVO);
	}	
	
}
