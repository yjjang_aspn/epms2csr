package aspn.hello.epms.community.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.community.model.CommunityVO;


/**
 * [게시판] Service Class
 * 
 * @author 서정민
 * @since 2019.02.19
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2019.02.19		서정민			최초 생성
 *
 * </pre>
 */

public interface CommunityService {
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	* 게시판 목록 조회
	*
	* @author 서정민
	* @since 2019.02.19
	* @param CommunityVO communityVO
	* @return List<HashMap<String, Object>>
	*/
	public List<HashMap<String, Object>> selectCommunityList(CommunityVO communityVO);
	
	/**
	 * 게시판 상세 조회
	 *
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return HashMap<String, Object>
	 */
	public HashMap<String, Object> selectCommunityDtl(CommunityVO communityVO);
	
	/**
	 * 게시판 등록
	 *
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return HashMap<String, Object>
	 */
	HashMap<String, Object> insertCommunity(CommunityVO communityVO);
	
	/**
	 * 게시판 수정
	 *
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return HashMap<String, Object>
	 * @throws Exception 
	 */
	HashMap<String, Object> updateCommunity(CommunityVO communityVO) throws Exception;
	
	/**
	 * 게시판 삭제
	 *
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return HashMap<String, Object>
	 * @throws Exception 
	 */
	HashMap<String, Object> deleteCommunity(CommunityVO communityVO) throws Exception;

	/**
	 * 세션 권한 공장 목록
	 * 
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> locationListOpt(CommunityVO communityVO);
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	* 모바일 - 게시판 목록 조회
	*
	* @author 서정민
	* @since 2019.02.25
	* @param CommunityVO communityVO
	* @return List<HashMap<String, Object>>
	*/
	public List<HashMap<String, Object>> selectCommunityList2(CommunityVO communityVO);
	
	/**
	 * 모바일 - 게시판 상세 조회
	 *
	 * @author 서정민
	 * @since 2019.02.25
	 * @param CommunityVO communityVO
	 * @return HashMap<String, Object>
	 */
	public HashMap<String, Object> selectCommunityDtl2(CommunityVO communityVO);
	
}
