package aspn.hello.epms.community.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.community.model.CommunityVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [게시판] Mapper Class
 * 
 * @author 서정민
 * @since 2019.02.19
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2019.02.19		서정민			최초 생성
 *
 * </pre>
 */
@Mapper("communityMapper")
public interface CommunityMapper {
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 게시판 목록 조회
	 * 
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> selectCommunityList(CommunityVO communityVO);
	
	/**
	 * 게시판 상세 조회
	 * 
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return HashMap<String, Object>
	 */
	public HashMap<String, Object> selectCommunityDtl(CommunityVO communityVO);
	
	/**
	 * 게시판 SEQ 조회(SEQ_COMMUNITY)
	 *
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return int
	 */
	int selectCommunitySeq(CommunityVO communityVO);
	
	/**
	 * 게시판 등록 COMMUNITY INSERT
	 *
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return int
	 */
	int insertCommunity(CommunityVO communityVO);
	
	/**
	 * 게시판 수정 COMMUNITY UPDATE
	 *
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return int
	 */
	int updateCommunity(CommunityVO communityVO);
	
	/**
	 * 게시판 삭제 COMMUNITY UPDATE
	 *
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return int
	 */
	int deleteCommunity(CommunityVO communityVO);
	
	/**
	 * 세션 권한 공장 목록
	 * 
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> locationListOpt(CommunityVO communityVO);

	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - 게시판 목록 조회
	 * 
	 * @author 서정민
	 * @since 2019.02.25
	 * @param CommunityVO communityVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> selectCommunityList2(CommunityVO communityVO);
	
	/**
	 * 모바일 - 게시판 상세 조회
	 * 
	 * @author 서정민
	 * @since 2019.02.25
	 * @param CommunityVO communityVO
	 * @return HashMap<String, Object>
	 */
	public HashMap<String, Object> selectCommunityDtl2(CommunityVO communityVO);
	
}
