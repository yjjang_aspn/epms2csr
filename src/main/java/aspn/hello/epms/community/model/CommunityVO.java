package aspn.hello.epms.community.model;

import aspn.hello.com.model.AbstractVO;

/**
 * [게시판] VO 클래스
 * 
 * @author 서정민
 * @since 2019.02.19
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2019.02.19		서정민			최초 생성
 *
 *      </pre>
 */

public class CommunityVO extends AbstractVO {

	private static final long serialVersionUID = 1L;

	/** EPMS_COMMUNITY */
	private String COMMUNITY					= ""; // 게시판 Key
	private String COMPANY_ID					= ""; // 회사코드
	private String LOCATION						= ""; // 공장코드
	private String TYPE							= ""; // 게시판 유형(ACCIDENT:아차사고, SUGGESTION:제안)
	private String TITLE						= ""; // 제목
	private String CONTENT						= ""; // 내용
	private String COST							= ""; // 비용
	private String DATE_CHECK					= ""; // 수행일
	private String REG_ID						= ""; // 등록자
	private String REG_DT						= ""; // 등록일
	private String UPD_ID						= ""; // 수정자
	private String UPD_DT						= ""; // 수정일시
	private String DEL_YN 						= ""; // 삭제여부(기본일 경우 'N')
	
	/** ATTACH DB model  */
	private String ATTACH_GRP_NO				= ""; //파일그룹번호
	private String DEL_SEQ						= ""; //파일삭제
	private String MODULE						= ""; //모듈 아이디
	
	public String getCOMMUNITY() {
		return COMMUNITY;
	}
	public void setCOMMUNITY(String cOMMUNITY) {
		COMMUNITY = cOMMUNITY;
	}
	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}
	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getTITLE() {
		return TITLE;
	}
	public void setTITLE(String tITLE) {
		TITLE = tITLE;
	}
	public String getCONTENT() {
		return CONTENT;
	}
	public void setCONTENT(String cONTENT) {
		CONTENT = cONTENT;
	}
	public String getCOST() {
		return COST;
	}
	public void setCOST(String cOST) {
		COST = cOST;
	}
	public String getDATE_CHECK() {
		return DATE_CHECK;
	}
	public void setDATE_CHECK(String dATE_CHECK) {
		DATE_CHECK = dATE_CHECK;
	}
	public String getREG_ID() {
		return REG_ID;
	}
	public void setREG_ID(String rEG_ID) {
		REG_ID = rEG_ID;
	}
	public String getREG_DT() {
		return REG_DT;
	}
	public void setREG_DT(String rEG_DT) {
		REG_DT = rEG_DT;
	}
	public String getUPD_ID() {
		return UPD_ID;
	}
	public void setUPD_ID(String uPD_ID) {
		UPD_ID = uPD_ID;
	}
	public String getUPD_DT() {
		return UPD_DT;
	}
	public void setUPD_DT(String uPD_DT) {
		UPD_DT = uPD_DT;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getATTACH_GRP_NO() {
		return ATTACH_GRP_NO;
	}
	public void setATTACH_GRP_NO(String aTTACH_GRP_NO) {
		ATTACH_GRP_NO = aTTACH_GRP_NO;
	}
	public String getDEL_SEQ() {
		return DEL_SEQ;
	}
	public void setDEL_SEQ(String dEL_SEQ) {
		DEL_SEQ = dEL_SEQ;
	}
	public String getMODULE() {
		return MODULE;
	}
	public void setMODULE(String mODULE) {
		MODULE = mODULE;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
