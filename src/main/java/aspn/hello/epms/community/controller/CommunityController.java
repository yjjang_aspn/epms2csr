package aspn.hello.epms.community.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.util.WebUtils;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.AspnMessageSource;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.JsUtil;
import aspn.com.common.util.SessionUtil;
import aspn.com.common.util.Paging;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.com.service.DeviceService;
import aspn.hello.epms.community.model.CommunityVO;
import aspn.hello.epms.community.service.CommunityService;
import aspn.com.common.util.ObjUtil;

/**
 * [게시판] Controller Class
 * 
 * @author 서정민
 * @since 2019.02.19
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2019.02.19		서정민			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/community")
public class CommunityController extends ContSupport {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	CommunityService communityService;
	
	@Autowired
	AttachService attachService;
	
	
	/**
	 * [게시판] : 화면 호출
	 *
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */                       
	@RequestMapping(value = "/communityList.do")
	public String communityList(CommunityVO communityVO, Model model) throws Exception {
		
		//게시판 유형(ACCIDENT:아차사고, SUGGESTION:제안)
		model.addAttribute("TYPE", communityVO.getTYPE());
		
		model.addAttribute("startDt", DateTime.requestMmDay("1","yyyy-MM-dd"));
		model.addAttribute("endDt", DateTime.getDateString());
		
		//근무지 정보
		model.addAttribute("location", getSsLocation());
		
		//공장 정보
		communityVO.setSubAuth(getSsSubAuth());
		communityVO.setCompanyId(getSsCompanyId());
		
		List<HashMap<String, Object>> locationList = communityService.locationListOpt(communityVO);
		model.addAttribute("locationList", locationList);
		
		
		return "/epms/community/communityList";
	}
	
	/** 
	 * [게시판] : 목록 그리드 레이아웃
	 *
	 * @author 서정민
	 * @since 2019.02.19
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/communityListLayout.do")
	public String communityListLayout() throws Exception {
		return "/epms/community/communityListLayout";
	}
	
	/**
	 * [게시판] : 목록 그리드 데이터
	 * 
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/communityListData.do")
	public String communityListData(CommunityVO communityVO, Model model){
		try {
			communityVO.setSubAuth(getSsSubAuth());     //세션_조회권한
			communityVO.setSsLocation(getSsLocation()); //세션_공장위치
			
			//게시판 목록
			List<HashMap<String, Object>> list = communityService.selectCommunityList(communityVO);
			model.addAttribute("list", list);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("CommunityController.communityListData Error !" + e.toString());
		}
		
		return "/epms/community/communityListData";
	}
	
	/**
	 * [게시판] - 상세내역 조회
	 * 
	 * @author 서정민
	 * @since 2019.02.19
	 * @param CommunityVO communityVO
	 * @param AttachVO attachVO
	 * @param Model model
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/communityDtl.do")
	@ResponseBody
	public Map<String, Object> communityDtl(CommunityVO communityVO, AttachVO attachVO, Model model){
		Map<String, Object> map= new HashMap<String, Object>();
		try {
			
			//게시판 상세 조회
			HashMap<String, Object> communityDtl = communityService.selectCommunityDtl(communityVO);			
			map.put("communityDtl", communityDtl);
			
			//첨부파일
			if(!("".equals(communityDtl.get("ATTACH_GRP_NO")) || communityDtl.get("ATTACH_GRP_NO") == null || "0".equals(String.valueOf(communityDtl.get("FILE_CNT"))) ) ){				
				attachVO.setATTACH_GRP_NO(String.valueOf(communityDtl.get("ATTACH_GRP_NO")));
				List<HashMap<String, Object>> attachList = attachService.selectFileList(attachVO);
				map.put("attachList", attachList);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("CommunityController.communityDtl Error !" + e.toString());
		}
		
		return map;
	}
	
	/**
	 * [게시판] : 등록 팝업 화면
	 * 
	 * @author 서정민
	 * @since 2019.02.19 
	 * @param CommunityVO communityVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popCommunityRegForm.do")
	public String popCommunityRegForm(CommunityVO communityVO, Model model) throws Exception{
		
		//공장 정보
		communityVO.setSubAuth(getSsSubAuth());
		communityVO.setCompanyId(getSsCompanyId());
		
		List<HashMap<String, Object>> locationList = communityService.locationListOpt(communityVO);
		model.addAttribute("locationList", locationList);
		
		//기존 내역 '수정'일 경우
		if("update".equals(communityVO.getFlag())){
			
			//상세내역 조회
			HashMap<String, Object> communityDtl = communityService.selectCommunityDtl(communityVO);			
			model.addAttribute("communityDtl", communityDtl);
			
			//첨부파일
			if(!("".equals(String.valueOf(communityDtl.get("ATTACH_GRP_NO"))) || communityDtl.get("ATTACH_GRP_NO") == null)){
				AttachVO attachVO = new AttachVO();
				attachVO.setATTACH_GRP_NO(String.valueOf(communityDtl.get("ATTACH_GRP_NO")));
				List<HashMap<String, Object>> attachList = attachService.selectFileList(attachVO);
				model.addAttribute("attachList", attachList);
			}
		}
		
		return "/epms/community/popCommunityRegForm";
	}
	
	/**
	 * [게시판] : 등록 insert
	 * 
	 * @author 서정민
	 * @since 2019.02.19
	 * @param HttpServletRequest request
	 * @param MultipartHttpServletRequest mRequest
	 * @param HttpServletResponse response
	 * @param CommunityVO communityVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertCommunity.do")
	@ResponseBody
	public HashMap<String, Object> insertCommunity(HttpServletRequest request, MultipartHttpServletRequest mRequest, HttpServletResponse response, CommunityVO communityVO, Model model) throws Exception {
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		communityVO.setCompanyId(getSsCompanyId());
//		communityVO.setLOCATION(getSsLocation());
		communityVO.setUserId(getSsUserId());
		
		try {
			
			if("Y".equals(communityVO.getAttachExistYn())){
				String attachGrpNo = attachService.selectAttachGrpNo(); 
		   
				AttachVO attachVO = new AttachVO();
				attachVO.setDEL_SEQ(communityVO.getDEL_SEQ());
				attachVO.setATTACH_GRP_NO(attachGrpNo);
				attachVO.setMODULE(communityVO.getMODULE());
				communityVO.setATTACH_GRP_NO(attachGrpNo);
				attachService.AttachEdit(request, mRequest, attachVO);   
			}
			
			
			rtnMap = communityService.insertCommunity(communityVO);
				
		} catch (Exception e) {
			logger.error("CommunityController.insertCommunity Error !");
			e.printStackTrace();
		}
		
		return rtnMap;
	}
	
	/**
	 * [게시판] : 수정 update
	 * 
	 * @author 서정민
	 * @since 2019.02.19
	 * @param HttpServletRequest request
	 * @param MultipartHttpServletRequest mRequest
	 * @param HttpServletResponse response
	 * @param CommunityVO communityVO
	 * @param Model model
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateCommunity.do")
	@ResponseBody
	public HashMap<String, Object> updateCommunity(HttpServletRequest request, MultipartHttpServletRequest mRequest, HttpServletResponse response, CommunityVO communityVO, Model model) {
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();	
		
		try {
			communityVO.setUserId(getSsUserId());
			
			AttachVO attachVO = new AttachVO();
			attachVO.setDEL_SEQ(communityVO.getDEL_SEQ());
			attachVO.setMODULE(communityVO.getMODULE());
			
			if("".equals(communityVO.getATTACH_GRP_NO()) || communityVO.getATTACH_GRP_NO() == null){
				if("Y".equals(communityVO.getAttachExistYn())){
					String attachGrpNo = attachService.selectAttachGrpNo();
					attachVO.setATTACH_GRP_NO(attachGrpNo);
					communityVO.setATTACH_GRP_NO(attachGrpNo);
					attachService.AttachEdit(request, mRequest, attachVO); 
				}
			}
			else{
				attachVO.setATTACH_GRP_NO(communityVO.getATTACH_GRP_NO());
 				attachService.AttachEdit(request, mRequest, attachVO); 
			}
			
			rtnMap = communityService.updateCommunity(communityVO);
			
		}catch (Exception e) {
			logger.error("CommunityController.updateCommunity Error !");
			e.printStackTrace();
		}
		
		return rtnMap;
	}
	
	/**
	 * [게시판] : 삭제 update
	 * 
	 * @author 서정민
	 * @since 2019.02.19
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @param CommunityVO communityVO
	 * @param Model model
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteCommunity.do")
	@ResponseBody
	public HashMap<String, Object> deleteCommunity(HttpServletRequest request, HttpServletResponse response, CommunityVO communityVO, Model model) {
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();	
		
		try {
			communityVO.setUserId(getSsUserId());
			
			rtnMap = communityService.deleteCommunity(communityVO);
			
		}catch (Exception e) {
			logger.error("CommunityController.deleteCommunity Error !");
			e.printStackTrace();
		}
		
		return rtnMap;
	}
	
	
}
