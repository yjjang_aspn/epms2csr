package aspn.hello.epms.community.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.JsonUtil;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.model.ComVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.epms.community.model.CommunityVO;
import aspn.hello.epms.community.service.CommunityService;
import aspn.hello.epms.preventive.plan.service.PreventivePlanService;

/**
 * [모바일 - 게시판] Controller Class
 * @author 서정민
 * @since 2018.02.25
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2019.02.25		서정민			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/mobile/epms/community")
public class MobileCommunityController extends ContSupport {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	CommunityService communityService;
	
	@Autowired
	PreventivePlanService preventivePlanService;
	
	@Autowired
	AttachService attachService;
	

	/**
	 * [게시판] - 목록 조회
	 * @author 서정민
	 * @since 2019.02.25
	 * @param CommunityVO communityVO
	 * @throws Exception
	 */
	@RequestMapping(value = "/communityList.do")
	@ResponseBody
	public Map<String, Object> communityList(CommunityVO communityVO) throws Exception {
		Map<String, Object> resultMap = new HashMap<>();
		try {
			
			communityVO.setSubAuth(getSsSubAuth());     //세션_조회권한
			communityVO.setCompanyId(getSsCompanyId());	//세션_회사코드
			
			// 1.게시판 목록
			communityVO.setLOCATION(addStringToQuoto(communityVO.getLOCATION()));
			
			List<HashMap<String, Object>> list = communityService.selectCommunityList2(communityVO);

			resultMap.put("COMMUNITY_LIST", list);
			
			// 2. 공장조회 권한
			List<HashMap<String, Object>> locationList = communityService.locationListOpt(communityVO);
			
			resultMap.put("LOCATION_LIST", locationList);
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);


		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}

	/**
	 * [게시판] - 상세 조회
	 * @author 서정민
	 * @since 2019.02.25
	 * @param CommunityVO communityVO
	 * @param NoticeVO _noticeVO
	 * @throws Exception
	 */
	@RequestMapping(value = "/communityDtl.do")
	@ResponseBody
	public Map<String, Object> noticeDtl(CommunityVO communityVO) throws Exception {
		Map<String, Object> resultMap = new HashMap<>();
		try {
			if (JsonUtil.isMobileNullParam(resultMap, communityVO, "COMMUNITY")) {
				return resultMap;
			} 
			
			HashMap<String, Object> result = communityService.selectCommunityDtl2(communityVO);
			
			if(result == null) {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.NO_DATA_FOUND);
			} else {
				resultMap.put("COMMUNITY", result);
				
				if(Integer.parseInt(result.get("FILE_CNT").toString()) > 0){
					AttachVO attachVO = new AttachVO();
					attachVO.setATTACH_GRP_NO(result.get("ATTACH_GRP_NO").toString());
					List<HashMap<String, Object>> attachList = attachService.selectFileList(attachVO);
					resultMap.put("ATTACH_LIST", attachList);
				}
				
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
				
			}
			return resultMap;
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * [게시판] : 등록 insert
	 * 
	 * @author 서정민
	 * @since 2019.02.25
	 * @param HttpServletRequest request
	 * @param MultipartHttpServletRequest mRequest
	 * @param HttpServletResponse response
	 * @param CommunityVO communityVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertCommunity.do")
	@ResponseBody
	public HashMap<String, Object> insertCommunity(HttpServletRequest request, MultipartHttpServletRequest mRequest, HttpServletResponse response, CommunityVO communityVO, Model model) throws Exception {
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		communityVO.setCompanyId(getSsCompanyId());
//		communityVO.setLOCATION(getSsLocation());
		communityVO.setUserId(getSsUserId());
		
		try {
			
			if("Y".equals(communityVO.getAttachExistYn())){
				String attachGrpNo = attachService.selectAttachGrpNo(); 
		   
				AttachVO attachVO = new AttachVO();
				attachVO.setDEL_SEQ(communityVO.getDEL_SEQ());
				attachVO.setATTACH_GRP_NO(attachGrpNo);
				attachVO.setMODULE(communityVO.getMODULE());
				communityVO.setATTACH_GRP_NO(attachGrpNo);
				attachService.AttachEdit(request, mRequest, attachVO);   
			}
			
			
			HashMap<String, Object> community = communityService.insertCommunity(communityVO);
			
			if(community != null) {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
			else{
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.FAILED_TO_SAVE);
			}
				
		} catch (Exception e) {
			logger.error("CommunityController.insertCommunity Error !");
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * [게시판] : 수정 update
	 * 
	 * @author 서정민
	 * @since 2019.02.25
	 * @param HttpServletRequest request
	 * @param MultipartHttpServletRequest mRequest
	 * @param HttpServletResponse response
	 * @param CommunityVO communityVO
	 * @param Model model
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateCommunity.do")
	@ResponseBody
	public HashMap<String, Object> updateCommunity(HttpServletRequest request, MultipartHttpServletRequest mRequest, HttpServletResponse response, CommunityVO communityVO, Model model) {
		HashMap<String, Object> resultMap = new HashMap<String, Object>();	
		
		try {
			communityVO.setUserId(getSsUserId());
			
			AttachVO attachVO = new AttachVO();
			attachVO.setDEL_SEQ(communityVO.getDEL_SEQ());
			attachVO.setMODULE(communityVO.getMODULE());
			
			if("".equals(communityVO.getATTACH_GRP_NO()) || communityVO.getATTACH_GRP_NO() == null){
				if("Y".equals(communityVO.getAttachExistYn())){
					String attachGrpNo = attachService.selectAttachGrpNo();
					attachVO.setATTACH_GRP_NO(attachGrpNo);
					communityVO.setATTACH_GRP_NO(attachGrpNo);
					attachService.AttachEdit(request, mRequest, attachVO); 
				}
			}
			else{
				attachVO.setATTACH_GRP_NO(communityVO.getATTACH_GRP_NO());
 				attachService.AttachEdit(request, mRequest, attachVO); 
			}
			
			HashMap<String, Object> community = communityService.updateCommunity(communityVO);
			
			if(community != null) {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
			else{
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.FAILED_TO_SAVE);
			}
			
		}catch (Exception e) {
			logger.error("CommunityController.updateCommunity Error !");
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * [게시판] : 삭제 update
	 * 
	 * @author 서정민
	 * @since 2019.02.25
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @param CommunityVO communityVO
	 * @param Model model
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteCommunity.do")
	@ResponseBody
	public HashMap<String, Object> deleteCommunity(HttpServletRequest request, HttpServletResponse response, CommunityVO communityVO, Model model) {
		HashMap<String, Object> resultMap = new HashMap<String, Object>();	
		
		try {
			communityVO.setUserId(getSsUserId());
			
			HashMap<String, Object> community = communityService.deleteCommunity(communityVO);
			
			if("Y".equals(community.get("delYn").toString())){
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
			else{
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.FAILED_TO_SAVE);
			}
			
		}catch (Exception e) {
			logger.error("CommunityController.deleteCommunity Error !");
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * 모바일 - Oracle IN절 입력 시 Quoto처리 
	 * 
	 * @author 최준영
	 * 
	 * @since 2018.03.06
	 * @param String 
	 * @return String
	 * @throws Exception
	 */
	public String addStringToQuoto(String originString){
		
		if(!"".equals(originString) && originString != null && originString.contains("[") && originString.contains("]")){
			originString = originString.replace("[", "");
			originString = originString.replace("]", "");
		}
		
		if("".equals(originString) || originString == null) {
			return "\'\'";
		}
		else if("ALL".equals(originString)) {
			return "ALL";
		} 
		else {
			String[] tempArray = originString.split(",");
			StringBuffer tempBuffer = new StringBuffer();
			
			if(tempArray.length == 1) {
				tempBuffer.append("(\'"+tempArray[0]+"\')");
			} else {
				for(int i=0; i < tempArray.length; i++){
					if(i == 0)							tempBuffer.append("(\'"+tempArray[i]+"\', ");	// 시작값
					else if( i == tempArray.length-1)	tempBuffer.append("\'"+tempArray[i]+"\')");		// 마지막값
					else 								tempBuffer.append("\'"+tempArray[i]+"\', ");	// 중간값
				}
			}
			
			return tempBuffer.toString();
		}
	}
}
