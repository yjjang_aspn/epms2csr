package aspn.hello.epms.repair.plan.service;
        
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aspn.hello.epms.repair.plan.model.RepairPlanVO;
import aspn.hello.rfc.model.ZPM_PREORDER_EXPORT;
import aspn.hello.rfc.model.ZPM_PREORDER_IMPORT;

/**
 * [고장관리-정비일정관리/배정] Service Class
 * 
 * @author 김영환
 * @since 2018.03.05
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.05		김영환			최초 생성
 *   2018.04.19		김영환			모바일 최초 생성
 *   
 * </pre>
 */

public interface RepairPlanService {
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 정비파트원 목록-전체
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairPlanVO repairPlanVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairMemberList(RepairPlanVO repairPlanVO);

	/**
	 * 정비파트원 목록-전체:RELATED
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairPlanVO repairPlanVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairMemberList2(RepairPlanVO repairPlanVO);
	
	/**
	 * 정비 목록
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairPlanVO repairPlanVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairPlanList(RepairPlanVO repairPlanVO);

	/**
	 * 정비 취소 
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairPlanVO repairPlanVO
	 * @return HashMap<String, Object>
	 */

	HashMap<String, Object> repairDelete(RepairPlanVO repairPlanVO);
	/**
	 * 정비 목록 수정-정비계획일, 배정자 수정
	 * 
	 * @author 김영환
	 * @since 2018.03.07
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param RepairPlanVO repairPlanVO
	 */ 
	void repairPlanListEdit(List<HashMap<String, Object>> saveDataList, RepairPlanVO repairPlanVO);
	
	/**
	 * 정비 상태 목록
	 * 
	 * @author 김영환
	 * @since 2018.05.14
	 * @param RepairPlanVO repairPlanVO
	 * @return List<HashMap<String, Object>>
	 */ 
	List<HashMap<String, Object>> repairStatusListOpt(RepairPlanVO repairPlanVO);
	
	/**
	 * 월별 예방점검 결과 조회 ajax
	 * @author 김영환
	 * @since 2018.12.03
	 * @param repairPlanVO
	 * @param model
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> monthlyRepairPlanList(RepairPlanVO repairPlanVO);
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 모바일 - 담당자배정 건수 (메인화면 뱃지카운트)
	 * 
	 * @author 서정민
	 * @since 2018.05.24
	 * @param RepairPlanVO repairPlanVO
	 * @return List<HashMap<String, Object>>
	 */
	int selectRepairPlanCnt(RepairPlanVO repairPlanVO);
	
	/**
	 * 모바일 - 정비일정관리/배정 목록
	 * 
	 * @author 김영환
	 * @since 2018.04.19
	 * @param RepairPlanVO repairPlanVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairPlanList2(RepairPlanVO repairPlanVO);

	/**
	 * 모바일 - 정비일정관리/배정 상세
	 * 
	 * @author 김영환
	 * @since 2018.04.19
	 * @param RepairPlanVO repairPlanVO
	 * @return HashMap<String, Object>
	 */
	HashMap<String, Object> selectRepairPlanDtl(RepairPlanVO repairPlanVO);

	/**
	 * 모바일 - 정비 계획일, 담당자(정비원) 등록  
	 * 
	 * @author 김영환
	 * @since 2018.04.20
	 * @param RepairPlanVO repairPlanVO
	 * @return
	 */
	HashMap<String, Object> updateRepairPlan(RepairPlanVO repairPlanVO);

	/**
	 * 모바일 - 해당 LOCATION, PART에 해당하는 정비원 목록
	 * 
	 * @author 김영환
	 * @since 2018.04.20
	 * @param RepairPlanVO repairPlanVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairMemberList3(RepairPlanVO repairPlanVO);

	
	
}
