package aspn.hello.epms.repair.plan.service;
        
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.SessionUtil;
import aspn.com.common.util.Utility;
import aspn.hello.com.service.DeviceService;
import aspn.hello.epms.repair.plan.mapper.RepairPlanMapper;
import aspn.hello.epms.repair.plan.model.RepairPlanVO;
import aspn.hello.epms.repair.request.model.RepairRequestVO;
import aspn.hello.epms.repair.request.service.RepairRequestService;
import aspn.hello.epms.repair.result.mapper.RepairResultMapper;
import aspn.hello.epms.repair.result.model.RepairResultVO;
import aspn.hello.rfc.model.ZPM_EQIP_DN_T_EQUP;
import aspn.hello.rfc.model.ZPM_PREORDER_EXPORT;
import aspn.hello.rfc.model.ZPM_PREORDER_IMPORT;
import aspn.hello.rfc.model.ZPM_PREORDER_T_AFKO;

/**
 * [고장관리-정비일정관리/배정] Business Implement Class
 * 
 * @author 김영환
 * @since 2018.03.05
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------		---------------------------
 *   2018.03.05		김영환			최초 생성
 *   2018.04.19		김영환			모바일 최초 생성
 *   2018.05.21		김영환			하드코딩(직접입력)->'상수'로 변경
 *   
 * </pre>
 */

@Service
public class RepairPlanServiceImpl implements RepairPlanService {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RepairPlanMapper repairPlanMapper;
	
	@Autowired
	RepairResultMapper repairResultMapper;
	
	@Autowired
	RepairRequestService repairRequestService;
	
	@Autowired
	DeviceService deviceService;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 정비파트원 목록-전체
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairPlanVO repairPlanVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairMemberList(RepairPlanVO repairPlanVO){
		return repairPlanMapper.selectRepairMemberList(repairPlanVO);
	}
	
	/**
	 * 정비파트원 목록-전체:RELATED
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairPlanVO repairPlanVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairMemberList2(RepairPlanVO repairPlanVO){
		return repairPlanMapper.selectRepairMemberList2(repairPlanVO);
	}

	/**
	 * 정비 목록
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairPlanVO repairPlanVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairPlanList(RepairPlanVO repairPlanVO) {		
		return repairPlanMapper.selectRepairPlanList(repairPlanVO);
	}

	/**
	 * 정비 취소 
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairPlanVO repairPlanVO
	 * @return HashMap<String, Object>
	 */
	@Override
	public HashMap<String, Object> repairDelete(RepairPlanVO repairPlanVO) {
		
		repairPlanMapper.updateRepairRequestCancel(repairPlanVO);
		repairPlanMapper.updateRepairResultCancel(repairPlanVO);
		
		return repairPlanMapper.selectRepairPlanDtl(repairPlanVO);
	}

	/**
	 * 정비 일정 목록-정비계획일 등록, 담당자(정비원) 배정
	 * 
	 * @author 김영환
	 * @since 2018.03.12
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param RepairPlanVO repairPlanVO
	 */
	@Override
	public void repairPlanListEdit(List<HashMap<String, Object>> saveDataList, RepairPlanVO repairPlanVO) {
		if(saveDataList != null){
			
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				
				RepairPlanVO repairPlanVO2 = Utility.toBean(tmpSaveData, repairPlanVO.getClass());
				RepairResultVO repairResultVO = new RepairResultVO();
				
				if (tmpSaveData.containsKey("Changed")) {
					if("".equals(repairPlanVO2.getMANAGER()) || repairPlanVO2.getMANAGER() == null) {
						repairPlanVO2.setREPAIR_STATUS(CodeConstants.EPMS_REPAIR_STATUS_RECEIVE);
					} else {
						repairPlanVO2.setREPAIR_STATUS(CodeConstants.EPMS_REPAIR_STATUS_PLAN);
					}
					
					//정비계획일 등록, 담당자(정비원) 배정
					repairPlanVO2.setUserId(repairPlanVO.getUserId());				 //수정자ID, 등록자ID
					repairPlanMapper.updateRepairPlanList(repairPlanVO2);
					
					if(CodeConstants.EPMS_REPAIR_STATUS_PLAN.equals(repairPlanVO2.getREPAIR_STATUS())) {
						repairPlanVO2.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //작업유형(01:정비, 02:예방보전)
						repairPlanVO2.setWORK_ID(repairPlanVO2.getREPAIR_RESULT());	     //작업대상ID
						repairPlanVO2.setUSER_ID(repairPlanVO2.getMANAGER());	         //담당자ID(정비원)
						
						repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR);
						repairResultVO.setWORK_ID(repairPlanVO2.getREPAIR_RESULT());
						repairResultVO.setUSER_ID(repairPlanVO2.getMANAGER());
						repairResultVO.setUserId(repairPlanVO.getUserId());
						repairResultVO.setWORK_STATUS("N");

						int cnt = repairPlanMapper.selectWorkLog(repairPlanVO2);
						//정비원 등록
						if(cnt == 0){
							// WORK_LOG SEQ 조회
							String workLogSeq = repairResultMapper.selectWorkLogSeq(repairResultVO);
							repairResultVO.setWORK_LOG(workLogSeq);
							
							repairResultMapper.insertWorkLog(repairResultVO);
							
							// 현재 날짜
							Date today = new Date();         
							SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
							String startDt = date.format(today);
							repairResultVO.setDATE_START(startDt);
							repairResultVO.setDATE_END(startDt);
							repairResultVO.setITEM_DESCRIPTION("");
							
							// 기본 정비실적 시간
							repairResultMapper.insertWorkLogItem(repairResultVO);
						}
					}
					
//					DeviceModel deviceMd = new DeviceModel();
//					deviceMd.setMEMBER(EquipmentMasterMd.getMEMBER());
//					List<DeviceModel> pushList = deviceService.getTargetUserPushList(deviceMd);
//					
//					if(pushList.size() > 0 ){
//						PushThread pt = new PushThread();
//						String actionUrl = "/project/equipmentMaster/mobile/repairDtl.do?REPAIR="+EquipmentMasterMd.getREPAIR()+"&pushViewYn=Y";
//						
//						pt.setParam(pushList, "정비일정관리", actionUrl, "정비요청 상세");
//						pt.start();
//					}
					
				} else if(tmpSaveData.containsKey("Added")) {
					
				} else if(tmpSaveData.containsKey("Deleted")) {
					
				}
			}
		}
	}
	
	/**
	 * 정비 상태 목록
	 * 
	 * @author 김영환
	 * @since 2018.05.14
	 * @param RepairPlanVO repairPlanVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> repairStatusListOpt(RepairPlanVO repairPlanVO) {
		return repairPlanMapper.repairStatusListOpt(repairPlanVO);
	}

	/**
	 * 월별 예방점검 결과 조회 ajax
	 * @author 김영환
	 * @since 2018.12.03
	 * @param repairPlanVO
	 * @param model
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> monthlyRepairPlanList(RepairPlanVO repairPlanVO) {
		return repairPlanMapper.monthlyRepairPlanList(repairPlanVO);
	}
	
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - 담당자배정 건수 (메인화면 뱃지카운트)
	 * 
	 * @author 서정민
	 * @since 2018.05.24
	 * @param RepairPlanVO repairPlanVO
	 * @return int
	 */
	@Override
	public int selectRepairPlanCnt(RepairPlanVO repairPlanVO) {
		return repairPlanMapper.selectRepairPlanCnt(repairPlanVO);
	}

	/**
	 * 모바일 - 정비일정관리/배정 목록
	 * 
	 * @author 김영환
	 * @since 2018.04.19
	 * @param RepairPlanVO repairPlanVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairPlanList2(RepairPlanVO repairPlanVO) {
		return repairPlanMapper.selectRepairPlanList2(repairPlanVO);
	}

	/**
	 * 모바일 - 정비일정관리/배정 목록 상세
	 * 
	 * @author 김영환
	 * @since 2018.04.19
	 * @param RepairPlanVO repairPlanVO
	 * @return HashMap<String, Object>
	 */
	@Override
	public HashMap<String, Object> selectRepairPlanDtl(RepairPlanVO repairPlanVO) {
		return repairPlanMapper.selectRepairPlanDtl(repairPlanVO);
	}

	/**
	 * 모바일 - 정비계획일 등록, 담당자(정비원) 배정
	 * 
	 * @author 김영환
	 * @since 2018.04.20
	 * @param RepairPlanVO repairPlanVO
	 */
	@Override
	public HashMap<String, Object> updateRepairPlan(RepairPlanVO repairPlanVO) {
		
		//정비계획일 등록, 담당자(정비원) 배정
		if("".equals(repairPlanVO.getMANAGER()) || repairPlanVO.getMANAGER() == null){
			repairPlanVO.setREPAIR_STATUS(CodeConstants.EPMS_REPAIR_STATUS_RECEIVE); //정비상태:접수
		}else{
			repairPlanVO.setREPAIR_STATUS(CodeConstants.EPMS_REPAIR_STATUS_PLAN);    //정비상태:배정완료
		}
		
		repairPlanMapper.updateRepairPlanList(repairPlanVO);
		
		repairPlanVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR);		  //작업유형 세팅 
		repairPlanVO.setWORK_ID(repairPlanVO.getREPAIR_RESULT());       	  //작업대상ID
		repairPlanVO.setUSER_ID(repairPlanVO.getMANAGER());	            	  //담당자(정비원)ID
		
		//정비계획일 등록, 담당자(정비원) 배정
		if(!("".equals(repairPlanVO.getMANAGER()) || repairPlanVO.getMANAGER() == null)){
			//등록된 정비원목록에 있는지 체크
			int cnt = repairPlanMapper.selectWorkLog(repairPlanVO);
			//정비원 등록
			if(cnt == 0){
				
				RepairResultVO repairResultVO = new RepairResultVO();
				
				// WORK_LOG SEQ 조회
				String workLogSeq = repairResultMapper.selectWorkLogSeq(repairResultVO);
				repairResultVO.setWORK_LOG(workLogSeq);
				
				repairResultVO.setWORK_ID(repairPlanVO.getREPAIR_RESULT());
				repairResultVO.setUSER_ID(repairPlanVO.getMANAGER());
				repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR);
				repairResultVO.setUserId(repairPlanVO.getUserId());
				
				//정비원 등록
				repairResultMapper.insertWorkLog(repairResultVO);
				
				// 현재 날짜
				Date today = new Date();         
				SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
				String startDt = date.format(today);
				repairResultVO.setDATE_START(startDt);
				repairResultVO.setDATE_END(startDt);
				repairResultVO.setITEM_DESCRIPTION("");
				
				// 기본 정비실적 시간
				repairResultMapper.insertWorkLogItem(repairResultVO);
			}
		}
		
		return repairPlanMapper.selectRepairPlanDtl(repairPlanVO);
	}

	/**
	 * 모바일 - 해당 LOCATION, PART에 해당하는 정비원 목록
	 * 
	 * @author 김영환
	 * @since 2018.04.20
	 * @param RepairPlanVO repairPlanVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairMemberList3(RepairPlanVO repairPlanVO) {
		return repairPlanMapper.selectRepairMemberList3(repairPlanVO);
	}
	
	
}
