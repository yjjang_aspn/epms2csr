package aspn.hello.epms.repair.plan.controller;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.ObjUtil;
import aspn.com.common.util.Utility;
import aspn.hello.com.service.AttachService;
import aspn.hello.com.service.DeviceService;
import aspn.hello.epms.preventive.plan.model.PreventivePlanVO;
import aspn.hello.epms.preventive.plan.service.PreventivePlanService;
import aspn.hello.epms.preventive.result.service.PreventiveResultService;
import aspn.hello.epms.repair.plan.model.RepairPlanVO;
import aspn.hello.epms.repair.plan.service.RepairPlanService;
import aspn.hello.epms.repair.request.model.RepairRequestVO;
import aspn.hello.epms.repair.request.service.RepairRequestService;
import aspn.hello.epms.repair.result.model.RepairResultVO;
import aspn.hello.epms.repair.result.service.RepairResultService;
import aspn.hello.rfc.model.ZPM_PREORDER_EXPORT;
import aspn.hello.rfc.model.ZPM_PREORDER_IMPORT;
import aspn.hello.rfc.model.ZPM_PREORDER_T_AFKO;
import aspn.hello.rfc.service.RfcService;


/**
 * [고장관리-정비일정관리/배정] Controller Class
 * 
 * @author 김영환
 * @since 2018.03.05
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------		---------------------------
 *   2018.03.05		김영환			최초 생성
 *   2018.05.21		김영환			하드코딩(직접입력)->'상수'로 변경
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/repair/plan")
public class RepairPlanController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RepairPlanService repairPlanService;

	@Autowired
	RepairRequestService repairRequestService;
	
	@Autowired
	RepairResultService repairResultService;
	
	@Autowired
	PreventivePlanService preventivePlanService;
	
	@Autowired
	PreventiveResultService preventiveResultService;
	
	@Autowired
	AttachService attachService;
	
	@Autowired
	DeviceService deviceService;
	
	@Autowired
	RfcService rfcService;
	
	/**
	 * 정비일정관리/배정
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairPlanList.do")
	public String repairPlanList(PreventivePlanVO preventivePlanVO,Model model) throws Exception {
		
		String startDt = "";
		String endDt = "";
		
		try {
			
			if(preventivePlanVO.getStartDt() == null || "".equals(preventivePlanVO.getStartDt())){
				String todate = DateTime.getShortDateString();
				todate = todate.substring(0,6) + "01";
				//해당 연도 추출
				int year = Integer.parseInt(todate.substring(0, 4));
				//해당 월 추출
				int month = Integer.parseInt(todate.substring(4,6));
				String txtYear = "";
				String txtMonth = "";
				txtYear  = Integer.toString(year);
				//월이 10월보다 작을 경우 월앞에 '0'추가
				if(month<10){
					txtMonth = "0" + Integer.toString(month);
				}else{
					txtMonth = Integer.toString(month);
				}
				//해당 월의 마지막 날짜 구하기
				Calendar cal = Calendar.getInstance();
				cal.set(year, month-1, 1);
				int lastDate = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
				//해달 월 마지막 일자 최종
				endDt = txtYear + txtMonth + Integer.toString(lastDate);
				startDt = todate;
			}
			
			preventivePlanVO.setSubAuth(getSsSubAuth());
			preventivePlanVO.setUserId(getSsUserId());
			preventivePlanVO.setCompanyId(getSsCompanyId());
			preventivePlanVO.setLOCATION(getSsLocation());
			preventivePlanVO.setPART(getSsPart());
			
			RepairResultVO RepairResultVO = new RepairResultVO();
			RepairResultVO.setCompanyId(getSsCompanyId());
			
			// 공통코드 - 정비유형목록 조회
			List<HashMap<String, Object>> requestTypeList = preventivePlanService.selectRequestTypeList(preventivePlanVO);
			
			// 공통코드 - 위치 정보
			List<HashMap<String, Object>> locationList = preventivePlanService.locationListOpt(preventivePlanVO);

			// 공통코드  - 파트 정보
			List<HashMap<String, Object>> partListOpt = preventivePlanService.partListOpt(preventivePlanVO);
			
			//해당 파트로 먼저선택
			if(preventivePlanVO.getPART().equals("") || preventivePlanVO.getPART() == null){
				String part1 = "";
				preventivePlanVO.setPART(part1);
			}

			String location = preventivePlanVO.getLOCATION();
			
			model.addAttribute("searchDt2", DateTime.getYearStr()+ "-" +DateTime.getMonth());
			model.addAttribute("requestTypeList", requestTypeList);
			model.addAttribute("locationList", locationList);
			model.addAttribute("partListOpt", partListOpt);
			model.addAttribute("startDt", startDt);
			model.addAttribute("endDt", endDt);
			model.addAttribute("part", getSsPart());
			model.addAttribute("location", location);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("RepairPlanController.repairPlanList Error !" + e.toString());
		}
		
		return "/epms/repair/plan/repairPlanList";
	}
	
	/**
	 * 정비일정관리/배정 - 정비 목록 Grid Layout
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param repairPlanVO repairPlanVO
	 * @param Model model 
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/repairPlanListLayout.do")
	public String repairPlanListLayout(RepairPlanVO repairPlanVO, Model model){
		try {
			repairPlanVO.setSsLocation(getSsLocation()); //세션_공장위치
			repairPlanVO.setSubAuth(getSsSubAuth());	 //세션_조회권한
			repairPlanVO.setSsRemarks(getSsRemarks());   //세션_파트 분리/통합 여부
			repairPlanVO.setSsPart(getSsPart());         //세션_파트
			repairPlanVO.setCompanyId(getSsCompanyId());
			
			// 팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				repairPlanVO.setAuthType(CodeConstants.AUTH_CHIEF);
			}
			
			//정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
				repairPlanVO.setAuthType(CodeConstants.AUTH_REPAIR);
			}
						
			//위치정보(PLANT)
			List<HashMap<String, Object>> repairStatusList = repairPlanService.repairStatusListOpt(repairPlanVO);
			HashMap<String, Object> repairStatusListOpt  = ObjUtil.list2Map(repairStatusList, "|");
			model.addAttribute("repairStatusListOpt", repairStatusListOpt);

			//정비파트원 목록 : 전체
			List<HashMap<String, Object>> list3 = repairPlanService.selectRepairMemberList(repairPlanVO);
			HashMap<String, Object> repairMemberListOpt  = ObjUtil.list2Map(list3, "|");
			model.addAttribute("repairMemberListOpt", repairMemberListOpt);
			
			//정비파트원 목록(공장별,파트별):RELATED
			List<HashMap<String, Object>> list4 = repairPlanService.selectRepairMemberList2(repairPlanVO);
			String relatedCombo = ObjUtil.list2MapRelated(list4, "|", "CODE", "NAME");
			model.addAttribute("relatedCombo", relatedCombo);
			
			//정비요청 구분:일일보전/계획보전(SAP예방정비)
			PreventivePlanVO preventivePlanVO = new PreventivePlanVO();
			List<HashMap<String, Object>> repairRequestTypeList = preventivePlanService.selectRequestTypeList(preventivePlanVO);
			HashMap<String, Object> repairRequestTypeListOpt  = ObjUtil.list2Map(repairRequestTypeList, "|");
			model.addAttribute("repairRequestTypeListOpt", repairRequestTypeListOpt);
			
		} catch (Exception e) {
			logger.error("RepairPlanController.repairPlanListLayout Error !" + e.toString());
			e.printStackTrace();
		}
		return "/epms/repair/plan/repairPlanListLayout";
	}
	
	/**
	 * 정비일정관리/배정 - 정비 목록 Grid Data
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairPlanVO repairPlanVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairPlanListData.do")
	public String repairPlanListData(RepairPlanVO repairPlanVO, Model model){
		try {
			
			String authType="";
			
			repairPlanVO.setSsLocation(getSsLocation()); //세션_공장위치
			repairPlanVO.setSubAuth(getSsSubAuth());	 //세션_조회권한
			repairPlanVO.setSsRemarks(getSsRemarks());   //세션_파트 분리/통합 여부
//			repairPlanVO.setSsPart(getSsPart());         //세션_파트
			
			// 팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				repairPlanVO.setAuthType(CodeConstants.AUTH_CHIEF); //조회권한
				authType=CodeConstants.AUTH_CHIEF;
			}
			//정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
				repairPlanVO.setAuthType(CodeConstants.AUTH_REPAIR); //조회권한
				authType=CodeConstants.AUTH_REPAIR;
			}
			
			//정비 목록
			List<HashMap<String, Object>> list= repairPlanService.selectRepairPlanList(repairPlanVO);
			model.addAttribute("userId", getSsUserId());
			model.addAttribute("list", list);
			
			model.addAttribute("authType", authType);
			model.addAttribute("user_location", getSsLocation());
			model.addAttribute("user_part", getSsPart());
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("RepairPlanController.repairPlanListData Error !" + e.toString());
		}
		
		return "/epms/repair/plan/repairPlanListData";
	}
	
	/**
	 * 정비취소 - 취소사유 팝업
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairPlanVO repairPlanVO
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popRepairCancelForm.do")
	public String popRepairCancelForm(RepairPlanVO repairPlanVO, Model model) throws Exception{
		return "/epms/repair/plan/popRepairCancelForm";
	}
	
	/**
	 * 정비일정관리/배정 - 정비 취소 
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairPlanVO repairPlanVO
	 * @return int
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairDelete.do")
	@ResponseBody
	public HashMap<String, Object> repairDelete(RepairPlanVO repairPlanVO){
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		try {
			repairPlanVO.setCompanyId(getSsCompanyId());
			repairPlanVO.setSsLocation(getSsLocation());
			repairPlanVO.setUserId(getSsUserId());
			rtnMap = repairPlanService.repairDelete(repairPlanVO);
			
			if(rtnMap != null) {
				// Push 정보
				HashMap<String, Object> pushParam = rtnMap;
				String pushTitle = "";
				
				// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
				pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REQUEST);
				// Push Title
				pushTitle = pushParam.get("CANCEL_MEMBER_NAME").toString() + "님이 정비를 취소하였습니다.";
				pushParam.put("PUSHTITLE", pushTitle);
				// 상세화면 IDX 세팅
				pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
				// 알림음 유형 세팅(01:일반, 02:긴급)
				pushParam.put("PUSHALARM", "01");
				
				deviceService.sendPush(pushParam);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("RepairPlanController.repairDelete Error !" + e.toString());
		}
		
		return rtnMap;
	}
	
	/**
	 * 정비일정관리/배정 - 정비 목록 수정 (정비계획일, 배정자 수정)
	 * 
	 * @author 김영환
	 * @since 2018.03.09
	 * @param RepairPlanVO repairPlanVO
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairPlanListEdit.do")
	@ResponseBody
	public String repairPlanListEdit(RepairPlanVO repairPlanVO){
		try{
			repairPlanVO.setUserId(getSsUserId());			
			repairPlanService.repairPlanListEdit(Utility.getEditDataList(repairPlanVO.getUploadData()), repairPlanVO);
			return getSaved();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("RepairPlanController.repairPlanListEdit Error !" + e.toString());
			return getSaveFail();
		}		
	}
	
	/**
	 * 월별 예방점검 결과 조회 ajax
	 * @author 김영환
	 * @since 2018.12.03
	 * @param repairPlanVO
	 * @param model
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairPlanAjax.do")
	@ResponseBody
	public HashMap<String, Object> repairPlanAjax(RepairPlanVO repairPlanVO, Model model){
		
		HashMap<String, Object> map= new HashMap<String, Object>();
		
		try {
			
			repairPlanVO.setSubAuth(getSsSubAuth());
			repairPlanVO.setUserId(getSsUserId());
			
			//월별 예방 점검 계획 횟수 조회 (COUNT)
			List<HashMap<String, Object>> monthlyRepairPlanList = repairPlanService.monthlyRepairPlanList(repairPlanVO);
			
			map.put("monthlyRepairPlanList", monthlyRepairPlanList);
			model.addAttribute("SUB_ROOT", repairPlanVO.getSUB_ROOT());
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("RepairPlanController.repairPlanAjax Error !" + e.toString());
		}
		
		return map;
	}
		
	/**
	 * [SAP동기화] : 예방보전 오더 생성 
	 *
	 * @author 김영환
	 * @since 2019.02.19
	 * @param ZPM_PREORDER_IMPORT zpm_preorder_import
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/preventiveOrderInsert.do")
	@ResponseBody
	public HashMap<String, Object> preventiveOrderInsert(ZPM_PREORDER_IMPORT zpm_preorder_import) throws Exception {

		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		// SAP에서 보내준 예방보전 있는지 카운트 
		int sap_preventive_request_cnt = 0;
		String E_RESULT = null;
		String E_MESSAGE = null;

		// SAP의 예방보전오더정보 다운
		ZPM_PREORDER_EXPORT tables = rfcService.getPreventiveOrderInfo(zpm_preorder_import);
		
		try {
			
			RepairRequestVO repairRequestVO = new RepairRequestVO();
			
			if(tables != null){
				
				for (ZPM_PREORDER_T_AFKO zpm_preorder_t_afko : tables.getZpm_preorder_t_afko()) {
					
					repairRequestVO = new RepairRequestVO();
					repairRequestVO.setEQUIPMENT_UID(zpm_preorder_t_afko.getEQUNR());	//설비코드
					
					//설비정보 상세 조회 : 파트구분 통합여부 / 위치정보
					HashMap<String, Object> equipmentDtl = repairRequestService.selectEquipmentDtl(repairRequestVO);
					
					if(equipmentDtl !=null){
						
						//SAP에서 EQUIPMENT_UID값을 return하기 때문에 EQUIPMENT은 조회후 셋팅
						repairRequestVO.setEQUIPMENT(equipmentDtl.get("EQUIPMENT").toString());
						
						//파트구분 공장일 경우
						if(CodeConstants.REMARKS_SEPARATE.equals(String.valueOf(equipmentDtl.get("REMARKS")))){
							repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
						//파트통합 공장일 경우
						} else if(CodeConstants.REMARKS_UNITED.equals(String.valueOf(equipmentDtl.get("REMARKS")))){
							repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
						}

						repairRequestVO.setCompanyId(getSsCompanyId());
						repairRequestVO.setSsLocation(getSsLocation());
						repairRequestVO.setUserId(getSsUserId());
						repairRequestVO.setDIVISION(getSsDivision());
						
						//PM 오더 유형이 SAP_예방보전(계획보전)일경우
						if("DM01".equals(zpm_preorder_t_afko.getAUART())){ 
							repairRequestVO.setREQUEST_TYPE(CodeConstants.EPMS_REQUEST_TYPE_PREVENTIVE_SAP);	//요청유형 세팅
							repairRequestVO.setE_AUFNR(zpm_preorder_t_afko.getAUFNR());                         //PM 오더 번호
							repairRequestVO.setREPAIR_TYPE("150");												//정비유형
						}
						else{
							continue;
						}
						
						//처리파트
						//INGPR - D01 (기계파트), D02 (전기파트), D03 (시설파트)
						String part = zpm_preorder_t_afko.getINGPR().replaceAll("D", "").trim();
						
						// 파트 지정일 경우
						if(part.equals("01") || part.equals("02") || part.equals("03")){
							repairRequestVO.setPART(repairRequestVO.getCompanyId()+"_"+part);
					    // 파트가 지정되어 넘어 오지 않는 경우 : SAP에서 받은 대로 넣어줌
						} else {
							repairRequestVO.setPART(zpm_preorder_t_afko.getINGPR());
						}
												
						repairRequestVO.setMANAGER("");						                                //담당자
						repairRequestVO.setREQUEST_DESCRIPTION(zpm_preorder_t_afko.getLTXA1());				//정비요청 세팅(점검내용과 동일)
						repairRequestVO.setDATE_PLAN(zpm_preorder_t_afko.getGSTRP());						//계획일
						repairRequestVO.setDATE_ASSIGN(zpm_preorder_t_afko. getGSTRP());					//배정일
						
						// 기존에 등록된 오더인지 확인
						HashMap<String, Object> repair_result_aufnr = repairRequestService.selectRepairResultAufnr(repairRequestVO);
						// 기존에 등록된 오더가 아니고, TECO!="X" > 신규등록
						if(repair_result_aufnr == null){
							if(!"X".equals(zpm_preorder_t_afko.getTECO())){
								// 예방정비 요청
								repairRequestService.insertRepairRequest(repairRequestVO);
								// SAP에서 보내준 예방보전 건수 카운트
								sap_preventive_request_cnt++;
							}
						}
						// 기존에 등록된 오더 중 완료건이 아니고(E_RESULT!="S"), TECO=="X" 인 경우 > SAP에서 마감(TECO)친 경우, 오더 삭제
						else {
							if(!"S".equals(String.valueOf(repair_result_aufnr.get("E_RESULT")))
									&& "X".equals(zpm_preorder_t_afko.getTECO())){
								repairRequestVO.setREPAIR_REQUEST(String.valueOf(repair_result_aufnr.get("REPAIR_REQUEST")));
								repairRequestVO.setREPAIR_RESULT(String.valueOf(repair_result_aufnr.get("REPAIR_RESULT")));
								repairRequestService.updateRepairTeco(repairRequestVO);
							}
						}
						
					}
					
				}
			}
			
			if("S".equals(tables.getE_RESULT())){	// SAP과 정상적 통신을 했을 경우
				if(sap_preventive_request_cnt > 0){ // SAP과 통신후 일일 보전이 생성 되었을 경우
					E_RESULT = "S";
					E_MESSAGE = String.valueOf(sap_preventive_request_cnt) + "건의 '계획보전'이 생성되었습니다.";
				}else if( sap_preventive_request_cnt == 0){ // SAP과 정상적 통신은 했지만 생성된 일일 보전이 없을경우
					E_RESULT = "S";
					E_MESSAGE = "생성할 '계획보전'이 없습니다.";
				}
			}
			else{									// SAP과 정상적 통신을 못 했을경우
				E_RESULT = tables.getE_RESULT();
				E_MESSAGE = tables.getE_MESSAGE();
			}
			
			rtnMap.put("E_RESULT", E_RESULT);
			rtnMap.put("E_MESSAGE", E_MESSAGE);
			logger.info("E_RESULT:" + E_RESULT);
			logger.info("E_MESSAGE:" + E_MESSAGE);
			
			
		}catch (Exception e) {
			logger.error("RepairPlanController.preventiveOrderInsert Error !");
			e.printStackTrace();
		}
		
		return rtnMap;
	}
	
}
