package aspn.hello.epms.repair.plan.model;
        
import aspn.hello.com.model.AbstractVO;

/**
 * [고장관리-정비일정관리/배정] VO Class
 * 
 * @author 김영환
 * @since 2018.03.05
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.05		김영환			최초 생성
 *   
 * </pre>
 */

public class RepairPlanVO extends AbstractVO{
	private static final long serialVersionUID = 1L;
	/** MEMBER DB model  */	
	private String LOCATION					= "";	//소속공장(성남,대구)
	
	/** COMCD DB model  */
	private String REMARKS					= "";	//비고(01:파트구분,02:파트통합)
	
	/** EPMS_REPAIR_REQUEST model */
	private String CANCEL_DESCRIPTION		= "";	//취소사유
	private String CANCEL_MEMBER			= "";	//정비취소자
	private String REPAIR_REQUEST			= "";	//정비요청ID
	private String REQUEST_TYPE				= "";	//정비유형
	private String PART						= "";	//파트(01:기계,02:전기,03:시설)
	
	/** EPMS_REPAIR_RESULT model */
	private String REPAIR_RESULT			= "";	//정비실적 ID
	private String DATE_PLAN				= "";	//정비실적 ID

	private String MANAGER					= "";	//배정자
	private String REPAIR_STATUS			= "";	//정비상태(01:접수, 02:배정완료, 03:정비완료)

	/** EPMS_WORK_LOG model */
	private String SEQ_WORK_LOG				= "";	//WORK_LOG:작업이력ID
	private String WORK_TYPE				= "";	//작업유형(01:정비, 02:예방보전)
	private String WORK_ID					= "";	//작업대상ID
	private String USER_ID					= "";	//유저ID
	
	/** parameter model */
	private String authType					= "";	//조회권한
	
	/** EPMS_EQUIPMENT model */
	private String SUB_ROOT					= "";	//조회권한

	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getREMARKS() {
		return REMARKS;
	}
	public void setREMARKS(String rEMARKS) {
		REMARKS = rEMARKS;
	}
	public String getCANCEL_DESCRIPTION() {
		return CANCEL_DESCRIPTION;
	}
	public void setCANCEL_DESCRIPTION(String cANCEL_DESCRIPTION) {
		CANCEL_DESCRIPTION = cANCEL_DESCRIPTION;
	}
	public String getCANCEL_MEMBER() {
		return CANCEL_MEMBER;
	}
	public void setCANCEL_MEMBER(String cANCEL_MEMBER) {
		CANCEL_MEMBER = cANCEL_MEMBER;
	}
	public String getREPAIR_REQUEST() {
		return REPAIR_REQUEST;
	}
	public void setREPAIR_REQUEST(String rEPAIR_REQUEST) {
		REPAIR_REQUEST = rEPAIR_REQUEST;
	}
	public String getPART() {
		return PART;
	}
	public void setPART(String pART) {
		PART = pART;
	}
	public String getREPAIR_RESULT() {
		return REPAIR_RESULT;
	}
	public void setREPAIR_RESULT(String rEPAIR_RESULT) {
		REPAIR_RESULT = rEPAIR_RESULT;
	}
	public String getDATE_PLAN() {
		return DATE_PLAN;
	}
	public void setDATE_PLAN(String dATE_PLAN) {
		DATE_PLAN = dATE_PLAN;
	}
	public String getREQUEST_TYPE() {
		return REQUEST_TYPE;
	}
	public void setREQUEST_TYPE(String rEQUEST_TYPE) {
		REQUEST_TYPE = rEQUEST_TYPE;
	}
	public String getMANAGER() {
		return MANAGER;
	}
	public void setMANAGER(String mANAGER) {
		MANAGER = mANAGER;
	}
	public String getREPAIR_STATUS() {
		return REPAIR_STATUS;
	}
	public void setREPAIR_STATUS(String rEPAIR_STATUS) {
		REPAIR_STATUS = rEPAIR_STATUS;
	}
	public String getSEQ_WORK_LOG() {
		return SEQ_WORK_LOG;
	}
	public void setSEQ_WORK_LOG(String sEQ_WORK_LOG) {
		SEQ_WORK_LOG = sEQ_WORK_LOG;
	}
	public String getWORK_TYPE() {
		return WORK_TYPE;
	}
	public void setWORK_TYPE(String wORK_TYPE) {
		WORK_TYPE = wORK_TYPE;
	}
	public String getWORK_ID() {
		return WORK_ID;
	}
	public void setWORK_ID(String wORK_ID) {
		WORK_ID = wORK_ID;
	}
	public String getUSER_ID() {
		return USER_ID;
	}
	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}
	public String getAuthType() {
		return authType;
	}
	public void setAuthType(String authType) {
		this.authType = authType;
	}
	public String getSUB_ROOT() {
		return SUB_ROOT;
	}
	public void setSUB_ROOT(String sUB_ROOT) {
		SUB_ROOT = sUB_ROOT;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
