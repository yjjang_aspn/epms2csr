package aspn.hello.epms.repair.plan.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.JsonUtil;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.com.service.DeviceService;
import aspn.hello.epms.main.model.MainVO;
import aspn.hello.epms.main.service.MainService;
import aspn.hello.epms.repair.plan.model.RepairPlanVO;
import aspn.hello.epms.repair.plan.service.RepairPlanService;


/**
 * [모바일 -고장관리-정비일정관리/배정] Controller Class
 * 
 * @author 김영환
 * @since 2018.04.19
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.19		김영환			최초 생성
 *   2018.05.21		김영환			하드코딩(직접입력)->'상수'로 변경
 *
 * </pre>
 */

@Controller
@RequestMapping("/mobile/epms/repair/plan")
public class MobileRepairPlanController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RepairPlanService repairPlanService;
	
	@Autowired
	AttachService attachService;

	@Autowired
	DeviceService deviceService;
	
	@Autowired
	MainService mainService;
	
	/**
	 * 정비일정관리/배정 목록
	 * 
	 * @author 김영환
	 * @since 2018.04.19
	 * @param RepairPlanVO repairPlanVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairPlanList.do")
	@ResponseBody
	public Map<String, Object> repairPlanList(RepairPlanVO repairPlanVO){
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			
			// 1. 권한별 공장목록 조회
			MainVO mainVO = new MainVO();
			mainVO.setCompanyId(getSsCompanyId());
			mainVO.setSubAuth(getSsSubAuth());
			
			List<HashMap<String, Object>> locationList = mainService.selectLocationListOpt(mainVO);
			resultMap.put("LOCATION_LIST", locationList);
			
			// 2. 담당자 배정 목록 조회
			repairPlanVO.setCompanyId(getSsCompanyId());
			repairPlanVO.setSsLocation(getSsLocation()); //세션_공장위치
			repairPlanVO.setSubAuth(getSsSubAuth());	 //세션_조회권한
			repairPlanVO.setSsRemarks(getSsRemarks());   //세션_파트 분리/통합 여부
			repairPlanVO.setSsPart(getSsPart());         //세션_파트
			//IN절 입력 시 Quoto 처리
			repairPlanVO.setLOCATION(addStringToQuoto(repairPlanVO.getLOCATION()));
			repairPlanVO.setREPAIR_STATUS(addStringToQuoto(repairPlanVO.getREPAIR_STATUS()));
			
			// 팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				repairPlanVO.setAuthType(CodeConstants.AUTH_CHIEF);
			}
			
			// 정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
				repairPlanVO.setAuthType(CodeConstants.AUTH_REPAIR);
				
				// 공무(기계, 전기, 시설) 일 경우
				if("BR00_01".equals(getSsPart()) || "BR00_02".equals(getSsPart()) || "BR00_03".equals(getSsPart())){
					repairPlanVO.setPART("BR00_01,BR00_02,BR00_03");
				}
				// 생산 일 경우
				else if("BR00_04".equals(getSsPart())){
					repairPlanVO.setPART("BR00_04");
				}
				else {
					repairPlanVO.setPART(getSsPart());
				}
				repairPlanVO.setPART(addStringToQuoto(repairPlanVO.getPART()));
			}
			
			// 담당자 배정 목록 조회
			resultMap.put("PLAN_LIST", repairPlanService.selectRepairPlanList2(repairPlanVO));
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			
		} catch (Exception e) {	
			e.printStackTrace();
			logger.error("MobileRepairPlanController.repairPlanList Error !"+ e.toString());
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;

	}
	
	/**
	 * 정비일정관리/배정 상세
	 * 
	 * @author 김영환
	 * @since 2018.04.19
	 * @param RepairPlanVO repairPlanVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairPlanDtl.do")
	@ResponseBody
	public Map<String, Object> repairPlanDtl(RepairPlanVO repairPlanVO){
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			
			repairPlanVO.setCompanyId(getSsCompanyId());
			repairPlanVO.setSsLocation(getSsLocation());
			
			//정비 목록
			HashMap<String, Object> PLAN_DTL = repairPlanService.selectRepairPlanDtl(repairPlanVO);
			
			if(PLAN_DTL==null){
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.NO_DATA_FOUND);
			}else {
				resultMap.put("PLAN_DTL", PLAN_DTL);
				if(Integer.parseInt(PLAN_DTL.get("FILE_CNT").toString()) > 0){
					AttachVO attachVO = new AttachVO();
					attachVO.setATTACH_GRP_NO(String.valueOf(PLAN_DTL.get("ATTACH_GRP_NO")));
					List<HashMap<String, Object>> REQUEST_FILE_LIST = attachService.selectFileList(attachVO);
					resultMap.put("REQUEST_FILE_LIST", REQUEST_FILE_LIST);
				}
				
				//정비파트원 목록-해당LOCATION, PART의 정비파트원
				repairPlanVO.setSubAuth(getSsSubAuth());	 						//세션_조회권한
				repairPlanVO.setREMARKS(String.valueOf(PLAN_DTL.get("REMARKS")));   //정비요청 상세의 파트 분리/통합 여부
				repairPlanVO.setPART(String.valueOf(PLAN_DTL.get("PART")));			//정비요청 상세의 파트 
//				repairPlanVO.setLOCATION(String.valueOf(PLAN_DTL.get("LOCATION")));	//정비요청 상세의 공장위치
				repairPlanVO.setSUB_ROOT(String.valueOf(PLAN_DTL.get("LOCATION")));	//정비요청 상세의 공장위치
				List<HashMap<String, Object>> REPAIR_MEMBER_LIST = repairPlanService.selectRepairMemberList3(repairPlanVO);
				resultMap.put("REPAIR_MEMBER_LIST", REPAIR_MEMBER_LIST);
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("MobileRepairPlanController.repairPlanList Error !"+ e.toString());
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;

	}
	
	/**
	 * 정비일정관리/배정 - 정비계획일, 배정자 등록, 정비원
	 * 
	 * @author 김영환
	 * @since 2018.04.19
	 * @param RepairPlanVO repairPlanVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateRepairPlan.do")
	@ResponseBody
	public HashMap<String, Object> updateRepairPlan(RepairPlanVO repairPlanVO){
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		try{
			
			repairPlanVO.setCompanyId(getSsCompanyId());
			repairPlanVO.setSsLocation(getSsLocation());
			repairPlanVO.setUserId(getSsUserId());
			HashMap<String, Object> repairPlan = repairPlanService.updateRepairPlan(repairPlanVO);
			
			if (repairPlan != null) {
				// Push 정보
				HashMap<String, Object> pushParam = repairPlan;
				String pushTitle = "";
				
				// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
				pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REPAIR);
				// Push Title
				pushTitle = pushParam.get("MANAGER_NAME").toString() + "님이 정비 담당자로 배정되었습니다.";
				pushParam.put("PUSHTITLE", pushTitle);
				// 상세화면 IDX 세팅
				pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
				// 알림음 유형 세팅(01:일반, 02:긴급)
				pushParam.put("PUSHALARM", "01");
				
				deviceService.sendPush(pushParam);
				
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
			else {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.FAILED_TO_SAVE);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MobileRepairPlanController.updateRepairPlan Error !"+ e.toString());
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
		
	/**
	 * 정비일정관리/배정 - 정비 취소 
	 * 
	 * @author 김영환
	 * @since2018.04.19
	 * @param RepairPlanVO repairPlanVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteRepairPlan.do")
	@ResponseBody
	public Map<String, Object> deleteRepairPlan(RepairPlanVO repairPlanVO){
		
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			
			if (JsonUtil.isMobileNullParam(resultMap, repairPlanVO,	"REPAIR_REQUEST","REPAIR_RESULT","CANCEL_DESCRIPTION")) {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.PARAM_NULL);
				
			} else {
				repairPlanVO.setCompanyId(getSsCompanyId());
				repairPlanVO.setSsLocation(getSsLocation());
				repairPlanVO.setUserId(getSsUserId());
				HashMap<String, Object> repairDelete = repairPlanService.repairDelete(repairPlanVO);
				
				if(repairDelete != null){
					// Push 정보
					HashMap<String, Object> pushParam = repairDelete;
					String pushTitle = "";
					
					// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
					pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REQUEST);
					// Push Title
					pushTitle = pushParam.get("CANCEL_MEMBER_NAME").toString() + "님이 정비를 취소하였습니다.";
					pushParam.put("PUSHTITLE", pushTitle);
					// 상세화면 IDX 세팅
					pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
					// 알림음 유형 세팅(01:일반, 02:긴급)
					pushParam.put("PUSHALARM", "01");
					
					deviceService.sendPush(pushParam);
					
					JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
				} else {
					JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.FAILED_TO_DELETE);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("MobileRepairPlanController.deleteRepairPlan Error !"+ e.toString());
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * 모바일 - Oracle IN절 입력 시 Quoto처리 
	 * 
	 * @author 김영환
	 * 
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	public String addStringToQuoto(String originString){
		
		if("".equals(originString) || originString == null) {
			return "\'\'";
		} 
		else if("ALL".equals(originString)) {
			return "ALL";
		} 
		else {
			String[] tempArray = originString.split(",");
			StringBuffer tempBuffer = new StringBuffer();
			
			if(tempArray.length == 1) {
				tempBuffer.append("(\'"+tempArray[0]+"\')");
			} else {
				for(int i=0; i < tempArray.length; i++){
					if(i == 0)							tempBuffer.append("(\'"+tempArray[i]+"\', ");	// 시작값
					else if( i == tempArray.length-1)	tempBuffer.append("\'"+tempArray[i]+"\')");		// 마지막값
					else 								tempBuffer.append("\'"+tempArray[i]+"\', ");	// 중간값
				}
			}
			
			return tempBuffer.toString();
		}
	}
	
}
