package aspn.hello.epms.repair.request.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.repair.request.model.RepairRequestVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [고장관리-고장등록] Mapper Class
 * 
 * @author 김영환
 * @since 2018.03.06
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.06		김영환		최초 생성
 *   
 * </pre>
 */

@Mapper("repairRequestMapper")
public interface RepairRequestMapper {

	/**
	 * 정비이력 조회
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param RepairRequestVO repairRequestVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairHistoryList(RepairRequestVO repairRequestVO);

	/**
	 * 내 정비요청목록
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param RepairRequestVO repairRequestVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairRequestList(RepairRequestVO repairRequestVO);

	/**
	 * 정비요청 취소
	 *
	 * @author 김영환
	 * @since 2018.03.07
	 * @param RepairRequestVO repairRequestVO
	 * @return int
	 */
	int deleteRepairRequest(RepairRequestVO repairRequestVO);

	/**
	 * 라인 상세정보
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return HashMap<String, Object>
	 */
	HashMap<String, Object> selectCategoryDtl(RepairRequestVO repairRequestVO);

	/**
	 * 설비 상세정보
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return HashMap<String, Object>
	 */
	HashMap<String, Object> selectEquipmentDtl(RepairRequestVO repairRequestVO);

	/**
	 * 정비요청 SEQ 체크(SEQ_REPAIR_REQUEST 체크)
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return int
	 */
	int selectRepairRequestSeq(RepairRequestVO repairRequestVO);

	/**
	 * 정비요청 등록 REPAIR_REQUEST INSERT
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return int
	 */
	int insertRepairRequest(RepairRequestVO repairRequestVO);

	/**
	 * 정비요청 - 승인처리 REPAIR_REQUEST UPDATE
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return int
	 */
	int updateRepairRequestAppr1(RepairRequestVO repairRequestVO);

	/**
	 * 정비실적 SEQ 체크(SEQ_REPAIR 체크)
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return int
	 */
	int selectRepairSeq(RepairRequestVO repairRequestVO);

	/**
	 * 신규정비건 등록 REPAIR_RESULT INSERT
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return int
	 */
	int insertRepairResult(RepairRequestVO repairRequestVO);

	/**
	 * 정비 목록 수정(정비계획일, 배정자 수정)
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return int
	 */
	void updateRepairPlanList(RepairRequestVO repairRequestVO);

	/**
	 * 등록된 정비원목록에 있는지 체크
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return int
	 */
	int selectWorkLog(RepairRequestVO repairRequestVO);

	/**
	 * 파트 목록
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectPartList(RepairRequestVO repairRequestVO);

	/**
	 * 정비요청 첨부파일 수정
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return int
	 */
	void updateRepairRequestAttach(RepairRequestVO repairRequestVO);
	
	/**
	 * 생성된 오더가 있는지 확인
	 *
	 * @author 김영환
	 * @since 2019.02.28
	 * @param RepairRequestVO repairRequestVO
	 * @return HashMap<String, Object>
	 */
	HashMap<String, Object> selectRepairResultAufnr(RepairRequestVO repairRequestVO);
	
	/**
	 * SAP에서 마감(TECO)된 오더 삭제 : REPAIR_REQUEST
	 *
	 * @author 김영환
	 * @since 2019.02.28
	 * @param RepairRequestVO repairRequestVO
	 * @return int
	 */
	void updateRepairRequestTeco(RepairRequestVO repairRequestVO);
	
	/**
	 * SAP에서 마감(TECO)된 오더 삭제 : REPAIR_RESULT
	 *
	 * @author 김영환
	 * @since 2019.02.28
	 * @param RepairRequestVO repairRequestVO
	 * @return int
	 */
	void updateRepairResultTeco(RepairRequestVO repairRequestVO);

	/**
	 * 모바일 - 내 요청현황 목록 조회
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectRepairRequestList2(RepairRequestVO repairRequestVO);

	/**
	 * 모바일 - 요청 및 정비 상세
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	HashMap<String, Object> selectRepairRequestDtl(RepairRequestVO repairRequestVO);

	/**
	 * 고장등록 시 권한별 정비원 목록
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectWorkMemberList(RepairRequestVO repairRequestVO);

	/**
	 * 모바일 - 고장등록 파트정보 호출
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectPartList2(RepairRequestVO repairRequestVO);

}
