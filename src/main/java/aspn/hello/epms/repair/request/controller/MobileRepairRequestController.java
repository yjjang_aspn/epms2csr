package aspn.hello.epms.repair.request.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.JsonUtil;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.com.service.DeviceService;
import aspn.hello.epms.main.model.MainVO;
import aspn.hello.epms.main.service.MainService;
import aspn.hello.epms.repair.request.model.RepairRequestVO;
import aspn.hello.epms.repair.request.service.RepairRequestService;
import aspn.hello.epms.repair.result.model.RepairResultVO;
import aspn.hello.epms.repair.result.service.RepairResultService;

/**
 * [모바일 고장관리-고장등록] Controller Class
 * @author 김영환
 * @since 2018.03.06
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.06		김영환		최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/mobile/epms/repair/request")
public class MobileRepairRequestController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	RepairRequestService repairRequestService;
	
	@Autowired
	RepairResultService repairResultService;
	
	@Autowired
	AttachService attachService;
	
	@Autowired
	DeviceService deviceService;

	@Autowired
	MainService mainService;
	
	/**
	 * 모바일 - 정비요청화면 호출
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairRequestForm.do")
	@ResponseBody
	public Map<String, Object> repairRequestForm(RepairRequestVO repairRequestVO){
		
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			HashMap<String, Object> equipmentDtl = repairRequestService.selectEquipmentDtl(repairRequestVO);
			
			if(equipmentDtl == null) {
				
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			
			} else {
				
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
				
				// 회사코드 세팅
				repairRequestVO.setCompanyId(getSsCompanyId());
				// 공장정보 세팅
				repairRequestVO.setLOCATION(String.valueOf(equipmentDtl.get("LOCATION")));
				
				// 소속 파트정보 세팅
				resultMap.put("PART", getSsPart());
				// 소속 공장정보 세팅
				resultMap.put("LOCATION", getSsLocation());
				
				// 파트목록
				List<HashMap<String, Object>> part_list = repairRequestService.selectPartList2(repairRequestVO);
				if(part_list.size() > 0) resultMap.put("PART_LIST", part_list);
				
				repairRequestVO.setCompanyId(getSsCompanyId());
				repairRequestVO.setSsLocation(getSsLocation());
				repairRequestVO.setSubAuth(getSsSubAuth());
				repairRequestVO.setSUB_ROOT(String.valueOf(equipmentDtl.get("LOCATION")));
				
				// 파트구분여부 전달
				resultMap.put("REMARKS", String.valueOf(equipmentDtl.get("REMARKS")));
				
				//팀장일 경우 전체 정비원 전달
				if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
					
					List<HashMap<String, Object>> work_Member_List = repairRequestService.selectWorkMemberList(repairRequestVO);
					
					resultMap.put("WORK_MEMBER_LIST", work_Member_List);
					resultMap.put("AUTH", CodeConstants.AUTH_CHIEF);
				}
				//정비원일 경우
				else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
					
					//파트구분 공장일 경우 본인이 속한 파트원만 조회
					//통합일 경우 전체 정비원 전달
					if(CodeConstants.REMARKS_SEPARATE.equals(String.valueOf(equipmentDtl.get("REMARKS")))){
						repairRequestVO.setREMARKS(CodeConstants.REMARKS_SEPARATE);
						repairRequestVO.setPART(getSsPart());
					}
					
					List<HashMap<String, Object>> work_Member_List = repairRequestService.selectWorkMemberList(repairRequestVO);
					
					resultMap.put("WORK_MEMBER_LIST", work_Member_List);
					resultMap.put("AUTH", CodeConstants.AUTH_REPAIR);
				
				} 
				//일반 사용자일 경우
				else {
					
					resultMap.put("AUTH", CodeConstants.AUTH_COMMON);
				}
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
			
		} catch(Exception e) {
			logger.error("MobileRepairRequestController.repairRequestForm > " + e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}

	/**
	 * 모바일 - 고장등록 insert
	 * 
	 * @author 김영환
	 * @since 2018.03.08
	 * @param HttpServletRequest request
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertRepairRequest.do")
	@ResponseBody
	public HashMap<String, Object> insertRepairRequest(HttpServletRequest request, MultipartHttpServletRequest mRequest, RepairRequestVO repairRequestVO, Model model) throws Exception {
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		try {
			repairRequestVO.setCompanyId(getSsCompanyId());
			repairRequestVO.setSsLocation(getSsLocation());
			repairRequestVO.setUserId(getSsUserId());
			repairRequestVO.setDIVISION(getSsDivision());
			
			if("Y".equals(repairRequestVO.getAttachExistYn())){
				String attachGrpNo = attachService.selectAttachGrpNo(); 
		   
				AttachVO attachVO = new AttachVO();
				attachVO.setDEL_SEQ(repairRequestVO.getDEL_SEQ());
				attachVO.setATTACH_GRP_NO(attachGrpNo);
				attachVO.setMODULE("111");
				repairRequestVO.setATTACH_GRP_NO(attachGrpNo);
				attachService.AttachEdit(request, mRequest, attachVO);   
			}
			
			HashMap<String, Object> equipmentDtl = repairRequestService.selectEquipmentDtl(repairRequestVO);
			
			repairRequestVO.setLOCATION(String.valueOf(equipmentDtl.get("LOCATION")));
			
			//파트구분 공장일 경우
			if(CodeConstants.REMARKS_SEPARATE.equals(repairRequestVO.getREMARKS())){
				//팀장일 경우 요청후 자동결재
				if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
					repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
				//정비원일 경우
				} else if (getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1){
					if(getSsSubAuth().indexOf(repairRequestVO.getLOCATION())>-1 && getSsPart().equals(repairRequestVO.getPART()) ){
//					if(getSsLocation().equals(repairRequestVO.getLOCATION()) && getSsPart().equals(repairRequestVO.getPART())){
						repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
					}
				}
			//파트통합 공장일 경우
			} else if(CodeConstants.REMARKS_UNITED.equals(repairRequestVO.getREMARKS())){
				//팀장, 정비원일 경우 요청후 자동결재
				if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
					repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
				} else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1) {
					if(getSsSubAuth().indexOf(repairRequestVO.getLOCATION())>-1){
//					if(getSsLocation().equals(repairRequestVO.getLOCATION())){
						repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
					}
				}
			}
			
			HashMap<String, Object> repairRequest = repairRequestService.insertRepairRequest(repairRequestVO);
			
			if(repairRequest != null) {
				
				// Push 정보
				HashMap<String, Object> pushParam = repairRequest;
				String pushTitle = "";
				
				// 1. 자동결재일 경우 (접수 or 배정)
				if(CodeConstants.FLAG_AUTOAPPR.equals(repairRequestVO.getFlag())){
					
					// 1-1. 접수까지 수행된 경우 (처리파트 전체 정비원에게 푸시 전송)
					if(CodeConstants.EPMS_REPAIR_STATUS_RECEIVE.equals(pushParam.get("REPAIR_STATUS").toString())){
						// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
						pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_PLAN);
						// Push Title
						pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비를 요청하였습니다.(담당자 미배정)";
						pushParam.put("PUSHTITLE", pushTitle);
						// 상세화면 IDX 세팅
						pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
						// 알림음 유형 세팅(01:일반, 02:긴급)
						pushParam.put("PUSHALARM", pushParam.get("URGENCY").toString());
					}
					
					// 1-2. 배정까지 수행된 경우 (정비 담당자에게 푸시 전송)
					else if(CodeConstants.EPMS_REPAIR_STATUS_PLAN.equals(pushParam.get("REPAIR_STATUS").toString())){
						// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
						pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REPAIR);
						// Push Title
						pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비를 요청하였습니다.(담당자 : " + pushParam.get("MANAGER_NAME").toString() + ")";
						pushParam.put("PUSHTITLE", pushTitle);
						// 상세화면 IDX 세팅
						pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
						// 알림음 유형 세팅(01:일반, 02:긴급)
						pushParam.put("PUSHALARM", pushParam.get("URGENCY").toString());
					}
				}
				// 2. 자동결재 아닐 경우 (처리파트 전체 정비원에게 푸시 전송)
				else{
					// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
					pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_APPR);
					// Push Title
					pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비를 요청하였습니다.";
					pushParam.put("PUSHTITLE", pushTitle);
					// 상세화면 IDX 세팅
					pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
					// 알림음 유형 세팅(01:일반, 02:긴급)
					pushParam.put("PUSHALARM", pushParam.get("URGENCY").toString());
				}
				
				deviceService.sendPush(pushParam);
				
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
			else {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.FAILED_TO_SAVE);
			}
		   
		}catch (Exception e) {
			logger.error("MobileRepairRequestController.repairRequestInsert > " + e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * 모바일 - 내 요청현황 목록 조회
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairRequestList.do")
	@ResponseBody
	public Map<String, Object> repairRequestList(RepairRequestVO repairRequestVO) throws Exception{
		
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			
			// 1. 권한별 공장목록 조회
			MainVO mainVO = new MainVO();
			mainVO.setCompanyId(getSsCompanyId());
			mainVO.setSubAuth(getSsSubAuth());
			
			List<HashMap<String, Object>> locationList = mainService.selectLocationListOpt(mainVO);
			resultMap.put("LOCATION_LIST", locationList);
			
			// 2. 요청현황 목록 조회
			repairRequestVO.setCompanyId(getSsCompanyId());
			repairRequestVO.setSsLocation(getSsLocation());
			repairRequestVO.setSubAuth(getSsSubAuth());		//세션_조회권한
			repairRequestVO.setUserId(getSsUserId());
			//IN절 입력 시 Quoto 처리
			repairRequestVO.setLOCATION(addStringToQuoto(repairRequestVO.getLOCATION()));
			repairRequestVO.setAPPR_STATUS(addStringToQuoto(repairRequestVO.getAPPR_STATUS()));
			repairRequestVO.setREPAIR_STATUS(addStringToQuoto(repairRequestVO.getREPAIR_STATUS()));
			
			// 정비요청현황 목록 (EPMS_REPAIR_REQUEST)
			List<HashMap<String, Object>> request_list= repairRequestService.selectRepairRequestList2(repairRequestVO);
			
			// 정비실적에  등록된 정비원 조회 (EPMS_WORK_LOG)
			RepairResultVO repairResultVO = new RepairResultVO();
			repairResultVO.setCompanyId(getSsCompanyId());
			
			List<HashMap<String, Object>> work_log_list = new ArrayList<HashMap<String, Object>>();
			if (request_list != null){
				//정비실적현황 목록의 정비ID가져오기
				List<String> work_id = new ArrayList<String>();
				for (int i=0; i < request_list.size() ; i++ ){
					if(request_list.get(i).get("REPAIR_RESULT") != null){
						work_id.add(String.valueOf(request_list.get(i).get("REPAIR_RESULT")));
					}
				}
				repairResultVO.setWorkIdArr(work_id);
				
				//정비실적에 등록된 정비원 조회
				repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)

				work_log_list = repairResultService.selectWorkMemberList(repairResultVO);
			}
			
			if(request_list.isEmpty() == false) {
				
				List<HashMap<String, Object>> tmp_list = new ArrayList<HashMap<String, Object>>();
				String REPAIR_RESULT = "";	//정비ID (EPMS_REPAIR_RESULT)
				String WORK_ID = "";		//작업ID (EPMS_WORK_LOG)
				
				for(HashMap<String, Object> result : request_list){
					
					tmp_list = new ArrayList<HashMap<String, Object>>();
					REPAIR_RESULT = String.valueOf(result.get("REPAIR_RESULT"));
					
					for(HashMap<String, Object> item : work_log_list){

						WORK_ID = String.valueOf(item.get("WORK_ID"));
						
						if(REPAIR_RESULT.equals(WORK_ID)){
							tmp_list.add(item);
						}
					}
					result.put("WORK_LOG", tmp_list);
				}
			}
			resultMap.put("REQUEST_LIST", request_list);
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			
		} catch (Exception e) {
			logger.error("MobileRepairRequestController.repairRequestList > " + e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * 모바일 - 내 요청현황 상세조회
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairRequestDtl.do")
	@ResponseBody
	public Map<String, Object> repairRequestDtl(RepairRequestVO repairRequestVO) throws Exception{
		
		Map<String, Object> resultMap = new HashMap<>();
		
		// VO 초기화
		RepairResultVO repairResultVO = new RepairResultVO();
		repairResultVO.setCompanyId(getSsCompanyId());
		repairResultVO.setSsLocation(getSsLocation());
		
		try {
			
			repairRequestVO.setCompanyId(getSsCompanyId());
			repairRequestVO.setSsLocation(getSsLocation());
			
			// 요청 상세
			HashMap<String, Object> request_detail = repairRequestService.selectRepairRequestDtl(repairRequestVO);
			
			if(request_detail == null) {
				
				resultMap.put("REQUEST_DTL", request_detail);
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			
			} else {
				
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
				
				repairResultVO.setREPAIR_REQUEST(String.valueOf(request_detail.get("REPAIR_REQUEST")));
				resultMap.put("REQUEST_DTL", request_detail);
				
				// 고장사진
				if(Integer.parseInt(request_detail.get("FILE_CNT2").toString()) > 0){
					AttachVO attachVO = new AttachVO();
					attachVO.setATTACH_GRP_NO(String.valueOf(request_detail.get("ATTACH_GRP_NO2")));
					List<HashMap<String, Object>> request_file_list = attachService.selectFileList(attachVO);
					resultMap.put("REQUEST_FILE_LIST", request_file_list);
				}
			
				/*************************** 실적조회 ****************************/
				
				// 실적 상세
				HashMap<String, Object> result_detail = repairResultService.selectRepairResultDtl2(repairResultVO);

				// 실적이 있을 경우 수행
				if(result_detail != null){
					
					resultMap.put("RESULT_DTL", result_detail);
					repairResultVO.setREPAIR_RESULT(String.valueOf(result_detail.get("REPAIR_RESULT")));
					
					// 정비사진
					if(Integer.parseInt(result_detail.get("FILE_CNT").toString()) > 0){
						AttachVO attachVO = new AttachVO();
						attachVO.setATTACH_GRP_NO(String.valueOf(result_detail.get("ATTACH_GRP_NO")));
						List<HashMap<String, Object>> result_file_list = attachService.selectFileList(attachVO);
						resultMap.put("RESULT_FILE_LIST", result_file_list);
					}
					
					// 정비실적에 등록된 자재 목록
					List<HashMap<String, Object>> used_material_list = repairResultService.selectRepairUsedMaterialList(repairResultVO);
					if(used_material_list != null) {
						resultMap.put("USED_MATERIAL_LIST", used_material_list);
					}
					
					// 정비실적에 등록된 정비원 조회
					repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); // 01:정비실적, 02:고장원인분석
					repairResultVO.setWORK_ID(String.valueOf(result_detail.get("REPAIR_RESULT")));
					List<HashMap<String, Object>> work_member_list = repairResultService.selectWorkMemberList(repairResultVO);
					if(work_member_list != null) {
						resultMap.put("WORK_MEMBER_LIST", work_member_list);
					}
				}
			}
			
		} catch (Exception e) {
			logger.error("MobileRepairRequestController.repairRequestDtl > " + e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * 모바일 - 고장등록(정비요청)취소 
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return int
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteRepairRequest.do")
	@ResponseBody
	public Map<String, Object> deleteRepairRequest(RepairRequestVO repairRequestVO) throws Exception {
		
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			repairRequestVO.setCompanyId(getSsCompanyId());
			repairRequestVO.setSsLocation(getSsLocation());
			repairRequestVO.setUserId(getSsUserId());
			
			//정비요청 취소
			HashMap<String, Object> requestCancel = repairRequestService.deleteRepairRequest(repairRequestVO);
			
			if(requestCancel != null) {
				//정비취소일 경우
				if(CodeConstants.EPMS_APPR_STATUS_CANCEL_REQUEST.equals(requestCancel.get("APPR_STATUS").toString())){
					// 1-1. 요청자에게 푸시 전송
					// Push 정보
					HashMap<String, Object> pushParam = requestCancel;
					String pushTitle = "";
					
					// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
					pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REQUEST);
					// Push Title
					pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비요청을 취소하였습니다.";
					pushParam.put("PUSHTITLE", pushTitle);
					// 상세화면 IDX 세팅
					pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
					// 알림음 유형 세팅(01:일반, 02:긴급)
					pushParam.put("PUSHALARM", "01");
					
					deviceService.sendPush(pushParam);
					
					pushParam = new HashMap<String, Object>();
					pushParam = requestCancel;
					
					// 1-2. 처리파트 전체 정비원에게 푸시 전송
					// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
					pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REQUEST_CANCEL);
					// Push Title
					pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비요청을 취소하였습니다.";
					pushParam.put("PUSHTITLE", pushTitle);
					// 상세화면 IDX 세팅
					pushParam.put("PUSHMENUDETAILNDX", "");
					// 알림음 유형 세팅(01:일반, 02:긴급)
					pushParam.put("PUSHALARM", "01");
					
					deviceService.sendPush(pushParam);
					
					JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
				}
			}
			else{
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.FAILED_TO_SAVE);
			}
			
		}catch (Exception e) {
			logger.error("MobileRepairRequestController.repairRequestDelete > " + e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * 모바일 - Oracle IN절 입력 시 Quoto처리 
	 * 
	 * @author 김영환
	 * 
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	public String addStringToQuoto(String originString){
		
//		if(!"".equals(originString) && originString != null && originString.contains("[") && originString.contains("]")){
//			originString = originString.replace("[", "");
//			originString = originString.replace("]", "");
//		}
		
		if("".equals(originString) || originString == null) {
			return "\'\'";
		}
		else if("ALL".equals(originString)) {
			return "ALL";
		} 
		else {
			String[] tempArray = originString.split(",");
			StringBuffer tempBuffer = new StringBuffer();
			
			if(tempArray.length == 1) {
				tempBuffer.append("(\'"+tempArray[0]+"\')");
			} else {
				for(int i=0; i < tempArray.length; i++){
					if(i == 0)							tempBuffer.append("(\'"+tempArray[i]+"\', ");	// 시작값
					else if( i == tempArray.length-1)	tempBuffer.append("\'"+tempArray[i]+"\')");		// 마지막값
					else 								tempBuffer.append("\'"+tempArray[i]+"\', ");	// 중간값
				}
			}
			
			return tempBuffer.toString();
		}
	}
	
}
