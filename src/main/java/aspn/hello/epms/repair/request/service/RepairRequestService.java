package aspn.hello.epms.repair.request.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.epms.repair.request.model.RepairRequestVO;

/**
 * [고장관리-고장등록] Service Class
 * 
 * @author 김영환
 * @since 2018.02.19
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.19		김영환		최초 생성
 *   
 * </pre>
 */

public interface RepairRequestService {

	/**
	 * 정비이력 조회
	 *
	 * @author 김영환
	 * @since 2018.02.19
	 * @param RepairRequestVO repairRequestVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairHistoryList(RepairRequestVO repairRequestVO);

	/**
	 * 내 정비요청목록
	 *
	 * @author 김영환
	 * @since 2018.02.19
	 * @param RepairRequestVO repairRequestVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairRequestList(RepairRequestVO repairRequestVO);

	/**
	 * 정비요청 취소
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return int
	 */
	HashMap<String, Object> deleteRepairRequest(RepairRequestVO repairRequestVO);

	/**
	 * 라인 상세정보
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return HashMap<String, Object>
	 */
	HashMap<String, Object> selectCategoryDtl(RepairRequestVO repairRequestVO);

	/**
	 * 설비 상세정보
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return HashMap<String, Object>
	 */
	HashMap<String, Object> selectEquipmentDtl(RepairRequestVO repairRequestVO);

	/**
	 * 정비요청
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return HashMap<String, Object>
	 */
	HashMap<String, Object> insertRepairRequest(RepairRequestVO repairRequestVO);

	/**
	 * 파트목록 호출
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return HashMap<String, Object>
	 */
	List<HashMap<String, Object>> selectPartList(RepairRequestVO repairRequestVO);

	/**
	 * 정비요청 첨부파일 수정
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return int
	 */
	void updateRepairRequestAttach(RepairRequestVO repairRequestVO);

	/**
	 * 모바일 - 내 요청현황 목록 조회
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectRepairRequestList2(RepairRequestVO repairRequestVO);

	/**
	 * 모바일 - 요청 및 정비 상세
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	HashMap<String, Object> selectRepairRequestDtl(RepairRequestVO repairRequestVO);

	/**
	 * 고장등록 시 권한별 정비원 목록
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectWorkMemberList(RepairRequestVO repairRequestVO);

	/**
	 * 모바일 - 고장등록 파트정보 호출
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectPartList2(RepairRequestVO repairRequestVO);

	/**
	 * 생성된 오더가 있는지 확인
	 *
	 * @author 김영환
	 * @since 2019.02.28
	 * @param RepairRequestVO repairRequestVO
	 * @return HashMap<String, Object>
	 */
	HashMap<String, Object> selectRepairResultAufnr(RepairRequestVO repairRequestVO);
	
	/**
	 * SAP에서 마감(TECO)된 오더 삭제
	 *
	 * @author 김영환
	 * @since 2019.02.28
	 * @param RepairRequestVO repairRequestVO
	 * @return HashMap<String, Object>
	 */
	void updateRepairTeco(RepairRequestVO repairRequestVO);
}
