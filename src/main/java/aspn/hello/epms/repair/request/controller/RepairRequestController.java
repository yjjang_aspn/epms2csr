package aspn.hello.epms.repair.request.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.com.service.DeviceService;
import aspn.hello.epms.repair.request.model.RepairRequestVO;
import aspn.hello.epms.repair.request.service.RepairRequestService;
import aspn.hello.sys.service.CategoryService;

/**
 * [고장관리-고장등록] Controller Class
 * @author 김영환
 * @since 2018.03.06
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.06		김영환		최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/repair/request")
public class RepairRequestController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	RepairRequestService repairRequestService;
	
	@Autowired
	CategoryService categoryService;
	
	@Autowired
	AttachService attachService;

	@Autowired
	DeviceService deviceService;
	
	@Value("#{config['equipment_hierarchy']}")
	private String equipment_hierarchy;

	/**
	 * 고장등록 : 메인화면
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairRequestList.do")
	public String repairRequestList() throws Exception{
		return "/epms/repair/request/repairRequestList";
	}
	
	/**
	 * 고장등록 : 설비 Grid Layout
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairRequestEquipmentLayout.do")
	public String repairRequestEquipmentLayout(Model model) {
		
		model.addAttribute("equipment_hierarchy", equipment_hierarchy);
		
		return "/epms/repair/request/repairRequestEquipmentLayout";
	}
	
	/**
	 * 고장등록 : 정비이력 Grid Layout
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairHistoryListLayout.do")
	public String repairHistoryListLayout() throws Exception{
		return "/epms/repair/request/repairHistoryListLayout";
	}
	
	/**
	 * 고장등록 : 정비이력 Grid Data
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairHistoryListData.do")
	public String repairHistoryListData(RepairRequestVO repairRequestVO, Model model) {
		try {
			//정비현황 목록
			List<HashMap<String, Object>> list= repairRequestService.selectRepairHistoryList(repairRequestVO);
			model.addAttribute("list", list);
		} catch (Exception e) {
			logger.error("RepairRequestController.repairHistoryListData Error !");
			e.printStackTrace();
		}
		
		return "/epms/repair/request/repairHistoryListData";
	}
	
	/**
	 * 고장등록 : 내 요청현황 Grid Layout
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param Model model
	 * @return String 
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairRequestListLayout.do")
	public String repairRequestListLayout(Model model) throws Exception{
		
		return "/epms/repair/request/repairRequestListLayout";
	}
	
	/**
	 * 고장등록 : 내 요청현황 Grid Data
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairRequestListData.do")
	public String repairRequestListData(RepairRequestVO repairRequestVO, Model model) throws Exception{
		try {
			repairRequestVO.setUserId(getSsUserId());
			
			//내 정비요청현황 목록
			List<HashMap<String, Object>> list= repairRequestService.selectRepairRequestList(repairRequestVO);
			model.addAttribute("list", list);
			model.addAttribute("userId", getSsUserId());
		} catch (Exception e) {
			logger.error("RepairRequestController.repairRequestListData Error !");
			e.printStackTrace();
		}
		return "/epms/repair/request/repairRequestListData";
	}
	
	/**
	 * 고장등록 : 고장등록(정비요청)취소 
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return int
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairRequestDelete.do")
	@ResponseBody
	public HashMap<String, Object> repairRequestDelete(RepairRequestVO repairRequestVO, Model model) throws Exception {
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();	
		
		try {
			repairRequestVO.setCompanyId(getSsCompanyId());
//			repairRequestVO.setSsLocation(getSsLocation());
			repairRequestVO.setUserId(getSsUserId());
			
			//정비요청 취소
			rtnMap = repairRequestService.deleteRepairRequest(repairRequestVO);
			
			if(rtnMap != null) {
				//정비취소일 경우
				if(CodeConstants.EPMS_APPR_STATUS_CANCEL_REQUEST.equals(rtnMap.get("APPR_STATUS").toString())){
					// 1-1. 요청자에게 푸시 전송
					// Push 정보
					HashMap<String, Object> pushParam = rtnMap;
					String pushTitle = "";
					
					// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
					pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REQUEST);
					// Push Title
					pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비요청을 취소하였습니다.";
					pushParam.put("PUSHTITLE", pushTitle);
					// 상세화면 IDX 세팅
					pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
					// 알림음 유형 세팅(01:일반, 02:긴급)
					pushParam.put("PUSHALARM", "01");
					
					deviceService.sendPush(pushParam);
					
					pushParam = new HashMap<String, Object>();
					pushParam = rtnMap;
					
					// 1-2. 처리파트 전체 정비원에게 푸시 전송
					// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
					pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REQUEST_CANCEL);
					// Push Title
					pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비요청을 취소하였습니다.";
					pushParam.put("PUSHTITLE", pushTitle);
					// 상세화면 IDX 세팅
					pushParam.put("PUSHMENUDETAILNDX", "");
					// 알림음 유형 세팅(01:일반, 02:긴급)
					pushParam.put("PUSHALARM", "01");
					
					deviceService.sendPush(pushParam);
					
				}
			}
			
		}catch (Exception e) {
			logger.error("RepairRequestController.repairRequestDelete Error !");
			e.printStackTrace();
		}
		return rtnMap;
	}
	
	/**
	 * 고장등록 : 고장등록(정비요청) 팝업 화면
	 * 
	 * @author 김영환
	 * @since 2018.03.07 
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popRepairRequestForm.do")
	public String popRepairRequestForm(RepairRequestVO repairRequestVO, Model model) throws Exception{
				
		//설비정보
		HashMap<String, Object> equipmentDtl = repairRequestService.selectEquipmentDtl(repairRequestVO);
		model.addAttribute("equipmentDtl", equipmentDtl);
		model.addAttribute("PART",getSsPart());
		model.addAttribute("userId",getSsUserId());

		//팀장일 경우 
		if(getSsSubAuth().indexOf("AUTH02")>-1){
			model.addAttribute("AUTH", CodeConstants.AUTH_CHIEF);
		}
		//정비원일 경우
		else if(getSsSubAuth().indexOf("AUTH03")>-1 && !"".equals(getSsPart())){
			model.addAttribute("AUTH", CodeConstants.AUTH_REPAIR);
		}
		//일반 사용자일 경우
		else {
			model.addAttribute("AUTH", CodeConstants.AUTH_COMMON);
		}
		
		// 파트통합여부
		model.addAttribute("REMARKS", String.valueOf(equipmentDtl.get("REMARKS")));
		
		return "/epms/repair/request/popRepairRequestForm";
	}
	
	/**
	 * 고장등록 : 고장등록(정비요청) 정비원 목록
	 * 
	 * @author 김영환
	 * @since 2018.03.07 
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectWorkMemberList.do")
	@ResponseBody
	public List<HashMap<String, Object>> selectWorkMemberList(RepairRequestVO repairRequestVO, Model model) throws Exception{
		
		repairRequestVO.setCompanyId(getSsCompanyId());
//		repairRequestVO.setSsLocation(getSsLocation());
		repairRequestVO.setSubAuth(getSsSubAuth());
		List<HashMap<String, Object>> workMemberList = repairRequestService.selectWorkMemberList(repairRequestVO);
		
		return workMemberList;
	}
	
	/**
	 * 고장등록 : 고장등록(정비요청) insert
	 * 
	 * @author 김영환
	 * @since 2018.03.08
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popRepairRequestInsert.do")
	@ResponseBody
	public HashMap<String, Object> popRepairRequestInsert(HttpServletRequest request, MultipartHttpServletRequest mRequest, HttpServletResponse response, RepairRequestVO repairRequestVO, Model model) throws Exception {
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		repairRequestVO.setCompanyId(getSsCompanyId());
		repairRequestVO.setSsLocation(getSsLocation());
		repairRequestVO.setUserId(getSsUserId());
		repairRequestVO.setDIVISION(getSsDivision());
		
		try {
			
			if ( CodeConstants.COMM_YES.equals(repairRequestVO.getAttachExistYn()) ) {
				
				String attachGrpNo = attachService.selectAttachGrpNo(); 
		   
				AttachVO attachVO = new AttachVO();
				attachVO.setDEL_SEQ(repairRequestVO.getDEL_SEQ());
				attachVO.setATTACH_GRP_NO(attachGrpNo);
				attachVO.setMODULE(repairRequestVO.getMODULE());
				repairRequestVO.setATTACH_GRP_NO(attachGrpNo);
				attachService.AttachEdit(request, mRequest, attachVO);   
			}
			
			
			/**
			 *  [YJJANG] Check for
			 *  
			//파트구분 공장일 경우
			if(CodeConstants.REMARKS_SEPARATE.equals(repairRequestVO.getREMARKS())){
				//팀장일 경우 요청후 자동결재
				if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
					repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
				//정비원일 경우
				} else if (getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1){
					if(getSsSubAuth().indexOf(repairRequestVO.getLOCATION())>-1 && getSsPart().equals(repairRequestVO.getPART()) ){
//					if(getSsLocation().equals(repairRequestVO.getLOCATION()) && getSsPart().equals(repairRequestVO.getPART())){
						repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
					}
				}
				
			//파트통합 공장일 경우
			} else if(CodeConstants.REMARKS_UNITED.equals(repairRequestVO.getREMARKS())){
				//팀장, 정비원일 경우 요청후 자동결재
				if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
					repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
				} else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1) {
//					if(getSsLocation().equals(repairRequestVO.getLOCATION())){
					if(getSsSubAuth().indexOf(repairRequestVO.getLOCATION())>-1){
						repairRequestVO.setFlag(CodeConstants.FLAG_AUTOAPPR);
					}
				}
			}
			 */

			
			rtnMap = repairRequestService.insertRepairRequest(repairRequestVO);
			
			if(rtnMap != null) {
				
				// Push 정보
				HashMap<String, Object> pushParam = rtnMap;
				String pushTitle = "";
				
				// 1. 자동결재일 경우 (접수 or 배정)
				if(CodeConstants.FLAG_AUTOAPPR.equals(repairRequestVO.getFlag())){
					
					// 1-1. 접수까지 수행된 경우 : 처리파트 전체 정비원에게 푸시 전송
					if(CodeConstants.EPMS_REPAIR_STATUS_RECEIVE.equals(pushParam.get("REPAIR_STATUS").toString())){
						// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
						pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_PLAN);
						// Push Title
						pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비를 요청하였습니다.(담당자 미배정)";
						pushParam.put("PUSHTITLE", pushTitle);
						// 상세화면 IDX 세팅
						pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
						// 알림음 유형 세팅(01:일반, 02:긴급)
						pushParam.put("PUSHALARM", pushParam.get("URGENCY").toString());
					}
					
					// 1-2. 배정까지 수행된 경우 : 정비 담당자에게 푸시 전송
					else if(CodeConstants.EPMS_REPAIR_STATUS_PLAN.equals(pushParam.get("REPAIR_STATUS").toString())){
						// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
						pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REPAIR);
						// Push Title
						pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비를 요청하였습니다.(담당자 : " + pushParam.get("MANAGER_NAME").toString() + ")";
						pushParam.put("PUSHTITLE", pushTitle);
						// 상세화면 IDX 세팅
						pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
						// 알림음 유형 세팅(01:일반, 02:긴급)
						pushParam.put("PUSHALARM", pushParam.get("URGENCY").toString());
					}
					
					deviceService.sendPush(pushParam);
				}
				// 2. 자동결재 아닐 경우  : 처리파트 전체 정비원에게 푸시 전송
				else{
					// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
					pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_APPR);
					// Push Title
					pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비를 요청하였습니다.";
					pushParam.put("PUSHTITLE", pushTitle);
					// 상세화면 IDX 세팅
					pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
					// 알림음 유형 세팅(01:일반, 02:긴급)
					pushParam.put("PUSHALARM", pushParam.get("URGENCY").toString());
					
					deviceService.sendPush(pushParam);
				}
				
			}	
		} catch (Exception e) {
			logger.error("RepairRequestController.popRepairRequestInsert Error !");
			e.printStackTrace();
		}
		
		return rtnMap;
	}
	
}
