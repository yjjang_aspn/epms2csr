package aspn.hello.epms.repair.request.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.config.CodeConstants;
import aspn.hello.com.mapper.AttachMapper;
import aspn.hello.epms.repair.request.mapper.RepairRequestMapper;
import aspn.hello.epms.repair.request.model.RepairRequestVO;
import aspn.hello.epms.repair.result.mapper.RepairResultMapper;
import aspn.hello.epms.repair.result.model.RepairResultVO;

/**
 * [고장관리-고장등록] Business Implement Class
 * 
 * @author 김영환
 * @since 2018.03.06
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.06		김영환		최초 생성
 *   
 * </pre>
 */

@Service
public class RepairRequestServiceImpl implements RepairRequestService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RepairRequestMapper repairRequestMapper;
	
	@Autowired
	RepairResultMapper repairResultMapper;
	
	@Autowired
	AttachMapper attachMapper;

	/**
	 * 정비이력 조회
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param RepairRequestVO repairRequestVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairHistoryList(RepairRequestVO repairRequestVO) {
		return repairRequestMapper.selectRepairHistoryList(repairRequestVO);
	}

	/**
	 * 내 정비요청목록
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param RepairRequestVO repairRequestVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairRequestList(RepairRequestVO repairRequestVO) {
		return repairRequestMapper.selectRepairRequestList(repairRequestVO);
	}

	/**
	 * 정비요청 취소
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param RepairRequestVO repairRequestVO
	 * @return int
	 */
	@Override
	public HashMap<String, Object> deleteRepairRequest(RepairRequestVO repairRequestVO) {
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		HashMap<String, Object> requestDtl = repairRequestMapper.selectRepairRequestDtl(repairRequestVO);
		
		// 결재상태가 "대기"일 경우만 요청취소 가능
		if(CodeConstants.EPMS_APPR_STATUS_WAIT.equals(requestDtl.get("APPR_STATUS").toString())){

			repairRequestMapper.deleteRepairRequest(repairRequestVO);
			rtnMap = repairRequestMapper.selectRepairRequestDtl(repairRequestVO);
		}
		
		return rtnMap;
	}

	/**
	 * 라인 상세정보
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return HashMap<String, Object>
	 */
	@Override
	public HashMap<String, Object> selectCategoryDtl(RepairRequestVO repairRequestVO) {
		return repairRequestMapper.selectCategoryDtl(repairRequestVO);
	}

	/**
	 * 설비 상세정보
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return HashMap<String, Object>
	 */
	@Override
	public HashMap<String, Object> selectEquipmentDtl(RepairRequestVO repairRequestVO) {
		return repairRequestMapper.selectEquipmentDtl(repairRequestVO);
	}

	/**
	 * 정비요청
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return HashMap<String, Object>
	 */
	@Override
	public HashMap<String, Object> insertRepairRequest(RepairRequestVO repairRequestVO) {
		
		//정비요청 SEQ 체크(SEQ_REPAIR_REQUEST 체크)
		int SEQ_REPAIR_REQUEST = repairRequestMapper.selectRepairRequestSeq(repairRequestVO);			
		repairRequestVO.setREPAIR_REQUEST(Integer.toString(SEQ_REPAIR_REQUEST));
		
		//신규요청일 경우 (타파트지원요청이 아닐경우)
		if("".equals(repairRequestVO.getREPAIR_REQUEST_ORG()) || repairRequestVO.getREPAIR_REQUEST_ORG() == null){
			repairRequestVO.setREPAIR_REQUEST_ORG(Integer.toString(SEQ_REPAIR_REQUEST));
		}
		
		//정비요청 등록 REPAIR_REQUEST INSERT
		repairRequestMapper.insertRepairRequest(repairRequestVO);
		
		//자동 결재승인처리
		if(CodeConstants.FLAG_AUTOAPPR.equals(repairRequestVO.getFlag())){
			//정비요청 - 승인처리 REPAIR_REQUEST UPDATE
			repairRequestMapper.updateRepairRequestAppr1(repairRequestVO);
			
			//정비실적 SEQ 체크(SEQ_REPAIR 체크)
			int SEQ_REPAIR_RESULT = repairRequestMapper.selectRepairSeq(repairRequestVO);	
			repairRequestVO.setREPAIR_RESULT(Integer.toString(SEQ_REPAIR_RESULT));

			// 선택한 정비원이 없을 경우 신규 정비건 담당자를 넣지 않음
			if(repairRequestVO.getMANAGER() == null || "00".equals(repairRequestVO.getMANAGER()) || "".equals(repairRequestVO.getMANAGER())){
				repairRequestVO.setMANAGER("");
			}
			
			//신규정비건 등록 REPAIR_RESULT INSERT
			repairRequestMapper.insertRepairResult(repairRequestVO);
			
			// 선택한 정비원이 있을 경우 배정
			if(repairRequestVO.getMANAGER() != null && !"00".equals(repairRequestVO.getMANAGER()) && !"".equals(repairRequestVO.getMANAGER())){
				//배정자 및 정비상태(배정완료) 세팅
				repairRequestVO.setREPAIR_STATUS(CodeConstants.EPMS_APPR_STATUS_APPR);
				
				repairRequestMapper.updateRepairPlanList(repairRequestVO);
				
				//작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)
				repairRequestVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR);
				//작업대상ID 세팅
				repairRequestVO.setWORK_ID(repairRequestVO.getREPAIR_RESULT());
				
				int cnt = repairRequestMapper.selectWorkLog(repairRequestVO);
				if(cnt == 0){
					RepairResultVO repairResultVO = new RepairResultVO();
					
					// WORK_LOG SEQ 조회
					String workLogSeq = repairResultMapper.selectWorkLogSeq(repairResultVO);
					repairResultVO.setWORK_LOG(workLogSeq);
					
					repairResultVO.setWORK_ID(repairRequestVO.getREPAIR_RESULT());
					repairResultVO.setUSER_ID(repairRequestVO.getMANAGER());
					repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR);
					repairResultVO.setUserId(repairRequestVO.getUserId());
					
					//정비원 등록
					repairResultMapper.insertWorkLog(repairResultVO);
					
					// 현재 날짜
					Date today = new Date();         
					SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
					String startDt = date.format(today);
					repairResultVO.setDATE_START(startDt);
					repairResultVO.setDATE_END(startDt);
					repairResultVO.setITEM_DESCRIPTION("");
					
					// 기본 정비실적 시간
					repairResultMapper.insertWorkLogItem(repairResultVO);
				}
			}
			
		} 
			
		return repairRequestMapper.selectRepairRequestDtl(repairRequestVO);
	}

	/**
	 * 파트 목록
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectPartList(RepairRequestVO repairRequestVO) {
		return repairRequestMapper.selectPartList(repairRequestVO);
	}

	/**
	 * 정비요청 첨부파일 수정
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return int
	 */
	@Override
	public void updateRepairRequestAttach(RepairRequestVO repairRequestVO) {
		repairRequestMapper.updateRepairRequestAttach(repairRequestVO);
	}
	
	/**
	 * 생성된 오더가 있는지 확인
	 *
	 * @author 김영환
	 * @since 2019.02.28
	 * @param RepairRequestVO repairRequestVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public HashMap<String, Object> selectRepairResultAufnr(RepairRequestVO repairRequestVO) {
		return repairRequestMapper.selectRepairResultAufnr(repairRequestVO);
	}
	
	/**
	 * SAP에서 마감(TECO)된 오더 삭제
	 *
	 * @author 김영환
	 * @since 2019.02.28
	 * @param RepairRequestVO repairRequestVO
	 * @return int
	 */
	@Override
	public void updateRepairTeco(RepairRequestVO repairRequestVO) {
		repairRequestMapper.updateRepairRequestTeco(repairRequestVO);
		repairRequestMapper.updateRepairResultTeco(repairRequestVO);
	}

	/**
	 * 모바일 - 내 요청현황 목록 조회
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairRequestList2(RepairRequestVO repairRequestVO) {
		return repairRequestMapper.selectRepairRequestList2(repairRequestVO);
	}

	/**
	 * 모바일 - 요청 및 정비 상세
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> selectRepairRequestDtl(RepairRequestVO repairRequestVO) {
		return repairRequestMapper.selectRepairRequestDtl(repairRequestVO);
	}

	/**
	 * 고장등록 시 권한별 정비원 목록
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectWorkMemberList(RepairRequestVO repairRequestVO) {
		return repairRequestMapper.selectWorkMemberList(repairRequestVO);
	}

	/**
	 * 모바일 - 고장등록 파트정보 호출
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectPartList2(RepairRequestVO repairRequestVO) {
		return repairRequestMapper.selectPartList2(repairRequestVO);
	}

}
