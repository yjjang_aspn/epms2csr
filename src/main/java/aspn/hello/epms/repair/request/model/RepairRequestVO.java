package aspn.hello.epms.repair.request.model;

import aspn.hello.com.model.AbstractVO;

/**
 * [고장관리-고장등록] VO Class
 * @author 김영환
 * @since 2018.03.06
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.06		김영환		최초 생성
 *   
 * </pre>
 */

public class RepairRequestVO extends AbstractVO{
	private static final long serialVersionUID = 1L;
	
	/** MATERIAL DB model  */
	private String MATERIAL					    = ""; //자재 ID
	private String LOCATION					    = ""; //회사 ID
	private String CD_MATERIAL_TYPE			    = ""; //자재구분(01:설비)
	private String MATERIAL_GRP1				= ""; //대그룹
	private String MATERIAL_UID				    = ""; //자재코드
	private String MATERIAL_NAME				= ""; //자재명
	private String MODEL						= ""; //모델명
	private String SPEC						    = ""; //규격/사양
	private String COUNTRY						= ""; //제조국
	private String MANUFACTURER				    = ""; //제조사
	private String CD_UNIT						= ""; //단위
	private String STOCK						= ""; //현재고
	private String STOCK_OPTIMAL				= ""; //안전재고
	private String MEMO						    = ""; //메모
	private String CD_DEL						= ""; //삭제여부
	private String SEQ_MATERIAL				    = ""; //자재시퀀스
	
	/** ATTACH DB model  */
	private String ATTACH_GRP_NO				= ""; //파일그룹번호
	private String DEL_SEQ						= ""; //파일삭제
	private String MODULE						= ""; //모듈 아이디
	
	/** EQUIPMENT DB model  */
	private String EQUIPMENT					= ""; //설비 ID
	private String EQUIPMENT_UID				= ""; //설비코드
	private String MEMBER						= ""; //등록자
	
	/** CATEGORY DB model  */
	private String CATEGORY					    = ""; //라인 ID
	private String TREE						    = ""; //구조체
	                                            
	/** DVISION DB model */                     
	private String DIVISION					    = ""; //부서 ID
	                                            
	/** PART DB model */                        
	private String PART						    = ""; //파트 ID
	                                            
	/** parameter model */                      
	private String FLAG						    = ""; //버튼 선택(1:등록(수정),2:삭제)
	
	/** COMCD DB model*/
	private String REMARKS						= ""; //비고(공장파트구분 01:파트구분,02:파트통합)
		
	/** CATEGORY DB model*/
	private String SUB_ROOT						= ""; //SUB_ROOT
	
	/** REPAIR_REQUEST DB model  */
	private String REPAIR_REQUEST				= ""; //정비요청 ID
	private String REPAIR_REQUEST_ORG			= ""; //원정비요청 ID
	private String URGENCY						= ""; //긴급도(01:일반,02:긴급)
	private String REPAIR_TYPE					= ""; //정비요청유형(110:돌발정비, 120:일상정비, 130:보전정비, 140:사후정비, 150:예방정비)
	private String REQUEST_TYPE					= ""; //정비요청유형(01:일반, 02:예방정비, 03:타파트 업무협조)
	private String MANAGER						= ""; //배정자
	private String WORK_TYPE					= ""; //작업유형(01:정비, 02:예방보전)
	private String WORK_ID						= ""; //작업대상ID
	private String REQUEST_DESCRIPTION			= ""; //고장내용
	private String APPR_STATUS					= ""; //결재상태(01:대기, 02:승인, 03:반려)
	
	/** REPAIR_RESULT DB model  */
	private String REPAIR_RESULT				= ""; //정비ID
	private String REPAIR_STATUS				= ""; //정비상태(01:접수, 02:배정완료, 03:정비완료)
	private String DATE_PLAN					= ""; //정비계획일
	private String DATE_ASSIGN					= ""; //배정일
	
	/** PREVENTIVE_RESULT DB model  */
	private String PREVENTIVE_RESULT			= ""; //예방보전실적 ID
	private String E_AUFNR						= ""; //SAP_예방보전(일일보전) 오더번호
	
	/** WORK_LOG DB model */
	private String USER_ID						= ""; //작업 유저ID
	private String SEQ_WORK_LOG					= ""; //예방보전실적시퀀스
	
	public String getMATERIAL() {
		return MATERIAL;
	}
	public void setMATERIAL(String mATERIAL) {
		MATERIAL = mATERIAL;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getCD_MATERIAL_TYPE() {
		return CD_MATERIAL_TYPE;
	}
	public void setCD_MATERIAL_TYPE(String cD_MATERIAL_TYPE) {
		CD_MATERIAL_TYPE = cD_MATERIAL_TYPE;
	}
	public String getMATERIAL_GRP1() {
		return MATERIAL_GRP1;
	}
	public void setMATERIAL_GRP1(String mATERIAL_GRP1) {
		MATERIAL_GRP1 = mATERIAL_GRP1;
	}
	public String getMATERIAL_UID() {
		return MATERIAL_UID;
	}
	public void setMATERIAL_UID(String mATERIAL_UID) {
		MATERIAL_UID = mATERIAL_UID;
	}
	public String getMATERIAL_NAME() {
		return MATERIAL_NAME;
	}
	public void setMATERIAL_NAME(String mATERIAL_NAME) {
		MATERIAL_NAME = mATERIAL_NAME;
	}
	public String getMODEL() {
		return MODEL;
	}
	public void setMODEL(String mODEL) {
		MODEL = mODEL;
	}
	public String getSPEC() {
		return SPEC;
	}
	public void setSPEC(String sPEC) {
		SPEC = sPEC;
	}
	public String getCOUNTRY() {
		return COUNTRY;
	}
	public void setCOUNTRY(String cOUNTRY) {
		COUNTRY = cOUNTRY;
	}
	public String getMANUFACTURER() {
		return MANUFACTURER;
	}
	public void setMANUFACTURER(String mANUFACTURER) {
		MANUFACTURER = mANUFACTURER;
	}
	public String getCD_UNIT() {
		return CD_UNIT;
	}
	public void setCD_UNIT(String cD_UNIT) {
		CD_UNIT = cD_UNIT;
	}
	public String getSTOCK() {
		return STOCK;
	}
	public void setSTOCK(String sTOCK) {
		STOCK = sTOCK;
	}
	public String getSTOCK_OPTIMAL() {
		return STOCK_OPTIMAL;
	}
	public void setSTOCK_OPTIMAL(String sTOCK_OPTIMAL) {
		STOCK_OPTIMAL = sTOCK_OPTIMAL;
	}
	public String getMEMO() {
		return MEMO;
	}
	public void setMEMO(String mEMO) {
		MEMO = mEMO;
	}
	public String getCD_DEL() {
		return CD_DEL;
	}
	public void setCD_DEL(String cD_DEL) {
		CD_DEL = cD_DEL;
	}
	public String getSEQ_MATERIAL() {
		return SEQ_MATERIAL;
	}
	public void setSEQ_MATERIAL(String sEQ_MATERIAL) {
		SEQ_MATERIAL = sEQ_MATERIAL;
	}
	public String getATTACH_GRP_NO() {
		return ATTACH_GRP_NO;
	}
	public void setATTACH_GRP_NO(String aTTACH_GRP_NO) {
		ATTACH_GRP_NO = aTTACH_GRP_NO;
	}
	public String getDEL_SEQ() {
		return DEL_SEQ;
	}
	public void setDEL_SEQ(String dEL_SEQ) {
		DEL_SEQ = dEL_SEQ;
	}
	public String getMODULE() {
		return MODULE;
	}
	public void setMODULE(String mODULE) {
		MODULE = mODULE;
	}
	public String getEQUIPMENT() {
		return EQUIPMENT;
	}
	public void setEQUIPMENT(String eQUIPMENT) {
		EQUIPMENT = eQUIPMENT;
	}
	public String getEQUIPMENT_UID() {
		return EQUIPMENT_UID;
	}
	public void setEQUIPMENT_UID(String eQUIPMENT_UID) {
		EQUIPMENT_UID = eQUIPMENT_UID;
	}
	public String getMEMBER() {
		return MEMBER;
	}
	public void setMEMBER(String mEMBER) {
		MEMBER = mEMBER;
	}
	public String getCATEGORY() {
		return CATEGORY;
	}
	public void setCATEGORY(String cATEGORY) {
		CATEGORY = cATEGORY;
	}
	public String getTREE() {
		return TREE;
	}
	public void setTREE(String tREE) {
		TREE = tREE;
	}
	public String getDIVISION() {
		return DIVISION;
	}
	public void setDIVISION(String dIVISION) {
		DIVISION = dIVISION;
	}
	public String getPART() {
		return PART;
	}
	public void setPART(String pART) {
		PART = pART;
	}
	public String getFLAG() {
		return FLAG;
	}
	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}
	public String getREMARKS() {
		return REMARKS;
	}
	public void setREMARKS(String rEMARKS) {
		REMARKS = rEMARKS;
	}
	public String getSUB_ROOT() {
		return SUB_ROOT;
	}
	public void setSUB_ROOT(String sUB_ROOT) {
		SUB_ROOT = sUB_ROOT;
	}
	public String getREPAIR_REQUEST() {
		return REPAIR_REQUEST;
	}
	public void setREPAIR_REQUEST(String rEPAIR_REQUEST) {
		REPAIR_REQUEST = rEPAIR_REQUEST;
	}
	public String getREPAIR_REQUEST_ORG() {
		return REPAIR_REQUEST_ORG;
	}
	public void setREPAIR_REQUEST_ORG(String rEPAIR_REQUEST_ORG) {
		REPAIR_REQUEST_ORG = rEPAIR_REQUEST_ORG;
	}
	public String getURGENCY() {
		return URGENCY;
	}
	public void setURGENCY(String uRGENCY) {
		URGENCY = uRGENCY;
	}
	public String getREPAIR_TYPE() {
		return REPAIR_TYPE;
	}
	public void setREPAIR_TYPE(String rEPAIR_TYPE) {
		REPAIR_TYPE = rEPAIR_TYPE;
	}
	public String getREQUEST_TYPE() {
		return REQUEST_TYPE;
	}
	public void setREQUEST_TYPE(String rEQUEST_TYPE) {
		REQUEST_TYPE = rEQUEST_TYPE;
	}
	public String getMANAGER() {
		return MANAGER;
	}
	public void setMANAGER(String mANAGER) {
		MANAGER = mANAGER;
	}
	public String getWORK_TYPE() {
		return WORK_TYPE;
	}
	public void setWORK_TYPE(String wORK_TYPE) {
		WORK_TYPE = wORK_TYPE;
	}
	public String getWORK_ID() {
		return WORK_ID;
	}
	public void setWORK_ID(String wORK_ID) {
		WORK_ID = wORK_ID;
	}
	public String getREQUEST_DESCRIPTION() {
		return REQUEST_DESCRIPTION;
	}
	public void setREQUEST_DESCRIPTION(String rEQUEST_DESCRIPTION) {
		REQUEST_DESCRIPTION = rEQUEST_DESCRIPTION;
	}
	public String getAPPR_STATUS() {
		return APPR_STATUS;
	}
	public void setAPPR_STATUS(String aPPR_STATUS) {
		APPR_STATUS = aPPR_STATUS;
	}
	public String getREPAIR_RESULT() {
		return REPAIR_RESULT;
	}
	public void setREPAIR_RESULT(String rEPAIR_RESULT) {
		REPAIR_RESULT = rEPAIR_RESULT;
	}
	public String getREPAIR_STATUS() {
		return REPAIR_STATUS;
	}
	public void setREPAIR_STATUS(String rEPAIR_STATUS) {
		REPAIR_STATUS = rEPAIR_STATUS;
	}
	public String getDATE_PLAN() {
		return DATE_PLAN;
	}
	public void setDATE_PLAN(String dATE_PLAN) {
		DATE_PLAN = dATE_PLAN;
	}
	public String getPREVENTIVE_RESULT() {
		return PREVENTIVE_RESULT;
	}
	public void setPREVENTIVE_RESULT(String pREVENTIVE_RESULT) {
		PREVENTIVE_RESULT = pREVENTIVE_RESULT;
	}
	public String getE_AUFNR() {
		return E_AUFNR;
	}
	public void setE_AUFNR(String e_AUFNR) {
		E_AUFNR = e_AUFNR;
	}
	public String getUSER_ID() {
		return USER_ID;
	}
	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}
	public String getSEQ_WORK_LOG() {
		return SEQ_WORK_LOG;
	}
	public void setSEQ_WORK_LOG(String sEQ_WORK_LOG) {
		SEQ_WORK_LOG = sEQ_WORK_LOG;
	}
	public String getDATE_ASSIGN() {
		return DATE_ASSIGN;
	}
	public void setDATE_ASSIGN(String dATE_ASSIGN) {
		DATE_ASSIGN = dATE_ASSIGN;
	}
	
}
