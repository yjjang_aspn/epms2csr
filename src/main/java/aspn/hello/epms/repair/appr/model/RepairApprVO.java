package aspn.hello.epms.repair.appr.model;
        
import aspn.hello.com.model.AbstractVO;

/**
 * [고장관리-정비요청결재] VO Class
 * @author 김영환
 * @since 2018.03.05
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.05		김영환			최초 생성
 *   
 * </pre>
 */

public class RepairApprVO extends AbstractVO{
	private static final long serialVersionUID = 1L;
		
	/** EPMS_REPAIR_REQUEST model */
	private String REPAIR_REQUEST			= "";	//정비요청ID
	private String APPR_MEMBER				= "";	//결재자
	private String UPD_ID					= "";	//수정자
	private String REJECT_DESCRIPTION		= "";	//반려사유
	private String PART						= "";	//파트(01:기계,02:전기,03:시설)
	private String LOCATION					= "";	//파트(01:기계,02:전기,03:시설)
	private String REMARKS					= "";	//파트(01:기계,02:전기,03:시설)
	private String APPR_STATUS				= "";	//결재상태
	
	/** EPMS_REPAIR_RESULT model */
	private String REPAIR_RESULT			= "";	//정비실적 ID
	private String REPAIR_TYPE				= "";	//정비유형
	private String MANAGER					= "";	//배정자
	private String REPAIR_STATUS			= "";	//정비상태(01:접수, 02:배정완료, 03:정비완료)
	private String DATE_PLAN				= "";	//계획일
	
	/** EPMS_WORK_LOG model */
	private String SEQ_WORK_LOG				= "";	//WORK_LOG:작업이력ID
	private String WORK_TYPE				= "";	//작업유형(01:정비, 02:예방보전)
	private String WORK_ID					= "";	//작업대상ID
	private String USER_ID					= "";	//유저ID
	
	/** EPMS_PREVENTIVE_RESULT model */
	private String PREVENTIVE_RESULT		= "";	//예방보전ID		
	
	/** parameter model */
	private String authType					= "";	//권한여부
	private String mainSsLocation			= "";	//소속공장(성남,대구)
	
	/** EPMS_EQUIPMENT model */
	private String SUB_ROOT					= "";	//유저ID
	
	public String getREPAIR_REQUEST() {
		return REPAIR_REQUEST;
	}
	public void setREPAIR_REQUEST(String rEPAIR_REQUEST) {
		REPAIR_REQUEST = rEPAIR_REQUEST;
	}
	public String getAPPR_MEMBER() {
		return APPR_MEMBER;
	}
	public void setAPPR_MEMBER(String aPPR_MEMBER) {
		APPR_MEMBER = aPPR_MEMBER;
	}
	public String getUPD_ID() {
		return UPD_ID;
	}
	public void setUPD_ID(String uPD_ID) {
		UPD_ID = uPD_ID;
	}
	public String getREJECT_DESCRIPTION() {
		return REJECT_DESCRIPTION;
	}
	public void setREJECT_DESCRIPTION(String rEJECT_DESCRIPTION) {
		REJECT_DESCRIPTION = rEJECT_DESCRIPTION;
	}
	public String getPART() {
		return PART;
	}
	public void setPART(String pART) {
		PART = pART;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getREMARKS() {
		return REMARKS;
	}
	public void setREMARKS(String rEMARKS) {
		REMARKS = rEMARKS;
	}
	public String getAPPR_STATUS() {
		return APPR_STATUS;
	}
	public void setAPPR_STATUS(String aPPR_STATUS) {
		APPR_STATUS = aPPR_STATUS;
	}
	public String getREPAIR_RESULT() {
		return REPAIR_RESULT;
	}
	public void setREPAIR_RESULT(String rEPAIR_RESULT) {
		REPAIR_RESULT = rEPAIR_RESULT;
	}
	public String getREPAIR_TYPE() {
		return REPAIR_TYPE;
	}
	public void setREPAIR_TYPE(String rEPAIR_TYPE) {
		REPAIR_TYPE = rEPAIR_TYPE;
	}
	public String getMANAGER() {
		return MANAGER;
	}
	public void setMANAGER(String mANAGER) {
		MANAGER = mANAGER;
	}
	public String getREPAIR_STATUS() {
		return REPAIR_STATUS;
	}
	public void setREPAIR_STATUS(String rEPAIR_STATUS) {
		REPAIR_STATUS = rEPAIR_STATUS;
	}
	public String getDATE_PLAN() {
		return DATE_PLAN;
	}
	public void setDATE_PLAN(String dATE_PLAN) {
		DATE_PLAN = dATE_PLAN;
	}
	public String getSEQ_WORK_LOG() {
		return SEQ_WORK_LOG;
	}
	public void setSEQ_WORK_LOG(String sEQ_WORK_LOG) {
		SEQ_WORK_LOG = sEQ_WORK_LOG;
	}
	public String getWORK_TYPE() {
		return WORK_TYPE;
	}
	public void setWORK_TYPE(String wORK_TYPE) {
		WORK_TYPE = wORK_TYPE;
	}
	public String getWORK_ID() {
		return WORK_ID;
	}
	public void setWORK_ID(String wORK_ID) {
		WORK_ID = wORK_ID;
	}
	public String getUSER_ID() {
		return USER_ID;
	}
	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}
	public String getPREVENTIVE_RESULT() {
		return PREVENTIVE_RESULT;
	}
	public void setPREVENTIVE_RESULT(String pREVENTIVE_RESULT) {
		PREVENTIVE_RESULT = pREVENTIVE_RESULT;
	}
	public String getAuthType() {
		return authType;
	}
	public void setAuthType(String authType) {
		this.authType = authType;
	}
	public String getMainSsLocation() {
		return mainSsLocation;
	}
	public void setMainSsLocation(String mainSsLocation) {
		this.mainSsLocation = mainSsLocation;
	}
	public String getSUB_ROOT() {
		return SUB_ROOT;
	}
	public void setSUB_ROOT(String sUB_ROOT) {
		SUB_ROOT = sUB_ROOT;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
