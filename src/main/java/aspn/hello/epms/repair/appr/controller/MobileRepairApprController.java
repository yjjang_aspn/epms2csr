package aspn.hello.epms.repair.appr.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.JsonUtil;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.com.service.DeviceService;
import aspn.hello.epms.main.model.MainVO;
import aspn.hello.epms.main.service.MainService;
import aspn.hello.epms.repair.appr.model.RepairApprVO;
import aspn.hello.epms.repair.appr.service.RepairApprService;

/**
 * [모바일-고장관리-정비요청결재] Controller Class
 * 
 * @author 김영환
 * @since 2018.04.19
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------		---------------------------
 *   2018.04.19		김영환			최초 생성
 *   2018.05.21		김영환			하드코딩(직접입력)->'상수'로 변경
 *
 * </pre>
 */

@Controller
@RequestMapping("/mobile/epms/repair/appr")
public class MobileRepairApprController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RepairApprService repairApprService;
	
	@Autowired
	AttachService attachService;

	@Autowired
	DeviceService deviceService;

	@Autowired
	MainService mainService;
	
	/**
	 * 정비요청결재
	 * 
	 * @author 김영환
	 * @since 2018.04.19
	 * @param RepairApprVO repairApprVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairApprList.do")
	@ResponseBody
	public Map<String, Object> repairApprList(RepairApprVO repairApprVO){
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			
			// 1. 권한별 공장목록 조회
			MainVO mainVO = new MainVO();
			mainVO.setCompanyId(getSsCompanyId());
			mainVO.setSubAuth(getSsSubAuth());
			
			List<HashMap<String, Object>> locationList = mainService.selectLocationListOpt(mainVO);
			resultMap.put("LOCATION_LIST", locationList);
			
			// 2. 결재 목록 조회
			repairApprVO.setCompanyId(getSsCompanyId());
			repairApprVO.setSubAuth(getSsSubAuth());     //세션_조회권한
			repairApprVO.setSsLocation(getSsLocation()); //세션_공장위치
			repairApprVO.setSsRemarks(getSsRemarks());   //세션_파트 분리/통합 여부
			repairApprVO.setSsPart(getSsPart());         //세션_파트
			//IN절 입력 시 Quoto 처리
			repairApprVO.setLOCATION(addStringToQuoto(repairApprVO.getLOCATION()));
			
			// 팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				repairApprVO.setAuthType(CodeConstants.AUTH_CHIEF);
			}
			// 정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
				repairApprVO.setAuthType(CodeConstants.AUTH_REPAIR);
				
				// 공무(기계, 전기, 시설) 일 경우
				if("BR00_01".equals(getSsPart()) || "BR00_02".equals(getSsPart()) || "BR00_03".equals(getSsPart())){
					repairApprVO.setPART("BR00_01,BR00_02,BR00_03");
				}
				// 생산 일 경우
				else if("BR00_04".equals(getSsPart())){
					repairApprVO.setPART("BR00_04");
				}
				else {
					repairApprVO.setPART(getSsPart());
				}
				repairApprVO.setPART(addStringToQuoto(repairApprVO.getPART()));
			}
			
			resultMap.put("APPR_LIST", repairApprService.selectRepairApprList2(repairApprVO));
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("MobileRepairApprController.repairApprList Error !" + e.toString());
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * 정비요청결재 - 상세
	 * 
	 * @author 김영환
	 * @since 2018.04.19
	 * @param RepairApprVO repairApprVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairApprDtl.do")
	@ResponseBody
	public Map<String, Object> repairApprDtl(RepairApprVO repairApprVO){
				
		Map<String, Object> resultMap = new HashMap<>();		
		try {
			
			if (JsonUtil.isMobileNullParam(resultMap, repairApprVO,	"REPAIR_REQUEST")) {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.PARAM_NULL);
				
			} else {
				
				repairApprVO.setCompanyId(getSsCompanyId());
				repairApprVO.setSsLocation(getSsLocation());
				
				//정비요청 상세 조회
				HashMap<String, Object> APPR_DTL = repairApprService.selectRepairRequestDtl2(repairApprVO);
				if (APPR_DTL == null) {
					JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.NO_DATA_FOUND);
					
				}else {
					resultMap.put("APPR_DTL", APPR_DTL);
					
					if(Integer.parseInt(APPR_DTL.get("FILE_CNT").toString()) > 0){
						AttachVO attachVO = new AttachVO();
						attachVO.setATTACH_GRP_NO(String.valueOf(APPR_DTL.get("ATTACH_GRP_NO")));
						List<HashMap<String, Object>> ATTACH_LIST = attachService.selectFileList(attachVO);
						resultMap.put("ATTACH_LIST", ATTACH_LIST);
					}
					
					//파트 정보(본인 소속파트 제외)
					repairApprVO.setCompanyId(getSsCompanyId());
					repairApprVO.setSsPart(APPR_DTL.get("PART").toString());  //세션_파트
					List<HashMap<String, Object>> PART_LIST = repairApprService.selectPartList2(repairApprVO);
					resultMap.put("PART_LIST", PART_LIST);
										
					//정비원 목록
					repairApprVO.setSubAuth(getSsSubAuth());     						//세션_조회권한
					repairApprVO.setREMARKS(String.valueOf(APPR_DTL.get("REMARKS")));   //정비요청 상세의 파트 분리/통합 여부
//					repairApprVO.setLOCATION(String.valueOf(APPR_DTL.get("LOCATION"))); //정비요청 상세의 공장위치
					repairApprVO.setSUB_ROOT(String.valueOf(APPR_DTL.get("LOCATION"))); //정비요청 상세의 공장위치
					repairApprVO.setPART(String.valueOf(APPR_DTL.get("PART"))); 		//정비요청 상세의 파트
					
					List<HashMap<String, Object>> REPAIR_MEMBER_LIST = repairApprService.selectRepairMemberList(repairApprVO);			
					resultMap.put("REPAIR_MEMBER_LIST", REPAIR_MEMBER_LIST);
					
					// 소속 파트정보 세팅
					resultMap.put("PART", getSsPart());
					
					JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("MobileRepairApprController.repairApprDtl Error !" + e.toString());
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	
	/**
	 * 정비요청결재 - 승인/반려/파트이관
	 * 
	 * @author 김영환
	 * @since 2018.04.19
	 * @param RepairApprVO repairApprVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateRepairAppr.do")
	@ResponseBody
	public HashMap<String, Object> updateRepairAppr(RepairApprVO repairApprVO){
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		try {
			
			repairApprVO.setCompanyId(getSsCompanyId());
			repairApprVO.setSsLocation(getSsLocation());
			repairApprVO.setUserId(getSsUserId());     //세션_사용자ID
			repairApprVO.setSsRemarks(getSsRemarks()); //세션_파트 분리/통합 여부
			repairApprVO.setSsPart(getSsPart());       //세션_파트
			
			//팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				repairApprVO.setAuthType(CodeConstants.AUTH_CHIEF);
			}
			
			//정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
				repairApprVO.setAuthType(CodeConstants.AUTH_REPAIR);
			}
			
			HashMap<String, Object> repairAppr = repairApprService.updateRepairAppr(repairApprVO);
			
			String apprYn = String.valueOf(repairAppr.get("apprYn"));
			HashMap<String, Object> requestDtl = (HashMap<String, Object>) repairAppr.get("requestDtl");
			
			if("Y".equals(apprYn)){
				if (requestDtl != null ) {
					
					// Push 정보
					HashMap<String, Object> pushParam = requestDtl;
					String pushTitle = "";
					
					// 1. 접수(승인)일 경우
					if(repairApprVO.getFlag().equals(CodeConstants.FLAG_APPR)){
						// 1-1. 요청자에게 푸시 전송
						// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
						pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REQUEST);
						// Push Title
						pushTitle = pushParam.get("APPR_MEMBER_NAME").toString() + "님이 정비요청건을 접수하였습니다.";
						pushParam.put("PUSHTITLE", pushTitle);
						// 상세화면 IDX 세팅
						pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
						// 알림음 유형 세팅(01:일반, 02:긴급)
						pushParam.put("PUSHALARM", "01");
						
						deviceService.sendPush(pushParam);
						
						pushParam = new HashMap<String, Object>();
						pushParam = requestDtl;
						
						// 1-2. 담당자 배정시 정비 담당자에게 푸시 전송
						// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
						pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REPAIR);
						// Push Title
						pushTitle = pushParam.get("MANAGER_NAME").toString() + "님이 정비 담당자로 배정되었습니다.";
						pushParam.put("PUSHTITLE", pushTitle);
						// 상세화면 IDX 세팅
						pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
						// 알림음 유형 세팅(01:일반, 02:긴급)
						pushParam.put("PUSHALARM", "01");
						
						deviceService.sendPush(pushParam);
						
					}
					// 2. 반려일 경우 : 요청자에게 푸시 전송
					else if(repairApprVO.getFlag().equals(CodeConstants.FLAG_REJECT)){
						
						// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
						pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REQUEST);
						// Push Title
						pushTitle = pushParam.get("APPR_MEMBER_NAME").toString() + "님이 정비요청건을 반려하였습니다.";
						pushParam.put("PUSHTITLE", pushTitle);
						// 상세화면 IDX 세팅
						pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
						// 알림음 유형 세팅(01:일반, 02:긴급)
						pushParam.put("PUSHALARM", "01");

						deviceService.sendPush(pushParam);
						
					}
					// 3. 파트이관일 경우
					else if(repairApprVO.getFlag().equals(CodeConstants.FLAG_UPDATE)){
						
						// 3-1. 요청자에게 푸시 전송
						// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
						pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REQUEST);
						// Push Title
						pushTitle = "정비요청건이 " + pushParam.get("PART_NAME").toString() +" 파트로 이관되었습니다.";
						pushParam.put("PUSHTITLE", pushTitle);
						// 상세화면 IDX 세팅
						pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
						// 알림음 유형 세팅(01:일반, 02:긴급)
						pushParam.put("PUSHALARM", "01");

						deviceService.sendPush(pushParam);
						
						pushParam = new HashMap<String, Object>();
						pushParam = requestDtl;
						
						// 3-2. 이관파트 전체 정비원에게 푸시 전송
						// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
						pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_APPR);
						// Push Title
						pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비를 요청하였습니다.(파트이관)";
						pushParam.put("PUSHTITLE", pushTitle);
						// 상세화면 IDX 세팅
						pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
						// 알림음 유형 세팅(01:일반, 02:긴급)
						pushParam.put("PUSHALARM", "01");

						deviceService.sendPush(pushParam);
						
					}
					
					JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);				
				}
			}
			else {
				if (requestDtl != null ) {
					if(String.valueOf(requestDtl.get("APPR_STATUS")).equals(CodeConstants.EPMS_APPR_STATUS_APPR)){
						resultMap.put("RES_CD", CodeConstants.ALEADY_DATA);
						resultMap.put("RES_MSG", "이미 승인된 요청건입니다.");
					}
					else if(String.valueOf(requestDtl.get("APPR_STATUS")).equals(CodeConstants.EPMS_APPR_STATUS_REJECT)){
						resultMap.put("RES_CD", CodeConstants.ALEADY_DATA);
						resultMap.put("RES_MSG", "이미 반려된 요청건입니다.");
					}
					else{
						resultMap.put("RES_CD", CodeConstants.ALEADY_DATA);
						resultMap.put("RES_MSG", "이미 처리된 요청건입니다.");
					}
				}
				else{
					resultMap.put("RES_CD", CodeConstants.ALEADY_DATA);
					resultMap.put("RES_MSG", "요청건이 존재하지 않습니다.");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("MobileRepairApprController.updateRepairAppr Error !" + e.toString());
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * 모바일 - Oracle IN절 입력 시 Quoto처리 
	 * 
	 * @author 김영환
	 * 
	 * @since 2018.03.06
	 * @param String 
	 * @return String
	 * @throws Exception
	 */
	public String addStringToQuoto(String originString){
		
		if("".equals(originString) || originString == null) {
			return "\'\'";
		}
		else if("ALL".equals(originString)) {
			return "ALL";
		} 
		else {
			String[] tempArray = originString.split(",");
			StringBuffer tempBuffer = new StringBuffer();
			
			if(tempArray.length == 1) {
				tempBuffer.append("(\'"+tempArray[0]+"\')");
			} else {
				for(int i=0; i < tempArray.length; i++){
					if(i == 0)							tempBuffer.append("(\'"+tempArray[i]+"\', ");	// 시작값
					else if( i == tempArray.length-1)	tempBuffer.append("\'"+tempArray[i]+"\')");		// 마지막값
					else 								tempBuffer.append("\'"+tempArray[i]+"\', ");	// 중간값
				}
			}
			
			return tempBuffer.toString();
		}
	}

}
