package aspn.hello.epms.repair.appr.mapper;
        
import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.repair.appr.model.RepairApprVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [고장관리-정비요청결재] Mapper Class
 * 
 * @author 김영환
 * @since 2018.03.05
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.05		김영환			최초 생성
 *   2018.04.18		김영환			모바일 최초 생성
 *   
 * </pre>
 */

@Mapper("repairApprMapper")
public interface RepairApprMapper {
	
	/**
	 * 결재현황 목록
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairApprVO repairApprVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> selectRepairApprList(RepairApprVO repairApprVO);
	
	/**
	 * 정비요청 상세
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairApprVO repairApprVO
	 * @return HashMap<String, Object>
	 */
	public HashMap<String, Object> selectRepairRequestDtl(RepairApprVO repairApprVO);
	
	/**
	 * 파트 목록-본인 소속파트 제외
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairApprVO repairApprVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> selectPartList(RepairApprVO repairApprVO);
	
	/**
	 * 결재-승인,반려,파트이관
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairApprVO repairApprVO
	 * @return int
	 */
	public int selectRepairAppr(RepairApprVO repairApprVO);
	
	/**
	 * 정비요청 결재 - 승인
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairApprVO repairApprVO
	 * @return int
	 */
	public int updateRepairAppr1(RepairApprVO repairApprVO);
	
	/**
	 * 정비실적 PK값 조회
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairApprVO repairApprVO
	 * @return int
	 */
	public int selectRepairSeq(RepairApprVO repairApprVO);
	
	/**
	 * 신규 정비건 등록
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairApprVO repairApprVO
	 * @return int
	 */
	public int insertRepairResult(RepairApprVO repairApprVO);
	
	/**
	 * 정비 목록 수정 -정비계획일, 정비원, 배정자 등록
	 * 
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairPlanVO repairPlanVO2
	 */
	public void updateRepairPlan(RepairApprVO repairApprVO);
	
	/**
	 * 정비요청결재 - 반려
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairApprVO repairApprVO
	 * @return int
	 */
	public int updateRepairAppr2(RepairApprVO repairApprVO);
	
	/**
	 * 정비요청결재 - 파트이관
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairApprVO repairApprVO
	 * @return int
	 */
	public int updateRepairAppr3(RepairApprVO repairApprVO);
	
	
	///////////////////////
	///		모바일		///
	///////////////////////
	
	/**
	 * 모바일 - 결재현황 건수 (메인화면 뱃지카운트)
	 * 
	 * @author 서정민
	 * @since 2018.05.24
	 * @param RepairApprVO repairApprVO
	 * @return int
	 */
	public int selectRepairApprCnt(RepairApprVO repairApprVO);
	
	/**
	 * 모바일 - 결재현황 목록
	 * 
	 * @author 김영환
	 * @since 2018.04.18
	 * @param RepairApprVO repairApprVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> selectRepairApprList2(RepairApprVO repairApprVO);

	
	/**
	 * 모바일  - 정비요청 상세
	 * 
	 * @author 김영환
	 * @since 2018.04.18
	 * @param RepairApprVO repairApprVO
	 * @return HashMap<String, Object>
	 */
	public HashMap<String, Object> selectRepairRequestDtl2(RepairApprVO repairApprVO);

	/**
	 * 정비파트원 목록-해당LOCATION, PART의 정비원
	 *
	 * @author 김영환
	 * @since 2018.04.27
	 * @param RepairApprVO repairApprVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> selectRepairMemberList(RepairApprVO repairApprVO);

	/**
	 * 모바일 - 파트 정보(본인 소속파트 제외)
	 *
	 * @author 김영환
	 * @since 2018.04.19
	 * @param RepairApprVO repairApprVO
	 * @return HashMap<String, Object>
	 */
	public List<HashMap<String, Object>> selectPartList2(RepairApprVO repairApprVO);

	/**
	 * 모바일 - 결재:반려시 예방정비 완료처리
	 *
	 * @author 김영환
	 * @since 2018.05.14
	 * @param RepairApprVO repairApprVO
	 * @return HashMap<String, Object>
	 */
	public int updatePreventiveResult(RepairApprVO repairApprVO);
	
}
