package aspn.hello.epms.repair.appr.service;
        
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.config.CodeConstants;
import aspn.hello.com.service.DeviceService;
import aspn.hello.epms.repair.appr.mapper.RepairApprMapper;
import aspn.hello.epms.repair.appr.model.RepairApprVO;
import aspn.hello.epms.repair.result.mapper.RepairResultMapper;
import aspn.hello.epms.repair.result.model.RepairResultVO;

/**
 * [고장관리-정비요청결재] Business Implement Class
 * 
 * @author 김영환
 * @since 2018.03.05
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------		--------------------------------------------
 *   2018.03.05		김영환			최초 생성
 *   2018.04.19		김영환			모바일 최초 생성
 *   2018.04.30		김영환			정비요청 결재-승인: 정비계획일 등록, 담당자(정비원) 배정
 *   2018.05.21		김영환			하드코딩(직접입력)->'상수'로 변경
 *   
 * </pre>
 */

@Service
public class RepairApprServiceImpl implements RepairApprService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RepairApprMapper repairApprMapper;
	
	@Autowired
	DeviceService deviceService;
	
	@Autowired
	RepairResultMapper repairResultMapper;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 결재현황 목록
	 *
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairApprVO repairApprVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairApprList(RepairApprVO repairApprVO){
		return repairApprMapper.selectRepairApprList(repairApprVO);
	}
	
	/**
	 * 정비요청 상세
	 *
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairApprVO repairApprVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> selectRepairRequestDtl(RepairApprVO repairApprVO){
		return repairApprMapper.selectRepairRequestDtl(repairApprVO);
	}	
		
	/**
	 * 파트 목록-본인 소속파트 제외
	 *
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairApprVO repairApprVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectPartList(RepairApprVO repairApprVO){
		return repairApprMapper.selectPartList(repairApprVO);
	}
	
	/**
	 * 정비요청 결재-승인,반려,파트이관 (모바일, 웹 공통)
	 *
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairApprVO repairApprVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> updateRepairAppr(RepairApprVO repairApprVO){
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		int	cnt = repairApprMapper.selectRepairAppr(repairApprVO);
		if(cnt==1){
			resultMap.put("apprYn", "Y");
			
			if(repairApprVO.getFlag().equals(CodeConstants.FLAG_APPR)){  //정비요청 - 승인 
				repairApprMapper.updateRepairAppr1(repairApprVO);
				
				//정비실적 SEQ 체크(SEQ_REPAIR 체크)
				int SEQ_REPAIR;
				SEQ_REPAIR = repairApprMapper.selectRepairSeq(repairApprVO);	
				repairApprVO.setREPAIR_RESULT(Integer.toString(SEQ_REPAIR));
				//신규정비건 등록
				repairApprMapper.insertRepairResult(repairApprVO);
				
				if("".equals(repairApprVO.getMANAGER()) || repairApprVO.getMANAGER() == null)
				{
					repairApprVO.setREPAIR_STATUS(CodeConstants.EPMS_REPAIR_STATUS_RECEIVE); //정비실적 정비상태 :접수
				}else {
					repairApprVO.setREPAIR_STATUS(CodeConstants.EPMS_REPAIR_STATUS_PLAN);    //정비실적 정비상태 :배정완료
				}
				//정비계획일, 정비원, 배정자 등록
				repairApprMapper.updateRepairPlan(repairApprVO);
				
				repairApprVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //작업유형
				repairApprVO.setWORK_ID(repairApprVO.getREPAIR_RESULT());       //작업대상ID 세팅
				repairApprVO.setUSER_ID(repairApprVO.getMANAGER()); 	        //담당자ID 세팅
				
				if(!("".equals(repairApprVO.getMANAGER()) || repairApprVO.getMANAGER() == null)){
					
					RepairResultVO repairResultVO = new RepairResultVO();
					
					// WORK_LOG SEQ 조회
					String workLogSeq = repairResultMapper.selectWorkLogSeq(repairResultVO);
					repairResultVO.setWORK_LOG(workLogSeq);
					
					repairResultVO.setWORK_ID(repairApprVO.getREPAIR_RESULT());
					repairResultVO.setUSER_ID(repairApprVO.getMANAGER());
					repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR);
					repairResultVO.setUserId(repairApprVO.getUserId());
					
					//정비원 등록
					repairResultMapper.insertWorkLog(repairResultVO);
					
					// 현재 날짜
					Date today = new Date();
					SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
					String startDt = date.format(today);
					repairResultVO.setDATE_START(startDt);
					repairResultVO.setDATE_END(startDt);
					repairResultVO.setITEM_DESCRIPTION("");
					
					// 기본 정비실적 시간
					repairResultMapper.insertWorkLogItem(repairResultVO);
					
				}
					
			}
			else if(repairApprVO.getFlag().equals(CodeConstants.FLAG_REJECT)){  //정비요청 - 반려
				
				repairApprMapper.updateRepairAppr2(repairApprVO);				
				if (!("".equals(repairApprVO.getPREVENTIVE_RESULT()) || repairApprVO.getPREVENTIVE_RESULT() == null)) {
					repairApprMapper.updatePreventiveResult(repairApprVO);					
				}
			} 
			else if(repairApprVO.getFlag().equals(CodeConstants.FLAG_UPDATE)){ //정비요청 - 파트이관
				
				repairApprMapper.updateRepairAppr3(repairApprVO);
			}
		}
		else{
			resultMap.put("apprYn", "N");
		}
		
		resultMap.put("requestDtl", repairApprMapper.selectRepairRequestDtl(repairApprVO));
		
		return resultMap;
	}
	
	/**
	 * 정비파트원 목록-해당LOCATION, PART의 정비원
	 *
	 * @author 김영환
	 * @since 2018.04.27
	 * @param RepairApprVO repairApprVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairMemberList(RepairApprVO repairApprVO) {
		return repairApprMapper.selectRepairMemberList(repairApprVO);
	}
	
	
	

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - 결재현황 건수 (메인화면 뱃지카운트)
	 *
	 * @author 서정민
	 * @since 2018.05.24
	 * @param RepairApprVO repairApprVO
	 * @return int
	 */
	@Override
	public int selectRepairApprCnt(RepairApprVO repairApprVO) {
		return repairApprMapper.selectRepairApprCnt(repairApprVO);
	}

	/**
	 * 모바일 - 결재현황 목록
	 *
	 * @author 김영환
	 * @since 2018.04.19
	 * @param RepairApprVO repairApprVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairApprList2(RepairApprVO repairApprVO) {
		return repairApprMapper.selectRepairApprList2(repairApprVO);
	}

	/**
	 * 모바일 - 정비요청 상세
	 *
	 * @author 김영환
	 * @since 2018.04.19
	 * @param RepairApprVO repairApprVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> selectRepairRequestDtl2(RepairApprVO repairApprVO) {
		return repairApprMapper.selectRepairRequestDtl2(repairApprVO);
	}

	/**
	 * 모바일 - 파트 정보(본인 소속파트 제외)
	 *
	 * @author 김영환
	 * @since 2018.04.19
	 * @param RepairApprVO repairApprVO
	 * @return HashMap<String, Object>
	 */
	@Override
	public List<HashMap<String, Object>> selectPartList2(RepairApprVO repairApprVO) {
		return repairApprMapper.selectPartList2(repairApprVO);
	}
	
		
}
