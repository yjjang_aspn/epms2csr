package aspn.hello.epms.repair.appr.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.com.service.DeviceService;
import aspn.hello.epms.repair.appr.model.RepairApprVO;
import aspn.hello.epms.repair.appr.service.RepairApprService;

/**
 * [고장관리-정비요청결재] Controller Class
 * 
 * @author 김영환
 * @since 2018.03.05
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.05		김영환			최초 생성
 *   2018.04.30		김영환			정비요청 결재-승인:정비계획일, 정비원등록 생성
 *   2018.05.21		김영환			하드코딩(직접입력)->'상수'로 변경
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/repair/appr")
public class RepairApprController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RepairApprService repairApprService;
	
	@Autowired
	AttachService attachService;
	
	@Autowired
	DeviceService deviceService;
	
	/**
	 * 정비요청결재 현황
	 *
	 * @author 김영환
	 * @since 2018.03.05
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */                       
	@RequestMapping(value = "/repairApprList.do")
	public String repairApprList(Model model) throws Exception {
		model.addAttribute("startDt", DateTime.requestMmDay("1","yyyy-MM-dd"));
		model.addAttribute("endDt", DateTime.getDateString());
		return "/epms/repair/appr/repairApprList";
	}
	
	/** 
	 * 정비요청결재 - 결재현황 Grid Layout
	 *
	 * @author 김영환
	 * @since 2018.03.05
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairApprListLayout.do")
	public String repairApprListLayout() throws Exception {
		return "/epms/repair/appr/repairApprListLayout";
	}
	
	/**
	 * 정비요청결재 - 결재현황 Grid Data
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairApprVO repairApprVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairApprListData.do")
	public String repairApprListData(RepairApprVO repairApprVO, Model model){
		try {
			repairApprVO.setSubAuth(getSsSubAuth());     //세션_조회권한
			repairApprVO.setSsLocation(getSsLocation()); //세션_공장위치
			repairApprVO.setSsRemarks(getSsRemarks());   //세션_파트 분리/통합 여부
			repairApprVO.setSsPart(getSsPart());         //세션_파트
			repairApprVO.setCompanyId(getSsCompanyId()); //세션_공장id
			
			//팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				repairApprVO.setAuthType(CodeConstants.AUTH_CHIEF);
			}
			//정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
				repairApprVO.setAuthType(CodeConstants.AUTH_REPAIR);
			}
			
			//결재현황 목록
			List<HashMap<String, Object>> list = repairApprService.selectRepairApprList(repairApprVO);
			model.addAttribute("list", list);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("RepairApprController.repairApprListData Error !" + e.toString());
		}
		
		return "/epms/repair/appr/repairApprListData";
	}
	
	/**
	 * 정비요청결재 - 정비요청서 상세 Ajax
	 * 
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairApprVO repairApprVO
	 * @param AttachVO attachVO
	 * @param Model model
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairRequestDtlAjax.do")
	@ResponseBody
	public Map<String, Object> repairRequestDtlAjax(RepairApprVO repairApprVO, AttachVO attachVO, Model model){
		Map<String, Object> map= new HashMap<String, Object>();
		try {
			
			repairApprVO.setCompanyId(getSsCompanyId());
			repairApprVO.setSsLocation(getSsLocation());
			
			//정비요청 상세 조회
			HashMap<String, Object> repairRequestDtl = repairApprService.selectRepairRequestDtl(repairApprVO);			
			map.put("repairRequestDtl", repairRequestDtl);
			
			//첨부파일
			if(!("".equals(repairRequestDtl.get("ATTACH_GRP_NO")) || repairRequestDtl.get("ATTACH_GRP_NO") == null || "0".equals(String.valueOf(repairRequestDtl.get("FILE_CNT"))) ) ){				
				attachVO.setATTACH_GRP_NO(String.valueOf(repairRequestDtl.get("ATTACH_GRP_NO")));
				List<HashMap<String, Object>> attachList = attachService.selectFileList(attachVO);
				map.put("attachList", attachList);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("RepairApprController.repairRequestDtlAjax Error !" + e.toString());
		}
		
		return map;
	}
	
	/**
	 * 정비요청결재 - 파트이관 팝업
	 * 
	 * @author 김영환
	 * @since 2018.02.28
	 * @param RepairApprVO repairApprVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popApprChangeForm.do")
	public String popApprChangeForm(RepairApprVO repairApprVO, Model model){
		try {
			
			repairApprVO.setCompanyId(getSsCompanyId());
			
			//파트 정보(본인 소속파트 제외)
			List<HashMap<String, Object>> partList = repairApprService.selectPartList(repairApprVO);
			model.addAttribute("partList", partList);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("RepairApprController.popApprChangeForm Error !" + e.toString());
		}
		
		return "/epms/repair/appr/popApprChangeForm";
	}
	
	/**
	 * 정비요청결재 - 반려 팝업
	 * 
	 * @author 김영환
	 * @since 2018.02.28
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popApprRejectForm.do")
	public String popApprRejectForm() throws Exception {		
		return "/epms/repair/appr/popApprRejectForm";
	}
	
	/**
	 * 정비요청결재 - 승인 팝업
	 * 
	 * @author 김영환
	 * @since 2018.02.28
	 * @param RepairApprVO repairApprVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popApprOkForm.do")
	public String popApprOkForm(RepairApprVO repairApprVO, Model model) {
		try {
			repairApprVO.setSubAuth(getSsSubAuth());   //세션_조회권한
			
			//정비 요청 상세의 공장, 파트를 조회하여 정비원 목록 출력
			List<HashMap<String, Object>> REPAIR_MEMBER_LIST = repairApprService.selectRepairMemberList(repairApprVO);
			model.addAttribute("REPAIR_MEMBER_LIST", REPAIR_MEMBER_LIST);
			model.addAttribute("today", DateTime.getDateString());
			model.addAttribute("userId", getSsUserId());
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("RepairApprController.popApprChangeForm Error !" + e.toString());
		}
		
		return "/epms/repair/appr/popApprOkForm";
	}
	
	/**
	 * 정비요청결재 - 결재(승인,반려,파트이관) 
	 * 
	 * @author 김영환
	 * @since 2018.02.28
	 * @param RepairApprVO repairApprVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairApprAjax.do")
	@ResponseBody
	public HashMap<String, Object> repairApprAjax(RepairApprVO repairApprVO){
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		try {
			
			repairApprVO.setCompanyId(getSsCompanyId());
			repairApprVO.setSsLocation(getSsLocation());
			repairApprVO.setUserId(getSsUserId());
			repairApprVO.setSsRemarks(getSsRemarks());   //세션_파트 분리/통합 여부
			repairApprVO.setSsPart(getSsPart());         //세션_파트
			
			//팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				repairApprVO.setAuthType(CodeConstants.AUTH_CHIEF);
			}
			
			//정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
				repairApprVO.setAuthType(CodeConstants.AUTH_REPAIR);
			}
			
			rtnMap = repairApprService.updateRepairAppr(repairApprVO);
			
			String apprYn = String.valueOf(rtnMap.get("apprYn"));
			HashMap<String, Object> requestDtl = (HashMap<String, Object>) rtnMap.get("requestDtl");
			
			if("Y".equals(apprYn)){
				if(requestDtl != null) {
					// Push 정보
					HashMap<String, Object> pushParam = requestDtl;
					String pushTitle = "";
					
					// 1. 접수(승인)일 경우 
					if(repairApprVO.getFlag().equals(CodeConstants.FLAG_APPR)){
						
						// 1-1. 요청자에게 푸시 전송
						// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
						pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REQUEST);
						// Push Title
						pushTitle = pushParam.get("APPR_MEMBER_NAME").toString() + "님이 정비요청건을 접수하였습니다.";
						pushParam.put("PUSHTITLE", pushTitle);
						// 상세화면 IDX 세팅
						pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
						// 알림음 유형 세팅(01:일반, 02:긴급)
						pushParam.put("PUSHALARM", "01");
						
						deviceService.sendPush(pushParam);
						
						pushParam = new HashMap<String, Object>();
						pushParam = requestDtl;
						
						// 1-2. 담당자 배정시 정비 담당자에게 푸시 전송
						// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
						pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REPAIR);
						// Push Title
						pushTitle = pushParam.get("MANAGER_NAME").toString() + "님이 정비 담당자로 배정되었습니다.";
						pushParam.put("PUSHTITLE", pushTitle);
						// 상세화면 IDX 세팅
						pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
						// 알림음 유형 세팅(01:일반, 02:긴급)
						pushParam.put("PUSHALARM", "01");
						
						deviceService.sendPush(pushParam);
						
					}
					// 2. 반려일 경우 : 요청자에게 푸시 전송
					else if(repairApprVO.getFlag().equals(CodeConstants.FLAG_REJECT)){
						
						// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
						pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REQUEST);
						// Push Title
						pushTitle = pushParam.get("APPR_MEMBER_NAME").toString() + "님이 정비요청건을 반려하였습니다.";
						pushParam.put("PUSHTITLE", pushTitle);
						// 상세화면 IDX 세팅
						pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
						// 알림음 유형 세팅(01:일반, 02:긴급)
						pushParam.put("PUSHALARM", "01");

						deviceService.sendPush(pushParam);
						
					}
					// 3. 파트이관일 경우
					else if(repairApprVO.getFlag().equals(CodeConstants.FLAG_UPDATE)){
						
						// 3-1. 요청자에게 푸시 전송
						// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
						pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REQUEST);
						// Push Title
						pushTitle = "정비요청건이 " + pushParam.get("PART_NAME").toString() +" 파트로 이관되었습니다.";
						pushParam.put("PUSHTITLE", pushTitle);
						// 상세화면 IDX 세팅
						pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
						// 알림음 유형 세팅(01:일반, 02:긴급)
						pushParam.put("PUSHALARM", "01");

						deviceService.sendPush(pushParam);
						
						pushParam = new HashMap<String, Object>();
						pushParam = requestDtl;
						
						// 3-2. 이관파트 전체 정비원에게 푸시 전송
						// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
						pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_APPR);
						// Push Title
						pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비를 요청하였습니다.(파트이관)";
						pushParam.put("PUSHTITLE", pushTitle);
						// 상세화면 IDX 세팅
						pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
						// 알림음 유형 세팅(01:일반, 02:긴급)
						pushParam.put("PUSHALARM", "01");

						deviceService.sendPush(pushParam);
						
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("RepairApprController.repairApprAjax Error !" + e.toString());
		}
		
		return rtnMap;
	}

}
