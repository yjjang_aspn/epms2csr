package aspn.hello.epms.repair.result.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.repair.result.model.RepairResultVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [고장관리-정비실적등록/조회] Mapper Class
 * 
 * @author 김영환
 * @since 2018.03.12
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.12		김영환		최초 생성
 *   
 * </pre>
 */

@Mapper("repairResultMapper")
public interface RepairResultMapper {

	/**
	 * 정비실적현황 조회
	 * 
	 * @author 김영환
	 * @since 2018.02.20
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairResultList(RepairResultVO repairResultVO);

	/**
	 * 정비실적 상세
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, String>>
	 */
	HashMap<String, Object> selectRepairResultDtl(RepairResultVO repairResultVO);

	/**
	 * 설비 구성목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectEquipmentBomList(RepairResultVO repairResultVO);

	/**
	 * 정비실적에 등록된 자재 목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairUsedMaterialList(RepairResultVO repairResultVO);

	/**
	 * 정비실적,고장원인분석에 등록된 정비원 목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectWorkMemberList(RepairResultVO repairResultVO);

	/**
	 * 설비 첨부이미지 목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectAttachList(RepairResultVO repairResultVO);

	/**
	 * 고장유형 목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectTroubleList1();

	/**
	 * 조치 목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectTroubleList2();

	/**
	 * 정비실적등록 출퇴근 현황 리스트
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectWorkerList(RepairResultVO repairResultVO);

	/**
	 * 파트 목록 (본인 소속파트 제외)
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectPartOptList(RepairResultVO repairResultVO);

	/**
	 * 정비실적 등록여부 체크
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return int
	 */
	int selectRepairResultCheck(RepairResultVO repairResultVO);

	/**
	 * 기존 사용자재 출고수량 조회
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return int
	 */
	int selectMaterialInoutQnty(RepairResultVO repairResultVO1);

	/**
	 * 출고내역 수정
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return int
	 */
	int updateMaterialInoutList(RepairResultVO repairResultVO1);

	/**
	 * 자재 재고수량 반영(출고)
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return int
	 */
	int updateMaterialStockOut(RepairResultVO repairResultVO1);

	/**
	 * 주기전 고장건 등록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return int
	 */
	int updateMaintenanceSuccess(RepairResultVO repairResultVO1);

	/**
	 * 입출고 헤더 PK값 조회
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return int
	 */
	int selectMaterialInoutSeq(RepairResultVO repairResultVO);

	/**
	 * 입출고 헤더 등록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return int
	 */
	int insertMaterialInout(RepairResultVO repairResultVO);

	/**
	 * 입출고 아이템 등록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return int
	 */
	int insertMaterialInoutItem(RepairResultVO repairResultVO1);

	/**
	 * 등록된 정비원목록에서 빠진 정비원 삭제
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return int
	 */
	int deleteWorkLog(RepairResultVO repairResultVO);

	/**
	 * 정비원 등록여부 체크
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return int
	 */
	int selectWorkLogCheck(RepairResultVO repairResultVO);
	
	/**
	 * 정비원 등록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return int
	 */
	int insertWorkLog(RepairResultVO repairResultVO);
	
	/**
	 * 정비실적등록/조회 - 정비원 실적 아이템 저장
	 *
	 * @author 김영환
	 * @since 2018.12.20
	 * @param RepairResultVO repairResultVO1 
	 */
	int insertWorkLogItem(RepairResultVO repairResultVO1);
	
	/**
	 * WORK_LOG테이블 SEQ조회
	 *
	 * @author 김영환
	 * @since 2018.12.21
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	String selectWorkLogSeq(RepairResultVO repairResultVO);
	
	/**
	 * 정비원 정비 실적 ITEM 조회
	 * 
	 * @author 김영환
	 * @since 2018.12.20
	 * @param RepairResultVO repairResultVO1
	 */
	List<HashMap<String, Object>> selectWorkLogItemList(RepairResultVO repairResultVO);

	/**
	 * 정비실적 임시저장/정비완료등록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return int
	 */
	int updateRepairResult(RepairResultVO repairResultVO);

	/**
	 * 정비실적 첨부파일 수정
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	void updateRepairResultAttach(RepairResultVO repairResultVO);
	
	/**
	 * 확정여부 업데이트
	 *
	 * @author 김영환
	 * @since 2019.01.04
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	int updateWorkLogStatus(RepairResultVO repairResultVO);
	
	/**
	 * 정비원 개별 실적 등록 여부 확인
	 *
	 * @author 김영환
	 * @since 2019.03.06
	 * @param RepairResultVO repairResultVO
	 * @return int
	 */
	int selectWorkLogItemCheck(RepairResultVO repairResultVO);
	
	/**
	 * 정비실적 - 정비원실적상태 상태 update 
	 *
	 * @author 김영환
	 * @since 2019.03.05
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	void updateWorkLogItem(RepairResultVO repairResultVO);
	
	/**
	 * 개인실적 완료여부 확인
	 *
	 * @author 서정민
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return HashMap<String, Object>
	 */
	HashMap<String, Object> workCompleteCheck(RepairResultVO repairResultVO);
	
	/**
	 * 정비시간 확인
	 *
	 * @author 서정민
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return HashMap<String, Object>
	 */
	HashMap<String, Object> repairTimeCheck(RepairResultVO repairResultVO);
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	/**
	 * 모바일 - 정비실적 건수 (메인화면 뱃지카운트)
	 *
	 * @author 서정민
	 * @since 2018.05.24
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	int selectRepairResultCnt(RepairResultVO repairResultVO);

	/**
	 * 모바일 - 정비실적 목록조회
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairResultList2(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 정비실적 상세조회
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	HashMap<String, Object> selectRepairResultDtl2(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 정비원 정비 실적 ITEM 조회
	 * 
	 * @author 김영환
	 * @since 2018.12.20
	 * @param RepairResultVO repairResultVO1
	 */
	List<HashMap<String, Object>> selectWorkLogItemList2(RepairResultVO repairResultVO);

	/**
	 * 모바일 - 본인소속 제외 파트목록
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectPartOptList2(RepairResultVO repairResultVO);

	/**
	 * 모바일 - 공통코드(정비유형목록 조회)
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	List<HashMap<String, Object>> selectRepairTypeList(RepairResultVO repairResultVO);

	/**
	 * 모바일 - 공통코드(고장유형목록 조회)
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectTroubleType1List();

	/**
	 * 모바일 - 공통코드(조치목록 조회)
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectTroubleType2List();
	
	/**
	 * 모바일 - 공통코드([SAP-코딩그룹]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectCatalogDGrpList(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 공통코드([SAP-코딩]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectCatalogDList(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 공통코드([SAP-대상그룹]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectCatalogBGrpList(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 공통코드([SAP-대상]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectCatalogBList(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 공통코드([SAP-손상그룹]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectCatalogCGrpList(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 공통코드([SAP-손상]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectCatalogCList(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 공통코드([SAP-원인그룹]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectCatalog5GrpList(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 공통코드([SAP-원인]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectCatalog5List(RepairResultVO repairResultVO);

	/**
	 * 돌발정비일 경우, 고장원인분석 생성
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	void insertRepairAnalysis(RepairResultVO repairResultVO);

	/**
	 * 예방정비의 경우, 점검상태(완료) 및 점검결과(NY) 업데이트
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	void updatePreventiveResult(RepairResultVO repairResultVO);

	/**
	 * 모바일 - 정비원 등록 시 정비원 목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairMemberList(RepairResultVO repairResultVO);

	/**
	 * 정비실적등록/조회 - 정비완료 건 승인으로 상태값 변경
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	void updateCompleteCancelStatus(RepairResultVO repairResultVO);

	/**
	 * 정비실적등록/조회 - 정비실적 내 임시저장된 사용자재 삭제 처리
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	void updateMaterialInoutDelYn(RepairResultVO repairResultVO1);

	/**
	 * 사용자정보
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */	
	String selectMemberIdInfo(RepairResultVO repairResultVO);
	
	/**
	 * 정비실적등록/조회 - 정비실적 등록 후 RFC에서 return 해준 자재 단가 update 처리
	 *
	 * @author 김영환
	 * @since 2019.02.14
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	void updateMaterialInoutItem(RepairResultVO repairResultVO1);
	
	/**
	 * 정비실적 - 정비원 등록 상태 update 
	 *
	 * @author 김영환
	 * @since 2019.03.05
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	int updateWorkLog(RepairResultVO repairResultVO);
	
	/**
	 * 작업인원 및 총작업시간
	 *
	 * @author 김영환
	 * @since 2019.03.08
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	HashMap<String, Object> selectWorkLogItemSum(RepairResultVO repairResultVO);
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// SAP
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * SAP 작업오더확정시 처리
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */	
	void updateAfterSAPOrder(RepairResultVO repairResultVO);

}
