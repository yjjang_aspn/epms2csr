package aspn.hello.epms.repair.result.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.JsonUtil;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.com.service.DeviceService;
import aspn.hello.epms.main.model.MainVO;
import aspn.hello.epms.main.service.MainService;
import aspn.hello.epms.preventive.result.model.PreventiveResultVO;
import aspn.hello.epms.preventive.result.service.PreventiveResultService;
import aspn.hello.epms.repair.request.model.RepairRequestVO;
import aspn.hello.epms.repair.request.service.RepairRequestService;
import aspn.hello.epms.repair.result.model.RepairResultVO;
import aspn.hello.epms.repair.result.service.RepairResultService;

/**
 * [모바일 - 고장관리-정비실적등록/조회] Controller Class
 * @author 김영환
 * @since 2018.03.12
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.12		김영환			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/mobile/epms/repair/result")
public class MobileRepairResultController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	RepairResultService repairResultService;
	
	@Autowired
	RepairRequestService repairRequestService;
	
	@Autowired
	PreventiveResultService preventiveResultService;
	
	@Autowired
	AttachService attachService;

	@Autowired
	DeviceService deviceService;
	
	@Autowired
	MainService mainService;
	
	/**
	 * 모바일 - 정비실적조회 목록
	 *
	 * @author 서정민
	 * @since 2019.03.05
	 * @param
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairResultList.do")
	@ResponseBody
	public Map<String, Object> repairResultList(RepairResultVO repairResultVO, Model model) throws Exception{
		
		Map<String, Object> resultMap = new HashMap<>();
		
		try{
			
			// 1. 권한별 공장목록 조회
			MainVO mainVO = new MainVO();
			mainVO.setCompanyId(getSsCompanyId());
			mainVO.setSubAuth(getSsSubAuth());
			
			List<HashMap<String, Object>> locationList = mainService.selectLocationListOpt(mainVO);
			resultMap.put("LOCATION_LIST", locationList);
			
			// 2. 정비현황 목록
			repairResultVO.setCompanyId(getSsCompanyId());
//			repairResultVO.setSsLocation(getSsLocation());	//세션_공장위치
			repairResultVO.setSubAuth(getSsSubAuth());		//세션_조회권한
			repairResultVO.setSsRemarks(getSsRemarks());	//세션_파트 분리/통합 여부
			repairResultVO.setSsPart(getSsPart());			//세션_파트
			repairResultVO.setUserId(getSsUserId());		//세션_아이디
			
			repairResultVO.setLOCATION(addStringToQuoto(repairResultVO.getLOCATION()));
			repairResultVO.setAPPR_STATUS(addStringToQuoto(repairResultVO.getAPPR_STATUS()));
			repairResultVO.setREPAIR_STATUS(addStringToQuoto(repairResultVO.getREPAIR_STATUS()));
			repairResultVO.setREQUEST_TYPE(addStringToQuoto(repairResultVO.getREQUEST_TYPE()));
			
			// 팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				repairResultVO.setAuthType(CodeConstants.AUTH_CHIEF);
			}
			// 정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
				repairResultVO.setAuthType(CodeConstants.AUTH_REPAIR);
				
				// 공무(기계, 전기, 시설) 일 경우
				if("BR00_01".equals(getSsPart()) || "BR00_02".equals(getSsPart()) || "BR00_03".equals(getSsPart())){
					repairResultVO.setPART("BR00_01,BR00_02,BR00_03");
				}
				// 생산 일 경우
				else if("BR00_04".equals(getSsPart())){
					repairResultVO.setPART("BR00_04");
				}
				else {
					repairResultVO.setPART(getSsPart());
				}
				repairResultVO.setPART(addStringToQuoto(repairResultVO.getPART()));
			}
			
			// 2-1. 정비현황 목록 (EPMS_REPAIR_RESULT)
			List<HashMap<String, Object>> result_list = repairResultService.selectRepairResultList2(repairResultVO);
			
			// 2-2. 정비실적에  등록된 정비원 조회 (EPMS_REPAIR_RESULT)
			List<HashMap<String, Object>> work_log_list = new ArrayList<HashMap<String, Object>>();
			if (result_list != null){
				//정비실적현황 목록의 정비ID가져오기
				List<String> work_id = new ArrayList<String>();
				for (int i=0; i < result_list.size() ; i++ ){
					if(result_list.get(i).get("REPAIR_RESULT") != null){
						work_id.add(String.valueOf(result_list.get(i).get("REPAIR_RESULT")));
					}
					
				}
				repairResultVO.setWorkIdArr(work_id);
				
				// 정비실적에 등록된 정비원 조회
				repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)

				work_log_list = repairResultService.selectWorkMemberList(repairResultVO);
			}
			
			if(result_list.isEmpty() == false) {
				
				List<HashMap<String, Object>> tmp_list = new ArrayList<HashMap<String, Object>>();
				String REPAIR_RESULT = "";	//정비ID (EPMS_REPAIR_RESULT)
				String WORK_ID = "";		//작업ID (EPMS_WORK_LOG)
				
				for(HashMap<String, Object> result : result_list){
					
					tmp_list = new ArrayList<HashMap<String, Object>>();
					REPAIR_RESULT = String.valueOf(result.get("REPAIR_RESULT"));
					
					for(HashMap<String, Object> item : work_log_list){

						WORK_ID = String.valueOf(item.get("WORK_ID"));
						
						if(REPAIR_RESULT.equals(WORK_ID)){
							tmp_list.add(item);
						}
					}
					result.put("WORK_LOG", tmp_list);
				}
			}
			resultMap.put("RESULT_LIST", result_list);
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			
		} catch (Exception e) {
			logger.error("MobileRepairResultController.repairResultList > " + e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * 모바일 - 정비실적 상세조회 (요청현황 상세)
	 * 
	 * @author 김영환
	 * 
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/repairResultDtl.do")
	@ResponseBody
	public Map<String, Object> repairResultDtl(RepairRequestVO repairRequestVO) throws Exception{
		
		Map<String, Object> resultMap = new HashMap<>();
		
		// VO 초기화
		RepairResultVO repairResultVO = new RepairResultVO();
		repairResultVO.setCompanyId(getSsCompanyId());
//		repairResultVO.setSsLocation(getSsLocation());
		
		try {
			
			repairRequestVO.setCompanyId(getSsCompanyId());
//			repairRequestVO.setSsLocation(getSsLocation());
			
			// 1. 요청 상세
			HashMap<String, Object> request_detail = repairRequestService.selectRepairRequestDtl(repairRequestVO);
			repairResultVO.setREPAIR_REQUEST(String.valueOf(request_detail.get("REPAIR_REQUEST")));
			
			if(request_detail == null) {
				
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
				resultMap.put("REQUEST_DTL", request_detail);
				
			} else {
				
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
				resultMap.put("REQUEST_DTL", request_detail);
				
				// 2. 고장사진
				if(Integer.parseInt(request_detail.get("FILE_CNT2").toString()) > 0){
					AttachVO attachVO = new AttachVO();
					attachVO.setATTACH_GRP_NO(String.valueOf(request_detail.get("ATTACH_GRP_NO2")));
					List<HashMap<String, Object>> request_file_list = attachService.selectFileList(attachVO);
					resultMap.put("REQUEST_FILE_LIST", request_file_list);
				}
			
				/*************************** 실적조회 ****************************/
				
				// 3. 실적 상세
				HashMap<String, Object> result_detail = repairResultService.selectRepairResultDtl2(repairResultVO);
				
				// 실적이 있을 경우 수행
				if(result_detail != null){
					
					resultMap.put("RESULT_DTL", result_detail);
					repairResultVO.setREPAIR_RESULT(String.valueOf(result_detail.get("REPAIR_RESULT")));
					
					// 4. 정비사진
					if(Integer.parseInt(result_detail.get("FILE_CNT").toString()) > 0){
						AttachVO attachVO = new AttachVO();
						attachVO.setATTACH_GRP_NO(String.valueOf(result_detail.get("ATTACH_GRP_NO")));
						List<HashMap<String, Object>> result_file_list = attachService.selectFileList(attachVO);
						resultMap.put("RESULT_FILE_LIST", result_file_list);
					}
					
					// 5. 정비실적에 등록된 자재 목록
					List<HashMap<String, Object>> used_material_list = repairResultService.selectRepairUsedMaterialList(repairResultVO);
					if(used_material_list != null) {
						resultMap.put("USED_MATERIAL_LIST", used_material_list);
					}
					
					// 6-1. 정비실적에 등록된 정비원 조회
					repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)
					repairResultVO.setWORK_ID(String.valueOf(result_detail.get("REPAIR_RESULT")));
					List<HashMap<String, Object>> work_log_list = repairResultService.selectWorkMemberList(repairResultVO);
					
					// 6-2. 정비실적에 등록된 정비원 상세내역 조회
					if(work_log_list.isEmpty() == false) {
						
						String WORK_LOG[] =  new String[work_log_list.size()];
						for (int i=0; i < work_log_list.size() ; i++ ){
							WORK_LOG[i] = String.valueOf(work_log_list.get(i).get("WORK_LOG"));
						}
						repairResultVO.setWorkLogArr(WORK_LOG);
						List<HashMap<String, Object>> work_log_item_list = repairResultService.selectWorkLogItemList2(repairResultVO);
					
						List<HashMap<String, Object>> tmp_list = new ArrayList<HashMap<String, Object>>();
						String WORK_LOG1 = "";		//작업ID (EPMS_WORK_LOG)
						String WORK_LOG2 = "";		//작업ID (EPMS_WORK_LOG_ITEM)
						
						for(HashMap<String, Object> result : work_log_list){
							
							tmp_list = new ArrayList<HashMap<String, Object>>();
							WORK_LOG1 = String.valueOf(result.get("WORK_LOG"));
							
							for(HashMap<String, Object> item : work_log_item_list){

								WORK_LOG2 = String.valueOf(item.get("WORK_LOG"));
								
								if(WORK_LOG1.equals(WORK_LOG2)){
									tmp_list.add(item);
								}
							}
							result.put("WORK_LOG_ITEM", tmp_list);
						}
					}
					
					resultMap.put("WORK_LOG", work_log_list);
					
					
					// 7. 파트 구분일 경우
					if(CodeConstants.REMARKS_SEPARATE.equals(String.valueOf(request_detail.get("REMARKS")))){
						// 파트 정보
						repairResultVO.setCompanyId(getSsCompanyId());
						repairResultVO.setPART(String.valueOf(request_detail.get("PART")));
						List<HashMap<String, Object>> part_list = repairResultService.selectPartOptList2(repairResultVO);
						if(part_list != null) {
							resultMap.put("PART_LIST", part_list);
						}
					}
				}
			}
			
		} catch (Exception e) {
			logger.error("MobileRepairResultController.repairResultDtl > " + e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * 모바일 - 정비실적 실적등록폼
	 * 
	 * @author 김영환
	 * 
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/repairResultForm.do")
	@ResponseBody
	public Map<String, Object> repairResultForm(RepairRequestVO repairRequestVO) throws Exception{
		
		Map<String, Object> resultMap = new HashMap<>();
		
		// VO 초기화
		RepairResultVO repairResultVO = new RepairResultVO();
		repairResultVO.setCompanyId(getSsCompanyId());
//		repairResultVO.setSsLocation(getSsLocation());
		
		try {
			
			repairRequestVO.setCompanyId(getSsCompanyId());
//			repairRequestVO.setSsLocation(getSsLocation());
			
			// 1. 요청 상세
			HashMap<String, Object> request_detail = repairRequestService.selectRepairRequestDtl(repairRequestVO);
			repairResultVO.setREPAIR_REQUEST(String.valueOf(request_detail.get("REPAIR_REQUEST")));
			
			if(request_detail == null) {
				
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
				resultMap.put("REQUEST_DTL", request_detail);
				
			} else {
				
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
				resultMap.put("REQUEST_DTL", request_detail);
				
				// 2. 고장사진
				if(Integer.parseInt(request_detail.get("FILE_CNT2").toString()) > 0){
					AttachVO attachVO = new AttachVO();
					attachVO.setATTACH_GRP_NO(String.valueOf(request_detail.get("ATTACH_GRP_NO2")));
					List<HashMap<String, Object>> request_file_list = attachService.selectFileList(attachVO);
					resultMap.put("REQUEST_FILE_LIST", request_file_list);
				}
			
				/*************************** 실적조회 ****************************/
				
				// 3. 실적 상세
				repairResultVO.setPAGE("repairResultForm");
				HashMap<String, Object> result_detail = repairResultService.selectRepairResultDtl2(repairResultVO);
				
				// 실적이 있을 경우 수행
				if(result_detail != null){
					
					resultMap.put("RESULT_DTL", result_detail);
					repairResultVO.setREPAIR_RESULT(String.valueOf(result_detail.get("REPAIR_RESULT")));
					
					// 4. 정비사진
					if(Integer.parseInt(result_detail.get("FILE_CNT").toString()) > 0){
						AttachVO attachVO = new AttachVO();
						attachVO.setATTACH_GRP_NO(String.valueOf(result_detail.get("ATTACH_GRP_NO")));
						List<HashMap<String, Object>> result_file_list = attachService.selectFileList(attachVO);
						resultMap.put("RESULT_FILE_LIST", result_file_list);
					}
					
					// 5. 정비실적에 등록된 자재 목록
					List<HashMap<String, Object>> used_material_list = repairResultService.selectRepairUsedMaterialList(repairResultVO);
					if(used_material_list.isEmpty() == false) {
						resultMap.put("USED_MATERIAL_LIST", used_material_list);
					}
					
					// 6-1. 정비실적에 등록된 정비원 조회
					repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)
					repairResultVO.setWORK_ID(String.valueOf(result_detail.get("REPAIR_RESULT")));
					List<HashMap<String, Object>> work_log_list = repairResultService.selectWorkMemberList(repairResultVO);
					
					// 6-2. 정비실적에 등록된 정비원 상세내역 조회
					if(work_log_list.isEmpty() == false) {
						
						String WORK_LOG[] =  new String[work_log_list.size()];
						for (int i=0; i < work_log_list.size() ; i++ ){
							WORK_LOG[i] = String.valueOf(work_log_list.get(i).get("WORK_LOG"));
						}
						repairResultVO.setWorkLogArr(WORK_LOG);
						List<HashMap<String, Object>> work_log_item_list = repairResultService.selectWorkLogItemList2(repairResultVO);

						List<HashMap<String, Object>> tmp_list = new ArrayList<HashMap<String, Object>>();
						String WORK_LOG1 = "";		//작업ID (EPMS_WORK_LOG)
						String WORK_LOG2 = "";		//작업ID (EPMS_WORK_LOG_ITEM)
						
						for(HashMap<String, Object> result : work_log_list){
							
							tmp_list = new ArrayList<HashMap<String, Object>>();
							WORK_LOG1 = String.valueOf(result.get("WORK_LOG"));
							
							for(HashMap<String, Object> item : work_log_item_list){

								WORK_LOG2 = String.valueOf(item.get("WORK_LOG"));
								
								if(WORK_LOG1.equals(WORK_LOG2)){
									tmp_list.add(item);
								}
							}
							result.put("WORK_LOG_ITEM", tmp_list);
						}
					}
					
					resultMap.put("WORK_LOG", work_log_list);
					
					
					// 정비실적에 등록된 정비원 조회
//					repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)
//					repairResultVO.setWORK_ID(String.valueOf(result_detail.get("REPAIR_RESULT")));
//					List<HashMap<String, Object>> work_member_list = repairResultService.selectWorkMemberList(repairResultVO);
//					if(work_member_list.isEmpty() == false) {
//						resultMap.put("WORK_MEMBER_LIST", work_member_list);
//					}
					
					// 7-1. 공통코드 - 정비유형목록 조회
					repairResultVO.setREQUEST_TYPE(String.valueOf(request_detail.get("REQUEST_TYPE")));
					List<HashMap<String, Object>> repair_type_list = repairResultService.selectRepairTypeList(repairResultVO);
					if(repair_type_list.isEmpty() == false) {
						resultMap.put("REPAIR_TYPE_LIST", repair_type_list);
					}
					
					// 7-2. 공통코드 - 고장유형목록 조회
					List<HashMap<String, Object>> trouble_type1_list = repairResultService.selectTroubleType1List();
					if(trouble_type1_list.isEmpty() == false) {
						resultMap.put("TROUBLE_TYPE1_LIST", trouble_type1_list);
					}
					
					// 7-3. 공통코드 - 조치목록 조회
					List<HashMap<String, Object>> trouble_type2_list = repairResultService.selectTroubleType2List();
					if(trouble_type2_list.isEmpty() == false) {
						resultMap.put("TROUBLE_TYPE2_LIST", trouble_type2_list);
					}
					
					// 20190328 던킨 요청으로 사용 안함
					// 7-4-1. 공통코드 - 공통코드([SAP-코딩그룹]목록 조회)
//					List<HashMap<String, Object>> catalog_D_grp_list = repairResultService.selectCatalogDGrpList(repairResultVO);
//					List<HashMap<String, Object>> catalog_D_list = repairResultService.selectCatalogDList(repairResultVO);
//					if(catalog_D_grp_list.isEmpty() == false) {
//						
//						List<HashMap<String, Object>> tmp_list = new ArrayList<HashMap<String, Object>>();
//						String COMCD_GRP1 = "";		//그룹리스트 COMCD_GRP
//						String COMCD = "";			//그룹리스트 COMCD
//						String COMCD_GRP2 = "";		//아이템리스트 COMCD_GRP
//						String UPPER_COMCD = "";	//아이템리스트 UPPER_COMCD
//						
//						for(HashMap<String, Object> grp : catalog_D_grp_list){
//							
//							tmp_list = new ArrayList<HashMap<String, Object>>();
//							COMCD_GRP1 = String.valueOf(grp.get("COMCD_GRP"));
//							COMCD = String.valueOf(grp.get("COMCD"));
//							
//							for(HashMap<String, Object> item : catalog_D_list){
//
//								COMCD_GRP2 = String.valueOf(item.get("COMCD_GRP"));
//								UPPER_COMCD = String.valueOf(item.get("UPPER_COMCD"));
//								
//								if(COMCD_GRP1.equals(COMCD_GRP2) && COMCD.equals(UPPER_COMCD)){
//									tmp_list.add(item);
//								}
//							}
//							grp.put("ITEM_LIST", tmp_list);
//						}
//						
//						resultMap.put("CATALOG_D_GRP_LIST", catalog_D_grp_list);
//					}
					
					// 7-4-2. 공통코드 - 공통코드([SAP-대상그룹]목록 조회)
//					List<HashMap<String, Object>> catalog_B_grp_list = repairResultService.selectCatalogBGrpList(repairResultVO);
//					List<HashMap<String, Object>> catalog_B_list = repairResultService.selectCatalogBList(repairResultVO);
//					if(catalog_B_grp_list.isEmpty() == false) {
//						
//						List<HashMap<String, Object>> tmp_list = new ArrayList<HashMap<String, Object>>();
//						String COMCD_GRP1 = "";		//그룹리스트 COMCD_GRP
//						String COMCD = "";			//그룹리스트 COMCD
//						String COMCD_GRP2 = "";		//아이템리스트 COMCD_GRP
//						String UPPER_COMCD = "";	//아이템리스트 UPPER_COMCD
//						
//						for(HashMap<String, Object> grp : catalog_B_grp_list){
//							
//							tmp_list = new ArrayList<HashMap<String, Object>>();
//							COMCD_GRP1 = String.valueOf(grp.get("COMCD_GRP"));
//							COMCD = String.valueOf(grp.get("COMCD"));
//							
//							for(HashMap<String, Object> item : catalog_B_list){
//
//								COMCD_GRP2 = String.valueOf(item.get("COMCD_GRP"));
//								UPPER_COMCD = String.valueOf(item.get("UPPER_COMCD"));
//								
//								if(COMCD_GRP1.equals(COMCD_GRP2) && COMCD.equals(UPPER_COMCD)){
//									tmp_list.add(item);
//								}
//							}
//							grp.put("ITEM_LIST", tmp_list);
//						}
//						
//						resultMap.put("CATALOG_B_GRP_LIST", catalog_B_grp_list);
//					}

					// 7-4-3. 공통코드 - 공통코드([SAP-손상그룹]목록 조회)
//					List<HashMap<String, Object>> catalog_C_grp_list = repairResultService.selectCatalogCGrpList(repairResultVO);
//					List<HashMap<String, Object>> catalog_C_list = repairResultService.selectCatalogCList(repairResultVO);
//					if(catalog_B_grp_list.isEmpty() == false) {
//						
//						List<HashMap<String, Object>> tmp_list = new ArrayList<HashMap<String, Object>>();
//						String COMCD_GRP1 = "";		//그룹리스트 COMCD_GRP
//						String COMCD = "";			//그룹리스트 COMCD
//						String COMCD_GRP2 = "";		//아이템리스트 COMCD_GRP
//						String UPPER_COMCD = "";	//아이템리스트 UPPER_COMCD
//						
//						for(HashMap<String, Object> grp : catalog_C_grp_list){
//							
//							tmp_list = new ArrayList<HashMap<String, Object>>();
//							COMCD_GRP1 = String.valueOf(grp.get("COMCD_GRP"));
//							COMCD = String.valueOf(grp.get("COMCD"));
//							
//							for(HashMap<String, Object> item : catalog_C_list){
//
//								COMCD_GRP2 = String.valueOf(item.get("COMCD_GRP"));
//								UPPER_COMCD = String.valueOf(item.get("UPPER_COMCD"));
//								
//								if(COMCD_GRP1.equals(COMCD_GRP2) && COMCD.equals(UPPER_COMCD)){
//									tmp_list.add(item);
//								}
//							}
//							grp.put("ITEM_LIST", tmp_list);
//						}
//						
//						resultMap.put("CATALOG_C_GRP_LIST", catalog_C_grp_list);
//					}
					
					// 7-4-4. 공통코드 - 공통코드([SAP-원인그룹]목록 조회)
//					List<HashMap<String, Object>> catalog_5_grp_list = repairResultService.selectCatalog5GrpList(repairResultVO);
//					List<HashMap<String, Object>> catalog_5_list = repairResultService.selectCatalog5List(repairResultVO);
//					if(catalog_B_grp_list.isEmpty() == false) {
//						
//						List<HashMap<String, Object>> tmp_list = new ArrayList<HashMap<String, Object>>();
//						String COMCD_GRP1 = "";		//그룹리스트 COMCD_GRP
//						String COMCD = "";			//그룹리스트 COMCD
//						String COMCD_GRP2 = "";		//아이템리스트 COMCD_GRP
//						String UPPER_COMCD = "";	//아이템리스트 UPPER_COMCD
//						
//						for(HashMap<String, Object> grp : catalog_5_grp_list){
//							
//							tmp_list = new ArrayList<HashMap<String, Object>>();
//							COMCD_GRP1 = String.valueOf(grp.get("COMCD_GRP"));
//							COMCD = String.valueOf(grp.get("COMCD"));
//							
//							for(HashMap<String, Object> item : catalog_5_list){
//
//								COMCD_GRP2 = String.valueOf(item.get("COMCD_GRP"));
//								UPPER_COMCD = String.valueOf(item.get("UPPER_COMCD"));
//								
//								if(COMCD_GRP1.equals(COMCD_GRP2) && COMCD.equals(UPPER_COMCD)){
//									tmp_list.add(item);
//								}
//							}
//							grp.put("ITEM_LIST", tmp_list);
//						}
//						
//						resultMap.put("CATALOG_5_GRP_LIST", catalog_5_grp_list);
//					}
					
					// 8. 사용자재 등록용 설비 BOM 목록
					repairResultVO.setEQUIPMENT(String.valueOf(request_detail.get("EQUIPMENT")));
					repairResultVO.setLOCATION(String.valueOf(request_detail.get("LOCATION")));		//공장ID
					repairResultVO.setCompanyId(getSsCompanyId());									//회사ID
					
					List<HashMap<String, Object>> equipment_bom_list = repairResultService.selectEquipmentBomList(repairResultVO);
					if(equipment_bom_list.isEmpty() == false) {
						resultMap.put("EQUIPMENT_BOM_LIST", equipment_bom_list);
					}
					
					// 9. 정비원 등록용 정비원 목록
					repairResultVO.setREMARKS(String.valueOf(request_detail.get("REMARKS")));		//01:파트구분, 02:파트통합
					repairResultVO.setPART(String.valueOf(request_detail.get("PART")));				//파트
					repairResultVO.setSUB_ROOT(String.valueOf(request_detail.get("LOCATION")));		//공장코드
					
					repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); // 작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)
					repairResultVO.setWORK_ID(String.valueOf(result_detail.get("REPAIR_RESULT")));
					
					List<HashMap<String, Object>> repair_member_list = repairResultService.selectRepairMemberList(repairResultVO);
					if(repair_member_list.isEmpty() == false) {
						resultMap.put("REPAIR_MEMBER_LIST", repair_member_list);
					}
				}
			}
			
		} catch (Exception e) {
			logger.error("MobileRepairRequestController.repairRequestDtl > " + e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * 모바일 - 타파트 요청
	 * 
	 * @author 김영환
	 * 
	 * @since 2018.03.06
	 * @param RepairRequestVO repairRequestVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertRepairRequest.do")
	@ResponseBody
	public Map<String, Object> repairRequestInsert(RepairRequestVO repairRequestVO) throws Exception{
		
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			
			repairRequestVO.setCompanyId(getSsCompanyId());
			repairRequestVO.setUserId(getSsUserId());
			repairRequestVO.setDIVISION(getSsDivision());
			
			// 정비요청
			HashMap<String, Object> repairRequest = repairRequestService.insertRepairRequest(repairRequestVO);
			
			if(repairRequest != null){
				
				// Push 정보
				HashMap<String, Object> pushParam = repairRequest;
				String pushTitle = "";
				// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REQUEST_CANCEL-요청취소
				pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_APPR);
				// Push Title
				pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비를 요청하였습니다.(타파트 요청)";
				pushParam.put("PUSHTITLE", pushTitle);
				// 상세화면 IDX 세팅
				pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
				// 알림음 유형 세팅(01:일반, 02:긴급)
				pushParam.put("PUSHALARM", pushParam.get("URGENCY").toString());
				
				deviceService.sendPush(pushParam);
				
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
			else{
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.FAILED_TO_SAVE);
			}
			
			
			/* Device 푸시알람 추가 예정 */
			
		}catch (Exception e) {
			logger.error("RepairResultController.repairRequestAjax Error !");
			e.printStackTrace();
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * 모바일 - 정비실적 등록(임시저장/완료)
	 * @author 김영환
	 * @since 2018.03.15
	 * @param 
	 * @param model
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateRepairResult.do")
	@ResponseBody
	public HashMap<String, Object> repairResultUpdate(HttpServletRequest request, MultipartHttpServletRequest mRequest, RepairResultVO repairResultVO, Model model) {
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		try {
			repairResultVO.setCompanyId(getSsCompanyId());
//			repairResultVO.setSsLocation(getSsLocation());
			repairResultVO.setUserId(getSsUserId());		//id세팅
			//repairResultVO.setMEMBER_ID(getSsMemberId());	//사번세팅
			
			AttachVO attachVO = new AttachVO();
			attachVO.setDEL_SEQ(repairResultVO.getDEL_SEQ());
			attachVO.setMODULE(repairResultVO.getMODULE()); //112
			
			if("".equals(repairResultVO.getATTACH_GRP_NO()) || repairResultVO.getATTACH_GRP_NO() == null){
				if("Y".equals(repairResultVO.getAttachExistYn())){
					String attachGrpNo = attachService.selectAttachGrpNo();
					attachVO.setATTACH_GRP_NO(attachGrpNo);
					repairResultVO.setATTACH_GRP_NO(attachGrpNo);
					attachService.AttachEdit(request, mRequest, attachVO); 
				}
			}
			else{
				attachVO.setATTACH_GRP_NO(repairResultVO.getATTACH_GRP_NO());
				attachService.AttachEdit(request, mRequest, attachVO); 
			}
			
//			if(repairResultVO.getMemberArr().contains("[") && repairResultVO.getMemberArr().contains("]")){
//				repairResultVO.setMemberArr(repairResultVO.getMemberArr().replace("[", ""));
//				repairResultVO.setMemberArr(repairResultVO.getMemberArr().replace("]", ""));
//			}
			
			//HashMap<String, Object> repairResult = repairResultService.updateRepairResult(repairResultVO, attachVO);
			
			HashMap<String, Object> repairResult = null;
			
			repairResult = repairResultService.updateRepairResult(repairResultVO, attachVO);
			
			String updateYn = String.valueOf(repairResult.get("updateYn"));
			HashMap<String, Object> resultDtl = (HashMap<String, Object>) repairResult.get("resultDtl");
			
			if("SUCCESS".equals(updateYn)) {
				
				// 임시저장일 경우
				if(repairResultVO.getFlag().equals(CodeConstants.FLAG_SAVE)){

					JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
				}
				// 완료일 경우
				else if(repairResultVO.getFlag().equals(CodeConstants.FLAG_COMPLETE)){
					
					if("S".equals(String.valueOf(resultDtl.get("E_RESULT")))){
						// 정비완료일 경우
						if(CodeConstants.EPMS_REPAIR_STATUS_COMPLETE.equals(resultDtl.get("REPAIR_STATUS").toString())){
							// Push 정보
							HashMap<String, Object> pushParam = resultDtl;
							String pushTitle = "";
							
							// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
							pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REQUEST_COMPLETE);
							// Push Title
							pushTitle = pushParam.get("MANAGER_NAME").toString() + "님이 정비를 완료하였습니다.";
							pushParam.put("PUSHTITLE", pushTitle);
							// 상세화면 IDX 세팅
							pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
							// 알림음 유형 세팅(01:일반, 02:긴급)
							pushParam.put("PUSHALARM", "01");
							
							deviceService.sendPush(pushParam);
							
							pushParam = new HashMap<String, Object>();
							pushParam = resultDtl;
							
							// 1-2. 정비 담당자에게 담당자에게 푸시 전송
							// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
							pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REPAIR_COMPLETE);
							// Push Title
							pushTitle = pushParam.get("MANAGER_NAME").toString() + "님이 정비를 완료하였습니다.";
							pushParam.put("PUSHTITLE", pushTitle);
							// 상세화면 IDX 세팅
							pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
							// 알림음 유형 세팅(01:일반, 02:긴급)
							pushParam.put("PUSHALARM", "01");
							
							deviceService.sendPush(pushParam);
						}
						JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
					}
					else{

						resultMap.put("RES_CD", "9999");
						resultMap.put("RES_MSG", String.valueOf(repairResult.get("E_MESSAGE")));
					}
				}
			}
			// 등록가능한 정비실적 체크
			else if("NO_REPAIR".equals(updateYn)){
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.NO_REPAIR);
			}
			// 개인실적 완료여부 체크 (참여정비원 모두 완료상태)
			else if("NO_COMPLETE".equals(updateYn)){
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.NO_COMPLETE);
			}
			// 정비시간 체크 (정비종료시간 >= 개인작업종료시간 중 가장 늦은 시간)
			else if("NO_REPAIR_TIME".equals(updateYn)){
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.NO_REPAIR_TIME);
			}
			else{
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.FAILED_TO_SAVE);
			}
			
		}catch (Exception e) {
			logger.error("MobileRepairResultController.updateRepairResult Error !");
			e.printStackTrace();
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * 모바일 - 개인별 작업 등록(임시저장/작업완료)
	 * @author 서정민
	 * @since 2019.03.08
	 * @param 
	 * @param model
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateWorkLog.do")
	@ResponseBody
	public HashMap<String, Object> updateWorkLog(RepairResultVO repairResultVO, Model model) {
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		try {
			
			repairResultVO.setCompanyId(getSsCompanyId());
			repairResultVO.setUserId(getSsUserId());
			repairResultVO.setREG_ID(getSsUserId());
			
//			if(repairResultVO.getWorkLogItemArr().contains("[") && repairResultVO.getWorkLogItemArr().contains("]")){
//				repairResultVO.setMemberArr(repairResultVO.getWorkLogItemArr().replace("[", ""));
//				repairResultVO.setMemberArr(repairResultVO.getWorkLogItemArr().replace("]", ""));
//			}
			
			HashMap<String, Object> workLogItem = repairResultService.updateWorkLogItem(repairResultVO);
			
			String updateYn = String.valueOf(workLogItem.get("updateYn"));
			
			if("SUCCESS".equals(updateYn)){
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
			else if("PARAM_NULL".equals(updateYn)){
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.PARAM_NULL);
			}
			else if("NO_WORK_LOG".equals(updateYn)){
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.NO_WORK_LOG);
			}
			else if("NO_WORK_LOG_TIME".equals(updateYn)){
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.NO_WORK_LOG_TIME);
			}
			else{
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
			}
			
		}catch (Exception e) {
			logger.error("MobileRepairResultController.updateWorkLog Error !");
			e.printStackTrace();
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * 모바일 - 개인별 작업 취소
	 * @author 서정민
	 * @since 2019.03.08
	 * @param 
	 * @param model
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/cancelWorkLog.do")
	@ResponseBody
	public HashMap<String, Object> cancelWorkLog(RepairResultVO repairResultVO) {
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		try {
			//작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)
			repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR);
			repairResultVO.setWORK_STATUS("N");
			repairResultService.updateWorkLogStatus(repairResultVO);
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			
		}catch (Exception e) {
			logger.error("MobileRepairResultController.cancelWorkLog Error !");
			e.printStackTrace();
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * 모바일 - 정비원 추가/삭제
	 * @author 서정민
	 * @since 2019.03.07
	 * @param RepairResultVO repairResultVO
	 * @param PreventiveResultVO preventiveResultVO
	 * @param model
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/workMemberList.do")
	@ResponseBody
	public Map<String, Object> workMemberList(RepairResultVO repairResultVO, PreventiveResultVO preventiveResultVO, Model model) throws Exception{
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		try {
			
			if("REPAIR".equals(repairResultVO.getFlag())){
				
				// 정비실적에 등록된 정비원 조회
				HashMap<String, Object> repairResultDtl = repairResultService.selectRepairResultDtl(repairResultVO);
				
				repairResultVO.setCompanyId(getSsCompanyId());
				repairResultVO.setREMARKS(String.valueOf(repairResultDtl.get("REMARKS")));		//01:파트구분, 02:파트통합
				repairResultVO.setPART(String.valueOf(repairResultDtl.get("PART")));			//파트
				repairResultVO.setSUB_ROOT(String.valueOf(repairResultDtl.get("LOCATION")));	//공장코드
				repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); // 작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)
				repairResultVO.setWORK_ID(repairResultVO.getREPAIR_RESULT());
				
				List<HashMap<String, Object>> repair_member_list = repairResultService.selectRepairMemberList(repairResultVO);
				resultMap.put("WORK_LOG", repair_member_list);

				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
			else if("PREVENTIVE".equals(repairResultVO.getFlag())){
				
				//예방보전실적에 등록된 정비원 조회
				HashMap<String, Object> preventiveResultDtl = preventiveResultService.selectPreventiveResultDtl(preventiveResultVO);
				
				repairResultVO.setCompanyId(getSsCompanyId());
				repairResultVO.setREMARKS(String.valueOf(preventiveResultDtl.get("REMARKS")));		//01:파트구분, 02:파트통합
				repairResultVO.setPART(String.valueOf(preventiveResultDtl.get("PART")));			//파트
				repairResultVO.setSUB_ROOT(String.valueOf(preventiveResultDtl.get("LOCATION")));	//공장코드
				repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_PREVENTIVE); // 작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)
				repairResultVO.setWORK_ID(preventiveResultVO.getPREVENTIVE_RESULT());
				
				List<HashMap<String, Object>> repair_member_list = repairResultService.selectRepairMemberList(repairResultVO);
				resultMap.put("WORK_LOG", repair_member_list);

				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
			else{
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.PARAM_NULL);
			}
			
			
		}catch (Exception e) {
			logger.error("MobileRepairResultController.updateWorkLog Error !");
			e.printStackTrace();
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * 모바일 - 정비원 추가/삭제
	 * @author 서정민
	 * @since 2019.03.07
	 * @param RepairResultVO repairResultVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateWorkMember.do")
	@ResponseBody
	public HashMap<String, Object> updateWorkMember(RepairResultVO repairResultVO) {
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		try {
			repairResultVO.setUserId(getSsUserId());		//세션_아이디
			HashMap<String, Object> repairResult = repairResultService.updateWorkMember(repairResultVO);
			
			String updateYn = String.valueOf(repairResult.get("updateYn"));
			
			if("SUCCESS".equals(updateYn)){
				
				// 1. 정비실적에 등록된 정비원 조회
				repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)
				repairResultVO.setWORK_ID(repairResultVO.getREPAIR_RESULT());
				List<HashMap<String, Object>> work_log_list = repairResultService.selectWorkMemberList(repairResultVO);
				
				// 2. 정비실적에 등록된 정비원 상세내역 조회
				String WORK_LOG[] =  new String[work_log_list.size()];
				for (int i=0; i < work_log_list.size() ; i++ ){
					WORK_LOG[i] = String.valueOf(work_log_list.get(i).get("WORK_LOG"));
				}
				repairResultVO.setWorkLogArr(WORK_LOG);
				repairResultVO.setUSER_ID("");
				repairResultVO.setPAGE("repairResultForm");
				List<HashMap<String, Object>> work_log_item_list = repairResultService.selectWorkLogItemList2(repairResultVO);
				
				if(work_log_list.isEmpty() == false) {
					
					List<HashMap<String, Object>> tmp_list = new ArrayList<HashMap<String, Object>>();
					String WORK_LOG1 = "";		//작업ID (EPMS_WORK_LOG)
					String WORK_LOG2 = "";		//작업ID (EPMS_WORK_LOG_ITEM)
					
					for(HashMap<String, Object> result : work_log_list){
						
						tmp_list = new ArrayList<HashMap<String, Object>>();
						WORK_LOG1 = String.valueOf(result.get("WORK_LOG"));
						
						for(HashMap<String, Object> item : work_log_item_list){

							WORK_LOG2 = String.valueOf(item.get("WORK_LOG"));
							
							if(WORK_LOG1.equals(WORK_LOG2)){
								tmp_list.add(item);
							}
						}
						result.put("WORK_LOG_ITEM", tmp_list);
					}
				}
				
				resultMap.put("WORK_LOG", work_log_list);
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			}
			else if("NO_REPAIR".equals(updateYn)){
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.NO_REPAIR);
			}
			else if("NO_MANAGER".equals(updateYn)){
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.NO_MANAGER);
			}
			else{
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.FAILED_TO_SAVE);
			}
			
		}catch (Exception e) {
			logger.error("MobileRepairResultController.updateWorkLog Error !");
			e.printStackTrace();
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * 모바일 - Oracle IN절 입력 시 Quoto처리 
	 * 
	 * @author 김영환
	 * 
	 * @since 2018.03.06
	 * @param String 
	 * @return String
	 * @throws Exception
	 */
	public String addStringToQuoto(String originString){
		
		if("".equals(originString) || originString == null) {
			return "\'\'";
		}
		else if("ALL".equals(originString)) {
			return "ALL";
		} 
		else {
			String[] tempArray = originString.split(",");
			StringBuffer tempBuffer = new StringBuffer();
			
			if(tempArray.length == 1) {
				tempBuffer.append("(\'"+tempArray[0]+"\')");
			} else {
				for(int i=0; i < tempArray.length; i++){
					if(i == 0)							tempBuffer.append("(\'"+tempArray[i]+"\', ");	// 시작값
					else if( i == tempArray.length-1)	tempBuffer.append("\'"+tempArray[i]+"\')");		// 마지막값
					else 								tempBuffer.append("\'"+tempArray[i]+"\', ");	// 중간값
				}
			}
			
			return tempBuffer.toString();
		}
	}
}
