package aspn.hello.epms.repair.result.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.SessionUtil;
import aspn.hello.com.mapper.AttachMapper;
import aspn.hello.com.model.AttachVO;
import aspn.hello.epms.equipment.history.mapper.EquipmentHistoryMapper;
import aspn.hello.epms.equipment.history.model.EquipmentHistoryVO;
import aspn.hello.epms.material.master.mapper.MaterialMasterMapper;
import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.epms.repair.request.mapper.RepairRequestMapper;
import aspn.hello.epms.repair.request.model.RepairRequestVO;
import aspn.hello.epms.repair.result.mapper.RepairResultMapper;
import aspn.hello.epms.repair.result.model.RepairResultVO;
import aspn.hello.rfc.model.ZPM_CNF_CANCEL_EXPORT;
import aspn.hello.rfc.model.ZPM_CNF_CANCEL_IMPORT;
import aspn.hello.rfc.model.ZPM_CNF_CANCEL_T_CNF;
import aspn.hello.rfc.model.ZPM_ORDER_CNF_EXPORT;
import aspn.hello.rfc.model.ZPM_ORDER_CNF_IMPORT;
import aspn.hello.rfc.model.ZPM_ORDER_CNF_T_COMP;
import aspn.hello.rfc.model.ZPM_ORDER_CNF_T_LINES;
import aspn.hello.rfc.model.ZPM_ORDER_CNF_T_TASK;
import aspn.hello.rfc.model.ZPM_ORDER_CNF_T_WORK;
import aspn.hello.rfc.service.RfcService;
import net.sf.json.JSONArray;

/**
 * [고장관리-정비실적등록/조회] Business Implement Class
 * 
 * @author 김영환
 * @since 2018.03.06
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.06		김영환		최초 생성
 *   
 * </pre>
 */

@Service
public class RepairResultServiceImpl implements RepairResultService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RepairResultMapper repairResultMapper;
	
	@Autowired
	AttachMapper attachMapper;
	
	@Autowired
	EquipmentHistoryMapper equipmentHistoryMapper;
	
	@Autowired
	MaterialMasterMapper materialMasterMapper;
	
	@Autowired
	RepairRequestMapper repairRequestMapper;
	
	@Autowired
	RfcService rfcService;

	/**
	 * 정비실적현황 조회
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairResultList(RepairResultVO repairResultVO) {
		return repairResultMapper.selectRepairResultList(repairResultVO);
	}

	/**
	 * 정비실적 상세
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public HashMap<String, Object> selectRepairResultDtl(RepairResultVO repairResultVO) {
		return repairResultMapper.selectRepairResultDtl(repairResultVO);
	}

	/**
	 * 정비실적,고장원인분석에 등록된 정비원 목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectWorkMemberList(RepairResultVO repairResultVO) {
		return repairResultMapper.selectWorkMemberList(repairResultVO);
	}

	/**
	 * 설비 첨부이미지 목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectAttachList(RepairResultVO repairResultVO) {
		return repairResultMapper.selectAttachList(repairResultVO);
	}

	/**
	 * 고장유형 목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectTroubleList1() {
		return repairResultMapper.selectTroubleList1();
	}

	/**
	 * 조치 목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectTroubleList2() {
		return repairResultMapper.selectTroubleList2();
	}

	/**
	 * 정비실적등록 출퇴근 현황 리스트
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectWorkerList(RepairResultVO repairResultVO) {
		return repairResultMapper.selectWorkerList(repairResultVO);
	}

	/**
	 * 파트 목록 (본인 소속파트 제외)
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectPartOptList(RepairResultVO repairResultVO) {
		return repairResultMapper.selectPartOptList(repairResultVO);
	}
	
	/**
	 * 정비실적등록/조회 - 정비원 실적 아이템 저장
	 *
	 * @author 김영환
	 * @since 2018.12.20
	 * @param RepairResultVO repairResultVO1 
	 */
	@Override
	public void insertWorkLogItem(RepairResultVO repairResultVO1) {
		repairResultMapper.insertWorkLogItem(repairResultVO1);
	}	
	
	/**
	 * 정비원 정비 실적 ITEM 조회
	 * 
	 * @author 김영환
	 * @since 2018.12.20
	 * @param RepairResultVO repairResultVO1
	 */
	@Override
	public List<HashMap<String, Object>> selectWorkLogItemList(RepairResultVO repairResultVO){
		return repairResultMapper.selectWorkLogItemList(repairResultVO);
	}
	
	/**
	 * 정비원 개별 - 실적등록:임시저장, 작업완료 
	 * 
	 * @author 김영환
	 * @since 2018.12.20
	 * @param RepairResultVO repairResultVO
	 * @return HashMap<String, Object>
	 * @throws Exception 
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	public HashMap<String, Object> updateWorkLogItem(RepairResultVO repairResultVO) throws Exception {
				
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		//개인작업 완료여부 체크
		repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR);	// 작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)
		repairResultVO.setWORK_STATUS("N");
		int	cnt = repairResultMapper.selectWorkLogCheck(repairResultVO); 
		
		if(cnt==1){
			//정비원 개인작업 등록
			if(!("".equals(repairResultVO.getWorkLogItemArr()) || repairResultVO.getWorkLogItemArr() == null)){
				
				// JSON -> String
				List<HashMap<String, Object>> workLogArr = JSONArray.fromObject(convertStandardJSONString(repairResultVO.getWorkLogItemArr()));
				
				RepairResultVO repairResultVO1 = new RepairResultVO();
				
				int workLogValidationCnt = 0;
				
				for(Map<String, Object> item : workLogArr){
					
					if(String.valueOf(item.get("DATE_START")).compareTo(String.valueOf(item.get("DATE_END"))) > 0){
						workLogValidationCnt++;
					}
					else{
						repairResultVO1 = new RepairResultVO();
						
						repairResultVO1.setWORK_LOG(String.valueOf(item.get("WORK_LOG")));
						
						if(item.containsKey("WORK_LOG_ITEM") == true){
							repairResultVO1.setWORK_LOG_ITEM(item.get("WORK_LOG_ITEM").toString());	// 작업이력상세 ID
						}
						if(item.containsKey("DESCRIPTION") == true){
							repairResultVO1.setITEM_DESCRIPTION(item.get("DESCRIPTION").toString());// 정비원 정비 내용
						}
						repairResultVO1.setUSER_ID(String.valueOf(item.get("USER_ID")));			// 정비원ID
						repairResultVO1.setDATE_START(String.valueOf(item.get("DATE_START")));		// 정비원 정비 시작일시
						repairResultVO1.setDATE_END(String.valueOf(item.get("DATE_END")));			// 정비원 정비 종료일시
						repairResultVO1.setDEL_YN(String.valueOf(item.get("DEL_YN")));				// 삭제 여부
						repairResultVO1.setUserId(repairResultVO.getUserId());
						
						// 개별실적 등록 여부 확인
						int cnt2 = repairResultMapper.selectWorkLogItemCheck(repairResultVO1);
						
						// 등록되지 않은 개별 실적인 경우
						if ( cnt2 == 0 && "N".equals(String.valueOf(item.get("DEL_YN"))) ) {
							// 정비원실적등록
							repairResultMapper.insertWorkLogItem(repairResultVO1);
						}
						// 기존에 등록된 개별 실적인 경우
						else if(cnt2 != 0) {
							repairResultMapper.updateWorkLogItem(repairResultVO1);
						}
					}
				}
				
				if(workLogValidationCnt > 0){
					resultMap.put("updateYn", "NO_WORK_LOG_TIME");
				}
				else{
					// 작업완료일 경우
					if("COMPLETE".equals(repairResultVO.getFlag())){
						repairResultVO1.setWORK_STATUS("Y");
						repairResultVO1.setWORK_ID(repairResultVO.getWORK_ID());
						repairResultVO1.setWORK_TYPE(repairResultVO.getWORK_TYPE());
						repairResultMapper.updateWorkLogStatus(repairResultVO1);
					}
					resultMap.put("updateYn", "SUCCESS");
					resultMap.put("WORK_LOG", repairResultVO1.getWORK_LOG());
				}
			}
			else{
				resultMap.put("updateYn", "PARAM_NULL");
			}
		}
		else{
			resultMap.put("updateYn", "NO_WORK_LOG");
		}

		return resultMap;

	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// SAP
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * 정비실적 임시저장,정비완료
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param AttachVO attachVO
	 * @param RepairResultVO repairResultVO
	 * @return HashMap<String, Object>
	 * @throws Exception 
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	public HashMap<String, Object> updateRepairResult(RepairResultVO repairResultVO, AttachVO attachVO) throws Exception {
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
//		int qntyBefore = 0;	// 수량체크
//		int qntyAfter = 0;
		int	cnt = repairResultMapper.selectRepairResultCheck(repairResultVO); //정비실적 등록여부 체크
		String ssUserId = (String) SessionUtil.getAttribute("ssUserId");
		
		// 정비실적 상세
		HashMap<String, Object> repairResultDtl = repairResultMapper.selectRepairResultDtl(repairResultVO);
		
		if(cnt==1){
			
			resultMap.put("updateYn", "SUCCESS");
			
			RepairResultVO repairResultVO1 = new RepairResultVO();
			ZPM_ORDER_CNF_IMPORT zpm_order_cnf_import = new ZPM_ORDER_CNF_IMPORT();
			
			// 1. 기존에 등록된 사용자재 변경
			if(!("".equals(repairResultVO.getMaterialUsedArr()) || repairResultVO.getMaterialUsedArr() == null)){
				List<HashMap<String, Object>> materialUsedArr = JSONArray.fromObject(convertStandardJSONString(repairResultVO.getMaterialUsedArr()));
				
				//입출고 헤더 ID 담기
				repairResultVO1.setMATERIAL_INOUT(repairResultVO.getMATERIAL_INOUT());
				//출고 날짜 담기
				repairResultVO1.setDATE_INOUT(repairResultVO.getTROUBLE_TIME2());
				
				for(Map<String, Object> item : materialUsedArr){
					repairResultVO1.setUserId(repairResultVO.getUserId());
					repairResultVO1.setEQUIPMENT_BOM(String.valueOf(item.get("EQUIPMENT_BOM")));
					repairResultVO1.setMATERIAL(String.valueOf(item.get("MATERIAL")));

					// 삭제 여부 체크
					if("Y".equals(String.valueOf(item.get("DEL_YN")))){
						repairResultVO1.setQNTY("0");
					} else {
						repairResultVO1.setQNTY(String.valueOf(item.get("QNTY")));
					}
					
					// 수량여부 체크
					if("".equals(repairResultVO1.getQNTY()) || repairResultVO1.getQNTY() == null){
						repairResultVO1.setQNTY("0");
					}
					
					//반영 전 현재 안전재고/재고 수량 조회
					MaterialMasterVO materialMasterVO = new MaterialMasterVO();
					materialMasterVO.setMATERIAL(repairResultVO1.getMATERIAL());
					
					HashMap<String, Object> before_stockMap = materialMasterMapper.selectMaterialMasterStock(materialMasterVO);
					int before_stock = Integer.parseInt(String.valueOf(before_stockMap.get("STOCK")));
					int before_stock_optimal = Integer.parseInt(String.valueOf(before_stockMap.get("STOCK_OPTIMAL")));
					
					String STOCK_FLAG = "N";	// 기존 재고가 안전재고 미만일 경우 (STOCK_FLAG:'N')
					if(before_stock >= before_stock_optimal) {
						STOCK_FLAG = "Y"; 		// 기존 재고가 안전재고 이상일 경우 (STOCK_FLAG:'Y')
					}
					
					//기존 사용자재 출고수량  (기존 내역 출고 수량 조회)
//					qntyBefore = repairResultMapper.selectMaterialInoutQnty(repairResultVO1);
					//변경 사용자재 출고수량
//					qntyAfter = Integer.parseInt(repairResultVO1.getQNTY());
					
					//출고내역 수정(수량) MATERIAL_INOUT_ITEM UPDATE
					repairResultMapper.updateMaterialInoutList(repairResultVO1);
					
					// 던킨의 경우 : 재고 관리는 SAP에 저장된 자재 재고로 Update 한다. (2019.02.21~)
					
					//출고수량 차이 재고 반영
//					if(rtnVal > 0){
//						rtnVal = 0;
						
						//출고수량 차이 세팅
//						repairResultVO1.setQNTY(Integer.toString(qntyAfter-qntyBefore));
					
						//출고수량 차이 재고 반영
//						rtnVal = repairResultMapper.updateMaterialStockOut(repairResultVO1);
//					}
					
					// 기존 재고가 안전재고 이상이었는데, 반영 후 안전재고 미만이 될 경우 DATE_ALERT 등록
					HashMap<String, Object> after_stockMap = materialMasterMapper.selectMaterialMasterStock(materialMasterVO);
					int after_stock = Integer.parseInt(String.valueOf(after_stockMap.get("STOCK")));
					int after_stock_optimal = Integer.parseInt(String.valueOf(after_stockMap.get("STOCK_OPTIMAL")));
					
					//안전재고/재고가 다를경우 DATE_ALERT 필드 업데이트
					if(STOCK_FLAG == "Y" && after_stock_optimal > after_stock){
						materialMasterMapper.updateMaterialMasterDateAlert(materialMasterVO);
					}
					
					// 사용자재 삭제 시 입출고 아이템 목록에서도 삭제
					if("Y".equals(String.valueOf(item.get("DEL_YN")))){
						repairResultMapper.updateMaterialInoutDelYn(repairResultVO1);
					}
					
				}
			}
			
			// 2. 추가 자재 내역 등록
			if(!("".equals(repairResultVO.getMaterialArr()) || repairResultVO.getMaterialArr() == null)){
				
				List<HashMap<String, Object>> materialArr = JSONArray.fromObject(convertStandardJSONString(repairResultVO.getMaterialArr()));
				
				//기존에 등록된 자재 출고 내역이 없었다면, 입출고 헤더 등록부터
				if("".equals(repairResultVO.getMATERIAL_INOUT()) || repairResultVO.getMATERIAL_INOUT() == null){
					//입출고 SEQ 체크(SEQ_MATERIAL_INOUT 체크)
					int SEQ_MATERIAL_INOUT;
					SEQ_MATERIAL_INOUT = repairResultMapper.selectMaterialInoutSeq(repairResultVO);			
					repairResultVO.setMATERIAL_INOUT(Integer.toString(SEQ_MATERIAL_INOUT));
					
					//입출고 날짜 담기
					repairResultVO.setDATE_INOUT(repairResultVO.getTROUBLE_TIME2());
					
					//입출고유형 (02:출고)
					repairResultVO.setMATERIAL_INOUT_TYPE(CodeConstants.EPMS_MATERIAL_INOUT_TYPE_OUT);
					
					//입출고 등록(헤더) MATERIAL_INOUT INSERT
					repairResultMapper.insertMaterialInout(repairResultVO);
					
				}
				
				for(Map<String, Object> item : materialArr){
					//로그인 계정 정보 담기
					repairResultVO1.setUserId(repairResultVO.getUserId());
					//입출고 헤더 PK (MATERIAL_INOUT) 담기
					repairResultVO1.setMATERIAL_INOUT(repairResultVO.getMATERIAL_INOUT());
					//입출고 날짜 담기
					repairResultVO1.setDATE_INOUT(repairResultVO.getTROUBLE_TIME2());
					
					//실적유형, ID 등록
					repairResultVO1.setTARGET_TYPE("REPAIR");
					repairResultVO1.setTARGET_ID(repairResultVO.getREPAIR_RESULT());
					
					//BOM코드 담기
					repairResultVO1.setEQUIPMENT_BOM(String.valueOf(item.get("EQUIPMENT_BOM")));
					//자재코드 담기
					repairResultVO1.setMATERIAL(String.valueOf(item.get("MATERIAL")));
					//수량 담기
					repairResultVO1.setQNTY(String.valueOf(item.get("QNTY")));
					
					//반영 전 현재 안전재고/재고 수량 조회
					MaterialMasterVO materialMasterVO = new MaterialMasterVO();
					materialMasterVO.setMATERIAL(repairResultVO1.getMATERIAL());
					
//					HashMap<String, Object> stockMap_before = materialMasterMapper.selectMaterialMasterStock(materialMasterVO);
//					int stock_before = Integer.parseInt(String.valueOf(stockMap_before.get("STOCK")));
//					int stock_optimal_before = Integer.parseInt(String.valueOf(stockMap_before.get("STOCK_OPTIMAL")));
//					
//					String STOCK_FLAG = "N";	// 기존 재고가 안전재고 미만일 경우 (STOCK_FLAG:'N')
//					if(stock_before >= stock_optimal_before){
//						STOCK_FLAG = "Y";		// 기존 재고가 안전재고 이상일 경우 (STOCK_FLAG:'Y')
//					}
					
					//수량이 0보다 크다면 입출고 내역 등록 (20170126 수정 : 수량이 0도 등록)
					if(Integer.parseInt(repairResultVO1.getQNTY()) >= 0 || "".equals(repairResultVO1.getQNTY()) || repairResultVO1.getQNTY() == null){
						
						if("".equals(repairResultVO1.getQNTY()) || repairResultVO1.getQNTY() == null){
							repairResultVO1.setQNTY("0");
						}
						
						//출고 등록(아이템) MATERIAL_INOUT_ITEM INSERT
						repairResultMapper.insertMaterialInoutItem(repairResultVO1);
						
						// 던킨의 경우 : 재고 관리는 SAP에 저장된 자재 재고로 Update 한다. (2019.02.21~)
//							if(rtnVal > 0){
//								rtnVal = 0;
						
							//자재 재고수량 반영(출고) MATERIAL UPDATE
//								rtnVal = repairResultMapper.updateMaterialStockOut(repairResultVO1);
//							}
					}
					
					// 기존 재고가 안전재고 이상이었는데, 반영 후 안전재고 미만이 될 경우 DATE_ALERT 등록
//					HashMap<String, Object> stockMap_after = materialMasterMapper.selectMaterialMasterStock(materialMasterVO);
//					int stock_after = Integer.parseInt(String.valueOf(stockMap_after.get("STOCK")));
//					int stock_optimal_after = Integer.parseInt(String.valueOf(stockMap_after.get("STOCK_OPTIMAL")));
//					
//					if(STOCK_FLAG == "Y" && stock_optimal_after > stock_after){
//						materialMasterMapper.updateMaterialMasterDateAlert(materialMasterVO);
//					}
					
				}
					
			}
			
			// 3. 정비원 등록 - 정비원 관련 내용은 jsp에서 memberArr변수에 json형태로 담겨온다.
			if(!("".equals(repairResultVO.getMemberArr()) || repairResultVO.getMemberArr() == null)){
				
				// 작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)
				repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR);
				
				// 작업대상ID 세팅
				repairResultVO.setWORK_ID(repairResultVO.getREPAIR_RESULT());
				
				// JSON -> String
				List<HashMap<String, Object>> memberArr = JSONArray.fromObject(convertStandardJSONString(repairResultVO.getMemberArr()));
				
				for(Map<String, Object> item : memberArr){
					
					repairResultVO.setWORK_LOG(String.valueOf(item.get("WORK_LOG"))); 		// 작업이력ID
					repairResultVO.setUSER_ID(String.valueOf(item.get("USER_ID")));			// 정비원ID
					repairResultVO.setDEL_YN(String.valueOf(item.get("DEL_YN")));			// 삭제 여부
					
					// 정비원 등록여부 체크
					int	workLogCheck = repairResultMapper.selectWorkLogCheck(repairResultVO);
					
					// 등록되지 않은 정비원의 경우
					if ( workLogCheck == 0 && "N".equals(repairResultVO.getDEL_YN())) {
						
						// WorkLog테이블 Seq 조회
						String workLogSeq = repairResultMapper.selectWorkLogSeq(repairResultVO);
						repairResultVO.setWORK_LOG(workLogSeq);
						
						// '정비원' 등록
						repairResultMapper.insertWorkLog(repairResultVO);
						
						// 정비원이 추가된 경우 정비실적 기본 세팅
						SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						Date today = new Date();
						repairResultVO.setDATE_START(date.format(today));					// 정비원 정비 시작일시
						repairResultVO.setDATE_END(date.format(today));						// 정비원 정비 종료일시
						repairResultVO.setITEM_DESCRIPTION("");				                // 정비원 정비 내용
						
						// 정비원 '실적'등록
						repairResultMapper.insertWorkLogItem(repairResultVO);
					}
					// 기존에 등록된 정비원의 경우
					else {
						// 정비원 '상태' 등록
						repairResultMapper.updateWorkLog(repairResultVO);
					}
				}
			}
			
			// 4. 정비실적  - 임시저장 / 정비완료등록
			repairResultMapper.updateRepairResult(repairResultVO);
			
			
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//////////////////////////////////////////////// SAP RFC 통신 시작 ////////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			// ※ 정비완료의 경우, SAP RFC 통신
			if(repairResultVO.getFlag().equals(CodeConstants.FLAG_COMPLETE)){
				
				// 개인실적 완료여부 체크 
				repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); // 작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)
				HashMap<String, Object> completeCheck = repairResultMapper.workCompleteCheck(repairResultVO);
				
				// 전부 다 완료상태인 경우
				if(String.valueOf(completeCheck.get("COMPLETE_CNT")).equals(String.valueOf(completeCheck.get("TOTAL_CNT")))){

					// 정비시간 체크 (정비종료시간 >= 개인작업종료시간 중 가장 늦은 시간)
					HashMap<String, Object> repairTimeCheck = repairResultMapper.repairTimeCheck(repairResultVO);
					
					// 개인실적 완료여부 및 정비시간 체크 완료시(SAP 확정 수행)
					if("Y".equals(String.valueOf(repairTimeCheck.get("REPAIR_TIME_YN")))){
						// 1. IMPORT (헤더)
						// 1-1. 오더유형 셋팅
						if("110".equals(repairResultVO.getREPAIR_TYPE())) {			// 돌발정비
							zpm_order_cnf_import.setI_TYPECODE("DM02");
						}
						else if("120".equals(repairResultVO.getREPAIR_TYPE())) {	// 일반정비
							zpm_order_cnf_import.setI_TYPECODE("DM02");
						}
						else if("130".equals(repairResultVO.getREPAIR_TYPE())) {	// 개선정비
							zpm_order_cnf_import.setI_TYPECODE("DM02");
						}
						else if("150".equals(repairResultVO.getREPAIR_TYPE())) {	// 예방정비
							zpm_order_cnf_import.setI_TYPECODE("DM01");
						}
						
						// 1-2. 오더번호
						String AUFNR = String.valueOf(repairResultDtl.get("E_AUFNR"));
						
						if(!"".equals(AUFNR) && AUFNR != null && AUFNR != "null"){
							zpm_order_cnf_import.setI_AUFNR(String.valueOf(repairResultDtl.get("E_AUFNR")));
						} else {
							zpm_order_cnf_import.setI_AUFNR(" ");
						}
						
						// 1-3. 작업제목
						String[] REPAIR_DESCRIPTION = parseStringByBytes(repairResultVO.getREPAIR_DESCRIPTION(), 40, "UTF-8");
						zpm_order_cnf_import.setI_KTEXT1(REPAIR_DESCRIPTION[0]);
						
						// 1-4. 활동유형
//						zpm_order_cnf_import.setI_ILART(repairResultVO.getREPAIR_TYPE()); //활동유형
						zpm_order_cnf_import.setI_ILART("");	
						
						
						// 고장 시작/종료 시간 String -> Date
						Date DATE_TROUBLE_TIME1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(repairResultVO.getTROUBLE_TIME1());
						Date DATE_TROUBLE_TIME2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(repairResultVO.getTROUBLE_TIME2());
						
						// 정비 시작/종료 시간 String -> Date
						Date DATE_REPAIR_TIME1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(repairResultVO.getREPAIR_TIME1());
						Date DATE_REPAIR_TIME2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(repairResultVO.getREPAIR_TIME2());
						
						// 고장 시작/종료 시간 Date -> String 포멧변경
						String TROUBLE_TIME1 = new SimpleDateFormat("yyyyMMddHHmmss").format(DATE_TROUBLE_TIME1);
						String TROUBLE_TIME2 = new SimpleDateFormat("yyyyMMddHHmmss").format(DATE_TROUBLE_TIME2);
						
						// 정비 시작/종료 시간 Date -> String 포멧변경
						String REPAIR_TIME1 = new SimpleDateFormat("yyyyMMddHHmmss").format(DATE_REPAIR_TIME1);
						String REPAIR_TIME2 = new SimpleDateFormat("yyyyMMddHHmmss").format(DATE_REPAIR_TIME2);
						
						
						RepairRequestVO repairRequestVO = new RepairRequestVO();
						repairRequestVO.setCompanyId((String) SessionUtil.getAttribute("ssCompanyId"));
//						repairRequestVO.setSsLocation((String) SessionUtil.getAttribute("ssLocation"));
						repairRequestVO.setREPAIR_REQUEST(String.valueOf(repairResultDtl.get("REPAIR_REQUEST")));
						HashMap<String, Object> repairRequestDtl = repairRequestMapper.selectRepairRequestDtl(repairRequestVO);
						
//						zpm_order_cnf_import.setI_TPLNR(String.valueOf(repairRequestDtl.get("LINE")));
						zpm_order_cnf_import.setI_TPLNR("");								// 1-5. 기능위치
						zpm_order_cnf_import.setI_EQUNR(String.valueOf(repairRequestDtl.get("EQUIPMENT_UID"))); // 1-6. 설비 ID
//						zpm_order_cnf_import.setI_INGPR("010");								// 1-7. 보전계획자
						zpm_order_cnf_import.setI_INGPR("");								// 1-7. 보전계획자
						zpm_order_cnf_import.setI_GSBER("");								// 1-8. 사업영역
						zpm_order_cnf_import.setI_PLANT("5100");							// 1-9. 플랜트
//						zpm_order_cnf_import.setI_ARBPL("DM01");							// 1-10. 주요작업장
						zpm_order_cnf_import.setI_ARBPL("");								// 1-10. 주요작업장
						zpm_order_cnf_import.setI_GSTRP(REPAIR_TIME1.substring(0, 8));		// 1-11. 기본시작일자 = 작업시작년월일(고장 시작일)
						zpm_order_cnf_import.setI_GLTRP(REPAIR_TIME2.substring(0, 8));		// 1-12. 기본종료일자 = 작업종료년월일(고장 종료일)
						zpm_order_cnf_import.setI_PLWERKS("5100");							// 1-13. 계획플랜트
						zpm_order_cnf_import.setI_WERKS("5100");							// 1-14. 정비플랜트
						zpm_order_cnf_import.setI_METHOD("CREATE");							// 1-15. CRUD = 신규 : CREATE 고정
						
//						zpm_order_cnf_import.setI_QMGRP(repairResultVO.getCATALOG_D_GRP()); // 1-16. 코딩그룹
//						if (!"".equals(zpm_order_cnf_import.getI_QMGRP()) ||zpm_order_cnf_import.getI_QMGRP()==null ) {
//							zpm_order_cnf_import.setI_QMCOD(repairResultVO.getCATALOG_D());	// 1-17. 코딩
//						}
//						
//						zpm_order_cnf_import.setI_OTGRP(repairResultVO.getCATALOG_B_GRP()); // 1-18. 대상그룹
//						if (!"".equals(zpm_order_cnf_import.getI_OTGRP()) ||zpm_order_cnf_import.getI_OTGRP()==null ) {
//							zpm_order_cnf_import.setI_OTCOD(repairResultVO.getCATALOG_B());	// 1-19. 대상
//						}
//						
//						zpm_order_cnf_import.setI_FEGRP(repairResultVO.getCATALOG_C_GRP()); // 1-20. 손상그룹
//						if (!"".equals(zpm_order_cnf_import.getI_FEGRP()) || zpm_order_cnf_import.getI_FEGRP()==null ) {
//							zpm_order_cnf_import.setI_FECOD(repairResultVO.getCATALOG_C());	// 1-21. 손상
//						}
//						
//						zpm_order_cnf_import.setI_URGRP(repairResultVO.getCATALOG_5_GRP()); // 1-22. 원인그룹
//						if (!"".equals(zpm_order_cnf_import.getI_URGRP()) || zpm_order_cnf_import.getI_URGRP()==null ) {
//							zpm_order_cnf_import.setI_URCOD(repairResultVO.getCATALOG_5());	// 1-23.  원인
//						}
						
						// 1-24. 고장여부
						if(repairResultVO.getTROUBLE_TIME1().equals(repairResultVO.getTROUBLE_TIME2())) {
							zpm_order_cnf_import.setI_MSAUS(" ");	//가동중
						} else {
							zpm_order_cnf_import.setI_MSAUS("X");	//고장
						}
							
						zpm_order_cnf_import.setI_AUSVN(TROUBLE_TIME1.substring(0, 8));							        // 1-25. 오작동시작일자
						zpm_order_cnf_import.setI_AUZTV(TROUBLE_TIME1.substring(8, TROUBLE_TIME1.length()));	        // 1-26. 오작동시작시간
						                                                                                                
						zpm_order_cnf_import.setI_AUSBS(TROUBLE_TIME2.substring(0, 8));							        // 1-27. 오작동종료일자 : 고장일 경우 필수
						zpm_order_cnf_import.setI_AUZTB(TROUBLE_TIME2.substring(8, TROUBLE_TIME2.length()));	        // 1-28. 오작동종료시간 : 고장일 경우 필수
												
						zpm_order_cnf_import.setI_EAUSZT(String.valueOf((DATE_TROUBLE_TIME2.getTime()-DATE_TROUBLE_TIME1.getTime()) / (1000*60*60)));	// 1-29. 고장기간(H)
						zpm_order_cnf_import.setI_MAUEH("H");															// 1-30. 고장시간단위 : 'H' 고정
						
						
						// 2. T_WORK (작업 내역 셋팅)
						List<ZPM_ORDER_CNF_T_WORK> t_work = new ArrayList<ZPM_ORDER_CNF_T_WORK>();
						
						ZPM_ORDER_CNF_T_WORK zpm_order_cnf_t_work = new ZPM_ORDER_CNF_T_WORK();
						zpm_order_cnf_t_work.setVORNR("0010");					// 2-1. 작업OP : '0010' 고정
						zpm_order_cnf_t_work.setSTEUS("PM01");					// 2-2. 제어키
						zpm_order_cnf_t_work.setLTXA1(REPAIR_DESCRIPTION[0]);	// 2-3. 작업내역 : 한글 20자 or 영문 40자
						zpm_order_cnf_t_work.setWERKS("5100");					// 2-4. 정비플랜트
						zpm_order_cnf_t_work.setARBPL("DM01");					// 2-5. 작업장
						
//						String[] member_arr = repairResultVO.getMemberArr().split(",");
//						zpm_order_cnf_t_work.setANZZL(String.valueOf(member_arr.length)); //작업인원수
//						zpm_order_cnf_t_work.setARBEI(String.valueOf(member_arr.length * Integer.parseInt(zpm_order_cnf_import.getI_EAUSZT()))); //총공수(소수점 2자리)
						
						// 작업인원 및 총 작업시간 조회
						HashMap<String, Object> workLogItemSum  = repairResultMapper.selectWorkLogItemSum(repairResultVO);
						
						zpm_order_cnf_t_work.setARBEI(String.valueOf(workLogItemSum.get("WORK_TIME")));	 // 2-6. 총작업시간(소수점 2자리)
						zpm_order_cnf_t_work.setDAUNO(zpm_order_cnf_import.getI_EAUSZT());				 // 2-7. 고장기간('H')
						zpm_order_cnf_t_work.setISMNU("H");												 // 2-8. 시간단위('H')
						zpm_order_cnf_t_work.setANZZL(String.valueOf(workLogItemSum.get("MEMBER_CNT"))); // 2-9. 작업인원수
						zpm_order_cnf_t_work.setLARNT("PM01");					   						 // 2-10. 액티비티 유형
						zpm_order_cnf_t_work.setINDET("0");												 // 2-11. 계산키 : '0'고정
						zpm_order_cnf_t_work.setMETHOD("CREATE");										 // 2-12. CRUD : CREATE
						zpm_order_cnf_t_work.setISDD(REPAIR_TIME1.substring(0, 8));						 // 2-13. 작업 시작일자						  
						zpm_order_cnf_t_work.setISDZ(REPAIR_TIME1.substring(8, REPAIR_TIME1.length()));  // 2-14. 작업 시작시간 
						zpm_order_cnf_t_work.setIEDD(REPAIR_TIME2.substring(0, 8));						 // 2-15. 작업 종료시간 
						zpm_order_cnf_t_work.setIEDZ(REPAIR_TIME2.substring(8, REPAIR_TIME2.length()));  // 2-16. 작업 종료시간 
						zpm_order_cnf_t_work.setPERNR(" ");												 // 2-17. 작업자ID : '공백'고정
						zpm_order_cnf_t_work.setLTXA2(" ");												 // 2-18. 확정내역 : '공백'고정 
						zpm_order_cnf_t_work.setAUERU("X");												 // 2-19. 최종확정여부 :'X'고정
						zpm_order_cnf_t_work.setBUDAT(REPAIR_TIME2.substring(0, 8)); 					 // 2-20. 전기일자(작업종료일자)
						
						t_work.add(zpm_order_cnf_t_work);
						zpm_order_cnf_import.setZpm_order_cnf_t_work(t_work);
						
						
						// 3. T_LINES (작업내용)
						String[] TDLINE = parseStringByBytes(repairResultVO.getREPAIR_DESCRIPTION(), 100, "UTF-8");
						List<ZPM_ORDER_CNF_T_LINES> t_lines = new ArrayList<ZPM_ORDER_CNF_T_LINES>();
						
						for(int i=0; i<TDLINE.length; i++) {
							ZPM_ORDER_CNF_T_LINES zpm_order_cnf_t_lines = new ZPM_ORDER_CNF_T_LINES();
							zpm_order_cnf_t_lines.setTDLINE(TDLINE[i]);	//텍스트라인 : 장문의 설명인 경우 무한대로 입력가능(132자분할
							
							t_lines.add(zpm_order_cnf_t_lines);
						}
						
						zpm_order_cnf_import.setZpm_order_cnf_t_lines(t_lines);
						
						
						// 4. T_COMP (사용자재)
						List<ZPM_ORDER_CNF_T_COMP> t_comp = new ArrayList<ZPM_ORDER_CNF_T_COMP>();
						ZPM_ORDER_CNF_T_COMP zpm_order_cnf_t_comp = new ZPM_ORDER_CNF_T_COMP();
						int i = 10;
						
						// 기존에 등록된 사용자재 변경
						if(!("".equals(repairResultVO.getMaterialUsedArr()) || repairResultVO.getMaterialUsedArr() == null)){
							List<HashMap<String, Object>> materialUsedArr = JSONArray.fromObject(convertStandardJSONString(repairResultVO.getMaterialUsedArr()));

							for(Map<String, Object> item : materialUsedArr){
								zpm_order_cnf_t_comp = new ZPM_ORDER_CNF_T_COMP();
								zpm_order_cnf_t_comp.setMATERIAL(String.valueOf(item.get("MATERIAL_UID")));		// 4-1. 자재코드
								zpm_order_cnf_t_comp.setPLANT("5100");											// 4-2. 플랜트
								zpm_order_cnf_t_comp.setSTGE_LOC("9102");										// 4-3. 저장위치
								zpm_order_cnf_t_comp.setITEM_CAT("L");											// 4-4. 품목범주
								
								// 4-5. 품목번호 ('0010' 형태) 
								String suffix = String.format("%04d", i);
								zpm_order_cnf_t_comp.setITEM_NUMBER(suffix);
								i = i + 10;
								
								zpm_order_cnf_t_comp.setACTIVITY("0010");										// 4-6. 공정번호
								zpm_order_cnf_t_comp.setQUANTITY(String.valueOf(item.get("QNTY")));				// 4-7. 소요수량
								zpm_order_cnf_t_comp.setQUANTITY_UNIT(String.valueOf(item.get("UNIT")));		// 4-8. 단위
								zpm_order_cnf_t_comp.setMETHOD("CREATE");										// 4-9. CRUD
								
								//수량이 0보다 크다면 출고 내역 등록
								if(Integer.parseInt(repairResultVO1.getQNTY()) > 0 && "N".equals(String.valueOf(item.get("DEL_YN"))) ){
									t_comp.add(zpm_order_cnf_t_comp);
								}
							}
						}
						
						// 추가 자재 내역 등록
						if(!("".equals(repairResultVO.getMaterialArr()) || repairResultVO.getMaterialArr() == null)){
							
							List<HashMap<String, Object>> materialArr = JSONArray.fromObject(convertStandardJSONString(repairResultVO.getMaterialArr()));

							for(Map<String, Object> item : materialArr){
								zpm_order_cnf_t_comp = new ZPM_ORDER_CNF_T_COMP();
								zpm_order_cnf_t_comp.setMATERIAL(String.valueOf(item.get("MATERIAL_UID")));		// 4-1. 자재코드
								zpm_order_cnf_t_comp.setPLANT("5100");											// 4-2. 플랜트
								zpm_order_cnf_t_comp.setSTGE_LOC("9102");										// 4-3. 저장위치
								zpm_order_cnf_t_comp.setITEM_CAT("L");											// 4-4. 품목범주
								
								// 4-5. 품목번호 ('0010' 형태)
								String suffix = String.format("%04d", i);
								zpm_order_cnf_t_comp.setITEM_NUMBER(suffix);
								i = i + 10;

								zpm_order_cnf_t_comp.setACTIVITY("0010");										// 4-6. 공정번호
								zpm_order_cnf_t_comp.setQUANTITY(String.valueOf(item.get("QNTY")));				// 4-7. 소요수량
								zpm_order_cnf_t_comp.setQUANTITY_UNIT(String.valueOf(item.get("UNIT")));		// 4-8. 단위
								zpm_order_cnf_t_comp.setMETHOD("CREATE");										// 4-9. CRUD
								
								//수량이 0보다 크다면 출고 내역 등록
								if(Integer.parseInt(repairResultVO1.getQNTY()) > 0 && "N".equals(String.valueOf(item.get("DEL_YN")))){
									t_comp.add(zpm_order_cnf_t_comp);
								}
								
							}
						}
						zpm_order_cnf_import.setZpm_order_cnf_t_comp(t_comp);
						
						
						// 5. T_TASK (정비원 개별 실적)
						List<ZPM_ORDER_CNF_T_TASK> zpm_order_cnf_t_task = new ArrayList<ZPM_ORDER_CNF_T_TASK>();
						ZPM_ORDER_CNF_T_TASK t_task = new ZPM_ORDER_CNF_T_TASK();
						
						if(!("".equals(repairResultVO.getMemberArr()) || repairResultVO.getMemberArr() == null)){
											
							// JSON -> String
							List<HashMap<String, Object>> memberArr = JSONArray.fromObject(convertStandardJSONString(repairResultVO.getMemberArr()));
							
							for(Map<String, Object> item : memberArr){
								
								t_task = new ZPM_ORDER_CNF_T_TASK();
								
								// 5-1. 코드그룹 -> 던킨 'PM1' 고정
								t_task.setMNGRP("PM1");	
								
								// 5-2. 타스크코드(활동 유형)
								if("110".equals(repairResultVO.getREPAIR_TYPE())) {			// 돌발정비
									t_task.setMNCOD("2");
								}
								else if("120".equals(repairResultVO.getREPAIR_TYPE())) {	// 일반정비
									t_task.setMNCOD("2");
								}
								else if("130".equals(repairResultVO.getREPAIR_TYPE())) {	// 개선정비
									t_task.setMNCOD("2");
								}
								else if("150".equals(repairResultVO.getREPAIR_TYPE())) {	// 예방정비
									t_task.setMNCOD("1");
								}
								
								// 개인 실적 조회
								repairResultVO.setWORK_LOG(String.valueOf(item.get("WORK_LOG"))); 		// 작업이력ID
								repairResultVO.setUSER_ID(String.valueOf(item.get("USER_ID")));			// 정비원ID
								repairResultVO.setDEL_YN(String.valueOf(item.get("DEL_YN")));			// 삭제 여부
								
								// 정비원이 삭제되지 않은 경우에만 정비원 개인실적 등록
								if("N".equals(repairResultVO.getDEL_YN())){
									// 5-3. 직무책임자 ID (사번)
									// 정비원 사번 조회
									repairResultVO.setUSER_ID(String.valueOf(item.get("USER_ID")));
									String member_id = repairResultMapper.selectMemberIdInfo(repairResultVO);
									
									t_task.setPARNR(member_id);
									
									String worklogarr[] =  new String[1];
									worklogarr[0] =  String.valueOf(item.get("WORK_LOG"));
									repairResultVO.setWorkLogArr(worklogarr);
									
									List<HashMap<String, Object>> rtnMap = repairResultMapper.selectWorkLogItemList(repairResultVO);
									
									if(rtnMap !=null){
										
										for(Map<String, Object> item2 : rtnMap){
											
											String DATE_START = String.valueOf(item2.get("DATE_START"));
											t_task.setPSTER(DATE_START.substring(0, 8));					// 5-4. SAP:계획된시작일    = WEB:개인별 작업시작일자
											t_task.setPSTUR(DATE_START.substring(8, DATE_START.length()));	// 5-5. SAP:계획된시작시간 = WEB:개인별 작업시작시간
											
											String DATE_END = String.valueOf(item2.get("DATE_END"));
											t_task.setPETER(DATE_END.substring(0, 8));                      // 5-6. SAP:계획된종료일    = WEB:개인별 작업종료일자
											t_task.setPETUR(DATE_END.substring(8, DATE_END.length()));      // 5-7. SAP:계획된종료시간 = WEB:개인별 작업종료시간
											
											zpm_order_cnf_t_task.add(t_task);
										}
									}
								}
							}
						}
						
						zpm_order_cnf_import.setZpm_order_cnf_t_task(zpm_order_cnf_t_task);
					
						
						// 6. RFC 통신
						//ZPM_ORDER_CNF_EXPORT tables = rfcService.setOrderCompInfo(zpm_order_cnf_import);
						
						/**  [YJJANG] 6. RFC 통신  **/
						ZPM_ORDER_CNF_EXPORT tables = new ZPM_ORDER_CNF_EXPORT();
						
						tables.setE_RESULT( "S" );
						
						
						// 6-1. RFC 성공일 경우
						if("S".equals(tables.getE_RESULT())) {
							
							// 6-1-1. SAP 작업오더확정 처리 (완료)
							repairResultVO.setREPAIR_STATUS(CodeConstants.EPMS_REPAIR_STATUS_COMPLETE);
							repairResultVO.setE_RESULT(tables.getE_RESULT());
							repairResultVO.setE_MESSAGE(tables.getE_MESSAGE());
							repairResultVO.setE_AUFNR(tables.getE_AUFNR());			// 오더번호
							repairResultVO.setE_RMZHL(tables.getE_RMZHL());			// 확정카운터 : 확정취소할 경우 번호
							
							repairResultMapper.updateAfterSAPOrder(repairResultVO);	
							
							// 6-1-2. 기기이력 등록
							EquipmentHistoryVO equipmentHistoryVO = new EquipmentHistoryVO();
							equipmentHistoryVO.setEQUIPMENT(repairResultVO.getEQUIPMENT());
							
							if("110".equals(repairResultVO.getREPAIR_TYPE())){			// 돌발정비
								equipmentHistoryVO.setHISTORY_TYPE(CodeConstants.EPMS_HISTORY_TYPE_110);
							} 
							else if("120".equals(repairResultVO.getREPAIR_TYPE())) {	// 일반정비
								equipmentHistoryVO.setHISTORY_TYPE(CodeConstants.EPMS_HISTORY_TYPE_120);
							} 
							else if("130".equals(repairResultVO.getREPAIR_TYPE())) {	// 개선정비
								equipmentHistoryVO.setHISTORY_TYPE(CodeConstants.EPMS_HISTORY_TYPE_130);
							} 
							else if("150".equals(repairResultVO.getREPAIR_TYPE())){		// 예방정비
								equipmentHistoryVO.setHISTORY_TYPE(CodeConstants.EPMS_HISTORY_TYPE_150);
							}
							equipmentHistoryVO.setHISTORY_DESCRIPTION(repairResultVO.getREPAIR_DESCRIPTION());
							equipmentHistoryVO.setDETAIL_ID(repairResultVO.getREPAIR_RESULT());
							equipmentHistoryVO.setUserId(repairResultVO.getMANAGER());
							
							equipmentHistoryMapper.insertEquipmentHistory(equipmentHistoryVO);
							
							// 6-1-3. 예방정비의 경우, 점검상태(완료) 업데이트
							if(CodeConstants.EPMS_REQUEST_TYPE_PREVENTIVE.equals(repairResultVO.getREQUEST_TYPE())){
								repairResultVO.setSTATUS(CodeConstants.EPMS_PREVENTIVE_STATUS_COMPLETE);
								// 예방보전실적 업데이트
								repairResultMapper.updatePreventiveResult(repairResultVO);
							}
							 
							// 6-1-4. 자재단가(출고가) 업데이트
							if ( tables != null 
									&& tables.getZpm_order_cnf_t_comp() != null
									&& tables.getZpm_order_cnf_t_comp().size() > 0 ) {
								
								for (ZPM_ORDER_CNF_T_COMP rtn_t_comp : tables.getZpm_order_cnf_t_comp()) {
									repairResultVO.setTARGET_TYPE("REPAIR");								// 대상유형(REPAIR:정비, PREVENTIVE:예방보전)
									repairResultVO.setTARGET_ID(repairResultVO.getREPAIR_RESULT());			// 대상ID(정비실적ID)
									repairResultVO.setMATERIAL_UID(rtn_t_comp.getMATERIAL().toString());	// 자재코드
									repairResultVO.setPRICE(rtn_t_comp.getVERPR().trim().toString());		// 자재 단가
									repairResultVO.setUPD_ID(ssUserId);
									
									repairResultMapper.updateMaterialInoutItem(repairResultVO);
								}
								
							}
												
						} 
						// 6-2. RFC 실패일 경우
						else {
							
							// 6-2-1. SAP 작업오더확정 실패 등록
							repairResultVO.setREPAIR_STATUS(CodeConstants.EPMS_REPAIR_STATUS_PLAN);
							repairResultVO.setE_RESULT(tables.getE_RESULT());
							repairResultVO.setE_MESSAGE(tables.getE_MESSAGE());
							if(!"".equals(tables.getE_AUFNR()) && tables.getE_AUFNR() != null) repairResultVO.setE_AUFNR(tables.getE_AUFNR());
							if(!"".equals(tables.getE_RMZHL()) && tables.getE_RMZHL() != null) repairResultVO.setE_RMZHL(tables.getE_RMZHL());
							repairResultMapper.updateAfterSAPOrder(repairResultVO);
							
							
							resultMap.put("E_RESULT", tables.getE_RESULT());
							resultMap.put("E_MESSAGE", tables.getE_MESSAGE());
							logger.info("E_RESULT:" + tables.getE_RESULT());
							logger.info("E_MESSAGE:" + tables.getE_MESSAGE());
							
							// return시 jsp에서 필요한 데이터
							resultMap.put("updateYn", "SUCCESS");
							resultMap.put("resultDtl", repairResultMapper.selectRepairResultDtl(repairResultVO));
							
							return resultMap;
						}
					}
					else{
						resultMap.put("updateYn", "NO_REPAIR_TIME");
					}
				}
				else{
					resultMap.put("updateYn", "NO_COMPLETE");
				}
			}
			
		}
		else{
			
			resultMap.put("updateYn", "NO_REPAIR");
		}
		// if(repairResultVO.getFlag().equals(CodeConstants.FLAG_COMPLETE))
		
		
		resultMap.put("resultDtl", repairResultMapper.selectRepairResultDtl(repairResultVO));
			
		return resultMap;		
	}
	
	/**
	 * 정비원 개별실적 - 정비완료취소
	 * 
	 * @author 김영환
	 * @since 2018.12.20
	 * @param RepairResultVO repairResultVO
	 * @return int
	 * @throws Exception 
	 */
	@Override
	public int updateWorkLogStatus(RepairResultVO repairResultVO) {
		return repairResultMapper.updateWorkLogStatus(repairResultVO);
	}
	
	/**
	 * 정비원 추가/삭제
	 *
	 * @author 서정민
	 * @since 2019.03.06
	 * @param AttachVO attachVO
	 * @param RepairResultVO repairResultVO
	 * @return HashMap<String, Object>
	 * @throws Exception 
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	public HashMap<String, Object> updateWorkMember(RepairResultVO repairResultVO) throws Exception {
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		int	repairResultCheck = repairResultMapper.selectRepairResultCheck(repairResultVO);
		
		if(repairResultCheck == 1){
			
			String ssUserId = (String) SessionUtil.getAttribute("ssUserId");
			HashMap<String, Object> repairResultDtl = repairResultMapper.selectRepairResultDtl(repairResultVO);
			
			if(ssUserId.equals(String.valueOf(repairResultDtl.get("MANAGER")))){
				//정비원 등록
				if(!("".equals(repairResultVO.getMemberArr()) || repairResultVO.getMemberArr() == null)){
					
					// 작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)
					// WORK_TYPE 파라미터로 던져야함
					repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR);
					
					// 작업대상ID 세팅
					repairResultVO.setWORK_ID(repairResultVO.getREPAIR_RESULT());
					
					// String -> JSON
					List<HashMap<String, Object>> memberArr = JSONArray.fromObject(convertStandardJSONString(repairResultVO.getMemberArr()));
					
					for(Map<String, Object> item : memberArr){
						
						repairResultVO.setWORK_LOG(String.valueOf(item.get("WORK_LOG"))); 		// 작업이력ID
						repairResultVO.setUSER_ID(String.valueOf(item.get("USER_ID")));			// 정비원ID
						repairResultVO.setDEL_YN(String.valueOf(item.get("DEL_YN")));			// 삭제 여부
						
						int	workLogCheck = repairResultMapper.selectWorkLogCheck(repairResultVO);
						
						// 등록되지 않은 정비원의 경우
						if ( workLogCheck == 0 && "N".equals(repairResultVO.getDEL_YN())) {
							
							// WorkLog테이블 Seq 조회
							String workLogSeq = repairResultMapper.selectWorkLogSeq(repairResultVO);
							repairResultVO.setWORK_LOG(workLogSeq);
							
							// 정비원등록  WORK_LOG INSERT
							repairResultMapper.insertWorkLog(repairResultVO);
							
							// 정비원이 추가된 경우 정비실적 기본 세팅
							SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Date today = new Date();
							repairResultVO.setDATE_START(date.format(today));						// 정비원 정비 시작일시
							repairResultVO.setDATE_END(date.format(today));							// 정비원 정비 종료일시
							repairResultVO.setITEM_DESCRIPTION("");				                    // 정비원 정비 내용
							
							// 정비원실적 WORK_LOG_ITEM INSERT
							repairResultMapper.insertWorkLogItem(repairResultVO);
						}
						// 기존에 등록된 정비원의 경우
						else {
							repairResultMapper.updateWorkLog(repairResultVO);
						}
						
					}
				}
				resultMap.put("updateYn", "SUCCESS");
			}
			else{
				resultMap.put("updateYn", "NO_MANAGER");
			}
		}
		else{
			resultMap.put("updateYn", "NO_REPAIR");
		}
		
		resultMap.put("workMember", repairResultMapper.selectWorkMemberList(repairResultVO));
			
		return resultMap;		
	}

	public String[] parseStringByBytes(String raw, int len, String encoding) {  
		if (raw == null) return null;
		String[] ary = null;
		
		try {
			// raw 의 byte
			byte[] rawBytes = raw.getBytes(encoding);
			int rawLength = rawBytes.length;
			int index = 0;
			int minus_byte_num = 0;
			int offset = 0;
			int hangul_byte_num = encoding.equals("UTF-8") ? 3 : 2;
			
			if(rawLength > len){
				int aryLength = (rawLength / len) + (rawLength % len != 0 ? 1 : 0);
				ary = new String[aryLength];
				
				for(int i=0; i<aryLength; i++){
					minus_byte_num = 0;
					offset = len;
					
					if(index + offset > rawBytes.length){
						offset = rawBytes.length - index;
					}
					
					for(int j=0; j<offset; j++){      
						if(((int)rawBytes[index + j] & 0x80) != 0){
							minus_byte_num ++;
						}
					}     
					
					if(minus_byte_num % hangul_byte_num != 0){
						offset -= minus_byte_num % hangul_byte_num;
					}     
					
					ary[i] = new String(rawBytes, index, offset, encoding);     
					index += offset ;
				}    
			} else {
				ary = new String[]{raw};
			}    
		} catch(Exception e) {
		
		}     
		
		return ary;
	}
	
	/**
	 * 정비실적 첨부파일 수정
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	@Override
	public void updateRepairResultAttach(RepairResultVO repairResultVO) {
		repairResultMapper.updateRepairResultAttach(repairResultVO);
	}

	/**
	 * 정비실적에 등록된 자재 목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairUsedMaterialList(RepairResultVO repairResultVO) {
		return repairResultMapper.selectRepairUsedMaterialList(repairResultVO);
	}

	/**
	 * 정비실적등록/조회 - 정비확정취소 : 정비완료 건 승인으로 상태값 변경
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	@Override
	public HashMap<String, Object> updateCompleteStatus(RepairResultVO repairResultVO) throws Exception{
		
		HashMap<String, Object> repairInfo = repairResultMapper.selectRepairResultDtl(repairResultVO);
		ZPM_CNF_CANCEL_T_CNF zpm_cnf_cancel_t_cnf = new ZPM_CNF_CANCEL_T_CNF();
		zpm_cnf_cancel_t_cnf.setAUFNR(String.valueOf(repairInfo.get("E_AUFNR")));
		zpm_cnf_cancel_t_cnf.setVORNR("0010");
		  
		if ( repairInfo.get("RMZHL") == null || "".equals(repairInfo.get("RMZHL"))){
			zpm_cnf_cancel_t_cnf.setRMZHL("");
		} else {
			zpm_cnf_cancel_t_cnf.setRMZHL(String.valueOf(repairInfo.get("RMZHL")));
		}
		
		List<ZPM_CNF_CANCEL_T_CNF> t_cnf = new ArrayList<ZPM_CNF_CANCEL_T_CNF>();
		t_cnf.add(zpm_cnf_cancel_t_cnf);
		
		ZPM_CNF_CANCEL_IMPORT zpm_cnf_cancel_import = new ZPM_CNF_CANCEL_IMPORT();
		zpm_cnf_cancel_import.setT_cnf(t_cnf);
		
		ZPM_CNF_CANCEL_EXPORT zpm_cnf_cancel_export = rfcService.setOrderCancelInfo(zpm_cnf_cancel_import);
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("E_RESULT", zpm_cnf_cancel_export.getE_RESULT());
		map.put("E_MESSAGE", zpm_cnf_cancel_export.getE_MESSAGE());
		logger.info("E_RESULT:" + zpm_cnf_cancel_export.getE_RESULT());
		logger.info("E_MESSAGE:" + zpm_cnf_cancel_export.getE_MESSAGE());
			
		if("S".equals(zpm_cnf_cancel_export.getE_RESULT() )) {
			
			//정비상태 변경(정비완료>배정)
			repairResultVO.setREPAIR_STATUS(CodeConstants.EPMS_REPAIR_STATUS_PLAN);
			repairResultVO.setE_RESULT(zpm_cnf_cancel_export.getE_RESULT());
			repairResultVO.setE_MESSAGE(zpm_cnf_cancel_export.getE_MESSAGE());
			
			// SAP예방보전 오더일 경우
			if(CodeConstants.EPMS_REQUEST_TYPE_PREVENTIVE_SAP.equals(String.valueOf(repairInfo.get("REQUEST_TYPE")))){
				for(ZPM_CNF_CANCEL_T_CNF t_cnf_item : zpm_cnf_cancel_export.getT_cnf()){
					repairResultVO.setE_AUFNR(t_cnf_item.getAUFNR());	//작업오더
					repairResultVO.setE_RMZHL(t_cnf_item.getRMZHL());	//확정카운터
				}
			}
			else{
				repairResultVO.setE_AUFNR("");	//작업오더
				repairResultVO.setE_RMZHL("");	//확정카운터
			}
			
			repairResultMapper.updateCompleteCancelStatus(repairResultVO);
			
			//기기이력 삭제
			EquipmentHistoryVO equipmentHistoryVO = new EquipmentHistoryVO();
			equipmentHistoryVO.setUserId(repairResultVO.getUserId());
			equipmentHistoryVO.setHISTORY_TYPE("'110','120','130','140','150'"); // 110:돌발정비, 120:일상정비, 130:보전정비, 140:사후정비, 150:예방정비
			equipmentHistoryVO.setDETAIL_ID(repairResultVO.getREPAIR_RESULT());
			equipmentHistoryMapper.deleteEquipmentHistory(equipmentHistoryVO);
			
			if(CodeConstants.EPMS_REQUEST_TYPE_PREVENTIVE.equals(String.valueOf(repairInfo.get("REQUEST_TYPE")))) {
				repairResultVO.setSTATUS(CodeConstants.EPMS_PREVENTIVE_STATUS_PROBLEM);
				repairResultMapper.updatePreventiveResult(repairResultVO);
			}
		}
		return map;
	}
	
	/**
	 *  특수문자 변환 처리
	 *
	 * @author 김영환
	 * @since 2018.11.29
	 * @param String data_json
	 * @return String
	 */
	public static String convertStandardJSONString(String data_json) {
		
        data_json = data_json.replaceAll("&amp;", "&");
        data_json = data_json.replaceAll("&quot;", "\"");
        data_json = data_json.replaceAll("&gt;", ">");
        data_json = data_json.replaceAll("&lt;", "<");
        
        return data_json;
    }
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - 정비실적 건수 (메인화면 뱃지카운트)
	 *
	 * @author 서정민
	 * @since 2018.05.24
	 * @param RepairResultVO repairResultVO
	 * @return int
	 */
	@Override
	public int selectRepairResultCnt(RepairResultVO repairResultVO) {
		return repairResultMapper.selectRepairResultCnt(repairResultVO);
	}

	/**
	 * 모바일 - 정비실적 목록조회
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairResultList2(RepairResultVO repairResultVO) {
		return repairResultMapper.selectRepairResultList2(repairResultVO);
	}
	
	/**
	 * 모바일 - 정비실적 상세조회
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	@Override
	public HashMap<String, Object> selectRepairResultDtl2(RepairResultVO repairResultVO) {
		return repairResultMapper.selectRepairResultDtl2(repairResultVO);
	}
	
	/**
	 * 모바일 - 정비원 정비 실적 ITEM 조회
	 * 
	 * @author 김영환
	 * @since 2018.12.20
	 * @param RepairResultVO repairResultVO1
	 */
	@Override
	public List<HashMap<String, Object>> selectWorkLogItemList2(RepairResultVO repairResultVO){
		return repairResultMapper.selectWorkLogItemList2(repairResultVO);
	}

	/**
	 * 모바일 - 본인 소속 제외 파트목록
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectPartOptList2(RepairResultVO repairResultVO) {
		return repairResultMapper.selectPartOptList2(repairResultVO);
	}

	/**
	 * 모바일 - 공통코드(정비유형목록 조회)
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairTypeList(RepairResultVO repairResultVO) {
		return repairResultMapper.selectRepairTypeList(repairResultVO);
	}

	/**
	 * 모바일 - 공통코드(고장유형목록 조회)
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectTroubleType1List() {
		return repairResultMapper.selectTroubleType1List();
	}

	/**
	 * 모바일 - 공통코드(조치목록 조회)
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectTroubleType2List() {
		return repairResultMapper.selectTroubleType2List();
	}
	
	/**
	 * 모바일 - 공통코드([SAP-코딩그룹]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectCatalogDGrpList(RepairResultVO repairResultVO) {
		return repairResultMapper.selectCatalogDGrpList(repairResultVO);
	}
	
	/**
	 * 모바일 - 공통코드([SAP-코딩]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectCatalogDList(RepairResultVO repairResultVO) {
		return repairResultMapper.selectCatalogDList(repairResultVO);
	}
	
	/**
	 * 모바일 - 공통코드([SAP-대상그룹]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectCatalogBGrpList(RepairResultVO repairResultVO) {
		return repairResultMapper.selectCatalogBGrpList(repairResultVO);
	}
	
	/**
	 * 모바일 - 공통코드([SAP-대상]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectCatalogBList(RepairResultVO repairResultVO) {
		return repairResultMapper.selectCatalogBList(repairResultVO);
	}
	
	/**
	 * 모바일 - 공통코드([SAP-손상]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectCatalogCGrpList(RepairResultVO repairResultVO) {
		return repairResultMapper.selectCatalogCGrpList(repairResultVO);
	}
	
	/**
	 * 모바일 - 공통코드([SAP-손상]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectCatalogCList(RepairResultVO repairResultVO) {
		return repairResultMapper.selectCatalogCList(repairResultVO);
	}
	
	/**
	 * 모바일 - 공통코드([SAP-원인그룹]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectCatalog5GrpList(RepairResultVO repairResultVO) {
		return repairResultMapper.selectCatalog5GrpList(repairResultVO);
	}
	
	/**
	 * 모바일 - 공통코드([SAP-원인]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectCatalog5List(RepairResultVO repairResultVO) {
		return repairResultMapper.selectCatalog5List(repairResultVO);
	}

	/**
	 * 모바일 - 정비원 등록 시 정비원 목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairMemberList(RepairResultVO repairResultVO) {
		return repairResultMapper.selectRepairMemberList(repairResultVO);
	}

	/**
	 * 모바일 - 사용자재 등록 시 설비 BOM 조회
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectEquipmentBomList(RepairResultVO repairResultVO) {
		return repairResultMapper.selectEquipmentBomList(repairResultVO);
	}
	
}
