package aspn.hello.epms.repair.result.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.com.model.AttachVO;
import aspn.hello.epms.repair.result.model.RepairResultVO;

/**
 * [고장관리-정비실적등록/조회] Service Class
 * 
 * @author 김영환
 * @since 2018.02.19
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.19		김영환		최초 생성
 *   
 * </pre>
 */

public interface RepairResultService {

	/**
	 * 정비실적현황 조회
	 *
	 * @author 김영환
	 * @since 2018.02.19
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairResultList(RepairResultVO repairResultVO);
	
	/**
	 * 정비실적 상세
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	HashMap<String, Object> selectRepairResultDtl(RepairResultVO repairResultVO);

	/**
	 * 정비실적,고장원인분석에 등록된 정비원 목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectWorkMemberList(RepairResultVO repairResultVO);

	/**
	 * 설비 첨부이미지 목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectAttachList(RepairResultVO repairResultVO);

	/**
	 * 고장유형 목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectTroubleList1();
	
	/**
	 * 조치 목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectTroubleList2();

	/**
	 * 정비실적등록 출퇴근 현황 리스트 팝업 데이터 조회 
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectWorkerList(RepairResultVO repairResultVO);

	/**
	 * 파트 목록 (본인 소속파트 제외)
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectPartOptList(RepairResultVO repairResultVO);
	
	/**
	 * 정비실적 첨부파일 수정
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	void updateRepairResultAttach(RepairResultVO repairResultVO);

	/**
	 * 정비실적에 등록된 자재 목록
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	List<HashMap<String, Object>> selectRepairUsedMaterialList(RepairResultVO repairResultVO);
	
	/**
	 * 정비실적등록/조회 - 정비원 실적 아이템 저장
	 * 
	 * @author 김영환
	 * @since 2018.12.20
	 * @param RepairResultVO repairResultVO
	 */
	void insertWorkLogItem(RepairResultVO repairResultVO) throws Exception;

	/**
	 * 정비원 정비 실적 ITEM 조회
	 * 
	 * @author 김영환
	 * @since 2018.12.20
	 * @param RepairResultVO repairResultVO
	 */
	List<HashMap<String, Object>> selectWorkLogItemList(RepairResultVO repairResultVO) throws Exception;
	
	/**
	 * 정비원 개별 실적  - 임시저장 / 작업완료
	 * 
	 * @author 김영환
	 * @since 2018.12.20
	 * @param RepairResultVO repairResultVO
	 * @return HashMap<String, Object>
	 * @throws Exception 
	 */
	HashMap<String, Object> updateWorkLogItem(RepairResultVO repairResultVO) throws Exception;
	
	/**
	 * 정비원 개별실적 - 정비완료취소
	 * 
	 * @author 김영환
	 * @since 2018.12.20
	 * @param RepairResultVO repairResultVO
	 * @return int
	 * @throws Exception
	 *
	 */
	int updateWorkLogStatus(RepairResultVO repairResultVO);
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// SAP
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 정비실적 임시저장/정비완료로 변경
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return HashMap<String, Object>
	 * @throws Exception 
	 */
	HashMap<String, Object> updateRepairResult(RepairResultVO repairResultVO, AttachVO attachVO) throws Exception;
	
	/**
	 * 정비실적등록/조회 - 정비확정취소
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 * @throws Exception 
	 */
	HashMap<String, Object> updateCompleteStatus(RepairResultVO repairResultVO) throws Exception;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - 정비실적 건수 (메인화면 뱃지카운트)
	 *
	 * @author 서정민
	 * @since 2018.05.24
	 * @param RepairResultVO repairResultVO
	 * @return int
	 */
	int selectRepairResultCnt(RepairResultVO repairResultVO);

	/**
	 * 모바일 - 정비실적 목록조회
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	List<HashMap<String, Object>> selectRepairResultList2(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 정비실적 상세조회
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	HashMap<String, Object> selectRepairResultDtl2(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 정비원 정비 실적 ITEM 조회
	 * 
	 * @author 김영환
	 * @since 2018.12.20
	 * @param RepairResultVO repairResultVO
	 */
	List<HashMap<String, Object>> selectWorkLogItemList2(RepairResultVO repairResultVO) throws Exception;

	/**
	 * 모바일 - 본인 소속 제외한 파트정보
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	List<HashMap<String, Object>> selectPartOptList2(RepairResultVO repairResultVO);

	/**
	 * 모바일 - 공통코드(정비유형목록 조회)
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairTypeList(RepairResultVO repairResultVO);

	/**
	 * 모바일 - 공통코드(고장유형목록 조회)
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectTroubleType1List();

	/**
	 * 모바일 - 공통코드(조치목록 조회)
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectTroubleType2List();
	
	/**
	 * 모바일 - 공통코드([SAP-코딩그룹]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectCatalogDGrpList(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 공통코드([SAP-코딩]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectCatalogDList(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 공통코드([SAP-대상그룹]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectCatalogBGrpList(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 공통코드([SAP-대상]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectCatalogBList(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 공통코드([SAP-손상그룹]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectCatalogCGrpList(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 공통코드([SAP-손상]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectCatalogCList(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 공통코드([SAP-원인그룹]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectCatalog5GrpList(RepairResultVO repairResultVO);
	
	/**
	 * 모바일 - 공통코드([SAP-원인]목록 조회)
	 *
	 * @author 서정민
	 * @since 2018.10.10
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectCatalog5List(RepairResultVO repairResultVO);

	/**
	 * 모바일 - 정비원 등록 시 정비원 목록
	 *
	 * @author 김영환
	 * @param repairResultVO 
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairMemberList(RepairResultVO repairResultVO);

	/**
	 * 모바일 - 사용자재 등록 시 설비 BOM 조회
	 *
	 * @author 김영환
	 * @param repairResultVO 
	 * @since 2018.03.12
	 * @param 
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectEquipmentBomList(RepairResultVO repairResultVO);

	/**
	 * 정비원 추가/삭제
	 * 
	 * @author 서정민
	 * @since 2019.03.06
	 * @param RepairResultVO repairResultVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	HashMap<String, Object> updateWorkMember(RepairResultVO repairResultVO) throws Exception;

	

	

}
