package aspn.hello.epms.repair.result.model;

import java.util.ArrayList;
import java.util.List;

import aspn.hello.com.model.AbstractVO;

/**
 * [고장관리-고장등록] VO Class
 * @author 김영환
 * @since 2018.03.06
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.06		김영환		최초 생성
 *   
 * </pre>
 */

public class RepairResultVO extends AbstractVO{
	private static final long serialVersionUID = 1L;
	
	/** EQUIPMENT DB model  */
	private String EQUIPMENT					= ""; //설비 ID
	private String LOCATION						= ""; //회사 ID
	
	/** EQUIPMENT_BOM DB model  */
	private String EQUIPMENT_BOM				= ""; //설비BOM ID
	
	/** CATEGORY DB model*/
	private String TREE							= ""; //구조체
	
	/** MAINTENANCE_BOM DB model  */
	private String PART							= ""; //파트(01:기계, 02:전기, 03:시설)
	
	/** MAINTENANCE_RESULT DB model  */
	private String PREVENTIVE_RESULT			= ""; //예방보전실적ID
	private String STATUS						= ""; //점검상태(01:미점검, 02:정비필요, 03:완료)
	private String CHECK_DECISION				= ""; //판정(01:Y, 02:N, 03:NY)

	/** parameter model */
	private String srcType						= ""; //검색구분
	
	/** REPAIR_REQUEST DB model  */
	private String APPR_STATUS					= ""; //결재상태(01:대기, 02:승인, 03:반려)
	private String REQUEST_TYPE					= ""; //정비요청유형(01:일반, 02:예방정비, 03:타파트 업무협조)
	
	/** REPAIR_RESULT DB model  */
	private String REPAIR_RESULT				= ""; //정비 ID
	private String REPAIR_REQUEST				= ""; //요청 ID
	private String TROUBLE_TYPE1				= ""; //고장현상ID
	private String TROUBLE_TYPE2				= ""; //조치ID
	private String TROUBLE_TIME1				= ""; //고장시작시간
	private String TROUBLE_TIME2				= ""; //고장종료시간
	private String REPAIR_TIME1					= ""; //정비시작시간
	private String REPAIR_TIME2					= ""; //정비종료시간
	private String REPAIR_TYPE					= ""; //정비요청유형(110:돌발정비, 120:일상정비, 130:보전정비, 140:사후정비, 150:예방정비)
	private String REPAIR_STATUS				= ""; //정비상태(01:접수, 02:배정완료, 03:정비완료)
	private String MATERIAL_UNREG				= ""; //미등록자재
	private String REPAIR_DESCRIPTION			= ""; //정비내용
	private String DATE_REPAIR					= ""; //정비완료일
	private String REPAIR_MEMBER				= ""; //정비원ID
	private String OUTSOURCING					= ""; //외주
	private String MANAGER						= ""; //배정자
	private String DEL_YN						= ""; //사용자재 삭제 플래그(정비실적 수정시)
		
	/** MATERIAL_GRP DB model  */
	private String MATERIAL						= ""; //자재 ID
	private String MATERIAL_GRP					= ""; //장치 ID
	private String MATERIAL_UID					= ""; //자재 코드
	
	/** MATERIAL_INOUT DB model  */
	private String MATERIAL_INOUT				= ""; //입출고 ID
	private String MATERIAL_INOUT_TYPE			= ""; //입출고유형(01:입고,02:출고,03:임의입고,04:임의출고)
	private String DATE_INOUT					= ""; //입출고일
	private String SEQ_MATERIAL_INOUT_ITEM		= ""; //아이템 ID
	private String PRICE						= ""; //가격
	private String TARGET_TYPE					= ""; //실적유형(REPAIR, PREVENTIVE)
	private String TARGET_ID					= ""; //실적ID
	
	/** MATERIAL_INOUT_ITEM DB model  */
	private String QNTY							= ""; //수량
	
	/** WORK_LOG DB model  */
	private String WORK_ID						= ""; //작업대상ID
	private String WORK_TYPE					= ""; //작업유형(01:정비, 02:예방보전)
	private String USER_ID						= ""; //정비원ID
	private String SEQ_WORK_LOG					= ""; //예방보전실적시퀀스
	
	/** WORK_LOG_ITEM DB model  */
	private String WORK_LOG_ITEM				= ""; //작업이력상세 ID
	private String WORK_LOG						= ""; //작업이력ID
	private String REG_ID						= ""; //등록자
	private String UPD_ID						= ""; //수정자
	private String DATE_START					= ""; //정비 시작일시
	private String DATE_START_DATE				= ""; //정비 시작날짜
	private String DATE_START_HOUR				= ""; //정비 시작시간
	private String DATE_START_MINUTE			= ""; //정비 시작분
	private String DATE_END						= ""; //정비 종료일시
	private String DATE_END_DATE				= ""; //정비 종료날짜
	private String DATE_END_HOUR				= ""; //정비 종료시간
	private String DATE_END_MINUTE				= ""; //정비 종료분 
	private String ITEM_DESCRIPTION				= ""; //내용
	private String SEQ_DSP						= ""; //내용순서
	private String WORK_STATUS					= ""; //확정여부
	private String DESCRIPTION					= ""; //정비 내용
		
	/** ATTACH DB model  */
	private String MODULE						= ""; //모듈 아이디
	private String ATTACH_GRP_NO				= ""; //파일그룹번호
	private String DEL_SEQ     					= ""; //파일삭제
	
	/** DIVISION DB model  */
	private String DIVISION			 			= ""; //사용자 부서 코드
	
	/** OPT_COMCD_TB DB model*/
	private String REMARKS						= ""; //비고(공장파트구분 01:파트구분,02:파트통합)
	
	/** REPAIR_ANALYSIS DB model  */
	private String SEQ_REPAIR_ANALYSIS			= ""; //고장원인분석시퀀스
	
	/** EQUIPMENT_HISTORY DB model  */
	private String HISTORY_TYPE					= ""; //이력유형
	private String DETAIL_ID					= ""; //상세 ID
	
	/** parameter model */
	private String PAGE							= ""; //현재 사용중인 페이지 정보
	private String materialUsedArr				= ""; //자재목록(정비실적 수정시)
	private String qntyUsedArr					= ""; //수량목록(정비실적 수정시)
	private String equipmentBomUsedArr			= ""; //bom목록(정비실적 수정시)
	private String materialArr					= ""; //자재목록(정비실적 등록,수정시)
	private String qntyArr						= ""; //수량목록(정비실적 등록,수정시)
	private String equipmentBomArr				= ""; //bom목록(정비실적 등록,수정시)
	private String memberArr					= ""; //이미 등록된 정비원목록(정비실적 등록,수정시)
	private String authType						= ""; //조회권한
	private String workLogItemArr				= ""; //이미 등록된 정비원목록(정비실적 등록,수정시)
	
	private String [] workLogArr;
	private List<String> workIdArr				= new ArrayList<String>();
		
	private String E_RESULT					    = "";
	private String E_MESSAGE				    = "";
	private String E_AUFNR					    = "";
	private String E_RMZHL						= "";
	private String CATALOG_D_GRP				= "";
	private String CATALOG_D					= "";
	private String CATALOG_B_GRP				= "";
	private String CATALOG_B					= "";
	private String CATALOG_C_GRP				= "";
	private String CATALOG_C					= "";
	private String CATALOG_5_GRP				= "";
	private String CATALOG_5					= "";
	private String SUB_ROOT						= ""; //공장 위치
	private String COST							= ""; //미사용 자재 단가
	
	public String getEQUIPMENT() {
		return EQUIPMENT;
	}
	public void setEQUIPMENT(String eQUIPMENT) {
		EQUIPMENT = eQUIPMENT;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getEQUIPMENT_BOM() {
		return EQUIPMENT_BOM;
	}
	public void setEQUIPMENT_BOM(String eQUIPMENT_BOM) {
		EQUIPMENT_BOM = eQUIPMENT_BOM;
	}
	public String getTREE() {
		return TREE;
	}
	public void setTREE(String tREE) {
		TREE = tREE;
	}
	public String getPART() {
		return PART;
	}
	public void setPART(String pART) {
		PART = pART;
	}
	public String getPREVENTIVE_RESULT() {
		return PREVENTIVE_RESULT;
	}
	public void setPREVENTIVE_RESULT(String pREVENTIVE_RESULT) {
		PREVENTIVE_RESULT = pREVENTIVE_RESULT;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getCHECK_DECISION() {
		return CHECK_DECISION;
	}
	public void setCHECK_DECISION(String cHECK_DECISION) {
		CHECK_DECISION = cHECK_DECISION;
	}
	public String getSrcType() {
		return srcType;
	}
	public void setSrcType(String srcType) {
		this.srcType = srcType;
	}
	public String getAPPR_STATUS() {
		return APPR_STATUS;
	}
	public void setAPPR_STATUS(String aPPR_STATUS) {
		APPR_STATUS = aPPR_STATUS;
	}
	public String getREQUEST_TYPE() {
		return REQUEST_TYPE;
	}
	public void setREQUEST_TYPE(String rEQUEST_TYPE) {
		REQUEST_TYPE = rEQUEST_TYPE;
	}
	public String getREPAIR_RESULT() {
		return REPAIR_RESULT;
	}
	public void setREPAIR_RESULT(String rEPAIR_RESULT) {
		REPAIR_RESULT = rEPAIR_RESULT;
	}
	public String getREPAIR_REQUEST() {
		return REPAIR_REQUEST;
	}
	public void setREPAIR_REQUEST(String rEPAIR_REQUEST) {
		REPAIR_REQUEST = rEPAIR_REQUEST;
	}
	public String getTROUBLE_TYPE1() {
		return TROUBLE_TYPE1;
	}
	public void setTROUBLE_TYPE1(String tROUBLE_TYPE1) {
		TROUBLE_TYPE1 = tROUBLE_TYPE1;
	}
	public String getTROUBLE_TYPE2() {
		return TROUBLE_TYPE2;
	}
	public void setTROUBLE_TYPE2(String tROUBLE_TYPE2) {
		TROUBLE_TYPE2 = tROUBLE_TYPE2;
	}
	public String getTROUBLE_TIME1() {
		return TROUBLE_TIME1;
	}
	public void setTROUBLE_TIME1(String tROUBLE_TIME1) {
		TROUBLE_TIME1 = tROUBLE_TIME1;
	}
	public String getTROUBLE_TIME2() {
		return TROUBLE_TIME2;
	}
	public void setTROUBLE_TIME2(String tROUBLE_TIME2) {
		TROUBLE_TIME2 = tROUBLE_TIME2;
	}
	public String getREPAIR_TIME1() {
		return REPAIR_TIME1;
	}
	public void setREPAIR_TIME1(String rEPAIR_TIME1) {
		REPAIR_TIME1 = rEPAIR_TIME1;
	}
	public String getREPAIR_TIME2() {
		return REPAIR_TIME2;
	}
	public void setREPAIR_TIME2(String rEPAIR_TIME2) {
		REPAIR_TIME2 = rEPAIR_TIME2;
	}
	public String getREPAIR_TYPE() {
		return REPAIR_TYPE;
	}
	public void setREPAIR_TYPE(String rEPAIR_TYPE) {
		REPAIR_TYPE = rEPAIR_TYPE;
	}
	public String getREPAIR_STATUS() {
		return REPAIR_STATUS;
	}
	public void setREPAIR_STATUS(String rEPAIR_STATUS) {
		REPAIR_STATUS = rEPAIR_STATUS;
	}
	public String getMATERIAL_UNREG() {
		return MATERIAL_UNREG;
	}
	public void setMATERIAL_UNREG(String mATERIAL_UNREG) {
		MATERIAL_UNREG = mATERIAL_UNREG;
	}
	public String getREPAIR_DESCRIPTION() {
		return REPAIR_DESCRIPTION;
	}
	public void setREPAIR_DESCRIPTION(String rEPAIR_DESCRIPTION) {
		REPAIR_DESCRIPTION = rEPAIR_DESCRIPTION;
	}
	public String getDATE_REPAIR() {
		return DATE_REPAIR;
	}
	public void setDATE_REPAIR(String dATE_REPAIR) {
		DATE_REPAIR = dATE_REPAIR;
	}
	public String getREPAIR_MEMBER() {
		return REPAIR_MEMBER;
	}
	public void setREPAIR_MEMBER(String rEPAIR_MEMBER) {
		REPAIR_MEMBER = rEPAIR_MEMBER;
	}
	public String getOUTSOURCING() {
		return OUTSOURCING;
	}
	public void setOUTSOURCING(String oUTSOURCING) {
		OUTSOURCING = oUTSOURCING;
	}
	public String getMANAGER() {
		return MANAGER;
	}
	public void setMANAGER(String mANAGER) {
		MANAGER = mANAGER;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getMATERIAL() {
		return MATERIAL;
	}
	public void setMATERIAL(String mATERIAL) {
		MATERIAL = mATERIAL;
	}
	public String getMATERIAL_GRP() {
		return MATERIAL_GRP;
	}
	public void setMATERIAL_GRP(String mATERIAL_GRP) {
		MATERIAL_GRP = mATERIAL_GRP;
	}
	public String getMATERIAL_UID() {
		return MATERIAL_UID;
	}
	public void setMATERIAL_UID(String mATERIAL_UID) {
		MATERIAL_UID = mATERIAL_UID;
	}
	public String getMATERIAL_INOUT() {
		return MATERIAL_INOUT;
	}
	public void setMATERIAL_INOUT(String mATERIAL_INOUT) {
		MATERIAL_INOUT = mATERIAL_INOUT;
	}
	public String getMATERIAL_INOUT_TYPE() {
		return MATERIAL_INOUT_TYPE;
	}
	public void setMATERIAL_INOUT_TYPE(String mATERIAL_INOUT_TYPE) {
		MATERIAL_INOUT_TYPE = mATERIAL_INOUT_TYPE;
	}
	public String getDATE_INOUT() {
		return DATE_INOUT;
	}
	public void setDATE_INOUT(String dATE_INOUT) {
		DATE_INOUT = dATE_INOUT;
	}
	public String getSEQ_MATERIAL_INOUT_ITEM() {
		return SEQ_MATERIAL_INOUT_ITEM;
	}
	public void setSEQ_MATERIAL_INOUT_ITEM(String sEQ_MATERIAL_INOUT_ITEM) {
		SEQ_MATERIAL_INOUT_ITEM = sEQ_MATERIAL_INOUT_ITEM;
	}
	public String getPRICE() {
		return PRICE;
	}
	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}
	public String getTARGET_TYPE() {
		return TARGET_TYPE;
	}
	public void setTARGET_TYPE(String tARGET_TYPE) {
		TARGET_TYPE = tARGET_TYPE;
	}
	public String getTARGET_ID() {
		return TARGET_ID;
	}
	public void setTARGET_ID(String tARGET_ID) {
		TARGET_ID = tARGET_ID;
	}
	public String getQNTY() {
		return QNTY;
	}
	public void setQNTY(String qNTY) {
		QNTY = qNTY;
	}
	public String getWORK_ID() {
		return WORK_ID;
	}
	public void setWORK_ID(String wORK_ID) {
		WORK_ID = wORK_ID;
	}
	public String getWORK_TYPE() {
		return WORK_TYPE;
	}
	public void setWORK_TYPE(String wORK_TYPE) {
		WORK_TYPE = wORK_TYPE;
	}
	public String getUSER_ID() {
		return USER_ID;
	}
	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}
	public String getSEQ_WORK_LOG() {
		return SEQ_WORK_LOG;
	}
	public void setSEQ_WORK_LOG(String sEQ_WORK_LOG) {
		SEQ_WORK_LOG = sEQ_WORK_LOG;
	}
	public String getWORK_LOG_ITEM() {
		return WORK_LOG_ITEM;
	}
	public void setWORK_LOG_ITEM(String wORK_LOG_ITEM) {
		WORK_LOG_ITEM = wORK_LOG_ITEM;
	}
	public String getWORK_LOG() {
		return WORK_LOG;
	}
	public void setWORK_LOG(String wORK_LOG) {
		WORK_LOG = wORK_LOG;
	}
	public String getREG_ID() {
		return REG_ID;
	}
	public void setREG_ID(String rEG_ID) {
		REG_ID = rEG_ID;
	}
	public String getUPD_ID() {
		return UPD_ID;
	}
	public void setUPD_ID(String uPD_ID) {
		UPD_ID = uPD_ID;
	}
	public String getDATE_START() {
		return DATE_START;
	}
	public void setDATE_START(String dATE_START) {
		DATE_START = dATE_START;
	}
	public String getDATE_START_DATE() {
		return DATE_START_DATE;
	}
	public void setDATE_START_DATE(String dATE_START_DATE) {
		DATE_START_DATE = dATE_START_DATE;
	}
	public String getDATE_START_HOUR() {
		return DATE_START_HOUR;
	}
	public void setDATE_START_HOUR(String dATE_START_HOUR) {
		DATE_START_HOUR = dATE_START_HOUR;
	}
	public String getDATE_START_MINUTE() {
		return DATE_START_MINUTE;
	}
	public void setDATE_START_MINUTE(String dATE_START_MINUTE) {
		DATE_START_MINUTE = dATE_START_MINUTE;
	}
	public String getDATE_END() {
		return DATE_END;
	}
	public void setDATE_END(String dATE_END) {
		DATE_END = dATE_END;
	}
	public String getDATE_END_DATE() {
		return DATE_END_DATE;
	}
	public void setDATE_END_DATE(String dATE_END_DATE) {
		DATE_END_DATE = dATE_END_DATE;
	}
	public String getDATE_END_HOUR() {
		return DATE_END_HOUR;
	}
	public void setDATE_END_HOUR(String dATE_END_HOUR) {
		DATE_END_HOUR = dATE_END_HOUR;
	}
	public String getDATE_END_MINUTE() {
		return DATE_END_MINUTE;
	}
	public void setDATE_END_MINUTE(String dATE_END_MINUTE) {
		DATE_END_MINUTE = dATE_END_MINUTE;
	}
	public String getITEM_DESCRIPTION() {
		return ITEM_DESCRIPTION;
	}
	public void setITEM_DESCRIPTION(String iTEM_DESCRIPTION) {
		ITEM_DESCRIPTION = iTEM_DESCRIPTION;
	}
	public String getSEQ_DSP() {
		return SEQ_DSP;
	}
	public void setSEQ_DSP(String sEQ_DSP) {
		SEQ_DSP = sEQ_DSP;
	}
	public String getWORK_STATUS() {
		return WORK_STATUS;
	}
	public void setWORK_STATUS(String wORK_STATUS) {
		WORK_STATUS = wORK_STATUS;
	}
	public String getDESCRIPTION() {
		return DESCRIPTION;
	}
	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
	public String getMODULE() {
		return MODULE;
	}
	public void setMODULE(String mODULE) {
		MODULE = mODULE;
	}
	public String getATTACH_GRP_NO() {
		return ATTACH_GRP_NO;
	}
	public void setATTACH_GRP_NO(String aTTACH_GRP_NO) {
		ATTACH_GRP_NO = aTTACH_GRP_NO;
	}
	public String getDEL_SEQ() {
		return DEL_SEQ;
	}
	public void setDEL_SEQ(String dEL_SEQ) {
		DEL_SEQ = dEL_SEQ;
	}
	public String getDIVISION() {
		return DIVISION;
	}
	public void setDIVISION(String dIVISION) {
		DIVISION = dIVISION;
	}
	public String getREMARKS() {
		return REMARKS;
	}
	public void setREMARKS(String rEMARKS) {
		REMARKS = rEMARKS;
	}
	public String getSEQ_REPAIR_ANALYSIS() {
		return SEQ_REPAIR_ANALYSIS;
	}
	public void setSEQ_REPAIR_ANALYSIS(String sEQ_REPAIR_ANALYSIS) {
		SEQ_REPAIR_ANALYSIS = sEQ_REPAIR_ANALYSIS;
	}
	public String getHISTORY_TYPE() {
		return HISTORY_TYPE;
	}
	public void setHISTORY_TYPE(String hISTORY_TYPE) {
		HISTORY_TYPE = hISTORY_TYPE;
	}
	public String getDETAIL_ID() {
		return DETAIL_ID;
	}
	public void setDETAIL_ID(String dETAIL_ID) {
		DETAIL_ID = dETAIL_ID;
	}
	public String getPAGE() {
		return PAGE;
	}
	public void setPAGE(String pAGE) {
		PAGE = pAGE;
	}
	public String getMaterialUsedArr() {
		return materialUsedArr;
	}
	public void setMaterialUsedArr(String materialUsedArr) {
		this.materialUsedArr = materialUsedArr;
	}
	public String getQntyUsedArr() {
		return qntyUsedArr;
	}
	public void setQntyUsedArr(String qntyUsedArr) {
		this.qntyUsedArr = qntyUsedArr;
	}
	public String getEquipmentBomUsedArr() {
		return equipmentBomUsedArr;
	}
	public void setEquipmentBomUsedArr(String equipmentBomUsedArr) {
		this.equipmentBomUsedArr = equipmentBomUsedArr;
	}
	public String getMaterialArr() {
		return materialArr;
	}
	public void setMaterialArr(String materialArr) {
		this.materialArr = materialArr;
	}
	public String getQntyArr() {
		return qntyArr;
	}
	public void setQntyArr(String qntyArr) {
		this.qntyArr = qntyArr;
	}
	public String getEquipmentBomArr() {
		return equipmentBomArr;
	}
	public void setEquipmentBomArr(String equipmentBomArr) {
		this.equipmentBomArr = equipmentBomArr;
	}
	public String getMemberArr() {
		return memberArr;
	}
	public void setMemberArr(String memberArr) {
		this.memberArr = memberArr;
	}
	public String getAuthType() {
		return authType;
	}
	public void setAuthType(String authType) {
		this.authType = authType;
	}
	public String getWorkLogItemArr() {
		return workLogItemArr;
	}
	public void setWorkLogItemArr(String workLogItemArr) {
		this.workLogItemArr = workLogItemArr;
	}
	public String[] getWorkLogArr() {
		return workLogArr;
	}
	public void setWorkLogArr(String[] workLogArr) {
		this.workLogArr = workLogArr;
	}
	public List<String> getWorkIdArr() {
		return workIdArr;
	}
	public void setWorkIdArr(List<String> workIdArr) {
		this.workIdArr = workIdArr;
	}
	public String getE_RESULT() {
		return E_RESULT;
	}
	public void setE_RESULT(String e_RESULT) {
		E_RESULT = e_RESULT;
	}
	public String getE_MESSAGE() {
		return E_MESSAGE;
	}
	public void setE_MESSAGE(String e_MESSAGE) {
		E_MESSAGE = e_MESSAGE;
	}
	public String getE_AUFNR() {
		return E_AUFNR;
	}
	public void setE_AUFNR(String e_AUFNR) {
		E_AUFNR = e_AUFNR;
	}
	public String getE_RMZHL() {
		return E_RMZHL;
	}
	public void setE_RMZHL(String e_RMZHL) {
		E_RMZHL = e_RMZHL;
	}
	public String getCATALOG_D_GRP() {
		return CATALOG_D_GRP;
	}
	public void setCATALOG_D_GRP(String cATALOG_D_GRP) {
		CATALOG_D_GRP = cATALOG_D_GRP;
	}
	public String getCATALOG_D() {
		return CATALOG_D;
	}
	public void setCATALOG_D(String cATALOG_D) {
		CATALOG_D = cATALOG_D;
	}
	public String getCATALOG_B_GRP() {
		return CATALOG_B_GRP;
	}
	public void setCATALOG_B_GRP(String cATALOG_B_GRP) {
		CATALOG_B_GRP = cATALOG_B_GRP;
	}
	public String getCATALOG_B() {
		return CATALOG_B;
	}
	public void setCATALOG_B(String cATALOG_B) {
		CATALOG_B = cATALOG_B;
	}
	public String getCATALOG_C_GRP() {
		return CATALOG_C_GRP;
	}
	public void setCATALOG_C_GRP(String cATALOG_C_GRP) {
		CATALOG_C_GRP = cATALOG_C_GRP;
	}
	public String getCATALOG_C() {
		return CATALOG_C;
	}
	public void setCATALOG_C(String cATALOG_C) {
		CATALOG_C = cATALOG_C;
	}
	public String getCATALOG_5_GRP() {
		return CATALOG_5_GRP;
	}
	public void setCATALOG_5_GRP(String cATALOG_5_GRP) {
		CATALOG_5_GRP = cATALOG_5_GRP;
	}
	public String getCATALOG_5() {
		return CATALOG_5;
	}
	public void setCATALOG_5(String cATALOG_5) {
		CATALOG_5 = cATALOG_5;
	}
	public String getSUB_ROOT() {
		return SUB_ROOT;
	}
	public void setSUB_ROOT(String sUB_ROOT) {
		SUB_ROOT = sUB_ROOT;
	}
	public String getCOST() {
		return COST;
	}
	public void setCOST(String cOST) {
		COST = cOST;
	}
		
}
