package aspn.hello.epms.repair.result.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.JsonUtil;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.model.ComVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.com.service.ComService;
import aspn.hello.com.service.DeviceService;
import aspn.hello.epms.repair.request.model.RepairRequestVO;
import aspn.hello.epms.repair.request.service.RepairRequestService;
import aspn.hello.epms.repair.result.model.RepairResultVO;
import aspn.hello.epms.repair.result.service.RepairResultService;

/**
 * [고장관리-정비실적등록/조회] Controller Class
 * @author 김영환
 * @since 2018.03.12
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.12		김영환			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/repair/result")
public class RepairResultController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	RepairResultService repairResultService;
	
	@Autowired
	RepairRequestService repairRequestService;
	
	@Autowired
	AttachService attachService;
	
	@Autowired
	DeviceService deviceService;
	
	@Autowired
	ComService comService;

	/**
	 * 정비실적등록/조회 : 메인화면
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairResultList.do")
	public String repairResultList(Model model) throws Exception{
		model.addAttribute("startDt", DateTime.requestMmDay("1","yyyy-MM-dd"));
		model.addAttribute("endDt", DateTime.getDateString());
		
		return "/epms/repair/result/repairResultList";
	}
	
	/**
	 * 정비실적등록/조회 : 실적 현황 Grid Layout
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairResultListLayout.do")
	public String repairResultListLayout() throws Exception{
		return "/epms/repair/result/repairResultListLayout";
	}
	
	/**
	 * 정비실적등록/조회 : 실적 현황 Grid Data
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairResultVO repairResultVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairResultListData.do")
	public String repairResultListData(RepairResultVO repairResultVO, Model model) throws Exception{
		
		try {
			repairResultVO.setUserId(getSsUserId());
			repairResultVO.setSubAuth(getSsSubAuth());
			repairResultVO.setCompanyId(getSsCompanyId());
			repairResultVO.setSsPart(getSsPart());
			
			//팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				repairResultVO.setAuthType(CodeConstants.AUTH_CHIEF);
			}
			//정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
				repairResultVO.setAuthType(CodeConstants.AUTH_REPAIR);
			}
			
			//정비실적현황 목록
			List<HashMap<String, Object>> list= repairResultService.selectRepairResultList(repairResultVO);
			
			//정비실적에 등록된 정비원 조회
			List<HashMap<String, Object>> workMemberList = null;
			
			if (list != null){
				//정비실적현황 목록의 정비ID가져오기
				List<String> work_id = new ArrayList<String>();
				for (int i=0; i < list.size() ; i++ ){
					work_id.add(String.valueOf(list.get(i).get("REPAIR_RESULT")));
				}
				repairResultVO.setWorkIdArr(work_id);
				
				//정비실적에 등록된 정비원 조회
				repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //01:정비실적, 02:고장원인분석
				workMemberList = repairResultService.selectWorkMemberList(repairResultVO);
			}
			
			model.addAttribute("repairRegMember", workMemberList);
			model.addAttribute("userId", getSsUserId());
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			logger.error("RepairResultController.repairResultListData Error !");
			e.printStackTrace();
		}
		
		return "/epms/repair/result/repairResultListData";
	}
	
	/**
	 * 설비관리 정비실적 상세정보 팝업 화면
	 * 
	 * @author 김영환
	 * @since 2018.03.12 
	 * @param RepairResultVO repairResultVO
	 * @param  Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popRepairResultForm.do")
	public String popRepairResultForm(RepairResultVO repairResultVO, Model model) throws Exception{
		
		repairResultVO.setCompanyId(getSsCompanyId());
		repairResultVO.setSubAuth(getSsSubAuth());
//		repairResultVO.setSsLocation(getSsLocation());
		
		//정비실적 상세 조회
		HashMap<String, Object> repairResultDtl = repairResultService.selectRepairResultDtl(repairResultVO);
		model.addAttribute("repairResultDtl", repairResultDtl);
		
		//던킨 : 정비원 조회시 SUB_ROOT 필요
		repairResultVO.setSUB_ROOT(String.valueOf(repairResultDtl.get("LOCATION"))); 
		
		//고장사진
		if(!("".equals(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO1"))) || repairResultDtl.get("ATTACH_GRP_NO1") == null)){
			AttachVO attachVO = new AttachVO();
			attachVO.setATTACH_GRP_NO(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO1")));
			List<HashMap<String, Object>> attachList1 = attachService.selectFileList(attachVO);
			model.addAttribute("attachList1", attachList1);
		}
		
		if(!"".equals(String.valueOf(repairResultDtl.get("REPAIR_RESULT"))) && repairResultDtl.get("REPAIR_RESULT") != null){
			//정비실적에 등록된 자재 목록
			List<HashMap<String, Object>> usedMaterial = repairResultService.selectRepairUsedMaterialList(repairResultVO);
			model.addAttribute("usedMaterial", usedMaterial);
			
			//정비실적에 등록된 정비원 조회
			repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //01:정비실적, 02:고장원인분석
			repairResultVO.setWORK_ID(String.valueOf(repairResultDtl.get("REPAIR_RESULT")));
			List<HashMap<String, Object>> workMemberList = repairResultService.selectWorkMemberList(repairResultVO);
			model.addAttribute("repairRegMember", workMemberList);
			
			String WORK_LOG[] =  new String[workMemberList.size()];
			for (int i=0; i < workMemberList.size() ; i++ ){
				WORK_LOG[i] = String.valueOf(workMemberList.get(i).get("WORK_LOG"));
			}
			repairResultVO.setWorkLogArr(WORK_LOG);
			
			//정비원 정비 실적 조회
			List<HashMap<String, Object>> workLogItem = null;
			
			if(repairResultVO.getWorkLogArr().length != 0){
				workLogItem = repairResultService.selectWorkLogItemList(repairResultVO);
			}
			
			model.addAttribute("workLogItem", workLogItem);
			
			//정비사진
			if(!("".equals(repairResultDtl.get("ATTACH_GRP_NO2")) || repairResultDtl.get("ATTACH_GRP_NO2") == null)){
				AttachVO attachVO = new AttachVO();
				attachVO.setATTACH_GRP_NO(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO2")));
				List<HashMap<String, Object>> attachList2 = attachService.selectFileList(attachVO);
				model.addAttribute("attachList2", attachList2);
			}
			
			//고장유형 목록
			List<HashMap<String, Object>> troubleList1 = repairResultService.selectTroubleList1();
			model.addAttribute("troubleList1", troubleList1);
			
			//조치 목록
			List<HashMap<String, Object>> troubleList2 = repairResultService.selectTroubleList2();
			model.addAttribute("troubleList2", troubleList2);

			repairResultVO.setSubAuth(getSsSubAuth());
			repairResultVO.setREMARKS(getSsRemarks());
			repairResultVO.setLOCATION(String.valueOf(repairResultDtl.get("LOCATION")));
			repairResultVO.setPART(String.valueOf(repairResultDtl.get("PART")));
			repairResultVO.setUSER_ID(getSsUserId());
			
			List<HashMap<String, Object>> worker = repairResultService.selectWorkerList(repairResultVO);
			model.addAttribute("worker", worker);
		}
		
		//로그인한 사용자 ID
		model.addAttribute("PAGE", repairResultVO.getPAGE());
		model.addAttribute("userId", getSsUserId());
		
		//정비확정취소 권한
		if(getSsSubAuth().indexOf(CodeConstants.AUTH_FUNC03)>-1){
			model.addAttribute("cancelAuth", "Y");
		}
		else{
			model.addAttribute("cancelAuth", "N");
		}
		
		return "/epms/repair/result/popRepairResultForm";
	}
	
	/**
	 * 설비관리 정비실적 상세정보 팝업 화면
	 * 
	 * @author 김영환
	 * @since 2018.03.12 
	 * @param RepairResultVO repairResultVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popRepairResultRegForm.do")
	public String popRepairResultRegForm(RepairResultVO repairResultVO, Model model) throws Exception{

		repairResultVO.setCompanyId(getSsCompanyId());
//		repairResultVO.setSsLocation(getSsLocation());
		
		//정비실적 상세 조회
		HashMap<String, Object> repairResultDtl = repairResultService.selectRepairResultDtl(repairResultVO);
		model.addAttribute("repairResultDtl", repairResultDtl);
		
		//고장사진
		if(!("".equals(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO1"))) || repairResultDtl.get("ATTACH_GRP_NO1") == null)){
			AttachVO attachVO = new AttachVO();
			attachVO.setATTACH_GRP_NO(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO1")));
			List<HashMap<String, Object>> attachList1 = attachService.selectFileList(attachVO);
			model.addAttribute("attachList1", attachList1);
		}
		
		if(!"".equals(String.valueOf(repairResultDtl.get("REPAIR_RESULT"))) && repairResultDtl.get("REPAIR_RESULT") != null){
			//정비실적에 등록된 자재 목록
			List<HashMap<String, Object>> usedMaterial = repairResultService.selectRepairUsedMaterialList(repairResultVO);
			model.addAttribute("usedMaterial", usedMaterial);
			
			//정비실적에 등록된 정비원 조회
			repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //01:정비실적, 02:고장원인분석
			repairResultVO.setWORK_ID(repairResultVO.getREPAIR_RESULT());
			List<HashMap<String, Object>> workMemberList = repairResultService.selectWorkMemberList(repairResultVO);
			model.addAttribute("repairRegMember", workMemberList);
			
			String WORK_LOG[] =  new String[workMemberList.size()];
			for (int i=0; i < workMemberList.size() ; i++ ){
				WORK_LOG[i] = String.valueOf(workMemberList.get(i).get("WORK_LOG"));
			}
			
			repairResultVO.setWorkLogArr(WORK_LOG);
			
			//정비원 정비 실적 조회                                                                                                    
			List<HashMap<String, Object>> workLogItem = repairResultService.selectWorkLogItemList(repairResultVO);
			model.addAttribute("workLogItem", workLogItem);
			
			//정비사진
			if(!("".equals(repairResultDtl.get("ATTACH_GRP_NO2")) || repairResultDtl.get("ATTACH_GRP_NO2") == null)){
				AttachVO attachVO = new AttachVO();
				attachVO.setATTACH_GRP_NO(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO2")));
				List<HashMap<String, Object>> attachList2 = attachService.selectFileList(attachVO);
				model.addAttribute("attachList2", attachList2);
			}
			
			//정비유형 목록
			repairResultVO.setREQUEST_TYPE(String.valueOf(repairResultDtl.get("REQUEST_TYPE")));
			List<HashMap<String, Object>> repair_type_list = repairResultService.selectRepairTypeList(repairResultVO);
			model.addAttribute("repairTypeList", repair_type_list);
			
			//고장유형 목록
			List<HashMap<String, Object>> troubleList1 = repairResultService.selectTroubleList1();
			model.addAttribute("troubleList1", troubleList1);
			
			//조치 목록
			List<HashMap<String, Object>> troubleList2 = repairResultService.selectTroubleList2();
			model.addAttribute("troubleList2", troubleList2);
		}
		
		model.addAttribute("companyId", getSsCompanyId());
		model.addAttribute("userId", getSsUserId());
		
		return "/epms/repair/result/popRepairResultRegForm";
	}
	
	/**
	 * 출퇴근  현황 리스트 팝업(정비실적등록 정비원선택시)
	 * 
	 * @author 김영환
	 * @since 2018.03.13
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popWorkerList.do")
	public String popWorkerList() throws Exception{
		return "/epms/repair/result/popWorkerList";
	}
	
	/**
	 * 출퇴근  현황 리스트 팝업 레이아웃(정비실적등록 정비원선택시)
	 * 
	 * @author 김영환
	 * @since 2016.10.31
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popWorkerListLayout.do")
	public String popWorkerListLayout()throws Exception{
		return "/epms/repair/result/popWorkerListLayout";
	}
	
	/**
	 * 정비실적등록 출퇴근 현황 리스트
	 * 
	 * @author 김영환
	 * @since 2016.10.31
	 * @param RepairResultVO repairResultVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popWorkerListData.do")
	public String popWorkerListData(RepairResultVO repairResultVO, Model model) {
		try{
			repairResultVO.setCompanyId(getSsCompanyId());
//			repairResultVO.setSsLocation(getSsLocation());
			repairResultVO.setSubAuth(getSsSubAuth());
			List<HashMap<String, Object>> list = repairResultService.selectWorkerList(repairResultVO);
			model.addAttribute("list", list);
		} catch(Exception e){
			logger.error("RepairResultController.popWorkerListData Error !");
			e.printStackTrace();
		}

		return "/epms/repair/result/popWorkerListData";
	}
	
	/**
	 * 정비실적등록/조회 - 정비실적 상세 : 자재선택팝업 화면
	 * 
	 * @author 김영환 
	 * @since 2018.03.13
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popRepairMaterialList.do")
	public String popRepairMaterialList() throws Exception{
		return "/epms/repair/result/popRepairMaterialList";
	}
	
	/**
	 * 정비실적등록/조회 - 정비실적 상세 : 자재선택팝업 화면 Grid Layout
	 * 
	 * @author 김영환
	 * @since 2018.03.13
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popRepairMaterialListLayout.do")
	public String popRepairMaterialListLayout() throws Exception{
		return "/epms/repair/result/popRepairMaterialListLayout";
	}
	
	/**
	 * 정비실적등록/조회 - 타파트요청 팝업 화면
	 * @author 김영환
	 * @since 2018.03.14
	 * @param RepairResultVO repairResultVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popRequestPartForm.do")
	public String popRequestPartForm(RepairResultVO repairResultVO, Model model) throws Exception{
		try{
			
			repairResultVO.setCompanyId(getSsCompanyId());
			
			//파트 정보
			List<HashMap<String, Object>> partList = repairResultService.selectPartOptList(repairResultVO);
			model.addAttribute("partList", partList);
		}catch(Exception e){
			logger.error("RepairResultController.popRequestPartForm Error !");
			e.printStackTrace();
		}
		
		return "/epms/repair/result/popRequestPartForm";
	}
	
	/**
	 * 정비실적등록/조회 - 타파트요청
	 * 
	 * @author 김영환
	 * @since 2018.03.14
	 * @param RepairResultVO repairResultVO
	 * @param Model model
	 * @return int
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairRequestAjax.do")
	@ResponseBody
	public HashMap<String, Object> repairRequestAjax(RepairRequestVO repairRequestVO, Model model) throws Exception {

		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		try {
			
			repairRequestVO.setCompanyId(getSsCompanyId());
//			repairRequestVO.setSsLocation(getSsLocation());
			repairRequestVO.setUserId(getSsUserId());
			repairRequestVO.setDIVISION(getSsDivision());
			
			// 정비요청
			resultMap = repairRequestService.insertRepairRequest(repairRequestVO);
			
			if(resultMap != null){
				// Push 정보
				HashMap<String, Object> pushParam = resultMap;
				String pushTitle = "";
				// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REQUEST_CANCEL-요청취소
				pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_APPR);
				// Push Title
				pushTitle = pushParam.get("REQUEST_MEMBER_NAME").toString() + "님이 정비를 요청하였습니다.(타파트 요청)";
				pushParam.put("PUSHTITLE", pushTitle);
				// 상세화면 IDX 세팅
				pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
				// 알림음 유형 세팅(01:일반, 02:긴급)
				pushParam.put("PUSHALARM", pushParam.get("URGENCY").toString());
				
				deviceService.sendPush(pushParam);
			}
			
			
		}catch (Exception e) {
			logger.error("RepairResultController.repairRequestAjax Error !");
			e.printStackTrace();
		}
		
		return resultMap;
	}
	
	/**
	 * 정비실적등록/조회 - 실적 임시저장,정비완료 
	 * 
	 * @author 김영환
	 * @since 2018.03.15
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @param RepairResultVO repairResultVO
	 * @param Model model
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairResultRegAjax.do")
	@ResponseBody
	public HashMap<String, Object> repairResultRegAjax(HttpServletRequest request, MultipartHttpServletRequest mRequest, HttpServletResponse response, RepairResultVO repairResultVO, Model model) {
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();	
		
		try {
			repairResultVO.setCompanyId(getSsCompanyId());
//			repairResultVO.setSsLocation(getSsLocation());
			repairResultVO.setUserId(getSsUserId());
			
			AttachVO attachVO = new AttachVO();
			attachVO.setDEL_SEQ(repairResultVO.getDEL_SEQ());
			attachVO.setMODULE(repairResultVO.getMODULE());
			
			if("".equals(repairResultVO.getATTACH_GRP_NO()) || repairResultVO.getATTACH_GRP_NO() == null){
				if("Y".equals(repairResultVO.getAttachExistYn())){
					String attachGrpNo = attachService.selectAttachGrpNo();
					attachVO.setATTACH_GRP_NO(attachGrpNo);
					repairResultVO.setATTACH_GRP_NO(attachGrpNo);
					attachService.AttachEdit(request, mRequest, attachVO); 
				}
			}
			else{
				attachVO.setATTACH_GRP_NO(repairResultVO.getATTACH_GRP_NO());
 				attachService.AttachEdit(request, mRequest, attachVO); 
			}
			
			rtnMap = repairResultService.updateRepairResult(repairResultVO, attachVO);
			
			String updateYn = String.valueOf(rtnMap.get("updateYn"));
			
			@SuppressWarnings("unchecked")
			HashMap<String, Object> resultDtl = (HashMap<String, Object>) rtnMap.get("resultDtl");
			
			if("SUCCESS".equals(updateYn) && "S".equals(String.valueOf(resultDtl.get("E_RESULT"))) ) {
				//정비완료일 경우
				if(CodeConstants.EPMS_REPAIR_STATUS_COMPLETE.equals(String.valueOf(resultDtl.get("REPAIR_STATUS")))){
					
					// 1-1. 요청자에게 푸시 전송
					// Push 정보
					HashMap<String, Object> pushParam = resultDtl;
					String pushTitle = "";
					
					// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
					pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REQUEST_COMPLETE);
					// Push Title
					pushTitle = pushParam.get("MANAGER_NAME").toString() + "님이 정비를 완료하였습니다.";
					pushParam.put("PUSHTITLE", pushTitle);
					// 상세화면 IDX 세팅
					pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
					// 알림음 유형 세팅(01:일반, 02:긴급)
					pushParam.put("PUSHALARM", "01");
					
					deviceService.sendPush(pushParam);
					
					pushParam = new HashMap<String, Object>();
					pushParam = resultDtl;
					
					// 1-2. 정비 담당자에게 담당자에게 푸시 전송
					// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REPAIR_COMPLETE-정비상세(완료), REQUEST_CANCEL-요청취소
					pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_REPAIR_COMPLETE);
					// Push Title
					pushTitle = pushParam.get("MANAGER_NAME").toString() + "님이 정비를 완료하였습니다.";
					pushParam.put("PUSHTITLE", pushTitle);
					// 상세화면 IDX 세팅
					pushParam.put("PUSHMENUDETAILNDX", pushParam.get("REPAIR_REQUEST").toString());
					// 알림음 유형 세팅(01:일반, 02:긴급)
					pushParam.put("PUSHALARM", "01");
					
					deviceService.sendPush(pushParam);
					
				}
			}
		}catch (Exception e) {
			logger.error("RepairResultController.repairResultRegAjax Error !");
			e.printStackTrace();
		}
		
		return rtnMap;
	}
	
	/**
	 * 정비실적등록/조회 - 정비확정취소 : 정비완료 건 승인으로 상태값 변경 
	 * @author 김영환
	 * @since 2018.03.14
	 * @param RepairResultVO repairResultVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateCompleteStatus.do")
	@ResponseBody
	public HashMap<String, Object> updateCompleteStatus(RepairResultVO repairResultVO, Model model) throws Exception{
		
		HashMap<String, Object> rtnMap = new HashMap<String,Object>();
		
		try{
			repairResultVO.setCompanyId(getSsCompanyId());
			repairResultVO.setSsLocation(getSsLocation());
			repairResultVO.setUserId(getSsUserId());
			
			rtnMap = repairResultService.updateCompleteStatus(repairResultVO);
			//rtnMap.put("RES_CD", CodeConstants.SUCCESS);
			
		}catch (Exception e) {
			rtnMap.put("RES_CD", CodeConstants.ERROR_ETC);
			logger.error("RepairResultController.updateCompleteStatus > " + e.toString());
			e.printStackTrace();
		}
		
		return rtnMap;
	}
	
	/**
	 * 정비원 개별실적 - 임시저장 or 작업완료
	 * 
	 * @author 김영환
	 * @since 2018.03.15
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @param RepairResultVO repairResultVO
	 * @param Model model
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/workLogRegAjax.do")
	@ResponseBody
	public HashMap<String, Object> workLogRegAjax(HttpServletRequest request, MultipartHttpServletRequest mRequest, HttpServletResponse response, RepairResultVO repairResultVO, Model model) {
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();	
		
		try {
			repairResultVO.setCompanyId(getSsCompanyId());
			repairResultVO.setUserId(getSsUserId());
			repairResultVO.setREG_ID(getSsUserId());
			repairResultVO.setUSER_ID(getSsUserId());
			
			rtnMap = repairResultService.updateWorkLogItem(repairResultVO);
			
			//정비실적에 등록된 정비원 조회
			String WORK_LOG[] =  new String[1];
			WORK_LOG[0] = String.valueOf(rtnMap.get("WORK_LOG"));
			repairResultVO.setWorkLogArr(WORK_LOG);
			repairResultVO.setDEL_YN("N");
			
			//정비원 정비 실적 조회                                                                                                    
			List<HashMap<String, Object>> workLogItem = repairResultService.selectWorkLogItemList(repairResultVO);
			rtnMap.put("workLogItem", workLogItem);
			
		}catch (Exception e) {
			logger.error("RepairResultController.workLogRegAjax Error !");
			e.printStackTrace();
		}
		
		return rtnMap;
	}
	
	/**
	 * 정비원 개별실적 - 정비완료 취소 
	 * 
	 * @author 김영환
	 * @since 2018.03.15
	 * @param RepairResultVO repairResultVO
	 * @param Model model
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/workLogCancelAjax.do")
	@ResponseBody
	public HashMap<String, Object> workLogCancelAjax(RepairResultVO repairResultVO) {
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		try {
			//작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)
			repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR);
			repairResultService.updateWorkLogStatus(repairResultVO);
			
			//정비실적에 등록된 정비원 조회
			String WORK_LOG[] =  new String[1];
			WORK_LOG[0] = repairResultVO.getWORK_LOG();
			repairResultVO.setWorkLogArr(WORK_LOG);
			repairResultVO.setDEL_YN("N");
			
			//정비원 정비 실적 조회                                                                                                    
			List<HashMap<String, Object>> workLogItem = repairResultService.selectWorkLogItemList(repairResultVO);
			rtnMap.put("workLogItem", workLogItem);
			
		}catch (Exception e) {
			logger.error("RepairResultController.workLogCancelAjax Error !");
			e.printStackTrace();
		}
		return rtnMap;
	}
		
	/**
	 * 메인화면 내 요청현황 실적별 상세 화면
	 * 
	 * @author 김영환
	 * @since 2018.03.12 
	 * @param RepairResultVO repairResultVO
	 * @param  Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairResultDtl.do")
	public String repairResultDtl(RepairResultVO repairResultVO, Model model) throws Exception{
		
		if(CodeConstants.EPMS_APPR_STATUS_WAIT.equals(repairResultVO.getAPPR_STATUS()) || CodeConstants.EPMS_APPR_STATUS_CANCEL_REQUEST.equals(repairResultVO.getAPPR_STATUS())
				|| CodeConstants.EPMS_APPR_STATUS_REJECT.equals(repairResultVO.getAPPR_STATUS())){
			
			RepairRequestVO repairRequestVO = new RepairRequestVO();
			repairRequestVO.setCompanyId(getSsCompanyId());
//			repairRequestVO.setSsLocation(getSsLocation());
			repairRequestVO.setREPAIR_REQUEST(repairResultVO.getREPAIR_REQUEST());
			
			HashMap<String, Object> repairResultDtl = repairRequestService.selectRepairRequestDtl(repairRequestVO);
			model.addAttribute("repairResultDtl", repairResultDtl);
			
			//고장사진
			if(!("".equals(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO1"))) || repairResultDtl.get("ATTACH_GRP_NO1") == null)){
				AttachVO attachVO = new AttachVO();
				attachVO.setATTACH_GRP_NO(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO1")));
				List<HashMap<String, Object>> attachList1 = attachService.selectFileList(attachVO);
				model.addAttribute("attachList1", attachList1);
			}
			
		} else {
			repairResultVO.setCompanyId(getSsCompanyId());
//			repairResultVO.setSsLocation(getSsLocation());
			//정비실적 상세 조회
			HashMap<String, Object> repairResultDtl = repairResultService.selectRepairResultDtl(repairResultVO);
			model.addAttribute("repairResultDtl", repairResultDtl);
			repairResultVO.setREPAIR_RESULT(String.valueOf(repairResultDtl.get("REPAIR_RESULT")));
			
			//정비실적에 등록된 자재 목록
			List<HashMap<String, Object>> usedMaterial = repairResultService.selectRepairUsedMaterialList(repairResultVO);
			model.addAttribute("usedMaterial", usedMaterial);
			
			//정비실적에 등록된 정비원 조회
			repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //01:정비실적, 02:고장원인분석
			repairResultVO.setWORK_ID(repairResultVO.getREPAIR_RESULT());
			List<HashMap<String, Object>> workMemberList = repairResultService.selectWorkMemberList(repairResultVO);
			model.addAttribute("repairRegMember", workMemberList);
			
			//고장사진
			if(!("".equals(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO1"))) || repairResultDtl.get("ATTACH_GRP_NO1") == null)){
				AttachVO attachVO = new AttachVO();
				attachVO.setATTACH_GRP_NO(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO1")));
				List<HashMap<String, Object>> attachList1 = attachService.selectFileList(attachVO);
				model.addAttribute("attachList1", attachList1);
			}
			
			//정비사진
			if(!("".equals(repairResultDtl.get("ATTACH_GRP_NO2")) || repairResultDtl.get("ATTACH_GRP_NO2") == null)){
				AttachVO attachVO = new AttachVO();
				attachVO.setATTACH_GRP_NO(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO2")));
				List<HashMap<String, Object>> attachList2 = attachService.selectFileList(attachVO);
				model.addAttribute("attachList2", attachList2);
			}
			
			//고장유형 목록
			List<HashMap<String, Object>> troubleList1 = repairResultService.selectTroubleList1();
			model.addAttribute("troubleList1", troubleList1);
			
			//조치 목록
			List<HashMap<String, Object>> troubleList2 = repairResultService.selectTroubleList2();
			model.addAttribute("troubleList2", troubleList2);
			
			//로그인한 사용자 ID
			model.addAttribute("userId", getSsUserId());
		}
		
		return "/epms/repair/result/repairResultDtl";
	}
	
	/**
	 * 공통코드 호출
	 * @author 김영환
	 * @since 2018.06.14
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectComCodeList.do")
	@ResponseBody
	public Map<String, Object> selectComCodeList(ComVO comVO, HttpServletRequest request) throws Exception {
		Map<String, Object> resultMap = new HashMap<>();
		try {
			comVO.setCOMPANY_ID(getSsCompanyId());
			List<HashMap<String, Object>> comCodeList = comService.selectComCodeList(comVO);
			resultMap.put("comCodeList", comCodeList);
			
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
}
