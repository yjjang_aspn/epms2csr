package aspn.hello.epms.repair.analysis.model;

import aspn.hello.com.model.AbstractVO;

/**
 * [고장관리-고장원인분석] VO Class
 * @author 김영환
 * @since 2018.04.24
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.24		김영환		최초 생성
 *   
 * </pre>
 */

public class RepairAnalysisVO extends AbstractVO{
	private static final long serialVersionUID = 1L;
	
	private String SEQ_REPAIR_ANALYSIS			= ""; //고장원인분석 시퀀스
	private String SEQ_REPAIR_ANALYSIS_ITEM		= ""; //고장원인분석 상세 시퀀스
	
	/** EQUIPMENT DB model  */
	private String EQUIPMENT					= ""; //설비 ID
	private String LOCATION						= ""; //회사 ID
	
	/** EQUIPMENT_BOM DB model  */
	private String EQUIPMENT_BOM				= ""; //설비BOM ID
	
	/** CATEGORY DB model*/
	private String TREE							= ""; //구조체
	
	/** MAINTENANCE_BOM DB model  */
	private String PART							= ""; //파트(01:기계, 02:전기, 03:시설)

	/** parameter model */
	private String srcType						= ""; //검색구분
	
	/** REPAIR_RESULT DB model  */
	private String REPAIR_RESULT				= ""; //정비 ID
	private String TROUBLE_TYPE1				= ""; //고장현상ID
	private String TROUBLE_TYPE2				= ""; //조치ID
	private String TROUBLE_TIME1				= ""; //고장시작시간
	private String TROUBLE_TIME2				= ""; //고장종료시간
	private String REPAIR_TYPE					= ""; //정비요청유형(110:돌발정비, 120:일상정비, 130:보전정비, 140:사후정비, 150:예방정비)
	private String REPAIR_STATUS				= ""; //정비상태(01:접수, 02:배정완료, 03:정비완료)
	private String MATERIAL_UNREG				= ""; //미등록자재
	private String REPAIR_DESCRIPTION			= ""; //정비내용
	private String DATE_REPAIR					= ""; //정비완료일
	private String REPAIR_MEMBER				= ""; //정비원ID
	private String OUTSOURCING					= ""; //외주
	private String MANAGER						= ""; //배정자
	
	private String DEL_YN                       = "";
	private String REG_ID                       = "";
	private String REG_DT                       = "";
	private String UPD_DT                    	= ""; 
	private String UPD_ID                    	= "";
	private String flag		           	    	= ""; // 임시저장 불러온 후 저장완료인지 체크
	private String FILE_LIST_NAME				= "";
	
	/** REPAIR_ANALYSIS DB model */
	private String REPAIR_ANALYSIS				= ""; //고장원인분석ID
	private String TITLE                        = ""; //제목
	private String DATE_ANALYSIS                = ""; //분석일
	private String DESCRIPTION                  = ""; //피해현황
	private String SAVE_YN                      = ""; //저장여부
	private String ITEM_CNT                    	= ""; //아이템 수
	private String USER_NM                    	= ""; //유저이름

	/** REPAIR_ANALYSIS_DTL DB model */
	private String REPAIR_ANALYSIS_ITEM			= ""; //고장원인분석아이템ID
	private String SEQ_DSP                      = ""; //출력순서
	private String DESCRIPTION1                 = ""; //원인
	private String DESCRIPTION2                 = ""; //대책
		
	/** MATERIAL_GRP DB model  */
	private String MATERIAL						= ""; //자재 ID
	private String MATERIAL_GRP					= ""; //장치 ID
	
	/** MATERIAL_INOUT DB model  */
	private String MATERIAL_INOUT				= ""; //입출고 ID
	private String MATERIAL_INOUT_TYPE			= ""; //입출고유형(01:입고,02:출고,03:임의입고,04:임의출고)
	private String DATE_INOUT					= ""; //입출고일
	
	/** MATERIAL_INOUT_ITEM DB model  */
	private String QNTY							= ""; //수량
	
	/** WORK_LOG DB model  */
	private String WORK_ID						= ""; //작업대상ID
	private String WORK_TYPE					= ""; //작업유형(01:정비, 02:예방보전)
	private String USER_ID						= ""; //정비원ID
	
	/** ATTACH DB model  */
	private String MODULE						= ""; //모듈 아이디
	private String ATTACH_GRP_NO				= ""; //파일그룹번호
	private String DEL_SEQ     					= ""; //파일삭제
	private String FILE_CNT						= ""; //파일 카운트
	private String attachExistYn 				= ""; // 파일등록여부
	/** DIVISION DB model  */
	private String DIVISION			 			= ""; //사용자 부서 코드
	
	/** OPT_COMCD_TB DB model*/
	private String REMARKS						= ""; //비고(공장파트구분 01:파트구분,02:파트통합)
	
	/** parameter model */
	private String materialUsedArr				= ""; //자재목록(정비실적 수정시)
	private String qntyUsedArr					= ""; //수량목록(정비실적 수정시)
	private String equipmentBomUsedArr			= ""; //bom목록(정비실적 수정시)
	private String materialArr					= ""; //자재목록(정비실적 등록,수정시)
	private String qntyArr						= ""; //수량목록(정비실적 등록,수정시)
	private String equipmentBomArr				= ""; //bom목록(정비실적 등록,수정시)
	private String memberArr					= ""; //정비원목록(정비실적 등록,수정시)
	private String delYnArr						= ""; //삭제여부
	
	public String getSEQ_REPAIR_ANALYSIS() {
		return SEQ_REPAIR_ANALYSIS;
	}
	public void setSEQ_REPAIR_ANALYSIS(String sEQ_REPAIR_ANALYSIS) {
		SEQ_REPAIR_ANALYSIS = sEQ_REPAIR_ANALYSIS;
	}
	public String getSEQ_REPAIR_ANALYSIS_ITEM() {
		return SEQ_REPAIR_ANALYSIS_ITEM;
	}
	public void setSEQ_REPAIR_ANALYSIS_ITEM(String sEQ_REPAIR_ANALYSIS_ITEM) {
		SEQ_REPAIR_ANALYSIS_ITEM = sEQ_REPAIR_ANALYSIS_ITEM;
	}
	public String getEQUIPMENT() {
		return EQUIPMENT;
	}
	public void setEQUIPMENT(String eQUIPMENT) {
		EQUIPMENT = eQUIPMENT;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getEQUIPMENT_BOM() {
		return EQUIPMENT_BOM;
	}
	public void setEQUIPMENT_BOM(String eQUIPMENT_BOM) {
		EQUIPMENT_BOM = eQUIPMENT_BOM;
	}
	public String getTREE() {
		return TREE;
	}
	public void setTREE(String tREE) {
		TREE = tREE;
	}
	public String getPART() {
		return PART;
	}
	public void setPART(String pART) {
		PART = pART;
	}
	public String getSrcType() {
		return srcType;
	}
	public void setSrcType(String srcType) {
		this.srcType = srcType;
	}
	public String getREPAIR_RESULT() {
		return REPAIR_RESULT;
	}
	public void setREPAIR_RESULT(String rEPAIR_RESULT) {
		REPAIR_RESULT = rEPAIR_RESULT;
	}
	public String getTROUBLE_TYPE1() {
		return TROUBLE_TYPE1;
	}
	public void setTROUBLE_TYPE1(String tROUBLE_TYPE1) {
		TROUBLE_TYPE1 = tROUBLE_TYPE1;
	}
	public String getTROUBLE_TYPE2() {
		return TROUBLE_TYPE2;
	}
	public void setTROUBLE_TYPE2(String tROUBLE_TYPE2) {
		TROUBLE_TYPE2 = tROUBLE_TYPE2;
	}
	public String getTROUBLE_TIME1() {
		return TROUBLE_TIME1;
	}
	public void setTROUBLE_TIME1(String tROUBLE_TIME1) {
		TROUBLE_TIME1 = tROUBLE_TIME1;
	}
	public String getTROUBLE_TIME2() {
		return TROUBLE_TIME2;
	}
	public void setTROUBLE_TIME2(String tROUBLE_TIME2) {
		TROUBLE_TIME2 = tROUBLE_TIME2;
	}
	public String getREPAIR_TYPE() {
		return REPAIR_TYPE;
	}
	public void setREPAIR_TYPE(String rEPAIR_TYPE) {
		REPAIR_TYPE = rEPAIR_TYPE;
	}
	public String getREPAIR_STATUS() {
		return REPAIR_STATUS;
	}
	public void setREPAIR_STATUS(String rEPAIR_STATUS) {
		REPAIR_STATUS = rEPAIR_STATUS;
	}
	public String getMATERIAL_UNREG() {
		return MATERIAL_UNREG;
	}
	public void setMATERIAL_UNREG(String mATERIAL_UNREG) {
		MATERIAL_UNREG = mATERIAL_UNREG;
	}
	public String getREPAIR_DESCRIPTION() {
		return REPAIR_DESCRIPTION;
	}
	public void setREPAIR_DESCRIPTION(String rEPAIR_DESCRIPTION) {
		REPAIR_DESCRIPTION = rEPAIR_DESCRIPTION;
	}
	public String getDATE_REPAIR() {
		return DATE_REPAIR;
	}
	public void setDATE_REPAIR(String dATE_REPAIR) {
		DATE_REPAIR = dATE_REPAIR;
	}
	public String getREPAIR_MEMBER() {
		return REPAIR_MEMBER;
	}
	public void setREPAIR_MEMBER(String rEPAIR_MEMBER) {
		REPAIR_MEMBER = rEPAIR_MEMBER;
	}
	public String getOUTSOURCING() {
		return OUTSOURCING;
	}
	public void setOUTSOURCING(String oUTSOURCING) {
		OUTSOURCING = oUTSOURCING;
	}
	public String getMANAGER() {
		return MANAGER;
	}
	public void setMANAGER(String mANAGER) {
		MANAGER = mANAGER;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getREG_ID() {
		return REG_ID;
	}
	public void setREG_ID(String rEG_ID) {
		REG_ID = rEG_ID;
	}
	public String getREG_DT() {
		return REG_DT;
	}
	public void setREG_DT(String rEG_DT) {
		REG_DT = rEG_DT;
	}
	public String getUPD_DT() {
		return UPD_DT;
	}
	public void setUPD_DT(String uPD_DT) {
		UPD_DT = uPD_DT;
	}
	public String getUPD_ID() {
		return UPD_ID;
	}
	public void setUPD_ID(String uPD_ID) {
		UPD_ID = uPD_ID;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getFILE_LIST_NAME() {
		return FILE_LIST_NAME;
	}
	public void setFILE_LIST_NAME(String fILE_LIST_NAME) {
		FILE_LIST_NAME = fILE_LIST_NAME;
	}
	public String getREPAIR_ANALYSIS() {
		return REPAIR_ANALYSIS;
	}
	public void setREPAIR_ANALYSIS(String rEPAIR_ANALYSIS) {
		REPAIR_ANALYSIS = rEPAIR_ANALYSIS;
	}
	public String getTITLE() {
		return TITLE;
	}
	public void setTITLE(String tITLE) {
		TITLE = tITLE;
	}
	public String getDATE_ANALYSIS() {
		return DATE_ANALYSIS;
	}
	public void setDATE_ANALYSIS(String dATE_ANALYSIS) {
		DATE_ANALYSIS = dATE_ANALYSIS;
	}
	public String getDESCRIPTION() {
		return DESCRIPTION;
	}
	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
	public String getSAVE_YN() {
		return SAVE_YN;
	}
	public void setSAVE_YN(String sAVE_YN) {
		SAVE_YN = sAVE_YN;
	}
	public String getITEM_CNT() {
		return ITEM_CNT;
	}
	public void setITEM_CNT(String iTEM_CNT) {
		ITEM_CNT = iTEM_CNT;
	}
	public String getUSER_NM() {
		return USER_NM;
	}
	public void setUSER_NM(String uSER_NM) {
		USER_NM = uSER_NM;
	}
	public String getREPAIR_ANALYSIS_ITEM() {
		return REPAIR_ANALYSIS_ITEM;
	}
	public void setREPAIR_ANALYSIS_ITEM(String rEPAIR_ANALYSIS_ITEM) {
		REPAIR_ANALYSIS_ITEM = rEPAIR_ANALYSIS_ITEM;
	}
	public String getSEQ_DSP() {
		return SEQ_DSP;
	}
	public void setSEQ_DSP(String sEQ_DSP) {
		SEQ_DSP = sEQ_DSP;
	}
	public String getDESCRIPTION1() {
		return DESCRIPTION1;
	}
	public void setDESCRIPTION1(String dESCRIPTION1) {
		DESCRIPTION1 = dESCRIPTION1;
	}
	public String getDESCRIPTION2() {
		return DESCRIPTION2;
	}
	public void setDESCRIPTION2(String dESCRIPTION2) {
		DESCRIPTION2 = dESCRIPTION2;
	}
	public String getMATERIAL() {
		return MATERIAL;
	}
	public void setMATERIAL(String mATERIAL) {
		MATERIAL = mATERIAL;
	}
	public String getMATERIAL_GRP() {
		return MATERIAL_GRP;
	}
	public void setMATERIAL_GRP(String mATERIAL_GRP) {
		MATERIAL_GRP = mATERIAL_GRP;
	}
	public String getMATERIAL_INOUT() {
		return MATERIAL_INOUT;
	}
	public void setMATERIAL_INOUT(String mATERIAL_INOUT) {
		MATERIAL_INOUT = mATERIAL_INOUT;
	}
	public String getMATERIAL_INOUT_TYPE() {
		return MATERIAL_INOUT_TYPE;
	}
	public void setMATERIAL_INOUT_TYPE(String mATERIAL_INOUT_TYPE) {
		MATERIAL_INOUT_TYPE = mATERIAL_INOUT_TYPE;
	}
	public String getDATE_INOUT() {
		return DATE_INOUT;
	}
	public void setDATE_INOUT(String dATE_INOUT) {
		DATE_INOUT = dATE_INOUT;
	}
	public String getQNTY() {
		return QNTY;
	}
	public void setQNTY(String qNTY) {
		QNTY = qNTY;
	}
	public String getWORK_ID() {
		return WORK_ID;
	}
	public void setWORK_ID(String wORK_ID) {
		WORK_ID = wORK_ID;
	}
	public String getWORK_TYPE() {
		return WORK_TYPE;
	}
	public void setWORK_TYPE(String wORK_TYPE) {
		WORK_TYPE = wORK_TYPE;
	}
	public String getUSER_ID() {
		return USER_ID;
	}
	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}
	public String getMODULE() {
		return MODULE;
	}
	public void setMODULE(String mODULE) {
		MODULE = mODULE;
	}
	public String getATTACH_GRP_NO() {
		return ATTACH_GRP_NO;
	}
	public void setATTACH_GRP_NO(String aTTACH_GRP_NO) {
		ATTACH_GRP_NO = aTTACH_GRP_NO;
	}
	public String getDEL_SEQ() {
		return DEL_SEQ;
	}
	public void setDEL_SEQ(String dEL_SEQ) {
		DEL_SEQ = dEL_SEQ;
	}
	public String getFILE_CNT() {
		return FILE_CNT;
	}
	public void setFILE_CNT(String fILE_CNT) {
		FILE_CNT = fILE_CNT;
	}
	public String getAttachExistYn() {
		return attachExistYn;
	}
	public void setAttachExistYn(String attachExistYn) {
		this.attachExistYn = attachExistYn;
	}
	public String getDIVISION() {
		return DIVISION;
	}
	public void setDIVISION(String dIVISION) {
		DIVISION = dIVISION;
	}
	public String getREMARKS() {
		return REMARKS;
	}
	public void setREMARKS(String rEMARKS) {
		REMARKS = rEMARKS;
	}
	public String getMaterialUsedArr() {
		return materialUsedArr;
	}
	public void setMaterialUsedArr(String materialUsedArr) {
		this.materialUsedArr = materialUsedArr;
	}
	public String getQntyUsedArr() {
		return qntyUsedArr;
	}
	public void setQntyUsedArr(String qntyUsedArr) {
		this.qntyUsedArr = qntyUsedArr;
	}
	public String getEquipmentBomUsedArr() {
		return equipmentBomUsedArr;
	}
	public void setEquipmentBomUsedArr(String equipmentBomUsedArr) {
		this.equipmentBomUsedArr = equipmentBomUsedArr;
	}
	public String getMaterialArr() {
		return materialArr;
	}
	public void setMaterialArr(String materialArr) {
		this.materialArr = materialArr;
	}
	public String getQntyArr() {
		return qntyArr;
	}
	public void setQntyArr(String qntyArr) {
		this.qntyArr = qntyArr;
	}
	public String getEquipmentBomArr() {
		return equipmentBomArr;
	}
	public void setEquipmentBomArr(String equipmentBomArr) {
		this.equipmentBomArr = equipmentBomArr;
	}
	public String getMemberArr() {
		return memberArr;
	}
	public void setMemberArr(String memberArr) {
		this.memberArr = memberArr;
	}
	public String getDelYnArr() {
		return delYnArr;
	}
	public void setDelYnArr(String delYnArr) {
		this.delYnArr = delYnArr;
	}

}
