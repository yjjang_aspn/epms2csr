package aspn.hello.epms.repair.analysis.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.repair.analysis.model.RepairAnalysisVO;
import aspn.hello.epms.repair.result.model.RepairResultVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [고장관리-고장원인분석] Mapper Class
 * 
 * @author 김영환
 * @since 2018.04.24
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.24		김영환		최초 생성
 *   
 * </pre>
 */

@Mapper("repairAnalysisMapper")
public interface RepairAnalysisMapper {
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	/**
	 * 고장원인분석 조회
	 *
	 * @author 김영환
	 * @since 2018.04.25
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairAnalysisList(RepairAnalysisVO repairAnalysisVO);

	/**
	 * 고장원인분석  팝업 데이터 호출
	 * @author 김영환
	 * @since 2018.04.26
	 * @param ReportMonthlyFailureVO repairAnalysisVO
	 * @return HashMap<String, Object>
	 */
	HashMap<String, Object> selectRepairAnalysisInfo(RepairAnalysisVO repairAnalysisVO);

	/**
	 * 고장원인분석  팝업 아이템 목록 호출
	 * @author 김영환
	 * @since 2018.04.26
	 * @param ReportMonthlyFailureVO repairAnalysisVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairAnalysisItem(RepairAnalysisVO repairAnalysisVO);

	/**
	 * 고장원인분석  팝업 헤더  수정
	 * @author 김영환
	 * @since 2018.04.26
	 * @param ReportMonthlyFailureVO repairAnalysisVO
	 */
	void updateRepairAnalysisInfo(RepairAnalysisVO repairAnalysisVO);

	/**
	 * 고장원인분석  팝업 헤더 저장
	 * @author 김영환
	 * @since 2018.04.26
	 * @param ReportMonthlyFailureVO repairAnalysisVO
	 */
	void insertRepairAnalysisInfo(RepairAnalysisVO repairAnalysisVO);

	/**
	 * 고장원인분석  팝업 아이템 수정
	 * @author 김영환
	 * @since 2018.04.26
	 * @param ReportMonthlyFailureVO repairAnalysisVO
	 */
	void updateRepairAnalysisItem(RepairAnalysisVO repairAnalysisVO);

	/**
	 * 고장원인분석  팝업 아이템 저장
	 * @author 김영환
	 * @since 2018.04.26
	 * @param ReportMonthlyFailureVO repairAnalysisVO
	 */
	void insertRepairAnalysisItem(RepairAnalysisVO repairAnalysisVO);

	/**
	 * 고장원인분석  임시저장 누를 시 기존에 있던 아이템 삭제
	 * @author 김영환
	 * @since 2018.05.25
	 * @param ReportMonthlyFailureVO repairAnalysisVO
	 */
	void deleteRepairAnalysisItem(RepairAnalysisVO repairAnalysisVO);

	/**
	 * 고장원인분석  불러오기 목록
	 * @author 김영환
	 * @since 2018.05.25
	 * @param RepairAnalysisVO repairAnalysisVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectPopRepairAnalysisListData(RepairAnalysisVO repairAnalysisVO);
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
