package aspn.hello.epms.repair.analysis.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.ObjUtil;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.epms.repair.analysis.model.RepairAnalysisVO;
import aspn.hello.epms.repair.analysis.service.RepairAnalysisService;
import aspn.hello.epms.repair.result.model.RepairResultVO;
import aspn.hello.epms.repair.result.service.RepairResultService;

/**
 * [고장관리-고장원인분석] Controller Class
 * @author 김영환
 * @since 2018.04.24
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.24		김영환		최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/repair/analysis")
public class RepairAnalysisController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RepairAnalysisService repairAnalysisService;
	
	@Autowired
	AttachService attachService;
	
	@Autowired
	RepairResultService repairResultService;
	
	/**
	 * 고장원인분석 : 메인화면
	 *
	 * @author 김영환
	 * @since 2018.04.24
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairAnalysisList.do")
	public String repairAnalysisList(Model model) throws Exception{
		model.addAttribute("startDt", DateTime.requestMmDay("1","yyyy-MM-dd"));
		model.addAttribute("endDt", DateTime.getDateString());
		return "/epms/repair/analysis/repairAnalysisList";
	}
	
	
	/**
	 * 고장원인분석  팝업 화면
	 * @author 김영환
	 * @since 2018.04.24
	 * @param RepairResultVO repairResultVO
	 * @param Model model
	 * @return String
	 */
	@RequestMapping(value = "/popRepairAnalysisForm.do")
	public String popRepairAnalysisForm(RepairAnalysisVO repairAnalysisVO, Model model){
		try{
			model.addAttribute("REPAIR_RESULT", repairAnalysisVO.getREPAIR_RESULT());
			model.addAttribute("LOCATION", repairAnalysisVO.getLOCATION());
			model.addAttribute("PART", repairAnalysisVO.getPART());
			
			
		}catch(Exception e){
			logger.error("RepairResultController.popRequestPartForm Error !");
			e.printStackTrace();
		}
		
		return "/epms/repair/analysis/popRepairAnalysisForm";
	}
	
	/**
	 * 고장원인분석  팝업 데이터 호출
	 * @author 김영환
	 * @since 2018.04.26
	 * @param ReportMonthlyFailureVO repairAnalysisVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/popRepairAnalysisInfo.do")
	@ResponseBody
	public Map<String, Object> popRepairAnalysisInfo(RepairAnalysisVO repairAnalysisVO) {
		Map<String, Object> resultMap = new HashMap<>();
		try {
			HashMap<String, Object> analysisInfo = repairAnalysisService.selectRepairAnalysisInfo(repairAnalysisVO);
			
			resultMap.put("endDt", DateTime.getDateString());
			
			//데이터 없을 시 
			if(analysisInfo == null) {
				resultMap.put("analysisInfo", null);
			} 
			//데이터 있을 시
			else {
				//정비실적에 등록된 정비원 조회
				RepairResultVO repairResultVO = new RepairResultVO();
				repairResultVO.setCompanyId(getSsCompanyId());
				repairResultVO.setSsLocation(getSsLocation());
				repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_PREVENTIVE); //01:정비실적, 02:고장원인분석
				repairResultVO.setWORK_ID(analysisInfo.get("REPAIR_ANALYSIS").toString());
				List<HashMap<String, Object>> workMemberList = repairResultService.selectWorkMemberList(repairResultVO);
				resultMap.put("repairRegMember", workMemberList);
				
				repairAnalysisVO.setREPAIR_ANALYSIS(analysisInfo.get("REPAIR_ANALYSIS").toString());
				
				List<HashMap<String, Object>> analysisItem = repairAnalysisService.selectRepairAnalysisItem(repairAnalysisVO);
			
				for (Map<String, Object> item : analysisItem) {
					
					if(Integer.parseInt(item.get("FILE_CNT").toString()) > 0){
						AttachVO attachVO = new AttachVO();
						attachVO.setATTACH_GRP_NO(item.get("ATTACH_GRP_NO").toString());
						List<HashMap<String, Object>> attachList = attachService.selectFileList(attachVO);
						
						item.put("ATTACH_LIST", attachList);
					}
				}
				resultMap.put("analysisInfo", analysisInfo);
				resultMap.put("analysisItem", analysisItem);
				
			}
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
		}
		return resultMap;
	}
	
	
	/**
	 * 고장원인분석  팝업 데이터 저장
	 * @author 김영환
	 * @since 2018.04.26
	 * @param HttpServletRequest request
	 * @param ReportMonthlyFailureVO repairAnalysisVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/popRepairAnalysisSave.do")
	@ResponseBody
	public Map<String, Object> popRepairAnalysisSave(HttpServletRequest request, MultipartHttpServletRequest mRequest, RepairAnalysisVO repairAnalysisVO) throws Exception{
		Map<String, Object> resultMap = new HashMap<>();
		try {
			repairAnalysisVO.setREG_ID(getSsUserId());
			
			// 임시저장 불러오고 저장완료 누르는지 체크
			if("reg".equals(repairAnalysisVO.getFlag())){
				repairAnalysisService.updateRepairAnalysisInfo(repairAnalysisVO);
				
				repairAnalysisService.deleteRepairAnalysisItem(repairAnalysisVO);
			} else {
				repairAnalysisService.insertRepairAnalysisInfo(repairAnalysisVO);
				repairAnalysisVO.setREPAIR_ANALYSIS(repairAnalysisVO.getSEQ_REPAIR_ANALYSIS());
			}
			
			//정비원 등록
			repairAnalysisService.insertWorkingLog(repairAnalysisVO);
			
			if(!"0".equals(repairAnalysisVO.getITEM_CNT())){
				// ITEM 수만큼 반복하기
				int i = 0;
				for(String str : request.getParameterValues("ATTACH_GRP_NO")){
					RepairAnalysisVO repairAnalysisVO1 = new RepairAnalysisVO();
					
					repairAnalysisVO1.setAttachExistYn(request.getParameterValues("attachExistYn")[i]);
					repairAnalysisVO1.setDEL_SEQ(request.getParameterValues("DEL_SEQ")[i]);
					repairAnalysisVO1.setFILE_CNT(request.getParameterValues("FILE_CNT")[i]);
					repairAnalysisVO1.setREPAIR_ANALYSIS(repairAnalysisVO.getREPAIR_ANALYSIS());
					repairAnalysisVO1.setDEL_YN(request.getParameterValues("DEL_YN")[i]);
					repairAnalysisVO1.setREPAIR_ANALYSIS_ITEM(request.getParameterValues("REPAIR_ANALYSIS_ITEM")[i]);
					repairAnalysisVO1.setREG_ID(getSsUserId());
					repairAnalysisVO1.setSEQ_DSP(request.getParameterValues("SEQ_DSP")[i]);
					repairAnalysisVO1.setDESCRIPTION1(request.getParameterValues("DESCRIPTION1")[i]);
					repairAnalysisVO1.setDESCRIPTION2(request.getParameterValues("DESCRIPTION2")[i]);
					
					String attachGrpNo = str;
					if (ObjUtil.isEmpty(attachGrpNo)) {
						if ("Y".equals(request.getParameterValues("attachExistYn")[i])) {
							attachGrpNo = attachService.selectAttachGrpNo();
						}
					}
					repairAnalysisVO1.setATTACH_GRP_NO(attachGrpNo);
					
					// 이미지 파일 등록
					if ("Y".equals(repairAnalysisVO1.getAttachExistYn()) || !ObjUtil.isEmpty(repairAnalysisVO1.getDEL_SEQ())) {
						AttachVO attachVO = new AttachVO();
						attachVO.setDEL_SEQ(repairAnalysisVO1.getDEL_SEQ());
						attachVO.setATTACH_GRP_NO(attachGrpNo);
						attachVO.setMODULE(CodeConstants.FILE_MODULE_EPMS_REPAIR_ANALYSIS);
						String fileName = request.getParameterValues("FILE_LIST_NAME")[i];
						attachService.AttachListEdit(request, mRequest, attachVO, fileName);
					}
					
					if ("0".equals(repairAnalysisVO1.getFILE_CNT())) {
						repairAnalysisVO1.setATTACH_GRP_NO("");
					}
					i++;
					
					
					// 임시저장 불러오고 저장완료 누르는지 체크
					if("reg".equals(repairAnalysisVO.getFlag())){
						if(ObjUtil.isEmpty(repairAnalysisVO1.getREPAIR_ANALYSIS_ITEM())){
							repairAnalysisService.insertRepairAnalysisItem(repairAnalysisVO1);
						} else {
							repairAnalysisService.updateRepairAnalysisItem(repairAnalysisVO1);
						}
					} else {
						repairAnalysisService.insertRepairAnalysisItem(repairAnalysisVO1);
					}
					
				}
			}
			
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
		}
		return resultMap;
	}
	
	/**
	 * 고장원인분석 :  Grid Layout
	 *
	 * @author 김영환
	 * @since 2018.04.25
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairAnalysisListLayout.do")
	public String repairAnalysisListLayout() throws Exception{
		return "/epms/repair/analysis/repairAnalysisListLayout";
	}
	
	/**
	 * 고장원인분석 : Grid Data
	 *
	 * @author 김영환
	 * @since 2018.04.25
	 * @param RepairResultVO repairResultVO
	 * @param Model model
	 * @return String
	 */
	@RequestMapping(value = "/repairAnalysisListData.do")
	public String repairAnalysisListData(RepairAnalysisVO repairAnalysisVO, Model model){
		try {
			repairAnalysisVO.setUserId(getSsUserId());
			repairAnalysisVO.setLOCATION(getSsLocation());
			repairAnalysisVO.setPART(getSsPart());
			repairAnalysisVO.setSubAuth(getSsSubAuth());
			
			//고장원인분석 목록
			List<HashMap<String, Object>> list= repairAnalysisService.selectRepairAnalysisList(repairAnalysisVO);
			model.addAttribute("list", list);
		} catch (Exception e) {
			logger.error("RepairAnalysisController.repairAnalysisListData Error !");
			e.printStackTrace();
		}
		return "/epms/repair/analysis/repairAnalysisListData";
	}
	
	/**
	 * 고장원인분석 불러오기 목록:  Grid Layout
	 *
	 * @author 김영환
	 * @since 2018.04.25
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popRepairAnalysisListLayout.do")
	public String popRepairAnalysisListLayout() throws Exception{
		return "/epms/repair/analysis/popRepairAnalysisListLayout";
	}
	
	/**
	 * 고장원인분석 불러오기 목록: Grid Data
	 *
	 * @author 김영환
	 * @since 2018.04.25
	 * @param RepairAnalysisVO repairAnalysisVO
	 * @param Model model
	 * @return String
	 */
	@RequestMapping(value = "/popRepairAnalysisListData.do")
	public String popRepairAnalysisListData(RepairAnalysisVO repairAnalysisVO, Model model){
		try {
			
			//고장원인분석 목록
			List<HashMap<String, Object>> list= repairAnalysisService.selectPopRepairAnalysisListData(repairAnalysisVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			logger.error("RepairAnalysisController.popRepairAnalysisListData Error !");
			e.printStackTrace();
		}
		return "/epms/repair/analysis/popRepairAnalysisListData";
	}
}