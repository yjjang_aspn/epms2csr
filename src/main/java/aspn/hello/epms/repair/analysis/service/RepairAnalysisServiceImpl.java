package aspn.hello.epms.repair.analysis.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.config.CodeConstants;
import aspn.hello.com.mapper.AttachMapper;
import aspn.hello.epms.repair.analysis.mapper.RepairAnalysisMapper;
import aspn.hello.epms.repair.analysis.model.RepairAnalysisVO;
import aspn.hello.epms.repair.result.mapper.RepairResultMapper;
import aspn.hello.epms.repair.result.model.RepairResultVO;

/**
 * [고장관리-고장원인분석] Business Implement Class
 * 
 * @author 김영환
 * @since 2018.04.24
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.24		김영환		최초 생성
 *   
 * </pre>
 */

@Service
public class RepairAnalysisServiceImpl implements RepairAnalysisService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RepairAnalysisMapper repairAnalysisMapper;
	
	@Autowired
	RepairResultMapper repairResultMapper;
	
	@Autowired
	AttachMapper attachMapper;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 고장원인분석 조회
	 *
	 * @author 김영환
	 * @since 2018.04.25
	 * @param RepairResultVO repairResultVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairAnalysisList(RepairAnalysisVO repairAnalysisVO) {
		return repairAnalysisMapper.selectRepairAnalysisList(repairAnalysisVO);
	}

	/**
	 * 고장원인분석  팝업 데이터 호출
	 * @author 김영환
	 * @since 2018.04.26
	 * @param ReportMonthlyFailureVO repairAnalysisVO
	 * @return HashMap<String, Object>
	 */
	@Override
	public HashMap<String, Object> selectRepairAnalysisInfo(RepairAnalysisVO repairAnalysisVO) {
		return repairAnalysisMapper.selectRepairAnalysisInfo(repairAnalysisVO);
	}

	/**
	 * 고장원인분석  팝업 아이템 목록 호출
	 * @author 김영환
	 * @since 2018.04.26
	 * @param ReportMonthlyFailureVO repairAnalysisVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairAnalysisItem(RepairAnalysisVO repairAnalysisVO) {
		return repairAnalysisMapper.selectRepairAnalysisItem(repairAnalysisVO);
	}

	/**
	 * 고장원인분석  팝업 헤더  수정
	 * @author 김영환
	 * @since 2018.04.26
	 * @param ReportMonthlyFailureVO repairAnalysisVO
	 */
	@Override
	public void updateRepairAnalysisInfo(RepairAnalysisVO repairAnalysisVO) {
		repairAnalysisMapper.updateRepairAnalysisInfo(repairAnalysisVO);
	}

	/**
	 * 고장원인분석  팝업 헤더 저장
	 * @author 김영환
	 * @since 2018.04.26
	 * @param ReportMonthlyFailureVO repairAnalysisVO
	 */
	@Override
	public void insertRepairAnalysisInfo(RepairAnalysisVO repairAnalysisVO) {
		repairAnalysisMapper.insertRepairAnalysisInfo(repairAnalysisVO);
	}

	/**
	 * 고장원인분석  팝업 아이템 수정
	 * @author 김영환
	 * @since 2018.04.26
	 * @param ReportMonthlyFailureVO repairAnalysisVO
	 */
	@Override
	public void updateRepairAnalysisItem(RepairAnalysisVO repairAnalysisVO) {
		repairAnalysisMapper.updateRepairAnalysisItem(repairAnalysisVO);
	}

	/**
	 * 고장원인분석  팝업 아이템 저장
	 * @author 김영환
	 * @since 2018.04.26
	 * @param ReportMonthlyFailureVO repairAnalysisVO
	 */
	@Override
	public void insertRepairAnalysisItem(RepairAnalysisVO repairAnalysisVO) {
		repairAnalysisMapper.insertRepairAnalysisItem(repairAnalysisVO);
	}

	/**
	 * 고장원인분석  정비원 저장
	 * @author 김영환
	 * @since 2018.04.26
	 * @param ReportMonthlyFailureVO repairAnalysisVO
	 */
	@Override
	public void insertWorkingLog(RepairAnalysisVO repairAnalysisVO) {
		//정비원 등록
		if(!("".equals(repairAnalysisVO.getMemberArr()) || repairAnalysisVO.getMemberArr() == null)){

			RepairResultVO repairResultVO = new RepairResultVO();
			//작업유형 세팅(01:정비, 02:예방보전, 03:사후분석)
			repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_PREVENTIVE);
			//작업대상ID 세팅
			repairResultVO.setWORK_ID(repairAnalysisVO.getREPAIR_ANALYSIS());
			repairResultVO.setUserId(repairAnalysisVO.getREG_ID());
						
			String[] member_arr = repairAnalysisVO.getMemberArr().split(",");
			String[] delYnArr = repairAnalysisVO.getDelYnArr().split(",");
			int	workLogCheck = 0;
			for(int i=0; i<member_arr.length; i++){
				//정비원ID 담기
				repairResultVO.setUSER_ID(member_arr[i].trim());
				repairResultVO.setDEL_YN(delYnArr[i].trim());
				workLogCheck = repairResultMapper.selectWorkLogCheck(repairResultVO);
				
				// 등록되지 않은 정비원의 경우
				if ( workLogCheck == 0 && "N".equals(repairResultVO.getDEL_YN())) {
					// WorkLog테이블 Seq 조회
					String workLogSeq = repairResultMapper.selectWorkLogSeq(repairResultVO);
					repairResultVO.setWORK_LOG(workLogSeq);
					
					//정비원등록 WORK_LOG INSERT
					repairResultMapper.insertWorkLog(repairResultVO);
				} else {
					repairResultMapper.updateWorkLog(repairResultVO);
				}
			}
		}
	}

	/**
	 * 고장원인분석  임시저장 누를 시 기존에 있던 아이템 삭제
	 * @author 김영환
	 * @since 2018.05.25
	 * @param ReportMonthlyFailureVO repairAnalysisVO
	 */
	@Override
	public void deleteRepairAnalysisItem(RepairAnalysisVO repairAnalysisVO) {
		repairAnalysisMapper.deleteRepairAnalysisItem(repairAnalysisVO);
	}

	/**
	 * 고장원인분석  불러오기 목록
	 * @author 김영환
	 * @since 2018.05.25
	 * @param RepairAnalysisVO repairAnalysisVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectPopRepairAnalysisListData(RepairAnalysisVO repairAnalysisVO) {
		return repairAnalysisMapper.selectPopRepairAnalysisListData(repairAnalysisVO);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
