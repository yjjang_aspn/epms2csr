package aspn.hello.epms.repair.review.service;
        
import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.repair.review.model.RepairReviewVO;

/**
 * [고장관리-설비보전검토리스트] Service Class
 * 
 * @author 김영환
 * @since 2018.03.06
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.06		김영환			최초 생성
 *   
 * </pre>
 */

public interface RepairReviewService {

	/**
	 * 고장분석기준: 기간
	 *
	 * @author 김영환
	 * @since 2018.03.06	
	 * @return int
	 */
	public int selectStandardPeriod();

	/**
	 * 고장분석기준: 횟수
	 *
	 * @author 김영환
	 * @since 2018.03.06	
	 * @return int
	 */
	public int selectStandardFrequency();

	/**
	 * 예방보전검토 : 설비별 정비횟수 목록
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairReviewVO repairReviewVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> selectRepairReviewList(RepairReviewVO repairReviewVO);

	/**
	 * 예방보전검토 : 정비현황 목록
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairReviewVO repairReviewVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> selectRepairResultList(RepairReviewVO repairReviewVO);

	/**
	 * 고장원인분석 기준정보 변경
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairReviewVO repairReviewVO
	 * @return int
	 */
	public int analysisStandardUpdate(RepairReviewVO repairReviewVO);

}
