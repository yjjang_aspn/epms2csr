package aspn.hello.epms.repair.review.controller;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.util.ContSupport;
import aspn.hello.epms.repair.review.model.RepairReviewVO;
import aspn.hello.epms.repair.review.service.RepairReviewService;

/**
 * [고장관리-설비보전검토리스트] Controller Class
 * 
 * @author 김영환
 * @since 2018.03.06
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.06		김영환			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/repair/review")
public class RepairReviewController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RepairReviewService repairReviewService;
		
	/**
	 * 설비보전검토리스트 화면
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairReviewList.do")
	public String repairReviewList(Model model){
		try {
			//고장분석기준: 기간
			model.addAttribute("period", repairReviewService.selectStandardPeriod());
			//고장분석기준 : 횟수
			model.addAttribute("frequency", repairReviewService.selectStandardFrequency());
		} catch (Exception e) {
			logger.error("RepairReviewController.repairReviewList Error !" + e.toString());
			e.printStackTrace();
		}
		return "/epms/repair/review/repairReviewList";
	}
	
	/**
	 * 설비보전검토리스트 - 설비별 고장현황 Grid Layout
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairReviewListLayout.do")
	public String repairReviewListLayout() throws Exception {
		return "/epms/repair/review/repairReviewListLayout";
	}
	
	/**
	 * 설비보전검토리스트	 - 설비별 고장현황 Grid Data
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairReviewVO repairReviewVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairReviewListData.do")
	public String repairReviewListData(RepairReviewVO repairReviewVO, Model model){
		try {
			repairReviewVO.setSubAuth(getSsSubAuth());
			
			//설비별 고장현황 목록
			List<HashMap<String, Object>> list= repairReviewService.selectRepairReviewList(repairReviewVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("RepairReviewController.repairReviewListData Error !" + e.toString());
		}
		return "/epms/repair/review/repairReviewListData";
	}
	
	/**
	 * 설비보전검토리스트  - 정비내역 Grid Data
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairReviewVO repairReviewVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairResultListData.do")
	public String repairResultListData(RepairReviewVO repairReviewVO, Model model){
		try {
			//정비현황 목록
			List<HashMap<String, Object>> list= repairReviewService.selectRepairResultList(repairReviewVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("RepairReviewController.repairResultListData Error !" + e.toString());
		}
		return "/epms/repair/result/repairResultListData";
	}
	
	/**
	 * 설비보전검토리스트  - 고장원인분석 기준정보 팝업 화면 
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popAnalysisStandard.do")
	public String popAnalysisStandard(Model model){
		try {
			//고장분석기준: 기간
			model.addAttribute("period", repairReviewService.selectStandardPeriod());
			//고장분석기준 : 횟수
			model.addAttribute("frequency", repairReviewService.selectStandardFrequency());
		} catch (Exception e) {
			logger.error("RepairReviewController.popAnalysisStandard Error !" + e.toString());
			e.printStackTrace();
		}
		return "/epms/repair/review/popAnalysisStandard";
	}
	
	/**
	 * 설비보전검토리스트  - 고장원인분석 기준정보 변경
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairReviewVO repairReviewVO
	 * @return int
	 * @throws Exception
	 */
	@RequestMapping(value = "/analysisStandardUpdate.do")
	@ResponseBody
	public int analysisStandardUpdate(RepairReviewVO repairReviewVO){
		int rtnVal = 0;
		try {
			repairReviewVO.setUserId(getSsUserId());
			rtnVal = repairReviewService.analysisStandardUpdate(repairReviewVO);
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("RepairReviewController.analysisStandardUpdate Error !" + e.toString());
		}
		return rtnVal;
	}
	
}
