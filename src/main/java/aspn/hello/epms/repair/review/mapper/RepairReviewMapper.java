package aspn.hello.epms.repair.review.mapper;
        
import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.repair.review.model.RepairReviewVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [고장관리-설비보전검토리스트] Mapper Class
 * 
 * @author 김영환
 * @since 2018.03.06
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.06		김영환			최초 생성
 *   
 * </pre>
 */

@Mapper("repairReviewMapper")
public interface RepairReviewMapper {

	/**
	 * 고장분석기준 : 기간
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @return int
	 */
	public int selectStandardPeriod();

	/**
	 * 고장분석기준 : 횟수
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @return int
	 */
	public int selectStandardFrequency();

	/**
	 * 예방보전검토 : 설비별 정비횟수 목록
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairReviewVO repairReviewVO
	 * @return List<RepairReviewVO>
	 */
	public List<HashMap<String, Object>> selectRepairReviewList(RepairReviewVO repairReviewVO);

	/**
	 * 정비현황 목록
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairReviewVO repairReviewVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> selectRepairResultList(RepairReviewVO repairReviewVO);

	/**
	 * 고장분석기준: 기간 변경
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairReviewVO repairReviewVO
	 * @return int
	 */
	public int updatAnalysisStandard1(RepairReviewVO repairReviewVO);

	/**
	 * 고장분석기준: 횟수 변경
	 * 
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairReviewVO repairReviewVO
	 * @return int
	 */
	public int updateAnalysisStandard2(RepairReviewVO repairReviewVO);

	/**
	 * 조치 목록
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param RepairReviewVO repairReviewVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> selectTroubleListOpt2();
	
}
