package aspn.hello.epms.repair.review.service;
        
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.hello.epms.repair.review.mapper.RepairReviewMapper;
import aspn.hello.epms.repair.review.model.RepairReviewVO;

/**
 * [고장관리-설비보전검토리스트] Business Implement Class
 * 
 * @author 김영환
 * @since 2018.03.06
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.06		김영환			 최초 생성
 *   
 * </pre>
 */

@Service
public class RepairReviewServiceImpl implements RepairReviewService {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RepairReviewMapper repairReviewMapper;

	/**
	 * 고장분석기준 : 기간
	 *
	 * @author 김영환
	 * @since 2018.03.05
	 * @return int
	 */
	@Override
	public int selectStandardPeriod() {
		return repairReviewMapper.selectStandardPeriod();
	}

	/**
	 * 고장분석기준: 횟수
	 *
	 * @author 김영환
	 * @since 2018.03.05
	 * @return int
	 */
	@Override
	public int selectStandardFrequency() {
		return repairReviewMapper.selectStandardFrequency();
	}

	/**
	 * 예방보전검토 : 설비별 정비횟수 목록
	 *
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairReviewVO repairReviewVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairReviewList(RepairReviewVO repairReviewVO) {
		
		int period = repairReviewMapper.selectStandardPeriod();	//고장분석기준 : 기간
		repairReviewVO.setPERIOD(Integer.toString(period));
		
		int frequency = repairReviewMapper.selectStandardFrequency();	//고장분석기준 : 횟수 			
		repairReviewVO.setFREQUENCY(Integer.toString(frequency));
		
		return repairReviewMapper.selectRepairReviewList(repairReviewVO);
	}

	/**
	 * 예방보전검토 : 정비현황 목록
	 *
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairReviewVO repairReviewVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairResultList(RepairReviewVO repairReviewVO) {				
		return repairReviewMapper.selectRepairResultList(repairReviewVO);
	}

	/**
	 * 고장원인분석 기준정보 변경
	 *
	 * @author 김영환
	 * @since 2018.03.05
	 * @param RepairReviewVO repairReviewVO
	 * @return int
	 */
	@Override
	public int analysisStandardUpdate(RepairReviewVO repairReviewVO) {
		int rtnVal = 0;
		
		//고장분석기준: 기간 변경  
		rtnVal = repairReviewMapper.updatAnalysisStandard1(repairReviewVO);
		
		//고장분석기준: 횟수 변경
		rtnVal = repairReviewMapper.updateAnalysisStandard2(repairReviewVO);
		
		return rtnVal;
	}
	
	
}
