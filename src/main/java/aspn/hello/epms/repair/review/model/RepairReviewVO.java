package aspn.hello.epms.repair.review.model;
        
import aspn.hello.com.model.AbstractVO;

/**
 * [고장관리-설비보전검토리스트] VO Class
 * @author 김영환
 * @since 2018.03.06
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.06		김영환			최초 생성
 *   
 * </pre>
 */

public class RepairReviewVO extends AbstractVO{
	private static final long serialVersionUID = 1L;
	
	/** EPMS_EQUIPMENT DB model  */
	private String EQUIPMENT				= ""; //설비 ID
		
	/** COMCD DB model  */
	private String PERIOD					= ""; //고장분석기준 : 기간
	private String FREQUENCY				= ""; //고장분석기준 : 횟수
	
	/** parameter model  */
	private String flag						= ""; //flag
	
	public String getEQUIPMENT() {
		return EQUIPMENT;
	}
	public void setEQUIPMENT(String eQUIPMENT) {
		EQUIPMENT = eQUIPMENT;
	}
	public String getPERIOD() {
		return PERIOD;
	}
	public void setPERIOD(String pERIOD) {
		PERIOD = pERIOD;
	}
	public String getFREQUENCY() {
		return FREQUENCY;
	}
	public void setFREQUENCY(String fREQUENCY) {
		FREQUENCY = fREQUENCY;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	
}
