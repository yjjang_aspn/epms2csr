package aspn.hello.epms.report.preventiveMember.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.report.preventiveMember.model.ReportPreventiveMemberVO;

/**
 * [레포트 예방보전현황(정비원)] Service Class
 * 
 * @author 김영환
 * @since 2018.05.28
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.28		김영환			최초 생성
 *   
 * </pre>
 */

public interface ReportPreventiveMemberService {
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 레포트 예방보전 현황(정비원) 목록
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportPreventiveMemberVO reportPreventiveMemberVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> reportPreventiveMemberList(ReportPreventiveMemberVO reportPreventiveMemberVO);
	
}
