package aspn.hello.epms.report.preventiveMember.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.preventive.work.model.PreventiveWorkVO;
import aspn.hello.epms.report.preventiveMember.model.ReportPreventiveMemberVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [레포트 예방보전현황(정비원)] Mapper Class
 * 
 * @author 김영환
 * @since 2018.05.28
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.28		김영환			최초 생성
 *   
 * </pre>
 */

@Mapper("reportPreventiveMemberMapper")
public interface ReportPreventiveMemberMapper {
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 레포트 예방보전 현황(정비원) 목록
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportPreventiveMemberVO reportPreventiveMemberVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> reportPreventiveMemberList(ReportPreventiveMemberVO reportPreventiveMemberVO);
}
