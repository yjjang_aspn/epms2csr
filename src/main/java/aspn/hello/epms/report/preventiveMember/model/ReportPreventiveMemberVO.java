package aspn.hello.epms.report.preventiveMember.model;

import aspn.hello.com.model.AbstractVO;

/**
 * [레포트 예방보전현황(정비원)] VO
 * 
 * @author 김영환
 * @since 2018.05.28
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.28		김영환			최초 생성
 *   
 * </pre>
 */

public class ReportPreventiveMemberVO extends AbstractVO{
	
	private static final long serialVersionUID = 1L;
	
	private String PART_NAME						= ""; // 파트이름
	private String PART								= ""; // 파트 ID
	private String NAME								= ""; // 정비원 이름
	private String USER_ID							= ""; // 정비원 ID
	private String LOCATION							= ""; // 소속공장
	
	private String SUM_PLAN							= ""; // 전체 계획건
	private String SUM_COMPLETE						= ""; // 전체 실시건(만료이전)
	
	private String TBM_PLAN							= ""; // TBM 계획건
	private String TBM_COMPLETE						= ""; // TBM 실시건(만료이전)
	
	private String CBM_PLAN							= ""; // CBM 계획건
	private String CBM_COMPLETE						= ""; // CBM 실시건(만료이전)
	private String CBM_ERROR						= ""; // CBM 이상건
	private String CBM_MEASURE						= ""; // CBM 조치건
	public String getPART_NAME() {
		return PART_NAME;
	}
	public void setPART_NAME(String pART_NAME) {
		PART_NAME = pART_NAME;
	}
	public String getPART() {
		return PART;
	}
	public void setPART(String pART) {
		PART = pART;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getUSER_ID() {
		return USER_ID;
	}
	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getSUM_PLAN() {
		return SUM_PLAN;
	}
	public void setSUM_PLAN(String sUM_PLAN) {
		SUM_PLAN = sUM_PLAN;
	}
	public String getSUM_COMPLETE() {
		return SUM_COMPLETE;
	}
	public void setSUM_COMPLETE(String sUM_COMPLETE) {
		SUM_COMPLETE = sUM_COMPLETE;
	}
	public String getTBM_PLAN() {
		return TBM_PLAN;
	}
	public void setTBM_PLAN(String tBM_PLAN) {
		TBM_PLAN = tBM_PLAN;
	}
	public String getTBM_COMPLETE() {
		return TBM_COMPLETE;
	}
	public void setTBM_COMPLETE(String tBM_COMPLETE) {
		TBM_COMPLETE = tBM_COMPLETE;
	}
	public String getCBM_PLAN() {
		return CBM_PLAN;
	}
	public void setCBM_PLAN(String cBM_PLAN) {
		CBM_PLAN = cBM_PLAN;
	}
	public String getCBM_COMPLETE() {
		return CBM_COMPLETE;
	}
	public void setCBM_COMPLETE(String cBM_COMPLETE) {
		CBM_COMPLETE = cBM_COMPLETE;
	}
	public String getCBM_ERROR() {
		return CBM_ERROR;
	}
	public void setCBM_ERROR(String cBM_ERROR) {
		CBM_ERROR = cBM_ERROR;
	}
	public String getCBM_MEASURE() {
		return CBM_MEASURE;
	}
	public void setCBM_MEASURE(String cBM_MEASURE) {
		CBM_MEASURE = cBM_MEASURE;
	}
	
}
