package aspn.hello.epms.report.preventiveMember.controller;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.hello.com.service.AttachService;
import aspn.hello.epms.report.preventiveMember.model.ReportPreventiveMemberVO;
import aspn.hello.epms.report.preventiveMember.service.ReportPreventiveMemberService;
import aspn.hello.mem.model.MemberVO;

/**
 * [레포트 예방보전 현황(정비원)] Controller Class
 * @author 김영환
 * @since 2018.05.28
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.28		김영환			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/report/preventiveMember")
public class ReportPreventiveMemberController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	
	@Autowired
	ReportPreventiveMemberService reportPreventiveMemberService;
	
	@Autowired
	AttachService attachService;

	/**
	 * [레포트 예방보전 현황(정비원)] : 화면호출
	 *
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportPreventiveMemberVO reportPreventiveMemberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportPreventiveMemberList.do")
	public String reportPreventiveMemberList(ReportPreventiveMemberVO reportPreventiveMemberVO, Model model) throws Exception{
		return "/epms/report/preventiveMember/reportPreventiveMemberList";
	}
	
	/**
	 * [레포트 예방보전 현황(정비원)] : 그리드 레이아웃
	 *
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportPreventiveMemberVO reportPreventiveMemberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportPreventiveMemberListLayout.do")
	public String reportPreventiveMemberListLayout(ReportPreventiveMemberVO reportPreventiveMemberVO, Model model) throws Exception{
		return "/epms/report/preventiveMember/reportPreventiveMemberListLayout";
	}
	
	/**
	 * [레포트 예방보전 현황(정비원)] : 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportPreventiveMemberVO reportPreventiveMemberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportPreventiveMemberListData.do")
	public String reportPreventiveMemberListData(ReportPreventiveMemberVO reportPreventiveMemberVO, Model model) throws Exception{
		reportPreventiveMemberVO.setSubAuth(getSsSubAuth());
		reportPreventiveMemberVO.setCompanyId(getSsCompanyId());
//		reportPreventiveMemberVO.setSsLocation(getSsLocation());
		
		List<HashMap<String, Object>> list = reportPreventiveMemberService.reportPreventiveMemberList(reportPreventiveMemberVO);
		
		model.addAttribute("list", list);
		return "/epms/report/preventiveMember/reportPreventiveMemberListData";
	}
}
