package aspn.hello.epms.report.materialMonthlyOutput.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.TreeGridUtil;
import aspn.hello.epms.report.materialMonthlyOutput.model.ReportMaterialMonthlyOutputVO;
import aspn.hello.epms.report.materialMonthlyOutput.service.ReportMaterialMonthlyOutputService;


/**
 * [레포트-자재출고현황 분석] Controller Class
 * @author 김영환
 * @since 2018.06.14
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.06.14			김영환			최초 생성
 *
 * </pre>
 */

@Controller      
@RequestMapping("/epms/report/materialMonthlyOutput")
public class ReportMaterialMonthlyOutputController extends ContSupport {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/* INSTANCE VAR */
	@Autowired
	ReportMaterialMonthlyOutputService reportMaterialMonthlyOutputService; 
	
	
	/**
	 * [레포트-자재출고현황 분석] : 메인화면
	 *
	 * @author 김영환
	 * @since 2018.06.14
	 * @param ReportMaterialMonthlyOutputVO reportMaterialMonthlyOutputVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportMaterialMonthlyOutputList.do")
	public String reportMaterialMonthlyOutputList(ReportMaterialMonthlyOutputVO reportMaterialMonthlyOutputVO, Model model){
		
		try {
			model.addAttribute("searchDt", DateTime.getYearStr()+ "-" +DateTime.getMonth());
		} catch (Exception e) {
			logger.error("ReportMaterialMonthlyOutputController.reportMaterialMonthlyOutputList > " + e.toString());
			e.printStackTrace();
		}

		return "/epms/report/materialMonthlyOutput/reportMaterialMonthlyOutputList";
		
	}
	
	/**
	 * [레포트-자재출고현황 분석] : 메인 그리드 레이아웃
	 *
	 * @author 김영환
	 * @since 2018.06.14
	 * @param ReportMaterialMonthlyOutputVO reportMaterialMonthlyOutputVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportMaterialMonthlyOutputListLayout.do")
	public String materialMonthlyOutputListLayout(ReportMaterialMonthlyOutputVO reportMaterialMonthlyOutputVO, Model model){
		SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM");
		try{
			String startDt = reportMaterialMonthlyOutputVO.getStartDt();
		    Date date = fm.parse(startDt);
		    
		    Calendar cal = new GregorianCalendar(Locale.KOREA);
		    cal.setTime(date);
		    
		    String strDate = fm.format(cal.getTime());
		    
		    List<String> STDYYYY_LIST = new ArrayList();
		    
		    STDYYYY_LIST.add(strDate);
		    for (int i = 0; i < reportMaterialMonthlyOutputVO.getDiffMonth(); i++) {
		    	cal.add(Calendar.MONTH, 1);
		    	strDate = fm.format(cal.getTime());
		    	STDYYYY_LIST.add(strDate);
		    }
		    model.addAttribute("list", STDYYYY_LIST);
			
		} catch (Exception e) {
			logger.error("MaterialMonthlyOutputController.MaterialMonthlyOutputListLayout > " + e.toString());
			e.printStackTrace();
		}
			
		return "/epms/report/materialMonthlyOutput/reportMaterialMonthlyOutputListLayout";
	}
	
	/**
	 * [레포트-자재출고현황 분석] : 메인 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.06.14
	 * @param ReportMaterialMonthlyOutputVO reportMaterialMonthlyOutputVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */						  
	@RequestMapping(value = "/reportMaterialMonthlyOutputListData.do")
	public String MaterialMonthlyOutputListData(ReportMaterialMonthlyOutputVO reportMaterialMonthlyOutputVO, Model model) throws Exception{
		SimpleDateFormat fm = new SimpleDateFormat("yyyyMM");
		List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>> ();
		try {
			String startDt = reportMaterialMonthlyOutputVO.getStartDt().replace("-", "");
		    Date date = fm.parse(startDt);
		    
		    Calendar cal = new GregorianCalendar(Locale.KOREA);
		    cal.setTime(date);
		    
		    String strDate = fm.format(cal.getTime());
		    
		    List<String> STDYYYY_LIST = new ArrayList();
		    
		    STDYYYY_LIST.add(strDate);
		    for (int i = 0; i < reportMaterialMonthlyOutputVO.getDiffMonth(); i++) {
		    	cal.add(Calendar.MONTH, 1);
		    	strDate = fm.format(cal.getTime());
		    	STDYYYY_LIST.add(strDate);
		    }
		    reportMaterialMonthlyOutputVO.setSTDYYYY_LIST(STDYYYY_LIST);
		    reportMaterialMonthlyOutputVO.setSubAuth(getSsSubAuth());
		    reportMaterialMonthlyOutputVO.setCompanyId(getSsCompanyId());
		    
		    list = reportMaterialMonthlyOutputService.selectReportMaterialMonthlyOutputList(reportMaterialMonthlyOutputVO);
		    
		    model.addAttribute("list", list);
		    model.addAttribute("list2", STDYYYY_LIST);
		    
 		} catch (Exception e) {
			logger.error("MaterialMonthlyOutputController.MaterialMonthlyOutputListData > " + e.toString());
			e.printStackTrace();
		}
//		return TreeGridUtil.getGridListData(list);
		return "/epms/report/materialMonthlyOutput/reportMaterialMonthlyOutputListData";
	}
	
	
	
}
