package aspn.hello.epms.report.materialMonthlyOutput.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.report.materialMonthlyOutput.model.ReportMaterialMonthlyOutputVO;

/**
 * [레포트-자재출고현황 분석] Service Class
 * @author 김영환
 * @since 2018.06.14
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.06.14			김영환			최초 생성
 *
 * </pre>
 */

public interface ReportMaterialMonthlyOutputService {

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * 레포트 자재출고 현황 분석
	 * @author 김영환
	 * @since 2018.06.14
	 * @param ReportMaterialMonthlyOutputVO reportMaterialMonthlyOutputVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectReportMaterialMonthlyOutputList(ReportMaterialMonthlyOutputVO reportMaterialMonthlyOutputVO);
	
	
}
