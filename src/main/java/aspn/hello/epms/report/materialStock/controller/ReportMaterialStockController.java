package aspn.hello.epms.report.materialStock.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import aspn.com.common.util.ContSupport;
import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.epms.report.repairMember.model.ReportRepairMemberVO;
import aspn.hello.rfc.model.ZMM_SELL_BY_DATE_EXPORT;
import aspn.hello.rfc.model.ZMM_SELL_BY_DATE_IMPORT;
import aspn.hello.rfc.model.ZMM_SELL_BY_DATE_ITAB;
import aspn.hello.rfc.service.RfcService;


/**
 * 
 * [레포트 - SAP 유통기한 및 재고수량 조회] Controller Class
 * @author 김영환
 * @since 2019.04.02
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2019.04.02			김영환			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/report/materialStock")
public class ReportMaterialStockController extends ContSupport {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/* INSTANCE VAR */
	
	@Autowired
	RfcService rfcService;
	
	/**
	 * [레포트 - SAP 유통기한 및 재고수량 조회] : 화면호출
	 *
	 * @author 김영환
	 * @since 2019.04.02
	 * @param ReportRepairMemberVO reportRepairMemberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportMaterialStockList.do")
	public String reportMaterialStockList(ReportRepairMemberVO reportRepairMemberVO, Model model) throws Exception{
		return "/epms/report/materialStock/reportMaterialStockList";
	}
	
	/**
	 * [레포트 - SAP 유통기한 및 재고수량 조회] : 그리드 레이아웃
	 *
	 * @author 김영환
	 * @since 2019.03.29
	 * @param ReportRepairMemberVO reportRepairMemberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportMaterialStockListLayout.do")
	public String reportMaterialStockListLayout(ReportRepairMemberVO reportRepairMemberVO, Model model) throws Exception{
		return "/epms/report/materialStock/reportMaterialStockListLayout";
	}
	
	/**
	 *[레포트 - SAP 유통기한 및 재고수량 조회] : 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.11.14
	 * @param ZMM_SELL_BY_DATE_IMPORT zmm_sell_by_date_import
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportMaterialStockListData.do")
	public String reportMaterialStockListData(Model model) throws Exception {
		
		ZMM_SELL_BY_DATE_IMPORT zmm_sell_by_date_import = new ZMM_SELL_BY_DATE_IMPORT();
		
		// 현재날짜 기준 어제 날짜
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE , -1);
		
		// 업데이트일자 (필수)
		zmm_sell_by_date_import.setI_BUDAT(new java.text.SimpleDateFormat("yyyyMMdd").format(cal.getTime()));
		ZMM_SELL_BY_DATE_EXPORT tables = rfcService.getReportMaterialStockInfo(zmm_sell_by_date_import);

		HashMap<String, Object> map = null;
		List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		
		// SAP에서 가져온 정보 담기
		if(tables != null){
			
			for (ZMM_SELL_BY_DATE_ITAB itab : tables.getZmm_sell_by_date_itab()) {
				map = new HashMap<>();
				
				map.put("REPAIR_TIME2", itab.getBUDAT());	// 전기일자
				map.put("LOCATION", itab.getWERKS());		// 플랜트
				map.put("WAREHOUSE", itab.getLGORT());		// 저장위치
				map.put("MATERIAL_UID", itab.getMATNR());	// 자재코드
				map.put("MATERIAL_NAME", itab.getMAKTX());	// 자재명
				map.put("ADATE", itab.getADATE());			// 유통기한
				map.put("STOCK", itab.getMENGE());			// 수량
				map.put("UNIT", itab.getMEINS());			// 기본단위
				
				list.add(map);
				
			}                                                           
		}	
		
		model.addAttribute("list", list);
		
		return "/epms/report/materialStock/reportMaterialStockListData";
	}
	
}
