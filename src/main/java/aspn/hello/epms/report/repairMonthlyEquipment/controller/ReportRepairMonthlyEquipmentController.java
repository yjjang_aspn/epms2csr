package aspn.hello.epms.report.repairMonthlyEquipment.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.hello.com.model.ComVO;
import aspn.hello.com.service.ComService;
import aspn.hello.epms.report.repairMonthlyEquipment.model.ReportRepairMonthlyEquipmentVO;
import aspn.hello.epms.report.repairMonthlyEquipment.service.ReportRepairMonthlyEquipmentService;
import aspn.hello.epms.report.repairMonthlyLine.model.ReportRepairMonthlyLineVO;
import aspn.hello.epms.report.repairMonthlyLine.service.ReportRepairMonthlyLineService;

/**
 * [레포트-설비별 설비고장분석(월별)] Controller Class
 * @author 김영환
 * @since 2018.05.28
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.28		김영환		최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/report/repairMonthlyEquipment")
public class ReportRepairMonthlyEquipmentController extends ContSupport {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/* INSTANCE VAR */
	@Autowired
	ReportRepairMonthlyEquipmentService reportRepairMonthlyEquipmentService;
	
	@Autowired
	ComService comService;

	/**
	 * 설비별 설비 고장분석(월별) : 메인화면
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportRepairMonthlyEquipmentList.do")
	public String reportRepairMonthlyEquipmentList(ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO, Model model){
		
		try {
			reportRepairMonthlyEquipmentVO.setSubAuth(getSsSubAuth());
			reportRepairMonthlyEquipmentVO.setCompanyId(getSsCompanyId());
			List<HashMap<String, Object>> locationList = reportRepairMonthlyEquipmentService.selectLocationListOpt(reportRepairMonthlyEquipmentVO);
			
			model.addAttribute("locationList", locationList);
			
			model.addAttribute("searchDt", DateTime.getYearStr()+ "-" +DateTime.getMonth());
			
		} catch (Exception e) {
			logger.error("ReportRepairMonthlyEquipmentController.reportRepairMonthlyEquipmentList > " + e.toString());
			e.printStackTrace();
		}

		return "/epms/report/repairMonthlyEquipment/reportRepairMonthlyEquipmentList";
		
	}
	
	/**
	 * 설비별 설비 고장분석(월별) : 메인 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportRepairMonthlyEquipmentListData.do")
	public String reportRepairMonthlyEquipmentListData(ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO, Model model){
		
		try {

			ComVO comVO = new ComVO();
			comVO.setFlag("PART");
			comVO.setCOMPANY_ID(getSsCompanyId());
			
			List<HashMap<String, Object>> partList = comService.selectComCodeList(comVO);
			model.addAttribute("partList", partList);
			
			reportRepairMonthlyEquipmentVO.setPARTLIST(partList);
			reportRepairMonthlyEquipmentVO.setSubAuth(getSsSubAuth());
			reportRepairMonthlyEquipmentVO.setCompanyId(getSsCompanyId());
			
			List<HashMap<String, Object>> list= reportRepairMonthlyEquipmentService.selectRepairMonthlyEquipmentList(reportRepairMonthlyEquipmentVO);
			model.addAttribute("list", list);
			
 		} catch (Exception e) {
			logger.error("ReportRepairMonthlyEquipmentController.reportRepairMonthlyEquipmentListData > " + e.toString());
			e.printStackTrace();
		}
		
		return "/epms/report/repairMonthlyEquipment/reportRepairMonthlyEquipmentListData";
	}
	
	/**
	 * 설비별 설비 고장분석(월별) : 메인 그리드 레이아웃
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportRepairMonthlyEquipmentListLayout.do")
	public String reportRepairMonthlyEquipmentListLayout(ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO, Model model){
		
		try{
			
			if(!"".equals(reportRepairMonthlyEquipmentVO.getSearchDt())){
				
				//설정 기간 조회
				reportRepairMonthlyEquipmentVO.setSearchDt(reportRepairMonthlyEquipmentVO.getSearchDt().replace("-", "") + "01");
				HashMap<String, Object> date  = reportRepairMonthlyEquipmentService.selectAnalysisSetDate(reportRepairMonthlyEquipmentVO);
				model.addAttribute("date", date);
			}
			
			ComVO comVO = new ComVO();
			comVO.setFlag("PART");
			comVO.setCOMPANY_ID(getSsCompanyId());
			
			List<HashMap<String, Object>> partList = comService.selectComCodeList(comVO);
			model.addAttribute("partList", partList);
			
		} catch (Exception e) {
			logger.error("ReportRepairMonthlyEquipmentController.reportRepairMonthlyEquipmentListLayout > " + e.toString());
			e.printStackTrace();
		}
			
		return "/epms/report/repairMonthlyEquipment/reportRepairMonthlyEquipmentListLayout";
	}
	
}
