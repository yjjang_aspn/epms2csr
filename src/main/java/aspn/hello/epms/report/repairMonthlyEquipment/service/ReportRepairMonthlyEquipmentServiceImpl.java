package aspn.hello.epms.report.repairMonthlyEquipment.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.hello.epms.report.repairMonthlyEquipment.mapper.ReportRepairMonthlyEquipmentMapper;
import aspn.hello.epms.report.repairMonthlyEquipment.model.ReportRepairMonthlyEquipmentVO;

/**
 * [레포트-설비별 설비고장분석(월별)] Business Implement Class
 * @author 김영환
 * @since 2018.05.28
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.28		김영환		최초 생성
 *
 * </pre>
 */


@Service
public class ReportRepairMonthlyEquipmentServiceImpl implements ReportRepairMonthlyEquipmentService {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	ReportRepairMonthlyEquipmentMapper reportRepairMonthlyEquipmentMapper;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 설비별 설비고장 분석(월별) : 권한별 소속공장 목록
	 *
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectLocationListOpt(ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO) {
		return reportRepairMonthlyEquipmentMapper.selectLocationListOpt(reportRepairMonthlyEquipmentVO);
	}

	/**
	 * 설비별 설비고장 분석(월별) : 메인 레이아웃 기준월/전월/전년동월 날짜 출력
	 *
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> selectAnalysisSetDate(ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO) {
		return reportRepairMonthlyEquipmentMapper.selectAnalysisSetDate(reportRepairMonthlyEquipmentVO);
	}

	/**
	 * 설비별 설비고장 분석(월별) : 메인 그리드 데이터 라인별 (고장건만 조회)
	 *
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairMonthlyEquipmentListOpt(ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO) {
		return reportRepairMonthlyEquipmentMapper.selectRepairMonthlyEquipmentListOpt(reportRepairMonthlyEquipmentVO);
	}

	/**
	 * 설비별 설비고장 분석(월별) : 메인 그리드 데이터 (전체 조회)
	 *
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override 
	public List<HashMap<String, Object>> selectRepairMonthlyEquipmentList(ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO) {
		return reportRepairMonthlyEquipmentMapper.selectRepairMonthlyEquipmentList(reportRepairMonthlyEquipmentVO);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
