package aspn.hello.epms.report.repairMonthlyEquipment.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.report.repairMonthlyEquipment.model.ReportRepairMonthlyEquipmentVO;

/**
 * [레포트-설비별 설비 고장분석(월별)] Service Class
 * @author 김영환
 * @since 2018.05.28
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.28		김영환		최초 생성
 *
 * </pre>
 */

public interface ReportRepairMonthlyEquipmentService {

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 설비고장 분석(월별) : 권한별 소속공장 목록
	 *
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectLocationListOpt(ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO);

	/**
	 * 설비고장 분석(월별) : 메인 레이아웃 기준월/전월/전년동월 날짜 출력
	 *
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	HashMap<String, Object> selectAnalysisSetDate(ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO);

	/**
	 * 설비별 설비고장 분석(월별) : 메인 그리드 데이터 (고장건만 조회)
	 *
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportRepairMonthlyEquipmentVO reportRepairMonthlyVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectRepairMonthlyEquipmentListOpt(ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO);

	/**
	 * 설비별 설비고장 분석(월별) : 메인 그리드 데이터 (전체 조회)
	 *
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportRepairMonthlyEquipmentVO reportRepairMonthlyVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectRepairMonthlyEquipmentList(ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO);
	

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
