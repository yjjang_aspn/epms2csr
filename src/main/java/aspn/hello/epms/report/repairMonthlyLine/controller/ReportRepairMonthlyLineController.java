package aspn.hello.epms.report.repairMonthlyLine.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.hello.com.model.ComVO;
import aspn.hello.com.service.ComService;
import aspn.hello.epms.report.repairMonthlyLine.model.ReportRepairMonthlyLineVO;
import aspn.hello.epms.report.repairMonthlyLine.service.ReportRepairMonthlyLineService;

/**
 * [레포트-설비고장분석(월별)] Controller Class
 * @author 김영환
 * @since 2018.05.28
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.28		김영환		최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/report/repairMonthlyLine")
public class ReportRepairMonthlyLineController extends ContSupport {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/* INSTANCE VAR */
	@Autowired
	ReportRepairMonthlyLineService reportRepairMonthlyLineService;
	
	@Autowired
	ComService comService;

	/**
	 * 라인별 설비 고장분석(월별) : 메인화면
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param ReportRepairMonthlyLineVO reportRepairMonthlyLineVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportRepairMonthlyLineList.do")
	public String reportRepairMonthlyLineList(ReportRepairMonthlyLineVO reportRepairMonthlyLineVO, Model model){
		
		try {
			reportRepairMonthlyLineVO.setSubAuth(getSsSubAuth());
			reportRepairMonthlyLineVO.setCompanyId(getSsCompanyId());
			List<HashMap<String, Object>> locationList = reportRepairMonthlyLineService.selectLocationListOpt(reportRepairMonthlyLineVO);
			
			model.addAttribute("locationList", locationList);
			model.addAttribute("searchDt", DateTime.getYearStr()+ "-" +DateTime.getMonth());
			
		} catch (Exception e) {
			logger.error("ReportRepairMonthlyLineController.reportRepairMonthlyLineList > " + e.toString());
			e.printStackTrace();
		}

		return "/epms/report/repairMonthlyLine/reportRepairMonthlyLineList";
		
	}
	
	/**
	 * 라인별 설비 고장분석(월별) : 메인 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param ReportRepairMonthlyLineVO reportRepairMonthlyLineVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportRepairMonthlyLineListData.do")
	public String reportRepairMonthlyLineListData(ReportRepairMonthlyLineVO reportRepairMonthlyLineVO, Model model){
		
		try {
			
			ComVO comVO = new ComVO();
			comVO.setFlag("PART");
			comVO.setCOMPANY_ID(getSsCompanyId());
			
			List<HashMap<String, Object>> partList = comService.selectComCodeList(comVO);
			model.addAttribute("partList", partList);
			
			reportRepairMonthlyLineVO.setPARTLIST(partList);
			reportRepairMonthlyLineVO.setSubAuth(getSsSubAuth());
			reportRepairMonthlyLineVO.setCompanyId(getSsCompanyId());
			
			List<HashMap<String, Object>> list= reportRepairMonthlyLineService.selectRepairMonthlyLineList(reportRepairMonthlyLineVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			logger.error("ReportRepairMonthlyLineController.reportRepairMonthlyLineListData > " + e.toString());
			e.printStackTrace();
		}
		
		return "/epms/report/repairMonthlyLine/reportRepairMonthlyLineListData";
	}
	
	/**
	 * 라인별 설비 고장분석(월별) : 메인 그리드 레이아웃
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param ReportRepairMonthlyLineVO reportRepairMonthlyLineVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportRepairMonthlyLineListLayout.do")
	public String reportRepairMonthlyLineListLayout(ReportRepairMonthlyLineVO reportRepairMonthlyLineVO, Model model){
		
		try{
			
			if(!"".equals(reportRepairMonthlyLineVO.getSearchDt())){
				
				//설정 기간 조회
				reportRepairMonthlyLineVO.setSearchDt(reportRepairMonthlyLineVO.getSearchDt().replace("-", "") + "01");
				HashMap<String, Object> date  = reportRepairMonthlyLineService.selectAnalysisSetDate(reportRepairMonthlyLineVO);
				model.addAttribute("date", date);
			}
			
			ComVO comVO = new ComVO();
			comVO.setFlag("PART");
			comVO.setCOMPANY_ID(getSsCompanyId());
			
			List<HashMap<String, Object>> partList = comService.selectComCodeList(comVO);
			model.addAttribute("partList", partList);
			
		} catch (Exception e) {
			logger.error("ReportRepairMonthlyLineController.reportRepairMonthlyLineListLayout > " + e.toString());
			e.printStackTrace();
		}
			
		return "/epms/report/repairMonthlyLine/reportRepairMonthlyLineListLayout";
	}
	
	/**
	 * 라인별 설비 고장분석(월별) : 고장 상세현황 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param ReportRepairMonthlyLineVO reportRepairMonthlyLineVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popRepairDetailListData.do")
	public String popRepairDetailListData(ReportRepairMonthlyLineVO reportRepairMonthlyLineVO, Model model){
		
		try {
			
			reportRepairMonthlyLineVO.setStartDt(reportRepairMonthlyLineVO.getSearchDt().replace("-", "") + "01");
			reportRepairMonthlyLineVO.setEndDt(getLastDayOfMonth(reportRepairMonthlyLineVO.getSearchDt()));
			
			reportRepairMonthlyLineVO.setSubAuth(getSsSubAuth());
			reportRepairMonthlyLineVO.setCompanyId(getSsCompanyId());
			List<HashMap<String, Object>> List = reportRepairMonthlyLineService.selectRepairDetailList(reportRepairMonthlyLineVO);
			model.addAttribute("list", List);
		} catch (Exception e) {
			logger.error("ReportRepairMonthlyLineController.popRepairDetailListData > " + e.toString());
			e.printStackTrace();
		}
		
		return "/epms/report/repairMonthlyLine/popRepairDetailListData";
	}
	
	/**
	 * 라인별 설비 고장분석(월별) : 고장 상세현황 그리드 레이아웃
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param ReportRepairMonthlyLineVO reportRepairMonthlyLineVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popRepairDetailListLayout.do")
	public String popRepairDetailListLayout(ReportRepairMonthlyLineVO reportRepairMonthlyLineVO, Model model){
		return "/epms/report/repairMonthlyLine/popRepairDetailListLayout";
	}
	
	/**
	 * 시작일 기준 월 말일 날짜 검색
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param String startDt
	 * @return String
	 * @throws Exception
	 */
	public String getLastDayOfMonth(String searchDt) {
		
		String endOfMonth = "";
		
		try {
			String startDt = searchDt.replace("-", "") + "01";
			
			SimpleDateFormat transDate = new SimpleDateFormat("yyyyMMdd");
			Date tdate = transDate.parse(startDt);
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(tdate);
			
			int endDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			endOfMonth = searchDt.replace("-", "") + String.valueOf(endDay);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return endOfMonth;
	}
	
}
