package aspn.hello.epms.report.repairMonthlyLine.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.hello.epms.report.repairMonthlyLine.mapper.ReportRepairMonthlyLineMapper;
import aspn.hello.epms.report.repairMonthlyLine.model.ReportRepairMonthlyLineVO;

/**
 * [레포트-라인별 설비고장분석(월별)] Business Implement Class
 * @author 김영환
 * @since 2018.05.28
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.28		김영환		최초 생성
 *
 * </pre>
 */


@Service
public class ReportRepairMonthlyLineServiceImpl implements ReportRepairMonthlyLineService {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	ReportRepairMonthlyLineMapper reportRepairMonthlyLineMapper;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 설비고장 분석(월별) : 권한별 소속공장 목록
	 *
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectLocationListOpt(ReportRepairMonthlyLineVO reportRepairMonthlyLineVO) {
		return reportRepairMonthlyLineMapper.selectLocationListOpt(reportRepairMonthlyLineVO);
	}

	/**
	 * 설비고장 분석(월별) : 메인 레이아웃 기준월/전월/전년동월 날짜 출력
	 *
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> selectAnalysisSetDate(ReportRepairMonthlyLineVO reportRepairMonthlyLineVO) {
		return reportRepairMonthlyLineMapper.selectAnalysisSetDate(reportRepairMonthlyLineVO);
	}

	/**
	 * 정비현황 분석(라인별/기준월) : 메인 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportRepairMonthlyEquipmentVO reportRepairMonthlyEquipmentVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override 
	public List<HashMap<String, Object>> selectRepairMonthlyLineList(ReportRepairMonthlyLineVO reportRepairMonthlyLineVO) {
		return reportRepairMonthlyLineMapper.selectRepairMonthlyLineList(reportRepairMonthlyLineVO);
	}

	/**
	 * 라인별 설비 고장분석(월별) : 고장 상세현황 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param ReportRepairMonthlyLineVO reportRepairMonthlyLineVO
	 * @return ist<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairDetailList(ReportRepairMonthlyLineVO reportRepairMonthlyLineVO) {
		return reportRepairMonthlyLineMapper.selectRepairDetailList(reportRepairMonthlyLineVO);
	}

	/**
	 * 전체 파트정보 호출
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param ReportRepairMonthlyLineVO reportRepairMonthlyLineVO
	 * @return ist<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectPartList() {
		return reportRepairMonthlyLineMapper.selectPartList();
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
