package aspn.hello.epms.report.repairMonthlyLine.model;

import java.util.HashMap;
import java.util.List;

import aspn.hello.com.model.AbstractVO;

/**
 * [레포트-설비고장 분석(월별)] VO Class
 * @author 김영환
 * @since 2018.04.24
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.24		김영환		최초 생성
 *   
 * </pre>
 */

public class ReportRepairMonthlyLineVO extends AbstractVO{
	private static final long serialVersionUID = 1L;
	
	/** EPMS_EQUIPMENT DB **/
	private String EQUIPMENT			= ""; 					  //설비 ID
	private String TREE					= ""; 					  //구조체
	
	/** Report model */
	private String LOCATION				= ""; 					  // 위치 
	private String PART					= ""; 					  // 파트
	
	/** DateSrc model */
	private String searchDt				= ""; 					  //검색 일
	
	/** Parameter model */
	private String view_type			= ""; 					  //조회 타입
	private List<HashMap<String, Object>> PARTLIST; 			  //파트코드 정보
	
	public String getEQUIPMENT() {
		return EQUIPMENT;
	}
	public void setEQUIPMENT(String eQUIPMENT) {
		EQUIPMENT = eQUIPMENT;
	}
	public String getTREE() {
		return TREE;
	}
	public void setTREE(String tREE) {
		TREE = tREE;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getPART() {
		return PART;
	}
	public void setPART(String pART) {
		PART = pART;
	}
	public String getSearchDt() {
		return searchDt;
	}
	public void setSearchDt(String searchDt) {
		this.searchDt = searchDt;
	}
	public String getView_type() {
		return view_type;
	}
	public void setView_type(String view_type) {
		this.view_type = view_type;
	}
	public List<HashMap<String, Object>> getPARTLIST() {
		return PARTLIST;
	}
	public void setPARTLIST(List<HashMap<String, Object>> pARTLIST) {
		PARTLIST = pARTLIST;
	}

}
