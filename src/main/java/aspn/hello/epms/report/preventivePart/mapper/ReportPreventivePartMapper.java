package aspn.hello.epms.report.preventivePart.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.preventive.work.model.PreventiveWorkVO;
import aspn.hello.epms.report.preventivePart.model.ReportPreventivePartVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [레포트 예방보전현황(정비원)] Mapper Class
 * 
 * @author 김영환
 * @since 2018.05.30
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.30		김영환			최초 생성
 *   
 * </pre>
 */

@Mapper("reportPreventivePartMapper")
public interface ReportPreventivePartMapper {
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 레포트 예방보전 현황(정비원) 목록
	 * @author 김영환
	 * @since 2018.05.30
	 * @param ReportPreventivePartVO reportPreventivePartVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> reportPreventivePartList(ReportPreventivePartVO reportPreventivePartVO);
}
