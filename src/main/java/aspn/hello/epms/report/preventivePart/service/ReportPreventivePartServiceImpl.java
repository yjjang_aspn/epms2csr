package aspn.hello.epms.report.preventivePart.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.hello.epms.report.preventivePart.mapper.ReportPreventivePartMapper;
import aspn.hello.epms.report.preventivePart.model.ReportPreventivePartVO;

/**
 * [레포트 예방보전현황(정비원)] ServiceImpl Class
 * 
 * @author 김영환
 * @since 2018.05.30
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.30		김영환			최초 생성
 *   
 * </pre>
 */

@Service
public class ReportPreventivePartServiceImpl implements ReportPreventivePartService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	ReportPreventivePartMapper reportPreventivePartMapper;

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 레포트 예방보전 현황(정비원) 목록
	 * @author 김영환
	 * @since 2018.05.30
	 * @param ReportPreventivePartVO reportPreventivePartVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> reportPreventivePartList(ReportPreventivePartVO reportPreventivePartVO) {
		return reportPreventivePartMapper.reportPreventivePartList(reportPreventivePartVO);
	}

}
