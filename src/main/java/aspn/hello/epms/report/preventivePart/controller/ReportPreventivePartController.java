package aspn.hello.epms.report.preventivePart.controller;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import aspn.com.common.util.ContSupport;
import aspn.hello.com.service.AttachService;
import aspn.hello.epms.report.preventiveMember.model.ReportPreventiveMemberVO;
import aspn.hello.epms.report.preventiveMember.service.ReportPreventiveMemberService;
import aspn.hello.epms.report.preventivePart.model.ReportPreventivePartVO;
import aspn.hello.epms.report.preventivePart.service.ReportPreventivePartService;

/**
 * [레포트 예방보전 현황(파트)] Controller Class
 * @author 김영환
 * @since 2018.05.30
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.30		김영환			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/report/preventivePart")
public class ReportPreventivePartController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	
	@Autowired
	ReportPreventivePartService reportPreventivePartService;
	
	@Autowired
	ReportPreventiveMemberService reportPreventiveMemberService;
	
	@Autowired
	AttachService attachService;

	/**
	 * [레포트 예방보전 현황(파트)] : 화면호출
	 *
	 * @author 김영환
	 * @since 2018.05.30
	 * @param ReportPreventivePartVO reportPreventivePartVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportPreventivePartList.do")
	public String reportPreventivePartList(ReportPreventivePartVO reportPreventivePartVO, Model model) throws Exception{
		return "/epms/report/preventivePart/reportPreventivePartList";
	}
	
	/**
	 * [레포트 예방보전 현황(파트)] : 그리드 레이아웃
	 *
	 * @author 김영환
	 * @since 2018.05.30
	 * @param ReportPreventivePartVO reportPreventivePartVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportPreventivePartListLayout.do")
	public String reportPreventivePartListLayout(ReportPreventivePartVO reportPreventivePartVO, Model model) throws Exception{
		return "/epms/report/preventivePart/reportPreventivePartListLayout";
	}
	
	/**
	 * [레포트 예방보전 현황(파트)] : 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.05.30
	 * @param ReportPreventivePartVO reportPreventivePartVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportPreventivePartListData.do")
	public String reportPreventivePartListData(ReportPreventivePartVO reportPreventivePartVO, Model model) throws Exception{
		reportPreventivePartVO.setSubAuth(getSsSubAuth());
		reportPreventivePartVO.setCompanyId(getSsCompanyId());
		
		List<HashMap<String, Object>> list = reportPreventivePartService.reportPreventivePartList(reportPreventivePartVO);
		
		model.addAttribute("list", list);
		return "/epms/report/preventivePart/reportPreventivePartListData";
	}
}
