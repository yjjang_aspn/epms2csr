package aspn.hello.epms.report.repairPeriodLine.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.main.model.MainVO;
import aspn.hello.epms.report.repairPeriodEquipment.model.ReportRepairPeriodEquipmentVO;
import aspn.hello.epms.report.repairPeriodLine.model.ReportRepairPeriodLineVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [레포트-설비고장 분석(기간)] Mapper Class
 * 
 * @author 김영환
 * @since 2018.05.28
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.28			김영환			최초 생성
 *   
 * </pre>
 */

@Mapper("reportRepairPeriodLineMapper")
public interface ReportRepairPeriodLineMapper {
	 
	/**
	* 설비고장 분석 (기간)-라인
	*
	* @author 김영환
	* @since 2018.05.28
	* @param ReportRepairPeriodLineVO reportRepairPeriodLineVO
	* @return List<HashMap<String, Object>>
	*/
	List<HashMap<String, Object>> selectReportRepairPeriodLineList(ReportRepairPeriodLineVO reportRepairPeriodLineVO);

	/**
	* 고장 상세 현황
	*
	* @author 김영환
	* @since 2018.05.28
	* @param ReportRepairPeriodLineVO reportRepairPeriodLineVO
	* @return List<HashMap<String, Object>>
	*/
	List<HashMap<String, Object>> selectTroubleDetailList(ReportRepairPeriodLineVO reportRepairPeriodLineVO);

	/**
	 * 라인별 고장유형 통계
	 * 
	* @author 김영환
	* @since 2018.07.05
	* @param ReportRepairPeriodLineVO reportRepairPeriodLineVO
	* @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairResultListOpt(ReportRepairPeriodLineVO reportRepairPeriodLineVO);

	/**
	 * 라인별 조치유형 통계
	 * @author 김영환
	 * @since 2018.07.05
	 * @param ReportRepairPeriodLineVO reportRepairPeriodLineVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairResultListOpt2(ReportRepairPeriodLineVO reportRepairPeriodLineVO);

	/**
	 * 라인별 고장유형/조치유형 팝업 데이터
	 * 
	 * @author 김영환
	 * @since 2018.07.05
	 * @param ReportRepairPeriodLineVO reportRepairPeriodLineVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectPopRepairResultListOpt(ReportRepairPeriodLineVO reportRepairPeriodLineVO);
	
}
