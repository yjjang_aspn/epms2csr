package aspn.hello.epms.report.repairPeriodLine.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.hello.com.model.ComVO;
import aspn.hello.com.service.ComService;
import aspn.hello.epms.main.model.MainVO;
import aspn.hello.epms.report.repairPeriodLine.model.ReportRepairPeriodLineVO;
import aspn.hello.epms.report.repairPeriodLine.service.ReportRepairPeriodLineService;


/**
 * [레포트 - 라인별 설비고장 분석(기간)] Controller Class
 * @author 김영환
 * @since 2018.05.28
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.28			김영환			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/report/repairPeriodLine")
public class ReportRepairPeriodLineController extends ContSupport {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/* INSTANCE VAR */
	@Autowired
	ReportRepairPeriodLineService reportRepairPeriodLineService;
	
	@Autowired
	ComService comService;
		
	/**
	 * 설비고장 분석 (기간) -  PLANT
	 * 
	 * @author 김영환
	 * @since 2018.05.28	
	 * @param ReportRepairPeriodLineVO reportRepairPeriodLineVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/reportRepairPeriodLineList.do")
	public String reportRepairPeriodLineList(ReportRepairPeriodLineVO reportRepairPeriodLineVO, Model model){
			
		model.addAttribute("startDt", DateTime.requestMmDay("1","yyyy-MM-dd"));
		model.addAttribute("endDt", DateTime.getDateString());
		
		return "/epms/report/repairPeriodLine/reportRepairPeriodLineList";
	}
	
	/**
	 *  설비고장 분석(기간) : 메인 그리드 ( 고장건만 조회 )
	 * 
	 * @author 김영환
	 * @since 2018.05.28
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportRepairPeriodLineListLayout.do")
	public String reportRepairPeriodLineListLayout(Model model) throws Exception {
		
		try{
		
			ComVO comVO = new ComVO();
			comVO.setFlag("PART");
			comVO.setCOMPANY_ID(getSsCompanyId());
			
			List<HashMap<String, Object>> partList = comService.selectComCodeList(comVO);
			model.addAttribute("partList", partList);
		
		} catch (Exception e) {
			logger.error("ReportRepairPeriodLineController.reportRepairPeriodLineListLayout > " + e.toString());
			e.printStackTrace();
		}
		
		return "/epms/report/repairPeriodLine/reportRepairPeriodLineListLayout";
	}
	
	/**
	 * 설비고장 분석 (기간): 메인 그리드 데이터
	 * 
	 * @author 김영환
	 * @since 2018.05.28	
	 * @param ReportRepairPeriodLineVO reportRepairPeriodLineVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportRepairPeriodLineListData.do")
	public String reportRepairPeriodLineListData(ReportRepairPeriodLineVO reportRepairPeriodLineVO, Model model){
		
		try {
			
			ComVO comVO = new ComVO();
			comVO.setFlag("PART");
			comVO.setCOMPANY_ID(getSsCompanyId());
			
			List<HashMap<String, Object>> partList = comService.selectComCodeList(comVO);
			model.addAttribute("partList", partList);
			
			reportRepairPeriodLineVO.setPARTLIST(comService.selectComCodeList(comVO));
			reportRepairPeriodLineVO.setSubAuth(getSsSubAuth());
			reportRepairPeriodLineVO.setCompanyId(getSsCompanyId());
			List<HashMap<String, Object>> list= reportRepairPeriodLineService.selectReportRepairPeriodLineList(reportRepairPeriodLineVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ReportRepairPeriodLineController.reportRepairPeriodLineListData Error !" + e.toString());
		}
		
		return "/epms/report/repairPeriodLine/reportRepairPeriodLineListData";
				
	}
	
	/**
	 * 고장 상세 현황  Grid Layout
	 * 
	 * @author 김영환
	 * @since 2018.05.28
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/reportTroubleDetailLineListLayout.do")
	public String reportTroubleDetailLineListLayout() throws Exception{
		
		return "/epms/report/repairPeriodLine/reportTroubleDetailLineListLayout";
		
	}
	
	/**
	 * 고장 상세 현황  Grid Data
	 * 
	 * @author 김영환
	 * @since 2018.05.28	
	 * @param ReportRepairPeriodLineVO reportRepairPeriodLineVO
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportTroubleDetailLineListData.do")
	public String reportTroubleDetailLineListData(ReportRepairPeriodLineVO reportRepairPeriodLineVO, Model model){
		
		try {
			reportRepairPeriodLineVO.setSubAuth(getSsSubAuth());
			reportRepairPeriodLineVO.setCompanyId(getSsCompanyId());
			//정비현황 목록
			List<HashMap<String, Object>> list = reportRepairPeriodLineService.selectTroubleDetailList(reportRepairPeriodLineVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ReportRepairPeriodLineController.reportTroubleDetailLineListData Error !" + e.toString());
		}
		return "/epms/report/repairPeriodLine/reportTroubleDetailLineListData";
											  
	}
	
	/**
	 * 라인별 고장유형 / 조치유형 그래프 데이터
	 * 
	 * @author 김영환
	 * @since 2018.07.05	
	 * @param ReportRepairPeriodLineVO reportRepairPeriodLineVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectRepairResultInfo.do")
	@ResponseBody
	public Map<String, Object> selectRepairResultInfo(ReportRepairPeriodLineVO reportRepairPeriodLineVO) throws Exception {
		
		reportRepairPeriodLineVO.setSubAuth(getSsSubAuth());
		reportRepairPeriodLineVO.setCompanyId(getSsCompanyId());
		
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			// 고장유형별 정비건
			List<HashMap<String, Object>> trouble_type1 = reportRepairPeriodLineService.selectRepairResultListOpt(reportRepairPeriodLineVO);
			resultMap.put("TROUBLE_TYPE1", trouble_type1);
			
			// 조치유형별 정비건
			List<HashMap<String, Object>> trouble_type2 = reportRepairPeriodLineService.selectRepairResultListOpt2(reportRepairPeriodLineVO);
			resultMap.put("TROUBLE_TYPE2", trouble_type2);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ReportRepairPeriodLineController.selectRepairResultInfo Error !" + e.toString());
		}
		
		return resultMap;
	}
	
	/**
	 * 고장유형 / 조치유형 팝업 데이터
	 *
	 * @author 김영환
	 * @since 2018.06.19
	 * @param RepairResultVO repairResultVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popRepairResultListOptData.do")
	public String popRepairResultListOptData(ReportRepairPeriodLineVO reportRepairPeriodLineVO, Model model) throws Exception{
		try {
			
			reportRepairPeriodLineVO.setSubAuth(getSsSubAuth());
			reportRepairPeriodLineVO.setCompanyId(getSsCompanyId());
//			reportRepairPeriodLineVO.setSsLocation(getSsLocation());
			
			//정비실적현황 목록
			List<HashMap<String, Object>> list= reportRepairPeriodLineService.selectPopRepairResultListOpt(reportRepairPeriodLineVO);
			model.addAttribute("list", list);
			model.addAttribute("flag", "MAIN");
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ReportRepairPeriodLineController.popRepairResultListOptData Error !" + e.toString());
		}
		return "/epms/repair/result/repairResultListData";
	}
		
}
