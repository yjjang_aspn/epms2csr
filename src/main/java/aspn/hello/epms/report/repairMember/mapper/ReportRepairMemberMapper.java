package aspn.hello.epms.report.repairMember.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.report.repairMember.model.ReportRepairMemberVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * 
 * [레포트 - 정비원별 작업 시간] Controller Class
 * @author 김영환
 * @since 2019.03.29
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2019.03.29			김영환			최초 생성
 *
 * </pre>
 */

@Mapper("ReportRepairMemberMapper")
public interface ReportRepairMemberMapper {
	 
	/**
	* 레포트 - 정비원별 작업 시간
	*
	* @author 김영환
	* @since 2019.03.29
	* @param ReportRepairMemberVO reportRepairMemberVO
	* @return List<HashMap<String, Object>>
	*/
	List<HashMap<String, Object>> reportRepairMemberList(ReportRepairMemberVO reportRepairMemberVO);

	/**
	 * 레포트 - 정비원별 정비실적 조회
	 * @author 김영환
	 * @since 2019.03.29
	 * @param ReportRepairMemberVO reportRepairMemberVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairResultList(ReportRepairMemberVO reportRepairMemberVO);
	
}
