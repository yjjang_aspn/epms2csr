package aspn.hello.epms.report.repairMember.model;

import java.util.ArrayList;
import java.util.List;

import aspn.hello.com.model.AbstractVO;

/**
 * 
 * [레포트 - 정비원별 작업 시간] Controller Class
 * @author 김영환
 * @since 2019.03.29
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2019.03.29			김영환			최초 생성
 *
 * </pre>
 */

public class ReportRepairMemberVO extends AbstractVO{
	
	/** EQUIPMENT DB model  */
	private String LOCATION						= ""; //공장위치
	/** WORK_LOG DB model  */
	private String USER_ID						= ""; //정비원ID
	
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getUSER_ID() {
		return USER_ID;
	}
	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}
	
	
	
}
