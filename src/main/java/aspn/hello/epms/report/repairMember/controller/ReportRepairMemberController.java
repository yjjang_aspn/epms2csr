package aspn.hello.epms.report.repairMember.controller;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import aspn.com.common.util.ContSupport;
import aspn.hello.epms.report.repairMember.model.ReportRepairMemberVO;
import aspn.hello.epms.report.repairMember.service.ReportRepairMemberService;


/**
 * 
 * [레포트 - 정비원별 작업 시간] Controller Class
 * @author 김영환
 * @since 2019.03.29
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2019.03.29			김영환			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/report/repairMember")
public class ReportRepairMemberController extends ContSupport {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/* INSTANCE VAR */
	@Autowired
	ReportRepairMemberService reportRepairMemberService;
	
	/**
	 * [레포트 - 정비원별 작업 시간] : 화면호출
	 *
	 * @author 김영환
	 * @since 2019.03.29
	 * @param ReportRepairMemberVO reportRepairMemberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportRepairMemberList.do")
	public String reportRepairMemberList(ReportRepairMemberVO reportRepairMemberVO, Model model) throws Exception{
		return "/epms/report/repairMember/reportRepairMemberList";
	}
	
	/**
	 * [레포트 - 정비원별 작업 시간] : 그리드 레이아웃
	 *
	 * @author 김영환
	 * @since 2019.03.29
	 * @param ReportRepairMemberVO reportRepairMemberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportRepairMemberListLayout.do")
	public String reportRepairMemberListLayout(ReportRepairMemberVO reportRepairMemberVO, Model model) throws Exception{
		return "/epms/report/repairMember/reportRepairMemberListLayout";
	}
	
	/**
	 * [레포트 - 정비원별 작업 시간] : 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2019.03.29
	 * @param ReportRepairMemberVO reportRepairMemberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportRepairMemberListData.do")
	public String reportRepairMemberListData(ReportRepairMemberVO reportRepairMemberVO, Model model) throws Exception{
		reportRepairMemberVO.setCompanyId(getSsCompanyId());
		List<HashMap<String, Object>> list = reportRepairMemberService.reportRepairMemberList(reportRepairMemberVO);
		model.addAttribute("list", list);
		return "/epms/report/repairMember/reportRepairMemberListData";
	}
	
	/**
	 * 라인별 설비 고장분석(월별) : 고장 상세현황 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2019.03.29
	 * @param ReportRepairMemberVO reportRepairMemberVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popReportRepairResultListData.do")
	public String popReportRepairResultListData(ReportRepairMemberVO reportRepairMemberVO, Model model){
		try {
			reportRepairMemberVO.setCompanyId(getSsCompanyId());
			List<HashMap<String, Object>> List = reportRepairMemberService.selectRepairResultList(reportRepairMemberVO);
			model.addAttribute("list", List);
		} catch (Exception e) {
			logger.error("RepairResultController.popReportRepairResultListData > " + e.toString());
			e.printStackTrace();
		}
		return "/epms/report/repairMember/popReportRepairResultListData";
	}
	
	/**
	 * 라인별 설비 고장분석(월별) : 고장 상세현황 그리드 레이아웃
	 *
	 * @author 김영환
	 * @since 2019.03.29
	 * @param ReportRepairMemberVO reportRepairMemberVO
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popRepairDetailListLayout.do")
	public String popRepairDetailListLayout(ReportRepairMemberVO reportRepairMemberVO){
		return "/epms/report/repairMember/popRepairDetailListLayout";
	}
		
}
