package aspn.hello.epms.report.repairPeriodEquipment.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.hello.epms.report.repairPeriodEquipment.mapper.ReportRepairPeriodEquipmentMapper;
import aspn.hello.epms.report.repairPeriodEquipment.model.ReportRepairPeriodEquipmentVO;
import aspn.hello.epms.report.repairPeriodLine.mapper.ReportRepairPeriodLineMapper;
import aspn.hello.epms.report.repairPeriodLine.model.ReportRepairPeriodLineVO;

/**
 * [레포트-설비고장 분석(기간)] Business Implement Class
 * 
 * @author 김영환
 * @since 2018.05.28
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.28			김영환			최초 생성
 *   
 * </pre>
 */

@Service
public class ReportRepairPeriodEquipmentServiceImpl implements ReportRepairPeriodEquipmentService {
	
	@Autowired
	ReportRepairPeriodEquipmentMapper reportRepairPeriodEquipmentMapper;
	
	/**
	* 정비현황 분석(설비별/상세) 조회  
	*
	* @author 김영환
	* @since 2018.05.28
	* @param ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO
	* @return List<HashMap<String, Object>>
	*/
	@Override
	public List<HashMap<String, Object>> selectRepairPeriodEquipmentList(ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO) {
		return reportRepairPeriodEquipmentMapper.selectRepairPeriodEquipmentList(reportRepairPeriodEquipmentVO);
	}
	
	/**
	 * 고장 상세 현황
	 *
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectTroubleDetailList(ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO) {
		return reportRepairPeriodEquipmentMapper.selectTroubleDetailList(reportRepairPeriodEquipmentVO);
	}

	/**
	 * 설비별 고장유형 통계
	 * 
	 * @author 김영환
	 * @since 2018.07.05
	 * @param ReportPeriodVO reportPeriodVo
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairResultListOpt(ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO) {
		return reportRepairPeriodEquipmentMapper.selectRepairResultListOpt(reportRepairPeriodEquipmentVO);
	}

	/**
	 * 설비별 정비실적 통계
	 * 
	 * @author 김영환
	 * @since 2018.07.05
	 * @param ReportPeriodVO reportPeriodVo
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairResultListOpt2(ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO) {
		return reportRepairPeriodEquipmentMapper.selectRepairResultListOpt2(reportRepairPeriodEquipmentVO);
	}
		
}
