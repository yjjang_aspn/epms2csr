package aspn.hello.epms.report.repairPeriodEquipment.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.report.repairPeriodEquipment.model.ReportRepairPeriodEquipmentVO;
import aspn.hello.epms.report.repairPeriodLine.model.ReportRepairPeriodLineVO;

/**
 * [레포트-라이별 고장 분석(기간)]  Service Class
 * 
 * @author 김영환
 * @since 2018.05.30
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.30		김영환			최초 생성
 *   
 * </pre>
 */

public interface ReportRepairPeriodEquipmentService {
	
	/**
	* 정비현황 분석(설비별/상세) 조회   
	*
	* @author 김영환
	* @since 2018.05.28
	* @param ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO
	* @return List<HashMap<String, Object>>
	*/
	List<HashMap<String, Object>> selectRepairPeriodEquipmentList(ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO);
	
	/**
	* 정비현황 목록
	*
	* @author 김영환
	* @since 2018.05.28
	* @param ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO
	* @return List<HashMap<String, Object>>
	*/
	List<HashMap<String, Object>> selectTroubleDetailList(ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO);

	/**
	 * 설비별 고장유형 통계
	 * 
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairResultListOpt(ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO);

	/**
	 * 설비별 조치유형 통계
	 * 
	 * @author 김영환
	 * @since 2018.05.28
	 * @param ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairResultListOpt2(ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO);
	
}
