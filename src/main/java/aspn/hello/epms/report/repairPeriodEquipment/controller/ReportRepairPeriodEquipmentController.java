package aspn.hello.epms.report.repairPeriodEquipment.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.hello.com.model.ComVO;
import aspn.hello.com.service.ComService;
import aspn.hello.epms.report.repairPeriodEquipment.model.ReportRepairPeriodEquipmentVO;
import aspn.hello.epms.report.repairPeriodEquipment.service.ReportRepairPeriodEquipmentService;
import aspn.hello.epms.report.repairPeriodLine.model.ReportRepairPeriodLineVO;


/**
 * [레포트 - 설비별 고장 분석(기간)] Controller Class
 * @author 김영환
 * @since 2018.05.28
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.28		김영환			최초 생성
 *
 * </pre>
 */

@Controller      
@RequestMapping("/epms/report/repairPeriodEquipment")
public class ReportRepairPeriodEquipmentController extends ContSupport {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/* INSTANCE VAR */
	@Autowired
	ReportRepairPeriodEquipmentService reportRepairPeriodEquipmentService; 
	
	@Autowired
	ComService comService;
		
	/**
	 * 설비고장 분석 (기간) -  PLANT
	 * 
	 * @author 김영환
	 * @since 2018.05.28	
	 * @param ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/reportRepairPeriodEquipmentList.do")
	public String reportRepairPeriodEquipmentList(Model model){
		
		try {

			model.addAttribute("startDt", DateTime.requestMmDay("1","yyyy-MM-dd"));
			model.addAttribute("endDt", DateTime.getDateString());
			
		} catch (Exception e) {
			logger.error("ReportRepairPeriodEquipmentController.reportRepairPeriodLineList > " + e.toString());
			e.printStackTrace();
		}
		
		return "/epms/report/repairPeriodEquipment/reportRepairPeriodEquipmentList";

	}
	
	/**
	 *  설비고장 분석(기간) : 메인 그리드 ( 고장건만 조회 )
	 * 
	 * @author 김영환
	 * @since 2018.05.28	
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportRepairPeriodEquipmentListLayout.do")
	public String reportRepairPeriodEquipmentListLayout(Model model) throws Exception {
		
		try{
			
			ComVO comVO = new ComVO();
			comVO.setFlag("PART");
			comVO.setCOMPANY_ID(getSsCompanyId());
			
			List<HashMap<String, Object>> partList = comService.selectComCodeList(comVO);
			model.addAttribute("partList", partList);
			
		} catch (Exception e) {
			logger.error("ReportRepairPeriodEquipmentController.reportRepairPeriodEquipmentListLayout > " + e.toString());
			e.printStackTrace();
		}
		
		return "/epms/report/repairPeriodEquipment/reportRepairPeriodEquipmentListLayout";
	}
	
	/**
	 * 정비현황분석 설비별 상세 (기간별)
	 * 
	 * @author 김영환
	 * @since 2018.05.28	
	 * @param ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportRepairPeriodEquipmentListData.do")
	public String reportRepairPeriodEquipmentListData(ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO, Model model){
		
		try {
			
			ComVO comVO = new ComVO();
			comVO.setFlag("PART");
			comVO.setCOMPANY_ID(getSsCompanyId());

			List<HashMap<String, Object>> partList = comService.selectComCodeList(comVO);
			model.addAttribute("partList", partList);
			
			reportRepairPeriodEquipmentVO.setPARTLIST(comService.selectComCodeList(comVO));
			reportRepairPeriodEquipmentVO.setSubAuth(getSsSubAuth());
			reportRepairPeriodEquipmentVO.setCompanyId(getSsCompanyId());
			
			List<HashMap<String, Object>> list= reportRepairPeriodEquipmentService.selectRepairPeriodEquipmentList(reportRepairPeriodEquipmentVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ReportRepairPeriodEquipmentController.reportRepairPeriodLineListData Error !" + e.toString());
		}
		
		return "/epms/report/repairPeriodEquipment/reportRepairPeriodEquipmentListData";
				
	}
	
	/**
	 * 고장 상세 현황  Grid Layout
	 * 
	 * @author 김영환
	 * @since 2018.05.28	
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/reportTroubleDetailEquipmentListLayout.do")
	public String reportTroubleDetailEquipmentListLayout() throws Exception{
		
		return "/epms/report/repairPeriodEquipment/reportTroubleDetailEquipmentListLayout";
		
	}
	
	/**
	 * 고장 상세 현황  Grid Data
	 * 
	 * @author 김영환
	 * @since 2018.05.28	
	 * @param ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportTroubleDetailEquipmentListData.do")
	public String reportTroubleDetailEquipmentListData(ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO, Model model){
		
		try {
			reportRepairPeriodEquipmentVO.setCompanyId(getSsCompanyId());
			
			//정비현황 목록
			List<HashMap<String, Object>> list = reportRepairPeriodEquipmentService.selectTroubleDetailList(reportRepairPeriodEquipmentVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ReportRepairPeriodEquipmentController.reportTroubleDetailEquipmentListData Error !" + e.toString());
		}
		return "/epms/report/repairPeriodEquipment/reportTroubleDetailEquipmentListData";
											  
	}
	
	/**
	 * 설비별 고장유형 / 조치유형 그래프 데이터
	 * 
	 * @author 김영환
	 * @since 2018.05.28	
	 * @param ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO
	 * @return  Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectRepairResultInfo.do")
	@ResponseBody
	public Map<String, Object> selectRepairResultInfo(ReportRepairPeriodEquipmentVO reportRepairPeriodEquipmentVO) throws Exception {
		
		reportRepairPeriodEquipmentVO.setSubAuth(getSsSubAuth());
		reportRepairPeriodEquipmentVO.setCompanyId(getSsCompanyId());
		
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			// 고장유형 별 정비건
			List<HashMap<String, Object>> trouble_type1 = reportRepairPeriodEquipmentService.selectRepairResultListOpt(reportRepairPeriodEquipmentVO);
			resultMap.put("TROUBLE_TYPE1", trouble_type1);
			
			// 조치유형 별 정비건
			List<HashMap<String, Object>> trouble_type2 = reportRepairPeriodEquipmentService.selectRepairResultListOpt2(reportRepairPeriodEquipmentVO);
			resultMap.put("TROUBLE_TYPE2", trouble_type2);
			
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
		}
		
		return resultMap;
	}
		
}
