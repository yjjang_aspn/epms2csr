package aspn.hello.epms.report.repairPeriodEquipment.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import aspn.hello.com.model.AbstractVO;
import aspn.hello.epms.report.repairPeriodLine.model.ReportRepairPeriodLineVO;

/**
 * [레포트-설비고장 분석(기간)] VO Class
 * 
 * @author 김영환
 * @since 2018.05.28
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.28			김영환			최초 생성
 *   
 * </pre>
 */

public class ReportRepairPeriodEquipmentVO extends AbstractVO{
	
	private static final long serialVersionUID = 1L;
	
	/** EPMS_EQUIPMENT DB **/
	private String TREE									= ""; //구조체
	private String EQUIPMENT							= ""; //설비 ID
	private String LOCATION								= ""; //위치
	
	/** EPMS_REPAIR_REQUEST DB **/
	private String PART									= ""; //파트
	private List<HashMap<String, Object>> PARTLIST; 	 	  //파트코드 정보
	
	/** parameter model */
	
	public String getTREE() {
		return TREE;
	}
	public void setTREE(String tREE) {
		TREE = tREE;
	}
	public String getEQUIPMENT() {
		return EQUIPMENT;
	}
	public void setEQUIPMENT(String eQUIPMENT) {
		EQUIPMENT = eQUIPMENT;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getPART() {
		return PART;
	}
	public void setPART(String pART) {
		PART = pART;
	}
	public List<HashMap<String, Object>> getPARTLIST() {
		return PARTLIST;
	}
	public void setPARTLIST(List<HashMap<String, Object>> pARTLIST) {
		PARTLIST = pARTLIST;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
