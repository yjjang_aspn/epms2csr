package aspn.hello.epms.report.repairMtbfMttrLine.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.report.repairMtbfMttrLine.model.ReportRepairMtbfMttrLineVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [레포트-MTBF/MTTR 분석(라인)] Mapper Class
 * @author 김영환
 * @since 2018.06.07
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.06.07			김영환			최초 생성
 *
 * </pre>
 */

@Mapper("reportRepairMtbfMttrLineMapper")
public interface ReportRepairMtbfMttrLineMapper {

	List<HashMap<String, Object>> selectReportRepairMtbfMttrLineList(ReportRepairMtbfMttrLineVO reportRepairMtbfMttrLineVO);
}
