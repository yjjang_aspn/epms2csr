package aspn.hello.epms.report.repairMtbfMttrLine.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.TreeGridUtil;
import aspn.hello.epms.report.preventiveMember.service.ReportPreventiveMemberService;
import aspn.hello.epms.report.repairMtbfMttrLine.model.ReportRepairMtbfMttrLineVO;
import aspn.hello.epms.report.repairMtbfMttrLine.service.ReportRepairMtbfMttrLineService;


/**
 * [레포트-MTBF/MTTR 분석(라인)] Controller Class
 * @author 김영환
 * @since 2018.06.07
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.06.07			김영환			최초 생성
 *
 * </pre>
 */

@Controller      
@RequestMapping("/epms/report/repairMtbfMttrLine")
public class ReportRepairMtbfMttrLineController extends ContSupport {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/* INSTANCE VAR */
	@Autowired
	ReportRepairMtbfMttrLineService reportRepairMtbfMttrLineService; 
	@Autowired
	ReportPreventiveMemberService reportPreventiveMemberService; 
	
	/**
	 * [레포트-MTBF/MTTR 분석(라인)] : 메인화면
	 *
	 * @author 김영환
	 * @since 2018.06.07
	 * @param ReportRepairMtbfMttrLineVO reportRepairMtbfMttrLineVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportRepairMtbfMttrLineList.do")
	public String reportRepairMtbfMttrLineList(ReportRepairMtbfMttrLineVO reportRepairMtbfMttrLineVO, Model model){
		
		try {
			model.addAttribute("searchDt", DateTime.getYearStr()+ "-" +DateTime.getMonth());
		} catch (Exception e) {
			logger.error("ReportRepairMtbfMttrLineController.reportRepairMtbfMttrLineList > " + e.toString());
			e.printStackTrace();
		}

		return "/epms/report/repairMtbfMttrLine/reportRepairMtbfMttrLineList";
		
	}
	
	/**
	 * [레포트-MTBF/MTTR 분석(라인)] : 메인 그리드 레이아웃
	 *
	 * @author 김영환
	 * @since 2018.06.07
	 * @param ReportRepairMtbfMttrLineVO reportRepairMtbfMttrLineVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportRepairMtbfMttrLineListLayout.do")
	public String repairMtbfMttrLineListLayout(ReportRepairMtbfMttrLineVO reportRepairMtbfMttrLineVO, Model model){
		SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM");
		try{
			String startDt = reportRepairMtbfMttrLineVO.getStartDt();
		    Date date = fm.parse(startDt);
		    
		    Calendar cal = new GregorianCalendar(Locale.KOREA);
		    cal.setTime(date);
		    
		    String strDate = fm.format(cal.getTime());
		    
		    List<String> STDYYYY_LIST = new ArrayList();
		    
		    STDYYYY_LIST.add(strDate);
		    for (int i = 0; i < reportRepairMtbfMttrLineVO.getDiffMonth(); i++) {
		    	cal.add(Calendar.MONTH, 1);
		    	strDate = fm.format(cal.getTime());
		    	STDYYYY_LIST.add(strDate);
		    }
		    model.addAttribute("list", STDYYYY_LIST);
			
		} catch (Exception e) {
			logger.error("RepairMtbfMttrLineController.repairMtbfMttrLineListLayout > " + e.toString());
			e.printStackTrace();
		}
			
		return "/epms/report/repairMtbfMttrLine/reportRepairMtbfMttrLineListLayout";
	}
	
	/**
	 * [레포트-MTBF/MTTR 분석(라인)] : 메인 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.06.07
	 * @param ReportRepairMtbfMttrLineVO reportRepairMtbfMttrLineVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */						  
	@RequestMapping(value = "/reportRepairMtbfMttrLineListData.do")
	public String repairMtbfMttrLineListData(ReportRepairMtbfMttrLineVO reportRepairMtbfMttrLineVO, Model model) throws Exception{
		SimpleDateFormat fm = new SimpleDateFormat("yyyyMM");
		List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>> ();
		try {
			String startDt = reportRepairMtbfMttrLineVO.getStartDt().replace("-", "");
		    Date date = fm.parse(startDt);
		    
		    Calendar cal = new GregorianCalendar(Locale.KOREA);
		    cal.setTime(date);
		    
		    String strDate = fm.format(cal.getTime());
		    
		    List<String> STDYYYY_LIST = new ArrayList();
		    
		    STDYYYY_LIST.add(strDate);
		    for (int i = 0; i < reportRepairMtbfMttrLineVO.getDiffMonth(); i++) {
		    	cal.add(Calendar.MONTH, 1);
		    	strDate = fm.format(cal.getTime());
		    	STDYYYY_LIST.add(strDate);
		    }
		    reportRepairMtbfMttrLineVO.setSTDYYYY_LIST(STDYYYY_LIST);
		    reportRepairMtbfMttrLineVO.setSubAuth(getSsSubAuth());
		    reportRepairMtbfMttrLineVO.setCompanyId(getSsCompanyId());
		    list = reportRepairMtbfMttrLineService.selectReportRepairMtbfMttrLineList(reportRepairMtbfMttrLineVO);
		    
		    model.addAttribute("list", list);
		    model.addAttribute("list2", STDYYYY_LIST);
		    
 		} catch (Exception e) {
			logger.error("RepairMtbfMttrLineController.repairMtbfMttrLineListData > " + e.toString());
			e.printStackTrace();
		}
		
		return "/epms/report/repairMtbfMttrLine/reportRepairMtbfMttrLineListData";
	}
	
}
