package aspn.hello.epms.report.repairMtbfMttrLine.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.hello.epms.report.repairMtbfMttrLine.mapper.ReportRepairMtbfMttrLineMapper;
import aspn.hello.epms.report.repairMtbfMttrLine.model.ReportRepairMtbfMttrLineVO;

/**
 * [레포트-MTBF/MTTR 분석(라인)] ServiceImpl Class
 * @author 김영환
 * @since 2018.06.07
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.06.07			김영환			최초 생성
 *
 * </pre>
 */

@Service
public class ReportRepairMtbfMttrLineServiceImpl implements ReportRepairMtbfMttrLineService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	ReportRepairMtbfMttrLineMapper reportRepairMtbfMttrLineMapper;


	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Override
	public List<HashMap<String, Object>> selectReportRepairMtbfMttrLineList(ReportRepairMtbfMttrLineVO reportRepairMtbfMttrLineVO) {
		return reportRepairMtbfMttrLineMapper.selectReportRepairMtbfMttrLineList(reportRepairMtbfMttrLineVO);
	}

}
