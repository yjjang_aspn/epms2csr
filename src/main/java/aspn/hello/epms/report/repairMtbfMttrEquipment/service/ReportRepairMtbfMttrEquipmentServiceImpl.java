package aspn.hello.epms.report.repairMtbfMttrEquipment.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.hello.epms.report.repairMtbfMttrEquipment.mapper.ReportRepairMtbfMttrEquipmentMapper;
import aspn.hello.epms.report.repairMtbfMttrEquipment.model.ReportRepairMtbfMttrEquipmentVO;

/**
 * [레포트-MTBF/MTTR 분석(설비)] ServiceImpl Class
 * @author 김영환
 * @since 2018.06.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.06.04			김영환			최초 생성
 *
 * </pre>
 */

@Service
public class ReportRepairMtbfMttrEquipmentServiceImpl implements ReportRepairMtbfMttrEquipmentService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	ReportRepairMtbfMttrEquipmentMapper reportRepairMtbfMttrEquipmentMapper;


	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Override
	public List<HashMap<String, Object>> selectReportRepairMtbfMttrEquipmentList(ReportRepairMtbfMttrEquipmentVO reportRepairMtbfMttrEquipmentVO) {
		return reportRepairMtbfMttrEquipmentMapper.selectReportRepairMtbfMttrEquipmentList(reportRepairMtbfMttrEquipmentVO);
	}

}
