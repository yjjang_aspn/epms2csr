package aspn.hello.epms.report.repairMtbfMttrEquipment.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.TreeGridUtil;
import aspn.hello.epms.report.preventiveMember.service.ReportPreventiveMemberService;
import aspn.hello.epms.report.repairMtbfMttrEquipment.model.ReportRepairMtbfMttrEquipmentVO;
import aspn.hello.epms.report.repairMtbfMttrEquipment.service.ReportRepairMtbfMttrEquipmentService;


/**
 * [레포트-MTBF/MTTR 분석(설비)] Controller Class
 * @author 김영환
 * @since 2018.06.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.06.04			김영환			최초 생성
 *
 * </pre>
 */

@Controller      
@RequestMapping("/epms/report/repairMtbfMttrEquipment")
public class ReportRepairMtbfMttrEquipmentController extends ContSupport {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/* INSTANCE VAR */
	@Autowired
	ReportRepairMtbfMttrEquipmentService reportRepairMtbfMttrEquipmentService; 
	@Autowired
	ReportPreventiveMemberService reportPreventiveMemberService; 
	
	/**
	 * [레포트-MTBF/MTTR 분석(설비)] : 메인화면
	 *
	 * @author 김영환
	 * @since 2018.06.04
	 * @param ReportRepairMtbfMttrEquipmentVO reportRepairMtbfMttrEquipmentVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportRepairMtbfMttrEquipmentList.do")
	public String reportRepairMtbfMttrEquipmentList(ReportRepairMtbfMttrEquipmentVO reportRepairMtbfMttrEquipmentVO, Model model){
		
		try {
			model.addAttribute("searchDt", DateTime.getYearStr()+ "-" +DateTime.getMonth());
		} catch (Exception e) {
			logger.error("ReportRepairMtbfMttrEquipmentController.reportRepairMtbfMttrEquipmentList > " + e.toString());
			e.printStackTrace();
		}

		return "/epms/report/repairMtbfMttrEquipment/reportRepairMtbfMttrEquipmentList";
		
	}
	
	/**
	 * [레포트-MTBF/MTTR 분석(설비)] : 메인 그리드 레이아웃
	 *
	 * @author 김영환
	 * @since 2018.06.04
	 * @param ReportRepairMtbfMttrEquipmentVO reportRepairMtbfMttrEquipmentVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportRepairMtbfMttrEquipmentListLayout.do")
	public String repairMtbfMttrEquipmentListLayout(ReportRepairMtbfMttrEquipmentVO reportRepairMtbfMttrEquipmentVO, Model model){
		SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM");
		try{
			String startDt = reportRepairMtbfMttrEquipmentVO.getStartDt();
		    Date date = fm.parse(startDt);
		    
		    Calendar cal = new GregorianCalendar(Locale.KOREA);
		    cal.setTime(date);
		    
		    String strDate = fm.format(cal.getTime());
		    
		    List<String> STDYYYY_LIST = new ArrayList();
		    
		    STDYYYY_LIST.add(strDate);
		    for (int i = 0; i < reportRepairMtbfMttrEquipmentVO.getDiffMonth(); i++) {
		    	cal.add(Calendar.MONTH, 1);
		    	strDate = fm.format(cal.getTime());
		    	STDYYYY_LIST.add(strDate);
		    }
		    model.addAttribute("list", STDYYYY_LIST);
			
		} catch (Exception e) {
			logger.error("RepairMtbfMttrEquipmentController.repairMtbfMttrEquipmentListLayout > " + e.toString());
			e.printStackTrace();
		}
			
		return "/epms/report/repairMtbfMttrEquipment/reportRepairMtbfMttrEquipmentListLayout";
	}
	
	/**
	 * [레포트-MTBF/MTTR 분석(설비)] : 메인 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.06.04
	 * @param ReportRepairMtbfMttrEquipmentVO reportRepairMtbfMttrEquipmentVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */						  
	@RequestMapping(value = "/reportRepairMtbfMttrEquipmentListData.do")
	public String repairMtbfMttrEquipmentListData(ReportRepairMtbfMttrEquipmentVO reportRepairMtbfMttrEquipmentVO, Model model) throws Exception{
		SimpleDateFormat fm = new SimpleDateFormat("yyyyMM");
		List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>> ();
		try {
			String startDt = reportRepairMtbfMttrEquipmentVO.getStartDt().replace("-", "");
		    Date date = fm.parse(startDt);
		    
		    Calendar cal = new GregorianCalendar(Locale.KOREA);
		    cal.setTime(date);
		    
		    String strDate = fm.format(cal.getTime());
		    
		    List<String> STDYYYY_LIST = new ArrayList();
		    
		    STDYYYY_LIST.add(strDate);
		    for (int i = 0; i < reportRepairMtbfMttrEquipmentVO.getDiffMonth(); i++) {
		    	cal.add(Calendar.MONTH, 1);
		    	strDate = fm.format(cal.getTime());
		    	STDYYYY_LIST.add(strDate);
		    }
		    reportRepairMtbfMttrEquipmentVO.setSTDYYYY_LIST(STDYYYY_LIST);
		    reportRepairMtbfMttrEquipmentVO.setSubAuth(getSsSubAuth());
		    reportRepairMtbfMttrEquipmentVO.setCompanyId(getSsCompanyId());
		    list = reportRepairMtbfMttrEquipmentService.selectReportRepairMtbfMttrEquipmentList(reportRepairMtbfMttrEquipmentVO);
		    
		    model.addAttribute("list", list);
		    model.addAttribute("list2", STDYYYY_LIST);
		    
 		} catch (Exception e) {
			logger.error("RepairMtbfMttrEquipmentController.repairMtbfMttrEquipmentListData > " + e.toString());
			e.printStackTrace();
		}

		return "/epms/report/repairMtbfMttrEquipment/reportRepairMtbfMttrEquipmentListData";
	}
	
	
	
}
