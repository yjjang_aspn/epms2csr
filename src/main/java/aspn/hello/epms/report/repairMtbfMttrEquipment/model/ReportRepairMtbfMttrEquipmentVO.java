package aspn.hello.epms.report.repairMtbfMttrEquipment.model;

import java.util.ArrayList;
import java.util.List;

import aspn.hello.com.model.AbstractVO;
import aspn.hello.epms.report.repairMtbfMttrEquipment.model.ReportRepairMtbfMttrEquipmentVO;

/**
 * [레포트-MTBF/MTTR 분석(설비)] VO Class
 * @author 김영환
 * @since 2018.06.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.06.04			김영환			최초 생성
 *
 * </pre>
 */

public class ReportRepairMtbfMttrEquipmentVO extends AbstractVO{
	private static final long serialVersionUID = 1L;
	
	/** CATEGORY DB **/
	private String TREE = "";
	
	
	/** parameter */
	private int diffMonth = 0;
	private List<String> STDYYYY_LIST;

	public String getTREE() {
		return TREE;
	}

	public void setTREE(String tREE) {
		TREE = tREE;
	}

	public int getDiffMonth() {
		return diffMonth;
	}

	public void setDiffMonth(int diffMonth) {
		this.diffMonth = diffMonth;
	}

	public List<String> getSTDYYYY_LIST() {
		return STDYYYY_LIST;
	}

	public void setSTDYYYY_LIST(List<String> sTDYYYY_LIST) {
		STDYYYY_LIST = sTDYYYY_LIST;
	}
	
	
	
}
