package aspn.hello.epms.report.repairMtbfMttrEquipment.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.report.preventivePart.model.ReportPreventivePartVO;
import aspn.hello.epms.report.repairMtbfMttrEquipment.model.ReportRepairMtbfMttrEquipmentVO;

/**
 * [레포트-MTBF/MTTR 분석(설비)] Service Class
 * @author 김영환
 * @since 2018.06.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.06.04			김영환			최초 생성
 *
 * </pre>
 */

public interface ReportRepairMtbfMttrEquipmentService {

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	List<HashMap<String, Object>> selectReportRepairMtbfMttrEquipmentList(ReportRepairMtbfMttrEquipmentVO reportRepairMtbfMttrEquipmentVO);
	
	
}
