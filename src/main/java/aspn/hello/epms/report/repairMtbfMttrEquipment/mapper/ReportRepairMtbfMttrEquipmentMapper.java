package aspn.hello.epms.report.repairMtbfMttrEquipment.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.report.repairMtbfMttrEquipment.model.ReportRepairMtbfMttrEquipmentVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [레포트-MTBF/MTTR 분석(설비)] Mapper Class
 * @author 김영환
 * @since 2018.06.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.06.04			김영환			최초 생성
 *
 * </pre>
 */

@Mapper("reportRepairMtbfMttrEquipmentMapper")
public interface ReportRepairMtbfMttrEquipmentMapper {

	List<HashMap<String, Object>> selectReportRepairMtbfMttrEquipmentList(ReportRepairMtbfMttrEquipmentVO reportRepairMtbfMttrEquipmentVO);
}
