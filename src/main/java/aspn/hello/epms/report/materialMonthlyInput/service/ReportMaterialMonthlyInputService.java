package aspn.hello.epms.report.materialMonthlyInput.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.report.materialMonthlyInput.model.ReportMaterialMonthlyInputVO;

/**
 * [레포트-자재입고현황 분석] Service Class
 * @author 김영환
 * @since 2018.06.12
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.06.12			김영환			최초 생성
 *
 * </pre>
 */

public interface ReportMaterialMonthlyInputService {

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * 레포트 자재입고 현황 분석
	 * @author 김영환
	 * @since 2018.06.12
	 * @param ReportMaterialMonthlyInputVO reportMaterialMonthlyInputVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectReportMaterialMonthlyInputList(ReportMaterialMonthlyInputVO reportMaterialMonthlyInputVO);
	
	
}
