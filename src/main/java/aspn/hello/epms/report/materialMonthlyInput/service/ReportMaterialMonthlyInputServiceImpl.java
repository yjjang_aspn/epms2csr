package aspn.hello.epms.report.materialMonthlyInput.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.hello.epms.report.materialMonthlyInput.mapper.ReportMaterialMonthlyInputMapper;
import aspn.hello.epms.report.materialMonthlyInput.model.ReportMaterialMonthlyInputVO;

/**
 * [레포트-자재입고현황 분석] ServiceImpl Class
 * @author 김영환
 * @since 2018.06.12
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.06.12			김영환			최초 생성
 *
 * </pre>
 */

@Service
public class ReportMaterialMonthlyInputServiceImpl implements ReportMaterialMonthlyInputService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	ReportMaterialMonthlyInputMapper reportMaterialMonthlyInputMapper;


	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * 레포트 자재입고 현황 분석
	 * @author 김영환
	 * @since 2018.06.12
	 * @param ReportMaterialMonthlyInputVO reportMaterialMonthlyInputVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectReportMaterialMonthlyInputList(ReportMaterialMonthlyInputVO reportMaterialMonthlyInputVO) {
		return reportMaterialMonthlyInputMapper.selectReportMaterialMonthlyInputList(reportMaterialMonthlyInputVO);
	}

}
