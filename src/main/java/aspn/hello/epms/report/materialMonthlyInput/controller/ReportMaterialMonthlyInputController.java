package aspn.hello.epms.report.materialMonthlyInput.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.TreeGridUtil;
import aspn.hello.epms.report.materialMonthlyInput.model.ReportMaterialMonthlyInputVO;
import aspn.hello.epms.report.materialMonthlyInput.service.ReportMaterialMonthlyInputService;


/**
 * [레포트-자재입고현황 분석] Controller Class
 * @author 김영환
 * @since 2018.06.12
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일				수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.06.12			김영환			최초 생성
 *
 * </pre>
 */

@Controller      
@RequestMapping("/epms/report/materialMonthlyInput")
public class ReportMaterialMonthlyInputController extends ContSupport {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/* INSTANCE VAR */
	@Autowired
	ReportMaterialMonthlyInputService reportMaterialMonthlyInputService; 
	
	
	/**
	 * [레포트-자재입고현황 분석] : 메인화면
	 *
	 * @author 김영환
	 * @since 2018.06.12
	 * @param ReportMaterialMonthlyInputVO reportMaterialMonthlyInputVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportMaterialMonthlyInputList.do")
	public String reportMaterialMonthlyInputList(ReportMaterialMonthlyInputVO reportMaterialMonthlyInputVO, Model model){
		
		try {
			model.addAttribute("searchDt", DateTime.getYearStr()+ "-" +DateTime.getMonth());
		} catch (Exception e) {
			logger.error("ReportMaterialMonthlyInputController.reportMaterialMonthlyInputList > " + e.toString());
			e.printStackTrace();
		}

		return "/epms/report/materialMonthlyInput/reportMaterialMonthlyInputList";
		
	}
	
	/**
	 * [레포트-자재입고현황 분석] : 메인 그리드 레이아웃
	 *
	 * @author 김영환
	 * @since 2018.06.12
	 * @param ReportMaterialMonthlyInputVO reportMaterialMonthlyInputVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/reportMaterialMonthlyInputListLayout.do")
	public String materialMonthlyInputListLayout(ReportMaterialMonthlyInputVO reportMaterialMonthlyInputVO, Model model){
		SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM");
		try{
			String startDt = reportMaterialMonthlyInputVO.getStartDt();
		    Date date = fm.parse(startDt);
		    
		    Calendar cal = new GregorianCalendar(Locale.KOREA);
		    cal.setTime(date);
		    
		    String strDate = fm.format(cal.getTime());
		    
		    List<String> STDYYYY_LIST = new ArrayList();
		    
		    STDYYYY_LIST.add(strDate);
		    for (int i = 0; i < reportMaterialMonthlyInputVO.getDiffMonth(); i++) {
		    	cal.add(Calendar.MONTH, 1);
		    	strDate = fm.format(cal.getTime());
		    	STDYYYY_LIST.add(strDate);
		    }
		    model.addAttribute("list", STDYYYY_LIST);
			
		} catch (Exception e) {
			logger.error("MaterialMonthlyInputController.MaterialMonthlyInputListLayout > " + e.toString());
			e.printStackTrace();
		}
			
		return "/epms/report/materialMonthlyInput/reportMaterialMonthlyInputListLayout";
	}
	
	/**
	 * [레포트-자재입고현황 분석] : 메인 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.06.12
	 * @param ReportMaterialMonthlyInputVO reportMaterialMonthlyInputVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */						  
	@RequestMapping(value = "/reportMaterialMonthlyInputListData.do")
	public String MaterialMonthlyInputListData(ReportMaterialMonthlyInputVO reportMaterialMonthlyInputVO, Model model) throws Exception{
		SimpleDateFormat fm = new SimpleDateFormat("yyyyMM");
		List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>> ();
		try {
			String startDt = reportMaterialMonthlyInputVO.getStartDt().replace("-", "");
		    Date date = fm.parse(startDt);
		    
		    Calendar cal = new GregorianCalendar(Locale.KOREA);
		    cal.setTime(date);
		    
		    String strDate = fm.format(cal.getTime());
		    
		    List<String> STDYYYY_LIST = new ArrayList();
		    
		    STDYYYY_LIST.add(strDate);
		    for (int i = 0; i < reportMaterialMonthlyInputVO.getDiffMonth(); i++) {
		    	cal.add(Calendar.MONTH, 1);
		    	strDate = fm.format(cal.getTime());
		    	STDYYYY_LIST.add(strDate);
		    }
		    reportMaterialMonthlyInputVO.setSTDYYYY_LIST(STDYYYY_LIST);
		    reportMaterialMonthlyInputVO.setSubAuth(getSsSubAuth());
		    reportMaterialMonthlyInputVO.setCompanyId(getSsCompanyId());
		    
		    list = reportMaterialMonthlyInputService.selectReportMaterialMonthlyInputList(reportMaterialMonthlyInputVO);
		    
		    model.addAttribute("list", list);
		    model.addAttribute("list2", STDYYYY_LIST);
		    
 		} catch (Exception e) {
			logger.error("MaterialMonthlyInputController.MaterialMonthlyInputListData > " + e.toString());
			e.printStackTrace();
		}
//		return TreeGridUtil.getGridListData(list);
		return "/epms/report/materialMonthlyInput/reportMaterialMonthlyInputListData";
	}
	
	
	
}
