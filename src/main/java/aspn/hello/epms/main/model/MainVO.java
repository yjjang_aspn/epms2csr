package aspn.hello.epms.main.model;

import java.util.List;

import aspn.hello.com.model.AbstractVO;

/**
 * [대시보드] VO Class
 * @author 김영환
 * @since 2018.06.18
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.06.18		김영환		최초 생성
 *   
 * </pre>
 */

public class MainVO extends AbstractVO{

	private static final long serialVersionUID = 1910040868046659410L;
	
	/** Repair Request Type */
	private String APPR_STATUS		= "";   // 결재상태
	private String DATE_REQUEST		= "";   // 요청일
	
	/** Repair Result Type */
	private String REPAIR_STATUS	= "";   // 정비상태
	private String DATE_PLAN		= "";   // 정비계획일
	private String MANAGER			= "";   // 담당자
	
	/** Member Type */
	private String LOCATION			= "";	// 공장ID
	private String PART				= "";   // 파트ID
	
	/** COMCD Type*/
	private String REMARKS			= "";   // 비고(공장파트구분 01:파트구분,02:파트통합)
	
	/** Parameter Type */
	private String PARAM_DATE 		= "";	// 검색날짜
	private String PARAM_PART 		= "";	// 검색파트
	private String PARAM_LOCATION 	= "";	// 검색공장
	private String PARAM_YEAR		= "";	// 년도
	private List<String> STDYYYY_LIST; 		// YYYY-MM 리스트
	private String CHART					= "";	// 차트ID
	private String STATUS 					= "";   // 데이터 상태
	private String authType					= "";	//권한여부
	
	public String getAPPR_STATUS() {
		return APPR_STATUS;
	}
	public void setAPPR_STATUS(String aPPR_STATUS) {
		APPR_STATUS = aPPR_STATUS;
	}
	public String getDATE_REQUEST() {
		return DATE_REQUEST;
	}
	public void setDATE_REQUEST(String dATE_REQUEST) {
		DATE_REQUEST = dATE_REQUEST;
	}
	public String getREPAIR_STATUS() {
		return REPAIR_STATUS;
	}
	public void setREPAIR_STATUS(String rEPAIR_STATUS) {
		REPAIR_STATUS = rEPAIR_STATUS;
	}
	public String getDATE_PLAN() {
		return DATE_PLAN;
	}
	public void setDATE_PLAN(String dATE_PLAN) {
		DATE_PLAN = dATE_PLAN;
	}
	public String getMANAGER() {
		return MANAGER;
	}
	public void setMANAGER(String mANAGER) {
		MANAGER = mANAGER;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getPART() {
		return PART;
	}
	public void setPART(String pART) {
		PART = pART;
	}
	public String getREMARKS() {
		return REMARKS;
	}
	public void setREMARKS(String rEMARKS) {
		REMARKS = rEMARKS;
	}
	public String getPARAM_DATE() {
		return PARAM_DATE;
	}
	public void setPARAM_DATE(String pARAM_DATE) {
		PARAM_DATE = pARAM_DATE;
	}
	public String getPARAM_PART() {
		return PARAM_PART;
	}
	public void setPARAM_PART(String pARAM_PART) {
		PARAM_PART = pARAM_PART;
	}
	public String getPARAM_LOCATION() {
		return PARAM_LOCATION;
	}
	public void setPARAM_LOCATION(String pARAM_LOCATION) {
		PARAM_LOCATION = pARAM_LOCATION;
	}
	public String getPARAM_YEAR() {
		return PARAM_YEAR;
	}
	public void setPARAM_YEAR(String pARAM_YEAR) {
		PARAM_YEAR = pARAM_YEAR;
	}
	public List<String> getSTDYYYY_LIST() {
		return STDYYYY_LIST;
	}
	public void setSTDYYYY_LIST(List<String> sTDYYYY_LIST) {
		STDYYYY_LIST = sTDYYYY_LIST;
	}
	public String getCHART() {
		return CHART;
	}
	public void setCHART(String cHART) {
		CHART = cHART;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getAuthType() {
		return authType;
	}
	public void setAuthType(String authType) {
		this.authType = authType;
	}
	
	
}
