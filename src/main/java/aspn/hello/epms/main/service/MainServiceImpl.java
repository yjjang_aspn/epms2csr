package aspn.hello.epms.main.service;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.hello.com.model.ComVO;
import aspn.hello.epms.main.mapper.MainMapper;
import aspn.hello.epms.main.model.MainVO;
import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.epms.notice.model.NoticeVO;

@Service
public class MainServiceImpl implements MainService {
	
	@Autowired
	MainMapper mainMapper;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 일반 공지사항 목록
	 * @author 정호윤
	 * @since 2017.09.25.
	 * @param String ssCompanyId, String type
	 * @return HashMap<String, Object>
	 */
	@Override
	public List<NoticeVO> getNoticeList(NoticeVO noticeVO) throws Exception {
		return mainMapper.getNoticeList(noticeVO);
	}
	
	/**
	 * 재고부족 자재 리스트
	 *
	 * @author 김영환
	 * @since 2018.06.13
	 * @param MainVO mainVO
	 * @return HashMap<String, Object>
	 */
	@Override
	public List<HashMap<String, Object>> selectMaterialShortageList(MaterialMasterVO materialMasterVO) {
		return mainMapper.selectMaterialShortageList(materialMasterVO);
	}
	
	/**
	 * 메인 예방보전 차트 데이터
	 * @author 김영환
	 * @since 2018.06.15
	 * @return Map<String, Object> params
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectPreventiveChartData(MainVO mainVO) {
		return mainMapper.selectPreventiveChartData(mainVO);
	}

	/**
	 * 메인 정비실적 차트 데이터
	 * 
	 * @author 김영환
	 * @since 2018. 06. 18
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairResultChartData(MainVO mainVO) {
		return mainMapper.selectRepairResultChartData(mainVO);
	}

	/**
	 * MTBF/MTTR 정보 데이터
	 * @author 김영환
	 * @since 2018. 06. 18
	 * @param HashMap<String, Object> params
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectMtbfMttrInfo(MainVO mainVO) {
		return mainMapper.selectMtbfMttrInfo(mainVO);
	}

	/**
	 * 정비원 현황 
	 * @author 김영환
	 * @since 2018. 06. 19
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectWokerInfo(MainVO mainVO) {
		return mainMapper.selectWokerInfo(mainVO);
	}

	/**
	 * 돌발정비 현황
	 * @author 김영환
	 * @since 2018. 06. 19
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairEMInfo(MainVO mainVO) {
		return mainMapper.selectRepairEMInfo(mainVO);
	}

	/**
	 * 권한별 공장 목록 조회
	 * @author 김영환
	 * @since 2018. 06. 19
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectLocationListOpt(MainVO mainVO) {
		return mainMapper.selectLocationListOpt(mainVO);
	}

	/**
	 * 정비유형 별 월별 정비실적 통계
	 * @author 김영환
	 * @since 2018. 06. 19
	 * @param MainVO mainVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairResultListOpt1(MainVO mainVO) {
		return mainMapper.selectRepairResultListOpt1(mainVO);
	}

	/**
	 * 고장/조치시간 별 월별 정비실적 통계
	 * @author 김영환
	 * @since 2018. 06. 19
	 * @param MainVO mainVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> selectRepairResultListOpt2(MainVO mainVO) {
		return mainMapper.selectRepairResultListOpt2(mainVO);
	}

	/**
	 * 고장유형 별 월별 정비실적 통계
	 * @author 김영환
	 * @since 2018. 06. 19
	 * @param MainVO mainVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairResultListOpt3(MainVO mainVO) {
		return mainMapper.selectRepairResultListOpt3(mainVO);
	}

	/**
	 * 조치유형 별 월별 정비실적 통계
	 * @author 김영환
	 * @since 2018. 06. 19
	 * @param MainVO mainVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairResultListOpt4(MainVO mainVO) {
		return mainMapper.selectRepairResultListOpt4(mainVO);
	}

	/**
	 * 예방보전 현황 상세 수행현황 차트 데이터
	 * @author 김영환
	 * @since 2018. 06. 22
	 * @param MainVO mainVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> preventiveInfoDtlPerformChartData(MainVO mainVO) {
		return mainMapper.preventiveInfoDtlPerformChartData(mainVO);
	}

	/**
	 * 조건별 정비실적 현황
	 * @author 김영환
	 * @since 2018. 06. 20
	 * @param MainVO mainVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> selectRepairResultStatus(MainVO mainVO) {
		return mainMapper.selectRepairResultStatus(mainVO);
	}

	/**
	 * 정비실적현황 상세페이지 결제관련 레이어 팝업 데이터
	 * @author 김영환
	 * @since 2018. 06. 20
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairApprListOpt(MainVO mainVO) {
		return mainMapper.selectRepairApprListOpt(mainVO);
	}

	/**
	 * 대시보드 - 정비실적 상세페이지 실적관련 레이어 팝업 데이터
	 * @author 김영환
	 * @since 2018. 06. 20
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairResultListOpt(MainVO mainVO) {
		return mainMapper.selectRepairResultListOpt(mainVO);
	}

	/**
	 * 예방보전 현황 상세 CBM 차트 데이터
	 * @author 김영환
	 * @since 2018. 06. 25
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */	
	@Override
	public List<HashMap<String, Object>> preventiveInfoDtlCBMData(MainVO mainVO) {
		return mainMapper.preventiveInfoDtlCBMData(mainVO);
	}

	/**
	 * 예방보전 현황 상세 TBM 차트 데이터
	 * @author 김영환
	 * @since 2018. 06. 25
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */	
	@Override
	public List<HashMap<String, Object>> preventiveInfoDtlTBMData(MainVO mainVO) {
		return mainMapper.preventiveInfoDtlTBMData(mainVO);
	}

	/**
	 * 예방보전 현황 상세 CBM 판정 차트 데이터
	 * @author 김영환
	 * @since 2018. 06. 20
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> preventiveInfoDtlCbmDecisionChartData(MainVO mainVO) {
		return mainMapper.preventiveInfoDtlCbmDecisionChartData(mainVO);
	}

	/**
	 * 예방보전 현황 상세 계획일 초과현황
	 * @author 김영환
	 * @since 2018. 06. 25
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> preventiveInfoDtlDayOverList(MainVO mainVO) {
		return mainMapper.preventiveInfoDtlDayOverList(mainVO);
	}

	/**
	 * 자재 재고현황 리스트
	 *
	 * @author 김영환
	 * @since 2018.06.13
	 * @param MaterialMasterVO materialMasterVO
	 * @return HashMap<String, Object>
	 */
	@Override
	public List<HashMap<String, Object>> selectMaterialMasterList(MaterialMasterVO materialMasterVO) {
		return mainMapper.selectMaterialMasterList(materialMasterVO);
	}

	/**
	 * 월별 돌발정비 차트 데이터
	 *
	 * @author 김영환
	 * @since 2018.06.13
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectRepairEMChartData(MainVO mainVO) {
		return mainMapper.selectRepairEMChartData(mainVO);
	}

	/**
	 * 공장별 파트정보
	 *
	 * @author 김영환
	 * @param mainVO 
	 * @since 2018.06.13
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectPartInfo(MainVO mainVO) {
		return mainMapper.selectPartInfo(mainVO);
	}

	/**
	 * 돌발정비현황 차트데이터
	 *
	 * @author 김영환
	 * @param mainVO 
	 * @since 2018.06.13
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectTroubleListChartData(MainVO mainVO) {
		return mainMapper.selectTroubleListChartData(mainVO);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
}
