package aspn.hello.epms.main.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.com.model.ComVO;
import aspn.hello.epms.main.model.MainVO;
import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.epms.notice.model.NoticeVO;

public interface MainService {
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 일반 공지사항 목록
	 * @author 김영환
	 * @since 2018.06.01
	 * @param String ssCompanyId, String type
	 * @return HashMap<String, Object>
	 */
	List<NoticeVO> getNoticeList(NoticeVO noticeVO) throws Exception;

	/**
	 * 재고부족 자재 리스트
	 *
	 * @author 김영환
	 * @since 2018.06.13
	 * @param MainVO mainVO
	 * @return HashMap<String, Object>
	 */
	public List<HashMap<String, Object>> selectMaterialShortageList(MaterialMasterVO materialMasterVO);
	
	/**
	 * 메인 예방보전 차트 데이터
	 * @author 김영환
	 * @since 2018.06.15
	 * @return Map<String, Object> params
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectPreventiveChartData(MainVO mainVO);

	/**
	 * 메인 정비실적 차트 데이터
	 * 
	 * @author 김영환
	 * @since 2018. 06. 18
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectRepairResultChartData(MainVO mainVO);

	/**
	 * MTBF/MTTR 정보 데이터
	 * @author 김영환
	 * @since 2018. 06. 18
	 * @param HashMap<String, Object> params
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectMtbfMttrInfo(MainVO mainVO);

	/**
	 * 정비원 현황 
	 * @author 김영환
	 * @since 2018. 06. 18
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectWokerInfo(MainVO mainVO);

	/**
	 * 돌발정비 현황
	 * @author 김영환
	 * @since 2018. 06. 18
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectRepairEMInfo(MainVO mainVO);

	/**
	 * 조건별 정비실적 현황
	 * @author 김영환
	 * @since 2018. 06. 20
	 * @param MainVO mainVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	HashMap<String, Object> selectRepairResultStatus(MainVO mainVO);
	
	/**
	 * 권한별 공장 목록 조회
	 * @author 김영환
	 * @since 2018. 06. 19
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectLocationListOpt(MainVO mainVO);

	/**
	 * 정비유형 별 월별 정비실적 통계
	 * @author 김영환
	 * @since 2018. 06. 19
	 * @param MainVO mainVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectRepairResultListOpt1(MainVO mainVO);

	/**
	 * 고장/조치시간 별 월별 정비실적 통계
	 * @author 김영환
	 * @since 2018. 06. 19
	 * @param MainVO mainVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	HashMap<String, Object> selectRepairResultListOpt2(MainVO mainVO);

	/**
	 * 고장유형 별 월별 정비실적 통계
	 * @author 김영환
	 * @since 2018. 06. 19
	 * @param MainVO mainVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectRepairResultListOpt3(MainVO mainVO);

	/**
	 * 조치유형 별 월별 정비실적 통계
	 * @author 김영환
	 * @since 2018. 06. 19
	 * @param MainVO mainVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectRepairResultListOpt4(MainVO mainVO);
	
	/**
	 * 예방보전 현황 상세 수행현황 차트 데이터
	 * @author 김영환
	 * @since 2018. 06. 22
	 * @param MainVO mainVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> preventiveInfoDtlPerformChartData(MainVO mainVO);
	
	/**
	 * 정비실적현황 상세페이지 결제관련 레이어 팝업 데이터
	 * @author 김영환
	 * @since 2018. 06. 20
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectRepairApprListOpt(MainVO mainVO);

	/**
	 * 대시보드 - 정비실적 상세페이지 실적관련 레이어 팝업 데이터
	 * @author 김영환
	 * @since 2018. 06. 20
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectRepairResultListOpt(MainVO mainVO);

	/**
	 * 예방보전 현황 상세 CBM 차트 데이터
	 * @author 김영환
	 * @since 2018. 06. 25
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */	
	List<HashMap<String, Object>> preventiveInfoDtlCBMData(MainVO mainVO);
	
	/**
	 * 예방보전 현황 상세 TBM 차트 데이터
	 * @author 김영환
	 * @since 2018. 06. 25
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */	
	List<HashMap<String, Object>> preventiveInfoDtlTBMData(MainVO mainVO);

	/**
	 * 예방보전 현황 상세 CBM 판정 차트 데이터
	 * @author 김영환
	 * @since 2018. 06. 20
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> preventiveInfoDtlCbmDecisionChartData(MainVO mainVO);

	/**
	 * 예방보전 현황 상세 계획일 초과현황
	 * @author 김영환
	 * @since 2018. 06. 25
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	HashMap<String, Object> preventiveInfoDtlDayOverList(MainVO mainVO);

	/**
	 * 자재 재고현황 리스트
	 *
	 * @author 김영환
	 * @since 2018.06.13
	 * @param MaterialMasterVO materialMasterVO
	 * @return HashMap<String, Object>
	 */
	List<HashMap<String, Object>> selectMaterialMasterList(MaterialMasterVO materialMasterVO);

	/**
	 * 월별 돌발정비 차트 데이터
	 *
	 * @author 김영환
	 * @since 2018.06.13
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectRepairEMChartData(MainVO mainVO);

	/**
	 * 공장별 파트정보
	 *
	 * @author 김영환
	 * @param mainVO 
	 * @since 2018.06.13
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectPartInfo(MainVO mainVO);

	/**
	 * 돌발정비현황 차트데이터
	 *
	 * @author 김영환
	 * @param mainVO 
	 * @since 2018.06.13
	 * @param MainVO mainVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectTroubleListChartData(MainVO mainVO);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
}
