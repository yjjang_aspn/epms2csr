package aspn.hello.epms.main.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.JsonUtil;
import aspn.com.common.util.ObjUtil;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.model.ComVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.com.service.ComService;
import aspn.hello.epms.main.model.MainVO;
import aspn.hello.epms.main.service.MainService;
import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.epms.notice.model.NoticeVO;
import aspn.hello.epms.preventive.plan.model.PreventivePlanVO;
import aspn.hello.epms.preventive.plan.service.PreventivePlanService;
import aspn.hello.epms.repair.appr.model.RepairApprVO;
import aspn.hello.epms.repair.appr.service.RepairApprService;
import aspn.hello.epms.repair.request.model.RepairRequestVO;
import aspn.hello.epms.repair.request.service.RepairRequestService;
import aspn.hello.epms.repair.result.model.RepairResultVO;
import aspn.hello.epms.repair.result.service.RepairResultService;

/**
 * epms 메인 컨트롤러 클래스
 * @author 김영환
 * @since 2018. 06. 01
 * @version 1.0
 * @see

 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일               		 	수정자                                  수정내용
 *  -----------    		--------    ---------------------------
 *  2018.06.13			김영환			재고부족 자재 최초 생성
 *  
 *
 * </pre>
 */
@Controller
@RequestMapping("/epms/main")
public class MainController extends ContSupport{
	Logger logger = LoggerFactory.getLogger(this.getClass());	

	@Autowired
	MainService mainService;
	
	@Autowired
	ComService comService;
	
	@Autowired
	AttachService attachService;
	
	@Autowired
	RepairRequestService repairRequestService;
	
	@Autowired
	RepairApprService repairApprService;
	
	@Autowired
	RepairResultService repairResultService;
	
	@Autowired
	PreventivePlanService preventivePlanService;
	
	/**
	 * epms 메인대시보드
	 * @author 김영환
	 * @since 2018. 06. 01
	 * @param request
	 * @param HttpServletRequest request, MainVO mainVO, Model model
	 * @return "/hello/epms/main/mainDashBoard"
	 * @throws Exception
	 */
	@RequestMapping(value = "/mainDashBoard.do")
	public String loginForm(HttpServletRequest request, MainVO mainVO, NoticeVO noticeVO, Model model) throws Exception {
		try {
			
			mainVO.setCompanyId(getSsCompanyId());
			mainVO.setSubAuth(getSsSubAuth());
			// 권한별 공장목록 조회
			List<HashMap<String, Object>> locationList = mainService.selectLocationListOpt(mainVO);
			model.addAttribute("locationList", locationList);
			
			noticeVO.setCompanyId(getSsCompanyId());
			noticeVO.setSubAuth(getSsSubAuth());
			noticeVO.setTYPE("2");
			
			// 팝업 공지사항
			List<NoticeVO> popNoticeList = mainService.getNoticeList(noticeVO);
			model.addAttribute("popNoticeList", popNoticeList);
			
		} catch (Exception e) {
			logger.error("Main DashBoard Error");
			e.printStackTrace();
		}
		
		return "/epms/main/mainDashBoard";
	}
	
	/**
	 * epms 메인대시보드 : 재고부족 자재 리스트
	 * @author 김영환
	 * @since 2018. 06. 13
	 * @param request
	 * @param HttpServletRequest request, MainVO mainVO, Model model
	 * @return String
	 * @throws Exception
	 */
//	@RequestMapping(value = "/materialShortageAjax.do")
//	@ResponseBody
//	public HashMap<String, Object> materialShortageAjax(MaterialMasterVO materialMasterVO, Model model){
//		
//		HashMap<String, Object> map= new HashMap<String, Object>();
//		
//		try {
//			
//			materialMasterVO.setSubAuth(getSsSubAuth());
//			materialMasterVO.setCompanyId(getSsCompanyId());
//			materialMasterVO.setSsLocation("5100");
//			
//			//재고부족 자재 리스트
//			List<HashMap<String, Object>> materialShortageList = mainService.selectMaterialShortageList(materialMasterVO);
//			map.put("materialShortageList", materialShortageList);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.error("MainController.materialShortageAjax Error !" + e.toString());
//		}
//		
//		return map;
//	}
	
	/**
	 * epms 메인대시보드 : 자재 재고현황 리스트
	 * @author 김영환
	 * @since 2018. 06. 13
	 * @param MaterialMasterVO materialMasterVO 
	 * @return String
	 * @throws Exception
	 */
//	@RequestMapping(value = "/materialMasterAjax.do")
//	@ResponseBody
//	public HashMap<String, Object> materialMasterAjax(MaterialMasterVO materialMasterVO){
//		
//		HashMap<String, Object> map= new HashMap<String, Object>();
//		
//		try {
//			
//			materialMasterVO.setSubAuth(getSsSubAuth());
//			materialMasterVO.setCompanyId(getSsCompanyId());
//			materialMasterVO.setSsLocation("5100");
//			
//			//재고부족 자재 리스트
//			List<HashMap<String, Object>> materialMasterList = mainService.selectMaterialMasterList(materialMasterVO);
//			map.put("materialMasterList", materialMasterList);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.error("MainController.materialMasterAjax Error !" + e.toString());
//		}
//		
//		return map;
//	}
	
	/**
	 * epms 메인대시보드 : 내 요청현황 리스트
	 * 
	 * @author 김영환
	 * @since 2018. 06. 15
	 * @param request
	 * @param HttpServletRequest request, MainVO mainVO, Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairRequestList.do")
	@ResponseBody
	public Map<String, Object> repairRequestList(RepairRequestVO repairRequestVO) throws Exception{
		
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			repairRequestVO.setCompanyId(getSsCompanyId());
			repairRequestVO.setSsLocation("5100");
			repairRequestVO.setUserId(getSsUserId());
			
			// 현재 날짜
			Date today = new Date();         
			SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd"); 
			String startDt = date.format(today);
			repairRequestVO.setEndDt(startDt);
			
			// 현재날짜 기준 한달 전 날짜
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH , -1);
			String endDt = new java.text.SimpleDateFormat("yyyyMMdd").format(cal.getTime());
			repairRequestVO.setStartDt(endDt);

			repairRequestVO.setLOCATION("ALL");			//조회 공장
			repairRequestVO.setAPPR_STATUS("ALL");		//결재상태
			repairRequestVO.setREPAIR_STATUS("ALL");	//정비상태
			repairRequestVO.setFlag("MY");				//ALL:전체, MY:내요청
			
			//내 정비요청현황 목록
			List<HashMap<String, Object>> list= repairRequestService.selectRepairRequestList2(repairRequestVO);
			
			resultMap.put("REQUEST_LIST", list);
			
		} catch (Exception e) {
			logger.error("MainController.repairRequestList > " + e.toString());
			e.printStackTrace();
		}
		return resultMap;
	}
	
	/**
	 * epms 메인대시보드 : 내 요청현황 리스트 상세페이지
	 * 
	 * @author 김영환
	 * @since 2018. 06. 15
	 * @param RepairRequestVO repairRequestVO
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/mainMyReqListDtl.do")
	public String mainMyReqListDtl(RepairRequestVO repairRequestVO, Model model) throws Exception{
		
		try {
			repairRequestVO.setCompanyId(getSsCompanyId());
			repairRequestVO.setSsLocation("5100");
			repairRequestVO.setUserId(getSsUserId());
			
			// 현재 날짜
			Date today = new Date();         
			SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd"); 
			String startDt = date.format(today);
			repairRequestVO.setEndDt(startDt);
			
			// 현재날짜 기준 한달 전 날짜
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH , -1);
			String endDt = new java.text.SimpleDateFormat("yyyyMMdd").format(cal.getTime());
			repairRequestVO.setStartDt(endDt);
			
			repairRequestVO.setLOCATION("ALL");			//조회 공장
			repairRequestVO.setAPPR_STATUS("ALL");		//결재상태
			repairRequestVO.setREPAIR_STATUS("ALL");	//정비상태
			repairRequestVO.setFlag("MY");				//ALL:전체, MY:내요청
			
			//내 정비요청현황 목록
			List<HashMap<String, Object>> list= repairRequestService.selectRepairRequestList2(repairRequestVO);
			
			model.addAttribute("REQUEST_LIST", list);
			
		} catch (Exception e) {
			logger.error("MainController.mainMyReqListDtl > " + e.toString());
			e.printStackTrace();
		}
		return "/epms/main/mainMyReqListDtl";
	}
	
	/**
	 * 예방보전 차트 데이터
	 * @author 김영환
	 * @since 2018.06.15
	 * @throws Exception
	 */
	//2019.03.25 던킨 요청으로 사용안함
//	@RequestMapping(value = "/preventiveChartData.do")
//	@ResponseBody
//	public Map<String, Object> preventiveChartData(MainVO mainVO) throws Exception {
//		Map<String, Object> resultMap = new HashMap<>();
//		try {
//			mainVO.setPARAM_DATE(DateTime.getYearStr()+ "-" +DateTime.getMonth());
//			mainVO.setCompanyId(getSsCompanyId());
//			mainVO.setSubAuth(getSsSubAuth());
//			//mainVO.setSsLocation(getSsLocation());
//			
//			List<HashMap<String, Object>> preventiveChartData= mainService.selectPreventiveChartData(mainVO);
//			resultMap.put("preventive", preventiveChartData);
//			
//			ComVO comVO = new ComVO();
//			String com_grp = "PREVENTIVE_STATUS";
//			comVO.setFlag(com_grp);
//			List<HashMap<String, Object>> comCodeList = comService.selectComCodeList(comVO);
//			HashMap<String, Object> comCode = new HashMap<String, Object>(); 
//			for ( HashMap<String, Object> code : comCodeList ){
//				comCode.put(String.valueOf(code.get(com_grp)), code.get(com_grp + "_NAME"));
//			}
//			
//			resultMap.put("comCode", comCode);
//			
//			
//		} catch (Exception e) {
//			logger.error(e.toString());
//			e.printStackTrace();
//			
//			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
//		}
//		return resultMap;
//	}
	
	/**
	 * mtbfMttr 데이터
	 * @author 김영환
	 * @since 2018.06.15
	 * @throws Exception
	 */
	@RequestMapping(value = "/mtbfMttrInfo.do")
	@ResponseBody
	public Map<String, Object> mtbfMttrInfo(HttpServletRequest request, MainVO mainVO) throws Exception {
		Map<String, Object> resultMap = new HashMap<>();
		SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM");
		try {

		    mainVO.setCompanyId(getSsCompanyId());
		    mainVO.setSubAuth(getSsSubAuth());
			
			String startDt = DateTime.getYearStr()+ "-" +DateTime.getMonth();
		    Date date = fm.parse(startDt);
		    
		    Calendar cal = new GregorianCalendar(Locale.KOREA);
		    cal.setTime(date);
		    
		    String strDate = fm.format(cal.getTime());
		    
		    List<String> STDYYYY_LIST = new ArrayList();
		    
		    STDYYYY_LIST.add(strDate);
		    for (int i = 0; i < 1; i++) {
		    	cal.add(Calendar.MONTH, -1);
		    	strDate = fm.format(cal.getTime());
		    	STDYYYY_LIST.add(strDate);
		    }
		    
		    mainVO.setSTDYYYY_LIST(STDYYYY_LIST);
		    //mainVO.setPARAM_LOCATION(getSsLocation());
		    
		    List<HashMap<String, Object>> mtbfMttrInfo = mainService.selectMtbfMttrInfo(mainVO);
		    
		    resultMap.put("mtbfMttrInfo", mtbfMttrInfo);
		    
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * mtbfMttrDtl 데이터
	 * @author 김영환
	 * @since 2018.06.22
	 * @throws Exception
	 */
	@RequestMapping(value = "/mtbfMttrDtlInfo.do")
	@ResponseBody
	public Map<String, Object> mtbfMttrDtlInfo(HttpServletRequest request, MainVO mainVO) throws Exception {
		Map<String, Object> resultMap = new HashMap<>();
		SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM");
		try {

		    mainVO.setCompanyId(getSsCompanyId());
		    mainVO.setSubAuth(getSsSubAuth());
		    
			String year = mainVO.getPARAM_YEAR().isEmpty() ? DateTime.getYearStr() : mainVO.getPARAM_YEAR();
			String startDt = year + "-01";
		    Date date = fm.parse(startDt);
		    
		    Calendar cal = new GregorianCalendar(Locale.KOREA);
		    cal.setTime(date);
		    cal.add(Calendar.YEAR, -1);
		    
		    String strDate = fm.format(cal.getTime());
		    
		    List<String> STDYYYY_LIST = new ArrayList();
		    
		    STDYYYY_LIST.add(strDate);
		    for (int i = 0; i < 11; i++) {
		    	cal.add(Calendar.MONTH, 1);
		    	strDate = fm.format(cal.getTime());
		    	STDYYYY_LIST.add(strDate);
		    }
		    
		    mainVO.setSTDYYYY_LIST(STDYYYY_LIST);
		    
		    List<HashMap<String, Object>> previousYearMtbfMttrInfo = mainService.selectMtbfMttrInfo(mainVO);
		    
		    STDYYYY_LIST = new ArrayList();
		    cal.add(Calendar.MONTH, 1);
		    strDate = fm.format(cal.getTime());
		    STDYYYY_LIST.add(strDate);
		    
		    for (int i = 0; i < 11; i++) {
		    	cal.add(Calendar.MONTH, 1);
		    	strDate = fm.format(cal.getTime());
		    	STDYYYY_LIST.add(strDate);
		    }
		    
		    mainVO.setSTDYYYY_LIST(STDYYYY_LIST);
		    
		    List<HashMap<String, Object>> thisYearMtbfMttrInfo = mainService.selectMtbfMttrInfo(mainVO);
		    
		    resultMap.put("thisYearMtbfMttrInfo", thisYearMtbfMttrInfo);
		    resultMap.put("previousYearMtbfMttrInfo", previousYearMtbfMttrInfo);
		    
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/**
	 * epms 메인대시보드 : 정비실적 현황
	 * 
	 * @author 김영환
	 * @since 2018. 06. 18
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairResultChartData.do")
	@ResponseBody
	public Map<String, Object> repairResultChartData(MainVO mainVO, Model model){
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		try {
			mainVO.setCompanyId(getSsCompanyId());
			mainVO.setSubAuth(getSsSubAuth());
			mainVO.setSsPart(getSsPart());
			
			//팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				mainVO.setAuthType(CodeConstants.AUTH_CHIEF);
			}
			//정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
				mainVO.setAuthType(CodeConstants.AUTH_REPAIR);
			}
			
			List<HashMap<String, Object>> repairResultChartData = mainService.selectRepairResultChartData(mainVO);
			
			resultMap.put("repairResult", repairResultChartData);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("MainController.repairResultChartData Error !" + e.toString());
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * epms 메인대시보드 : 정비실적 현황
	 * 
	 * @author 김영환
	 * @since 2018. 06. 18
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairEMChartData.do")
	@ResponseBody
	public Map<String, Object> repairEMChartData(MainVO mainVO, Model model){
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		try {
			mainVO.setCompanyId(getSsCompanyId());
			mainVO.setSubAuth(getSsSubAuth());
			List<HashMap<String, Object>> repairEM = mainService.selectRepairEMChartData(mainVO);
			
			resultMap.put("repairEM", repairEM);
			
			resultMap.put("ssPart", getSsPart());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("MainController.repairEMInfo Error !" + e.toString());
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * epms 메인대시보드 : 정비실적 현황 상세페이지
	 * 
	 * @author 김영환
	 * @since 2018. 06. 18
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/mainRepairResultListDtl.do")
	public String mainRepairResultListDtl(MainVO mainVO, Model model){
		
		try {
			mainVO.setCompanyId(getSsCompanyId());
			mainVO.setSubAuth(getSsSubAuth());
			// 권한별 공장목록 조회
			List<HashMap<String, Object>> locationList = mainService.selectLocationListOpt(mainVO);
			model.addAttribute("locationList", locationList);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("MainController.mainRepairResultListDtl Error !" + e.toString());
		}
		
		
		return "/epms/main/mainRepairResultListDtl";
	}
	
	/**
	 * epms 메인대시보드 : 정비실적 결제관련 상세리스트
	 * 
	 * @author 김영환
	 * @since 2018. 06. 18
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */                      
	@RequestMapping(value = "/popRepairApprListOptData.do")
	public String popRepairApprListOptData(MainVO mainVO, Model model) throws Exception {
		
		try {

			mainVO.setCompanyId(getSsCompanyId());
			
			if(mainVO.getAPPR_STATUS().equals("REPAIR_REQUEST")){
				mainVO.setAPPR_STATUS("(01, 02, 03, 04)");
			}else if (mainVO.getAPPR_STATUS().equals("REPAIR_APPR")) {
				mainVO.setAPPR_STATUS("(01)");
			}else if (mainVO.getAPPR_STATUS().equals("REPAIR_REJECT")) {
				mainVO.setAPPR_STATUS("(03)");
			}else if (mainVO.getAPPR_STATUS().equals("REPAIR_CANCEL")) {
				mainVO.setAPPR_STATUS("(04)");
			}
			
			//결재현황 목록
			List<HashMap<String, Object>> list = mainService.selectRepairApprListOpt(mainVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("MainController.popRepairApprListOptData Error !" + e.toString());
		}
		
		return "/epms/repair/appr/repairApprListData";
	}
	
	/**
	 * epms 메인대시보드 : 정비실적 정비상태 관련 상세리스트
	 *
	 * @author 김영환
	 * @since 2018.06.19
	 * @param RepairResultVO repairResultVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popRepairResultListOptData.do")
	public String popRepairResultListOptData(MainVO mainVO, Model model) throws Exception{
		try {
			
			mainVO.setCompanyId(getSsCompanyId());
			
			// 차트 데이터가 아닐경우
			if("N".equals(mainVO.getFlag())){
				if(mainVO.getSTATUS().equals("REPAIR_COMPLETE")){
					mainVO.setREPAIR_STATUS("(03)");
				}else if (mainVO.getSTATUS().equals("REPAIR_ONGOING")) {
					mainVO.setREPAIR_STATUS("(01, 02)");
				}else if (mainVO.getSTATUS().equals("REPAIR_ASSIGN")) {
					mainVO.setREPAIR_STATUS("(02, 03)");
				}
			}
			
			//정비실적현황 목록
			List<HashMap<String, Object>> list= mainService.selectRepairResultListOpt(mainVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			logger.error("MainController.popRepairResultListData Error !");
			e.printStackTrace();
		}
		return "/epms/repair/result/repairResultListData";
	}
	
	/**
	 * epms 메인대시보드 : 정비실적 현황 상세페이지 정비 카운트
	 * 
	 * @author 김영환
	 * @param ComVO comVO
	 * @return Map<String, Object>
	 * @since 2018.06.14
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectRepairResultInfo.do")
	@ResponseBody
	public Map<String, Object> selectRepairResultInfo(MainVO mainVO) throws Exception {
		
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			
			mainVO.setCompanyId(getSsCompanyId());
			mainVO.setSubAuth(getSsSubAuth());
			
			// 1. 정비현황 : 정비요청, 정비완료, 진행중, 반려, 정비취소
			HashMap<String, Object> repairInfo= mainService.selectRepairResultStatus(mainVO);
			resultMap.put("repairInfo", repairInfo);
			
			// 정비유형 별 정비건
			List<HashMap<String, Object>> repair_type = mainService.selectRepairResultListOpt1(mainVO);
			resultMap.put("REPAIR_TYPE", repair_type);
			
			// 고장시간 별 정비건
			HashMap<String, Object> trouble_time = mainService.selectRepairResultListOpt2(mainVO);
			resultMap.put("TROUBLE_TIME", trouble_time);
			
			// 고장유형 별 정비건
			List<HashMap<String, Object>> trouble_type1 = mainService.selectRepairResultListOpt3(mainVO);
			resultMap.put("TROUBLE_TYPE1", trouble_type1);
			
			// 조치유형 별 정비건
			List<HashMap<String, Object>> trouble_type2 = mainService.selectRepairResultListOpt4(mainVO);
			resultMap.put("TROUBLE_TYPE2", trouble_type2);
			
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
		}
		
		return resultMap;
	}
	
	/**
	 * epms 메인대시보드 : 정비원 업무현황
	 * 
	 * @author 김영환
	 * @since 2018. 06. 19
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectPartInfo.do")
	@ResponseBody
	public Map<String, Object> selectPartInfo(MainVO mainVO){
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		try {
			mainVO.setCompanyId(getSsCompanyId());
			mainVO.setLOCATION(getSsLocation());
			
			List<HashMap<String, Object>> partInfo = mainService.selectPartInfo(mainVO);
			
			resultMap.put("partInfo", partInfo);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("MainController.wokerInfo Error !" + e.toString());
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * epms 메인대시보드 : 정비원 업무현황
	 * 
	 * @author 김영환
	 * @since 2018. 06. 19
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/wokerInfo.do")
	@ResponseBody
	public Map<String, Object> wokerInfo(MainVO mainVO){
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		try {
			mainVO.setCompanyId(getSsCompanyId());
			if("".equals(mainVO.getLOCATION())){
				mainVO.setLOCATION(getSsLocation());
			}
			List<HashMap<String, Object>> wokerInfo = mainService.selectWokerInfo(mainVO);
			resultMap.put("wokerInfo", wokerInfo);
			
			List<HashMap<String, Object>> partInfo = mainService.selectPartInfo(mainVO);
			resultMap.put("partInfo", partInfo);
			
			resultMap.put("ssPart", getSsPart());
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("MainController.wokerInfo Error !" + e.toString());
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * epms 메인대시보드 : 정비원 업무현황 상세페이지 메인화면
	 * 
	 * @author 김영환
	 * @since 2018. 06. 19
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/mainWorkStatListDtl.do")
	public String mainWorkStatListDtl(MainVO mainVO, Model model){
		
		try {
			mainVO.setCompanyId(getSsCompanyId());
			mainVO.setSubAuth(getSsSubAuth());
			// 권한별 공장목록 조회
			List<HashMap<String, Object>> locationList = mainService.selectLocationListOpt(mainVO);
			model.addAttribute("locationList", locationList);
			
			if("".equals(mainVO.getLOCATION())){
				mainVO.setLOCATION(getSsLocation());
			}
			// 정비원 목록 조회 
			List<HashMap<String, Object>> wokerInfo = mainService.selectWokerInfo(mainVO);
			model.addAttribute("WORKINFO_LIST", wokerInfo);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("MainController.mainWorkStatListDtl Error !" + e.toString());
		}
		
		return "/epms/main/mainWorkStatListDtl";
	}
	
	/**
	 * epms 메인대시보드 : 돌발정비 현황 
	 * @author 김영환
	 * @since 2018. 06. 19
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/repairEMInfo.do")
	@ResponseBody
	public Map<String, Object> repairEMInfo(MainVO mainVO){
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		try {
			mainVO.setCompanyId(getSsCompanyId());
			mainVO.setSsLocation(getSsLocation());
			List<HashMap<String, Object>> repairEMInfo = mainService.selectRepairEMInfo(mainVO);
			
			resultMap.put("repairEMInfo", repairEMInfo);
			
			resultMap.put("ssPart", getSsPart());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("MainController.repairEMInfo Error !" + e.toString());
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * epms 메인대시보드 : 돌발정비 현황 상세페이지\
	 * 
	 * @author 김영환
	 * @since 2018. 06. 19
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/mainTroubleListDtl.do")
	public String mainTroubleListDtl(MainVO mainVO, Model model){
		
		try {
			mainVO.setCompanyId(getSsCompanyId());
			mainVO.setSubAuth(getSsSubAuth());
			model.addAttribute("companyId", getSsCompanyId());
			// 권한별 공장목록 조회
			List<HashMap<String, Object>> locationList = mainService.selectLocationListOpt(mainVO);
			model.addAttribute("locationList", locationList);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("MainController.mainRepairResultListDtl Error !" + e.toString());
		}
		
		return "/epms/main/mainTroubleListDtl";
	}
	
	/**
	 * 돌발정비현황 차트 데이트
	 * 
	 * @author 김영환
	 * @since 2018.06.22
	 * @throws Exception
	 */
	@RequestMapping(value = "/troubleListChartData.do")
	@ResponseBody
	public Map<String, Object> troubleListChartData(HttpServletRequest request, MainVO mainVO) throws Exception {
		Map<String, Object> resultMap = new HashMap<>();
		SimpleDateFormat fm = new SimpleDateFormat("yyyyMM");
		try {

		    mainVO.setCompanyId(getSsCompanyId());
			
			String year = mainVO.getPARAM_YEAR().isEmpty() ? DateTime.getYearStr() : mainVO.getPARAM_YEAR();
			String startDt = year + "01";
		    Date date = fm.parse(startDt);
		    
		    Calendar cal = new GregorianCalendar(Locale.KOREA);
		    cal.setTime(date);
		    cal.add(Calendar.YEAR, -1);
		    
		    String strDate = fm.format(cal.getTime());
		    
		    List<String> STDYYYY_LIST = new ArrayList();
		    
		    STDYYYY_LIST.add(strDate);
		    for (int i = 0; i < 11; i++) {
		    	cal.add(Calendar.MONTH, 1);
		    	strDate = fm.format(cal.getTime());
		    	STDYYYY_LIST.add(strDate);
		    }
		    
		    mainVO.setSTDYYYY_LIST(STDYYYY_LIST);
		    
		    List<HashMap<String, Object>> lastYearTroubleList = mainService.selectTroubleListChartData(mainVO);
		    
		    STDYYYY_LIST = new ArrayList();
		    cal.add(Calendar.MONTH, 1);
		    strDate = fm.format(cal.getTime());
		    STDYYYY_LIST.add(strDate);
		    
		    for (int i = 0; i < 11; i++) {
		    	cal.add(Calendar.MONTH, 1);
		    	strDate = fm.format(cal.getTime());
		    	STDYYYY_LIST.add(strDate);
		    }
		    
		    mainVO.setSTDYYYY_LIST(STDYYYY_LIST);
		    List<HashMap<String, Object>> thisYearTroubleList = mainService.selectTroubleListChartData(mainVO);
		    
		    resultMap.put("thisYearTroubleList", thisYearTroubleList);
		    resultMap.put("lastYearTroubleList", lastYearTroubleList);
		    
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
	
	/** 
	 * 대시보드 : 정비요청, 결제대기, 반려, 정비취소 상태 레이어 팝업 Grid Layout
	 *
	 * @author 김영환
	 * @since 2018.03.05
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popMainRepairApprListLayout.do")
	public String popMainRepairApprListLayout() throws Exception {
		return "/epms/main/popMainRepairApprListLayout";
	}
	
	/**
	 * 대시보드 : 정비요청, 결제대기, 반려, 정비취소 상태 레이어 팝업 Grid Data
	 *
	 * @author 김영환
	 * @since 2018.03.05
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */                       
	@RequestMapping(value = "/popRepairApprListData.do")
	public String popRepairApprListData(RepairApprVO repairApprVO, Model model) throws Exception {
		
		try {
			repairApprVO.setSubAuth(getSsSubAuth());     	 //세션_조회권한
			repairApprVO.setSsLocation(getSsLocation());     //세션_공장위치
			repairApprVO.setSsRemarks("02");       			 //세션_파트 분리/통합 여부
			repairApprVO.setSsPart(getSsPart());			 //세션_파트
			repairApprVO.setCompanyId(getSsCompanyId());     //세션_공장코드
			
			if(repairApprVO.getAPPR_STATUS().equals("REPAIR_REQUEST")){
				repairApprVO.setAPPR_STATUS("(01, 02, 03, 04)");
			}else if (repairApprVO.getAPPR_STATUS().equals("REPAIR_APPR")) {
				repairApprVO.setAPPR_STATUS("(01)");
			}else if (repairApprVO.getAPPR_STATUS().equals("REPAIR_REJECT")) {
				repairApprVO.setAPPR_STATUS("(03)");
			}else if (repairApprVO.getAPPR_STATUS().equals("REPAIR_CANCEL")) {
				repairApprVO.setAPPR_STATUS("(04)");
			}
			
			//설정 기간 조회
			Calendar cal = Calendar.getInstance();
			String THIS_YEAR = String.valueOf(cal.get(Calendar.YEAR));
			String THIS_MONTH = String.format("%02d", Integer.parseInt(String.valueOf(cal.get(Calendar.MONTH)+1)));
			
			repairApprVO.setStartDt(THIS_YEAR +"-"+ THIS_MONTH +"-"+ "01");
			
			// 월 말일 날짜 구하기
			SimpleDateFormat transDate = new SimpleDateFormat("yyyyMMdd");
			Date tdate = transDate.parse(repairApprVO.getStartDt());
			cal.setTime(tdate);
			
			int endDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			String endOfMonth = String.valueOf(endDay);
			
			repairApprVO.setEndDt(THIS_YEAR +"-"+ THIS_MONTH +"-"+ endOfMonth);
			
			//팀장일 경우
			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
				repairApprVO.setAuthType(CodeConstants.AUTH_CHIEF);
			}
			//정비원일 경우
			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
				repairApprVO.setAuthType(CodeConstants.AUTH_REPAIR);
			}
			
			//결재현황 목록
			List<HashMap<String, Object>> list = repairApprService.selectRepairApprList(repairApprVO);
			model.addAttribute("list", list);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("MainController.popRepairApprListData Error !" + e.toString());
		}
		
		return "/epms/repair/appr/repairApprListData";
	}
	
	/**
	 * 대시보드 : 정비완료, 정비 진행중 상태 레이어 팝업 Grid Layout
	 *
	 * @author 김영환
	 * @since 2018.03.06
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popMainRepairResultListLayout.do")
	public String popMainRepairResultListLayout() throws Exception{
		return "/epms/main/popMainRepairResultListLayout";
	}
	
	/**
	 * 대시보드 : 정비완료, 정비 진행중 상태 레이어 팝업 Grid Data
	 *
	 * @author 김영환
	 * @since 2018.06.19
	 * @param RepairResultVO repairResultVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popRepairResultListData.do")
	public String popRepairResultListData(RepairResultVO repairResultVO, Model model) throws Exception{
		try {
			repairResultVO.setUserId(getSsUserId());
			repairResultVO.setSubAuth(getSsSubAuth());
			repairResultVO.setSsPart(getSsPart());     
			
			if(repairResultVO.getREPAIR_STATUS().equals("REPAIR_COMPLETE")){
				repairResultVO.setREPAIR_STATUS("(03)");
			}else if (repairResultVO.getREPAIR_STATUS().equals("REPAIR_ONGOING")) {
				repairResultVO.setREPAIR_STATUS("(01, 02)");
			}else if (repairResultVO.getREPAIR_STATUS().equals("REPAIR_ASSIGN")) {
				repairResultVO.setREPAIR_STATUS("(02, 03)");
			}
			
			//설정 기간 조회
			Calendar cal = Calendar.getInstance();
			String THIS_YEAR = String.valueOf(cal.get(Calendar.YEAR));
			String THIS_MONTH = String.format("%02d", Integer.parseInt(String.valueOf(cal.get(Calendar.MONTH)+1)));
			
			repairResultVO.setStartDt(THIS_YEAR +"-"+ THIS_MONTH +"-"+ "01");
			
			// 월 말일 날짜 구하기
			SimpleDateFormat transDate = new SimpleDateFormat("yyyyMMdd");
			Date tdate = transDate.parse(repairResultVO.getStartDt());
			cal.setTime(tdate);
			
			int endDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			String endOfMonth = String.valueOf(endDay);
			
			repairResultVO.setEndDt(THIS_YEAR +"-"+ THIS_MONTH +"-"+ endOfMonth);
			
			//정비실적현황 목록
			List<HashMap<String, Object>> list= repairResultService.selectRepairResultList(repairResultVO);
			model.addAttribute("list", list);
			
			model.addAttribute("flag", "MAIN");
			
		} catch (Exception e) {
			logger.error("MainController.popRepairResultListData Error !");
			e.printStackTrace();
		}
		return "/epms/repair/result/repairResultListData";
	}
	
	/**
	 * 대시보드 - 정비완료 상세화면
	 * 
	 * @author 김영환
	 * @since 2018.03.12 
	 * @param RepairResultVO repairResultVO
	 * @param  Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popMainRepairResultForm.do")
	public String popMainRepairResultForm(RepairResultVO repairResultVO, Model model) throws Exception{
		
		repairResultVO.setCompanyId(getSsCompanyId());
		repairResultVO.setSsLocation(getSsLocation());
		
		//정비실적 상세 조회
		HashMap<String, Object> repairResultDtl = repairResultService.selectRepairResultDtl(repairResultVO);
		model.addAttribute("repairResultDtl", repairResultDtl);
		
		//고장사진
		if(!("".equals(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO1"))) || repairResultDtl.get("ATTACH_GRP_NO1") == null)){
			AttachVO attachVO = new AttachVO();
			attachVO.setATTACH_GRP_NO(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO1")));
			List<HashMap<String, Object>> attachList1 = attachService.selectFileList(attachVO);
			model.addAttribute("attachList1", attachList1);
		}
		
		if(!"".equals(String.valueOf(repairResultDtl.get("REPAIR_RESULT"))) && repairResultDtl.get("REPAIR_RESULT") != null){
			//정비실적에 등록된 자재 목록
			List<HashMap<String, Object>> usedMaterial = repairResultService.selectRepairUsedMaterialList(repairResultVO);
			model.addAttribute("usedMaterial", usedMaterial);
			
			//정비실적에 등록된 정비원 조회
			repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //01:정비실적, 02:고장원인분석
			repairResultVO.setWORK_ID(repairResultVO.getREPAIR_RESULT());
			List<HashMap<String, Object>> workMemberList = repairResultService.selectWorkMemberList(repairResultVO);
			model.addAttribute("repairRegMember", workMemberList);
			
			//정비사진
			if(!("".equals(repairResultDtl.get("ATTACH_GRP_NO2")) || repairResultDtl.get("ATTACH_GRP_NO2") == null)){
				AttachVO attachVO = new AttachVO();
				attachVO.setATTACH_GRP_NO(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO2")));
				List<HashMap<String, Object>> attachList2 = attachService.selectFileList(attachVO);
				model.addAttribute("attachList2", attachList2);
			}
			
			//고장유형 목록
			List<HashMap<String, Object>> troubleList1 = repairResultService.selectTroubleList1();
			model.addAttribute("troubleList1", troubleList1);
			
			//조치 목록
			List<HashMap<String, Object>> troubleList2 = repairResultService.selectTroubleList2();
			model.addAttribute("troubleList2", troubleList2);
		}
		
		//로그인한 사용자 ID
		model.addAttribute("userId", getSsUserId());
		
		return "/epms/main/popMainRepairResultForm";
	}
	
	/**
	 * 대시보드 - 정비 진행중 상세화면
	 * 
	 * @author 김영환
	 * @since 2018.03.12 
	 * @param RepairResultVO repairResultVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popMainRepairResultRegForm.do")
	public String popRepairResultRegForm(RepairResultVO repairResultVO, Model model) throws Exception{
		
		repairResultVO.setCompanyId(getSsCompanyId());
		repairResultVO.setSsLocation(getSsLocation());
		
		//정비실적 상세 조회
		HashMap<String, Object> repairResultDtl = repairResultService.selectRepairResultDtl(repairResultVO);
		model.addAttribute("repairResultDtl", repairResultDtl);
		
		//고장사진
		if(!("".equals(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO1"))) || repairResultDtl.get("ATTACH_GRP_NO1") == null)){
			AttachVO attachVO = new AttachVO();
			attachVO.setATTACH_GRP_NO(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO1")));
			List<HashMap<String, Object>> attachList1 = attachService.selectFileList(attachVO);
			model.addAttribute("attachList1", attachList1);
		}
		
		if(!"".equals(String.valueOf(repairResultDtl.get("REPAIR_RESULT"))) && repairResultDtl.get("REPAIR_RESULT") != null){
			//정비실적에 등록된 자재 목록
			List<HashMap<String, Object>> usedMaterial = repairResultService.selectRepairUsedMaterialList(repairResultVO);
			model.addAttribute("usedMaterial", usedMaterial);
			
			//정비실적에 등록된 정비원 조회
			repairResultVO.setWORK_TYPE(CodeConstants.EPMS_WORK_TYPE_REPAIR); //01:정비실적, 02:고장원인분석
			repairResultVO.setWORK_ID(repairResultVO.getREPAIR_RESULT());
			List<HashMap<String, Object>> workMemberList = repairResultService.selectWorkMemberList(repairResultVO);
			model.addAttribute("repairRegMember", workMemberList);
			
			//정비사진
			if(!("".equals(repairResultDtl.get("ATTACH_GRP_NO2")) || repairResultDtl.get("ATTACH_GRP_NO2") == null)){
				AttachVO attachVO = new AttachVO();
				attachVO.setATTACH_GRP_NO(String.valueOf(repairResultDtl.get("ATTACH_GRP_NO2")));
				List<HashMap<String, Object>> attachList2 = attachService.selectFileList(attachVO);
				model.addAttribute("attachList2", attachList2);
			}
			
			//고장유형 목록
			List<HashMap<String, Object>> troubleList1 = repairResultService.selectTroubleList1();
			model.addAttribute("troubleList1", troubleList1);
			
			//조치 목록
			List<HashMap<String, Object>> troubleList2 = repairResultService.selectTroubleList2();
			model.addAttribute("troubleList2", troubleList2);
		}
		
		model.addAttribute("userId", getSsUserId());
		
		return "/epms/main/popMainRepairResultRegForm";
	}

	/**
	 * 예방보전일정관리/배정
	 * 일별 예방점검 리스트 조회  Grid Layout
	 * @author 김영환
	 * @since 2018.06.19
	 * @param preventivePlanVO
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	//2019.03.25 던킨 요청으로 사용안함
//	@SuppressWarnings("unchecked")
//	@RequestMapping(value = "/popMainPreventivePlanListLayout.do")
//	public String popMainPreventivePlanListLayout(PreventivePlanVO preventivePlanVO, Model model){
//		
//		try{
//
//			preventivePlanVO.setSubAuth(getSsSubAuth());	 //세션_조회권한
//
//			//정비파트원 목록
//			List<HashMap<String, Object>> list4 = preventivePlanService.repairMemberList(preventivePlanVO);
//			HashMap<String, Object> repairMemberListOpt  = ObjUtil.list2Map(list4, "|");
//			model.addAttribute("repairMemberListOpt", repairMemberListOpt);
//			
//			//정비파트원 목록(공장별,파트별):RELATED
//			List<HashMap<String, Object>> list5 = preventivePlanService.repairMemberList2(preventivePlanVO);
//			String relatedCombo = ObjUtil.list2MapRelated(list5, "|", "CODE", "NAME");
//			model.addAttribute("relatedCombo", relatedCombo);
//		}catch(Exception e){
//			e.printStackTrace();
//			logger.error("MainController.popMainPreventivePlanListLayout Error !" + e.toString());
//		}
//		
//		return "/epms/main/popMainPreventivePlanListLayout";
//	}
	
	/**
	 * 예방보전일정관리/배정
	 * 일별 예방점검 리스트 조회  Grid Layout
	 * @author 김영환
	 * @since 2018.06.19
	 * @param preventivePlanVO
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/popMainPreventiveResultListLayout.do")
	public String popMainPreventiveResultListLayout(PreventivePlanVO preventivePlanVO, Model model){
		
		try{
			
			preventivePlanVO.setSubAuth(getSsSubAuth());	 //세션_조회권한

			//정비파트원 목록
			List<HashMap<String, Object>> list4 = preventivePlanService.repairMemberList(preventivePlanVO);
			HashMap<String, Object> repairMemberListOpt  = ObjUtil.list2Map(list4, "|");
			model.addAttribute("repairMemberListOpt", repairMemberListOpt);
			
			//정비파트원 목록(공장별,파트별):RELATED
			List<HashMap<String, Object>> list5 = preventivePlanService.repairMemberList2(preventivePlanVO);
			String relatedCombo = ObjUtil.list2MapRelated(list5, "|", "CODE", "NAME");
			model.addAttribute("relatedCombo", relatedCombo);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MainController.popMainPreventiveResultListLayout Error !" + e.toString());
		}
		
		return "/epms/main/popMainPreventiveResultListLayout";
	}
	
	/**
	 * 일별 예방점검 리스트 조회 
	 * @author 김영환
	 * @since 2018.06.18
	 * @param preventivePlanVO
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	// 2019.03.25 던킨 요청으로 사용안함
//	@RequestMapping(value = "/popMainPreventivePlanListData.do")
//	public String popMainPreventivePlanListData(PreventivePlanVO preventivePlanVO, Model model){
//		try {
//			preventivePlanVO.setSubAuth(getSsSubAuth());
//			preventivePlanVO.setSsPart(getSsPart());
//			if(preventivePlanVO.getPREVENTIVE_STATUS().equals("PREVENTIVE_PLAN")){ //전체
//				preventivePlanVO.setPREVENTIVE_STATUS("(01, 02, 03, 09)");
//			}else if (preventivePlanVO.getPREVENTIVE_STATUS().equals("PREVENTIVE_COMPLETE")) { //정비필요, 완료
//				preventivePlanVO.setPREVENTIVE_STATUS("(02, 03)");
//			}else if (preventivePlanVO.getPREVENTIVE_STATUS().equals("PREVENTIVE_UNCHECK")) {  //미점검완료
//				preventivePlanVO.setPREVENTIVE_STATUS("(09)");
//			}else if (preventivePlanVO.getPREVENTIVE_STATUS().equals("PREVENTIVE_ONGOING")) { //대기
//				preventivePlanVO.setPREVENTIVE_STATUS("(01)");
//			}
//			//설정 기간 조회
//			Calendar cal = Calendar.getInstance();
//			String THIS_YEAR = ""; 
//			String THIS_MONTH = "";
//			String StartDt = preventivePlanVO.getStartDt();
//			
//			if(preventivePlanVO.getStartDt() != "") {
//				THIS_YEAR = preventivePlanVO.getStartDt().substring(0, 4);
//				THIS_MONTH = StartDt.substring(StartDt.length()-2,  StartDt.length());
//			} else {
//				THIS_YEAR = String.valueOf(cal.get(Calendar.YEAR));
//				THIS_MONTH = String.format("%02d", Integer.parseInt(String.valueOf(cal.get(Calendar.MONTH)+1)));
//			}
//			
//			preventivePlanVO.setStartDt(THIS_YEAR + THIS_MONTH + "01");
//			//월 말일 날짜 구하기
//			SimpleDateFormat transDate = new SimpleDateFormat("yyyyMMdd");
//			Date tdate = transDate.parse(preventivePlanVO.getStartDt());
//			cal.setTime(tdate);
//			
//			int endDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
//			String endOfMonth = String.valueOf(endDay);
//			
//			preventivePlanVO.setEndDt(THIS_YEAR + THIS_MONTH + endOfMonth);
//			
//			//팀장일 경우
//			if(getSsSubAuth().indexOf(CodeConstants.AUTH_CHIEF)>-1){
//				preventivePlanVO.setAuthType(CodeConstants.AUTH_CHIEF);
//			}
//			//정비원일 경우
//			else if(getSsSubAuth().indexOf(CodeConstants.AUTH_REPAIR)>-1 && !"".equals(getSsPart())){
//				preventivePlanVO.setAuthType(CodeConstants.AUTH_REPAIR);
//			}
//			
//			//예방보전 배정목록
//			List<HashMap<String, Object>> list= preventivePlanService.preventivePlanList(preventivePlanVO);
//			model.addAttribute("list", list);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.error("MainController.popMainPreventivePlanListData Error !" + e.toString());
//		}
//		
//		return "/epms/main/popMainPreventivePlanListData";
//	}
	
	/**
	 * epms 메인대시보드 : 예방보전 현황 상세페이지
	 * 
	 * @author 김영환
	 * @since 2018. 06. 20
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	// 2019.03.25 던킨 요청으로 사용안함
//	@RequestMapping(value = "/mainPreventiveListDtl.do")
//	public String mainPreventiveListDtl(MainVO mainVO, Model model){
//		
//		try {
//			mainVO.setCompanyId(getSsCompanyId());
//			mainVO.setSubAuth(getSsSubAuth());
//			// 권한별 공장목록 조회
//			List<HashMap<String, Object>> locationList = mainService.selectLocationListOpt(mainVO);
//			model.addAttribute("locationList", locationList);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.error("MainController.mainPreventiveListDtl Error !" + e.toString());
//		}
//		
//		
//		return "/epms/main/mainPreventiveListDtl";
//	}
	
	
	/**
	 * 대시보드 - 예방보전 현황 상세 데이터
	 * @author 김영환
	 * @since 2018. 06. 20
	 * @param MainVO mainVO ComVO comVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	// 2019.03.25 던킨 요청으로 사용안함
//	@RequestMapping(value = "/mainPreventiveListDtlAjax.do")
//	@ResponseBody
//	public Map<String, Object> mainPreventiveListDtlAjax(MainVO mainVO, ComVO comVO){
//		Map<String, Object> resultMap = new HashMap<String, Object>();
//		try {
//			
//			mainVO.setCompanyId(getSsCompanyId());
//			mainVO.setSubAuth(getSsSubAuth());
//			comVO.setSubAuth(getSsSubAuth());
//						
//			// 예방보전현황 : 점검 완료, 미점검 완료, 진행중
//			HashMap<String, Object> preventiveInfo= comService.selectPreventiveInfo(comVO);
//			
//			List<HashMap<String, Object>> cbmChartInfo = mainService.preventiveInfoDtlCBMData(mainVO);
//			List<HashMap<String, Object>> tbmChartInfo = mainService.preventiveInfoDtlTBMData(mainVO);
//			
//			List<HashMap<String, Object>> cbmDecisionChartInfo = mainService.preventiveInfoDtlCbmDecisionChartData(mainVO);
//			List<HashMap<String, Object>> performChartInfo = mainService.preventiveInfoDtlPerformChartData(mainVO);
//			
//			HashMap<String, Object> dayOverChartInfo = mainService.preventiveInfoDtlDayOverList(mainVO);
//			
//			resultMap.put("preventiveInfo", preventiveInfo);
//			resultMap.put("cbmChartInfo", cbmChartInfo);
//			resultMap.put("tbmChartInfo", tbmChartInfo);
//			resultMap.put("cbmDecisionChartInfo", cbmDecisionChartInfo);
//			resultMap.put("performChartInfo", performChartInfo);
//			resultMap.put("dayOverChartInfo", dayOverChartInfo);
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.error("MainController.mainPreventiveListDtlAjax Error !" + e.toString());
//			
//			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
//		}
//		
//		return resultMap;
//	}
	
	/**
	 * epms 메인대시보드 : MTBF/MTTR/가용도
	 * 
	 * @author 김영환
	 * @since 2018. 06. 21
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/mainMtbfMttrDtl.do")
	public String mainMtbfMttrDtl(MainVO mainVO, Model model){
	
		try {
			mainVO.setCompanyId(getSsCompanyId());
			mainVO.setSubAuth(getSsSubAuth());
			// 권한별 공장목록 조회
			List<HashMap<String, Object>> locationList = mainService.selectLocationListOpt(mainVO);
			model.addAttribute("locationList", locationList);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("MainController.mainRepairResultListDtl Error !" + e.toString());
		}
		
		return "/epms/main/mainMtbfMttrDtl";
	}
	
}
