package aspn.hello.epms.equipment.history.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import aspn.hello.epms.equipment.history.model.EquipmentHistoryVO;
import aspn.hello.epms.equipment.history.service.EquipmentHistoryService;
import aspn.hello.epms.equipment.master.model.EquipmentMasterVO;
import aspn.hello.epms.equipment.master.service.EquipmentMasterService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.config.SFAConstants;
import aspn.com.common.util.BizException;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.JsUtil;
import aspn.com.common.util.JsonUtil;
import aspn.com.common.util.ObjUtil;
import aspn.com.common.util.SessionUtil;
import aspn.com.common.util.TreeGridUtil;
import aspn.com.common.util.Utility;
import aspn.hello.com.model.AttachVO;
import aspn.hello.mem.model.MemberVO;
import aspn.hello.com.service.AttachService;

/**
 * [기기이력] Controller Class
 * @author 서정민
 * @since 2018.02.07
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.07		서정민			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/equipment/history")
public class EquipmentHistoryController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	EquipmentHistoryService equipmentHistoryService;
	
	@Autowired
	EquipmentMasterService equipmentMasterService;
	

	/**
	 * [기기이력] 팝업화면
	 *
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentHistoryVO equipmentHistoryVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popEquipmentHistoryList.do")
	@ResponseBody
	public HashMap<String, Object> popEquipmentHistoryList(EquipmentMasterVO equipmentMasterVO, Model model) throws Exception {
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		try {
			result.put("startDt", DateTime.requestMmDay("1","yyyyMMdd"));
			result.put("endDt", DateTime.getShortDateString());
			
			HashMap<String, Object> item = equipmentMasterService.selectEquipmentMasterDtl(equipmentMasterVO);
			result.put("item", item);
			
		} catch (Exception e) {
			logger.error("EquipmentHistoryController.popEquipmentHistoryList > " + e.toString());
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * [기기이력] 그리드 데이터
	 *
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentHistoryVO equipmentHistoryVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentHistoryListData.do")
	public String equipmentHistoryListData(EquipmentHistoryVO equipmentHistoryVO, Model model) throws Exception{
		
		try {
			//기기이력
			List<HashMap<String, Object>> list= equipmentHistoryService.selectEquipmentHistoryList(equipmentHistoryVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			logger.error("EquipmentHistoryController.equipmentHistoryListData > " + e.toString());
			e.printStackTrace();
		}
		return "/epms/equipment/history/equipmentHistoryListData";
	}
	
	/**
	 * [기기이력] 그리드 레이아웃
	 *
	 * @author 서정민
	 * @since 2018.02.07
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentHistoryListLayout.do")
	public String equipmentHistoryListLayout(Model model) throws Exception{
		
		return "/epms/equipment/history/equipmentHistoryListLayout";
	}
}
