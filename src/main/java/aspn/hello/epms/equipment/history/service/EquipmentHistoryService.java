package aspn.hello.epms.equipment.history.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aspn.hello.epms.equipment.history.model.EquipmentHistoryVO;

/**
 * [기기이력] Service Class
 * 
 * @author 서정민
 * @since 2018.02.07
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.07		서정민			최초 생성
 *   
 * </pre>
 */

public interface EquipmentHistoryService {
	
	
	//////////////////////////
	// 웹 및 공통
	//////////////////////////
	
	/**
	 * 설비마스터 기기이력 조회
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentHistoryVO equipmentHistoryVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectEquipmentHistoryList(EquipmentHistoryVO equipmentHistoryVO) throws Exception;

	//////////////////////////
	// 모바일
	//////////////////////////
	
	/**
	 * 모바일 - 설비 기기이력 조회
	 * 
	 * @author 김영환
	 * @since 2018.05.29
	 * @param EquipmentHistoryVO equipmentHistoryVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectEquipmentHistoryList2(EquipmentHistoryVO equipmentHistoryVO);

	
}
