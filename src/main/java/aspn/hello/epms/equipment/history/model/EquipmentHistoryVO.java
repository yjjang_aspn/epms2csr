package aspn.hello.epms.equipment.history.model;

import aspn.hello.com.model.AbstractVO;

import java.util.Arrays;

/**
 * [기기이력] VO Class
 * @author 서정민
 * @since 2018.02.07
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.07		서정민			최초 생성
 *   
 * </pre>
 */

public class EquipmentHistoryVO extends AbstractVO{
	
	private static final long serialVersionUID = 1L;
		
	/** EPMS_EQUIPMENT_HISTORY DB **/
	private String EQUIPMENT_HISTORY		= ""; //기기이력 ID
	private String EQUIPMENT				= ""; //위치(공장) ID
	private String HISTORY_TYPE				= ""; //이력유형(01:설비등록,02:라인이동,11:일반정비,12:예방정비,13:돌발정비,21:CBM,22:TBM)
	private String DATE_HISTORY				= ""; //날짜
	private String HISTORY_DESCRIPTION		= ""; //이력내용
	private String DETAIL_ID				= ""; //상세ID(정비실적ID, 점검실적ID)
	private String REG_ID					= ""; //등록자
	private String REG_DT					= ""; //등록일
	private String UPD_ID					= ""; //수정자
	private String UPD_DT					= ""; //수정일
	private String DEL_YN					= ""; //삭제여부
	private String SEQ_EQUIPMENT_HISTORY	= ""; //기기이력시퀀스
	
	public String getEQUIPMENT_HISTORY() {
		return EQUIPMENT_HISTORY;
	}
	public void setEQUIPMENT_HISTORY(String eQUIPMENT_HISTORY) {
		EQUIPMENT_HISTORY = eQUIPMENT_HISTORY;
	}
	public String getEQUIPMENT() {
		return EQUIPMENT;
	}
	public void setEQUIPMENT(String eQUIPMENT) {
		EQUIPMENT = eQUIPMENT;
	}
	public String getHISTORY_TYPE() {
		return HISTORY_TYPE;
	}
	public void setHISTORY_TYPE(String hISTORY_TYPE) {
		HISTORY_TYPE = hISTORY_TYPE;
	}
	public String getDATE_HISTORY() {
		return DATE_HISTORY;
	}
	public void setDATE_HISTORY(String dATE_HISTORY) {
		DATE_HISTORY = dATE_HISTORY;
	}
	public String getHISTORY_DESCRIPTION() {
		return HISTORY_DESCRIPTION;
	}
	public void setHISTORY_DESCRIPTION(String hISTORY_DESCRIPTION) {
		HISTORY_DESCRIPTION = hISTORY_DESCRIPTION;
	}
	public String getDETAIL_ID() {
		return DETAIL_ID;
	}
	public void setDETAIL_ID(String dETAIL_ID) {
		DETAIL_ID = dETAIL_ID;
	}
	public String getREG_ID() {
		return REG_ID;
	}
	public void setREG_ID(String rEG_ID) {
		REG_ID = rEG_ID;
	}
	public String getREG_DT() {
		return REG_DT;
	}
	public void setREG_DT(String rEG_DT) {
		REG_DT = rEG_DT;
	}
	public String getUPD_ID() {
		return UPD_ID;
	}
	public void setUPD_ID(String uPD_ID) {
		UPD_ID = uPD_ID;
	}
	public String getUPD_DT() {
		return UPD_DT;
	}
	public void setUPD_DT(String uPD_DT) {
		UPD_DT = uPD_DT;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getSEQ_EQUIPMENT_HISTORY() {
		return SEQ_EQUIPMENT_HISTORY;
	}
	public void setSEQ_EQUIPMENT_HISTORY(String sEQ_EQUIPMENT_HISTORY) {
		SEQ_EQUIPMENT_HISTORY = sEQ_EQUIPMENT_HISTORY;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
