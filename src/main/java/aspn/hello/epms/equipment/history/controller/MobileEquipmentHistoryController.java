package aspn.hello.epms.equipment.history.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import aspn.hello.epms.equipment.history.model.EquipmentHistoryVO;
import aspn.hello.epms.equipment.history.service.EquipmentHistoryService;
import aspn.hello.epms.equipment.master.model.EquipmentMasterVO;
import aspn.hello.epms.equipment.master.service.EquipmentMasterService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.config.SFAConstants;
import aspn.com.common.util.BizException;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.JsUtil;
import aspn.com.common.util.JsonUtil;
import aspn.com.common.util.ObjUtil;
import aspn.com.common.util.SessionUtil;
import aspn.com.common.util.TreeGridUtil;
import aspn.com.common.util.Utility;
import aspn.hello.com.model.AttachVO;
import aspn.hello.mem.model.MemberVO;
import aspn.hello.com.service.AttachService;

/**
 * [기기이력] Controller Class
 * @author 서정민
 * @since 2018.02.07
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.07		서정민			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/mobile/epms/equipment/history")
public class MobileEquipmentHistoryController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	EquipmentHistoryService equipmentHistoryService;
	

	/**
	 * [기기이력] 팝업화면
	 *
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentHistoryVO equipmentHistoryVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentHistoryList.do")
	@ResponseBody
	public Map<String, Object> popEquipmentHistoryList(EquipmentHistoryVO equipmentHistoryVO, Model model) throws Exception {
		
		Map<String, Object> resultMap = new HashMap<>();
		
		try {
			List<HashMap<String, Object>> history_list = equipmentHistoryService.selectEquipmentHistoryList2(equipmentHistoryVO);
			resultMap.put("HISTORY_LIST", history_list);
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			
		} catch (Exception e) {
			logger.error("MobileEquipmentHistoryController.equipmentHistoryList > " + e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
}
