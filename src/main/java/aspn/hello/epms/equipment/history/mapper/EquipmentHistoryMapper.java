package aspn.hello.epms.equipment.history.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.equipment.history.model.EquipmentHistoryVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [기기이력] Mapper Class
 * 
 * @author 서정민
 * @since 2018.02.07
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.07		서정민			최초 생성
 *   
 * </pre>
 */

@Mapper("equipmentHistoryMapper")
public interface EquipmentHistoryMapper {
	
	//////////////////////////
	// 웹 및 공통
	//////////////////////////
	
	/**
	 * 기기이력 조회
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentHistoryVO equipmentHistoryVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectEquipmentHistoryList(EquipmentHistoryVO equipmentHistoryVO);
	

	/**
	 * 기기이력 등록
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentHistoryVO equipmentHistoryVO
	 * @return int
	 * @throws Exception
	 */
	public int insertEquipmentHistory(EquipmentHistoryVO equipmentHistoryVO) throws Exception;

	/**
	 * 작업확정취소 시 기기이력 삭제
	 *
	 * @author 최준영
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */	
	void deleteEquipmentHistory(EquipmentHistoryVO equipmentHistoryVO);

	
	//////////////////////////
	// 모바일
	//////////////////////////
	
	/**
	 * 모바일 - 설비 기기이력 조회
	 * 
	 * @author 김영환
	 * @since 2018.05.29
	 * @param EquipmentHistoryVO equipmentHistoryVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectEquipmentHistoryList2(EquipmentHistoryVO equipmentHistoryVO);
	
}
