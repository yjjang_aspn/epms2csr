package aspn.hello.epms.equipment.history.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import aspn.com.common.util.Config;
import aspn.com.common.util.Utility;
import aspn.hello.com.mapper.AttachMapper;
import aspn.hello.com.model.AttachVO;
import aspn.hello.epms.equipment.history.mapper.EquipmentHistoryMapper;
import aspn.hello.epms.equipment.history.model.EquipmentHistoryVO;


/**
 * [기기이력] Business Implement Class
 * 
 * @author 서정민
 * @since 2018.02.07
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.07		서정민			최초 생성
 *   
 * </pre>
 */

@Service
public class EquipmentHistoryServiceImpl implements EquipmentHistoryService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	EquipmentHistoryMapper equipmentHistoryMapper;
	
	
	//////////////////////////
	// 웹 및 공통
	//////////////////////////
	
	/**
	 * 기기이력 조회
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentHistoryVO equipmentHistoryVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectEquipmentHistoryList(EquipmentHistoryVO equipmentHistoryVO) throws Exception {
		return equipmentHistoryMapper.selectEquipmentHistoryList(equipmentHistoryVO);
	}

	//////////////////////////
	// 모바일
	//////////////////////////	
	
	/**
	 * 모바일 - 설비 기기이력 조회
	 * 
	 * @author 김영환
	 * @since 2018.05.29
	 * @param EquipmentHistoryVO equipmentHistoryVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectEquipmentHistoryList2(EquipmentHistoryVO equipmentHistoryVO) {
		return equipmentHistoryMapper.selectEquipmentHistoryList2(equipmentHistoryVO);
	}
	
}
