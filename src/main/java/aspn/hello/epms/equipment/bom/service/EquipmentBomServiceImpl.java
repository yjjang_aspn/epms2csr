package aspn.hello.epms.equipment.bom.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.util.SessionUtil;
import aspn.com.common.util.Utility;
import aspn.hello.epms.equipment.bom.mapper.EquipmentBomMapper;
import aspn.hello.epms.equipment.bom.model.EquipmentBomVO;
import aspn.hello.epms.equipment.master.mapper.EquipmentMasterMapper;
import aspn.hello.epms.equipment.master.model.EquipmentMasterVO;
import aspn.hello.rfc.model.ZPM_BOM_DN_EXPORT;
import aspn.hello.rfc.model.ZPM_BOM_DN_IMPORT;
import aspn.hello.rfc.model.ZPM_BOM_DN_T_DEL;
import aspn.hello.rfc.model.ZPM_BOM_DN_T_ITEM;

/**
 * [설비BOM] Business Implement Class
 * 
 * @author 서정민
 * @since 2018.02.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.09		서정민			최초 생성
 *   2018.11.20		김영환			SAP 설비BOM 등록
 *   
 * </pre>
 */

@Service
public class EquipmentBomServiceImpl implements EquipmentBomService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	EquipmentBomMapper equipmentBomMapper;
	
	@Autowired
	EquipmentMasterMapper equipmentMasterMapper;
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 설비BOM 목록 조회
	 *
	 * @author 서정민
	 * @since 2018.02.09
	 * @param EquipmentBomVO equipmentBomVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectEquipmentBomList(EquipmentBomVO equipmentBomVO) throws Exception {
				
		return equipmentBomMapper.selectEquipmentBomList(equipmentBomVO);
	}
	
	/**
	 * 설비BOM 저장
	 *
	 * @author 서정민
	 * @since 2018.02.09
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param EquipmentBomVO equipmentBomVO
	 * @return void
	 * @throws Exception
	 */
	@Override
	public void equipmentBomEdit(List<HashMap<String, Object>> saveDataList, EquipmentBomVO equipmentBomVO) throws Exception{
				
		if(saveDataList != null){
			
			String equipment = equipmentBomVO.getEQUIPMENT();
			String[] equipment_arr;
			equipment_arr = equipment.split(",");
			
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				@SuppressWarnings("unchecked")
				EquipmentBomVO equipmentBomVO1 = Utility.toBean(tmpSaveData, equipmentBomVO.getClass());
				
				equipmentBomVO1.setUserId(equipmentBomVO.getUserId());
				
				if(tmpSaveData.containsKey("Changed")) {
					
					// 1. 자재 일괄매핑 팝업에서 BOM 등록할 경우
					if("insert".equals(equipmentBomVO1.getFlag())){	
						for(int i=0; i<equipment_arr.length; i++){
							// 설비ID 세팅
							equipmentBomVO1.setEQUIPMENT(equipment_arr[i]);
							
							// 설비BOM 등록 여부 확인
							if(equipmentBomMapper.existEquipmentBom(equipmentBomVO1) == 0){
								// 설비BOM 등록
								equipmentBomMapper.insertEquipmentBom(equipmentBomVO1);
								
							}
						}
					} 
					// 2. 설비BOM관리 화면에서 활성/비활성 수정할 경우
					else {
						// 설비BOM 활성/비활성 상태 업데이트
						equipmentBomMapper.updateEquipmentBom(equipmentBomVO1);
					}
				} else if (tmpSaveData.containsKey("Added")) {
					
				} else if(tmpSaveData.containsKey("Deleted")) {
					
				}
			}
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// SAP
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * SAP 설비BOM 다운로드 
	 *
	 * @author 김영환
	 * @since 2018.11.20
	 * @param ZPM_BOM_DN_EXPORT tables
	 * @param ZPM_BOM_DN_IMPORT zpm_bom_dn_import
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> updateEquipBom(ZPM_BOM_DN_EXPORT tables, ZPM_BOM_DN_IMPORT zpm_bom_dn_import) throws Exception {
		
		EquipmentBomVO equipmentBomVO = new EquipmentBomVO();
		String ssCompanyId = (String) SessionUtil.getAttribute("ssCompanyId");
		String ssUserId = (String) SessionUtil.getAttribute("ssUserId");		

		String OBJECT_TYPE = "";
		String OBJECT = "";
		String PLANT = "";
		
		if(tables != null){

			// 1. T_DEL
			if (tables.getZpm_bom_dn_t_del() != null){
				EquipmentBomVO equipmentBomVO1 = new EquipmentBomVO();
				
				for (ZPM_BOM_DN_T_DEL zpm_bom_dn_t_del : tables.getZpm_bom_dn_t_del()) {
					
					equipmentBomVO1 = new EquipmentBomVO();
					
					equipmentBomVO1.setCOMPANY_ID(ssCompanyId);							//회사코드
					equipmentBomVO1.setLOCATION(zpm_bom_dn_t_del.getWERKS());			//공장코드
					equipmentBomVO1.setTYPE(zpm_bom_dn_t_del.getZTYPE());				//OBJECT TYPE
					equipmentBomVO1.setOBJECT(zpm_bom_dn_t_del.getZOBJECT());			//OBJECT
					equipmentBomVO1.setUserId(ssUserId);								//사용자
					
					equipmentBomMapper.updateEqupBom(equipmentBomVO1);
				}
			}
			
			// 2. T_ITEM
			if (tables.getZpm_bom_dn_t_item() != null){
				
				EquipmentBomVO equipmentBomVO2 = new EquipmentBomVO();
				
				for (ZPM_BOM_DN_T_ITEM zpm_bom_dn_t_item : tables.getZpm_bom_dn_t_item()) {
					
					// 2-1. 기존 BOM 삭제
					if(!(OBJECT_TYPE.equals(zpm_bom_dn_t_item.getZTYPE()) && OBJECT.equals(zpm_bom_dn_t_item.getZOBJECT()) && PLANT.equals(zpm_bom_dn_t_item.getWERKS()))){
						
						equipmentBomVO2 = new EquipmentBomVO();
						
						equipmentBomVO2.setCOMPANY_ID(ssCompanyId);						//회사코드
						equipmentBomVO2.setLOCATION(zpm_bom_dn_t_item.getWERKS());		//공장코드
						equipmentBomVO2.setTYPE(zpm_bom_dn_t_item.getZTYPE());			//OBJECT TYPE
						equipmentBomVO2.setOBJECT(zpm_bom_dn_t_item.getZOBJECT());		//OBJECT
						equipmentBomVO2.setUserId(ssUserId);							//사용자
						
						equipmentBomMapper.updateEqupBom(equipmentBomVO2);
					}
					
					OBJECT_TYPE = zpm_bom_dn_t_item.getZTYPE();
					OBJECT = zpm_bom_dn_t_item.getZOBJECT();
					PLANT = zpm_bom_dn_t_item.getWERKS();
					
					// 2-2. 신규 및 수정 BOM 
					equipmentBomVO = new EquipmentBomVO();
						
					//설비BOM 등록
					equipmentBomVO.setCOMPANY_ID(ssCompanyId);								//회사코드
					equipmentBomVO.setLOCATION(zpm_bom_dn_t_item.getWERKS());				//공장코드
					equipmentBomVO.setTYPE(zpm_bom_dn_t_item.getZTYPE());					//OBJECT TYPE
					equipmentBomVO.setOBJECT(zpm_bom_dn_t_item.getZOBJECT());				//OBJECT
					equipmentBomVO.setOBJTXT(zpm_bom_dn_t_item.getZOBJTXT().trim());		//OBJECT내역
					equipmentBomVO.setDATE_EFFECT(zpm_bom_dn_t_item.getDATUV());			//효력시작일자     
					equipmentBomVO.setQNTY(zpm_bom_dn_t_item.getBMENG().trim());			//기준수량       
					equipmentBomVO.setUNIT(zpm_bom_dn_t_item.getBMEIN());					//단위
					equipmentBomVO.setTARGET_SEQ(zpm_bom_dn_t_item.getPOSNR());				//항목
					equipmentBomVO.setTARGET_TYPE(zpm_bom_dn_t_item.getPOSTP());			//품목범주       
					equipmentBomVO.setTARGET_OBJECT(zpm_bom_dn_t_item.getIDNRK());			//구성부품       
					equipmentBomVO.setTARGET_OBJTXT(zpm_bom_dn_t_item.getMAKTX().trim());	//구성부품내역     
					equipmentBomVO.setTARGET_QNTY(zpm_bom_dn_t_item.getMENGE().trim());		//구성부품수량     
					equipmentBomVO.setTARGET_UNIT(zpm_bom_dn_t_item.getMEINS());			//구성부품단위
					equipmentBomVO.setREG_ID(ssUserId);
					equipmentBomVO.setUPD_ID(ssUserId);
					equipmentBomVO.setDEL_YN("N");
					
					equipmentBomMapper.insertEqupBom(equipmentBomVO);
						
				}
			}
			
		}
		
		//SapSync 시퀀스 조회
		String SapSyncSeq = equipmentMasterMapper.selectSapSyncSeq();
		
		EquipmentMasterVO equipmentMasterVO = new EquipmentMasterVO();
		
		equipmentMasterVO.setSAP_SYNC(SapSyncSeq);
		equipmentMasterVO.setFUNCTION_ID("ZPM_BOM_DN");
		equipmentMasterVO.setUserId(ssUserId);
		equipmentMasterVO.setCOMPANY_ID(ssCompanyId);
		equipmentMasterVO.setLOCATION(zpm_bom_dn_import.getI_WERKS());
		
		//SAP 동기화 날짜 등록 
		equipmentMasterMapper.insertSapSync(equipmentMasterVO);
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("E_RESULT", tables.getE_RESULT());
		map.put("E_MESSAGE", tables.getE_MESSAGE());
		logger.info("E_RESULT:" + tables.getE_RESULT());
		logger.info("E_MESSAGE:" + tables.getE_MESSAGE());
		
		return map;
		
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	

	/**
	 * 모바일 - 설비BOM 목록 조회
	 *
	 * @author 서정민
	 * @since 2018.04.12
	 * @param EquipmentBomVO equipmentBomVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectEquipmentBomList2(EquipmentBomVO equipmentBomVO) throws Exception {
				
		return equipmentBomMapper.selectEquipmentBomList2(equipmentBomVO);
	}

	
}
