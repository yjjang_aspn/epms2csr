package aspn.hello.epms.equipment.bom.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aspn.hello.epms.equipment.bom.model.EquipmentBomVO;
import aspn.hello.rfc.model.ZPM_BOM_DN_EXPORT;
import aspn.hello.rfc.model.ZPM_BOM_DN_IMPORT;

/**
 * [설비BOM] Service Class
 * 
 * @author 서정민
 * @since 2018.02.07
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.07		서정민			최초 생성
 *   2018.11.20		김영환			SAP 설비BOM 등록
 *   
 * </pre>
 */

public interface EquipmentBomService {
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 설비BOM 목록
	 *
	 * @author 서정민
	 * @since 2018.02.09
	 * @param EquipmentBomVO equipmentBomVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectEquipmentBomList(EquipmentBomVO equipmentBomVO) throws Exception;
	
	/**
	 * 설비BOM 저장
	 *
	 * @author 서정민
	 * @since 2018.02.09
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param EquipmentBomVO equipmentBomVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public void equipmentBomEdit(List<HashMap<String, Object>> saveDataList, EquipmentBomVO equipmentBomVO) throws Exception;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// SAP
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * SAP 설비BOM 다운로드 
	 *
	 * @author 김영환
	 * @since 2018.11.20
	 * @param ZPM_BOM_DN_EXPORT tables
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public Map<String, Object> updateEquipBom(ZPM_BOM_DN_EXPORT tables, ZPM_BOM_DN_IMPORT zpm_bom_dn_import)  throws Exception;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - 설비BOM 목록
	 *
	 * @author 서정민
	 * @since 2018.04.12
	 * @param EquipmentBomVO equipmentBomVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectEquipmentBomList2(EquipmentBomVO equipmentBomVO) throws Exception;

	

	


}
