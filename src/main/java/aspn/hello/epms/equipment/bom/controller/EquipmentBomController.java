package aspn.hello.epms.equipment.bom.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.SessionUtil;
import aspn.com.common.util.Utility;
import aspn.hello.epms.equipment.bom.model.EquipmentBomVO;
import aspn.hello.epms.equipment.bom.service.EquipmentBomService;
import aspn.hello.epms.equipment.master.model.EquipmentMasterVO;
import aspn.hello.epms.equipment.master.service.EquipmentMasterService;
import aspn.hello.rfc.model.ZPM_BOM_DN_EXPORT;
import aspn.hello.rfc.model.ZPM_BOM_DN_IMPORT;
import aspn.hello.rfc.service.RfcService;

/**
 * [설비BOM] Controller Class
 * @author 서정민
 * @since 2018.02.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.09		서정민			최초 생성
 *   2018.11.20		김영환			SAP 설비BOM 등록
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/equipment/bom")
public class EquipmentBomController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	EquipmentBomService equipmentBomService;
	
	@Value("#{config['equipment_hierarchy']}")
	private String equipment_hierarchy;
	
	@Autowired
	RfcService rfcService;
	
	@Autowired
	EquipmentMasterService equipmentMasterService;


	/**
	 * [설비BOM조회] : 화면호출
	 *
	 * @author 서정민
	 * @since 2018.02.09
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentBomList.do")
	public String equipmentBomList(Model model) throws Exception{

		model.addAttribute("editYn", "N");
		
		return "/epms/equipment/bom/equipmentBomList";
	}
	
	/**
	 * [설비BOM조회],[설비BOM관리] : [설비] 그리드 레이아웃
	 *
	 * @author 서정민
	 * @since 2018.02.09
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentListLayout.do")
	public String equipmentListLayout(EquipmentBomVO equipmentBomVO, Model model) throws Exception{
		
		String editYn = equipmentBomVO.getEditYn();
		model.addAttribute("editYn", editYn);
		
		model.addAttribute("equipment_hierarchy", equipment_hierarchy);
		
		return "/epms/equipment/bom/equipmentListLayout";
	}
	
	/**
	 * [설비BOM조회]: [설비BOM] 그리드 데이터
	 *
	 * @author 서정민
	 * @since 2018.02.09
	 * @param equipmentMasterVO
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentBomListData.do")
	public String equipmentBomListData(EquipmentBomVO equipmentBomVO, Model model) throws Exception{
		
		try {
			equipmentBomVO.setCompanyId(getSsCompanyId());
			equipmentBomVO.setLOCATION("5100");	// 추후 ssLocation으로 변경 가능성 있음
			
			//설비BOM 목록
			List<HashMap<String, Object>> list= equipmentBomService.selectEquipmentBomList(equipmentBomVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return "/epms/equipment/bom/equipmentBomListData";
	}
	
	/**
	 * [설비BOM조회] : [설비BOM] 그리드 레이아웃
	 *
	 * @author 서정민
	 * @since 2018.02.09
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentBomListLayout.do")
	public String equipmentBomListLayout() throws Exception{
		
		return "/epms/equipment/bom/equipmentBomListLayout";
	}
	
	/**
	 * [설비BOM관리] : 화면호출
	 *
	 * @author 서정민
	 * @since 2018.02.09
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentBomMng.do")
	public String equipmentBomMng(Model model) throws Exception{

		model.addAttribute("editYn", "Y");
		
		return "/epms/equipment/bom/equipmentBomMng";
	}
	
	/**
	 * [설비BOM관리] : [설비BOM] 그리드 레이아웃
	 *
	 * @author 서정민
	 * @since 2018.02.09
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentBomMngLayout.do")
	public String equipmentBomMngLayout() throws Exception{
		
		return "/epms/equipment/bom/equipmentBomMngLayout";
	}
	
	/**
	 * [설비BOM관리] : 설비BOM 저장
	 * 
	 * @author 서정민
	 * @since 2018.02.09
	 * @param EquipmentBomVO equipmentBomVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentBomEdit.do")
	@ResponseBody
	public String equipmentBomEdit(EquipmentBomVO equipmentBomVO, Model model) throws Exception {
		
		try{
			equipmentBomVO.setUserId(this.getSsUserId());
			
			equipmentBomService.equipmentBomEdit( Utility.getEditDataList(equipmentBomVO.getUploadData()) , equipmentBomVO);
			return getSaved();
			
		}catch(Exception e){
			logger.error("EquipmentBomController.equipmentBomListEdit Error !");
			e.printStackTrace();

			return getSaveFail();
			
		}
	}
	
	/**
	 * [설비BOM관리] : [자재 일괄매핑 팝업] 자재 그리드 레이아웃
	 *
	 * @author 서정민
	 * @since 2018.02.09
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popEquipmentBomMngLayout.do")
	public String popEquipmentBomMngLayout() throws Exception{
		
		return "/epms/equipment/bom/popEquipmentBomMngLayout";
	}
	
	/**
	 * [설비BOM] : SAP 동기화 팝업 호출
	 *
	 * @author 김영환
	 * @since 2018.10.17
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popSyncEquipmentBom.do")
	public String popSyncEquipmentBom(Model model) throws Exception{
				
		return "/epms/equipment/bom/popSyncEquipmentBom";
	}
	
	/**
	 * SAP 설비BOM 다운로드 
	 *
	 * @author 김영환
	 * @since 2018.11.20
	 * @param ZPM_BOM_DN_IMPORT zpm_bom_dn_import
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateEquipBomAjax.do")
	@ResponseBody
	public Map<String, Object> updateEquipBom(EquipmentMasterVO equipmentMasterVO) throws Exception {
		
		String SsCompanyId = (String)SessionUtil.getAttribute("ssCompanyId");
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		Map<String, Object> map =  new HashMap<String, Object>();
		ZPM_BOM_DN_IMPORT zpm_bom_dn_import = new ZPM_BOM_DN_IMPORT();
		
		equipmentMasterVO.setLOCATION("5100");
		equipmentMasterVO.setFUNCTION_ID("ZPM_BOM_DN");
		equipmentMasterVO.setCOMPANY_ID(SsCompanyId);
		
		//전체 업데이트
		if("all".equals(equipmentMasterVO.getSAP_SYNC_FLAG())){ 
			rtnMap = null;
			
		}//수정된건만 업데이트
		else if("onlyUpdate".equals(equipmentMasterVO.getSAP_SYNC_FLAG())) {
			rtnMap = equipmentMasterService.selectSapSyncDay(equipmentMasterVO);
		}
		
		//전체 업데이트
		if(rtnMap == null){
			zpm_bom_dn_import.setI_YYYYMMDD("");
		}else{ 	//수정된건 업데이트시 날짜 가져오기
			zpm_bom_dn_import.setI_YYYYMMDD(String.valueOf(rtnMap.get("REG_DT")));
		}
		
		zpm_bom_dn_import.setI_WERKS("5100");//던킨-안양공장
		
		ZPM_BOM_DN_EXPORT tables = rfcService.updateEquipBom(zpm_bom_dn_import);
		map = equipmentBomService.updateEquipBom(tables, zpm_bom_dn_import);
		
		return map;
	}

}
