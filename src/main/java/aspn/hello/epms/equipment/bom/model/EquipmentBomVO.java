package aspn.hello.epms.equipment.bom.model;

import aspn.hello.com.model.AbstractVO;

import java.util.Arrays;

/**
 * [설비BOM] VO Class
 * @author 서정민
 * @since 2018.02.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.09		서정민			최초 생성
 *   
 * </pre>
 */

public class EquipmentBomVO extends AbstractVO{
	
	private static final long serialVersionUID = 1L;
	
	/** EPMS_EQUIPMENT DB **/
	private String EQUIPMENT				= ""; // 설비ID
	private String COMPANY_ID				= ""; // 회사코드
	private String LOCATION					= ""; // 위치ID
	private String LINE						= ""; // 라인ID (CATEGORY : CATEGORY_TYPE="LINE")
	private String PARENT_EQUIPMENT			= ""; // 부모설비ID
	private String DEPTH					= ""; // 레벨
	private String SEQ_DSP					= ""; // 출력순서
	private String ASSET_NO					= ""; // 자산번호
	private String COST_CENTER				= ""; // 코스트센터
	private String EQUIPMENT_UID			= ""; // 설비코드
	private String EQUIPMENT_NAME			= ""; // 설비명
	private String GRADE					= ""; // 설비등급
	private String MODEL					= ""; // 모델명
	private String DIMENSION				= ""; // 크기/치수 
	private String WEIGHT					= ""; // 총중량
	private String WEIGHT_UNIT				= ""; // 중량단위
	private String ACQUISITION_VALUE		= ""; // 취득가액
	private String CURRENCY					= ""; // 통화
	private String COUNTRY					= ""; // 제조국
	private String MANUFACTURER				= ""; // 제조사
	private String DATE_ACQUISITION			= ""; // 취득일
	private String DATE_INSTALL				= ""; // 설치일
	private String DATE_OPERATE				= ""; // 가동일
	private String DATE_VALID				= ""; // 유효일(효력시작일)
	private String ATTACH_GRP_NO			= ""; // 첨부파일
	private String ATTACH_GRP_NO2			= ""; // QR코드
	private String DESCRIPTION				= ""; // 메모
	private String REG_ID					= ""; // 등록자
	private String REG_DT					= ""; // 등록일
	private String UPD_ID					= ""; // 수정자
	private String UPD_DT					= ""; // 수정일
	private String DEL_YN					= ""; // 삭제여부
	private String E_RESULT					= ""; // SAP-성공여부
	private String E_MESSAGE				= ""; // SAP-메세지
	private String SEQ_EQUIPMENT			= ""; // 설비시퀀스
	
	private String TYPE                     = ""; //OBJECT TYPE
	private String OBJECT                   = ""; //OBJECT     
	private String OBJTXT                   = ""; //OBJECT내역
	private String DATE_EFFECT              = ""; //효력시작일자     
	private String QNTY                     = ""; //기준수량       
	private String UNIT                     = ""; //단위         
	private String TARGET_SEQ               = ""; //항목         
	private String TARGET_TYPE              = ""; //품목범주       
	private String TARGET_OBJECT            = ""; //구성부품       
	private String TARGET_OBJTXT            = ""; //구성부품내역     
	private String TARGET_QNTY              = ""; //구성부품수량     
	private String TARGET_UNIT              = ""; //구성부품단위     
	
	
	/** EPMS_MATERIAL DB **/
	private String MATERIAL					= ""; // 자재ID
	
	/** EPMS_EQUIPMENT_BOM DB **/
	private String EQUIPMENT_BOM			= ""; // 설비BOM ID
	private String SEQ_EQUIPMENT_BOM		= ""; // 설비BOM시퀀스
	
	/** CATEGORY DB  */
	private String CATEGORY   				= ""; // 카테고리ID
	private String NAME   					= ""; // 카테고리 이름
	private String CATEGORY_TYPE			= ""; // 카테고리 유형
	private String PARENT_CATEGORY			= ""; // 부모카테고리
	private String TREE   					= ""; // 구조체
	private String SUB_ROOT					= ""; // 공장구분(설비관리)
	
	/** parameter */
	public String getEQUIPMENT() {
		return EQUIPMENT;
	}
	public void setEQUIPMENT(String eQUIPMENT) {
		EQUIPMENT = eQUIPMENT;
	}
	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}
	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getLINE() {
		return LINE;
	}
	public void setLINE(String lINE) {
		LINE = lINE;
	}
	public String getPARENT_EQUIPMENT() {
		return PARENT_EQUIPMENT;
	}
	public void setPARENT_EQUIPMENT(String pARENT_EQUIPMENT) {
		PARENT_EQUIPMENT = pARENT_EQUIPMENT;
	}
	public String getDEPTH() {
		return DEPTH;
	}
	public void setDEPTH(String dEPTH) {
		DEPTH = dEPTH;
	}
	public String getSEQ_DSP() {
		return SEQ_DSP;
	}
	public void setSEQ_DSP(String sEQ_DSP) {
		SEQ_DSP = sEQ_DSP;
	}
	public String getASSET_NO() {
		return ASSET_NO;
	}
	public void setASSET_NO(String aSSET_NO) {
		ASSET_NO = aSSET_NO;
	}
	public String getCOST_CENTER() {
		return COST_CENTER;
	}
	public void setCOST_CENTER(String cOST_CENTER) {
		COST_CENTER = cOST_CENTER;
	}
	public String getEQUIPMENT_UID() {
		return EQUIPMENT_UID;
	}
	public void setEQUIPMENT_UID(String eQUIPMENT_UID) {
		EQUIPMENT_UID = eQUIPMENT_UID;
	}
	public String getEQUIPMENT_NAME() {
		return EQUIPMENT_NAME;
	}
	public void setEQUIPMENT_NAME(String eQUIPMENT_NAME) {
		EQUIPMENT_NAME = eQUIPMENT_NAME;
	}
	public String getGRADE() {
		return GRADE;
	}
	public void setGRADE(String gRADE) {
		GRADE = gRADE;
	}
	public String getMODEL() {
		return MODEL;
	}
	public void setMODEL(String mODEL) {
		MODEL = mODEL;
	}
	public String getDIMENSION() {
		return DIMENSION;
	}
	public void setDIMENSION(String dIMENSION) {
		DIMENSION = dIMENSION;
	}
	public String getWEIGHT() {
		return WEIGHT;
	}
	public void setWEIGHT(String wEIGHT) {
		WEIGHT = wEIGHT;
	}
	public String getWEIGHT_UNIT() {
		return WEIGHT_UNIT;
	}
	public void setWEIGHT_UNIT(String wEIGHT_UNIT) {
		WEIGHT_UNIT = wEIGHT_UNIT;
	}
	public String getACQUISITION_VALUE() {
		return ACQUISITION_VALUE;
	}
	public void setACQUISITION_VALUE(String aCQUISITION_VALUE) {
		ACQUISITION_VALUE = aCQUISITION_VALUE;
	}
	public String getCURRENCY() {
		return CURRENCY;
	}
	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}
	public String getCOUNTRY() {
		return COUNTRY;
	}
	public void setCOUNTRY(String cOUNTRY) {
		COUNTRY = cOUNTRY;
	}
	public String getMANUFACTURER() {
		return MANUFACTURER;
	}
	public void setMANUFACTURER(String mANUFACTURER) {
		MANUFACTURER = mANUFACTURER;
	}
	public String getDATE_ACQUISITION() {
		return DATE_ACQUISITION;
	}
	public void setDATE_ACQUISITION(String dATE_ACQUISITION) {
		DATE_ACQUISITION = dATE_ACQUISITION;
	}
	public String getDATE_INSTALL() {
		return DATE_INSTALL;
	}
	public void setDATE_INSTALL(String dATE_INSTALL) {
		DATE_INSTALL = dATE_INSTALL;
	}
	public String getDATE_OPERATE() {
		return DATE_OPERATE;
	}
	public void setDATE_OPERATE(String dATE_OPERATE) {
		DATE_OPERATE = dATE_OPERATE;
	}
	public String getDATE_VALID() {
		return DATE_VALID;
	}
	public void setDATE_VALID(String dATE_VALID) {
		DATE_VALID = dATE_VALID;
	}
	public String getATTACH_GRP_NO() {
		return ATTACH_GRP_NO;
	}
	public void setATTACH_GRP_NO(String aTTACH_GRP_NO) {
		ATTACH_GRP_NO = aTTACH_GRP_NO;
	}
	public String getATTACH_GRP_NO2() {
		return ATTACH_GRP_NO2;
	}
	public void setATTACH_GRP_NO2(String aTTACH_GRP_NO2) {
		ATTACH_GRP_NO2 = aTTACH_GRP_NO2;
	}
	public String getDESCRIPTION() {
		return DESCRIPTION;
	}
	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
	public String getREG_ID() {
		return REG_ID;
	}
	public void setREG_ID(String rEG_ID) {
		REG_ID = rEG_ID;
	}
	public String getREG_DT() {
		return REG_DT;
	}
	public void setREG_DT(String rEG_DT) {
		REG_DT = rEG_DT;
	}
	public String getUPD_ID() {
		return UPD_ID;
	}
	public void setUPD_ID(String uPD_ID) {
		UPD_ID = uPD_ID;
	}
	public String getUPD_DT() {
		return UPD_DT;
	}
	public void setUPD_DT(String uPD_DT) {
		UPD_DT = uPD_DT;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getE_RESULT() {
		return E_RESULT;
	}
	public void setE_RESULT(String e_RESULT) {
		E_RESULT = e_RESULT;
	}
	public String getE_MESSAGE() {
		return E_MESSAGE;
	}
	public void setE_MESSAGE(String e_MESSAGE) {
		E_MESSAGE = e_MESSAGE;
	}
	public String getSEQ_EQUIPMENT() {
		return SEQ_EQUIPMENT;
	}
	public void setSEQ_EQUIPMENT(String sEQ_EQUIPMENT) {
		SEQ_EQUIPMENT = sEQ_EQUIPMENT;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getOBJECT() {
		return OBJECT;
	}
	public void setOBJECT(String oBJECT) {
		OBJECT = oBJECT;
	}
	public String getOBJTXT() {
		return OBJTXT;
	}
	public void setOBJTXT(String oBJTXT) {
		OBJTXT = oBJTXT;
	}
	public String getDATE_EFFECT() {
		return DATE_EFFECT;
	}
	public void setDATE_EFFECT(String dATE_EFFECT) {
		DATE_EFFECT = dATE_EFFECT;
	}
	public String getQNTY() {
		return QNTY;
	}
	public void setQNTY(String qNTY) {
		QNTY = qNTY;
	}
	public String getUNIT() {
		return UNIT;
	}
	public void setUNIT(String uNIT) {
		UNIT = uNIT;
	}
	public String getTARGET_SEQ() {
		return TARGET_SEQ;
	}
	public void setTARGET_SEQ(String tARGET_SEQ) {
		TARGET_SEQ = tARGET_SEQ;
	}
	public String getTARGET_TYPE() {
		return TARGET_TYPE;
	}
	public void setTARGET_TYPE(String tARGET_TYPE) {
		TARGET_TYPE = tARGET_TYPE;
	}
	public String getTARGET_OBJECT() {
		return TARGET_OBJECT;
	}
	public void setTARGET_OBJECT(String tARGET_OBJECT) {
		TARGET_OBJECT = tARGET_OBJECT;
	}
	public String getTARGET_OBJTXT() {
		return TARGET_OBJTXT;
	}
	public void setTARGET_OBJTXT(String tARGET_OBJTXT) {
		TARGET_OBJTXT = tARGET_OBJTXT;
	}
	public String getTARGET_QNTY() {
		return TARGET_QNTY;
	}
	public void setTARGET_QNTY(String tARGET_QNTY) {
		TARGET_QNTY = tARGET_QNTY;
	}
	public String getTARGET_UNIT() {
		return TARGET_UNIT;
	}
	public void setTARGET_UNIT(String tARGET_UNIT) {
		TARGET_UNIT = tARGET_UNIT;
	}
	public String getMATERIAL() {
		return MATERIAL;
	}
	public void setMATERIAL(String mATERIAL) {
		MATERIAL = mATERIAL;
	}
	public String getEQUIPMENT_BOM() {
		return EQUIPMENT_BOM;
	}
	public void setEQUIPMENT_BOM(String eQUIPMENT_BOM) {
		EQUIPMENT_BOM = eQUIPMENT_BOM;
	}
	public String getSEQ_EQUIPMENT_BOM() {
		return SEQ_EQUIPMENT_BOM;
	}
	public void setSEQ_EQUIPMENT_BOM(String sEQ_EQUIPMENT_BOM) {
		SEQ_EQUIPMENT_BOM = sEQ_EQUIPMENT_BOM;
	}
	public String getCATEGORY() {
		return CATEGORY;
	}
	public void setCATEGORY(String cATEGORY) {
		CATEGORY = cATEGORY;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getCATEGORY_TYPE() {
		return CATEGORY_TYPE;
	}
	public void setCATEGORY_TYPE(String cATEGORY_TYPE) {
		CATEGORY_TYPE = cATEGORY_TYPE;
	}
	public String getPARENT_CATEGORY() {
		return PARENT_CATEGORY;
	}
	public void setPARENT_CATEGORY(String pARENT_CATEGORY) {
		PARENT_CATEGORY = pARENT_CATEGORY;
	}
	public String getTREE() {
		return TREE;
	}
	public void setTREE(String tREE) {
		TREE = tREE;
	}
	public String getSUB_ROOT() {
		return SUB_ROOT;
	}
	public void setSUB_ROOT(String sUB_ROOT) {
		SUB_ROOT = sUB_ROOT;
	}
	
}
