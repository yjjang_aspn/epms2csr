package aspn.hello.epms.equipment.bom.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.equipment.bom.model.EquipmentBomVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [설비BOM] Mapper Class
 * 
 * @author 서정민
 * @since 2018.02.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.09		서정민			최초 생성
 *   2018.11.20		김영환			SAP 설비BOM 등록
 *   
 * </pre>
 */

@Mapper("equipmentBomMapper")
public interface EquipmentBomMapper {
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 설비BOM 목록 조회
	 *
	 * @author 서정민
	 * @since 2018.02.09
	 * @param EquipmentBomVO equipmentBomVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectEquipmentBomList(EquipmentBomVO equipmentBomVO);
	
	/**
	 * 설비BOM 등록 여부 확인
	 *
	 * @author 서정민
	 * @since 2018.02.09
	 * @param EquipmentBomVO equipmentBomVO
	 * @return int
	 * @throws Exception
	 */
	public int existEquipmentBom(EquipmentBomVO equipmentBomVO) throws Exception;
	
	/**
	 * 설비BOM 등록
	 *
	 * @author 서정민
	 * @since 2018.02.09
	 * @param EquipmentBomVO equipmentBomVO
	 * @return int
	 * @throws Exception
	 */
	public int insertEquipmentBom(EquipmentBomVO equipmentBomVO) throws Exception;
	
	/**
	 * 설비BOM 활성/비활성 상태 업데이트
	 *
	 * @author 서정민
	 * @since 2018.02.09
	 * @param EquipmentBomVO equipmentBomVO
	 * @return int
	 * @throws Exception
	 */
	public int updateEquipmentBom(EquipmentBomVO equipmentBomVO) throws Exception;
	
	/**
	 * SAP 설비BOM 다운로드 
	 *
	 * @author 김영환
	 * @since 2018.11.20
	 * @param ZPM_BOM_DN_EXPORT tables
	 * @param ZPM_BOM_DN_IMPORT zpm_bom_dn_import
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public int insertEqupBom(EquipmentBomVO equipmentBomVO) throws Exception;
	
	/**
	 * SAP 설비BOM 다운로드 
	 *
	 * @author 김영환
	 * @since 2018.11.20
	 * @param EquipmentBomVO equipmentBomVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public int equipmentBomCheck(EquipmentBomVO equipmentBomVO) throws Exception;
	
	/**
	 * SAP 설비BOM Seq 조회
	 *
	 * @author 김영환
	 * @since 2018.11.20
	 * @param EquipmentBomVO equipmentBomVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public String selectEquipmentBomSeq() throws Exception;
	
	/**
	 * SAP 설비BOM : SAP삭제건 삭제
	 *
	 * @author 김영환
	 * @since 2018.11.20
	 * @param EquipmentBomVO equipmentBomVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public void updateEqupBom(EquipmentBomVO equipmentBomVO) throws Exception;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - 설비BOM 목록 조회
	 *
	 * @author 서정민
	 * @since 2018.04.12
	 * @param EquipmentBomVO equipmentBomVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectEquipmentBomList2(EquipmentBomVO equipmentBomVO);

	

}
