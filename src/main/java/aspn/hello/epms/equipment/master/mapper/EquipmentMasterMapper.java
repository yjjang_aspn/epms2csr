package aspn.hello.epms.equipment.master.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.equipment.master.model.EquipmentMasterVO;
import aspn.hello.sys.model.CategoryVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [설비마스터] Mapper Class
 * 
 * @author 서정민
 * @since 2018.02.07
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.07		서정민			최초 생성
 *   2018.11.12		김영환			SAP 설비마스터 조회
 *   
 * </pre>
 */

@Mapper("equipmentMasterMapper")
public interface EquipmentMasterMapper {
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 설비마스터 목록 조회
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectEquipmentMasterList(EquipmentMasterVO equipmentMasterVO);
	
	/**
	 * 설비마스터 상세 조회
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public HashMap<String, Object> selectEquipmentMasterDtl(EquipmentMasterVO equipmentMasterVO);
	
	/**
	 * 설비마스터 SEQ 조회 
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	public String selectEquipmentSeq() throws Exception;
	
	/**
	 * 설비마스터 등록
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return int
	 * @throws Exception
	 */
	public int insertEquipmentMaster(EquipmentMasterVO equipmentMasterVO) throws Exception;
	
	/**
	 * 설비마스터 수정
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return int
	 * @throws Exception
	 */
	public int updateEquipmentMaster(EquipmentMasterVO equipmentMasterVO) throws Exception;
	
	/**
	 * 설비마스터 등록성공 업데이트
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return int
	 * @throws Exception
	 */
	public int updateEquipmentSuccess(EquipmentMasterVO equipmentMasterVO) throws Exception;
	
	/**
	 * 첨부파일 등록/수정
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return int
	 * @throws Exception
	 */
	public int updateEquipmentAttach(EquipmentMasterVO equipmentMasterVO) throws Exception;
	
	/**
	 * 설비 관련 URL 조회
	 * 
	 * @author 서정민
	 * @since 2018.04.13
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectEquipmentUrlList(EquipmentMasterVO equipmentMasterVO);
	
	/**
	 * 설비 관련 URL 등록
	 * 
	 * @author 서정민
	 * @since 2018.04.13
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return int
	 * @throws Exception
	 */
	public int insertEquipmentUrl(EquipmentMasterVO equipmentMasterVO) throws Exception;
	
	/**
	 * 설비 관련 URL 수정
	 * 
	 * @author 서정민
	 * @since 2018.04.13
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return int
	 * @throws Exception
	 */
	public int updateEquipmentUrl(EquipmentMasterVO equipmentMasterVO) throws Exception;
	
	/**
	 * 기능위치 정보삭제
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param param1 
	 * @return
	 */
	void deleteFuncSite(HashMap<String, Object> param1);

	/**
	 * 기능위치 정보등록
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param T_FUNC t_func 
	 * @return
	 */
	void insertFuncSite(CategoryVO categoryVO);

	/**
	 * 기능위치 카테고리 tree구조 생성
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param T_FUNC t_func 
	 * @return
	 */
	void updateTree(CategoryVO categoryVO);

	/**
	 * 기능위치 시퀀스 변경
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param HashMap<String, Object> param
	 * @return
	 */
	void updateFuncSiteSeq(HashMap<String, Object> param);

	/**
	 * SAP 설비마스터확인
	 *
	 * @author 김영환
	 * @since 2018.11.12
	 * @param ZPM_EQIP_DN_T_EQUP zpm_eqip_dn_t_equp
	 * @return int
	 */
	int equipmentCheck(EquipmentMasterVO equipmentMasterVO);
	
	/**
	 * SAP 설비마스터 업데이트
	 *
	 * @author 김영환
	 * @since 2018.11.12
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return int
	 */
	public int insertEqup(EquipmentMasterVO equipmentMasterVO) ;
	
	/**
	 * SAP 설비마스터 동기화 날짜 가져오기
	 *
	 * @author 김영환
	 * @param ZPM_EQIP_DN_T_PLANT t_plantVo
	 * @since 2018.11.27
	 * @return Map<String, Object>
	 * @throws Exception
	 */	
	public HashMap<String, Object> selectSapSyncDay(EquipmentMasterVO equipmentMasterVO);

	/**
	 * SAP 설비마스터 동기화 날짜 등록 
	 *
	 * @author 김영환
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @since 2018.11.27
	 */	
	public void insertSapSync(EquipmentMasterVO equipmentMasterVO);

	/**
	 * 설비SAP_SYNC SEQ 조회 
	 * 
	 * @author 김영환
	 * @since 2018.11.28
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	public String selectSapSyncSeq();
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - 설비마스터 목록 조회
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectEquipmentMasterList2(EquipmentMasterVO equipmentMasterVO);
	
	/**
	 * 모바일 - 설비마스터 상세 조회
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public HashMap<String, Object> selectEquipmentMasterDtl2(EquipmentMasterVO equipmentMasterVO);

}
