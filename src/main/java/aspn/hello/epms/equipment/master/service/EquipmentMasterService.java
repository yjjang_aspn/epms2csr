package aspn.hello.epms.equipment.master.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aspn.hello.epms.equipment.master.model.EquipmentMasterVO;
import aspn.hello.rfc.model.ZPM_EQIP_DN_T_PLANT;
import aspn.hello.rfc.model.ZPM_EQUI_DN_EXPORT;
import aspn.hello.rfc.model.ZPM_FUNC_DN_EXPORT;
import aspn.hello.rfc.model.ZPM_FUNC_DN_IMPORT;

/**
 * [설비마스터] Service Class
 * 
 * @author 서정민
 * @since 2018.02.07
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.07		서정민			최초 생성
 *   2018.11.12		김영환			SAP 설비마스터 조회
 *   
 * </pre>
 */

public interface EquipmentMasterService {
								
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 설비마스터 목록 조회
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectEquipmentMasterList(EquipmentMasterVO equipmentMasterVO) throws Exception;
	
	/**
	 * 설비마스터 상세 조회
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public HashMap<String, Object> selectEquipmentMasterDtl(EquipmentMasterVO equipmentMasterVO) throws Exception;
	
	/**
	 * 설비마스터 Grid CRUD 처리
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return void
	 * @throws Exception
	 */
	public void equipmentMasterListEdit(List<HashMap<String, Object>> saveDataList, EquipmentMasterVO equipmentMasterVO) throws Exception;
	
	/**
	 * 첨부파일 등록/수정
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return void
	 * @throws Exception
	 */
	public int updateEquipmentAttach(EquipmentMasterVO equipmentMasterVO) throws Exception;
	
	/**
	 * 설비 관련 URL 조회
	 * 
	 * @author 서정민
	 * @since 2018.04.13
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectEquipmentUrlList(EquipmentMasterVO equipmentMasterVO) throws Exception;
	
	/**
	 * 설비 관련 URL Grid CRUD 처리
	 * 
	 * @author 서정민
	 * @since 2018.04.13
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return void
	 * @throws Exception
	 */
	public void equipmentUrlListEdit(List<HashMap<String, Object>> saveDataList, EquipmentMasterVO equipmentMasterVO) throws Exception;
	
	/**
	 * QR코드 일괄생성
	 * 
	 * @author 서정민
	 * @since 2018.08.09
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public HashMap<String, Object> createQrCode(EquipmentMasterVO equipmentMasterVO) throws Exception;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// SAP
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * SAP 기능위치 업데이트 
	 *
	 * @author 김영환
	 * @param zpm_func_dn_import 
	 * @since 2018.11.10
	 * @param ZPM_FUNC_DN_EXPORT tables
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	public Map<String, Object> updateFuncSiteInfo(ZPM_FUNC_DN_EXPORT tables, ZPM_FUNC_DN_IMPORT zpm_func_dn_import) throws Exception;
	
	/**
	 * SAP 설비마스터 업데이트
	 *
	 * @author 김영환
	 * @param tables 
	 * @since 2018.11.12
	 * @param ZPMPC_EQIP_DN_IMPORT zpmpc_eqip_dn_import
	 * @return Map<String, Object>
	 * @throws Exception
	 */	
	public HashMap<String, Object> updateEquipInfo(ZPM_EQUI_DN_EXPORT tables, ZPM_EQIP_DN_T_PLANT t_plantvo) throws Exception;
	
	/**
	 * SAP 설비마스터 동기화 날짜 가져오기
	 *
	 * @author 김영환
	 * @since 2018.11.12
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */	
	public HashMap<String, Object> selectSapSyncDay(EquipmentMasterVO equipmentMasterVO);
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 모바일 - 설비마스터 목록 조회
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> selectEquipmentMasterList2(EquipmentMasterVO equipmentMasterVO) throws Exception;
	
	/**
	 * 모바일 - 설비마스터 상세 조회
	 * 
	 * @author 서정민
	 * @since 2018.04.12
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	public HashMap<String, Object> selectEquipmentMasterDtl2(EquipmentMasterVO equipmentMasterVO) throws Exception;

	

	

	


}
