package aspn.hello.epms.equipment.master.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.Entity;
import aspn.com.common.util.JsonUtil;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.epms.equipment.bom.model.EquipmentBomVO;
import aspn.hello.epms.equipment.bom.service.EquipmentBomService;
import aspn.hello.epms.equipment.master.model.EquipmentMasterVO;
import aspn.hello.epms.equipment.master.service.EquipmentMasterService;

/**
 * [모바일 - 설비마스터] Controller Class
 * @author 서정민
 * @since 2018.02.07
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.12		서정민			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/mobile/epms/equipment/master")
public class MobileEquipmentMasterController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	EquipmentMasterService equipmentMasterService;
	
	@Autowired
	EquipmentBomService equipmentBomService;

	@Autowired
	AttachService attachService;

	/**
	 * 설비마스터 상세 조회
	 *
	 * @author 서정민
	 * @since 2018.04.12
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentMasterDtl.do")
	@ResponseBody
	public Map<String, Object> equipmentMasterDtl(EquipmentMasterVO equipmentMasterVO) throws Exception{

		Map<String, Object> resultMap = new HashMap<>();
		EquipmentBomVO equipmentBomVO = new EquipmentBomVO();
		AttachVO attachVO = new AttachVO();
		
		try {
			equipmentMasterVO.setSubAuth(getSsSubAuth());
			HashMap<String, Object> equipmentDtl = equipmentMasterService.selectEquipmentMasterDtl2(equipmentMasterVO);
			
			if(equipmentDtl == null){
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.NO_DATA_FOUND);
				return resultMap;
			}
			else{
				resultMap.put("EQUIPMENT_DTL", equipmentDtl);
				
				// 이미지
				if(Integer.parseInt(equipmentDtl.get("FILE_CNT").toString()) > 0){
					attachVO = new AttachVO();
					attachVO.setATTACH_GRP_NO(equipmentDtl.get("ATTACH_GRP_NO").toString());
					List<HashMap<String, Object>> attachList = attachService.selectFileList(attachVO);
					resultMap.put("ATTACH_LIST", attachList);
				}
				
				// 매뉴얼
				if(Integer.parseInt(equipmentDtl.get("FILE_CNT3").toString()) > 0){
					attachVO = new AttachVO();
					attachVO.setATTACH_GRP_NO(equipmentDtl.get("ATTACH_GRP_NO3").toString());
					List<HashMap<String, Object>> manualList = attachService.selectFileList(attachVO);
					resultMap.put("MANUAL_LIST", manualList);
				}
				
				// 참고URL
				if(Integer.parseInt(equipmentDtl.get("URL_CNT").toString()) > 0){
					
					List<HashMap<String, Object>> urlList= equipmentMasterService.selectEquipmentUrlList(equipmentMasterVO);
					resultMap.put("URL_LIST", urlList);
				}
				
				// 설비BOM
				equipmentBomVO.setEQUIPMENT(equipmentDtl.get("EQUIPMENT").toString());
				equipmentBomVO.setCompanyId(equipmentDtl.get("COMPANY_ID").toString());
				equipmentBomVO.setLOCATION(equipmentDtl.get("LOCATION").toString());
				List<HashMap<String, Object>> equipmentBomList= equipmentBomService.selectEquipmentBomList2(equipmentBomVO);
				
				if(!(equipmentBomList.size() == 0 || equipmentBomList == null)){
					resultMap.put("EQUIPMENT_BOM", equipmentBomList);
				}
				
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
				return resultMap;
			}

		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	/**
	 * 설비마스터 첨부파일 등록/수정 
	 * 
	 * @author 김영환
	 * @since 2018.05.09
	 * @param HttpServletRequest request
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateEquipmentMasterAttach.do")
	@ResponseBody
	public Map<String, Object> updateEquipmentMasterAttach(HttpServletRequest request, MultipartHttpServletRequest mRequest, AttachVO attachVO) throws Exception{
		
		Map<String, Object> resultMap = new HashMap<>();
		Entity entity = new Entity(request);
		
		try{
			
			// 첨부 파일 등록
			if("".equals(attachVO.getATTACH_GRP_NO()) || attachVO.getATTACH_GRP_NO() == null){
				if("Y".equals(attachVO.getAttachExistYn())){
					String attachGrpNo = attachService.selectAttachGrpNo();
					attachVO.setATTACH_GRP_NO(attachGrpNo);
				}
			}
			
			attachService.AttachEdit(request, mRequest, attachVO); 
			
			// 설비마스터 등록/수정
			EquipmentMasterVO equipmentMasterVO = new EquipmentMasterVO();
			equipmentMasterVO.setUserId(getSsUserId());
			equipmentMasterVO.setEQUIPMENT(entity.getString("EQUIPMENT"));
			equipmentMasterVO.setFlag("equipment");
			equipmentMasterVO.setATTACH_GRP_NO(attachVO.getATTACH_GRP_NO());
			 
			equipmentMasterService.updateEquipmentAttach(equipmentMasterVO);
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			
		}catch(Exception e){
			logger.error("MobileEquipmentMasterController.updateEquipmentMasterAttach > " + e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
}
