package aspn.hello.epms.equipment.master.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import aspn.com.common.util.Config;
import aspn.com.common.util.SessionUtil;
import aspn.com.common.util.Utility;
import aspn.hello.com.mapper.AttachMapper;
import aspn.hello.com.model.AttachVO;
import aspn.hello.epms.equipment.history.mapper.EquipmentHistoryMapper;
import aspn.hello.epms.equipment.history.model.EquipmentHistoryVO;
import aspn.hello.epms.equipment.master.mapper.EquipmentMasterMapper;
import aspn.hello.epms.equipment.master.model.EquipmentMasterVO;
import aspn.hello.rfc.model.ZPM_EQIP_DN_T_EQUP;
import aspn.hello.rfc.model.ZPM_EQIP_DN_T_PLANT;
import aspn.hello.rfc.model.ZPM_EQUI_DN_EXPORT;
import aspn.hello.rfc.model.ZPM_FUNC_DN_EXPORT;
import aspn.hello.rfc.model.ZPM_FUNC_DN_IMPORT;
import aspn.hello.rfc.model.ZPM_FUNC_DN_T_FUNC;
import aspn.hello.sys.mapper.CodeMapper;
import aspn.hello.sys.model.CategoryVO;
import aspn.hello.sys.model.CodeVO;

/**
 * [설비마스터] Business Implement Class
 * 
 * @author 서정민
 * @since 2018.02.07
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.07		서정민			최초 생성
 *   2018.11.12		김영환			SAP 설비마스터 조회
 *   
 * </pre>
 */

@Service
public class EquipmentMasterServiceImpl implements EquipmentMasterService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	EquipmentMasterMapper equipmentMasterMapper;
	
	@Autowired
	EquipmentHistoryMapper equipmentHistoryMapper;
	
	@Autowired
	CodeMapper codeMapper;
	
	@Autowired
	AttachMapper attachMapper;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 웹 및 공통
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 설비마스터 목록 조회
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentMasterVO equipmentmasterVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectEquipmentMasterList(EquipmentMasterVO equipmentmasterVO) throws Exception {
		return equipmentMasterMapper.selectEquipmentMasterList(equipmentmasterVO);
	}
	
	/**
	 * 설비마스터 상세 조회
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentMasterVO equipmentmasterVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> selectEquipmentMasterDtl(EquipmentMasterVO equipmentmasterVO) throws Exception {
		return equipmentMasterMapper.selectEquipmentMasterDtl(equipmentmasterVO);
	}
	
	/**
	 * 설비마스터 Grid CRUD 처리
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return void
	 * @throws Exception
	 */
	public void equipmentMasterListEdit(List<HashMap<String, Object>> saveDataList, EquipmentMasterVO equipmentMasterVO) throws Exception{
		
		HashMap<String, String> oParentEquipmentMap = new HashMap<String, String>();	//상위설비코드 맵
		HashMap<String, String> oTreeMap = new HashMap<String, String>();				//구조체 맵
		
		if(saveDataList != null){
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				@SuppressWarnings("unchecked")
				EquipmentMasterVO equipmentMasterVO1 = Utility.toBean(tmpSaveData, equipmentMasterVO.getClass());
				equipmentMasterVO1.setUserId(equipmentMasterVO.getUserId());
				
				if(tmpSaveData.containsKey("Added")) {

					// 1. QR코드 생성
					String SEQ_EQUIPMENT = equipmentMasterMapper.selectEquipmentSeq();
					equipmentMasterVO1.setEQUIPMENT(SEQ_EQUIPMENT);
					equipmentMasterVO1.setATTACH_GRP_NO2(attachMapper.selectAttachGrpNo());
					createQrImage(equipmentMasterVO1);
					
					// 2. 설비마스터 등록
					if(equipmentMasterVO1.getPARENT_EQUIPMENT().isEmpty() && !equipmentMasterVO1.getDEPTH().equals("1")){
						String index = Integer.toString(Integer.parseInt(equipmentMasterVO1.getDEPTH())-1);
						equipmentMasterVO1.setPARENT_EQUIPMENT(oParentEquipmentMap.get(index));
						equipmentMasterVO1.setTREE(oTreeMap.get(index));
					}
					equipmentMasterMapper.insertEquipmentMaster(equipmentMasterVO1);
					// 부모설비코드 및 구조체 맵 세팅
					oParentEquipmentMap.put(equipmentMasterVO1.getDEPTH(), SEQ_EQUIPMENT);
					if("1".equals(equipmentMasterVO1.getDEPTH())){
						oTreeMap.put(equipmentMasterVO1.getDEPTH(), SEQ_EQUIPMENT);
					}
					else{
						oTreeMap.put(equipmentMasterVO1.getDEPTH(), equipmentMasterVO1.getTREE()+"$"+SEQ_EQUIPMENT);
					}
					
					// 3. SAP 설비마스터 등록(RFC)
					// RFC 연동시 개발(RFC 성공시 4~5 수행)
					
					// 4. 기기이력 등록
					EquipmentHistoryVO equipmentHistoryVO = new EquipmentHistoryVO();
					equipmentHistoryVO.setEQUIPMENT(equipmentMasterVO1.getEQUIPMENT());
					equipmentHistoryVO.setHISTORY_TYPE("01");	//기기이력유형 : (01:설비등록,02:라인이동,11:일반정비,12:예방정비,13:돌발정비,21:CBM,22:TBM)
					equipmentHistoryVO.setHISTORY_DESCRIPTION("설비마스터 등록 : " + equipmentMasterVO1.getLINE_NAME());
					equipmentHistoryVO.setUserId(equipmentMasterVO1.getUserId());
					
					equipmentHistoryMapper.insertEquipmentHistory(equipmentHistoryVO);
					
					// 5. 성공업데이트(E_RESULT="S")
					equipmentMasterVO1.setE_RESULT("S");
					equipmentMasterMapper.updateEquipmentSuccess(equipmentMasterVO1);
					
				} else if (tmpSaveData.containsKey("Changed")) {
					
					// 1. SAP 설비마스터 수정(RFC)
					// RFC 연동시 개발(RFC 성공시 2~3 수행)
					
					// 2. 설비마스터 수정
					equipmentMasterMapper.updateEquipmentMaster(equipmentMasterVO1);
					
					// 3. 기기이력 등록(라인 변경시)
					if(!equipmentMasterVO1.getLINE().equals(equipmentMasterVO1.getORG_LINE())){
						
						EquipmentHistoryVO equipmentHistoryVO = new EquipmentHistoryVO();
						equipmentHistoryVO.setEQUIPMENT(equipmentMasterVO1.getEQUIPMENT());
						equipmentHistoryVO.setHISTORY_TYPE("02");	//기기이력유형 : (01:설비등록,02:라인이동,11:일반정비,12:예방정비,13:돌발정비,21:CBM,22:TBM)
						equipmentHistoryVO.setHISTORY_DESCRIPTION("라인 이동 : " + equipmentMasterVO1.getORG_LINE_NAME() + " > " + equipmentMasterVO1.getLINE_NAME());
						equipmentHistoryVO.setUserId(equipmentMasterVO1.getUserId());
						
						equipmentHistoryMapper.insertEquipmentHistory(equipmentHistoryVO);
					}
					
				} else if(tmpSaveData.containsKey("Deleted")) {
					
				}
			}
		}
	}
	
	/**
	 * 설비마스터 QR코드 생성
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return void
	 * @throws Exception
	 */
	public void createQrImage(EquipmentMasterVO equipmentMasterVO) throws Exception {

		//저장될 폴더 경로
		String yyFolder = new SimpleDateFormat("yyyy").format(new Date(System.currentTimeMillis()));
		String mmFolder = new SimpleDateFormat("MM").format(new Date(System.currentTimeMillis()));
		String fileClsNm = "qrcode";
		String fileDir = Config.getString("file.upload.dir") + File.separator + fileClsNm + File.separator + yyFolder + File.separator + mmFolder;
		
		File dir = new File(fileDir);
		if(!dir.exists()) dir.mkdirs();
		
		String fileRealname = "QRcode_" + equipmentMasterVO.getEQUIPMENT() + ".png";
		String ext = fileRealname.substring(fileRealname.lastIndexOf(".")+1, fileRealname.length());
		String timeM = String.valueOf(System.currentTimeMillis());
		String fileName = fileRealname.substring(0, fileRealname.lastIndexOf(".")) +"_"+ timeM +"."+ ext;
		
		File newFile = new File(fileDir + "/" +fileName);
		
		// 코드인식시 링크걸 URL주소
//		String url = "/mobile/epms/equipment/master/equipmentMasterDtl.do?EQUIPMENT="+equipmentMasterVO.getEQUIPMENT();
		String url = "COMPANY_ID="+equipmentMasterVO.getCOMPANY_ID()+"&EQUIPMENT="+equipmentMasterVO.getEQUIPMENT();
        String codeurl = new String(url.getBytes("UTF-8"), "ISO-8859-1");
        // 큐알코드 바코드 생상값
        int qrcodeColor = 0xFF2e4e96;
        // 큐알코드 배경색상값
        int backgroundColor = 0xFFFFFFFF;
        
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        // 3,4번째 parameter값 : width/height값 지정
        BitMatrix bitMatrix = qrCodeWriter.encode(codeurl, BarcodeFormat.QR_CODE, 500, 500);
        
        MatrixToImageConfig matrixToImageConfig = new MatrixToImageConfig(qrcodeColor,backgroundColor);
        BufferedImage bufferedImage = MatrixToImageWriter.toBufferedImage(bitMatrix,matrixToImageConfig);
        // ImageIO를 사용한 바코드 파일쓰기
        ImageIO.write(bufferedImage, "png", newFile);

        
        //파일 모델 생성
        AttachVO attachVO = new AttachVO();
		
        attachVO.setNAME(fileName);
        attachVO.setPATH(fileDir);
        attachVO.setFORMAT(ext);
        attachVO.setFILE_SIZE(newFile.length());
        attachVO.setCNT_DOWNLOAD("0");
        attachVO.setCD_DEL("1");
        attachVO.setATTACH_GRP_NO(equipmentMasterVO.getATTACH_GRP_NO2());
        attachVO.setMODULE("102");
        attachVO.setSEQ_DSP(1);
		
		attachMapper.insertAttach(attachVO);
	}
	
	/**
	 * 첨부파일 등록/수정
	 * 
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentMasterVO equipmentmasterVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public int updateEquipmentAttach(EquipmentMasterVO equipmentmasterVO) throws Exception {
		return equipmentMasterMapper.updateEquipmentAttach(equipmentmasterVO);
	}
	
	/**
	 * 설비 관련 URL 조회
	 * 
	 * @author 서정민
	 * @since 2018.04.13
	 * @param EquipmentMasterVO equipmentmasterVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectEquipmentUrlList(EquipmentMasterVO equipmentmasterVO) throws Exception {
		return equipmentMasterMapper.selectEquipmentUrlList(equipmentmasterVO);
	}
	
	/**
	 * 설비 관련 URL Grid CRUD 처리
	 * 
	 * @author 서정민
	 * @since 2018.04.13
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return void
	 * @throws Exception
	 */
	public void equipmentUrlListEdit(List<HashMap<String, Object>> saveDataList, EquipmentMasterVO equipmentMasterVO) throws Exception{
		
		HashMap<String, String> oParentEquipmentMap = new HashMap<String, String>();
		HashMap<String, String> oTreeMap = new HashMap<String, String>();
		
		if(saveDataList != null){
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				@SuppressWarnings("unchecked")
				EquipmentMasterVO equipmentMasterVO1 = Utility.toBean(tmpSaveData, equipmentMasterVO.getClass());
				equipmentMasterVO1.setUserId(equipmentMasterVO.getUserId());
				
				if(tmpSaveData.containsKey("Added")) {
					
					// 설비 관련 URL 등록
					equipmentMasterMapper.insertEquipmentUrl(equipmentMasterVO1);
					
				} else if (tmpSaveData.containsKey("Changed")) {
					
					// 설비 관련 URL 수정
					equipmentMasterMapper.updateEquipmentUrl(equipmentMasterVO1);
					
				} else if(tmpSaveData.containsKey("Deleted")) {
					
				}
			}
		}
	}
	
	
	/**
	 * QR코드 일괄생성
	 * 
	 * @author 서정민
	 * @since 2018.08.09
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> createQrCode(EquipmentMasterVO equipmentMasterVO) throws Exception {
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		List<HashMap<String, Object>> equipmentMasterList = equipmentMasterMapper.selectEquipmentMasterList(equipmentMasterVO);
		
		if(equipmentMasterList != null){
			EquipmentMasterVO equipmentMasterVO1 = new EquipmentMasterVO();
			
			for(HashMap<String, Object> item:equipmentMasterList) {	
				if("0".equals(item.get("FILE_CNT2").toString())){
					equipmentMasterVO1 = new EquipmentMasterVO();
					equipmentMasterVO1.setEQUIPMENT(item.get("EQUIPMENT").toString());
					equipmentMasterVO1.setCOMPANY_ID(item.get("COMPANY_ID").toString());
					equipmentMasterVO1.setATTACH_GRP_NO2(attachMapper.selectAttachGrpNo());
					createQrImage(equipmentMasterVO1);
					
					equipmentMasterVO1.setFlag("qrcode");
					equipmentMasterVO1.setUserId(equipmentMasterVO.getUserId());
					equipmentMasterMapper.updateEquipmentAttach(equipmentMasterVO1);
				}
			}
		}
		
		resultMap.put("RES_CD", "S");
		
		return resultMap;
	}
	
	/**
	 * SAP 설비마스터 동기화 날짜 가져오기
	 *
	 * @author 김영환
	 * @param ZPM_EQIP_DN_T_PLANT t_plantVo 
	 * @since 2018.11.27
	 * @return Map<String, Object>
	 * @throws Exception
	 */	
	@Override
	public HashMap<String, Object> selectSapSyncDay(EquipmentMasterVO equipmentMasterVO) {
		return equipmentMasterMapper.selectSapSyncDay(equipmentMasterVO);
	}
	
	/**
	 * SAP 기능위치 정보저장
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param ZPM_FUNC_DN_IMPORT zpm_func_dn_import
	 * @return Map<String, Object>
	 * @throws Exception 
	 */
	@Override
	public Map<String, Object> updateFuncSiteInfo(ZPM_FUNC_DN_EXPORT tables, ZPM_FUNC_DN_IMPORT zpm_func_dn_import) throws Exception {
		
		String ssCompanyId = (String) SessionUtil.getAttribute("ssCompanyId");
//		String ssLocation = (String) SessionUtil.getAttribute("ssLocation");
		String ssUserId = (String) SessionUtil.getAttribute("ssUserId");
		
		CodeVO codeVO = new CodeVO();
		
		//기능위치정보 삭제
		HashMap<String, Object> param1 = new HashMap<String, Object>();
		param1.put("companyId", ssCompanyId);
		param1.put("location", "5100");
		
		equipmentMasterMapper.deleteFuncSite(param1);
		
		//기능위치정보 등록
		if(tables != null){
			CategoryVO categoryVO = new CategoryVO();
			
			for (ZPM_FUNC_DN_T_FUNC t_func : tables.getT_FUNC()) {
				categoryVO = new CategoryVO();
				categoryVO.setDEPTH(t_func.getHLEVEL().trim());				 // 구조체 LEVEL
				categoryVO.setLOCATION("5100");							 	 // 던킨  plant(공장)코드 : 안양 5100, 그외 나머지 ck공장은 공장코드 없음.
				categoryVO.setCATEGORY(t_func.getOBJECT());					 // 카테고리 ID
				categoryVO.setCATEGORY_UID(t_func.getOBJECT());				 // 카테고리 코드
				
				String SUB_ROOT = categoryVO.getCATEGORY();					 // SUB_ROOT 필드에 넣을 값
				String sub_root_arr[] = SUB_ROOT.split("-",-1);
				
				// Depth가 0일 경우(공장단위)
				if("0".equals(categoryVO.getDEPTH())) {
					categoryVO.setPARENT_CATEGORY("LI-BR00");				 // 던킨사업부
					categoryVO.setSUB_ROOT("5100");							 // 공장구분
				}
				else {
					
					if(!"".equals(sub_root_arr[2])){
						categoryVO.setSUB_ROOT(sub_root_arr[2]);
					}else{
						categoryVO.setSUB_ROOT("");
					}
					
					categoryVO.setPARENT_CATEGORY(t_func.getPREDECESSOR());  // PREDECESSOR : 상위위치
				}
				
				
				categoryVO.setCATEGORY_NAME(t_func.getDESCRIPTION());		// 카테고리 명
				categoryVO.setTREE(t_func.getOBJECT());						// 구조체
				categoryVO.setREG_ID(ssUserId);								// 등록자
				categoryVO.setUserId(ssUserId);								// 등록자
				categoryVO.setCOMPANY_ID(ssCompanyId);						// 회사코드
				                                                        	
				if("X".equals(t_func.getINACT())) {							// 비활성유무
					categoryVO.setDEL_YN("Y");								// 삭제유무
				} else {                                                
					categoryVO.setDEL_YN("N");                          
				}                                                       
				                                                        
				// 기능위치 등록
				equipmentMasterMapper.insertFuncSite(categoryVO);
				
				// 공장 단위일 때 공통 테이블에 LOCATION 정보 등록
				if("1".equals(categoryVO.getDEPTH())) {
					codeVO = new CodeVO();
					codeVO.setCOMCD_GRP("LOCATION");
					codeVO.setCOMPANY_ID(ssCompanyId);
					codeVO.setREG_ID(ssUserId);
					codeVO.setCOMCD(categoryVO.getSUB_ROOT());
					codeVO.setCOMCD_NM(categoryVO.getCATEGORY_NAME());
					codeVO.setCOMCD_DESC(categoryVO.getCATEGORY_NAME());
					
					if("AY".equals(codeVO.getCOMCD())) {
						codeVO.setREMARKS("01");
					}else{
						codeVO.setREMARKS("02");
					}
					
					codeVO.setLOCATION("5100");
					
					if("X".equals(t_func.getINACT())) {							// 비활성유무
						codeVO.setDEL_YN("Y");									// 삭제유무
					} else {                                                
						codeVO.setDEL_YN("N");                          
					}
					
					//공통코드순서 조회
					String COMCD_ORD = codeMapper.selectComcdOrd(codeVO);
					codeVO.setCOMCD_ORD(COMCD_ORD);
					
					//공통코드상세 등록
					codeMapper.insertCodeDtl(codeVO);
				}
				
			}                                                           
			for (ZPM_FUNC_DN_T_FUNC t_func : tables.getT_FUNC()) {      
				categoryVO = new CategoryVO();                          
				                                                        
				categoryVO.setCATEGORY(t_func.getOBJECT());             
				categoryVO.setCOMPANY_ID(ssCompanyId);                  
				categoryVO.setLOCATION("5100");								//던킨:안양공장-5100
				
				// Tree 업데이트
				equipmentMasterMapper.updateTree(categoryVO);
			}
		}		
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("companyId", ssCompanyId);
		param.put("location", "5100");
		param.put("userId", ssUserId);
		
		// 기능위치 출력순서 SEQ - UPDATE
		equipmentMasterMapper.updateFuncSiteSeq(param);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("E_RESULT", tables.getE_RESULT());
		map.put("E_MESSAGE", tables.getE_MESSAGE());
		logger.info("E_RESULT:" + tables.getE_RESULT());
		logger.info("E_MESSAGE:" + tables.getE_MESSAGE());
		
		return map;
	}

	/**
	 * SAP 설비마스터 등록
	 *
	 * @author 김영환
	 * @since 2018.11.13
	 * @param ZPM_EQUI_DN_EXPORT tables
	 * @return Map<String, Object>
	 * @throws Exception 
	 */
	@Override
	public HashMap<String, Object> updateEquipInfo(ZPM_EQUI_DN_EXPORT tables, ZPM_EQIP_DN_T_PLANT t_plantvo) throws Exception {
		
		EquipmentMasterVO equipmentMasterVO = new EquipmentMasterVO();
		String ssUserId = (String) SessionUtil.getAttribute("ssUserId");
		String ssCompanyId = (String) SessionUtil.getAttribute("ssCompanyId");
		
		//설비마스터 등록
		if(tables != null){
			for (ZPM_EQIP_DN_T_EQUP zpm_eqip_dn_t_equp : tables.getZpm_eqip_dn_t_equp()) {

				equipmentMasterVO = new EquipmentMasterVO();
				equipmentMasterVO.setEQUIPMENT_UID(zpm_eqip_dn_t_equp.getEQUNR());		//EQUIPMENT_UID:설비코드, EQUNR: SAP 설비번호
				
				if(equipmentMasterMapper.equipmentCheck(equipmentMasterVO) == 0){
					
					EquipmentMasterVO equipmentMasterVO2 = new EquipmentMasterVO();
					String seqEquipment = equipmentMasterMapper.selectEquipmentSeq();	//설비마스터 SEQ 조회
					String attachGrpNo2 = attachMapper.selectAttachGrpNo();				//파일그룹 시퀀스 조회
					equipmentMasterVO2.setEQUIPMENT(seqEquipment);
					equipmentMasterVO2.setATTACH_GRP_NO2(attachGrpNo2);	
					equipmentMasterVO2.setCOMPANY_ID(zpm_eqip_dn_t_equp.getBUKRS());	//BUKRS:회사코드
					
					//설비마스터 QR코드 생성
					createQrImage(equipmentMasterVO2);
					
					//설비SEQ 및 QR코드SEQ 세팅
					equipmentMasterVO.setEQUIPMENT(seqEquipment);
					equipmentMasterVO.setATTACH_GRP_NO2(attachGrpNo2);
				}

				//equipmentMasterVO.setCOMPANY_ID(zpm_eqip_dn_t_equp.getBUKRS()); // 회사코드
				equipmentMasterVO.setCOMPANY_ID(ssCompanyId);					  // 회사코드
				equipmentMasterVO.setREG_ID(ssUserId);		 					  // 등록자
				equipmentMasterVO.setUPD_ID(ssUserId);		 					  // 수정자
				equipmentMasterVO.setLOCATION(zpm_eqip_dn_t_equp.getSWERK());	  // SWERK: 유지보수 플랜트
				equipmentMasterVO.setLINE(zpm_eqip_dn_t_equp.getTPLNR());		  // LINE: 라인ID, TPLNR: 설치기능위치
				
				if(!("".equals(zpm_eqip_dn_t_equp.getANLNR()) || zpm_eqip_dn_t_equp.getANLNR() == null)){	//자산번호: SAP데이터 'NULL'
					int ASSET_NO = Integer.parseInt(zpm_eqip_dn_t_equp.getANLNR());
					equipmentMasterVO.setASSET_NO(Integer.toString(ASSET_NO));
				}
				
				equipmentMasterVO.setEQUIPMENT_NAME(zpm_eqip_dn_t_equp.getEQKTX());		// EQKTX: 설비내역, EQUIPMENT_NAME: 설비 명
				equipmentMasterVO.setOBJ_TYPE(zpm_eqip_dn_t_equp.getEQART()); 			// EQART: 오브젝트유형
				equipmentMasterVO.setDIMENSION(zpm_eqip_dn_t_equp.getGROES());			// GROES: 크기/치수: SAP데이터 'NULL'
				equipmentMasterVO.setWEIGHT(zpm_eqip_dn_t_equp.getBRGEW().trim());		// BRGEW: 중량
				equipmentMasterVO.setWEIGHT_UNIT(zpm_eqip_dn_t_equp.getGEWEI());		// GEWEI: 중량 단위: SAP데이터 'NULL'
				equipmentMasterVO.setACQUISITION_VALUE(zpm_eqip_dn_t_equp.getANSWT().replaceAll(",", "").trim());	// ANSWT: 취득가액
				equipmentMasterVO.setCURRENCY(zpm_eqip_dn_t_equp.getWAERS());			// WAERS: 통화키: SAP데이터 'NULL'
				equipmentMasterVO.setCOUNTRY(zpm_eqip_dn_t_equp.getHERLD());			// HERLD: 제조국
				equipmentMasterVO.setDATE_ACQUISITION(zpm_eqip_dn_t_equp.getANSDT());	// ANSDT: 취득일
				equipmentMasterVO.setFIELD(zpm_eqip_dn_t_equp.getEQFNR());				// EQFNR: 정렬필드: SAP데이터 'NULL'
				equipmentMasterVO.setMANUFACTURER(zpm_eqip_dn_t_equp.getHERST());		// HERST: 제조사
				
				if(!("".equals(zpm_eqip_dn_t_equp.getKOSTL()) || zpm_eqip_dn_t_equp.getKOSTL() == null)){	//KOSTL: 코스트센터
					int COST_CENTER = Integer.parseInt(zpm_eqip_dn_t_equp.getKOSTL());
					equipmentMasterVO.setCOST_CENTER(Integer.toString(COST_CENTER));
				}
				
				equipmentMasterVO.setMODEL(zpm_eqip_dn_t_equp.getTYPBZ());			//TYPBZ: 모델번호: SAP데이터 'NULL'
				equipmentMasterVO.setDATE_INSTALL(zpm_eqip_dn_t_equp.getDATUM());	//DATUM: 설치/철거일자
				equipmentMasterVO.setDATE_OPERATE(zpm_eqip_dn_t_equp.getINBDT());	//INBDT: 가동시작일
				equipmentMasterVO.setDATE_VALID(zpm_eqip_dn_t_equp.getDATSL());		//DATSL: 유효일
				equipmentMasterVO.setE_RESULT("S");									//동기화 성공여부
								
				if("X".equals(zpm_eqip_dn_t_equp.getINACT())){						//INACT: 비활성유무
					equipmentMasterVO.setDEL_YN("Y");								//삭제여부
				}else{
					equipmentMasterVO.setDEL_YN("N");
				}
				
				// 설비마스터 등록 : MERGE UPDATE AND INSERT
				equipmentMasterMapper.insertEqup(equipmentMasterVO);
			}
		}
		
		//SapSync 시퀀스 조회
		String SapSyncSeq = equipmentMasterMapper.selectSapSyncSeq();
		
		equipmentMasterVO.setSAP_SYNC(SapSyncSeq);
		equipmentMasterVO.setFUNCTION_ID("ZPM_EQIP_DN");
		equipmentMasterVO.setUserId(ssUserId);
		equipmentMasterVO.setCOMPANY_ID(ssCompanyId);
		equipmentMasterVO.setLOCATION(t_plantvo.getPLANT());
		
		//SAP 동기화 날짜 등록 
		equipmentMasterMapper.insertSapSync(equipmentMasterVO);
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("E_RESULT", tables.getE_result());
		map.put("E_MESSAGE", tables.getE_message());
		logger.info("E_RESULT:" + tables.getE_result());
		logger.info("E_MESSAGE:" + tables.getE_message());
		
		return map;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 모바일
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	

	/**
	 * 모바일 - 설비마스터 목록 조회
	 * 
	 * @author 서정민
	 * @since 2018.04.11
	 * @param EquipmentMasterVO equipmentmasterVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectEquipmentMasterList2(EquipmentMasterVO equipmentmasterVO) throws Exception {
		return equipmentMasterMapper.selectEquipmentMasterList2(equipmentmasterVO);
	}
	
	/**
	 * 모바일 - 설비마스터 상세 조회
	 * 
	 * @author 서정민
	 * @since 2018.04.12
	 * @param EquipmentMasterVO equipmentmasterVO
	 * @return String, Object
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> selectEquipmentMasterDtl2(EquipmentMasterVO equipmentmasterVO) throws Exception {
		return equipmentMasterMapper.selectEquipmentMasterDtl2(equipmentmasterVO);
	}
}
