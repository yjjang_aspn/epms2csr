package aspn.hello.epms.equipment.master.model;

import aspn.hello.com.model.AbstractVO;

import java.util.Arrays;

/**
 * [설비마스터] VO Class
 * @author 서정민
 * @since 2018.02.07
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.07		서정민			최초 생성
 *   
 * </pre>
 */

public class EquipmentMasterVO extends AbstractVO{
	
	private static final long serialVersionUID = 1L;
	
	/** EPMS_EQUIPMENT DB **/
	private String EQUIPMENT				= ""; //설비 ID
	private String COMPANY_ID				= ""; //회사코드
	private String LOCATION					= ""; //위치(공장) ID
	private String LINE						= ""; //라인ID
	private String PARENT_EQUIPMENT			= ""; //부모설비 ID
	private String TREE						= ""; //구조체
	private String DEPTH					= ""; //레벨
	private String SEQ_DSP					= ""; //출력순서
	private String ASSET_NO					= ""; //자산번호
	private String COST_CENTER				= ""; //코스트센터
	private String EQUIPMENT_UID			= ""; //설비코드
	private String EQUIPMENT_NAME			= ""; //설비명
	private String GRADE					= ""; //설비등급
	private String MODEL					= ""; //모델명
	private String DIMENSION				= ""; //크기/치수 
	private String WEIGHT					= ""; //총중량
	private String WEIGHT_UNIT				= ""; //중량단위
	private String ACQUISITION_VALUE		= ""; //취득가액
	private String CURRENCY					= ""; //통화
	private String COUNTRY					= ""; //제조국
	private String MANUFACTURER				= ""; //자산제조사
	private String DATE_ACQUISITION			= ""; //취득일
	private String DATE_INSTALL				= ""; //설치일
	private String DATE_OPERATE				= ""; //가동일
	private String DATE_VALID				= ""; //유효일(효력시작일)
	private String ATTACH_GRP_NO			= ""; //이미지
	private String ATTACH_GRP_NO2			= ""; //QR코드
	private String ATTACH_GRP_NO3			= ""; //매뉴얼
	private String MEMO						= ""; //설비설명
	private String REG_ID					= ""; //등록자
	private String REG_DT					= ""; //등록일
	private String UPD_ID					= ""; //수정자
	private String UPD_DT					= ""; //수정일
	private String DEL_YN					= ""; //삭제여부
	private String E_RESULT					= ""; //SAP-성공여부
	private String E_MESSAGE				= ""; //SAP-메세지
	private String SEQ_EQUIPMENT			= ""; //설비시퀀스
	private String MODULE        			= ""; //모듈아이디(5:자산, 8:경비, 10:고객관리)
	private String FIELD        			= ""; //정렬필드
	private String OBJ_TYPE        			= ""; //오브젝트유형
	private String FUNCTION_ID				= ""; //함수_ID
	
//	private String EQTYP        			= ""; //SAP-설비범주
//	private String EQART        			= ""; //SAP-오브젝트유형
//	private String EARTX        			= ""; //SAP-오브젝트유형테스트
//	private String INVNR        			= ""; //재고번호
//	private String BAUJJ        			= ""; //설치년도
//	private String BAUMM        			= ""; //설치월
//	private String MAPAR        			= ""; //제조자부품번호
//	private String SERGE        			= ""; //제조자일련번호
//	private String SWERK        			= ""; //유지보수플랜트
//	private String BEBER        			= ""; //플랜트섹션
//	private String ARBPL        			= ""; //작업장
//	private String BUKRS        			= ""; //회사코드
//	private String GSBER        			= ""; //사업영역
//	private String IWERK        			= ""; //계획플랜트
//	private String INGRP        			= ""; //계획자그룹
//	private String GEWRK        			= ""; //주요작업장
//	private String RBNR        				= ""; //Catalogprofile
//	private String UZEIT        			= ""; //설치/철거시간
	
	
	/** CATEGORY DB  */
	private String CATEGORY   				= "";	//카테고리 코드
	private String NAME   					= "";	//카테고리 이름
	private String CATEGORY_TYPE			= "";	//카테고리 구분
	private String PARENT_CATEGORY			= "";	//부모카테고리
//	private String TREE   					= "";	//구조체
//	private String DEPTH	  				= "";	//레벨
//	private String SEQ_DSP					= "";	//출력순서
	private String SUB_ROOT					= "";	//공장코드(설비관리)
//	private String REG_ID					= "";
//	private String REG_DT					= "";
//	private String UPT_ID					= "";
//	private String UPT_DT					= "";
//	private String DEL_YN					= "";
	
	/** EPMS_EQUIPMENT_URL DB **/
	private String EQUIPMENT_URL			= ""; //URL ID
//	private String EQUIPMENT				= ""; //설비 ID
	private String URL_DESCRIPTION			= ""; //URL 설명
	private String URL_ADDRESS				= ""; //URL 주소
//	private String SEQ_DSP					= ""; //출력순서
//	private String REG_ID					= ""; //등록자
//	private String REG_DT					= ""; //등록일
//	private String UPD_ID					= ""; //수정자
//	private String UPD_DT					= ""; //수정일
//	private String DEL_YN					= ""; //삭제여부
	private String SEQ_EQUIPMENT_URL		= ""; //URL 시퀀스
	
	/** parameter */
	private String LINE_UID					= "";	//기능위치
	private String LINE_NAME				= "";	//라인
	private String ORG_LINE					= "";	//기존라인ID
	private String ORG_LINE_UID				= "";	//기존기능위치
	private String ORG_LINE_NAME			= "";	//기존라인
	private String SAP_SYNC					= "";	//SAP_SYNC 시퀀스
	private String I_OBJECT					= "";	//SAP 입력파라미터
	private String SAP_SYNC_FLAG			= "";	//전체업데이트 or 부분업데이트
	private String SAP_FACTORY				= "";	//공장: 7100-삼립 시화공장, 1100-샤니 성남공장, 1200-샤니 대구공장 
	
	public String getEQUIPMENT() {
		return EQUIPMENT;
	}
	public void setEQUIPMENT(String eQUIPMENT) {
		EQUIPMENT = eQUIPMENT;
	}
	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}
	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getLINE() {
		return LINE;
	}
	public void setLINE(String lINE) {
		LINE = lINE;
	}
	public String getPARENT_EQUIPMENT() {
		return PARENT_EQUIPMENT;
	}
	public void setPARENT_EQUIPMENT(String pARENT_EQUIPMENT) {
		PARENT_EQUIPMENT = pARENT_EQUIPMENT;
	}
	public String getTREE() {
		return TREE;
	}
	public void setTREE(String tREE) {
		TREE = tREE;
	}
	public String getDEPTH() {
		return DEPTH;
	}
	public void setDEPTH(String dEPTH) {
		DEPTH = dEPTH;
	}
	public String getSEQ_DSP() {
		return SEQ_DSP;
	}
	public void setSEQ_DSP(String sEQ_DSP) {
		SEQ_DSP = sEQ_DSP;
	}
	public String getASSET_NO() {
		return ASSET_NO;
	}
	public void setASSET_NO(String aSSET_NO) {
		ASSET_NO = aSSET_NO;
	}
	public String getCOST_CENTER() {
		return COST_CENTER;
	}
	public void setCOST_CENTER(String cOST_CENTER) {
		COST_CENTER = cOST_CENTER;
	}
	public String getEQUIPMENT_UID() {
		return EQUIPMENT_UID;
	}
	public void setEQUIPMENT_UID(String eQUIPMENT_UID) {
		EQUIPMENT_UID = eQUIPMENT_UID;
	}
	public String getEQUIPMENT_NAME() {
		return EQUIPMENT_NAME;
	}
	public void setEQUIPMENT_NAME(String eQUIPMENT_NAME) {
		EQUIPMENT_NAME = eQUIPMENT_NAME;
	}
	public String getGRADE() {
		return GRADE;
	}
	public void setGRADE(String gRADE) {
		GRADE = gRADE;
	}
	public String getMODEL() {
		return MODEL;
	}
	public void setMODEL(String mODEL) {
		MODEL = mODEL;
	}
	public String getDIMENSION() {
		return DIMENSION;
	}
	public void setDIMENSION(String dIMENSION) {
		DIMENSION = dIMENSION;
	}
	public String getWEIGHT() {
		return WEIGHT;
	}
	public void setWEIGHT(String wEIGHT) {
		WEIGHT = wEIGHT;
	}
	public String getWEIGHT_UNIT() {
		return WEIGHT_UNIT;
	}
	public void setWEIGHT_UNIT(String wEIGHT_UNIT) {
		WEIGHT_UNIT = wEIGHT_UNIT;
	}
	public String getACQUISITION_VALUE() {
		return ACQUISITION_VALUE;
	}
	public void setACQUISITION_VALUE(String aCQUISITION_VALUE) {
		ACQUISITION_VALUE = aCQUISITION_VALUE;
	}
	public String getCURRENCY() {
		return CURRENCY;
	}
	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}
	public String getCOUNTRY() {
		return COUNTRY;
	}
	public void setCOUNTRY(String cOUNTRY) {
		COUNTRY = cOUNTRY;
	}
	public String getMANUFACTURER() {
		return MANUFACTURER;
	}
	public void setMANUFACTURER(String mANUFACTURER) {
		MANUFACTURER = mANUFACTURER;
	}
	public String getDATE_ACQUISITION() {
		return DATE_ACQUISITION;
	}
	public void setDATE_ACQUISITION(String dATE_ACQUISITION) {
		DATE_ACQUISITION = dATE_ACQUISITION;
	}
	public String getDATE_INSTALL() {
		return DATE_INSTALL;
	}
	public void setDATE_INSTALL(String dATE_INSTALL) {
		DATE_INSTALL = dATE_INSTALL;
	}
	public String getDATE_OPERATE() {
		return DATE_OPERATE;
	}
	public void setDATE_OPERATE(String dATE_OPERATE) {
		DATE_OPERATE = dATE_OPERATE;
	}
	public String getDATE_VALID() {
		return DATE_VALID;
	}
	public void setDATE_VALID(String dATE_VALID) {
		DATE_VALID = dATE_VALID;
	}
	public String getATTACH_GRP_NO() {
		return ATTACH_GRP_NO;
	}
	public void setATTACH_GRP_NO(String aTTACH_GRP_NO) {
		ATTACH_GRP_NO = aTTACH_GRP_NO;
	}
	public String getATTACH_GRP_NO2() {
		return ATTACH_GRP_NO2;
	}
	public void setATTACH_GRP_NO2(String aTTACH_GRP_NO2) {
		ATTACH_GRP_NO2 = aTTACH_GRP_NO2;
	}
	public String getATTACH_GRP_NO3() {
		return ATTACH_GRP_NO3;
	}
	public void setATTACH_GRP_NO3(String aTTACH_GRP_NO3) {
		ATTACH_GRP_NO3 = aTTACH_GRP_NO3;
	}
	public String getMEMO() {
		return MEMO;
	}
	public void setMEMO(String mEMO) {
		MEMO = mEMO;
	}
	public String getREG_ID() {
		return REG_ID;
	}
	public void setREG_ID(String rEG_ID) {
		REG_ID = rEG_ID;
	}
	public String getREG_DT() {
		return REG_DT;
	}
	public void setREG_DT(String rEG_DT) {
		REG_DT = rEG_DT;
	}
	public String getUPD_ID() {
		return UPD_ID;
	}
	public void setUPD_ID(String uPD_ID) {
		UPD_ID = uPD_ID;
	}
	public String getUPD_DT() {
		return UPD_DT;
	}
	public void setUPD_DT(String uPD_DT) {
		UPD_DT = uPD_DT;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getE_RESULT() {
		return E_RESULT;
	}
	public void setE_RESULT(String e_RESULT) {
		E_RESULT = e_RESULT;
	}
	public String getE_MESSAGE() {
		return E_MESSAGE;
	}
	public void setE_MESSAGE(String e_MESSAGE) {
		E_MESSAGE = e_MESSAGE;
	}
	public String getSEQ_EQUIPMENT() {
		return SEQ_EQUIPMENT;
	}
	public void setSEQ_EQUIPMENT(String sEQ_EQUIPMENT) {
		SEQ_EQUIPMENT = sEQ_EQUIPMENT;
	}
	public String getMODULE() {
		return MODULE;
	}
	public void setMODULE(String mODULE) {
		MODULE = mODULE;
	}
	public String getFIELD() {
		return FIELD;
	}
	public void setFIELD(String fIELD) {
		FIELD = fIELD;
	}
	public String getOBJ_TYPE() {
		return OBJ_TYPE;
	}
	public void setOBJ_TYPE(String oBJ_TYPE) {
		OBJ_TYPE = oBJ_TYPE;
	}
	public String getCATEGORY() {
		return CATEGORY;
	}
	public void setCATEGORY(String cATEGORY) {
		CATEGORY = cATEGORY;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getCATEGORY_TYPE() {
		return CATEGORY_TYPE;
	}
	public void setCATEGORY_TYPE(String cATEGORY_TYPE) {
		CATEGORY_TYPE = cATEGORY_TYPE;
	}
	public String getPARENT_CATEGORY() {
		return PARENT_CATEGORY;
	}
	public void setPARENT_CATEGORY(String pARENT_CATEGORY) {
		PARENT_CATEGORY = pARENT_CATEGORY;
	}
	public String getSUB_ROOT() {
		return SUB_ROOT;
	}
	public void setSUB_ROOT(String sUB_ROOT) {
		SUB_ROOT = sUB_ROOT;
	}
	public String getEQUIPMENT_URL() {
		return EQUIPMENT_URL;
	}
	public void setEQUIPMENT_URL(String eQUIPMENT_URL) {
		EQUIPMENT_URL = eQUIPMENT_URL;
	}
	public String getURL_DESCRIPTION() {
		return URL_DESCRIPTION;
	}
	public void setURL_DESCRIPTION(String uRL_DESCRIPTION) {
		URL_DESCRIPTION = uRL_DESCRIPTION;
	}
	public String getURL_ADDRESS() {
		return URL_ADDRESS;
	}
	public void setURL_ADDRESS(String uRL_ADDRESS) {
		URL_ADDRESS = uRL_ADDRESS;
	}
	public String getSEQ_EQUIPMENT_URL() {
		return SEQ_EQUIPMENT_URL;
	}
	public void setSEQ_EQUIPMENT_URL(String sEQ_EQUIPMENT_URL) {
		SEQ_EQUIPMENT_URL = sEQ_EQUIPMENT_URL;
	}
	public String getLINE_UID() {
		return LINE_UID;
	}
	public void setLINE_UID(String lINE_UID) {
		LINE_UID = lINE_UID;
	}
	public String getLINE_NAME() {
		return LINE_NAME;
	}
	public void setLINE_NAME(String lINE_NAME) {
		LINE_NAME = lINE_NAME;
	}
	public String getORG_LINE() {
		return ORG_LINE;
	}
	public void setORG_LINE(String oRG_LINE) {
		ORG_LINE = oRG_LINE;
	}
	public String getORG_LINE_UID() {
		return ORG_LINE_UID;
	}
	public void setORG_LINE_UID(String oRG_LINE_UID) {
		ORG_LINE_UID = oRG_LINE_UID;
	}
	public String getORG_LINE_NAME() {
		return ORG_LINE_NAME;
	}
	public void setORG_LINE_NAME(String oRG_LINE_NAME) {
		ORG_LINE_NAME = oRG_LINE_NAME;
	}
	public String getFUNCTION_ID() {
		return FUNCTION_ID;
	}
	public void setFUNCTION_ID(String fUNCTION_ID) {
		FUNCTION_ID = fUNCTION_ID;
	}
	public String getSAP_SYNC() {
		return SAP_SYNC;
	}
	public void setSAP_SYNC(String sAP_SYNC) {
		SAP_SYNC = sAP_SYNC;
	}
	public String getI_OBJECT() {
		return I_OBJECT;
	}
	public void setI_OBJECT(String i_OBJECT) {
		I_OBJECT = i_OBJECT;
	}
	public String getSAP_SYNC_FLAG() {
		return SAP_SYNC_FLAG;
	}
	public void setSAP_SYNC_FLAG(String sAP_SYNC_FLAG) {
		SAP_SYNC_FLAG = sAP_SYNC_FLAG;
	}
	public String getSAP_FACTORY() {
		return SAP_FACTORY;
	}
	public void setSAP_FACTORY(String sAP_FACTORY) {
		SAP_FACTORY = sAP_FACTORY;
	}
			
}
