package aspn.hello.epms.equipment.master.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.SessionUtil;
import aspn.com.common.util.Utility;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.epms.equipment.master.model.EquipmentMasterVO;
import aspn.hello.epms.equipment.master.service.EquipmentMasterService;
import aspn.hello.rfc.model.ZPM_EQIP_DN_T_PLANT;
import aspn.hello.rfc.model.ZPM_EQUI_DN_EXPORT;
import aspn.hello.rfc.model.ZPM_EQUI_DN_IMPORT;
import aspn.hello.rfc.model.ZPM_FUNC_DN_EXPORT;
import aspn.hello.rfc.model.ZPM_FUNC_DN_IMPORT;
import aspn.hello.rfc.service.RfcService;

/**
 * [설비마스터] Controller Class
 * @author 서정민
 * @since 2018.02.07
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.07		서정민			최초 생성
 *   2018.11.12		김영환			SAP 설비마스터 조회
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/equipment/master")
public class EquipmentMasterController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	EquipmentMasterService equipmentMasterService;

	@Autowired
	AttachService attachService;
	
	@Autowired
	RfcService rfcService;
	
	@Value("#{config['equipment_hierarchy']}")
	private String equipment_hierarchy;

	/**
	 * [설비마스터 조회] : 화면 호출
	 *
	 * @author 서정민
	 * @since 2018.02.07
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentMasterList.do")
	public String equipmentMasterList(Model model) throws Exception{

		model.addAttribute("editYn", "N");
		model.addAttribute("equipment_hierarchy", equipment_hierarchy);
		
		return "/epms/equipment/master/equipmentMasterList";
	}
	
	/**
	 * [설비마스터 조회],[설비마스터 관리] : [설비] 그리드 데이터
	 *
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentMasterListData.do")
	public String equipmentListData(EquipmentMasterVO equipmentMasterVO, Model model) throws Exception{
		
		try {
			//이미지 팝업화면 수정가능여부(조회화면:N,관리화면:Y)
			String editYn = equipmentMasterVO.getEditYn();
			model.addAttribute("editYn", editYn);
			
			//설비마스터 목록
			equipmentMasterVO.setSubAuth(getSsSubAuth());
			List<HashMap<String, Object>> list= equipmentMasterService.selectEquipmentMasterList(equipmentMasterVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			logger.error("EquipmentMasterController.equipmentMasterListData > " + e.toString());
			e.printStackTrace();
		}
		return "/epms/equipment/master/equipmentMasterListData";
	}
	
	/**
	 * [설비마스터 조회] : [설비] 그리드 레이아웃
	 *
	 * @author 서정민
	 * @since 2018.02.07
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentMasterListLayout.do")
	public String equipmentListLayout(Model model) throws Exception{
		
		model.addAttribute("equipment_hierarchy", equipment_hierarchy);
		
		return "/epms/equipment/master/equipmentMasterListLayout";
	}
	
	/**
	 * [설비마스터 관리] : 화면 호출
	 *
	 * @author 서정민
	 * @since 2018.02.07
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentMasterMng.do")
	public String equipmentMasterMng(Model model) throws Exception{
		
		model.addAttribute("editYn", "Y");
		model.addAttribute("equipment_hierarchy", equipment_hierarchy);
		
		return "/epms/equipment/master/equipmentMasterMng";
	}
	
	/**
	 * [설비마스터 관리] : [설비] 그리드 레이아웃
	 *
	 * @author 서정민
	 * @since 2018.02.07
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentMasterMngLayout.do")
	public String equipmentMasterMngLayout(Model model) throws Exception{
		
		model.addAttribute("equipment_hierarchy", equipment_hierarchy);
		
		return "/epms/equipment/master/equipmentMasterMngLayout";
	}
	
	/**
	 * [설비마스터 관리] : 설비 그리드 저장
	 *
	 * @author 서정민
	 * @since 2018.02.07
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentMasterEdit.do")
	@ResponseBody
	public String equipmentListEdit(EquipmentMasterVO equipmentMasterVO, Model model) throws Exception {
		
		try {
			equipmentMasterVO.setUserId(this.getSsUserId());
			equipmentMasterService.equipmentMasterListEdit( Utility.getEditDataList(equipmentMasterVO.getUploadData()) , equipmentMasterVO);
			
			return getSaved();
			
		} catch (Exception e) {
			logger.error("EquipmentMasterController.equipmentMasterEdit > " + e.toString());
			e.printStackTrace();
			return getSaveFail();
		}
	}
	
	/**
	 * [설비마스터 조회],[설비마스터 관리] : 설비 관련 URL 팝업 호출
	 *
	 * @author 서정민
	 * @since 2018.04.13
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popEquipmentUrlList.do")
	@ResponseBody
	public HashMap<String, Object> popEquipmentUrlList(EquipmentMasterVO equipmentMasterVO, Model model) throws Exception {
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		try {
			
			HashMap<String, Object> item = equipmentMasterService.selectEquipmentMasterDtl(equipmentMasterVO);
			result.put("item", item);
			
		} catch (Exception e) {
			logger.error("EquipmentMasterController.popEquipmentUrlList > " + e.toString());
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * [설비마스터 조회],[설비마스터 관리] : [설비 관련 URL 팝업] URL 그리드 데이터
	 *
	 * @author 서정민
	 * @since 2018.04.13
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentUrlListData.do")
	public String equipmentUrlListData(EquipmentMasterVO equipmentMasterVO, Model model) throws Exception{
		
		try {
			//URL 리스트
			List<HashMap<String, Object>> list= equipmentMasterService.selectEquipmentUrlList(equipmentMasterVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			logger.error("EquipmentMasterController.equipmentUrlListData > " + e.toString());
			e.printStackTrace();
		}
		return "/epms/equipment/master/equipmentUrlListData";
	}
	
	/**
	 * [설비마스터 조회],[설비마스터 관리] : [설비 관련 URL 팝업] URL 그리드 레이아웃
	 *
	 * @author 서정민
	 * @since 2018.04.13
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentUrlListLayout.do")
	public String equipmentUrlListLayout(Model model) throws Exception{
		
		return "/epms/equipment/master/equipmentUrlListLayout";
	}
	
	/**
	 * [설비마스터 관리] : [설비 관련 URL 팝업] URL 그리드 저장
	 *
	 * @author 서정민
	 * @since 2018.04.13
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/equipmentUrlEdit.do")
	@ResponseBody
	public String equipmentUrlEdit(EquipmentMasterVO equipmentMasterVO, Model model) throws Exception {
		
		try {
			equipmentMasterVO.setUserId(this.getSsUserId());
			equipmentMasterService.equipmentUrlListEdit( Utility.getEditDataList(equipmentMasterVO.getUploadData()) , equipmentMasterVO);
			
			return getSaved();
			
		} catch (Exception e) {
			logger.error("EquipmentMasterController.equipmentMasterEdit > " + e.toString());
			e.printStackTrace();
			return getSaveFail();
		}
	}
	
	/**
	 * [설비마스터 조회],[설비마스터 관리] : 설비마스터 상세 팝업 호출
	 *
	 * @author 서정민
	 * @since 2018.04.13
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popEquipmentMasterDtl.do")
	public String popEquipmentMasterDtl(EquipmentMasterVO equipmentMasterVO, Model model) throws Exception{
		
		HashMap<String, Object> equipmentDtl = equipmentMasterService.selectEquipmentMasterDtl(equipmentMasterVO);
		model.addAttribute("equipmentDtl", equipmentDtl);
		
		AttachVO attachVO = new AttachVO();
		
		// 이미지
		if(equipmentDtl.get("ATTACH_GRP_NO") != null){
			attachVO.setATTACH_GRP_NO(equipmentDtl.get("ATTACH_GRP_NO").toString());
			List<HashMap<String, Object>> attachList = attachService.selectFileList(attachVO);
			model.addAttribute("attachList", attachList);
		}
		
		// 매뉴얼
		if(equipmentDtl.get("ATTACH_GRP_NO3") != null){
			attachVO.setATTACH_GRP_NO(equipmentDtl.get("ATTACH_GRP_NO3").toString());
			List<HashMap<String, Object>> manualList = attachService.selectFileList(attachVO);
			model.addAttribute("manualList", manualList);
		}
		
		// 참고URL
		List<HashMap<String, Object>> urlList= equipmentMasterService.selectEquipmentUrlList(equipmentMasterVO);
		model.addAttribute("urlList", urlList);
		
		return "/epms/equipment/master/popEquipmentMasterDtl";
	}
	
	/**
	 * [설비마스터 관리] : QR코드 인쇄 설정 팝업 호출
	 *
	 * @author 서정민
	 * @since 2018.06.14
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popQrcodePrintConfigForm.do")
	public String popQrcodePrintConfigForm(Model model) throws Exception{
		
		return "/epms/equipment/master/popQrcodePrintConfigForm";
	}
	
	/**
	 * [설비마스터 관리] : QR코드 인쇄 팝업 호출
	 *
	 * @author 서정민
	 * @since 2018.06.14
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popQrcodePrintForm.do")
	public String popQrcodePrintForm(Model model) throws Exception{
		
		return "/epms/equipment/master/popQrcodePrintForm";
	}

	/**
	 * [설비마스터 관리] : QR코드 일괄생성
	 *
	 * @author 서정민
	 * @since 2018.08.09
	 * @param EquipmentMasterVO equipmentMasterVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/qrcodeCreateAjax.do")
	@ResponseBody
	public HashMap<String, Object> qrcodeCreateAjax(EquipmentMasterVO equipmentMasterVO, Model model) throws Exception{
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		try {
			
			equipmentMasterVO.setUserId(getSsUserId());
			equipmentMasterVO.setSubAuth(getSsSubAuth());
			
			resultMap = equipmentMasterService.createQrCode(equipmentMasterVO);
			
		}catch (Exception e) {
			logger.error("EquipmentMasterController.qrcodeCreateAjax Error !");
			e.printStackTrace();
		}
		
		return resultMap;
	}
	
	/**
	 * [설비마스터 관리] : SAP 동기화 팝업 호출
	 *
	 * @author 김영환
	 * @since 2018.06.14
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popSyncEquipment.do")
	public String popSyncEquipment(Model model) throws Exception{
			
		return "/epms/equipment/master/popSyncEquipment";
	}
	
	/**
	 * [설비마스터 관리] : SAP 기능위치 업데이트
	 *
	 * @author 김영환
	 * @since 2018.11.10
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateFuncSiteInfo.do")
	@ResponseBody
	public Map<String, Object> updateFuncSiteInfo() throws Exception {
		
		ZPM_FUNC_DN_IMPORT zpm_func_dn_import = new ZPM_FUNC_DN_IMPORT();
		zpm_func_dn_import.setI_OBJECT("BR00-5100"); //SAP 필수 입력 파라미터 :던킨 안양공장
		/*
			I_OBJECT = 기능위치코드부터 최하위까지 조회됨
			 ex)
				던킨 : BR00
		     	삼립시화 : SL-1
				샤니성남 : A100-1100
				샤니대구 : A100-1200
		*/
		
		// SAP에서 기능위치 정보 가져오기
		ZPM_FUNC_DN_EXPORT tables = rfcService.getFuncSiteInfo(zpm_func_dn_import);
		// SAP에서 가져온 기능위치 정보 저장
		Map<String, Object> map = equipmentMasterService.updateFuncSiteInfo(tables, zpm_func_dn_import);
		
		return map;
	}
	
	/**
	 * SAP 설비마스터 등록
	 *
	 * @author 김영환
	 * @since 2018.11.12
	 * @param ZPMPC_EQIP_DN_IMPORT zpmpc_eqip_dn_import
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateEquipInfoAjax.do")
	@ResponseBody
	public Map<String, Object> updateEquipInfo(EquipmentMasterVO equipmentMasterVO) throws Exception {
		
		String SsCompanyId = (String)SessionUtil.getAttribute("ssCompanyId");
		
		equipmentMasterVO.setLOCATION("5100");
		equipmentMasterVO.setFUNCTION_ID("ZPM_EQIP_DN");
		equipmentMasterVO.setCOMPANY_ID(SsCompanyId);
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		if("all".equals(equipmentMasterVO.getSAP_SYNC_FLAG())){
			rtnMap = null;
		} else if ("onlyUpdate".equals(equipmentMasterVO.getSAP_SYNC_FLAG())) {
			// 동기화 날짜 가져오기
			rtnMap = equipmentMasterService.selectSapSyncDay(equipmentMasterVO);
		}
		
		ZPM_EQUI_DN_IMPORT zpm_equi_dn_import = new ZPM_EQUI_DN_IMPORT();
		
		if(rtnMap == null){//전체 업데이트
			zpm_equi_dn_import.setI_YYYYMMDD("");
		} else { 		   //수정된건만 업데이트
			zpm_equi_dn_import.setI_YYYYMMDD(String.valueOf(rtnMap.get("REG_DT")));
		}

		ZPM_EQIP_DN_T_PLANT t_plantvo = new ZPM_EQIP_DN_T_PLANT();
		t_plantvo.setPLANT("5100");		// 플랜트 : 필수값
		List<ZPM_EQIP_DN_T_PLANT> t_plantList = new ArrayList<ZPM_EQIP_DN_T_PLANT>();
		t_plantList.add(0,t_plantvo);
		
		// SAP에서 설비마스터 가져오기
		ZPM_EQUI_DN_EXPORT tables = rfcService.getEquipInfo(zpm_equi_dn_import, t_plantList);
		// SAP에서 가져온 설비마스터 정보 저장
		Map<String, Object> map = equipmentMasterService.updateEquipInfo(tables, t_plantvo);
		
		return map;
	}
}
