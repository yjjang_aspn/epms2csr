package aspn.hello.epms.material.master.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.sys.model.CategoryVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [자재관리-자재정보] Mapper Class
 * 
 * @author 김영환
 * @since 2018.02.20
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.20		김영환			최초 생성
 *   2018.11.14		김영환			SAP 자재 마스터 조회 
 *   
 * </pre>
 */

@Mapper("materialMasterMapper")
public interface MaterialMasterMapper {
	
	
	//////////////////////////
	// 웹 및 공통
	//////////////////////////
	
	/**
	 * 자재마스터 목록 조회
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialMasterVO materialMasterVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> selectMaterialMasterList(MaterialMasterVO materialMasterVO);

	/**
	 * 자재 등록여부 체크
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialMasterVO materialMasterVO
	 * @return List<HashMap<String, Object>>
	 */
	public int selectMaterialCheck(MaterialMasterVO materialMasterVO);

	/**
	 *  MATERIAL UPDATE (자재정보 수정)
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialMasterVO materialMasterVO
	 * @return List<HashMap<String, Object>>
	 */
	public int updateMaterialMaster(MaterialMasterVO materialMasterVO);

	/**
	 * 자재 활성/비활성 상태 업데이트
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialMasterVO materialMasterVO
	 * @return List<HashMap<String, Object>>
	 */
	public int updateMaterialStatus(MaterialMasterVO materialMasterVO);
	
	/**
	 * 자재에 연결된 설비BOM 전체 활성/비활성 상태 업데이트
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialMasterVO materialMasterVO
	 * @return List<HashMap<String, Object>>
	 */
	public int updateEquipmentBomStatus(MaterialMasterVO materialMasterVO);
	
	/**
	 * 자재에 연결된 예방보전BOM 활성/비활성 상태 업데이트
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialMasterVO materialMasterVO
	 * @return List<HashMap<String, Object>>
	 */
	public int updateMaintenanceBomStatus(MaterialMasterVO materialMasterVO);

	/**
	 * 자재정보 등록
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialMasterVO materialMasterVO
	 * @return List<HashMap<String, Object>>
	 */
	public int insertMaterialMaster(MaterialMasterVO materialMasterVO);

	/**
	 * 자재 정보 조회
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialMasterVO materialMasterVO
	 * @return HashMap
	 */
	public HashMap<String, Object> selectMaterialDtl(MaterialMasterVO materialMasterVO);
	
	/**
	 * 첨부파일 정보 호출
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param RepairRequestVO materialMasterVO
	 * @return HashMap
	 */
	public List<HashMap<String, Object>> selectAttachList(MaterialMasterVO materialMasterVO);

	/**
	 * 자재관리 자재등록 insert (관리자메뉴)
	 * @author 김영환
	 * @since 2018.03.21
	 * @param MaterialMasterVO materialMasterVO
	 * @return int
	 * @throws Exception
	 */
	public int insertMaterialAdd(MaterialMasterVO materialMasterVO);

	/**
	 * 자재관리 자재이미지 수정
	 * 
	 * @author 김영환
	 * @since 2018.03.21
	 * @param MaterialMasterVO materialMasterVO
	 * @return int
	 * @throws Exception
	 */
	public int updateMaterialImgModify(MaterialMasterVO materialMasterVO);
	
	/**
	 * 현재 안전재고/재고 수량 조회
	 *
	 * @author 김영환
	 * @since 2018.03.12
	 * @param RepairResultVO repairResultVO
	 * @return 
	 */
	HashMap<String, Object> selectMaterialMasterStock(MaterialMasterVO materialMasterVO);
	
	/**
	 * 안전재고/재고가 다를경우 DATE_ALERT 필드 업데이트
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MaterialMasterVO materialMasterVO
	 * @return HashMap
	 */
	public void updateMaterialMasterDateAlert(MaterialMasterVO materialMasterVO);
	
	/**
	 * SAP 자재마스터확인
	 *
	 * @author 김영환
	 * @since 2018.11.12
	 * @param ZPM_ITEM_T_ITEM zpm_item_t_item
	 * @return int
	 */
	public int selectMaterialCheck2(MaterialMasterVO materialMasterVO);
	
	/**
	 * 자재 시퀀스 조회
	 *
	 * @author 김영환
	 * @since 2018.11.14
	 * @return String
	 */
	public String selectMaterialSeq();
	
	/**
	 * SAP 자재그룹 삭제 
	 *
	 * @author 김영환
	 * @param MaterialMasterVO materialMasterVO
	 * @since 2018.10.01
	 */	
	public void deleteMaterialGroup(HashMap<String, Object> param1);
	
	/**
	 * SAP 자재그룹 등록 
	 *
	 * @author 김영환
	 * @param MaterialMasterVO materialMasterVO
	 * @since 2018.10.01
	 */	
	public void insertMaterialGroup(CategoryVO categoryVO);
	
	/**
	 * SAP 자재그룹 tree 업데이트 
	 *
	 * @author 김영환
	 * @param CategoryVO categoryVO
	 * @since 2018.10.01
	 */
	public void updateTree(CategoryVO categoryVO);
	
	/**
	 * SAP:자재그룹 출력순서 
	 *
	 * @author 김영환
	 * @param HashMap<String, Object> param
	 * @since 2018.10.01
	 */
	public void updateMaterialGroupSeq(HashMap<String, Object> param);
	
	/**
	 * SAP 자재마스터 업데이트
	 *
	 * @author 김영환
	 * @since 2018.11.17
	 * @param MaterialMasterVO materialMasterVO
	 * @return int
	 */
	public int insertMaterial(MaterialMasterVO materialMasterVO);
	
	//////////////////////////
	// 모바일
	//////////////////////////
	
	/**
	 * 모바일 - 자재마스터 목록 조회
	 *
	 * @author 서정민
	 * @since 2018.04.12
	 * @param MaterialMasterVO materialMasterVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> selectMaterialMasterList2(MaterialMasterVO materialMasterVO);
	
	/**
	 * 모바일 - 자재마스터 상세 조회
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MaterialMasterVO materialMasterVO
	 * @return HashMap
	 */
	public HashMap<String, Object> selectMaterialDtl2(MaterialMasterVO materialMasterVO);

	

	

	

	

	
	

}
