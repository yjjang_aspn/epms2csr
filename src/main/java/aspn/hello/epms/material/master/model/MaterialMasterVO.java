package aspn.hello.epms.material.master.model;

import aspn.hello.com.model.AbstractVO;

/**
 * [자재관리-자재정보] VO Class
 * @author 김영환
 * @since 2018.02.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.09		김영환		최초 생성
 *   
 * </pre>
 */

public class MaterialMasterVO extends AbstractVO{
	private static final long serialVersionUID = 1L;
	
	/** MATERIAL DB model  */
	private String MATERIAL						= ""; //자재 ID
	private String COMPANY_ID					= ""; //회사 ID
	private String LOCATION						= ""; //공장
	private String WAREHOUSE					= ""; //보관위치(창고)
	private String MATERIAL_TYPE				= ""; //자재유형
	private String MATERIAL_GRP1				= ""; //대그룹
	private String MATERIAL_UID					= ""; //자재코드
	private String MATERIAL_NAME				= ""; //자재명
	private String MODEL						= ""; //모델명
	private String SPEC							= ""; //규격/사양
	private String COUNTRY						= ""; //제조국
	private String MANUFACTURER					= ""; //제조사
	private String UNIT							= ""; //단위
	private String STOCK						= ""; //현재고
	private String STOCK_OPTIMAL				= ""; //안전재고
	private String MEMO							= ""; //메모
	private String DEL_YN						= ""; //삭제여부
	private String SEQ_MATERIAL					= ""; //자재시퀀스
	private String DASHBOARD_OPT1				= ""; //재고부족 대상(대시보드)
	private String DASHBOARD_OPT2				= ""; //재고현황 대상(대시보드)
	private String E_RESULT						= ""; //SAP 동기화 성공여부
	private String E_MESSAGE					= ""; //SAP-메세지
	private String REG_ID						= ""; //등록자
	private String REG_DT						= ""; //등록일
	private String UPD_ID						= ""; //수정자
	private String UPD_DT						= ""; //수정일
	
	/** ATTACH DB model  */
	private String ATTACH_GRP_NO				= ""; //파일그룹번호
	private String MODULE						= ""; //모듈 아이디
	
	/** File DB model  */
	private String DEL_SEQ                    	= ""; //파일삭제
	
	/** CATEGORY DB model */
	private String CATEGORY					= ""; //카테고리 ID 
	private String CATEGORY_TYPE			= ""; //카테고리 구분
	private String TREE						= ""; //구조체
	
	/** EPMS_SAP_SYNC DB model */
	private String FUNCTION_ID				= ""; //함수이름
	private String SAP_SYNC					= ""; //SAP_SYNC 시퀀스
	
	/** parameter model */
	private String BUDAT					= ""; //전기일자(작업종료일자)
	private String ADATE					= ""; //유통기한
	
	public String getMATERIAL() {
		return MATERIAL;
	}
	public void setMATERIAL(String mATERIAL) {
		MATERIAL = mATERIAL;
	}
	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}
	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getWAREHOUSE() {
		return WAREHOUSE;
	}
	public void setWAREHOUSE(String wAREHOUSE) {
		WAREHOUSE = wAREHOUSE;
	}
	public String getMATERIAL_TYPE() {
		return MATERIAL_TYPE;
	}
	public void setMATERIAL_TYPE(String mATERIAL_TYPE) {
		MATERIAL_TYPE = mATERIAL_TYPE;
	}
	public String getMATERIAL_GRP1() {
		return MATERIAL_GRP1;
	}
	public void setMATERIAL_GRP1(String mATERIAL_GRP1) {
		MATERIAL_GRP1 = mATERIAL_GRP1;
	}
	public String getMATERIAL_UID() {
		return MATERIAL_UID;
	}
	public void setMATERIAL_UID(String mATERIAL_UID) {
		MATERIAL_UID = mATERIAL_UID;
	}
	public String getMATERIAL_NAME() {
		return MATERIAL_NAME;
	}
	public void setMATERIAL_NAME(String mATERIAL_NAME) {
		MATERIAL_NAME = mATERIAL_NAME;
	}
	public String getMODEL() {
		return MODEL;
	}
	public void setMODEL(String mODEL) {
		MODEL = mODEL;
	}
	public String getSPEC() {
		return SPEC;
	}
	public void setSPEC(String sPEC) {
		SPEC = sPEC;
	}
	public String getCOUNTRY() {
		return COUNTRY;
	}
	public void setCOUNTRY(String cOUNTRY) {
		COUNTRY = cOUNTRY;
	}
	public String getMANUFACTURER() {
		return MANUFACTURER;
	}
	public void setMANUFACTURER(String mANUFACTURER) {
		MANUFACTURER = mANUFACTURER;
	}
	public String getUNIT() {
		return UNIT;
	}
	public void setUNIT(String uNIT) {
		UNIT = uNIT;
	}
	public String getSTOCK() {
		return STOCK;
	}
	public void setSTOCK(String sTOCK) {
		STOCK = sTOCK;
	}
	public String getSTOCK_OPTIMAL() {
		return STOCK_OPTIMAL;
	}
	public void setSTOCK_OPTIMAL(String sTOCK_OPTIMAL) {
		STOCK_OPTIMAL = sTOCK_OPTIMAL;
	}
	public String getMEMO() {
		return MEMO;
	}
	public void setMEMO(String mEMO) {
		MEMO = mEMO;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getSEQ_MATERIAL() {
		return SEQ_MATERIAL;
	}
	public void setSEQ_MATERIAL(String sEQ_MATERIAL) {
		SEQ_MATERIAL = sEQ_MATERIAL;
	}
	public String getATTACH_GRP_NO() {
		return ATTACH_GRP_NO;
	}
	public void setATTACH_GRP_NO(String aTTACH_GRP_NO) {
		ATTACH_GRP_NO = aTTACH_GRP_NO;
	}
	public String getMODULE() {
		return MODULE;
	}
	public void setMODULE(String mODULE) {
		MODULE = mODULE;
	}
	public String getDEL_SEQ() {
		return DEL_SEQ;
	}
	public void setDEL_SEQ(String dEL_SEQ) {
		DEL_SEQ = dEL_SEQ;
	}
	public String getCATEGORY() {
		return CATEGORY;
	}
	public void setCATEGORY(String cATEGORY) {
		CATEGORY = cATEGORY;
	}
	public String getCATEGORY_TYPE() {
		return CATEGORY_TYPE;
	}
	public void setCATEGORY_TYPE(String cATEGORY_TYPE) {
		CATEGORY_TYPE = cATEGORY_TYPE;
	}
	public String getTREE() {
		return TREE;
	}
	public void setTREE(String tREE) {
		TREE = tREE;
	}
	public String getDASHBOARD_OPT1() {
		return DASHBOARD_OPT1;
	}
	public void setDASHBOARD_OPT1(String dASHBOARD_OPT1) {
		DASHBOARD_OPT1 = dASHBOARD_OPT1;
	}
	public String getDASHBOARD_OPT2() {
		return DASHBOARD_OPT2;
	}
	public void setDASHBOARD_OPT2(String dASHBOARD_OPT2) {
		DASHBOARD_OPT2 = dASHBOARD_OPT2;
	}
	public String getE_RESULT() {
		return E_RESULT;
	}
	public void setE_RESULT(String e_RESULT) {
		E_RESULT = e_RESULT;
	}
	public String getE_MESSAGE() {
		return E_MESSAGE;
	}
	public void setE_MESSAGE(String e_MESSAGE) {
		E_MESSAGE = e_MESSAGE;
	}
	public String getREG_ID() {
		return REG_ID;
	}
	public void setREG_ID(String rEG_ID) {
		REG_ID = rEG_ID;
	}
	public String getREG_DT() {
		return REG_DT;
	}
	public void setREG_DT(String rEG_DT) {
		REG_DT = rEG_DT;
	}
	public String getUPD_ID() {
		return UPD_ID;
	}
	public void setUPD_ID(String uPD_ID) {
		UPD_ID = uPD_ID;
	}
	public String getUPD_DT() {
		return UPD_DT;
	}
	public void setUPD_DT(String uPD_DT) {
		UPD_DT = uPD_DT;
	}
	public String getFUNCTION_ID() {
		return FUNCTION_ID;
	}
	public void setFUNCTION_ID(String fUNCTION_ID) {
		FUNCTION_ID = fUNCTION_ID;
	}
	public String getSAP_SYNC() {
		return SAP_SYNC;
	}
	public void setSAP_SYNC(String sAP_SYNC) {
		SAP_SYNC = sAP_SYNC;
	}
	public String getBUDAT() {
		return BUDAT;
	}
	public void setBUDAT(String bUDAT) {
		BUDAT = bUDAT;
	}
	public String getADATE() {
		return ADATE;
	}
	public void setADATE(String aDATE) {
		ADATE = aDATE;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
