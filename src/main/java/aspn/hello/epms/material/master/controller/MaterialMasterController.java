package aspn.hello.epms.material.master.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.SessionUtil;
import aspn.com.common.util.Utility;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.epms.equipment.master.model.EquipmentMasterVO;
import aspn.hello.epms.equipment.master.service.EquipmentMasterService;
import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.epms.material.master.service.MaterialMasterService;
import aspn.hello.rfc.model.ZPM_MATERIAL_EXPORT;
import aspn.hello.rfc.model.ZPM_MATERIAL_IMPORT;
import aspn.hello.rfc.model.ZPM_MATKL_EXPORT;
import aspn.hello.rfc.model.ZPM_STOCK_EXPORT;
import aspn.hello.rfc.model.ZPM_STOCK_IMPORT;
import aspn.hello.rfc.service.RfcService;


/**
 * [자재관리-자재정보] Controller Class
 * @author 김영환
 * @since 2018.02.19
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.19		김영환			최초 생성
 *   2018.11.14		김영환			SAP 자재 마스터 조회    
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/material/master")
public class MaterialMasterController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	MaterialMasterService materialMasterService;
	
	@Autowired
	EquipmentMasterService equipmentMasterService;
	
	@Autowired
	AttachService attachService;
	
	@Autowired
	RfcService rfcService;

	/**
	 * 자재정보 조회 : 메인화면
	 *
	 * @author 김영환
	 * @since 2018.02.19
	 * @param
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialMasterList.do")
	public String materialMasterList() throws Exception{
		return "/epms/material/master/materialMasterList";
	}
	
	/**
	 * 자재정보 조회 : 자재 데이터 Grid Data
	 * 
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialMasterVO materialMasterVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialMasterListData.do")
	public String materialMasterListData(MaterialMasterVO materialMasterVO, Model model) {
		try {
			//세션 아이디 조회
			materialMasterVO.setSubAuth(getSsSubAuth());
			//수정가능여부(조회화면:N,관리화면:Y)
			String editYn = materialMasterVO.getEditYn();
			model.addAttribute("editYn", editYn);
			
			//자재목록
			List<HashMap<String, Object>> list= materialMasterService.selectMaterialMasterList(materialMasterVO);
			model.addAttribute("list", list);
		} catch (Exception e) {
			logger.error("MaterialMasterController.materialMasterListData Error !");
			e.printStackTrace();
		}
		return "/epms/material/master/materialMasterListData";
	}
	
	/**
	 * 자재정보 조회 : 자재 조회용 Grid Layout
	 * 
	 * @author 김영환
	 * @since 2018.02.20
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialMasterListLayout.do")
	public String materialMasterListLayout(MaterialMasterVO materialMasterVO, Model model) throws Exception{

		String location = materialMasterVO.getLOCATION();
		model.addAttribute("location", location);
		
		return "/epms/material/master/materialMasterListLayout";
	}
	
	/**
	 * 자재정보 관리 : 메인화면
	 *
	 * @author 김영환
	 * @since 2018.02.19
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialMasterMng.do")
	public String materialMasterMng() throws Exception{
		return "/epms/material/master/materialMasterMng";
	}
	
	/**
	 * 자재정보 관리 : 자재 관리용 Grid Layout
	 * 
	 * @author 김영환
	 * @since 2018.02.19
	 * @param
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialMasterMngLayout.do")
	public String materialMasterMngLayout(MaterialMasterVO materialMasterVO, Model model) throws Exception{

		String location = materialMasterVO.getLOCATION();
		model.addAttribute("location", location);
		
		return "/epms/material/master/materialMasterMngLayout";
	}
	
	/**
	 * 자재정보 관리 : 자재정보 수정
	 * 
	 * @author 김영환
	 * @since 2018.02.22
	 * @param MaterialMasterVO materialMasterVO, 
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialMasterListEdit.do")
	@ResponseBody
	public String materialMasterListEdit(MaterialMasterVO materialMasterVO, Model model) {
		
		try{
			materialMasterVO.setUserId(getSsUserId());
			materialMasterService.materialMasterListEdit( Utility.getEditDataList(materialMasterVO.getUploadData()) , materialMasterVO);
			
			// Grid Alert 리턴
			return getSaved();
			
		}catch(Exception e){
			
			logger.error("MaterialMasterController.materialMasterListEdit Error !");
			e.printStackTrace();
			
			// Grid Alert 리턴
			return getSaveFail();
			
		}
	}
	
	/**
	 * 자재관리 자재등록 insert (관리자메뉴)
	 * 
	 * @author 김영환
	 * @since 2018.03.21
	 * @param HttpServletRequest request
	 * @param MaterialMasterVO materialMasterVO
	 * @param Model model
	 * @return int
	 * @throws Exception
	 */
	@RequestMapping(value = "/popMaterialAddInsert.do")
	@ResponseBody
	public int popMaterialAddInsert(HttpServletRequest request, MultipartHttpServletRequest mRequest, MaterialMasterVO materialMasterVO, Model model) {
		
		int rtnVal = 0;
		try {
			materialMasterVO.setUserId(getSsUserId());
			if("Y".equals(materialMasterVO.getAttachExistYn())){
				
				String attachGrpNo = attachService.selectAttachGrpNo(); 
		   
				AttachVO attachVO = new AttachVO();
				attachVO.setATTACH_GRP_NO(attachGrpNo);
				attachVO.setMODULE(materialMasterVO.getMODULE());
				materialMasterVO.setATTACH_GRP_NO(attachGrpNo);
				attachService.AttachEdit(request, mRequest, attachVO); 
				
			}
			
			rtnVal = materialMasterService.insertMaterialAdd(materialMasterVO);
		   
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return rtnVal;
	}
	
	/**
	 * 자재관리 입고/출고현황 보기 팝업 화면
	 * 
	 * @author 김영환
	 * @since 2016.04.22
	 * @param MaterialMasterVO materialMasterVO
	 * @param Model model
	 * @return HashMap<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/popMaterialInOutView.do")
	@ResponseBody
	public HashMap<String, Object> popMaterialInOutView(MaterialMasterVO materialMasterVO, Model model) {
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		try{
			result.put("startDt", DateTime.requestMmDay("12","yyyy-MM-dd"));
			result.put("endDt", DateTime.getDateString());
			result.put("MATERIAL", materialMasterVO.getMATERIAL());
			
			HashMap<String, Object> item = materialMasterService.selectMaterialDtl(materialMasterVO);
			result.put("item", item);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("MaterialMasterController.popMaterialInOutView Error > " + e.toString());
			
		}
		
		return result;
	}
		
	/**
	 * 자재관리 자재상세정보 팝업 화면 
	 * 
	 * @author 김영환
	 * @since 2018.03.23
	 * @param MaterialMasterVO materialMasterVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popMaterialMasterDtl.do")
	public String popMaterialMasterDtl(MaterialMasterVO materialMasterVO, Model model) throws Exception{
		
		//자재정보
		HashMap<String, Object> materialDtl = materialMasterService.selectMaterialDtl(materialMasterVO);
		model.addAttribute("materialDtl", materialDtl);

		//첨부파일
		if(materialDtl.get("ATTACH_GRP_NO") != null){
			AttachVO attachVO = new AttachVO();
			attachVO.setATTACH_GRP_NO(String.valueOf(materialDtl.get("ATTACH_GRP_NO")));
			List<HashMap<String, Object>> attachList = attachService.selectFileList(attachVO);
			model.addAttribute("attachList", attachList);
		}
		
		return "/epms/material/master/popMaterialMasterDtl";
	}
	
	/**
	 * 자재 첨부파일 상세보기
	 * 
	 * @author 김영환
	 * @since 2018.04.02
	 * @param attachVO
	 * @param model
	 * @return String
	 */
	@RequestMapping(value="/popMaterialImgView.do")
	public String popMaterialImgView(AttachVO attachVO, Model model) {
		
		try {
			List<HashMap<String, Object>> fileList = attachService.selectFileList(attachVO);
			model.addAttribute("fileList", fileList);
			
		} catch(Exception e) {
			logger.error("MaterialMasterController.popMaterialImgView > " + e.toString());
			e.printStackTrace();
		}
		
		return "/epms/material/master/popMaterialImgView";
		
	}
	
	/**
	 * SAP : 자재 그룹  
	 *
	 * @author 김영환
	 * @since 2018.10.01
	 * @param ZPM_MATERIAL_IMPORT zpm_material_import
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateMaterialGroupInfoAjax.do")
	@ResponseBody
	public Map<String, Object> updateMaterialGroupInfoAjax() throws Exception {
		
		//SAP : 자재그룹 동기화
		ZPM_MATKL_EXPORT tables = rfcService.getMateriaGrouplInfo();
		Map<String, Object> map = materialMasterService.updateMaterialGroupInfoAjax(tables);
		
		return map;
		
	}
	
	/**
	 * [자재마스터] : SAP 동기화 팝업 호출
	 *
	 * @author 김영환
	 * @since 2018.10.17
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popSyncMaterial.do")
	public String popSyncMaterial(Model model) throws Exception{
				
		return "/epms/material/master/popSyncMaterial";
	}
	
	/**
	 * SAP 자재 마스터 등록 
	 *
	 * @author 김영환
	 * @since 2018.11.14
	 * @param ZPM_MATERIAL_IMPORT zpm_material_import
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateMaterialInfoAjax.do")
	@ResponseBody
	public Map<String, Object> updateMaterialInfo(EquipmentMasterVO equipmentMasterVO) throws Exception {
		
		String SsCompanyId = (String)SessionUtil.getAttribute("ssCompanyId");
		
		// SAP 싱크 날짜 가져오기
		equipmentMasterVO.setLOCATION("5100"); 
		equipmentMasterVO.setFUNCTION_ID("ZPM_MATERIAL");
		equipmentMasterVO.setCOMPANY_ID(SsCompanyId);
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		if("all".equals(equipmentMasterVO.getSAP_SYNC_FLAG())){ //전체 업데이트
			rtnMap = null;
		} else if ("onlyUpdate".equals(equipmentMasterVO.getSAP_SYNC_FLAG())) {//수정된건만 업데이트
			rtnMap = equipmentMasterService.selectSapSyncDay(equipmentMasterVO);
		}

		ZPM_MATERIAL_IMPORT zpm_material_import = new ZPM_MATERIAL_IMPORT();
		
		if(rtnMap == null){
			zpm_material_import.setI_YYYYMMDD("");
		} else {
			zpm_material_import.setI_YYYYMMDD(String.valueOf(rtnMap.get("REG_DT"))); 
		}
		
		zpm_material_import.setI_MATNR("");			//자재번호 : 선택
		zpm_material_import.setI_WERKS("5100");		//던킨 : 고정
		zpm_material_import.setI_LGORT("");		    //저장위치 : 선택값
		ZPM_MATERIAL_EXPORT tables = rfcService.getMaterialInfo(zpm_material_import);
		Map<String, Object> map = materialMasterService.updateMaterialInfo(tables, zpm_material_import);
		
		return map;
	}
	
	/**
	 * SAP 자재 재고 등록 
	 *
	 * @author 김영환
	 * @since 2018.11.14
	 * @param ZPM_MATERIAL_IMPORT zpm_material_import
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateMaterialStockAjax.do")
	@ResponseBody
	public Map<String, Object> updateMaterialStockAjax(EquipmentMasterVO equipmentMasterVO) throws Exception {
		
		String SsCompanyId = (String)SessionUtil.getAttribute("ssCompanyId");
		
		// SAP 싱크 날짜 가져오기
		equipmentMasterVO.setLOCATION("5100"); 
		equipmentMasterVO.setFUNCTION_ID("ZPM_STOCK");
		equipmentMasterVO.setCOMPANY_ID(SsCompanyId);
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		if("all".equals(equipmentMasterVO.getSAP_SYNC_FLAG())){ //전체 업데이트
			rtnMap = null;
		} else if ("onlyUpdate".equals(equipmentMasterVO.getSAP_SYNC_FLAG())) {//수정된건만 업데이트
			rtnMap = equipmentMasterService.selectSapSyncDay(equipmentMasterVO);
		}

		ZPM_STOCK_IMPORT zpm_stock_import = new  ZPM_STOCK_IMPORT();
		
		if(rtnMap == null){
			zpm_stock_import.setI_YYYYMMDD("");	//없으면 전체
		} else {
			zpm_stock_import.setI_YYYYMMDD(String.valueOf(rtnMap.get("REG_DT")));
		}
		zpm_stock_import.setI_MATNR("");		//자재번호 : 선택
		zpm_stock_import.setI_YYYYMMDD("20190101");		//없으면 전체
		zpm_stock_import.setI_WERKS("5100");	//플랜트 : 5100 고정
		zpm_stock_import.setI_LGORT("9102");	//저장위치 : 9102 고정
		
		// SAP에서 자재 재고 정보 가져오기
		ZPM_STOCK_EXPORT tables = rfcService.getMaterialStockInfo(zpm_stock_import);
		
		// SAP에서 자재 재고 정보 저장
//		Map<String, Object> map = materialMasterService.updateMaterialStockInfo(tables, zpm_stock_import);
		
		return null;
	}
	
}
