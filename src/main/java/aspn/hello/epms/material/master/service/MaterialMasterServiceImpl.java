package aspn.hello.epms.material.master.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.util.SessionUtil;
import aspn.com.common.util.Utility;
import aspn.hello.epms.equipment.master.mapper.EquipmentMasterMapper;
import aspn.hello.epms.equipment.master.model.EquipmentMasterVO;
import aspn.hello.epms.material.master.mapper.MaterialMasterMapper;
import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.rfc.model.ZPM_MATERIAL_EXPORT;
import aspn.hello.rfc.model.ZPM_MATERIAL_IMPORT;
import aspn.hello.rfc.model.ZPM_MATERIAL_T_ITEM;
import aspn.hello.rfc.model.ZPM_MATKL_EXPORT;
import aspn.hello.rfc.model.ZPM_MATKL_T_MATKL;
import aspn.hello.sys.model.CategoryVO;

/**
 * [자재관리-자재정보] Business Implement Class
 * 
 * @author 김영환
 * @since 2018.02.20
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.20		김영환			최초 생성
 *   2018.11.14		김영환			SAP 자재 마스터 조회 
 *   2019.02.25		김영환			SAP 자재 마스터 동기화시 현재고와 저장위치를 추가 저장 
 *   
 * </pre>
 */

@Service
public class MaterialMasterServiceImpl implements MaterialMasterService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	MaterialMasterMapper materialMasterMapper;
	
	@Autowired
	EquipmentMasterMapper equipmentMasterMapper;
	
	//////////////////////////
	// 웹 및 공통
	//////////////////////////
	
	/**
	 * 자재관리 목록 조회
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialMasterVO materialMasterVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectMaterialMasterList(MaterialMasterVO materialMasterVO)  {
		return materialMasterMapper.selectMaterialMasterList(materialMasterVO);
	}

	/**
	 * 자재관리 그리드 수정
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param MaterialMasterVO materialMasterVO
	 * @return 
	 
	 */
	@Override
	public void materialMasterListEdit(List<HashMap<String, Object>> saveDataList, MaterialMasterVO materialMasterVO) {
		int rtnVal=0;
		if(saveDataList != null){
			
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				
				MaterialMasterVO MaterialMasterVO = Utility.toBean(tmpSaveData, materialMasterVO.getClass());
				// 공백 제거
				MaterialMasterVO.setMATERIAL_NAME(MaterialMasterVO.getMATERIAL_NAME().trim());
				MaterialMasterVO.setSPEC(MaterialMasterVO.getSPEC().trim());
				MaterialMasterVO.setMANUFACTURER(MaterialMasterVO.getMANUFACTURER().trim());
				// 등록/수정자 아이디 세팅
				MaterialMasterVO.setUserId(materialMasterVO.getUserId());
				//이미 등록되어있는 자재인지 체크
				int cnt = materialMasterMapper.selectMaterialCheck(MaterialMasterVO);
				
				if(tmpSaveData.containsKey("Added")) {
					if(cnt == 0) {
						rtnVal = materialMasterMapper.insertMaterialMaster(MaterialMasterVO);
					}
				}else if (tmpSaveData.containsKey("Changed")) {
					if(cnt == 0){

						//반영 전 현재 안전재고/재고 수량 조회
						HashMap<String, Object> before_stockMap = materialMasterMapper.selectMaterialMasterStock(MaterialMasterVO);
						int before_stock = Integer.parseInt(String.valueOf(before_stockMap.get("STOCK")));
						int before_stock_optimal = Integer.parseInt(String.valueOf(before_stockMap.get("STOCK_OPTIMAL")));
						
						String stock_flag = "N";	// 기존 재고보다 안전재고 미만일 경우 (STOCK_FLAG:'N')
						if(before_stock >= before_stock_optimal){
							stock_flag = "Y"; 		// 기존 재고가 안전재고 이상일 경우 (STOCK_FLAG:'Y')
						}
						
						rtnVal = materialMasterMapper.updateMaterialMaster(MaterialMasterVO);
						//자재 활성/비활성 상태 업데이트
						rtnVal = materialMasterMapper.updateMaterialStatus(MaterialMasterVO);
						
						// 기존 재고가 안전재고 이상이었는데, 반영 후 안전재고 미만이 될 경우 DATE_ALERT 등록
						HashMap<String, Object> after_stockMap = materialMasterMapper.selectMaterialMasterStock(MaterialMasterVO);
						int after_stock = Integer.parseInt(String.valueOf(after_stockMap.get("STOCK")));
						int after_stock_optimal = Integer.parseInt(String.valueOf(after_stockMap.get("STOCK_OPTIMAL")));
						
						if(stock_flag == "Y" && after_stock_optimal > after_stock){
							materialMasterMapper.updateMaterialMasterDateAlert(MaterialMasterVO);
						}
					}
				}else if(tmpSaveData.containsKey("Deleted")) {
					//rtnVal = adminMaterialMasterDao.materialMasterDelete(MaterialMasterMd);
				}
			}
		}
	}

	/**
	 * 자재 정보 조회
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialMasterVO materialMasterVO
	 * @return HashMap
	 */
	@Override
	public HashMap<String, Object> selectMaterialDtl(MaterialMasterVO materialMasterVO) {
		return materialMasterMapper.selectMaterialDtl(materialMasterVO);
	}
	
	/**
	 * 첨부파일 정보 호출
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialMasterVO materialMasterVO
	 * @return HashMap
	 */
	@Override
	public List<HashMap<String, Object>> selectAttachList(MaterialMasterVO materialMasterVO) {
		return materialMasterMapper.selectAttachList(materialMasterVO);
	}

	/**
	 * 자재관리 자재등록 insert (관리자메뉴)
	 * 
	 * @author 김영환
	 * @since 2018.03.21
	 * @param MaterialMasterVO materialMasterVO
	 * @return int
	 * @throws Exception
	 */
	@Override
	public int insertMaterialAdd(MaterialMasterVO materialMasterVO) {
		int rtnVal = 0;
		//이미 등록되어있는 자재인지 체크
		materialMasterVO.setMATERIAL_NAME(materialMasterVO.getMATERIAL_NAME().trim());
		materialMasterVO.setSPEC(materialMasterVO.getSPEC().trim());
		materialMasterVO.setMANUFACTURER(materialMasterVO.getMANUFACTURER().trim());
		int cnt = materialMasterMapper.selectMaterialCheck(materialMasterVO);
		
		if(cnt ==0){
			//자재등록 MATERIAL INSERT
			rtnVal = materialMasterMapper.insertMaterialAdd(materialMasterVO);
		}else{
			rtnVal = 0;
		}
			
		return rtnVal;		
	}

	/**
	 * 자재관리 자재이미지 수정
	 * 
	 * @author 김영환
	 * @since 2018.03.21
	 * @param MaterialMasterVO materialMasterVO
	 * @return int
	 * @throws Exception
	 */
	@Override
	public int updateMaterialImgModify(MaterialMasterVO materialMasterVO) {
		return materialMasterMapper.updateMaterialImgModify(materialMasterVO);
	}
	
	
	//////////////////////////
	// SAP
	//////////////////////////
	
	/**
	 * SAP-자재그룹 등록
	 * 
	 * @author 김영환
	 * @since 2018.10.01
	 * @param MaterialMasterVO materialMasterVO
	 * @return int
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> updateMaterialGroupInfoAjax(ZPM_MATKL_EXPORT tables) throws Exception {
		
		String ssCompanyId = (String) SessionUtil.getAttribute("ssCompanyId");
//		String ssLocation = (String) SessionUtil.getAttribute("ssLocation");
		String ssUserId = (String) SessionUtil.getAttribute("ssUserId");
		
		//자재그룹 삭제
		HashMap<String, Object> param1 = new HashMap<String, Object>();
		param1.put("companyId", ssCompanyId);
		param1.put("location", "5100");
		
		materialMasterMapper.deleteMaterialGroup(param1);
		
		//자재그룹 등록
		if(tables != null){
			CategoryVO categoryVO = new CategoryVO();
			
			for (ZPM_MATKL_T_MATKL t_matkl : tables.getZpm_matkl_t_matkl()) {
//				if(isNumber(t_matkl.getMATKL())){
						
						categoryVO = new CategoryVO();
						
						categoryVO.setCATEGORY(t_matkl.getMATKL()); 		//자재그룹
						categoryVO.setCATEGORY_UID(t_matkl.getMATKL()); 	//자재그룹
						categoryVO.setCATEGORY_NAME(t_matkl.getWGBEZ());	//자재그룹내역
						categoryVO.setPARENT_CATEGORY("MA-"+"5100");
						categoryVO.setCOMPANY_ID(ssCompanyId);
						categoryVO.setLOCATION("5100");
						categoryVO.setREG_ID(ssUserId);
						categoryVO.setDEL_YN("N");
						categoryVO.setDEPTH("3");
						
						materialMasterMapper.insertMaterialGroup(categoryVO);
//				}
			}
			for (ZPM_MATKL_T_MATKL t_matkl : tables.getZpm_matkl_t_matkl()) {
//				if(isNumber(t_matkl.getMATKL())){
						
						categoryVO = new CategoryVO();
						
						categoryVO.setCATEGORY(t_matkl.getMATKL()); 	//자재그룹
						categoryVO.setCOMPANY_ID(ssCompanyId);
						categoryVO.setLOCATION("5100");
						
						materialMasterMapper.updateTree(categoryVO);
//				}
			}
		}
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("companyId", ssCompanyId);
		param.put("location", "5100");
		param.put("userId", ssUserId);
		
		//시퀀스 변경
		materialMasterMapper.updateMaterialGroupSeq(param);
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("E_RESULT", tables.getE_RESULT());
		map.put("E_MESSAGE", tables.getE_MESSAGE());
		logger.info("E_RESULT:" + tables.getE_RESULT());
		logger.info("E_MESSAGE:" + tables.getE_MESSAGE());
		
		return map;
		
	}
	
	public static boolean isNumber(String str){
        boolean result = false;
         
        try{
            Double.parseDouble(str) ;
            result = true ;
        }catch(Exception e){}
         
         
        return result ;
    }
	
	/**
	 * SAP 자재 마스터 등록 
	 *
	 * @author 김영환
	 * @since 2018.11.14
	 * @param ZPM_MATERIAL_IMPORT zpm_material_import
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> updateMaterialInfo(ZPM_MATERIAL_EXPORT tables, ZPM_MATERIAL_IMPORT zpm_material_import) throws Exception {
		
		MaterialMasterVO materialMasterVO = new MaterialMasterVO();
		String ssCompanyId = (String) SessionUtil.getAttribute("ssCompanyId");
		String ssUserId = (String) SessionUtil.getAttribute("ssUserId");
		
		//자재마스터 등록
		if(tables != null){
			for (ZPM_MATERIAL_T_ITEM t_item : tables.getZpm_material_t_item()) {
				
				materialMasterVO = new MaterialMasterVO();
				materialMasterVO.setMATERIAL_UID(t_item.getMATNR());	//자재번호

				materialMasterVO.setCOMPANY_ID(ssCompanyId);
				materialMasterVO.setLOCATION(t_item.getWERKS());        //플랜트 
				materialMasterVO.setMATERIAL_NAME(t_item.getMAKTX());	//자재내역
				materialMasterVO.setMATERIAL_GRP1(t_item.getMTART());	//자재유형
				materialMasterVO.setMATERIAL_TYPE(t_item.getMATKL());	//자재그룹
				materialMasterVO.setSPEC(t_item.getGROES());			//자재규격:SAP에서 NULL로 받음
				materialMasterVO.setUNIT(t_item.getMEINS());			//기본단위
				materialMasterVO.setSTOCK(t_item.getLABST());			//자재재고:20190225 추가
				materialMasterVO.setWAREHOUSE(t_item.getLGORT());		//저장위치
				
				materialMasterVO.setREG_ID(ssUserId); //등록자
				materialMasterVO.setUPD_ID(ssUserId); //수정자
				
				//t_item.getMTBEZ(); //자재유형내역    ex)삼립-설비자재
				//t_item.getWGBEZ(); //자재그룹내역  ex)전기자재 : 참조정보, 담당자 지정용도
				//t_item.getEKGRP(); //구매그룹     ex)B04
				//t_item.getEKNAM(); //구매그룹내역  ex)설비  
				//t_item.getLGPRO(); //저장위치     ex)null
				
				//자재 마스터 등록
				materialMasterVO.setE_RESULT("S");
				if("X".equals(t_item.getLVORM())){ 						//삭제여부
					materialMasterVO.setDEL_YN("Y");
				}else{
					materialMasterVO.setDEL_YN("N");
				}
				
				materialMasterMapper.insertMaterial(materialMasterVO);
				
			}
		}
				
		//SapSync 시퀀스 조회
		String SapSyncSeq = equipmentMasterMapper.selectSapSyncSeq();
		
		EquipmentMasterVO equipmentMasterVO = new EquipmentMasterVO();
		
		equipmentMasterVO.setSAP_SYNC(SapSyncSeq);
		equipmentMasterVO.setFUNCTION_ID("ZPM_MATERIAL");
		equipmentMasterVO.setUserId(ssUserId);
		equipmentMasterVO.setCOMPANY_ID(ssCompanyId);
		equipmentMasterVO.setLOCATION(zpm_material_import.getI_WERKS());
		
		//SAP 동기화 날짜 등록 
		equipmentMasterMapper.insertSapSync(equipmentMasterVO);
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("E_RESULT", tables.getE_RESULT());
		map.put("E_MESSAGE", tables.getE_MESSAGE());
		logger.info("E_RESULT:" + tables.getE_RESULT());
		logger.info("E_MESSAGE:" + tables.getE_MESSAGE());
		
		return map;

	}
	
	//////////////////////////
	// 모바일
	//////////////////////////	


	/**
	 * 모바일 - 자재마스터 목록 조회
	 *
	 * @author 서정민
	 * @since 2018.04.12
	 * @param MaterialMasterVO materialMasterVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectMaterialMasterList2(MaterialMasterVO materialMasterVO)  {
		return materialMasterMapper.selectMaterialMasterList2(materialMasterVO);
	}
	
	/**
	 * 모바일 - 자재마스터 상세 조회
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MaterialMasterVO materialMasterVO
	 * @return HashMap
	 */
	@Override
	public HashMap<String, Object> selectMaterialDtl2(MaterialMasterVO materialMasterVO) {
		return materialMasterMapper.selectMaterialDtl2(materialMasterVO);
	}

	

}
