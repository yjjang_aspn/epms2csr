package aspn.hello.epms.material.master.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.rfc.model.ZPM_MATERIAL_EXPORT;
import aspn.hello.rfc.model.ZPM_MATERIAL_IMPORT;
import aspn.hello.rfc.model.ZPM_MATKL_EXPORT;

/**
 * [자재관리-자재정보] Service Class
 * 
 * @author 김영환
 * @since 2018.02.19
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.19		김영환			최초 생성
 *   2018.11.14		김영환			SAP 자재 마스터 조회    
 *   
 * </pre>
 */

public interface MaterialMasterService {
	
	
	//////////////////////////
	// 웹 및 공통
	//////////////////////////
	
	/**
	 * 자재마스터 목록 조회
	 *
	 * @author 김영환
	 * @since 2018.02.19
	 * @param MaterialMasterVO materialMasterVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectMaterialMasterList(MaterialMasterVO materialMasterVO);

	/**
	 * 자재관리 그리드 수정
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param List<HashMap<String, Object>> saveDataList, MaterialMasterVO materialMasterVO
	 * @return 
	 * @
	 */
	void materialMasterListEdit(List<HashMap<String, Object>> saveDataList, MaterialMasterVO materialMasterVO);

	/**
	 * 자재 정보 조회
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialMasterVO materialMasterVO
	 * @return HashMap
	 * @
	 */
	HashMap<String, Object> selectMaterialDtl(MaterialMasterVO materialMasterVO);
	
	/**
	 * 첨부파일 정보 호출
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param RepairRequestVO materialMasterVO
	 * @return HashMap
	 * @
	 */
	List<HashMap<String, Object>> selectAttachList(MaterialMasterVO materialMasterVO);

	/**
	 * 자재관리 자재등록 insert (팝업등록)
	 * @author 김영환
	 * @since 2018.03.21
	 * @param MaterialMasterVO materialMasterVO
	 * @return int
	 * @throws Exception
	 */
	int insertMaterialAdd(MaterialMasterVO materialMasterVO);

	/**
	 * 자재관리 자재이미지 수정
	 * 
	 * @author 김영환
	 * @since 2018.03.21
	 * @param MaterialMasterVO materialMasterVO
	 * @param HttpServletResponse response
	 * @return int
	 * @throws Exception
	 */
	int updateMaterialImgModify(MaterialMasterVO materialMasterVO);
	
	//////////////////////////
	// SAP
	//////////////////////////
	
	/**
	 * SAP 자재 마스터 등록 
	 *
	 * @author 김영환
	 * @since 2018.11.14
	 * @param ZPM_MATERIAL_IMPORT zpm_material_import
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	Map<String, Object> updateMaterialInfo(ZPM_MATERIAL_EXPORT tables, ZPM_MATERIAL_IMPORT zpm_material_import) throws Exception;
	
	/**
	 * SAP 자재 그룹 등록
	 *
	 * @author 김영환 
	 * @since 2018.10.01
	 * @param ZPM_MATKL_EXPORT tables
	 * @return Map<String, Object>
	 * @throws Exception
	 */	
	Map<String, Object> updateMaterialGroupInfoAjax(ZPM_MATKL_EXPORT tables) throws Exception;
	
	//////////////////////////
	// 모바일
	//////////////////////////
	
	/**
	 * 모바일 - 자재마스터 목록 조회
	 *
	 * @author 서정민
	 * @since 2018.04.12
	 * @param MaterialMasterVO materialMasterVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectMaterialMasterList2(MaterialMasterVO materialMasterVO);
	
	/**
	 * 모바일 - 자재마스터 상세 조회
	 *
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MaterialMasterVO materialMasterVO
	 * @return HashMap
	 * @
	 */
	HashMap<String, Object> selectMaterialDtl2(MaterialMasterVO materialMasterVO);

	



	

	

	
	
	
}
