package aspn.hello.epms.material.master.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.JsonUtil;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.epms.equipment.bom.model.EquipmentBomVO;
import aspn.hello.epms.material.bom.service.MaterialBomService;
import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.epms.material.master.service.MaterialMasterService;
import aspn.com.common.util.Entity;


/**
 * [모바일 - 자재마스터] Controller Class
 * @author 서정민
 * @since 2018.05.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.05.09		서정민			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/mobile/epms/material/master")
public class MobileMaterialMasterController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	MaterialMasterService materialMasterService;

	@Autowired
	MaterialBomService materialBomService;
	
	@Autowired
	AttachService attachService;
	
	/**
	 * 자재마스터 상세 조회 
	 * 
	 * @author 서정민
	 * @since 2018.05.09
	 * @param MaterialMasterVO materialMasterVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialMasterDtl.do")
	@ResponseBody
	public Map<String, Object> materialMasterDtl(MaterialMasterVO materialMasterVO) throws Exception{
		
		Map<String, Object> resultMap = new HashMap<>();
		EquipmentBomVO equipmentBomVO = new EquipmentBomVO();
		AttachVO attachVO = new AttachVO();
		
		try {
			materialMasterVO.setSubAuth(getSsSubAuth());
			HashMap<String, Object> materialDtl = materialMasterService.selectMaterialDtl2(materialMasterVO);
			
			if(materialDtl == null){
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.NO_DATA_FOUND);
				return resultMap;
			}
			else{
				resultMap.put("MATERIAL_DTL", materialDtl);
				
				// 이미지
				if(Integer.parseInt(materialDtl.get("FILE_CNT").toString()) > 0){
					attachVO = new AttachVO();
					attachVO.setATTACH_GRP_NO(materialDtl.get("ATTACH_GRP_NO").toString());
					List<HashMap<String, Object>> attachList = attachService.selectFileList(attachVO);
					resultMap.put("ATTACH_LIST", attachList);
				}
				
				// 설비BOM
//				equipmentBomVO.setMATERIAL(materialDtl.get("MATERIAL").toString());
//				List<HashMap<String, Object>> equipmentBomList= materialBomService.selectMaterialBomList2(equipmentBomVO);
//				resultMap.put("EQUIPMENT_BOM", equipmentBomList);
				
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
				return resultMap;
			}
					
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
	
	
	/**
	 * 자재마스터 첨부파일 등록/수정 
	 * 
	 * @author 김영환
	 * @since 2018.05.09
	 * @param HttpServletRequest request
	 * @param MaterialMasterVO materialMasterVO
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateMaterialMasterAttach.do")
	@ResponseBody
	public Map<String, Object> updateMaterialMasterAttach(HttpServletRequest request, MultipartHttpServletRequest mRequest, AttachVO attachVO) throws Exception{
		
		Map<String, Object> resultMap = new HashMap<>();
		Entity entity = new Entity(request);
		
		try{
			
			// 첨부 파일 등록
			if("".equals(attachVO.getATTACH_GRP_NO()) || attachVO.getATTACH_GRP_NO() == null){
				if("Y".equals(attachVO.getAttachExistYn())){
					String attachGrpNo = attachService.selectAttachGrpNo();
					attachVO.setATTACH_GRP_NO(attachGrpNo);
				}
			}
			
			attachService.AttachEdit(request, mRequest, attachVO); 
			
			// 자재마스터 등록/수정
			MaterialMasterVO materialMasterVO = new MaterialMasterVO();
			materialMasterVO.setUserId(getSsUserId());
			materialMasterVO.setMATERIAL(entity.getString("MATERIAL"));
			materialMasterVO.setATTACH_GRP_NO(attachVO.getATTACH_GRP_NO());
			 
			materialMasterService.updateMaterialImgModify(materialMasterVO);
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
			
		}catch(Exception e){
			logger.error("MobileMaterialMasterController.updateMaterialMasterAttach > " + e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		
		return resultMap;
	}
}
