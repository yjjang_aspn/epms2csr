package aspn.hello.epms.material.input.model;

import aspn.hello.com.model.AbstractVO;

/**
 * [자재관리-입고현황] VO Class
 * @author 김영환
 * @since 2018.03.02
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.02		김영환		최초 생성
 *   
 * </pre>
 */

public class MaterialInputVO extends AbstractVO{
	private static final long serialVersionUID = 1L;
	
	/** MATERIAL INPUT DB model  */
	private String MATERIAL					= ""; //자재 ID
	private String MATERIAL_UID				= ""; //자재 코드
	private String DATE_INOUT				= ""; //입출고
	private String MATERIAL_INOUT_ITEM		= ""; //입출고내역 ID
	private String QNTY						= ""; //수량
	private String PRICE					= ""; //단가
	
	/** CATEGORY DB model  */	
	private String TREE						= ""; //구조체
	
	public String getMATERIAL() {
		return MATERIAL;
	}
	public void setMATERIAL(String mATERIAL) {
		MATERIAL = mATERIAL;
	}
	public String getMATERIAL_UID() {
		return MATERIAL_UID;
	}
	public void setMATERIAL_UID(String mATERIAL_UID) {
		MATERIAL_UID = mATERIAL_UID;
	}
	public String getDATE_INOUT() {
		return DATE_INOUT;
	}
	public void setDATE_INOUT(String dATE_INOUT) {
		DATE_INOUT = dATE_INOUT;
	}
	public String getMATERIAL_INOUT_ITEM() {
		return MATERIAL_INOUT_ITEM;
	}
	public void setMATERIAL_INOUT_ITEM(String mATERIAL_INOUT_ITEM) {
		MATERIAL_INOUT_ITEM = mATERIAL_INOUT_ITEM;
	}
	public String getQNTY() {
		return QNTY;
	}
	public void setQNTY(String qNTY) {
		QNTY = qNTY;
	}
	public String getPRICE() {
		return PRICE;
	}
	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}
	public String getTREE() {
		return TREE;
	}
	public void setTREE(String tREE) {
		TREE = tREE;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
}
