package aspn.hello.epms.material.input.service;

import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.util.Utility;
import aspn.hello.epms.material.input.mapper.MaterialInputMapper;
import aspn.hello.epms.material.input.model.MaterialInputVO;
/**
 * [자재관리-입고현황] Business Implement Class
 * 
 * @author 김영환
 * @since 2018.02.20
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.20		김영환		최초 생성
 *   
 * </pre>
 */

@Service
public class MaterialInputServiceImpl implements MaterialInputService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	MaterialInputMapper materialInputMapper;
 
	/**
	 * 자재입고 : Grid Data 조회
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialInputVO materialInputVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectMaterialInputList(MaterialInputVO materialInputVO) {
		return materialInputMapper.selectMaterialInputList(materialInputVO);
	}

	/**
	 * 입고현황 관리 : 입고관리 수정 Grid Edit
	 * 
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialInputVO materialInputVO
	 * @param List<HashMap<String, Object>> saveDataList
	 * @return String
	 */
	@Override
	public void materialInputMngEdit(List<HashMap<String, Object>> saveDataList, MaterialInputVO materialInputVO) {
		int rtnVal=0;
		int qntyBefore=0;
		int qntyAfter=0;
		int qntyDelete=0;
		
		if(saveDataList != null){
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				MaterialInputVO MaterialInputVO = Utility.toBean(tmpSaveData, materialInputVO.getClass());
				//로그인 계정 정보 담기
				MaterialInputVO.setUserId((materialInputVO.getUserId()));
				if (tmpSaveData.containsKey("Changed")) {
					//삭제 버튼 (flag : "del")
					if("del".equals(MaterialInputVO.getFlag())){
						//삭제될 입고내역 입고수량
						qntyDelete = Integer.parseInt(MaterialInputVO.getQNTY());
						//입고내역 삭제 MATERIAL_INOUT_ITEM UPDATE(삭제처리)
						rtnVal = materialInputMapper.deleteMaterialInoutList(MaterialInputVO);
						if(rtnVal >0){
							rtnVal = 0;
							//삭제될 입고내역 입고수량 세팅
							MaterialInputVO.setQNTY(Integer.toString(0-qntyDelete));
							//입고수량 차이 재고 반영
							rtnVal = materialInputMapper.updateMaterialStockIn(MaterialInputVO);
						}
					}
					//저장 버튼
					else{
						//기존 입고내역 입고수량  (기존 내역 입출고 수량 조회)
						qntyBefore = materialInputMapper.selectMaterialInoutQnty(MaterialInputVO);
						//변경 입고내역 입고수량
						qntyAfter = Integer.parseInt(MaterialInputVO.getQNTY());
						//입고내역 수정(아이템) MATERIAL_INOUT_ITEM UPDATE
						rtnVal = materialInputMapper.updateMaterialInoutList(MaterialInputVO);
						if(rtnVal > 0){
							rtnVal = 0;
							//입고수량 차이 세팅
							MaterialInputVO.setQNTY(Integer.toString(qntyAfter-qntyBefore));
							//입고수량 차이 재고 반영
							rtnVal = materialInputMapper.updateMaterialStockIn(MaterialInputVO);
						}
					}
				} else if(tmpSaveData.containsKey("Added")) {
					
				} else if(tmpSaveData.containsKey("Deleted")) {
					//삭제될 입고내역 입고수량
					qntyDelete = Integer.parseInt(MaterialInputVO.getQNTY());
					//입고내역 삭제 MATERIAL_INOUT_ITEM UPDATE(삭제처리)
					rtnVal = materialInputMapper.deleteMaterialInoutList(MaterialInputVO);
					if(rtnVal >0){
						rtnVal = 0;
						//삭제될 입고내역 입고수량 세팅
						MaterialInputVO.setQNTY(Integer.toString(0-qntyDelete));
						//입고수량 차이 재고 반영
						rtnVal = materialInputMapper.updateMaterialStockIn(MaterialInputVO);
					}
				}
			}
		}
	}

}
