package aspn.hello.epms.material.input.controller;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.Utility;
import aspn.hello.epms.material.input.model.MaterialInputVO;
import aspn.hello.epms.material.input.service.MaterialInputService;
import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.epms.material.master.service.MaterialMasterService;

/**
 * [자재관리-입고현황] Controller Class
 * @author 김영환
 * @since 2018.02.20
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.20		김영환		최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/material/input")
public class MaterialInputController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	MaterialInputService materialInputService;

	@Autowired
	MaterialMasterService materialMasterService;
	
	/**
	 * 입고현황 조회 : 메인화면
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialInputList.do")
	public String materialInputList(Model model) throws Exception{
		model.addAttribute("startDt", DateTime.requestMmDay("1","yyyy-MM-dd"));
		model.addAttribute("endDt", DateTime.getDateString());
		
		return "/epms/material/input/materialInputList";
	}
	
	/**
	 * 입고현황 조회 : 자재목록 Grid Layout
	 * 
	 * @author 김영환
	 * @since 2018.02.26
	 * @param MaterialInputVO materialInputVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialInputMasterListLayout.do")
	public String materialInputMasterListLayout(MaterialInputVO materialInputVO, Model model) throws Exception{
		return "/epms/material/input/materialInputMasterListLayout";
	}
	
	/**
	 * 입고현황 조회 : 자재목록 Grid Data
	 * 
	 * @author 김영환
	 * @since 2018.02.23
	 * @param MaterialInputVO materialInputVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialInputMasterListData.do")
	public String materialInputMasterListData(MaterialMasterVO materialMasterVO, Model model) {
		try {
			// 회원 권한 설정
			materialMasterVO.setSubAuth(getSsSubAuth());
			
			//수정가능여부(조회화면:N,관리화면:Y)
			String editYn = materialMasterVO.getEditYn();
			model.addAttribute("editYn", editYn);
			
			//자재목록(마스터SQL 공유)
			List<HashMap<String, Object>> list= materialMasterService.selectMaterialMasterList(materialMasterVO);
			model.addAttribute("list", list);
		} catch (Exception e) {
			logger.error("MaterialInputController.materialMasterListData Error !");
			e.printStackTrace();
		}
		return "/epms/material/input/materialInputMasterListData";
	}
	
	/**
	 * 입고현황 조회 : 입고현황 Grid Layout
	 * @author 김영환
	 * @since 2018.02.20
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialInputListLayout.do")
	public String materialInputListLayout() throws Exception{
		return "/epms/material/input/materialInputListLayout";
	}
	
	/**
	 * 입고현황 조회 : 입고현황 Grid Data
	 * @author 김영환
	 * @since 2018.02.20
	 * @param materialMasterMd
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialInputListData.do")
	public String materialInputListData(MaterialInputVO materialInputVO, Model model) {
		try {
			List<HashMap<String, Object>> list= materialInputService.selectMaterialInputList(materialInputVO);
			model.addAttribute("list", list);
		} catch (Exception e) {
			logger.error("MaterialInputController.materialInputListData Error!");
			e.printStackTrace();
		}
		return "/epms/material/input/materialInputListData";
	}
	
	/**
	 * 입고현황 관리 : 메인화면
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialInputMng.do")
	public String materialInputMng(Model model) throws Exception{
		model.addAttribute("startDt", DateTime.requestMmDay("1","yyyy-MM-dd"));
		model.addAttribute("endDt", DateTime.getDateString());
		
		return "/epms/material/input/materialInputMng";
	}
	
	/**
	 * 입고현황 관리 : 입고관리 Grid Layout
	 * @author 김영환
	 * @since 2018.02.20
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialInputMngLayout.do")
	public String materialInputMngLayout() throws Exception{
		return "/epms/material/input/materialInputMngLayout";
	}
	
	/**
	 * 입고현황 관리 : 입고관리 수정 Grid Edit
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialInputVO materialInputVO
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialInputMngEdit.do")
	@ResponseBody
	public String materialInputMngEdit(MaterialInputVO materialInputVO) throws Exception{
		try{
			materialInputService.materialInputMngEdit( Utility.getEditDataList(materialInputVO.getUploadData()) , materialInputVO);
			// Grid Alert
			return getSaved();
		}catch(Exception e){
			logger.error("MaterialInputController.materialInputMngEdit Error!");
			e.printStackTrace();
			// Grid Alert
			return getSaveFail();
		}
	}
}
