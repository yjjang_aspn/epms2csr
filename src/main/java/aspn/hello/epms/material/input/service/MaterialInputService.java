package aspn.hello.epms.material.input.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.material.input.model.MaterialInputVO;

/**
 * [자재관리-입고현황] Service Class
 * 
 * @author 김영환
 * @since 2018.02.20
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.21		김영환		최초 생성
 *   
 * </pre>
 */

public interface MaterialInputService {

	/**
	 * 자재입고 : Grid Data 조회
	 *
	 * @author 김영환
	 * @since 2018.02.21
	 * @param MaterialInputVO materialInputVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectMaterialInputList(MaterialInputVO materialInputVO);
	
	/**
	 * 입고현황 관리 : 입고관리 수정 Grid Edit
	 * 
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialInputVO materialInputVO
	 * @param List<HashMap<String, Object>> saveDataList
	 */
	void materialInputMngEdit(List<HashMap<String, Object>> saveDataList, MaterialInputVO materialInputVO);

}
