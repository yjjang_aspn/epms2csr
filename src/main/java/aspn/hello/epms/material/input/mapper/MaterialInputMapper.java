package aspn.hello.epms.material.input.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.material.input.model.MaterialInputVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [자재관리-입고현황] Mapper Class
 * 
 * @author 김영환
 * @since 2018.02.20
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.20		김영환		최초 생성
 *   
 * </pre>
 */

@Mapper("materialInputMapper")
public interface MaterialInputMapper {

	/**
	 * 자재마스터 목록 조회
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialInputVO materialInputVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectMaterialInputList(MaterialInputVO materialInputVO);

	/**
	 * 입고내역 삭제 MATERIAL_INOUT_ITEM UPDATE(삭제처리)
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialInputVO materialInputVO
	 * @return int
	 * @throws Exception
	 */
	int deleteMaterialInoutList(MaterialInputVO materialInputVO);

	/**
	 * 입고수량 차이 재고 반영
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialInputVO materialInputVO
	 * @return int
	 * @throws Exception
	 */
	int updateMaterialStockIn(MaterialInputVO materialInputVO);

	/**
	 * 기존 입고내역 입고수량  (기존 내역 입출고 수량 조회)
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialInputVO materialInputVO
	 * @return int
	 * @throws Exception
	 */
	int selectMaterialInoutQnty(MaterialInputVO materialInputVO);

	/**
	 * 입고내역 수정(아이템) MATERIAL_INOUT_ITEM UPDATE
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialInputVO materialInputVO
	 * @return int
	 * @throws Exception
	 */
	int updateMaterialInoutList(MaterialInputVO materialInputVO);
	
}
