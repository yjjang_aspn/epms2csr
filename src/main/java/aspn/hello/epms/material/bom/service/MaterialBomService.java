package aspn.hello.epms.material.bom.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aspn.hello.epms.equipment.bom.model.EquipmentBomVO;

/**
 * [설비BOM(자재->설비)] Service Class
 * 
 * @author 김영환
 * @since 2018.04.18
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.18		김영환			최초 생성
 *   
 * </pre>
 */

public interface MaterialBomService {
	
	//////////////////////////
	// 웹 및 공통
	//////////////////////////
	
	/**
	 * 설비BOM(자재) 목록
	 *
	 * @author 김영환
	 * @since 2018.04.18
	 * @param EquipmentBomVO equipmentBomVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectMaterialBomList(EquipmentBomVO equipmentBomVO) throws Exception;
	
	
	//////////////////////////
	// 모바일
	//////////////////////////
	
	/**
	 * 모바일 - 설비BOM(자재) 목록
	 *
	 * @author 김영환
	 * @since 2018.04.18
	 * @param EquipmentBomVO equipmentBomVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectMaterialBomList2(EquipmentBomVO equipmentBomVO) throws Exception;
}
