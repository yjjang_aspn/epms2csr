package aspn.hello.epms.material.bom.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.hello.epms.equipment.bom.model.EquipmentBomVO;
import aspn.hello.epms.material.bom.mapper.MaterialBomMapper;

/**
 * [설비BOM] Business Implement Class
 * 
 * @author 김영환
 * @since 2018.04.18
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.18		김영환			최초 생성
 *   
 * </pre>
 */

@Service
public class MaterialBomServiceImpl implements MaterialBomService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	MaterialBomMapper materialBomMapper;
	
	
	//////////////////////////
	// 웹 및 공통
	//////////////////////////
	
	/**
	 * 설비BOM(자재) 목록 조회
	 *
	 * @author 김영환
	 * @since 2018.04.18
	 * @param EquipmentBomVO equipmentBomVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectMaterialBomList(EquipmentBomVO equipmentBomVO) throws Exception {
				
		return materialBomMapper.selectMaterialBomList(equipmentBomVO);
	}
	
	
	//////////////////////////
	// 모바일
	//////////////////////////
	
	/**
	 * 모바일 - 설비BOM(자재) 목록 조회
	 *
	 * @author 서정민
	 * @since 2018.05.14
	 * @param EquipmentBomVO equipmentBomVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectMaterialBomList2(EquipmentBomVO equipmentBomVO) throws Exception {
				
		return materialBomMapper.selectMaterialBomList2(equipmentBomVO);
	}
	
}
