package aspn.hello.epms.material.bom.controller;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import aspn.com.common.util.ContSupport;
import aspn.hello.epms.equipment.bom.model.EquipmentBomVO;
import aspn.hello.epms.material.bom.service.MaterialBomService;
import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.epms.material.master.service.MaterialMasterService;

/**
 * [자재BOM] Controller Class
 * @author 김영환
 * @since 2014.04.17
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.17		김영환			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/material/bom")
public class MaterialBomController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	MaterialBomService materialBomService;

	/* INSTANCE VAR */
	@Autowired
	MaterialMasterService materialMasterService;

	/**
	 * [설비BOM(자재)조회] : 화면 호출
	 *
	 * @author 
	 * @since 2018.04.17
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialBomList.do")
	public String materialBomList() throws Exception{
		
		return "/epms/material/bom/materialBomList";
	}
	
	
	
	/**
	 * [설비BOM(자재)조회] : [자재] 그리드 레이아웃
	 * 
	 * @author 김영환
	 * @since 2018.04.17
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialListLayout.do")
	public String materialListLayout() throws Exception{
		return "/epms/material/bom/materialListLayout";
	}
	
	
	/**
	 * [설비BOM(자재)조회] : [자재] 그리드 데이터
	 * 
	 * @author 김영환
	 * @since 2018.04.17
	 * @param MaterialBomVO materialBomVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialListData.do")
	public String materialListData(MaterialMasterVO materialMasterVO, Model model) {
		try {
			//세션 아이디 조회
			materialMasterVO.setSubAuth(getSsSubAuth());
			//수정가능여부(조회화면:N,관리화면:Y)
			String editYn = materialMasterVO.getEditYn();
			model.addAttribute("editYn", editYn);
			
			//자재목록
			List<HashMap<String, Object>> list= materialMasterService.selectMaterialMasterList(materialMasterVO);
			model.addAttribute("list", list);
		} catch (Exception e) {
			logger.error("MaterialBomController.materialListData Error !");
			e.printStackTrace();
		}
		return "/epms/material/bom/materialListData";
	}
	
	/**
	 * [설비BOM(자재)조회] : [설비BOM] 그리드 레이아웃
	 *
	 * @author 김영환
	 * @since 2018.04.17
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialBomListLayout.do")
	public String materialBomListLayout() throws Exception{
		
		return "/epms/material/bom/materialBomListLayout";
	}
	
	
	/**
	 * [설비BOM(자재)조회] : [설비BOM] 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.04.17
	 * @param equipmentMasterVO
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialBomListData.do")
	public String materialBomListData(EquipmentBomVO equipmentBomVO, Model model) throws Exception{
		
		try {
			
			//설비BOM 목록
			List<HashMap<String, Object>> list= materialBomService.selectMaterialBomList(equipmentBomVO);
			model.addAttribute("list", list);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return "/epms/material/bom/materialBomListData";
	}
	
	
	
}
