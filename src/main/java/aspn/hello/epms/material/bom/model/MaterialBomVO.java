package aspn.hello.epms.material.bom.model;

import aspn.hello.com.model.AbstractVO;

import java.util.Arrays;

/**
 * [설비BOM] VO Class
 * @author 서정민
 * @since 2018.02.09
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.09		서정민			최초 생성
 *   
 * </pre>
 */

public class MaterialBomVO extends AbstractVO{
	
	private static final long serialVersionUID = 1L;
	
	/** EPMS_EQUIPMENT DB **/
	private String EQUIPMENT				= ""; // 1.설비ID
	private String LOCATION					= ""; // 2.위치ID
	private String LINE						= ""; // 3.라인ID (CATEGORY : CATEGORY_TYPE="LINE")
	private String PARENT_EQUIPMENT			= ""; // 4.부모설비ID
	private String DEPTH					= ""; // 5.레벨
	private String SEQ_DSP					= ""; // 6.출력순서
	private String ASSET_NO					= ""; // 7.자산번호
	private String COST_CENTER				= ""; // 8.코스트센터
	private String EQUIPMENT_UID			= ""; // 9.설비코드
	private String EQUIPMENT_NAME			= ""; //10.설비명
	private String GRADE					= ""; //11.설비등급
	private String MODEL					= ""; //12.모델명
	private String DIMENSION				= ""; //13.크기/치수 
	private String WEIGHT					= ""; //14.총중량
	private String WEIGHT_UNIT				= ""; //15.중량단위
	private String ACQUISITION_VALUE		= ""; //16.취득가액
	private String CURRENCY					= ""; //17.통화
	private String COUNTRY					= ""; //18.제조국
	private String MANUFACTURER				= ""; //19.제조사
	private String DATE_ACQUISITION			= ""; //20.취득일
	private String DATE_INSTALL				= ""; //21.설치일
	private String DATE_OPERATE				= ""; //22.가동일
	private String DATE_VALID				= ""; //23.유효일(효력시작일)
	private String ATTACH_GRP_NO			= ""; //24.첨부파일
	private String ATTACH_GRP_NO2			= ""; //25.QR코드
	private String DESCRIPTION				= ""; //26.메모
	private String REG_ID					= ""; //27.등록자
	private String REG_DT					= ""; //28.등록일
	private String UPT_ID					= ""; //29.수정자
	private String UPT_DT					= ""; //30.수정일
	private String DEL_YN					= ""; //31.삭제여부
	private String E_RESULT					= ""; //32.SAP-성공여부
	private String E_MESSAGE				= ""; //33.SAP-메세지
	private String SEQ_EQUIPMENT			= ""; //설비시퀀스

	/** EPMS_MATERIAL DB **/
	private String MATERIAL					= ""; // 1.자재ID
	
	
	/** EPMS_EQUIPMENT_BOM DB **/
	private String EQUIPMENT_BOM			= ""; // 1.설비BOM ID
//	private String EQUIPMENT				= ""; // 2.설비ID
//	private String MATERIAL					= ""; // 3.자재ID
//	private String REG_ID					= ""; // 4.등록자
//	private String REG_DT					= ""; // 5.등록일
//	private String UPT_ID					= ""; // 6.수정자
//	private String UPT_DT					= ""; // 7.수정일
//	private String DEL_YN					= ""; // 8.삭제여부
	private String SEQ_EQUIPMENT_BOM		= ""; //설비BOM시퀀스
	
	/** CATEGORY DB  */
	private String CATEGORY   				= ""; // 1.카테고리ID
	private String NAME   					= ""; // 2.카테고리 이름
	private String CATEGORY_TYPE			= ""; // 3.카테고리 유형
	private String PARENT_CATEGORY			= ""; // 4.부모카테고리
	private String TREE   					= ""; // 5.구조체
//	private String DEPTH	  				= ""; // 6.레벨
//	private String SEQ_DSP					= ""; // 7.출력순서
	private String SUB_ROOT					= ""; // 8.공장구분(설비관리)
//	private String REG_ID					= ""; // 9.등록자
//	private String REG_DT					= ""; //10.등록일
	private String UPD_ID					= ""; //11.수정자
	private String UPD_DT					= ""; //12.수정일
//	private String DEL_YN					= ""; //13.삭제여부
	
	/** parameter */
	
	public String getEQUIPMENT() {
		return EQUIPMENT;
	}
	public void setEQUIPMENT(String eQUIPMENT) {
		EQUIPMENT = eQUIPMENT;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getLINE() {
		return LINE;
	}
	public void setLINE(String lINE) {
		LINE = lINE;
	}
	public String getPARENT_EQUIPMENT() {
		return PARENT_EQUIPMENT;
	}
	public void setPARENT_EQUIPMENT(String pARENT_EQUIPMENT) {
		PARENT_EQUIPMENT = pARENT_EQUIPMENT;
	}
	public String getDEPTH() {
		return DEPTH;
	}
	public void setDEPTH(String dEPTH) {
		DEPTH = dEPTH;
	}
	public String getSEQ_DSP() {
		return SEQ_DSP;
	}
	public void setSEQ_DSP(String sEQ_DSP) {
		SEQ_DSP = sEQ_DSP;
	}
	public String getASSET_NO() {
		return ASSET_NO;
	}
	public void setASSET_NO(String aSSET_NO) {
		ASSET_NO = aSSET_NO;
	}
	public String getCOST_CENTER() {
		return COST_CENTER;
	}
	public void setCOST_CENTER(String cOST_CENTER) {
		COST_CENTER = cOST_CENTER;
	}
	public String getEQUIPMENT_UID() {
		return EQUIPMENT_UID;
	}
	public void setEQUIPMENT_UID(String eQUIPMENT_UID) {
		EQUIPMENT_UID = eQUIPMENT_UID;
	}
	public String getEQUIPMENT_NAME() {
		return EQUIPMENT_NAME;
	}
	public void setEQUIPMENT_NAME(String eQUIPMENT_NAME) {
		EQUIPMENT_NAME = eQUIPMENT_NAME;
	}
	public String getGRADE() {
		return GRADE;
	}
	public void setGRADE(String gRADE) {
		GRADE = gRADE;
	}
	public String getMODEL() {
		return MODEL;
	}
	public void setMODEL(String mODEL) {
		MODEL = mODEL;
	}
	public String getDIMENSION() {
		return DIMENSION;
	}
	public void setDIMENSION(String dIMENSION) {
		DIMENSION = dIMENSION;
	}
	public String getWEIGHT() {
		return WEIGHT;
	}
	public void setWEIGHT(String wEIGHT) {
		WEIGHT = wEIGHT;
	}
	public String getWEIGHT_UNIT() {
		return WEIGHT_UNIT;
	}
	public void setWEIGHT_UNIT(String wEIGHT_UNIT) {
		WEIGHT_UNIT = wEIGHT_UNIT;
	}
	public String getACQUISITION_VALUE() {
		return ACQUISITION_VALUE;
	}
	public void setACQUISITION_VALUE(String aCQUISITION_VALUE) {
		ACQUISITION_VALUE = aCQUISITION_VALUE;
	}
	public String getCURRENCY() {
		return CURRENCY;
	}
	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}
	public String getCOUNTRY() {
		return COUNTRY;
	}
	public void setCOUNTRY(String cOUNTRY) {
		COUNTRY = cOUNTRY;
	}
	public String getMANUFACTURER() {
		return MANUFACTURER;
	}
	public void setMANUFACTURER(String mANUFACTURER) {
		MANUFACTURER = mANUFACTURER;
	}
	public String getDATE_ACQUISITION() {
		return DATE_ACQUISITION;
	}
	public void setDATE_ACQUISITION(String dATE_ACQUISITION) {
		DATE_ACQUISITION = dATE_ACQUISITION;
	}
	public String getDATE_INSTALL() {
		return DATE_INSTALL;
	}
	public void setDATE_INSTALL(String dATE_INSTALL) {
		DATE_INSTALL = dATE_INSTALL;
	}
	public String getDATE_OPERATE() {
		return DATE_OPERATE;
	}
	public void setDATE_OPERATE(String dATE_OPERATE) {
		DATE_OPERATE = dATE_OPERATE;
	}
	public String getDATE_VALID() {
		return DATE_VALID;
	}
	public void setDATE_VALID(String dATE_VALID) {
		DATE_VALID = dATE_VALID;
	}
	public String getATTACH_GRP_NO() {
		return ATTACH_GRP_NO;
	}
	public void setATTACH_GRP_NO(String aTTACH_GRP_NO) {
		ATTACH_GRP_NO = aTTACH_GRP_NO;
	}
	public String getATTACH_GRP_NO2() {
		return ATTACH_GRP_NO2;
	}
	public void setATTACH_GRP_NO2(String aTTACH_GRP_NO2) {
		ATTACH_GRP_NO2 = aTTACH_GRP_NO2;
	}
	public String getDESCRIPTION() {
		return DESCRIPTION;
	}
	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
	public String getREG_ID() {
		return REG_ID;
	}
	public void setREG_ID(String rEG_ID) {
		REG_ID = rEG_ID;
	}
	public String getREG_DT() {
		return REG_DT;
	}
	public void setREG_DT(String rEG_DT) {
		REG_DT = rEG_DT;
	}
	public String getUPT_ID() {
		return UPT_ID;
	}
	public void setUPT_ID(String uPT_ID) {
		UPT_ID = uPT_ID;
	}
	public String getUPT_DT() {
		return UPT_DT;
	}
	public void setUPT_DT(String uPT_DT) {
		UPT_DT = uPT_DT;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getE_RESULT() {
		return E_RESULT;
	}
	public void setE_RESULT(String e_RESULT) {
		E_RESULT = e_RESULT;
	}
	public String getE_MESSAGE() {
		return E_MESSAGE;
	}
	public void setE_MESSAGE(String e_MESSAGE) {
		E_MESSAGE = e_MESSAGE;
	}
	public String getSEQ_EQUIPMENT() {
		return SEQ_EQUIPMENT;
	}
	public void setSEQ_EQUIPMENT(String sEQ_EQUIPMENT) {
		SEQ_EQUIPMENT = sEQ_EQUIPMENT;
	}
	public String getMATERIAL() {
		return MATERIAL;
	}
	public void setMATERIAL(String mATERIAL) {
		MATERIAL = mATERIAL;
	}
	public String getEQUIPMENT_BOM() {
		return EQUIPMENT_BOM;
	}
	public void setEQUIPMENT_BOM(String eQUIPMENT_BOM) {
		EQUIPMENT_BOM = eQUIPMENT_BOM;
	}
	public String getSEQ_EQUIPMENT_BOM() {
		return SEQ_EQUIPMENT_BOM;
	}
	public void setSEQ_EQUIPMENT_BOM(String sEQ_EQUIPMENT_BOM) {
		SEQ_EQUIPMENT_BOM = sEQ_EQUIPMENT_BOM;
	}
	public String getCATEGORY() {
		return CATEGORY;
	}
	public void setCATEGORY(String cATEGORY) {
		CATEGORY = cATEGORY;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getCATEGORY_TYPE() {
		return CATEGORY_TYPE;
	}
	public void setCATEGORY_TYPE(String cATEGORY_TYPE) {
		CATEGORY_TYPE = cATEGORY_TYPE;
	}
	public String getPARENT_CATEGORY() {
		return PARENT_CATEGORY;
	}
	public void setPARENT_CATEGORY(String pARENT_CATEGORY) {
		PARENT_CATEGORY = pARENT_CATEGORY;
	}
	public String getTREE() {
		return TREE;
	}
	public void setTREE(String tREE) {
		TREE = tREE;
	}
	public String getSUB_ROOT() {
		return SUB_ROOT;
	}
	public void setSUB_ROOT(String sUB_ROOT) {
		SUB_ROOT = sUB_ROOT;
	}
	public String getUPD_ID() {
		return UPD_ID;
	}
	public void setUPD_ID(String uPD_ID) {
		UPD_ID = uPD_ID;
	}
	public String getUPD_DT() {
		return UPD_DT;
	}
	public void setUPD_DT(String uPD_DT) {
		UPD_DT = uPD_DT;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
