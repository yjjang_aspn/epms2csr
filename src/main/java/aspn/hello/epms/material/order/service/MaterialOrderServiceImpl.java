package aspn.hello.epms.material.order.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.util.Utility;
import aspn.hello.epms.material.inout.mapper.MaterialInoutMapper;
import aspn.hello.epms.material.order.mapper.MaterialOrderMapper;
import aspn.hello.epms.material.order.model.MaterialOrderVO;
import net.sf.json.JSONArray;
/**
 * [자재관리-발주등록] Business Implement Class
 * 
 * @author 김영환
 * @since 2018.02.20
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.20		김영환		최초 생성
 *   
 * </pre>
 */

@Service
public class MaterialOrderServiceImpl implements MaterialOrderService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	MaterialOrderMapper materialOrderMapper;
	
	@Autowired
	MaterialInoutMapper materialInoutMapper;

	/**
	 * 발주등록 : 발주현황 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.02.21
	 * @param MaterialOrderVO materialOrderVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectMaterialOrderListData(MaterialOrderVO materialOrderVO) {
		return materialOrderMapper.selectMaterialOrderListData(materialOrderVO);
	}

	/**
	 * 발주등록 : 발주리스트 등록
	 *
	 * @author 김영환
	 * @since 2018.02.21
	 * @param MaterialOrderVO materialOrderVO
	 * @return HashMap<String, Object>
	 */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, Object> insertMateralOrderList(MaterialOrderVO materialOrderVO) {
		
		// 발주 공용 시퀀스 조회
		int SEQ_MATERIAL_INOUT = materialOrderMapper.selectMaterialOrderSeq();
		materialOrderVO.setMATERIAL_INOUT(String.valueOf(SEQ_MATERIAL_INOUT));
		
		// 발주 헤더 등록
		materialOrderMapper.insertMaterialOrder(materialOrderVO);
		
		// 발주 아이템 JSONArray로 변형
		List<Map<String,Object>> order_item_map = JSONArray.fromObject(StringEscapeUtils.unescapeHtml(materialOrderVO.getORDER_ITEM()));
		
		for(Map<String, Object> order_item_map_temp : order_item_map) {
			materialOrderVO.setMATERIAL(String.valueOf(order_item_map_temp.get("MATERIAL")));
			materialOrderVO.setQNTY_PLAN(String.valueOf(order_item_map_temp.get("QNTY_PLAN")));
			materialOrderVO.setPRICE(String.valueOf(order_item_map_temp.get("PRICE")));
			materialOrderVO.setMATERIAL_INOUT_TEMP(String.valueOf(order_item_map_temp.get("MATERIAL_INOUT_TEMP")));
			
			// 발주 아이템 등록
			materialOrderMapper.insertMaterialOrderItem(materialOrderVO);
			
			// 발주내역 삭제
			materialOrderMapper.deleteMaterialOrderCart(materialOrderVO);
		}
		
		return materialOrderMapper.selectMaterialOrderListData(materialOrderVO).get(0);
	}

	/**
	 * 발주등록 : 발주현황 상세조회
	 *
	 * @author 김영환
	 * @since 2018.02.21
	 * @param MaterialOrderVO materialOrderVO
	 * @return List<HashMap<String, Object>>
	 */
	@Override
	public List<HashMap<String, Object>> selectMaterialOrderDtlData(MaterialOrderVO materialOrderVO) {
		return materialOrderMapper.selectMaterialOrderDtlData(materialOrderVO);
	}

	/**
	 * 발주등록 : 입고완료 처리
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialOrderVO materialOrderVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@Override
	public int updateMaterialInoutStatus(MaterialOrderVO materialOrderVO) {
		return materialOrderMapper.updateMaterialInoutStatus(materialOrderVO);
	}

	/**
	 * 발주등록 : 입고수량 수정
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialOrderVO materialOrderVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@Override
	public void materialOrderDtlEdit(List<HashMap<String, Object>> saveDataList, MaterialOrderVO materialOrderVO) {
		
		if(saveDataList != null){
			
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				MaterialOrderVO materialOrderVO1 = Utility.toBean(tmpSaveData, materialOrderVO.getClass());
				materialOrderVO1.setUserId(materialOrderVO.getUserId());
				
				if (tmpSaveData.containsKey("Changed")) {
					
					// 기존 발주 아이템 데이터 조회
					HashMap<String, Object> materialInoutItem = materialOrderMapper.selectMaterialInoutItem(materialOrderVO1);
					materialOrderVO1.setPRE_QNTY(String.valueOf(materialInoutItem.get("QNTY")));
					
					// 임시저장 시 자재 수량 반영
					materialOrderMapper.updateMaterialMasterQnty(materialOrderVO1);
					
					// 발주 아이템 수정된 수량 반영
					materialOrderMapper.updateMaterialOrderQnty(materialOrderVO1);
					
				}
			}
		}
		
		// 입고완료일 경우 상태값 변경
		if("reg".equals(materialOrderVO.getFlag())){
			materialOrderMapper.updateMaterialInoutStatus(materialOrderVO);
		}
	}
 
	

}
