package aspn.hello.epms.material.order.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.Utility;
import aspn.hello.epms.material.order.model.MaterialOrderVO;
import aspn.hello.epms.material.order.service.MaterialOrderService;
import net.sf.json.JSONArray;

/**
 * [자재관리-입고현황] Controller Class
 * @author 김영환
 * @since 2018.02.20
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.20		김영환		최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/material/order")
public class MaterialOrderController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	MaterialOrderService materialOrderService;
	
	/**
	 * 발주등록 : 메인화면
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialOrderList.do")
	public String materialOrderList(Model model) throws Exception{
		model.addAttribute("DATE_INOUT", DateTime.getDateString());
		model.addAttribute("startDt", DateTime.requestMmDay("1","yyyy-MM-dd"));
		model.addAttribute("endDt", DateTime.getDateString());
		
		return "/epms/material/order/materialOrderList";
	}
	
	/**
	 * 발주등록 : 발주현황 그리드 레이아웃
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialOrderListLayout.do")
	public String materialOrderListLayout() throws Exception{
		
		return "/epms/material/order/materialOrderListLayout";
	}
	
	/**
	 * 발주등록 : 발주현황 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialOrderVO materialOrderVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialOrderListData.do")
	public String materialOrderListData(MaterialOrderVO materialOrderVO, Model model) throws Exception{
		try {
			materialOrderVO.setUserId(getSsUserId());
			materialOrderVO.setStartDt(materialOrderVO.getStartDt().replace("-", ""));
			materialOrderVO.setEndDt(materialOrderVO.getEndDt().replace("-", ""));
			List<HashMap<String, Object>> list= materialOrderService.selectMaterialOrderListData(materialOrderVO);
			model.addAttribute("list", list);
		} catch (Exception e) {
			logger.error("MaterialOrderController.materialOrderListData > " + e.toString());
			e.printStackTrace();
		}
		
		return "/epms/material/order/materialOrderListData";
	}
	
	/**
	 * 발주등록 : 발주리스트 등록
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialOrderVO materialOrderVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertMateralOrderList.do")
	@ResponseBody
	public HashMap<String, Object> insertMateralOrderList(MaterialOrderVO materialOrderVO) throws Exception{
		
		HashMap<String, Object> rtnMap = new HashMap<String, Object>();
		
		try {
			
			materialOrderVO.setUserId(getSsUserId());
			rtnMap = materialOrderService.insertMateralOrderList(materialOrderVO);
			
		} catch (Exception e) {
			logger.error("MaterialOrderController.materialOrderMngData > " + e.toString());
			e.printStackTrace();
		}

		return rtnMap;
	}
	
	/**
	 * 발주등록 : 발주현황 그리드 레이아웃
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popMaterialOrderDtlLayout.do")
	public String popMaterialOrderDtlLayout() throws Exception{
		
		return "/epms/material/order/popMaterialOrderDtlLayout";
	}
	
	/**
	 * 발주등록 : 발주현황 상세조회 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialOrderVO materialOrderVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popMaterialOrderDtlData.do")
	public String popMaterialOrderDtlData(MaterialOrderVO materialOrderVO, Model model) throws Exception{
		try {
			
			materialOrderVO.setCompanyId(getSsCompanyId());
			materialOrderVO.setSsLocation("5100");
			
			List<HashMap<String, Object>> list= materialOrderService.selectMaterialOrderDtlData(materialOrderVO);
			model.addAttribute("list", list);
		} catch (Exception e) {
			logger.error("MaterialOrderController.popMaterialOrderDtlData > " + e.toString());
			e.printStackTrace();
		}
		
		return "/epms/material/order/popMaterialOrderDtlData";
	}
	
	/**
	 * 발주등록 : 입고완료 처리
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialOrderVO materialOrderVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/popMaterialOrderDtlEdit.do")
	@ResponseBody
	public String popMaterialOrderDtlEdit(MaterialOrderVO materialOrderVO, Model model) throws Exception{
		try {
			materialOrderVO.setUserId(getSsUserId());
			materialOrderService.materialOrderDtlEdit( Utility.getEditDataList(materialOrderVO.getUploadData()) , materialOrderVO);
			
			return getSaved();
		} catch (Exception e) {
			logger.error("MaterialOrderController.popMaterialOrderDtlEdit > " + e.toString());
			e.printStackTrace();
			
			return getSaveFail();
		}
	}
	
}
