package aspn.hello.epms.material.order.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aspn.hello.epms.material.order.model.MaterialOrderVO;

/**
 * [자재관리-발주등록] Service Class
 * 
 * @author 김영환
 * @since 2018.02.20
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.21		김영환		최초 생성
 *   
 * </pre>
 */

public interface MaterialOrderService {

	/**
	 * 발주등록 : 발주현황 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.02.21
	 * @param MaterialOrderVO materialOrderVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectMaterialOrderListData(MaterialOrderVO materialOrderVO);

	/**
	 * 발주등록 : 발주리스트 등록
	 *
	 * @author 김영환
	 * @param string 
	 * @since 2018.02.21
	 * @param MaterialOrderVO materialOrderVO
	 * @return HashMap<String, Object>
	 */
	HashMap<String, Object> insertMateralOrderList(MaterialOrderVO materialOrderVO);

	/**
	 * 발주등록 : 발주현황 상세조회
	 *
	 * @author 김영환
	 * @since 2018.02.21
	 * @param MaterialOrderVO materialOrderVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectMaterialOrderDtlData(MaterialOrderVO materialOrderVO);

	/**
	 * 발주등록 : 입고완료 처리
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialOrderVO materialOrderVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	int updateMaterialInoutStatus(MaterialOrderVO materialOrderVO);

	/**
	 * 발주등록 : 입고 수량 변경
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialOrderVO materialOrderVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	void materialOrderDtlEdit(List<HashMap<String, Object>> editDataList, MaterialOrderVO materialOrderVO);
	
}
