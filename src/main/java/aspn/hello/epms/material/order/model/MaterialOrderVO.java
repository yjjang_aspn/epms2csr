package aspn.hello.epms.material.order.model;

import aspn.hello.com.model.AbstractVO;

/**
 * [자재관리-발주등록] VO Class
 * @author 김영환
 * @since 2018.03.02
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.03.02		김영환		최초 생성
 *   
 * </pre>
 */

public class MaterialOrderVO extends AbstractVO{
	private static final long serialVersionUID = 1L;
	
	/** MATERIAL INOUT DB model  */
	private String MATERIAL					= ""; //자재 ID
	private String MATERIAL_INOUT				= ""; //입출고ID
	private String MATERIAL_INOUT_TYPE			= ""; //입출고 구분 (05:발주)
	private String DATE_INOUT					= ""; //입출고 날짜
	private String MATERIAL_INOUT_ITEM			= ""; //입출고내역 ID
	private String SEQ_MATERIAL_INOUT_ITEM		= ""; //자재입출고내역 시퀀스
	private String QNTY						= ""; //수량
	private String QNTY_PLAN 					= ""; //계획 수량
	private String PRICE						= ""; //단가
	private String TITLE						= ""; //발주제목
	private String MATERIAL_INOUT_TEMP			= ""; //발주내역 ID
	private String MEMO						= ""; //비고 
	
	/** CATEGORY DB model  */
	private String TREE						= ""; //구조체
	
	/** Parameter */
	private String ORDER_ITEM					= ""; //발주 아이템 JSON값 수신
	private String PRE_QNTY					    = ""; //기존 아이템 수량
	
	public String getMATERIAL() {
		return MATERIAL;
	}
	public void setMATERIAL(String mATERIAL) {
		MATERIAL = mATERIAL;
	}
	public String getMATERIAL_INOUT() {
		return MATERIAL_INOUT;
	}
	public void setMATERIAL_INOUT(String mATERIAL_INOUT) {
		MATERIAL_INOUT = mATERIAL_INOUT;
	}
	public String getMATERIAL_INOUT_TYPE() {
		return MATERIAL_INOUT_TYPE;
	}
	public void setMATERIAL_INOUT_TYPE(String mATERIAL_INOUT_TYPE) {
		MATERIAL_INOUT_TYPE = mATERIAL_INOUT_TYPE;
	}
	public String getDATE_INOUT() {
		return DATE_INOUT;
	}
	public void setDATE_INOUT(String dATE_INOUT) {
		DATE_INOUT = dATE_INOUT;
	}
	public String getMATERIAL_INOUT_ITEM() {
		return MATERIAL_INOUT_ITEM;
	}
	public void setMATERIAL_INOUT_ITEM(String mATERIAL_INOUT_ITEM) {
		MATERIAL_INOUT_ITEM = mATERIAL_INOUT_ITEM;
	}
	public String getSEQ_MATERIAL_INOUT_ITEM() {
		return SEQ_MATERIAL_INOUT_ITEM;
	}
	public void setSEQ_MATERIAL_INOUT_ITEM(String sEQ_MATERIAL_INOUT_ITEM) {
		SEQ_MATERIAL_INOUT_ITEM = sEQ_MATERIAL_INOUT_ITEM;
	}
	public String getQNTY() {
		return QNTY;
	}
	public void setQNTY(String qNTY) {
		QNTY = qNTY;
	}
	public String getQNTY_PLAN() {
		return QNTY_PLAN;
	}
	public void setQNTY_PLAN(String qNTY_PLAN) {
		QNTY_PLAN = qNTY_PLAN;
	}
	public String getPRICE() {
		return PRICE;
	}
	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}
	public String getTITLE() {
		return TITLE;
	}
	public void setTITLE(String tITLE) {
		TITLE = tITLE;
	}
	public String getMATERIAL_INOUT_TEMP() {
		return MATERIAL_INOUT_TEMP;
	}
	public void setMATERIAL_INOUT_TEMP(String mATERIAL_INOUT_TEMP) {
		MATERIAL_INOUT_TEMP = mATERIAL_INOUT_TEMP;
	}
	public String getMEMO() {
		return MEMO;
	}
	public void setMEMO(String mEMO) {
		MEMO = mEMO;
	}
	public String getTREE() {
		return TREE;
	}
	public void setTREE(String tREE) {
		TREE = tREE;
	}
	public String getORDER_ITEM() {
		return ORDER_ITEM;
	}
	public void setORDER_ITEM(String oRDER_ITEM) {
		ORDER_ITEM = oRDER_ITEM;
	}
	public String getPRE_QNTY() {
		return PRE_QNTY;
	}
	public void setPRE_QNTY(String pRE_QNTY) {
		PRE_QNTY = pRE_QNTY;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
