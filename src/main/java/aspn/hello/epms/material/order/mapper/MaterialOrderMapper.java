package aspn.hello.epms.material.order.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.material.order.model.MaterialOrderVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [자재관리-입고현황] Mapper Class
 * 
 * @author 김영환
 * @since 2018.02.20
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.20		김영환		최초 생성
 *   
 * </pre>
 */

@Mapper("materialOrderMapper")
public interface MaterialOrderMapper {

	/**
	 * 발주등록 : 발주현황 그리드 데이터
	 *
	 * @author 김영환
	 * @since 2018.02.21
	 * @param MaterialOrderVO materialOrderVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectMaterialOrderListData(MaterialOrderVO materialOrderVO);

	/**
	 * 발주 헤더 PK 조회
	 *
	 * @author 김영환
	 * @since 2018.02.21
	 * @param 
	 * @return 
	 */
	int selectMaterialOrderSeq();

	/**
	 * 발주 헤더 등록
	 *
	 * @author 김영환
	 * @since 2018.02.21
	 * @param MaterialOrderVO materialOrderVO
	 * @return 
	 */
	void insertMaterialOrder(MaterialOrderVO materialOrderVO);

	/**
	 * 발주 아이템 등록
	 *
	 * @author 김영환
	 * @since 2018.02.21
	 * @param MaterialOrderVO materialOrderVO
	 * @return 
	 */
	void insertMaterialOrderItem(MaterialOrderVO materialOrderVO);

	/**
	 * 발주 임시내역 삭제
	 *
	 * @author 김영환
	 * @since 2018.02.21
	 * @param MaterialOrderVO materialOrderVO
	 * @return 
	 */
	void deleteMaterialOrderCart(MaterialOrderVO materialOrderVO);

	/**
	 * 발주등록 : 발주현황 상세조회
	 *
	 * @author 김영환
	 * @since 2018.02.21
	 * @param MaterialOrderVO materialOrderVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectMaterialOrderDtlData(MaterialOrderVO materialOrderVO);

	/**
	 * 발주등록 : 입고완료 처리
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialOrderVO materialOrderVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	int updateMaterialInoutStatus(MaterialOrderVO materialOrderVO);

	/**
	 * 발주등록 : 입고수량 수정
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialOrderVO materialOrderVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	void updateMaterialOrderQnty(MaterialOrderVO materialOrderVO1);

	/**
	 * 발주등록 : 임시저장 시 자재수량 반영
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialOrderVO materialOrderVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	void updateMaterialMasterQnty(MaterialOrderVO materialOrderVO1);

	/**
	 * 발주등록 : 기존 발주 아이템 데이터 조회
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialOrderVO materialOrderVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	HashMap<String, Object> selectMaterialInoutItem(MaterialOrderVO materialOrderVO1);

}
