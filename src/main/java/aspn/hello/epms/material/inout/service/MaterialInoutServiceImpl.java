package aspn.hello.epms.material.inout.service;

import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.util.Utility;
import aspn.hello.epms.material.inout.mapper.MaterialInoutMapper;
import aspn.hello.epms.material.inout.model.MaterialInoutVO;
/**
 * [자재관리-입출고 등록] Business Implement Class
 * 
 * @author 김영환
 * @since 2018.02.26
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.26		김영환		최초 생성
 *   
 * </pre>
 */

@Service
public class MaterialInoutServiceImpl implements MaterialInoutService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	MaterialInoutMapper materialInoutMapper;

	/**
	 * 자재입출고 : Grid Data
	 *
	 * @author jychoi
	 * @since 2018.02.26
	 * @param materialInoutVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectMaterialInoutMngData(MaterialInoutVO materialInoutVO) {
		return materialInoutMapper.selectMaterialInoutMngData(materialInoutVO);
	}

	/**
	 * 자재마스터 : 담기버튼 Grid Edit
	 *
	 * @author jychoi
	 * @since 2018.02.28
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param MaterialInoutVO materialInoutVO
	 * @throws Exception
	 */
	@Override
	public void materialInoutMasterEdit(List<HashMap<String, Object>> saveDataList, MaterialInoutVO materialInoutVO) {
		int rtnVal=0;
		try{
			if(saveDataList != null){
				for(HashMap<String, Object> tmpSaveData:saveDataList) {
					MaterialInoutVO MaterialInoutVO = Utility.toBean(tmpSaveData, materialInoutVO.getClass());
					//로그인 계정 정보 담기
					MaterialInoutVO.setUserId(materialInoutVO.getUserId());
					MaterialInoutVO.setMATERIAL_INOUT_TYPE(materialInoutVO.getMATERIAL_INOUT_TYPE());
					
					if (tmpSaveData.containsKey("Changed")) {
						//입출고 목록에 이미 담겨있는 자재인지 체크
						//입출고 목록에 아직 담겨있지 않은 자재라면 INSERT
						if(materialInoutMapper.selectMaterialInout(MaterialInoutVO) < 1){
							//자재 입출고목록 추가 MATERIAL_INOUT_TEMP INSERT
							rtnVal = materialInoutMapper.insertMaterialInoutTemp(MaterialInoutVO);
						}
					} else if(tmpSaveData.containsKey("Added")) {

					} else if(tmpSaveData.containsKey("Deleted")) {
							
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			rtnVal = 0;
		}
	}

	/**
	 * 자재입출고 : 등록/삭제 Grid Edit
	 *
	 * @author jychoi
	 * @since 2018.02.28
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param MaterialInoutVO materialInoutVO
	 * @throws Exception
	 */
	@Override
	public void materialInoutMngEdit(List<HashMap<String, Object>> saveDataList, MaterialInoutVO materialInoutVO) {
		int rtnVal=0;
		int cnt=0;
		
		// 전채 갯수
		if(saveDataList != null){
			for(HashMap<String, Object> tmpSaveData2:saveDataList) {
				MaterialInoutVO MaterialInoutVO2 = Utility.toBean(tmpSaveData2, materialInoutVO.getClass());
				if(!("".equals(MaterialInoutVO2.getQNTY()) || "0".equals(MaterialInoutVO2.getQNTY()))){
					cnt ++;
				}
			}
		}
		
		if(cnt>0){
			//등록 버튼
			if("1".equals(materialInoutVO.getFlag())){
				//입출고 SEQ 체크(SEQ_MATERIAL_INOUT 체크)
				int SEQ_MATERIAL_INOUT = materialInoutMapper.selectMaterialInoutSeq(materialInoutVO);			
				materialInoutVO.setMATERIAL_INOUT(Integer.toString(SEQ_MATERIAL_INOUT));
				
				//입출고 등록(헤더) MATERIAL_INOUT INSERT
				rtnVal = materialInoutMapper.insertMaterialInout(materialInoutVO);
				
				if(rtnVal > 0){
					rtnVal = 0;
					for(HashMap<String, Object> tmpSaveData:saveDataList) {
						
						MaterialInoutVO MaterialInoutVO = Utility.toBean(tmpSaveData, materialInoutVO.getClass());
						
						if(!("".equals(MaterialInoutVO.getQNTY()) || Integer.parseInt(MaterialInoutVO.getQNTY()) <= 0)){
							//로그인 계정 정보 담기
							MaterialInoutVO.setUserId(materialInoutVO.getUserId());
							//입출고 헤더 PK (MATERIAL_INOUT) 담기
							MaterialInoutVO.setMATERIAL_INOUT(materialInoutVO.getMATERIAL_INOUT());
							//입출고 날짜 담기
							MaterialInoutVO.setDATE_INOUT(materialInoutVO.getDATE_INOUT());
							
							//금액이 null일경우 0으로 세팅
							if(("".equals(MaterialInoutVO.getPRICE()))){
								MaterialInoutVO.setPRICE("0");
							}
							
							if (tmpSaveData.containsKey("Changed")) {
								//입출고 등록(아이템) MATERIAL_INOUT_ITEM INSERT
								rtnVal = materialInoutMapper.insertMaterialInoutItem(MaterialInoutVO);
								if(rtnVal > 0){
									rtnVal = 0;
									//입고 또는 임의입고 경우, 자재 재고수량 반영
									if("01".equals(materialInoutVO.getMATERIAL_INOUT_TYPE()) || "03".equals(materialInoutVO.getMATERIAL_INOUT_TYPE())){
										//자재 재고수량 반영(입고,임의입고) MATERIAL UPDATE
										rtnVal = materialInoutMapper.updateMaterialInoutIn(MaterialInoutVO);
									}
									//임의출고의 경우, 자재 재고수량 반영
									else if("04".equals(materialInoutVO.getMATERIAL_INOUT_TYPE())){
										//자재 재고수량 반영(임의출고) MATERIAL UPDATE
										rtnVal = materialInoutMapper.updateMaterialInoutOut(MaterialInoutVO);
									}else{
										
									}
									if(rtnVal > 0){
										rtnVal = 0;
										//입출고 등록 후 입출고목록의 자재항목 삭제
										rtnVal = materialInoutMapper.deleteMaterialInoutTemp(MaterialInoutVO);
									}
								}
							} else if(tmpSaveData.containsKey("Added")) {
								
							} else if(tmpSaveData.containsKey("Deleted")) {
								
							}
						}
					}
				} 
			}
		}
		//삭제 버튼
		else if("2".equals(materialInoutVO.getFlag())){
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				MaterialInoutVO MaterialInoutVO = Utility.toBean(tmpSaveData, materialInoutVO.getClass());
				//로그인 계정 정보 담기
				MaterialInoutVO.setUserId(materialInoutVO.getUserId());
				if (tmpSaveData.containsKey("Changed")) {
					//삭제 버튼(flag : "del")
					if("del".equals(MaterialInoutVO.getFlag())){
						//입출고 목록에 있는 자재 삭제 MATERIAL_INOUT_TEMP UPDATE(삭제처리)
						rtnVal = materialInoutMapper.deleteMaterialInoutTemp(MaterialInoutVO);
					}
				} else if(tmpSaveData.containsKey("Added")) {

				} else if(tmpSaveData.containsKey("Deleted")) {
					//입출고 목록에 있는 자재 삭제 MATERIAL_INOUT_TEMP UPDATE(삭제처리)
					rtnVal = materialInoutMapper.deleteMaterialInoutTemp(MaterialInoutVO);	
				}
			}
		}
		else{
			
		}	
		
	}

}
