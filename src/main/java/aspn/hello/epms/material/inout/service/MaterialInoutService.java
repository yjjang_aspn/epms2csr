package aspn.hello.epms.material.inout.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.material.inout.model.MaterialInoutVO;

/**
 * [자재관리-입출고 등록] Service Class
 * 
 * @author 김영환
 * @since 2018.02.26
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.26		김영환		최초 생성
 *   
 * </pre>
 */

public interface MaterialInoutService {

	/**
	 * 자재입출고 : Grid Data
	 *
	 * @author 김영환
	 * @since 2018.02.26
	 * @param materialMasterVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectMaterialInoutMngData(MaterialInoutVO materialInoutVO);
	
	/**
	 * 자재입출고 : 담기버튼 Grid Edit
	 *
	 * @author 김영환
	 * @since 2018.02.28
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param MaterialInoutVO materialInoutVO
	 * @return
	 */
	void materialInoutMasterEdit(List<HashMap<String, Object>> saveDataList, MaterialInoutVO materialInoutVO);

	/**
	 * 자재입출고 : 등록/삭제버튼
	 *
	 * @author 김영환
	 * @since 2018.02.28
	 * @param List<HashMap<String, Object>> saveDataList
	 * @param MaterialInoutVO materialInoutVO
	 * @return
	 */
	void materialInoutMngEdit(List<HashMap<String, Object>> editDataList, MaterialInoutVO materialInoutVO);

}
