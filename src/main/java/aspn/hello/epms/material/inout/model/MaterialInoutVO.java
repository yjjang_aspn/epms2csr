package aspn.hello.epms.material.inout.model;

import aspn.hello.com.model.AbstractVO;

/**
 * [자재관리-입출고 등록] Mapper Class
 * 
 * @author 김영환
 * @since 2018.02.20
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.20		김영환		최초 생성
 *   
 * </pre>
 */

public class MaterialInoutVO extends AbstractVO{
	private static final long serialVersionUID = 1L;
	
	/** MATERIAL INOUT DB Parameter model  */
	private String MATERIAL						= ""; //자재 ID
	private String MATERIAL_INOUT					= ""; //입출고 ID
	private String MATERIAL_INOUT_TYPE				= ""; //입출고타입(01:입고/발주,02:출고,03:임의입고,04:입의출고)
	private String DATE_INOUT						= ""; //입출고
	private String QNTY							= ""; //수량
	private String PRICE							= ""; //단가
	private String SEQ_MATERIAL_INOUT_ITEM			= ""; //자재입출고내역 시퀀스
	private String MATERIAL_INOUT_TEMP				= ""; //자재담기 ID
	private String SEQ_MATERIAL_INOUT_TEMP			= ""; //자재담기 시퀀스
	private String TITLE							= ""; //발주제목

	/** CATEGORY DB model  */
	private String TREE						= ""; //구조체
	
	public String getMATERIAL() {
		return MATERIAL;
	}
	public void setMATERIAL(String mATERIAL) {
		MATERIAL = mATERIAL;
	}
	public String getMATERIAL_INOUT() {
		return MATERIAL_INOUT;
	}
	public void setMATERIAL_INOUT(String mATERIAL_INOUT) {
		MATERIAL_INOUT = mATERIAL_INOUT;
	}
	public String getMATERIAL_INOUT_TYPE() {
		return MATERIAL_INOUT_TYPE;
	}
	public void setMATERIAL_INOUT_TYPE(String mATERIAL_INOUT_TYPE) {
		MATERIAL_INOUT_TYPE = mATERIAL_INOUT_TYPE;
	}
	public String getDATE_INOUT() {
		return DATE_INOUT;
	}
	public void setDATE_INOUT(String dATE_INOUT) {
		DATE_INOUT = dATE_INOUT;
	}
	public String getQNTY() {
		return QNTY;
	}
	public void setQNTY(String qNTY) {
		QNTY = qNTY;
	}
	public String getPRICE() {
		return PRICE;
	}
	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}
	public String getSEQ_MATERIAL_INOUT_ITEM() {
		return SEQ_MATERIAL_INOUT_ITEM;
	}
	public void setSEQ_MATERIAL_INOUT_ITEM(String sEQ_MATERIAL_INOUT_ITEM) {
		SEQ_MATERIAL_INOUT_ITEM = sEQ_MATERIAL_INOUT_ITEM;
	}
	public String getMATERIAL_INOUT_TEMP() {
		return MATERIAL_INOUT_TEMP;
	}
	public void setMATERIAL_INOUT_TEMP(String mATERIAL_INOUT_TEMP) {
		MATERIAL_INOUT_TEMP = mATERIAL_INOUT_TEMP;
	}
	public String getSEQ_MATERIAL_INOUT_TEMP() {
		return SEQ_MATERIAL_INOUT_TEMP;
	}
	public void setSEQ_MATERIAL_INOUT_TEMP(String sEQ_MATERIAL_INOUT_TEMP) {
		SEQ_MATERIAL_INOUT_TEMP = sEQ_MATERIAL_INOUT_TEMP;
	}
	public String getTREE() {
		return TREE;
	}
	public void setTREE(String tREE) {
		TREE = tREE;
	}
	public String getTITLE() {
		return TITLE;
	}
	public void setTITLE(String tITLE) {
		TITLE = tITLE;
	}
	

}
