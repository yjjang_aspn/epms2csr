package aspn.hello.epms.material.inout.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.material.inout.model.MaterialInoutVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [자재관리-입출고 등록] Mapper Class
 * 
 * @author 김영환
 * @since 2018.02.20
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.26		김영환		최초 생성
 *   
 * </pre>
 */

@Mapper("materialInoutMapper")
public interface MaterialInoutMapper {

	/**
	 * 자재입출고 : Grid Data
	 *
	 * @author 김영환
	 * @since 2018.02.26
	 * @param materialInoutVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectMaterialInoutMngData(MaterialInoutVO materialInoutVO);

	/**
	 * 입출고 목록에 이미 담겨있는 자재인지 체크
	 *
	 * @author 김영환
	 * @since 2018.02.26
	 * @param materialInoutVO
	 * @return int
	 */
	int selectMaterialInout(MaterialInoutVO materialInoutVO);

	/**
	 * 자재 입출고목록 추가 MATERIAL_INOUT_TEMP INSERT
	 *
	 * @author 김영환
	 * @since 2018.02.26
	 * @param materialInoutVO
	 * @return int
	 */
	int insertMaterialInoutTemp(MaterialInoutVO materialInoutVO);

	/**
	 * 입출고 헤더 PK 조회
	 *
	 * @author 김영환
	 * @since 2018.02.26
	 * @param materialInoutVO
	 * @return int
	 */
	int selectMaterialInoutSeq(MaterialInoutVO materialInoutVO);

	/**
	 * 입출고 헤더 추가
	 *
	 * @author 김영환
	 * @since 2018.02.26
	 * @param materialInoutVO
	 * @return int
	 */
	int insertMaterialInout(MaterialInoutVO materialInoutVO);

	/**
	 * 입출고 아이템 추가
	 *
	 * @author 김영환
	 * @since 2018.02.26
	 * @param materialInoutVO
	 * @return int
	 */
	int insertMaterialInoutItem(MaterialInoutVO materialInoutVO);

	/**
	 * 자재 재고수량 반영(입고/임의입고)
	 *
	 * @author 김영환
	 * @since 2018.02.26
	 * @param materialInoutVO
	 * @return int
	 */
	int updateMaterialInoutIn(MaterialInoutVO materialInoutVO);

	/**
	 * 자재 재고수량 반영(출고/임의출고)
	 *
	 * @author 김영환
	 * @since 2018.02.26
	 * @param materialInoutVO
	 * @return int
	 */
	int updateMaterialInoutOut(MaterialInoutVO materialInoutVO);

	/**
	 * 입출고 목록의 자재 삭제처리
	 *
	 * @author 김영환
	 * @since 2018.02.26
	 * @param materialInoutVO
	 * @return int
	 */
	int deleteMaterialInoutTemp(MaterialInoutVO materialInoutVO);
	
}
