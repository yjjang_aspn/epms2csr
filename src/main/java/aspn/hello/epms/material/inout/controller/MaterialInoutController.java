package aspn.hello.epms.material.inout.controller;

import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.Utility;
import aspn.hello.epms.material.inout.model.MaterialInoutVO;
import aspn.hello.epms.material.inout.service.MaterialInoutService;
import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.epms.material.master.service.MaterialMasterService;

/**
 * [자재관리-입출고 등록] Controller Class
 * @author 김영환
 * @since 2018.02.28
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.28		김영환		최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/material/inout")
public class MaterialInoutController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	MaterialInoutService materialInoutService;

	@Autowired
	MaterialMasterService materialMasterService;
	
	/**
	 * 입출고 등록 : 메인화면
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialInoutMng.do")
	public String materialInoutMng(Model model) throws Exception{
		model.addAttribute("DATE_INOUT", DateTime.getDateString());
		return "/epms/material/inout/materialInoutMng";
	}
	
	/**
	 * 입출고 등록 : 자재목록 Grid Layout
	 * @author 김영환
	 * @since 2018.02.28
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "materialInoutMasterLayout.do")
	public String materialInoutMasterLayout() throws Exception{
		return "/epms/material/inout/materialInoutMasterLayout";
	}
	
	/**
	 * 입출고 등록 : 자재목록 Grid Data
	 * @author 김영환
	 * @since 2018.02.28
	 * @param MaterialMasterVO materialMasterVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "materialInoutMasterData.do")
	public String materialInoutMasterData(MaterialMasterVO materialMasterVO, Model model) {
		try {
			//세션 아이디 조회
			materialMasterVO.setSubAuth(getSsSubAuth());
			//수정가능여부(조회화면:N,관리화면:Y)
			String editYn = materialMasterVO.getEditYn();
			model.addAttribute("editYn", editYn);
			
			//자재목록(MASTER 공유)
			List<HashMap<String, Object>> list= materialMasterService.selectMaterialMasterList(materialMasterVO);
			model.addAttribute("list", list);
		} catch (Exception e) {
			logger.error("MaterialInoutController.materialMasterListData Error !");
			e.printStackTrace();
		}
		return "/epms/material/inout/materialInoutMasterData";
	}
	
	/**
	 * 입출고 등록 : 등록 Grid Layout
	 * @author 김영환
	 * @since 2018.02.28
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "materialInoutMngLayout.do")
	public String materialInoutMngLayout() throws Exception{
		return "/epms/material/inout/materialInoutMngLayout";
	}
	
	/**
	 * 입출고 등록 : 등록 Grid Data
	 * @author 김영환
	 * @since 2018.02.28
	 * @param MaterialInoutVO materialMasterVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "materialInoutMngData.do")
	public String materialInoutMngData(MaterialInoutVO materialInoutVO, Model model) {
		try {
			materialInoutVO.setUserId(getSsUserId());
			List<HashMap<String, Object>> list= materialInoutService.selectMaterialInoutMngData(materialInoutVO);
			model.addAttribute("list", list);
		} catch (Exception e) {
			logger.error("MaterialInoutController.materialInoutMngData Error!");
			e.printStackTrace();
		}
		return "/epms/material/inout/materialInoutMngData";
	}
	
	/**
	 * 입출고 등록 : 자재목록 담기버튼 Grid Edit
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialInoutVO materialInoutVO
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialInoutMasterEdit.do")
	@ResponseBody
	public String materialInoutMasterEdit(MaterialInoutVO materialInoutVO) {
		try{
			materialInoutVO.setUserId(getSsUserId());
			materialInoutService.materialInoutMasterEdit( Utility.getEditDataList(materialInoutVO.getUploadData()) , materialInoutVO);
			// Grid Alert
			return getSaved();
		}catch(Exception e){
			logger.error("MaterialInoutController.materialInoutMasterEdit Error!");
			e.printStackTrace();
			// Grid Alert
			return getSaveFail();
		}
	}
	
	/**
	 * 입출고 등록 : 등록목록 등록/삭제버튼
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialInoutVO materialInoutVO
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialInoutMngEdit.do")
	@ResponseBody
	public String materialInoutMngEdit(MaterialInoutVO materialInoutVO) {
		try{
			materialInoutVO.setUserId(getSsUserId());
			materialInoutService.materialInoutMngEdit( Utility.getEditDataList(materialInoutVO.getUploadData()) , materialInoutVO);
			// Grid Alert
			return getSaved();
		}catch(Exception e){
			logger.error("MaterialInoutController.materialInoutMngEdit Error!");
			e.printStackTrace();
			// Grid Alert
			return getSaveFail();
		}
	}
}
