package aspn.hello.epms.material.output.service;

import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aspn.com.common.util.Utility;
import aspn.hello.epms.material.master.mapper.MaterialMasterMapper;
import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.epms.material.output.mapper.MaterialOutputMapper;
import aspn.hello.epms.material.output.model.MaterialOutputVO;
/**
 * [자재관리-출고현황] Business Implement Class
 * 
 * @author 김영환
 * @since 2018.02.26
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.26		김영환		최초 생성
 *   2018.06.11		김영환		84-161Line 수정(DATE_ALERT 기능 추가)
 *   
 * </pre>
 */

@Service
public class MaterialOutputServiceImpl implements MaterialOutputService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	MaterialOutputMapper materialOutputMapper;
	
	@Autowired
	MaterialMasterMapper materialMasterMapper;

	/**
	 * 자재출고 : 출고정보조회
	 *
	 * @author 김영환
	 * @since 2018.02.26
	 * @param MaterialInputVO materialMasterVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> selectMaterialOutputList(MaterialOutputVO materialOutputVO) {
		return materialOutputMapper.selectMaterialOutputList(materialOutputVO);
	}

	/**
	 * 자재출고 : 그리드 데이터 수정
	 *
	 * @author 김영환
	 * @since 2018.02.26
	 * @param MaterialInputVO materialMasterVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	@Override
	public void materialOutputListEdit(List<HashMap<String, Object>> saveDataList, MaterialOutputVO materialOutputVO) {
		int rtnVal=0;
		int qntyBefore=0;
		int qntyAfter=0;
		int qntyDelete=0;
		
		if(saveDataList != null){
			
			for(HashMap<String, Object> tmpSaveData:saveDataList) {
				
				MaterialOutputVO MaterialOutputVO = Utility.toBean(tmpSaveData, materialOutputVO.getClass());
				
				//로그인 계정 정보 담기
				MaterialOutputVO.setUserId(MaterialOutputVO.getUserId());
				
				//반영 전 현재 안전재고/재고 수량 조회
				MaterialMasterVO materialMasterVO = new MaterialMasterVO();
				materialMasterVO.setMATERIAL(MaterialOutputVO.getMATERIAL());
				
				HashMap<String, Object> before_stockMap = materialMasterMapper.selectMaterialMasterStock(materialMasterVO);
				int before_stock = Integer.parseInt(String.valueOf(before_stockMap.get("STOCK")));
				int before_stock_optimal = Integer.parseInt(String.valueOf(before_stockMap.get("STOCK_OPTIMAL")));
				
				String stock_flag = "N";	// 기존 재고보다 안전재고 미만일 경우 (STOCK_FLAG:'N')
				if(before_stock >= before_stock_optimal){
					stock_flag = "Y";		// 기존 재고가 안전재고 이상일 경우 (STOCK_FLAG:'Y')
				}
				
				if (tmpSaveData.containsKey("Changed")) {
					
					//삭제 버튼 (flag : "del")
					if("del".equals(MaterialOutputVO.getFlag())){
						//삭제될 출고내역 출고수량
						qntyDelete = Integer.parseInt(MaterialOutputVO.getQNTY());
						
						//출고내역 삭제 MATERIAL_INOUT_ITEM UPDATE(삭제처리)
						rtnVal = materialOutputMapper.deleteMaterialInoutList(MaterialOutputVO);
						
						if(rtnVal >0){
							
							rtnVal = 0;
							//삭제될 출고내역 출고수량 세팅
							MaterialOutputVO.setQNTY(Integer.toString(0-qntyDelete));
							//출고수량 차이 재고 반영
							rtnVal = materialOutputMapper.updateMaterialStockOut(MaterialOutputVO);
						}
					}
					//저장 버튼
					else{
						//기존 출고내역 출고수량  (기존 내역 입출고 수량 조회)
						qntyBefore = materialOutputMapper.selectMaterialInoutQnty(MaterialOutputVO);
						//변경 출고내역 출고수량
						qntyAfter = Integer.parseInt(MaterialOutputVO.getQNTY());
						
						//출고내역 수정(아이템) MATERIAL_INOUT_ITEM UPDATE
						rtnVal = materialOutputMapper.updateMaterialInoutList(MaterialOutputVO);
						
						if(rtnVal > 0){
							
							rtnVal = 0;
							//출고수량 차이 세팅
							MaterialOutputVO.setQNTY(Integer.toString(qntyAfter-qntyBefore));
							//출고수량 차이 재고 반영
							rtnVal = materialOutputMapper.updateMaterialStockOut(MaterialOutputVO);
						}
					}
				} else if(tmpSaveData.containsKey("Added")) {
					
					
				} else if(tmpSaveData.containsKey("Deleted")) {
					
					//삭제될 출고내역 출고수량
					qntyDelete = Integer.parseInt(MaterialOutputVO.getQNTY());
					
					//출고내역 삭제 MATERIAL_INOUT_ITEM UPDATE(삭제처리)
					rtnVal = materialOutputMapper.deleteMaterialInoutList(MaterialOutputVO);
					
					if(rtnVal >0){
						rtnVal = 0;
						//삭제될 출고내역 출고수량 세팅
						MaterialOutputVO.setQNTY(Integer.toString(0-qntyDelete));
						//출고수량 차이 재고 반영
						rtnVal = materialOutputMapper.updateMaterialStockOut(MaterialOutputVO);
						
					}
				}
				
				// 기존 재고가 안전재고 이상이었는데, 반영 후 안전재고 미만이 될 경우 DATE_ALERT 등록
				HashMap<String, Object> after_stockMap = materialMasterMapper.selectMaterialMasterStock(materialMasterVO);
				int after_stock = Integer.parseInt(String.valueOf(after_stockMap.get("STOCK")));
				int after_stock_optimal = Integer.parseInt(String.valueOf(after_stockMap.get("STOCK_OPTIMAL")));
				
				if(stock_flag == "Y" && after_stock_optimal > after_stock){
					materialMasterMapper.updateMaterialMasterDateAlert(materialMasterVO);
				}
			}
		}
	}

}
