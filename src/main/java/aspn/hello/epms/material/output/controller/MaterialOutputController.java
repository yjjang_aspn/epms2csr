package aspn.hello.epms.material.output.controller;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.util.ContSupport;
import aspn.com.common.util.DateTime;
import aspn.com.common.util.Utility;
import aspn.hello.epms.material.master.model.MaterialMasterVO;
import aspn.hello.epms.material.master.service.MaterialMasterService;
import aspn.hello.epms.material.output.model.MaterialOutputVO;
import aspn.hello.epms.material.output.service.MaterialOutputService;

/**
 * [자재관리-출고현황] Controller Class
 * @author 김영환
 * @since 2018.02.26
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.26		김영환		최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/material/output")
public class MaterialOutputController extends ContSupport {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	MaterialOutputService materialOutputService;

	@Autowired
	MaterialMasterService materialMasterService;
	
	/**
	 * 출고현황 조회 : 메인화면
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param request, model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialOutputList.do")
	public String materialInputList(MaterialOutputVO MaterialOutputVO, Model model) throws Exception{
		model.addAttribute("startDt", DateTime.requestMmDay("1","yyyy-MM-dd"));
		model.addAttribute("endDt", DateTime.getDateString());
		
		return "/epms/material/output/materialOutputList";
	}
	
	/**
	 * 출고현황 조회/관리 : 자재목록 Grid Layout
	 * @author 김영환
	 * @since 2018.02.26
	 * @param MaterialInputVO materialInputVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialOutputMasterListLayout.do")
	public String materialOutputMasterListLayout() throws Exception{
		return "/epms/material/output/materialOutputMasterListLayout";
	}
	
	/**
	 * 출고현황 조회/관리 : 자재목록 Grid Data
	 * @author 김영환
	 * @since 2018.02.23
	 * @param MaterialInputVO materialInputVO
	 * @param Model model
	 * @return list
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialOutputMasterListData.do")
	public String materialOutputMasterListData(MaterialMasterVO materialMasterVO, Model model) {
		try {
			// 회원 권한 설정
			materialMasterVO.setSubAuth(getSsSubAuth());
			
			//수정가능여부(조회화면:N,관리화면:Y)
			String editYn = materialMasterVO.getEditYn();
			model.addAttribute("editYn", editYn);
			
			//자재목록(MASTER 공유)
			List<HashMap<String, Object>> list= materialMasterService.selectMaterialMasterList(materialMasterVO);
			model.addAttribute("list", list);
		} catch (Exception e) {
			logger.error("MaterialOutputController.materialOutputMasterListData Error !");
			e.printStackTrace();
		}
		return "/epms/material/output/materialOutputMasterListData";
	}
	
	/**
	 * 출고현황 조회 : 출고목록 Grid Data
	 * @author 김영환
	 * @since 2018.02.26
	 * @param MaterialOutputVO materialOutputVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialOutputListData.do")
	public String materialOutputListData(MaterialOutputVO materialOutputVO, Model model) {
		try {
			List<HashMap<String, Object>> list= materialOutputService.selectMaterialOutputList(materialOutputVO);
			model.addAttribute("list", list);
		} catch (Exception e) {
			logger.error("MaterialOutputController.materialOutputListData Error !");
			e.printStackTrace();
		}
		return "/epms/material/output/materialOutputListData";
	}
	
	/**
	 * 출고현황 조회 : 출고목록 Grid Layout
	 * @author 김영환
	 * @since 2018.03.05
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialOutputListLayout.do")
	public String materialOutputListLayout() throws Exception{
		return "/epms/material/output/materialOutputListLayout";
	}
	
	/**
	 * 출고현황 조회 : 출고목록 Grid Edit
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialInputVO materialInputVO
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialOutputListEdit.do")
	@ResponseBody
	public String materialOutputListEdit(MaterialOutputVO materialOutputVO, Model model) {
		try{
			materialOutputService.materialOutputListEdit( Utility.getEditDataList(materialOutputVO.getUploadData()) , materialOutputVO);
			// Grid Alert
			return getSaved();
		}catch(Exception e){
			logger.error("MaterialOutputController.materialOutputListEdit Error !");
			e.printStackTrace();
			// Grid Alert
			return getSaveFail();
		}
	}
	
	/**
	 * 출고현황 관리 : 메인화면
	 * @author 김영환
	 * @since 2018.02.27
	 * @param Model model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialOutputMng.do")
	public String materialOutputMng(Model model) throws Exception{
		model.addAttribute("startDt", DateTime.requestMmDay("1","yyyy-MM-dd"));
		model.addAttribute("endDt", DateTime.getDateString());
		
		return "/epms/material/output/materialOutputMng";
	}
	
	/**
	 * 출고현황 관리 : 출고관리 Grid Layout
	 * @author 김영환
	 * @since 2018.02.26
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/materialOutputMngLayout.do")
	public String materialOutputMngLayout() throws Exception{
		return "/epms/material/output/materialOutputMngLayout";
	}
	
}
