package aspn.hello.epms.material.output.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.material.output.model.MaterialOutputVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * [자재관리-출고현황] Mapper Class
 * 
 * @author 김영환
 * @since 2018.02.20
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.26		김영환		최초 생성
 *   
 * </pre>
 */

@Mapper("materialOutputMapper")
public interface MaterialOutputMapper {

	/**
	 * 자재출고정보 조회
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialOutputVO materialOutputVO
	 * @return List<HashMap<String, Object>>
	 * @throws Exception
	 */
	List<HashMap<String, Object>> selectMaterialOutputList(MaterialOutputVO materialOutputVO);

	/**
	 * 입고내역 삭제 MATERIAL_INOUT_ITEM UPDATE(삭제처리)
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialOutputVO materialOutputVO
	 * @return int
	 * @throws Exception
	 */
	int deleteMaterialInoutList(MaterialOutputVO materialOutputVO);

	/**
	 * 기존 입고내역 입고수량  (기존 내역 입출고 수량 조회)
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialOutputVO materialOutputVO
	 * @return int
	 * @throws Exception
	 */
	int selectMaterialInoutQnty(MaterialOutputVO materialOutputVO);

	/**
	 * 입고내역 수정(아이템) MATERIAL_INOUT_ITEM UPDATE
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialOutputVO materialOutputVO
	 * @return int
	 * @throws Exception
	 */
	int updateMaterialInoutList(MaterialOutputVO materialOutputVO);

	/**
	 * 출고수량 차이 재고 반영
	 *
	 * @author 김영환
	 * @since 2018.02.20
	 * @param MaterialOutputVO materialOutputVO
	 * @return int
	 * @throws Exception
	 */
	int updateMaterialStockOut(MaterialOutputVO materialOutputVO); 
	
}
