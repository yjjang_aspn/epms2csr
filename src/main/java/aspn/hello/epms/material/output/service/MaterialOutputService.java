package aspn.hello.epms.material.output.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.material.output.model.MaterialOutputVO;

/**
 * [자재관리-출고현황] Service Class
 * 
 * @author 김영환
 * @since 2018.02.26
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.02.26		김영환		최초 생성
 *   
 * </pre>
 */

public interface MaterialOutputService {

	/**
	 * 자재출고 : Grid Data 조회
	 *
	 * @author 김영환
	 * @since 2018.02.26
	 * @param MaterialOutputVO materialMasterVO
	 * @return List<HashMap<String, Object>>
	 */
	List<HashMap<String, Object>> selectMaterialOutputList(MaterialOutputVO materialOutputVO);

	/**
	 * 자재출고 : Grid Data 수정
	 *
	 * @author 김영환
	 * @since 2018.02.26
	 * @param MaterialOutputVO materialMasterVO
	 * @param List<HashMap<String, Object>> saveDataList
	 * @return 
	 */
	void materialOutputListEdit(List<HashMap<String, Object>> saveDataList, MaterialOutputVO materialOutputVO);

}
