package aspn.hello.epms.notice.model;

import aspn.hello.com.model.AbstractVO;

/**
 * 공지사항VO 클래스
 * 
 * @author 윤선철
 * @since 2017.08.14
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.08.14  윤선철          최초 생성
 *
 *      </pre>
 */

public class NoticeVO extends AbstractVO {

	private static final long serialVersionUID = 1L;

	/** EPMS_NOTICE */
	private String COMPANY_ID = ""; // 회사코드
	private String NOTICE = ""; // 공지사항 Key
	private String TYPE = ""; // 공지사항 유형(1:기본, 2:팝업)
	private String TITLE = ""; // 공지사항 제목
	private String CONTENT = ""; // 공지사항 내용
	private String POPYN = ""; // 팝업 공지사항 노출 여부(기본일 경우 'N')
	private String POPSTART_DT = ""; // 팝업 공지사항 노출 시작일시
	private String POPEND_DT = ""; // 팝업 공지사항 노출 종료일시
	private String REG_ID = ""; // 작성자
	private String REG_DT = ""; // 작성일시
	private String UPD_ID = ""; // 수정자
	private String UPD_DT = ""; // 수정일시
	private String DEL_YN = ""; // 삭제여부(기본일 경우 'N')
	private String DEL_ID = ""; // 삭제자
	private String DEL_DT = ""; // 삭제일시
	private String LOCATION = ""; // 공지할 공장
	

	/** PARAMETER */
	private String user_nm = ""; // 작성자명
	private String sub_Auth = ""; // 공지사항권한
	private String searchKey = ""; // 검색 조건 Key
	private String searchVal = ""; // 검색 조건 Value
	private String DIVISION_NM = ""; // 부서명

	/** paging model */
	private String RN = ""; // 리스트의 ROWNUM
	private String page = ""; // 현재 페이지
	private int s_range = 0; // 조건 시작점
	private int e_range = 0; // 조건 끝점
	private int totalCnt = 0; // 게시물 총 갯수
	private int todayCnt = 0; // 오늘 게시물 갯수
	private String prevUrl = ""; // 이전 page주소
	
	private String startDt = ""; // 날짜 시작점
	private String endDt = "";   // 날짜 종료점

	/** ATTACH */
	private String ATTACH_GRP_NO = ""; // 파일 그룹 번호
	private String ATTACH = ""; // 파일 번호
	private String attachExistYn = ""; // 파일등록여부
	private String delSeq = ""; // 삭제시컨스
	private String FILE_CNT = ""; // 파일 건수
	
	
	
	
	public String getFILE_CNT() {
		return FILE_CNT;
	}

	public void setFILE_CNT(String fILE_CNT) {
		FILE_CNT = fILE_CNT;
	}

	public String getLOCATION() {
		return LOCATION;
	}

	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}

	public String getStartDt() {
		return startDt;
	}

	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}

	public String getEndDt() {
		return endDt;
	}

	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}

	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}

	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}

	public String getNOTICE() {
		return NOTICE;
	}

	public void setNOTICE(String nOTICE) {
		NOTICE = nOTICE;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public String getTITLE() {
		return TITLE;
	}

	public void setTITLE(String tITLE) {
		TITLE = tITLE;
	}

	public String getCONTENT() {
		return CONTENT;
	}

	public void setCONTENT(String cONTENT) {
		CONTENT = cONTENT;
	}

	public String getPOPYN() {
		return POPYN;
	}

	public void setPOPYN(String pOPYN) {
		POPYN = pOPYN;
	}

	public String getPOPSTART_DT() {
		return POPSTART_DT;
	}

	public void setPOPSTART_DT(String pOPSTART_DT) {
		POPSTART_DT = pOPSTART_DT;
	}

	public String getPOPEND_DT() {
		return POPEND_DT;
	}

	public void setPOPEND_DT(String pOPEND_DT) {
		POPEND_DT = pOPEND_DT;
	}

	public String getREG_ID() {
		return REG_ID;
	}

	public void setREG_ID(String rEG_ID) {
		REG_ID = rEG_ID;
	}

	public String getREG_DT() {
		return REG_DT;
	}

	public void setREG_DT(String rEG_DT) {
		REG_DT = rEG_DT;
	}

	public String getUPD_ID() {
		return UPD_ID;
	}

	public void setUPD_ID(String uPD_ID) {
		UPD_ID = uPD_ID;
	}

	public String getUPD_DT() {
		return UPD_DT;
	}

	public void setUPD_DT(String uPD_DT) {
		UPD_DT = uPD_DT;
	}

	public String getDEL_YN() {
		return DEL_YN;
	}

	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}

	public String getDEL_ID() {
		return DEL_ID;
	}

	public void setDEL_ID(String dEL_ID) {
		DEL_ID = dEL_ID;
	}

	public String getDEL_DT() {
		return DEL_DT;
	}

	public void setDEL_DT(String dEL_DT) {
		DEL_DT = dEL_DT;
	}

	public String getUser_nm() {
		return user_nm;
	}

	public void setUser_nm(String user_nm) {
		this.user_nm = user_nm;
	}

	public String getSub_Auth() {
		return sub_Auth;
	}

	public void setSub_Auth(String sub_Auth) {
		this.sub_Auth = sub_Auth;
	}

	public String getSearchKey() {
		return searchKey;
	}

	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	public String getSearchVal() {
		return searchVal;
	}

	public void setSearchVal(String searchVal) {
		this.searchVal = searchVal;
	}

	public String getDIVISION_NM() {
		return DIVISION_NM;
	}

	public void setDIVISION_NM(String dIVISION_NM) {
		DIVISION_NM = dIVISION_NM;
	}

	public String getRN() {
		return RN;
	}

	public void setRN(String rN) {
		RN = rN;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public int getS_range() {
		return s_range;
	}

	public void setS_range(int s_range) {
		this.s_range = s_range;
	}

	public int getE_range() {
		return e_range;
	}

	public void setE_range(int e_range) {
		this.e_range = e_range;
	}

	public int getTotalCnt() {
		return totalCnt;
	}

	public void setTotalCnt(int totalCnt) {
		this.totalCnt = totalCnt;
	}

	public int getTodayCnt() {
		return todayCnt;
	}

	public void setTodayCnt(int todayCnt) {
		this.todayCnt = todayCnt;
	}

	public String getPrevUrl() {
		return prevUrl;
	}

	public void setPrevUrl(String prevUrl) {
		this.prevUrl = prevUrl;
	}

	public String getATTACH_GRP_NO() {
		return ATTACH_GRP_NO;
	}

	public void setATTACH_GRP_NO(String aTTACH_GRP_NO) {
		ATTACH_GRP_NO = aTTACH_GRP_NO;
	}

	public String getATTACH() {
		return ATTACH;
	}

	public void setATTACH(String aTTACH) {
		ATTACH = aTTACH;
	}

	public String getAttachExistYn() {
		return attachExistYn;
	}

	public void setAttachExistYn(String attachExistYn) {
		this.attachExistYn = attachExistYn;
	}

	public String getDelSeq() {
		return delSeq;
	}

	public void setDelSeq(String delSeq) {
		this.delSeq = delSeq;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "NoticeVO [COMPANY_ID=" + COMPANY_ID + ", NOTICE=" + NOTICE + ", TYPE=" + TYPE + ", TITLE=" + TITLE
				+ ", CONTENT=" + CONTENT + ", POPYN=" + POPYN + ", POPSTART_DT=" + POPSTART_DT + ", POPEND_DT="
				+ POPEND_DT + ", ATTACH_GRP_NO=" + ATTACH_GRP_NO + ", REG_ID=" + REG_ID + ", REG_DT=" + REG_DT
				+ ", UPD_ID=" + UPD_ID + ", UPD_DT=" + UPD_DT + ", DEL_YN=" + DEL_YN + ", DEL_ID=" + DEL_ID
				+ ", DEL_DT=" + DEL_DT + ", sub_Auth=" + sub_Auth + ", searchKey=" + searchKey + ", searchVal="
				+ searchVal + ", RN=" + RN + ", page=" + page + ", s_range=" + s_range + ", e_range=" + e_range
				+ ", totalCnt=" + totalCnt + ", todayCnt=" + todayCnt + ", prevUrl=" + prevUrl + ", attachExistYn="
				+ attachExistYn + ", delSeq=" + delSeq + ", FILE_CNT=" + FILE_CNT + "]";
	}
}
