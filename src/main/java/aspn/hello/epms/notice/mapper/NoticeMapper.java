package aspn.hello.epms.notice.mapper;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.notice.model.NoticeVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * 공지사항를 관리하기 위한 Mapper Class
 * @author 김영환
	 * @since 2018.04.18
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2018.04.18  김영환           최초 생성
 *
 * </pre>
 */
@Mapper("noticeMapper")
public interface NoticeMapper {

	/**
	 * 공지사항 목록조회
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return List<NoticeVO>
	 * @throws Exception
	 */
	public List<NoticeVO> getNoticeList(NoticeVO noticeVO) throws Exception;
	
	/**
	 * 공지사항 총개수 조회
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return int
	 * @throws Exception
	 */
	public int getNoticeListCount(NoticeVO noticeVO) throws Exception;
	
	/**
	 * 공지사항 등록
	 * @author 김영환
	 * @since 2018.04.18
	 * @param 
	 * @return String
	 * @throws Exception
	 */
	public String selectNoticeSeq() throws Exception;
	
	/**
	 * 공지사항 등록
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return 
	 * @throws Exception
	 */
	public void insertNotice(NoticeVO noticeVO) throws Exception;

	/**
	 * 공지사항 상세조회
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return 
	 * @throws Exception
	 */
	public NoticeVO getNoticeView(NoticeVO noticeVO) throws Exception;

	/**
	 * 공지사항 삭제
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return 
	 * @throws Exception
	 */
	public void deleteNotice(NoticeVO noticeVO) throws Exception;

	/**
	 * 공지사항 수정
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return 
	 * @throws Exception
	 */
	public void updateNotice(NoticeVO noticeVO) throws Exception;

	/**
	 * 모바일 - 대시보드 공지사항 목록
	 * @author 김영환
	 * @since 2018.04.18
	 * @param 
	 * @return 
	 */
	List<HashMap<String,Object>> getMobileNoticeSimpleList(String companyId);

	/**
	 * 모바일 - 공지사항 목록
	 * @author 김영환
	 * @since 2018.04.18
	 * @param 
	 * @return
	 */
	List<HashMap<String, Object>> getMobileNoticeList(NoticeVO _noticeVO);

	/**
	 * 모바일 - 공지사항 컨텐츠
	 * @author 김영환
	 * @since 2018.04.18
	 * @param 
	 * @return 
	 */
	HashMap<String, Object> getMobileNoticeContent(NoticeVO _noticeVO);

	/**
	 * 공지사항 대상 공장 목록
	 * @author 김영환
	 * @since 2018.05.29
	 * @param NoticeVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> locationListOpt(NoticeVO noticeVO);

//	/**
//	 * 공지사항 목록
//	 * @param notice
//	 * @return
//	 * @throws Exception
//	 */
//	public List<NoticeModel> selectNoticeList(NoticeModel notice) throws Exception;
//	public Entity selectNoticeList(Entity param) throws Exception;
//
//	/**
//	 * 공지사항 상세
//	 * @param notice
//	 * @return
//	 * @throws Exception
//	 */
//	public NoticeModel selectNoticeView(NoticeModel notice) throws Exception;
//	public Entity selectNoticeView(Entity param) throws Exception;
//
//	/**
//	 * 공지사항 등록
//	 * @param notice
//	 * @throws Exception
//	 */
//	public void insertNotice(NoticeModel notice) throws Exception;
//	public void insertNotice(Entity param) throws Exception;
//
//	/**
//	 * 공지사항 수정
//	 * @param notice
//	 * @throws Exception
//	 */
//	public void updateNotice(NoticeModel notice) throws Exception;
//
//	/**
//	 * 공지사항 삭제
//	 * @param notice
//	 * @throws Exception
//	 */
//	public void updateNoticeDelete(NoticeModel data) throws Exception;


}
