package aspn.hello.epms.notice.service;

import aspn.com.common.util.ObjUtil;
import aspn.hello.epms.equipment.master.model.EquipmentMasterVO;
import aspn.hello.epms.notice.mapper.NoticeMapper;
import aspn.hello.epms.notice.model.NoticeVO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * 공지사항를 관리하기 위한 Implement Class
 * @author 김영환
	 * @since 2018.04.18
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2018.04.18  김영환           최초 생성
 *
 * </pre>
 */

@Service("noticeService")
public class NoticeServiceImpl implements NoticeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(NoticeServiceImpl.class);
	
	/** NoticeMapper */
	@Autowired
	private NoticeMapper noticeMapper;
	
	/**
	 * 공지사항 목록조회
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return List<NoticeVO>
	 * @throws Exception
	 */
	@Override
	public List<NoticeVO> getNoticeList(NoticeVO noticeVO) throws Exception {
		
		int startRange = 0;
		if(noticeVO.getPage()!=""){
			startRange = ObjUtil.setCurRange(((Integer.parseInt(noticeVO.getPage())-1) *  10)+""); 
		}
		noticeVO.setS_range(startRange+1);
		noticeVO.setE_range(startRange+10);
		
		return noticeMapper.getNoticeList(noticeVO);
	}

	/**
	 * 공지사항 총개수 조회
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return int
	 * @throws Exception
	 */
	@Override
	public int getNoticeListCount(NoticeVO noticeVO) throws Exception {
		return noticeMapper.getNoticeListCount(noticeVO);
	}

	/**
	 * 공지사항 등록
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return NoticeVO
	 * @throws Exception
	 */
	@Override
	public NoticeVO insertNotice(NoticeVO noticeVO) throws Exception {
		
		// 공지사항 시퀀스 세팅
		String SEQ_NOTICE = noticeMapper.selectNoticeSeq();
		noticeVO.setNOTICE(SEQ_NOTICE);
		
		noticeMapper.insertNotice(noticeVO);
		
		return noticeMapper.getNoticeView(noticeVO);
	}

	/**
	 * 공지사항 상세조회
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return NoticeVO
	 * @throws Exception
	 */
	@Override
	public NoticeVO getNoticeView(NoticeVO noticeVO) throws Exception {
		
		return noticeMapper.getNoticeView(noticeVO);
	}

	/**
	 * 공지사항 삭제
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return 
	 * @throws Exception
	 */
	@Override
	public void deleteNotice(NoticeVO noticeVO) throws Exception {
		noticeMapper.deleteNotice(noticeVO);
	}

	/**
	 * 공지사항 수정
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return NoticeVO
	 * @throws Exception
	 */
	@Override
	public NoticeVO updateNotice(NoticeVO noticeVO) throws Exception {
		noticeMapper.updateNotice(noticeVO);
		
		return noticeMapper.getNoticeView(noticeVO);
	}
	
	/**
	 * 공지사항 대상 공장 목록
	 * @author 김영환
	 * @since 2018.05.29
	 * @param NoticeVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> locationListOpt(NoticeVO noticeVO) {
		return noticeMapper.locationListOpt(noticeVO);
	}

	/**
	 * 모바일 - 메인대시보드 공지사항 목록 조회
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return 
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> getMobileNoticeSimpleList(String companyId) throws Exception {
		return noticeMapper.getMobileNoticeSimpleList(companyId);
	}

	/**
	 * 모바일 - 공지사항 목록 조회 
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return 
	 * @throws Exception
	 */
	@Override
	public List<HashMap<String, Object>> getMobileNoticeList(NoticeVO _noticeVO) throws Exception {
		return noticeMapper.getMobileNoticeList(_noticeVO);
	}

	
	/**
	 * 모바일 - 공지사항 컨텐츠
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return 
	 * @throws Exception
	 */
	@Override
	public HashMap<String, Object> getMobileNoticeContent(NoticeVO _noticeVO){
		return noticeMapper.getMobileNoticeContent(_noticeVO);
	}

	


	//	/**
//	 * 공지 사항 수정
//	 */
//	@Override
//	public void updateNotice(HttpServletRequest request, Entity param, AttachModel attachModel) throws Exception{
//		
//		
//	}
	
//	@Autowired
//	NoticeDao noticeDao;
//	
//	@Autowired
//	AttachDao attachDao;
//	
//	@Autowired
//	MsSqlSession msSqlSession;
//	
//	/**
//	 * 공지 사항 목록
//	 */	
//	@Override
//	public Entity noticeList(Entity param) throws Exception {
//				
//		int startRange = 0;
//		if(param.getString("page") != ""){
//			startRange = ObjUtil.setCurRange(((Integer.parseInt(param.getString("page"))-1) *  10)+"");
//		}
//		param.setValue("s_range", startRange);
//		param.setValue("e_range", 10);
//		
//		String sqlId = "aspn.project.eaccounting.notice.dao.NoticeDao.selectNoticeList";
//		return msSqlSession.selectList(sqlId,param);
//		
//	}
//	@Override
//	public int noticeListCount(Entity param) throws Exception {
//		
//		String sqlId = "aspn.project.eaccounting.notice.dao.NoticeDao.selectNoticeListCount";
//		return msSqlSession.selectOne(sqlId,"int",param).getInt();
//	}
//
//	/**
//	 * 공지 사항 상세
//	 */
//	@Override
//	public Entity noticeView(Entity param) throws Exception {
//		String sqlId = "aspn.project.eaccounting.notice.dao.NoticeDao.noticeViewCntUpdate";
//		msSqlSession.update(sqlId,param);
//		
//		sqlId = "aspn.project.eaccounting.notice.dao.NoticeDao.selectNoticeView";
//		return msSqlSession.selectOneRow(sqlId,param);
//		
//	}
//
//	/**
//	 * 공지 사항 등록
//	 */
//	@Override
//	public void insertNotice(Entity param) throws Exception {
//		
//		String sqlId = "aspn.project.eaccounting.notice.dao.NoticeDao.insertNotice";
//		msSqlSession.insert(sqlId,param);
//	}
//
//	/**
//	 * 공지 사항 수정
//	 */
//	@Override
//	public void updateNotice(HttpServletRequest request, Entity param, AttachModel attachModel) throws Exception{
//		
//		String attachGrpNo = "";
//		//insert 공급사 성공
//		if(!"".equals(attachModel.getDEL_SEQ()) && !"0".equals(attachModel.getDEL_SEQ())){
//			String [] arrDelSeq = StringUtils.split(param.getString("DEL_SEQ"),",");
//			attachModel.setArrDelSeq(arrDelSeq);
//
//			List<AttachModel> fileList = attachDao.selectFileList(attachModel);
//			for(AttachModel fileInfo:fileList) {
//				
//				//객체 정보를 가지고 물리 파일을 삭제
//				boolean delFileYn = FileHandler.deleteFile(fileInfo.getPATH(), fileInfo.getNAME());
//				if(delFileYn){
//					FileHandler.thumbDeleteFile(fileInfo.getPATH(), "thum_"+fileInfo.getNAME());
//				}
//				
//				//ATTACH 로 DB 로우 삭제 
//				attachDao.deleteFile(fileInfo);
//			}
//		}
//		
//		//insert 공급사 성공
//		List<AttachModel> fileList = FileHandler.uploadFiles(request, "fileList");
//		
//		//file 존재여부 체크
//		if( fileList != null && fileList.size() > 0 ) {
//			String arr_fileGbp[] = param.getString("fileGb").split(",");
//			if("".equals(attachModel.getATTACH_GRP_NO())){
//				attachGrpNo = attachDao.selectAttachGrpNo();	
//			}else{
//				attachGrpNo = attachModel.getATTACH_GRP_NO();
//			}
//			
//			/*file Insert start*/
//			for(int i = 0; i < fileList.size(); i++) {
//				
//				attachModel = fileList.get(i);
//				
//				attachModel.setATTACH_GRP_NO(attachGrpNo);
//				attachModel.setMODULE(param.getString("MODULE"));
//				attachModel.setCD_FILE_TYPE(arr_fileGbp[i]);
//				
//				attachDao.insertAttach(attachModel);
//			}
//			/*file Insert end*/
//		}
//
//		param.setValue("ATTACH_GRP_NO", attachGrpNo);
//		
//		String sqlId = "aspn.project.eaccounting.notice.dao.NoticeDao.updateNotice";
//		msSqlSession.update(sqlId,param);
//	}
//
//	/**
//	 * 공지 사항 삭제
//	 */
//	@Override
//	public void deleteNotice(Entity param) throws Exception{
//		String sqlId = "aspn.project.eaccounting.notice.dao.NoticeDao.updateNoticeDelete";
//		msSqlSession.update(sqlId,param);
//	}

}
