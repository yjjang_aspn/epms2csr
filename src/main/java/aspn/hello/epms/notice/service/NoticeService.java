package aspn.hello.epms.notice.service;

import java.util.HashMap;
import java.util.List;

import aspn.hello.epms.notice.model.NoticeVO;


/**
 * 공지사항를 관리하기 위한 Service Class
 * @author 김영환
 * @since 2018.04.18
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.04.18  윤선철          최초 생성
 *
 * </pre>
 */

public interface NoticeService {

	/**
	 * 공지사항 목록조회
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return List<NoticeVO>
	 * @throws Exception
	 */
	public List<NoticeVO> getNoticeList(NoticeVO noticeVO) throws Exception;

	/**
	 * 공지사항 총개수 조회
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return int
	 * @throws Exception
	 */
	public int getNoticeListCount(NoticeVO noticeVO) throws Exception;
	
	/**
	 * 공지사항 등록
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return NoticeVO
	 * @throws Exception
	 */
	public NoticeVO insertNotice(NoticeVO noticeVO) throws Exception;

	/**
	 * 공지사항 상세조회
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return NoticeVO
	 * @throws Exception
	 */
	public NoticeVO getNoticeView(NoticeVO noticeVO) throws Exception;

	/**
	 * 공지사항 삭제
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return 
	 * @throws Exception
	 */
	public void deleteNotice(NoticeVO noticeVO) throws Exception;
	
	/**
	 * 공지사항 수정
	 * @author 김영환
	 * @since 2018.04.18
	 * @param NoticeVO
	 * @return NoticeVO
	 * @throws Exception
	 */
	public NoticeVO updateNotice(NoticeVO noticeVO) throws Exception;

	/**
	 * 모바일 - 메인대시보드 공지사항 목록 조회
	 * @author 김영환
	 * @since 2018.04.18
	 * @param companyId 회사코드
	 * @return 공지사항 목록(TITLE, REG_DT)
	 * @throws Exception
	 */
	List<HashMap<String, Object>> getMobileNoticeSimpleList(String companyId) throws Exception;

	/**
	 * 모바일 - 공지사항 목록 조회
	 * @author 김영환
	 * @since 2018.04.18
	 * @param _noticeVO companyId, s_range, e_range
	 * @return 공지사항 목록(TITLE, REG_DT, USER_NM, ATTACH_GRP_NO, ATTACH_LIST[])
	 * @throws Exception
	 */
	List<HashMap<String, Object>> getMobileNoticeList(NoticeVO _noticeVO) throws Exception;

	/**
	 * 모바일 - 공지사항 컨텐츠 조회
	 * @author 김영환
	 * @since 2018.04.18
	 * @param _noticeVO NOTICE_NDX
	 * @return 공지사항 컨텐츠
	 * @throws Exception
	 */
	HashMap<String, Object> getMobileNoticeContent(NoticeVO _noticeVO) throws Exception;

	/**
	 * 공지사항 대상 공장 목록
	 * @author 김영환
	 * @since 2018.05.29
	 * @param NoticeVO
	 * @return List<HashMap<String, Object>>
	 */
	public List<HashMap<String, Object>> locationListOpt(NoticeVO noticeVO);

//	public int noticeListCount(Entity param) throws Exception;
//
//	/**
//	 * 공지 사항 상세
//	 * @param notice
//	 * @return
//	 * @throws Exception
//	 */
//	public Entity noticeView(Entity param) throws Exception;
//
//	/**
//	 * 공지 사항 등록
//	 * @param notice
//	 * @throws Exception
//	 */
//	public void insertNotice(Entity param) throws Exception;
//
//	/**
//	 * 공지사항 수정
//	 * @param notice
//	 * @throws Exception
//	 */
//	public void updateNotice(HttpServletRequest request, Entity param, AttachModel attachModel) throws Exception;
//
//	/**
//	 * 공지사항 삭제
//	 * @param notice
//	 * @throws Exception
//	 */
//	public void deleteNotice(Entity param) throws Exception;
}
