package aspn.hello.epms.notice.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.util.WebUtils;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.AspnMessageSource;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.JsUtil;
import aspn.com.common.util.SessionUtil;
import aspn.com.common.util.Paging;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.com.service.DeviceService;
import aspn.hello.epms.notice.model.NoticeVO;
import aspn.hello.epms.notice.service.NoticeService;
import aspn.hello.mem.model.MemberVO;
import aspn.com.common.util.ObjUtil;

/**
 * [공지사항] Controller Class
 * @author 김영환
 * @since 2018.04.17
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일			수정자			수정내용
 *   -----------	----------	---------------------------
 *   2018.04.17		김영환			최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/epms/notice")
public class NoticeController extends ContSupport {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	/* INSTANCE VAR */
	@Autowired
	NoticeService noticeService;
	
	@Autowired
	AttachService attachService;
	
	@Autowired
	DeviceService deviceService;
	
	/* 다국어 처리 */
	@Autowired
    AspnMessageSource msgSource;
	
	
	private String LIST_URL = "/epms/notice/noticeList.do";
	private String MNG_LIST_URL = "/epms/notice/noticeMng.do";
	private String FORM_URL = "/epms/notice/noticeWriteForm.do";
//	private String VIEW_URL = "/project/eaccounting/notice/web/noticeView.do?CONTENTS=";



	/**
	 * 공지사항 목록
	 * @author 김영환
	 * @since 2018.04.17
	 * @param NoticeVO noticeVO
	 * @param Model model
	 * @return /epms/notice/noticeList
	 * @throws Exception
	 */
	@RequestMapping(value = {"/noticeList.do", "/noticeMng.do"})
	public String noticeList(HttpServletRequest request, NoticeVO noticeVO, Model model) throws Exception {
		
		try {
			
			String[] urlArr = request.getRequestURI().split("/");
			
			String auth = "N";
			if("noticeMng.do".equals(urlArr[(urlArr.length)-1])){
				auth = "Y";
			}
			
			model.addAttribute("auth", auth);
			noticeVO.setCompanyId(getSsCompanyId());
			noticeVO.setSsLocation("5100");
			noticeVO.setSubAuth(getSsSubAuth());
			List<NoticeVO> list = noticeService.getNoticeList(noticeVO);
			int totalCnt = noticeService.getNoticeListCount(noticeVO);
			//검색 조건 값 세팅
			NoticeVO search = new NoticeVO();
			search.setTYPE(noticeVO.getTYPE());
			search.setSearchKey(noticeVO.getSearchKey());
			search.setSearchVal(noticeVO.getSearchVal());
			
			model.addAttribute("search", search);
			model.addAttribute("list", list);
			model.addAttribute("totalCnt", totalCnt);
			int page = 1;
			if(noticeVO.getPage()!=""){
				page = Integer.parseInt(noticeVO.getPage());
			}
			model.addAttribute("page", page);
			model.addAttribute("pageHTML", new Paging(page, totalCnt, 10, 10).getPagingHtml());		
			
			
		} catch (Exception e) {
		      e.printStackTrace();
		      logger.error("NoticeController.noticeList Error > " + e.toString());
		      
	    }
		return "/epms/notice/noticeList";

	}
	
/*	*//**
	 * 공지사항 관리 
	 * @author 김영환
	 * @since 2018.04.17
	 * @param NoticeVO noticeVO
	 * @param Model model
	 * @return /epms/notice/noticeList
	 * @throws Exception
	 *//*
	@RequestMapping(value = "/noticeMng.do")
	public String noticeMng(HttpServletRequest request, NoticeVO noticeVO, Model model) throws Exception {
		
		try {
			
			System.out.println(request.getRequestURI());
			
			model.addAttribute("auth", "Y");
			
			List<NoticeVO> list = noticeService.getNoticeList(noticeVO);
			int totalCnt = noticeService.getNoticeListCount(noticeVO);
			//검색 조건 값 세팅
			NoticeVO search = new NoticeVO();
			search.setTYPE(noticeVO.getTYPE());
			search.setSearchKey(noticeVO.getSearchKey());
			search.setSearchVal(noticeVO.getSearchVal());
			
			model.addAttribute("search", search);
			model.addAttribute("list", list);
			model.addAttribute("totalCnt", totalCnt);
			int page = 1;
			if(noticeVO.getPage()!=""){
				page = Integer.parseInt(noticeVO.getPage());
			}
			model.addAttribute("page", page);
			model.addAttribute("pageHTML", new Paging(page, totalCnt, 10, 10).getPagingHtml());		
			
			
		} catch (Exception e) {
		      e.printStackTrace();
		      logger.error("NoticeController.noticeList Error > " + e.toString());
		      
	    }
		return "/epms/notice/noticeList";

	}
	
*/	

	/**
	 * 공지 사항 등록 폼
	 * @author 김영환
	 * @since 2018.04.17
	 * @param NoticeVO noticeVO
	 * @param Model model
	 * @return /epms/notice/noticeWriteForm
	 * @throws Exception
	 */
	@RequestMapping(value = "/noticeWriteForm.do")
	public String noticeWriteForm(NoticeVO noticeVO, Model model) throws Exception {
		noticeVO.setCompanyId(getSsCompanyId());
		noticeVO.setSubAuth(getSsSubAuth());
		List<HashMap<String, Object>> locationList = noticeService.locationListOpt(noticeVO);
		model.addAttribute("locationList", locationList);
		return "/epms/notice/noticeWriteForm";
	}
	
	/**
	 * 공지사항 수정 폼
	 * @author 김영환
	 * @since 2018.04.17
	 * @param NoticeVO noticeVO
	 * @param Model model
	 * @return /epms/notice/noticeEditForm
	 * @throws Exception
	 */
	@RequestMapping(value = "/noticeEditForm.do")
	public String noticeEditForm(NoticeVO noticeVO, Model model) throws Exception {
		
		try {
			NoticeVO view = new NoticeVO();
			if(!"".equals(noticeVO.getNOTICE())){
				view = noticeService.getNoticeView(noticeVO);
				String auth = "N";
				if("Y".equals(noticeVO.getSub_Auth())){
					auth = "Y";
				}
				model.addAttribute("auth", auth);
				
				if(Integer.parseInt(view.getFILE_CNT().toString()) > 0){
					AttachVO attachVO = new AttachVO();
					attachVO.setATTACH_GRP_NO(view.getATTACH_GRP_NO());

					List<HashMap<String, Object>> attachList = attachService.selectFileList(attachVO);
					model.addAttribute("attachList", attachList);
				}
			}
			noticeVO.setCompanyId(getSsCompanyId());
			noticeVO.setSubAuth(getSsSubAuth());
			List<HashMap<String, Object>> locationList = noticeService.locationListOpt(noticeVO);
			model.addAttribute("locationList", locationList);
			model.addAttribute("view", view);
			
		} catch (Exception e) {
		      e.printStackTrace();
		      logger.error("NoticeController.noticeEditForm Error > " + e.toString());
		      
	    }
		
		return "/epms/notice/noticeEditForm";
	}
	
	/**
	 * 공지사항 상세
	 * @author 김영환
	 * @since 2018.04.17
	 * @param NoticeVO noticeVO
	 * @param Model model
	 * @return /epms/notice/noticeView
	 * @throws Exception
	 */
	@RequestMapping(value = "/noticeView.do")
	public String noticeView(HttpServletResponse response, NoticeVO noticeVO, Model model) throws Exception {
		
		try {
			//공지사항 관리 권한(등록,수정,삭제)
			String auth = "N";
			if(noticeVO.getSub_Auth().equals("Y")){
				auth = "Y";
			}
			model.addAttribute("auth", auth);
			
			NoticeVO view = noticeService.getNoticeView(noticeVO);
			model.addAttribute("view", view);
		
			if(Integer.parseInt(view.getFILE_CNT().toString()) > 0){
				AttachVO attachVO = new AttachVO();
				attachVO.setATTACH_GRP_NO(view.getATTACH_GRP_NO());

				List<HashMap<String, Object>> attachList = attachService.selectFileList(attachVO);
				model.addAttribute("attachList", attachList);
				model.addAttribute("attachSize", attachList.size());
			}

		} catch (Exception e) {
		      e.printStackTrace();
		      logger.error("NoticeController.noticeView Error > " + e.toString());
		      
	    }

		return "/epms/notice/noticeView";
	}
	
	/**
	 * 공지사항 등록 
	 * @author 김영환
	 * @since 2018.04.17
	 * @param NoticeVO noticeVO
	 * @param Model model
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertNotice.do")
	public void insertNotice(HttpServletRequest request, MultipartHttpServletRequest mRequest, HttpServletResponse response, NoticeVO noticeVO, Model model) throws Exception {
		
		try {
			
			// 이미지 파일 등록
			if ("Y".equals(noticeVO.getAttachExistYn())) {
				String attachGrpNo = attachService.selectAttachGrpNo();
				AttachVO attachVO = new AttachVO();
				attachVO.setDEL_SEQ(noticeVO.getDelSeq());
				attachVO.setATTACH_GRP_NO(attachGrpNo);
				attachVO.setMODULE("80");
				noticeVO.setATTACH_GRP_NO(attachGrpNo);
				attachService.AttachEdit(request, mRequest, attachVO);
			}
			noticeVO.setREG_ID(getSsUserId());
			noticeVO.setCOMPANY_ID(getSsCompanyId());
			
			//html 정규식 태그 치환
			String content = noticeVO.getCONTENT().replaceAll("&lt;", "<").replaceAll("&gt;", ">");			
			noticeVO.setCONTENT(content);
			// 공지 사항 등록
			NoticeVO notice = noticeService.insertNotice(noticeVO);

			//등록처리 후 공지사항 목록 화면 이동
			JsUtil.goAlertURL(msgSource.getMessage("commonmsg.Saved"), MNG_LIST_URL, response); // "등록되었습니다."
			
			if(notice != null){
				// Push 정보
				HashMap<String, Object> pushParam = new HashMap<String, Object>();
				String pushTitle = "";
				// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REQUEST_CANCEL-요청취소, NOTICE-공지사항
				pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_NOTICE);
				// Push Title
				pushTitle = "[공지사항] " + System.getProperty("line.separator") + notice.getTITLE();
				pushParam.put("PUSHTITLE", pushTitle);
				// 상세화면 IDX 세팅
				pushParam.put("PUSHMENUDETAILNDX", notice.getNOTICE());
				// 알림음 유형 세팅(01:일반, 02:긴급)
				pushParam.put("PUSHALARM", "01");
				
				pushParam.put("COMPANY_ID", notice.getCOMPANY_ID());
				pushParam.put("LOCATION", notice.getLOCATION());
				
				deviceService.sendPush(pushParam);
				
			}

		} catch (Exception e) {
		      e.printStackTrace();
		      logger.error("NoticeController.insertNotice Error > " + e.toString());
		      
	    }

	}
	
	/**
	 * 공지사항 삭제
	 * @author 김영환
	 * @since 2018.04.17
	 * @param NoticeVO noticeVO
	 * @param Model model
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteNotice.do")
	public void deleteNotice(HttpServletResponse response, NoticeVO noticeVO, Model model) throws Exception {
		
		try {
			// 공지사항 삭제
			noticeVO.setUPD_ID(getSsUserId());
			noticeService.deleteNotice(noticeVO);
			//삭제처리 후 공지사항 목록 화면 이동
			JsUtil.goAlertURL(msgSource.getMessage("noticeList.deleted"), MNG_LIST_URL, response); // 해당 공지사항이 정상 삭제되었습니다.
		} catch (Exception e) {
		      e.printStackTrace();
		      logger.error("NoticeController.deleteNotice Error > " + e.toString());
		      
	    }
	}
	
	/**
	 * 공지사항 수정
	 * @author 김영환
	 * @since 2018.04.17
	 * @param NoticeVO noticeVO
	 * @param Model model
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateNotice.do")
	public void updateNotice(HttpServletRequest request, MultipartHttpServletRequest mRequest, HttpServletResponse response, NoticeVO noticeVO,Model model) throws Exception {
				
		try {
			//수정 가능 여부
			NoticeVO data = noticeService.getNoticeView(noticeVO);
			if (data == null) {
				JsUtil.goAlertURL(msgSource.getMessage("noticeList.noEditItem"), MNG_LIST_URL, response); // 수정할 공지가 존재하지 않습니다.
				return;
			}
			
			// 파일 그룹 번호 세팅
			String attachGrpNo = data.getATTACH_GRP_NO();
			if (ObjUtil.isEmpty(attachGrpNo)) {
				if ("Y".equals(noticeVO.getAttachExistYn())) {
					attachGrpNo = attachService.selectAttachGrpNo();
				}
			}
			noticeVO.setATTACH_GRP_NO(attachGrpNo);
			
			// 이미지 파일 등록
			if ("Y".equals(noticeVO.getAttachExistYn()) || !ObjUtil.isEmpty(noticeVO.getDelSeq())) {
				AttachVO attachVO = new AttachVO();
				attachVO.setDEL_SEQ(noticeVO.getDelSeq());
				attachVO.setATTACH_GRP_NO(attachGrpNo);
				attachVO.setMODULE("8");
				noticeVO.setATTACH_GRP_NO(attachGrpNo);
				attachService.AttachEdit(request, mRequest, attachVO);
			}
						
			if ("0".equals(noticeVO.getFILE_CNT())) {
				noticeVO.setATTACH_GRP_NO("");
			}
			
			// html 정규식 태그 치환
			String content = noticeVO.getCONTENT().replaceAll("&lt;", "<").replaceAll("&gt;", ">");			
			noticeVO.setCONTENT(content);
			// 공지사항 수정
			noticeVO.setUPD_ID(getSsUserId());
			noticeVO.setCOMPANY_ID(getSsCompanyId());
			NoticeVO notice = noticeService.updateNotice(noticeVO);
			
			//수정처리 후 공지사항 목록 화면 이동
			JsUtil.goAlertURL(msgSource.getMessage("commonmsg.Edited"), MNG_LIST_URL, response); // 수정되었습니다.
//			JsUtil.goURL(VIEW_URL + param.getString("CONTENTS"), response);
			
			if(notice != null){
				// Push 정보
				HashMap<String, Object> pushParam = new HashMap<String, Object>();
				String pushTitle = "";
				// Push Menu 구분 : REQUEST-내요청상세, REQUEST_COMPLETE-내요청상세(완료), APPR-결재상세, PLAN-계획및배정상세, REPAIR-정비상세, REQUEST_CANCEL-요청취소, NOTICE-공지사항
				pushParam.put("PUSHMENUTYPE", CodeConstants.EPMS_PUSHMENUTYPE_NOTICE);
				// Push Title
				pushTitle = "[공지사항] " + System.getProperty("line.separator") + notice.getTITLE();
				pushParam.put("PUSHTITLE", pushTitle);
				// 상세화면 IDX 세팅
				pushParam.put("PUSHMENUDETAILNDX", notice.getNOTICE());
				// 알림음 유형 세팅(01:일반, 02:긴급)
				pushParam.put("PUSHALARM", "01");
				
				pushParam.put("COMPANY_ID", notice.getCOMPANY_ID());
				pushParam.put("LOCATION", notice.getLOCATION());
				
				deviceService.sendPush(pushParam);
				
			}

		} catch (Exception e) {
		      e.printStackTrace();
		      logger.error("NoticeController.updateNotice Error > " + e.toString());
		      
	    }

	}
//
//	/**
//	 * 공지 사항 등록 폼
//	 * @param reqeust
//	 * @param response
//	 * @param notice
//	 * @param model
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping(value = "/web/noticeWrite.do")
//	public String noticeWrite(HttpServletRequest request, HttpServletResponse response, NoticeModel notice, Model model) throws Exception {
//
//		return "/project/epmsounting/notice/web/noticeWrite";
//	}
//
//	/**
//	 * 공지 사항 등록 처리
//	 * @param reqeust
//	 * @param response
//	 * @param notice
//	 * @param model
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping(value = "/web/noticeWriteProc.do")
//	public void noticeWriteProc(HttpServletRequest request, HttpServletResponse response, NoticeModel notice, Model model) throws Exception {
//		
//		Entity param = new Entity(request);
//		
//		try {
//
//			if ("Y".equals(param.getString("attachExistYn"))) {
//				String attachGrpNo = attachService.selectAttachGrpNo();
//				AttachModel attachModel = new AttachModel();
//				attachModel.setDEL_SEQ(notice.getDEL_SEQ());
//				attachModel.setATTACH_GRP_NO(attachGrpNo);
//				attachModel.setMODULE("8");
//				param.setValue("attachGrpNo", attachGrpNo);
//				attachService.AttachEdit(request, attachModel);
//			}
//
//			param.setValue("member", WebUtils.getSessionAttribute(request, "user_id").toString());
//			param.setValue("locale", WebUtils.getSessionAttribute(request, "locale").toString());
//			if(WebUtils.getSessionAttribute(request, "bukrs").toString().equals("K100")){
//				param.setValue("module", "EACC_CA");
//			}else {
//				param.setValue("module", "EACC_HD");
//			}
//			
//
//			// 공지 사항 등록
//			noticeService.insertNotice(param);
//
//			JsUtil.goURL(LIST_URL, response);
//
//		} catch (Exception e) {
//			e.printStackTrace();logger.error(e.getMessage());
//			JsUtil.goAlertURL(SFAConstants.ERROR_MSG.get(SFAConstants.FAILED_TO_SAVE)[1], FORM_URL, response);
//			return;
//		}
//
//	}
//	
//	/**
//	 * 공지 사항 수정 폼
//	 * @param reqeust
//	 * @param response
//	 * @param notice
//	 * @param model
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping(value = "/web/noticeEdit.do")
//	public String noticeEdit(HttpServletRequest request, HttpServletResponse response, NoticeModel notice, Model model) throws Exception {
//
//		Entity param = new Entity(request);
//		
//		try {
//			param.setValue("locale", WebUtils.getSessionAttribute(request, "locale").toString());
//			
//			Entity item = new Entity();
//			if(!"".equals(param.getString("CONTENTS"))){
//				item = noticeService.noticeView(param).getEntity();
//				
//				if(item.getString("ATTACH_GRP_NO") != null && !"".equals(item.getString("ATTACH_GRP_NO"))){
//					AttachModel attachModel = new AttachModel();
//					attachModel.setATTACH_GRP_NO(item.getString("ATTACH_GRP_NO"));
//
//					List<AttachModel> attachList = attachService.selectFileList(attachModel);
//					model.addAttribute("attachList", attachList);
//				}
//			}
//			model.addAttribute("item", item);
//			
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		
//		return "/project/eaccounting/notice/web/noticeEdit";
//	}
//	
//	/**
//	 * 공지 사항 수정 처리
//	 * @param reqeust
//	 * @param response
//	 * @param notice
//	 * @param model
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping(value = "/web/noticeEditProc.do")
//	public void noticeEditProc(HttpServletRequest request, HttpServletResponse response, AttachModel attachModel, Model model) throws Exception {
//		
//		Entity param = new Entity(request);
//		
//		try {
//			
//			noticeService.updateNotice(request, param, attachModel);
//			
//			JsUtil.goURL(VIEW_URL + param.getString("CONTENTS"), response);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.error(e.getMessage());
//			JsUtil.showMessage("게시글 수정에  실패하였습니다. 다시 시도 해주십시오.", response);
//		}
//
//	}	
//
//	/**
//	 * 삭제처리
//	 * @param request, response, notice, model
//	 * @return List
//	 * @throws Exception
//	 */
//	@RequestMapping(value = "/web/noticeDeleteProc.do")
//	public void noticeDeleteProc(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
//		
//		Entity param = new Entity(request);
//		
//		try {
//			noticeService.deleteNotice(param);
//			
//			JsUtil.goURL(LIST_URL, response);
//		}catch (Exception e) {
//			e.printStackTrace();
//			logger.error(e.getMessage());
//			JsUtil.showMessage("게시글 삭제에  실패하였습니다. 다시 시도 해주십시오.", response);
//		}
//	}
	
	
}
