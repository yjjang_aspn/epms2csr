package aspn.hello.epms.notice.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import aspn.com.common.config.CodeConstants;
import aspn.com.common.util.ContSupport;
import aspn.com.common.util.JsonUtil;
import aspn.hello.com.model.AttachVO;
import aspn.hello.com.service.AttachService;
import aspn.hello.epms.notice.model.NoticeVO;
import aspn.hello.epms.notice.service.NoticeService;

/**
 * 공지사항 - 모바일을 관리하기 위한 컨트롤러 클래스
 * @author 김영환
 * @since 2018.04.18
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2018.04.18  김영환          최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/mobile/epms/notice")
public class MobileNoticeController extends ContSupport {
	private Logger logger = LoggerFactory.getLogger(this.getClass());


	@Autowired
	NoticeService noticeService;
	
	@Autowired
	AttachService attachService;
	

	/**
	 * 공지사항 - 모바일 리스트 호출
	 * @author 김영환
	 * @since 2018. 04. 18.
	 * @param NoticeVO _noticeVO
	 * @throws Exception
	 */
	@RequestMapping(value = "/noticeList.do")
	@ResponseBody
	public Map<String, Object> noticeList(NoticeVO _noticeVO) throws Exception {
		Map<String, Object> resultMap = new HashMap<>();
		try {

			_noticeVO.setCompanyId(getSsCompanyId());
			_noticeVO.setSsLocation("5100");
			_noticeVO.setSubAuth(getSsSubAuth());
			List<HashMap<String, Object>> list= noticeService.getMobileNoticeList(_noticeVO);

			resultMap.put("NOTICE_LIST", list);
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);


		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}

	/**
	 * 공지사항 - 모바일 상세보기 호출
	 * @author 김영환
	 * @since 2018. 04. 18.
	 * @param NoticeVO _noticeVO
	 * @throws Exception
	 */
	@RequestMapping(value = "/noticeDtl.do")
	@ResponseBody
	public Map<String, Object> noticeDtl(NoticeVO _noticeVO) throws Exception {
		Map<String, Object> resultMap = new HashMap<>();
		try {
			if (JsonUtil.isMobileNullParam(resultMap, _noticeVO, "NOTICE")) {
				return resultMap;
			} 
			
			HashMap<String, Object> result = noticeService.getMobileNoticeContent(_noticeVO);
			
			if(result == null) {
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.NO_DATA_FOUND);
			} else {
				resultMap.put("NOTICE", result);
				
				if(Integer.parseInt(result.get("FILE_CNT").toString()) > 0){
					AttachVO attachVO = new AttachVO();
					attachVO.setATTACH_GRP_NO(result.get("ATTACH_GRP_NO").toString());
					List<HashMap<String, Object>> attachList = attachService.selectFileList(attachVO);
					resultMap.put("ATTACH_LIST", attachList);
				}
				
				JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.SUCCESS);
				
			}
			return resultMap;
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			
			JsonUtil.setMobileNormalMessage(resultMap, CodeConstants.ERROR_ETC);
		}
		return resultMap;
	}
}
