<%@page contentType="application/vnd.ms-excel"%><%@page pageEncoding="UTF-8"%><%
String filename=request.getParameter("File").toString();
request.setCharacterEncoding("utf-8");
response.addHeader("Content-Disposition","attachment; filename=\""+new String(filename.getBytes("euc-kr"), "8859_1")+"\"");
response.addHeader("Cache-Control","max-age=1, must-revalidate");
String dataName = request.getParameter("dataName");
String XML = request.getParameter(dataName); 
if(XML==null) XML = "";
if(XML.length()>0&&XML.charAt(0)=='&') XML = XML.replaceAll("&lt;","<").replaceAll("&gt;",">").replaceAll("&amp;","&").replaceAll("&quot;","\"").replaceAll("&apos;","'").replaceAll("xls", "");
if(request.getParameter("htmlData") != null){
	XML = XML.replace("</body>", request.getParameter("htmlData")+"</body>");
}
if(XML.indexOf("<")>=0) out.print(XML);
else {
java.io.BufferedOutputStream O = new java.io.BufferedOutputStream(response.getOutputStream());
O.write(javax.xml.bind.DatatypeConverter.parseBase64Binary(XML)); O.flush(); O.close();
if(request.getParameter("htmlData") != null){
	out.print(request.getParameter("htmlData"));
}
} %>