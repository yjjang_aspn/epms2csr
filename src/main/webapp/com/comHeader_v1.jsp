<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="aspn.com.common.config.CodeConstants" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>HELLO COMPANY</title>
<link href="/css/com/web/common.css" rel="stylesheet" />
<link href="/css/com/web/page.css" rel="stylesheet" />
<link href="/css/com/web/cnc_treegrid.css" rel="stylesheet" />
<link href="/css/com/web/jquery-ui.css" rel="stylesheet" />
<script src="/js/jquery/jquery-1.11.2.min.js" type="text/javascript" ></script> <!-- 제이쿼리 구동 -->
<script src="/js/jquery/jquery-ui.js" type="text/javascript"></script>
<script src="/js/jquery/jquery.blockUI.js" type="text/javascript"></script>
<script src="/js/com/web/common.js" type="text/javascript"></script>
<script src="/js/com/web/menu_common.js" type="text/javascript"></script>
<script src="/js/com/web/default.js" type="text/javascript"></script>
<script src="/js/jquery/jquery.calendar.js" type="text/javascript"></script>
<script src="/js/jquery/jquery-accordion-menu.js" type="text/javascript"></script> <!-- 레프트메뉴 플러그인 -->
<script src="/js/treegrid/GridE.js" type="text/javascript" ></script>

<%-- 2018.04.04 추가 --%>
<script type="text/javascript" src="/js/jquery/jquery.MultiFile.js"></script>
<script type="text/javascript" src="/js/jquery/jquery-filestyle.js"></script>
<script type="text/javascript" src="/js/com/web/modalPopup.js"></script>
<script type="text/javascript" src="/js/com/web/tree_slide_left.js"></script>
<link href="/css/eacc/modalPopup.css" rel="stylesheet" >


<!-- 회사 코드 -->
<c:set var="COMPANY_ID" value="<%=CodeConstants.COMPANY_ID %>" />

<!-- 메뉴그룹 -->
<c:set var="HEADER_MENU_HELLOCOMPANY"   value="<%=CodeConstants.HEADER_MENU_HELLOCOMPANY %>" /> <%-- 헬로컴퍼니 --%>
<c:set var="HEADER_MENU_ORDER"          value="<%=CodeConstants.HEADER_MENU_ORDER %>" />        <%-- 주문 --%>
<c:set var="HEADER_MENU_PURCHASE"       value="<%=CodeConstants.HEADER_MENU_PURCHASE %>" />     <%-- 구매 --%>
<c:set var="HEADER_MENU_EACCOUNTING"    value="<%=CodeConstants.HEADER_MENU_EACCOUNTING %>" />  <%-- 경비 --%>
<c:set var="HEADER_MENU_ASSET"          value="<%=CodeConstants.HEADER_MENU_ASSET %>" />        <%-- 자산 --%>
<c:set var="HEADER_MENU_EPMS"           value="<%=CodeConstants.HEADER_MENU_EPMS %>" />         <%-- 설비관리 --%>

<!-- 공통코드 -->
<c:set var="COMM_YES" value="<%=CodeConstants.COMM_YES %>" /> <%-- Yes --%>
<c:set var="COMM_NO"  value="<%=CodeConstants.COMM_NO %>" />  <%-- No --%>

<!-- FLAG 코드 -->
<c:set var="FLAG_SAVE"     value="<%=CodeConstants.FLAG_SAVE %>" />     <%-- 임시저장 --%>
<c:set var="FLAG_COMPLETE" value="<%=CodeConstants.FLAG_COMPLETE %>" /> <%-- 등록완료 --%>
<c:set var="FLAG_INSERT"   value="<%=CodeConstants.FLAG_INSERT %>" />   <%-- 추가 --%>
<c:set var="FLAG_UPDATE"   value="<%=CodeConstants.FLAG_UPDATE %>" />   <%-- 수정 --%>
<c:set var="FLAG_DELETE"   value="<%=CodeConstants.FLAG_DELETE %>" />   <%-- 삭제 --%>
<c:set var="FLAG_APPR"     value="<%=CodeConstants.FLAG_APPR %>" />     <%-- 승인 --%>
<c:set var="FLAG_REJECT"   value="<%=CodeConstants.FLAG_REJECT %>" />   <%-- 반려 --%>
<c:set var="FLAG_AUTOAPPR" value="<%=CodeConstants.FLAG_AUTOAPPR %>" /> <%-- 자동결재 --%>

<!-- 파일 모듈 -->
<c:set var="FILE_MODULE_PROFILE"                value="<%=CodeConstants.FILE_MODULE_PROFILE %>" />                <%-- 프로필 사진 --%>
<c:set var="FILE_MODULE_EPMS_EQUIPMENT"         value="<%=CodeConstants.FILE_MODULE_EPMS_EQUIPMENT %>" />         <%-- 설비 사진 --%>
<c:set var="FILE_MODULE_EPMS_QRCODE"            value="<%=CodeConstants.FILE_MODULE_EPMS_QRCODE %>" />            <%-- 설비 QR코드 --%>
<c:set var="FILE_MODULE_EPMS_EQUIPMENT_MANUAL"  value="<%=CodeConstants.FILE_MODULE_EPMS_EQUIPMENT_MANUAL %>" />  <%-- 설비 매뉴얼 --%>
<c:set var="FILE_MODULE_EPMS_REPAIR_REQUEST"    value="<%=CodeConstants.FILE_MODULE_EPMS_REPAIR_REQUEST %>" />    <%-- 정비요청 --%>
<c:set var="FILE_MODULE_EPMS_REPPAIR_RESULT"    value="<%=CodeConstants.FILE_MODULE_EPMS_REPPAIR_RESULT %>" />    <%-- 정비실적 --%>
<c:set var="FILE_MODULE_EPMS_REPAIR_ANALYSIS"   value="<%=CodeConstants.FILE_MODULE_EPMS_REPAIR_ANALYSIS %>" />   <%-- 원인분석 --%>
<c:set var="FILE_MODULE_EPMS_ORDER"             value="<%=CodeConstants.FILE_MODULE_EPMS_ORDER %>" />             <%-- 오더 매뉴얼 --%>
<c:set var="FILE_MODULE_EPMS_PREVENTIVE_RESULT" value="<%=CodeConstants.FILE_MODULE_EPMS_PREVENTIVE_RESULT %>" /> <%-- 예방보전 실적 --%>
<c:set var="FILE_MODULE_EPMS_MATERIAL"          value="<%=CodeConstants.FILE_MODULE_EPMS_MATERIAL %>" />          <%-- 자재 사진 --%>

<!-- 파일 모듈 -->
<c:set var="CD_FILE_TYPE_ETC"   value="<%=CodeConstants.CD_FILE_TYPE_ETC %>" />   <%-- 기타 --%>
<c:set var="CD_FILE_TYPE_IMAGE" value="<%=CodeConstants.CD_FILE_TYPE_IMAGE %>" /> <%-- 이미지 --%>
<c:set var="CD_FILE_TYPE_VIDEO" value="<%=CodeConstants.CD_FILE_TYPE_VIDEO %>" /> <%-- 동영상 --%>
<c:set var="CD_FILE_TYPE_AUDIO" value="<%=CodeConstants.CD_FILE_TYPE_AUDIO %>" /> <%-- 음성 --%>

<!-- 사용자권한 (MEMBER : SUB_AUTH) -->
<c:set var="AUTH_COMMON"    value="<%=CodeConstants.AUTH_COMMON %>" />   <%-- 일반사용자 --%>
<c:set var="AUTH_CHIEF"     value="<%=CodeConstants.AUTH_CHIEF %>" />    <%-- 팀장 --%>
<c:set var="AUTH_REPAIR"    value="<%=CodeConstants.AUTH_REPAIR %>" />   <%-- 정비원 --%>

<!-- 사용자권한 (MEMBER : SUB_AUTH) -->
<c:set var="PART_01" value="<%=CodeConstants.PART_01 %>" /> <%-- 기계 --%>
<c:set var="PART_02" value="<%=CodeConstants.PART_02 %>" /> <%-- 전기 --%>
<c:set var="PART_03" value="<%=CodeConstants.PART_03 %>" /> <%-- 시설 --%>


<!-- 공장파트 구분 (COMCD : REMARKS) -->
<c:set var="REMARKS_SEPARATE"   value="<%=CodeConstants.REMARKS_SEPARATE %>" /> <%-- 파트구분 --%>
<c:set var="REMARKS_UNITED"     value="<%=CodeConstants.REMARKS_UNITED %>" />   <%-- 파트통합 --%>


<%-- 기기이력유형 (EPMS_EQUIPMENT_HISTORY : HISTORY_TYPE) --%>
<c:set var="EPMS_HISTORY_TYPE_REG"  value="<%=CodeConstants.EPMS_HISTORY_TYPE_REG %>" />  <%-- 설비등록 --%>
<c:set var="EPMS_HISTORY_TYPE_MOVE" value="<%=CodeConstants.EPMS_HISTORY_TYPE_MOVE %>" /> <%-- 라인이동 --%>
<c:set var="EPMS_HISTORY_TYPE_CM"   value="<%=CodeConstants.EPMS_HISTORY_TYPE_CM %>" />   <%-- 일반정비 --%>
<c:set var="EPMS_HISTORY_TYPE_PM"   value="<%=CodeConstants.EPMS_HISTORY_TYPE_PM %>" />   <%-- 예방정비 --%>
<c:set var="EPMS_HISTORY_TYPE_EM"   value="<%=CodeConstants.EPMS_HISTORY_TYPE_EM %>" />   <%-- 돌발정비 --%>
<c:set var="EPMS_HISTORY_TYPE_CBM"  value="<%=CodeConstants.EPMS_HISTORY_TYPE_CBM %>" />  <%-- CBM --%>
<c:set var="EPMS_HISTORY_TYPE_TBM"  value="<%=CodeConstants.EPMS_HISTORY_TYPE_TBM %>" />  <%-- TBM --%>

<%-- 작업유형 (EPMS_WORK_LOG : WORK_TYPE) --%>
<c:set var="EPMS_WORK_TYPE_REPAIR"     value="<%=CodeConstants.EPMS_WORK_TYPE_REPAIR %>" />     <%-- 정비실적 --%>
<c:set var="EPMS_WORK_TYPE_PREVENTIVE" value="<%=CodeConstants.EPMS_WORK_TYPE_PREVENTIVE %>" /> <%-- 예방보전실적 --%>
<c:set var="EPMS_WORK_TYPE_ANALYSIS"   value="<%=CodeConstants.EPMS_WORK_TYPE_ANALYSIS %>" />   <%-- 원인분석 --%>

<%-- 정비요청 요청유형 (EPMS_REPAIR_REQUEST : REQUEST_TYPE) --%>
<c:set var="EPMS_REQUEST_TYPE_COMMON"     value="<%=CodeConstants.EPMS_REQUEST_TYPE_COMMON %>" />     <%-- 일반 --%>
<c:set var="EPMS_REQUEST_TYPE_PREVENTIVE" value="<%=CodeConstants.EPMS_REQUEST_TYPE_PREVENTIVE %>" /> <%-- 예방정비 --%>
<c:set var="EPMS_REQUEST_TYPE_HELP"       value="<%=CodeConstants.EPMS_REQUEST_TYPE_HELP %>" />       <%-- 타파트 업무협조 --%>

<%-- 정비요청 결재상태 (EPMS_REPAIR_REQUEST : APPR_STATUS) --%>
<c:set var="EPMS_APPR_STATUS_WAIT"           value="<%=CodeConstants.EPMS_APPR_STATUS_WAIT %>" />           <%-- 대기 --%>
<c:set var="EPMS_APPR_STATUS_APPR"           value="<%=CodeConstants.EPMS_APPR_STATUS_APPR %>" />           <%-- 승인 --%>
<c:set var="EPMS_APPR_STATUS_REJECT"         value="<%=CodeConstants.EPMS_APPR_STATUS_REJECT %>" />         <%-- 반려 --%>
<c:set var="EPMS_APPR_STATUS_CANCEL_REPAIR"  value="<%=CodeConstants.EPMS_APPR_STATUS_CANCEL_REPAIR %>" />  <%-- 정비취소 --%>
<c:set var="EPMS_APPR_STATUS_CANCEL_REQUEST" value="<%=CodeConstants.EPMS_APPR_STATUS_CANCEL_REQUEST %>" /> <%-- 요청취소 --%>

<%-- 정비실적 정비상태 (EPMS_REPAIR_RESULT : REPAIR_STATUS) --%>
<c:set var="EPMS_REPAIR_STATUS_RECEIVE"  value="<%=CodeConstants.EPMS_REPAIR_STATUS_RECEIVE %>" />  <%-- 접수 --%>
<c:set var="EPMS_REPAIR_STATUS_PLAN"     value="<%=CodeConstants.EPMS_REPAIR_STATUS_PLAN %>" />     <%-- 배정완료 --%>
<c:set var="EPMS_REPAIR_STATUS_COMPLETE" value="<%=CodeConstants.EPMS_REPAIR_STATUS_COMPLETE %>" /> <%-- 정비완료 --%>
<c:set var="EPMS_REPAIR_STATUS_CANCEL"   value="<%=CodeConstants.EPMS_REPAIR_STATUS_CANCEL %>" />   <%-- 정비취소 --%>

<%-- 정비실적 정비유형 (EPMS_REPAIR_RESULT : REPAIR_TYPE) --%>
<c:set var="EPMS_REPAIR_TYPE_CM" value="<%=CodeConstants.EPMS_REPAIR_TYPE_CM %>" /> <%-- 일반정비 --%>
<c:set var="EPMS_REPAIR_TYPE_EM" value="<%=CodeConstants.EPMS_REPAIR_TYPE_EM %>" /> <%-- 돌발정비 --%>
<c:set var="EPMS_REPAIR_TYPE_PM" value="<%=CodeConstants.EPMS_REPAIR_TYPE_PM %>" /> <%-- 예방정비 --%>

<%-- 예방보전 업무구분 (EPMS_WORK : PREVENTIVE_TYPE) --%>
<c:set var="EPMS_PREVENTIVE_TYPE_REPAIR"      value="<%=CodeConstants.EPMS_PREVENTIVE_TYPE_REPAIR %>" />      <%-- 예방정비 --%>
<c:set var="EPMS_PREVENTIVE_TYPE_MAINTENANCE" value="<%=CodeConstants.EPMS_PREVENTIVE_TYPE_MAINTENANCE %>" /> <%-- 예방점검 --%>

<%-- 예방보전 점검구분 (EPMS_WORK : CHECK_TYPE) --%>
<c:set var="EPMS_CHECK_TYPE_CBM" value="<%=CodeConstants.EPMS_CHECK_TYPE_CBM %>" /> <%-- CBM --%>
<c:set var="EPMS_CHECK_TYPE_TBM" value="<%=CodeConstants.EPMS_CHECK_TYPE_TBM %>" /> <%-- TBM --%>

<%-- 예방보전 실적 상태 (EPMS_PREVENTIVE_RESULT : STATUS) --%>
<c:set var="EPMS_PREVENTIVE_STATUS_WAIT"     value="<%=CodeConstants.EPMS_PREVENTIVE_STATUS_WAIT %>" />     <%-- 대기 --%>
<c:set var="EPMS_PREVENTIVE_STATUS_PROBLEM"  value="<%=CodeConstants.EPMS_PREVENTIVE_STATUS_PROBLEM %>" />  <%-- 정비필요 --%>
<c:set var="EPMS_PREVENTIVE_STATUS_COMPLETE" value="<%=CodeConstants.EPMS_PREVENTIVE_STATUS_COMPLETE %>" /> <%-- 완료 --%>
<c:set var="EPMS_PREVENTIVE_STATUS_UNCHECK"  value="<%=CodeConstants.EPMS_PREVENTIVE_STATUS_UNCHECK %>" />  <%-- 미점검 --%>

<%-- 자재 입출고 유형 (EPMS_MATERIAL_INOUT : MATERIAL_INOUT_TYPE) --%>
<c:set var="EPMS_MATERIAL_INOUT_TYPE_IN"      value="<%=CodeConstants.EPMS_MATERIAL_INOUT_TYPE_IN %>" />      <%-- 입고(발주) --%>
<c:set var="EPMS_MATERIAL_INOUT_TYPE_OUT"     value="<%=CodeConstants.EPMS_MATERIAL_INOUT_TYPE_OUT %>" />     <%-- 출고 --%>
<c:set var="EPMS_MATERIAL_INOUT_TYPE_IN_OPT"  value="<%=CodeConstants.EPMS_MATERIAL_INOUT_TYPE_IN_OPT %>" />  <%-- 임의입고 --%>
<c:set var="EPMS_MATERIAL_INOUT_TYPE_OUT_OPT" value="<%=CodeConstants.EPMS_MATERIAL_INOUT_TYPE_OUT_OPT %>" /> <%-- 임의출고 --%>



<%-- 푸시메세지 유형) --%>
<c:set var="EPMS_PUSHMENUTYPE_REQUEST"        value="<%=CodeConstants.EPMS_PUSHMENUTYPE_REQUEST %>" />        <%-- 내요청 상세 --%>
<c:set var="EPMS_PUSHMENUTYPE_APPR"           value="<%=CodeConstants.EPMS_PUSHMENUTYPE_APPR %>" />           <%-- 결재 상세 --%>
<c:set var="EPMS_PUSHMENUTYPE_PLAN"           value="<%=CodeConstants.EPMS_PUSHMENUTYPE_PLAN %>" />           <%-- 계획및배정 상세 --%>
<c:set var="EPMS_PUSHMENUTYPE_REPAIR"         value="<%=CodeConstants.EPMS_PUSHMENUTYPE_REPAIR %>" />         <%-- 정비 상세 --%>
<c:set var="EPMS_PUSHMENUTYPE_REQUEST_CANCEL" value="<%=CodeConstants.EPMS_PUSHMENUTYPE_REQUEST_CANCEL %>" /> <%-- NULL(요청취소시) --%>

<!-- 에러코드 -->
<c:set var="SUCCESS"            value="<%=CodeConstants.SUCCESS %>" />          <%-- 성공 --%>
<c:set var="NO_DATA_FOUND"      value="<%=CodeConstants.NO_DATA_FOUND %>" />    <%-- 데이터가 존재하지 않습니다. --%>
<c:set var="FAILED_TO_SAVE"     value="<%=CodeConstants.FAILED_TO_SAVE %>" />   <%-- 데이터 저장에 실패하였습니다. --%>
<c:set var="FAILED_TO_DELETE"   value="<%=CodeConstants.FAILED_TO_DELETE %>" /> <%-- 데이터 삭제에 실패하였습니다. --%>
<c:set var="ALEADY_DATA"        value="<%=CodeConstants.ALEADY_DATA %>" />      <%-- 이미 등록된 데이터입니다. --%>
<c:set var="INVALID_DATA"       value="<%=CodeConstants.INVALID_DATA %>" />     <%-- 잘못된데이터입니다. --%>
<c:set var="INVALID_SQL"        value="<%=CodeConstants.INVALID_SQL %>" />      <%-- 잘못된 SQL문입니다. --%>
<c:set var="FAILED_TO_MODIFY"   value="<%=CodeConstants.FAILED_TO_MODIFY %>" /> <%-- 데이터 수정에 실패하였습니다. --%>
<c:set var="NO_ACCESS_RIGHT"    value="<%=CodeConstants.NO_ACCESS_RIGHT %>" />  <%-- 접근권한이 없습니다. --%>
<c:set var="INVALID_USER"       value="<%=CodeConstants.INVALID_USER %>" />     <%-- 사용자 정보가 일치하지 않습니다. --%>
<c:set var="INVALID_PASSWORD"   value="<%=CodeConstants.INVALID_PASSWORD %>" /> <%-- 비밀번호가 일치하지 않습니다. --%>
<c:set var="INVALID_DEVICE"     value="<%=CodeConstants.INVALID_DEVICE %>" />   <%-- 분실된 단말기입니다. 관리팀에 연락해 주세요. --%>
<c:set var="NO_REGIST_USER"     value="<%=CodeConstants.NO_REGIST_USER %>" />   <%-- 사용자 정보가 등록 되 있지 않습니다. 회원가입을 해주십시오. --%>
<c:set var="DROP_MEMBER"        value="<%=CodeConstants.DROP_MEMBER %>" />      <%-- 탈퇴한 회원입니다. --%>
<c:set var="DIS_SESSION"        value="<%=CodeConstants.DIS_SESSION %>" />      <%-- 로그인이 필요합니다. --%>
<c:set var="NO_AUTH"            value="<%=CodeConstants.NO_AUTH %>" />          <%-- 권한이 없습니다. --%>
<c:set var="NO_CONTENT"         value="<%=CodeConstants.NO_CONTENT %>" />       <%-- 게시물이 없습니다. --%>
<c:set var="SUCCESS_REG"        value="<%=CodeConstants.SUCCESS_REG %>" />      <%-- 등록 하였습니다. --%>
<c:set var="SUCCESS_UDT"        value="<%=CodeConstants.SUCCESS_UDT %>" />      <%-- 수정 하였습니다. --%>
<c:set var="SUCCESS_DEL"        value="<%=CodeConstants.SUCCESS_DEL %>" />      <%-- 삭제 하였습니다. --%>
<c:set var="CHECK_STATUS"       value="<%=CodeConstants.CHECK_STATUS %>" />     <%-- 상태 값을 확인해 주시기 바랍니다. --%>
<c:set var="PARAM_NULL"         value="<%=CodeConstants.PARAM_NULL %>" />       <%-- 수신 파라미터가 NULL 입니다. --%>
<c:set var="INVALID_METHOD"     value="<%=CodeConstants.INVALID_METHOD %>" />   <%-- 부적절한 Method 호출" --%>
<c:set var="INVALID_URL"        value="<%=CodeConstants.INVALID_URL %>" />      <%-- 잘못된 주소입니다. --%>
<c:set var="ERROR_ETC"          value="<%=CodeConstants.ERROR_ETC %>" />        <%-- 기타 오류 --%>
<c:set var="SYSTEM_ERROR"       value="<%=CodeConstants.SYSTEM_ERROR %>" />     <%-- 시스템 오류 발생. --%>

