<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Class Name : sessionCheck2.jsp
  Description : 세션이 끊어진경우 작업중인  모든 브라우저를 종료후 로그인화면으로 이동
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.07.06    이영탁              최초 생성

    author   : 이영탁
    since    : 2017.07.06
--%>

<script type="text/javascript">

if("${sessionScope.ssUserId}" != ""){

	try{opener.parent.parent.parent.document.location.href  = "<c:url value='/'/>"; self.close();}catch(e){}
	try{opener.parent.parent.document.location.href = "<c:url value='/'/>"; self.close();}catch(e){}
	try{opener.parent.document.location.href    = "<c:url value='/'/>"; self.close();}catch(e){}
	try{opener.document.location.href   = "<c:url value='/'/>"; self.close();}catch(e){}
	try{parent.parent.parent.document.location.href = "<c:url value='/'/>";}catch(e){}
	try{parent.parent.document.location.href    = "<c:url value='/'/>";}catch(e){}
	try{parent.document.location.href   = "<c:url value='/'/>";}catch(e){}
	try{document.location.href  = "<c:url value='/'/>";}catch(e){}
}else{
	
}	


</script>
