$(document).ready(function() {

var closingDate = $('#closingDate').val();
var closingApprDate = $('#closingApprDate').val();

//현금 경비 : 달력 날짜 클릭 시 선택일과 등록 마감일 비교
$('.inpCal').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			if(target.hasClass('todt')){
				var toDt = $('#toDate').val().replace(/-/gi, '');
				var choisDt = date.getFullYear() + '' + getDate(parseInt(date.getMonth())+1) + '' + getDate(date.getDate());
				
				if(Number(toDt) - Number(toDt.substring(0,6) +closingDate) > Number("0")){
					if(Number(choisDt.substring(0,6)) - Number(toDt.substring(0,6)) < Number("0")){
						alert("선택하신 날짜는 경비 작성일이 지나서 저장 및 상신을 할 수 없습니다.");
						target.val(toDt);
						return;
					}else{
						target.val(date.getFullYear() + '' + getDate(parseInt(date.getMonth())+1) + '' + getDate(date.getDate()));
					}
				}else{
					target.val(date.getFullYear() + '' + getDate(parseInt(date.getMonth())+1) + '' + getDate(date.getDate()));
				}
			}else{
				target.val(date.getFullYear() + '' + getDate(parseInt(date.getMonth())+1) + '' + getDate(date.getDate()));
			}
		}
	}).each(function(){
		if($(this).hasClass('thisMonth')){
			var date = new Date();
			$(this).val(date.getFullYear() + '' + getDate(parseInt(date.getMonth())+1) + '01');
		}else if($(this).hasClass('noneType') == false){
			var date = new Date();
			$(this).val(date.getFullYear() + '' + getDate(parseInt(date.getMonth())+1) + '' + getDate(date.getDate()));
		}
	});
});
