$(document).ready(function(){	
	// 달력 
	$('.openCal').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	}).each(function(){
		if($(this).hasClass('thisMonth')){
			var date = new Date();
			$(this).val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-01');
		}else if($(this).hasClass('noneType') == false){
			var date = new Date();
			$(this).val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	});
	
	// 숫자형만 입력(-입력 가능)
	$(document).on("keyup", ".onlyNumber", function() {
		$(this).val( $(this).val().replace(/[^-0-9.,]/gi,"") );
		$(this).val(number_format($(this).val()));
	});
	
	$("#searchGubun").change(function(){
		$("#searchGubunVal").val("");
	});

	$("#searchBunryu").change(function(){
		if($('#searchBunryu').val() != ""){
			Grids.GlAccountList.Source.Data.Url = "/hello/eacc/common/popGlAccountListData.do?bill_gb="+bill_gb+"&accgubun="+$("#ACCGUBUN").val()+"&account="+$('#searchBunryu').val();
			Grids.GlAccountList.ReloadBody();
		} 
	});

	bindEnterKey("#searchGubunVal", "#search");
});

function bindEnterKey(evtSrc, evtTar) {
	$(evtSrc).bind('keyup', (function(evt) {
		var e = evt ? evt : (window.event ? window.event : null);
		if (e.keyCode == 13) {
			$(evtTar).click();
		}
	}));
}

//구매처 검색 이벤트
function fn_searchLifnr(){
	
	var searchGubun = $("#searchGubun").val(); // 검색조건
	var searchVal = $("#searchGubunVal").val(); // 검색어
	
	if(searchGubun == ""){
		alert("조회 조건을 선택하세요.");
		$("#searchGubunVal").focus();
		return;
	}
	
	if(searchVal == ""){
		alert("검색어를 입력해주세요.");
		$("#searchGubun").focus();
		return;
	}
	
	var user_id = "";
	var card_num = "";
	var corporation_no = "";
	var name = "";
	
	if(searchGubun == "user_id"){
		user_id = searchVal;
	}else if(searchGubun == "card_num"){
		card_num = searchVal;
	}else if(searchGubun == "corporation_no"){
		corporation_no = searchVal;
	}else if(searchGubun == "name"){
		name = searchVal;
	}
	
	var parameters = "LIFNR="+user_id+"&STCD2="+corporation_no+"&CARDNO="+card_num+"&NAME="+name;
	var url = "/hello/eacc/common/popVendorListData.do?" + parameters;
	
	Grids.VendorList.Source.Data.Url = url;
	Grids.VendorList.ReloadBody();
}
// 구매처 세팅(구매처 조회팝업에서 호출)
function fnSetVendor(row){
	$("#PURCHASEOFFI_CD").val(row.LIFNR);	// 구매처코드
	$("#PURCHASEOFFI_NM").val(row.NAME1);	// 구매처명
	$("#BVTYP").val(row.BANKL);				// 은행코드
	$("#BANKA").val(row.BANKA);				// 은행명
	$("#PAYEE_CD").val(row.BANKA);			// 수취인코드
	$("#PAYEE_NM").val(row.BANKA);			// 수취인명
	$("#ACCOUNT_NO").val(row.BANKN);		// 계좌번호
	$("#ACCOUNT_HOLDER").val(row.KOINH);	// 예금주
	$("#BANK_CD").val(row.BANKL);			// 은행코드
	$("#BANK_NM").val(row.BANKA);
//	$("#PAY_TERM").val(row.PAY_TERM);		// 지급조건
//	$("#PAYDUE_DT").val(row.PAYDUE_DT);		// 지급예정일
	
	$("#td_banka").text(row.BANKA);		// 은행명
	$("#td_bankn").text(row.BANKN);		// 계좌번호
}

//사용자 조회
function getMemberList(divNo, curLvl, company_id){
	Grids.PopMemberDivList.Source.Data.Url = "/hello/eacc/common/popMemberDivData.do?divNo="+divNo+"&curLvl="+curLvl+"&COMPANY_ID="+company_id;
	Grids.PopMemberDivList.ReloadBody();	
}

function fn_addMember(gridNm){
//	<%--사원 추가 --%>
//	<%--레이어 팝업 구분자(추가직원-addPerson, 이관-transTaxPerson/transCardPerson)--%>
//	<%--이관대상자 : transGrid, transCol, transRow--%>
	var count = 0;
	var email = "";
	var userId = "";
	var name = "";
	var divisionNm = "";
	if(layerGubun == "transTaxPerson" || layerGubun == "transCardPerson"){
		for(var row = Grids[gridNm].GetFirst(); row; row = Grids[gridNm].GetNext(row)){
			if(row.SELECT_YN == '1'){
				email = row.EMAIL;
				userId = row.USER_ID;
				name = row.NAME;
				count ++;
			}
		}
		if(count > 1){
			alert("이관 대상자는 한 명만 선택 가능합니다.");
			return;
		}else{
			if(layerGubun == "transTaxPerson"){
				for(var i=0; i<transRow.length; i++){
					Grids[transGrid].SetValue(transRow[i], transCol, email, 1);
				}
				Grids.PopMemberDivList.ReloadBody();
				layer_open('memberModal');
			}else if(layerGubun == "transCardPerson"){
				for(var i=0; i<transRow.length; i++){
					Grids[transGrid].SetValue(transRow[i], transCol, userId, 1);
				}
				Grids.PopMemberDivList.ReloadBody();
				layer_open('memberModal');
			}
		}
	}else if(layerGubun == "addPerson"){
		for(var row = Grids[gridNm].GetFirst(); row; row = Grids[gridNm].GetNext(row)){
			if(row.SELECT_YN == '1'){
				count ++;
				if(userId == "" || userId == null){
					userId = row.USER_ID;
					name = row.NAME;
				}else{
					userId += ',' + row.USER_ID;
					name += ',' + row.NAME;
				}
			}
		}

		$('#addPrsnName').html('');
		
		var html = '<div class="inp-wrap readonly" style="width:100%;height:auto;margin-top:5px;">'
				 + '	<textarea cols="" rows="2" id="addPrsnNm" name="ADD_PRSN_NM" readonly="readonly" >'+ name + '(' + count + '명)</textarea>'
				 + '		<input type="hidden" name="ADD_PRSN" value="' + userId + '"/>'
				
				 + '</div>';
				 
		$('#addPrsnName').append(html);
		$('#prsnCnt').val(count);
		
		layer_open('memberModal')
	}else if(layerGubun == "apprPerson"){
		var isAddFlag = false;
		var msg = "";
		for(var row = Grids[gridNm].GetFirst(); row; row = Grids[gridNm].GetNext(row)){
			if(row.SELECT_YN == '1'){
				count ++;
				for(var r = Grids[apprGrid].GetFirstVisible(); r; r = Grids[apprGrid].GetNextVisible(r)){
					if(row.USER_ID == r.APPR_ID){
						if(msg == ""){
							msg += r.APPR_NM;
						}else{
							msg += ", " + r.APPR_NM;
						}
						isAddFlag = true;
					}		
				}
			}							
		}
		if(isAddFlag){
			alert(msg + "(은)는 이미 추가된 결재자 입니다.");
			return;
		}
		
		for(var row = Grids[gridNm].GetFirst(); row; row = Grids[gridNm].GetNext(row)){
			if(row.SELECT_YN == '1'){
				var rt = Grids[apprGrid].AddRow(null, Grids[apprGrid].GetFirst(), true);
				Grids[apprGrid].SetValue(rt, "COMPANY_ID", row.COMPANY_ID, 1);
				Grids[apprGrid].SetValue(rt, "APPR_ID", row.USER_ID, 1);
				Grids[apprGrid].SetValue(rt, "APPR_NM", row.NAME, 1);
				Grids[apprGrid].SetValue(rt, "APPR_DEPT_ID", row.DIVISION, 1);
				Grids[apprGrid].SetValue(rt, "APPR_DEPT_NM", row.DIVISION_NM, 1);
				Grids[apprGrid].SetValue(rt, "JOB_GRADE", row.JOB_GRADE, 1);
				count ++;
			}
		}
		if(!isAddFlag){
			fn_countApprSeq(apprGrid);
		}
		if(count == 0){
			alert("추가할 결재자를 선택하세요.");
			return;
		}
	}
}

/* 코스트센터 팝업 호출 */
var selectSeq = '';
function fn_popCostcenter(seq){
	selectSeq = seq;
	layer_open('costcenterModal');
	Grids.CostcenterList.Source.Data.Url = "/hello/eacc/common/popCostcenterListData.do";
	Grids.CostcenterList.ReloadBody();	
}

/* 코스트센터 세팅 */
function fnSetCostcenter(kostl, kokrs, ktext){
  	$(".kostl").eq(selectSeq).val(kostl);	//코스트센터 코드 세팅
  	$(".ktext").eq(selectSeq).val(ktext);	//코스트센터명 세팅
}

//GL계정 조회 팝업 호출
function fn_popGlAccount(bill_gb, seq){
	selectSeq = seq;
	
	// 계정 분류 콤보박스 set
	$.ajax({
		type: 'POST',
		url: '/hello/eacc/common/getAccountNmAjax.do',
		async : false, 
		dataType: 'json',
		data : {bill_gb : bill_gb, accgubun : $("#ACCGUBUN").val()},
		success: function (json) {
			var sb = new StringBuilder();
			$('#searchBunryu').html("");
			$.each(json, function(index, itemData){
				sb.append("<option value=" + itemData.ACCOUNT_NDX + ">" + itemData.ACCOUNT_NM + "</option>");
				draw = '<option value="">계정 분류 선택</option>' + sb.toString();
				$('#searchBunryu').html(draw);
			});
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert("계정 분류를 세팅하는 중 오류가 발생하였습니다.");
		}
	});
	
	layer_open('glaccountModal');
	Grids.GlAccountList.Source.Data.Url = "/hello/eacc/common/popGlAccountListData.do?bill_gb="+bill_gb+"&accgubun="+$("#ACCGUBUN").val();
	Grids.GlAccountList.ReloadBody();
}

//GL계정 코드 세팅
function fnSetGlAccount(row){
	
	// 세팅시 헤더 세금코드와 같은지 체크(2번째 행부터)
	var tax_cd = $("#TAX_CD").val();
	var tax_cmd = row.TAX_CMD;
	if(selectSeq >= 1 && tax_cmd == 'N'){
		if(tax_cd != row.TAX_CD){
			alert(row.GLNAME + " 계정은 " + tax_cd + " 세금코드를 사용할 수 없습니다.");
			return;
		}
	}
	
	// 기본 세금코드 설정
	var exists = false;
	$("#TAX_CD option").each(function(){
		if($(this).val() == row.TAX_CD){
			exists = true;
			return false;
		}
	});
	if(exists){
		$("#TAX_CD").val(row.TAX_CD);
		$("#TAX_CD").trigger("change");
	}
	
	$(".glCode").eq(selectSeq).val(row.GLCODE);	//GL계정 코드 세팅
	$(".glName").eq(selectSeq).val(row.GLNAME);	//GL계정명 세팅
	
	// 사용자 세금코드 변경 가능 여부 설정(Y-가능, N-불가)
	if(row.TAX_CMD == 'N'){
		$("#TAX_CD option").not(":selected").attr("disabled", true);
	}else{
		$("#TAX_CD option").not(":selected").attr("disabled", false);
	}
	
	// 계정 필수 입력 사항 세팅
	if(row.TAX_CMT != null && row.TAX_CMT != 'undefined'){
		$(".summary").eq(selectSeq).attr("placeholder", row.TAX_CMT);
	}else{
		$(".summary").eq(selectSeq).attr("placeholder", "");
	}
}

/* 오더번호 조회 팝업 호출 */
function fn_popInorder(seq){
	selectSeq = seq;
	layer_open('InorderModal');
	Grids.CostcenterList.Source.Data.Url = "/hello/eacc/common/popInorderListData.do";
	Grids.CostcenterList.ReloadBody();	
}

// 오더번호 세팅
function fnSetInorder(row){
	$(".aufnr").eq(selectSeq).val(row.AUFNR);		//오더번호 세팅
	$(".autxt").eq(selectSeq).val(row.AUTXT);		//오더명 세팅
	
	// 코스트센터 정보 삭제
	$(".kostl").eq(selectSeq).val("");
	$(".ktext").eq(selectSeq).val("");
}

/* 오더명 텍스트 삭제 */
function deleteText(seq){
	selectSeq = seq;
	$(".aufnr").eq(selectSeq).val('');		//오더번호
	$(".autxt").eq(selectSeq).val('');		//오더명
}

/* 콤마처리 */
function oneLimitCount(num){
	var num = num + '',
		oneLimitLnth = num.length,
		oneLimitTrd = oneLimitLnth % 3,
		oneLimitStr = 0;

	oneLimitStr = num.substring(0, oneLimitTrd);
	while(oneLimitTrd < oneLimitLnth){
		if(oneLimitStr != '') oneLimitStr += ',';
		oneLimitStr += num.substring(oneLimitTrd, oneLimitTrd + 3);
		oneLimitTrd += 3;
	}
	var minusComma= "-,";
	if(oneLimitStr.indexOf(minusComma) != -1){
		oneLimitStr = oneLimitStr.replace('-,','-');
	}
	oneLimit = oneLimitStr;
	return oneLimitStr;
}

//숫자 콤마로(oneLimitCount와 중복 확인할것)
function number_format(input) {
	input = input.replace(/,/g, "");

	var input = String(input);
	var reg = /(\-?\d+)(\d{3})($|\.\d+)/;
	if (reg.test(input)) {
		return input.replace(reg, function(str, p1, p2, p3) {
			return number_format(p1) + "," + p2 + "" + p3;
		});
	} else {
		return input;
	}
}

// 모달 파일 다운로드 폼 로드
function fn_openFileDownModal(attachGrpNo){
	var url = '/attach/modalFileDownForm.do?ATTACH_GRP_NO='+attachGrpNo;
	$("#fileDownModal").load(url, function(){
		$(this).modal();
	});
}



