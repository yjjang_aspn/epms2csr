/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	config.toolbarGroups = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		'/',
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		'/',
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'insert', groups: [ 'insert' ] }
//		'/',
//		{ name: 'tools', groups: [ 'tools' ] },
//		{ name: 'others', groups: [ 'others' ] },
//		{ name: 'about', groups: [ 'about' ] }
	];

	config.removeButtons = 'Maximize,ShowBlocks,About,Templates,Save,NewPage,Preview,Print,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CopyFormatting,NumberedList,BulletedList,Blockquote,CreateDiv,BidiLtr,Language,Anchor,Flash,Smiley,PageBreak,Iframe,BidiRtl';

	config.extraPlugins = 'table';
	config.extraPlugins = 'tabletools';
	config.extraPlugins = 'tableresize';
	config.extraPlugins = 'tableselection';
	config.extraPlugins = 'colordialog';
	config.extraPlugins = 'widget';
	config.extraPlugins = 'widgetselection';
	config.extraPlugins = 'lineutils';
	config.extraPlugins = 'dialog';
	config.extraPlugins = 'image2';
	config.extraPlugins = 'justify';
	config.extraPlugins = 'lineheight';
	config.extraPlugins = 'colorbutton';
	config.extraPlugins = 'panelbutton';
	config.extraPlugins = 'font';
	config.extraPlugins = 'filetools';
	config.extraPlugins = 'notification';
	config.extraPlugins = 'notificationaggregator';
	config.extraPlugins = 'uploadwidget';
	config.extraPlugins = 'uploadimage';
	config.extraPlugins = 'pastebase64';
	
	config.line_height="1em;2em;3em;4em";
	config.font_defaultLabel = '맑은 고딕';	
	config.font_names = '맑은 고딕; 돋움; 굴림; 궁서; 바탕;' +  CKEDITOR.config.font_names;
	config.fontSize_defaultLabel = '10pt';
	config.fontSize_sizes='8px/8px;8pt/8pt;9px/9px;9pt/9pt;10px/10px;10pt/10pt;11px/11px;11pt/11pt;12px/12px;12pt/12pt;13px/13px;13pt/13pt;14px/14px;14pt/14pt;15px/15px;15pt/15pt;16px/16px;16pt/16pt;18px/18px;18pt/18pt;20px/20px;20pt/20pt;22px/22px;/22pt/22pt;24px/24px;24pt/24pt;26px/26px;26pt/26pt;28px/28px;28pt/28pt;36px/36px;36pt/36pt;48px/48px;48pt/48pt;72px/72px;72pt/72pt;';
	config.language = 'ko';							//언어설정
	config.uiColor = '#EEEEEE';						//ui 색상
	config.height = '550px';						//Editor 높이 
	
	//config.enterMode =CKEDITOR.ENTER_BR;			//엔터키 입력시 br 태그 변경
	//config.shiftEnterMode =CKEDITOR.ENTER_BR;		//엔터키 입력시 br 태그 변경
	config.startupFocus = true;						// 시작시 포커스 설정
	config.allowedContent = true;					// 기본적인 html이 필터링으로 지워지는데 필터링을 하지 않는다.
	config.toolbarCanCollapse = true;				//툴바가 접히는 기능을 넣을때 사용합니다.
	config.fileEmptyBlocks = false;         		//공백 처리 설정
	config.disallowedContent = "table[cellspacing,cellpadding,border,align,summary,bgcolor,frame,rules,width]; td[axis,abbr,scope,align,bgcolor,char,charoff,height,nowrap,valign,width]; th[axis,abbr,align,bgcolor,char,charoff,height,nowrap,valign,width]; tbody[align,char,charoff,valign]; tfoot[align,char,charoff,valign]; thead[align,char,charoff,valign]; tr[align,bgcolor,char,charoff,valign]; col[align,char,charoff,valign,width]; colgroup[align,char,charoff,valign,width]";
	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	//config.removeButtons = 'Underline,Subscript,Superscript';

	// Set the most common block elements.
	//config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	//config.removeDialogTabs = 'image:advanced;link:advanced';
};
