/****************************************************************
 * 
 * 파일명 : common.js
 * 설  명 : 공통 JavaScript
 * 
 *    수정일      수정자     Version        Function 명
 * ------------    ---------   -------------  ----------------------------
 * 2017.07.04    이영탁      1.0             최초생성
 *
 */

// 창열기  
function openWin( winName ) {  
   var blnCookie    = getCookie( winName );  
   var obj = eval( "window." + winName ); 
   
   if( !blnCookie && obj != null ) {  
	   obj.style.display = "block";  
   }  
}  
  
// 창닫기  
function closeWin(winName, expiredays) {   
   setCookie( winName, "done" , expiredays);   
   var obj = eval( "window." + winName );  
   obj.style.display = "none";  
}  
function closeWinAt00(winName, expiredays) {   
   setCookieAt00( winName, "done" , expiredays);   
   var obj = eval( "window." + winName );  
   obj.style.display = "none";  
}  
  
// 쿠키 가져오기  
function getCookie( name ) {  
   var nameOfCookie = name + "=";  
   var x = 0;  
   while ( x <= document.cookie.length )  
   {  
	   var y = (x+nameOfCookie.length);  
	   if ( document.cookie.substring( x, y ) == nameOfCookie ) {  
		   if ( (endOfCookie=document.cookie.indexOf( ";", y )) == -1 )  
			   endOfCookie = document.cookie.length;  
		   return unescape( document.cookie.substring( y, endOfCookie ) );  
	   }  
	   x = document.cookie.indexOf( " ", x ) + 1;  
	   if ( x == 0 )  
		   break;  
   }  
   return "";  
}  
  
// 24시간 기준 쿠키 설정하기  
// expiredays 후의 클릭한 시간까지 쿠키 설정  
function setCookie( name, value, expiredays ) {   
   var todayDate = new Date();   
   todayDate.setDate( todayDate.getDate() + expiredays );   
   document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";"   
}  
  
// 00:00 시 기준 쿠키 설정하기  
// expiredays 의 새벽  00:00:00 까지 쿠키 설정  
function setCookieAt00( name, value, expiredays ) {   
	var todayDate = new Date();   
	todayDate = new Date(parseInt(todayDate.getTime() / 86400000) * 86400000 + 54000000);  
	if ( todayDate > new Date() )  
	{  
	expiredays = expiredays - 1;  
	}  
	todayDate.setDate( todayDate.getDate() + expiredays );   
	 document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";"   
  }  
  
$(function() {
	$( "#noticePop, #profilePop" ).draggable({cursor: "move"});
	
	// 공지사항
	var $notiSlides = $('.noti-slider').find('li');
	var $notiView = $('.noti-slider');
	var notiCurrent = 0;
	var $notiContList = $('.noti-cont-list');
	var $notiControls = $notiContList.find('li');

	$notiSlides.each(function (i){
		$(this).css({transform: 'translate(' + i*100 + '%, 0%)'});
	});
	function move(pos){
    $notiControls.removeClass('active').eq(pos).addClass('active');
		$notiView.animate({  view: (100*pos) }, {
			step: function(now) {
			$(this).css('transform','translate(-' + now + '%, 0%)');  
		},
			duration:'middle'
		},'linear');
	}
	$notiControls.on('click',function (e){
		e.preventDefault();
		if($notiView.is(':animated')){return  false;}
		notiCurrent = $(this).index();
		move(notiCurrent);
  }); 
});