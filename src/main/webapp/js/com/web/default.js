
//새로고침시 iframe만 새로고침 됨 
document.onkeydown = trapRefresh;
function trapRefresh(){
	
	var keyCode = window.keyCode ? window.keyCode : window.event.which ? window.event.which : window.event.charCode;
	//key막음 
	if(keyCode == 45 || /* insert*/
		keyCode == 46 || /* delete*/
		keyCode == 36 || /* home*/
		keyCode == 35 || /* end*/
		keyCode == 33 || /* page up*/
		keyCode == 34 /* page down */){
		event.stopImmediatePropagation();
	} 
	 
	if(keyCode==116){ //F5- iframe새로고침
		keyCode=0;
		event.cancelBubble=true; 
		event.returnValue=false;
		
		if((window.location.href).indexOf("main.do")>-1 ){
			mainReload();
		}else if((window.location.href).indexOf("leftMenuBar.do")>-1){
			parent.mainReload();
		}else{
			window.location.reload();
		}
		return false;
	}
	if(keyCode==118){ //F7-주소 alert 및 console로그 출력
		keyCode=0;
		event.cancelBubble=true; 
		event.returnValue=false;
		if((window.location.href).indexOf("main.do")>-1 ){
			alert(document.mainIfrm.location.href);
			console.log(document.mainIfrm.location.href);
		}else if((window.location.href).indexOf("leftMenuBar.do")>-1){
			alert(parent.mainIfrm.location.href);
			console.log(parent.mainIfrm.location.href);
		}else{
			alert(document.location.href);
			console.log(document.location.href);
		}
	}
}

var fileVal = "";

function setCookie(name, value, expires)
{
    document.cookie = name + "=" + escape(value) + "; path=/; expires=" + expires.toGMTString();
}

function getCookie(Name)
{
    var search = Name + "="

    if( document.cookie.length > 0 ) 
    {
        offset = document.cookie.indexOf(search)
    
        if (offset != -1) 
        {
            offset += search.length

            end = document.cookie.indexOf(";", offset)

            if (end == -1)
                end = document.cookie.length
            return unescape(document.cookie.substring(offset, end))
        }
    }
    
    return "";
}

//date
function getDate(num){
    return num < 10 ? num = '0' + num : num;
}

var inputGridId;
var inputRow;
var inputCodeCol;
var fileNm = "file";


function uploadFilePop(grid, row, col, module, width, height, file){

	inputGridId = grid.id;
	inputRow = row;
	inputCodeCol = col;
	fileNm = file;

	var attachGrpNo =grid.GetValue(inputRow, inputCodeCol);
	fn_openPop('/sys/attach/admin/fileEditForm.do', '?ATTACH_GRP_NO='+ attachGrpNo + '&MODULE='+module, width, height);
}

function uploadFileCallback(value){

	//반환된 ATTACH_GRP_NO를 row 해당 그리드 선택한 row에 설정
	var grid = Grids[inputGridId];
	grid.SetValue(inputRow, inputCodeCol, value, true);
	grid.SetValue(inputRow, "fileIcon","/sys/images/web/download.gif");
	grid.RefreshCell(inputRow,fileNm);
	
	//행의 추가 여부를 판단 하여 분기
	if(inputRow.Added != 1){
		grid.ActionSave();
		window.location.reload();
	} else {
		alert('파일이 등록 되었습니다. 저장 버튼을 클릭하시길 바랍니다.');
	}
}

function fn_openPop(popUrl, param, w, h){
	if(x == ""){
		x = 400;
		h = 450;
	}
	
	var width = screen.width;
	var height = screen.height;

	var x = (width/2)-(w/2);
	var y = (height/2)-(h/2);

	var opt = "left=" + x + ", top=" + y + ", width=" + w + ", height=" + h;
		opt = opt + ", toolbar=no,location=no,directories=no,status=no,menubar=no,addressbar=no";
		opt = opt + ",scrollbars=N";
		opt = opt + ",resizable=N";
		
	var win = window.open(popUrl+param, "_blank", opt);
	
	if(win == null){
		alert("팝업차단 기능이 실행되고있습니다.\n차단 해제후 다시 실행해 주시기 바랍니다.");
		return;
	}else{
		win.focus();		
	}
}

function fn_openPopPage(popUrl, param, w, h){
	if(x == ""){
		x = 400;
		h = 450;
	}
	
	var width = screen.width;
	var height = screen.height;

	var x = (width/2)-(w/2);
	var y = (height/2)-(h/2);
	if(win !=null){
		win.close();
		win = null;
	}
	var opt = "left=" + x + ", top=" + y + ", width=" + w + ", height=" + h;
		opt = opt + ", toolbar=no,location=no,directories=no,status=no,menubar=no";
		opt = opt + ",scrollbars=N";
		opt = opt + ",resizable=N";
		
	var win = window.open(popUrl+param, "win", opt);
	
	if(win == null){
		alert("팝업차단 기능이 실행되고있습니다.\n차단 해제후 다시 실행해 주시기 바랍니다.");
		return;
	}else{
		win.focus();	
		return false;
	}
}

/**
 * 팝업 호출 (네임 값을 받아 중복 호출 방지)
 * @param popUrl
 * @param name
 * @param w
 * @param h
 */
function fn_openPopName(popUrl, name, w, h){
	if(x == ""){
		x = 400;
		h = 450;
	}
	
	var width = screen.width;
	var height = screen.height;

	var x = (width/2)-(w/2);
	var y = (height/2)-(h/2);

	var opt = "left=" + x + ", top=" + y + ", width=" + w + ", height=" + h;
		opt = opt + ", toolbar=no,location=no,directories=no,status=no,menubar=no,addressbar=no";
		opt = opt + ",scrollbars=N";
		opt = opt + ",resizable=N";
		
	var win = window.open(popUrl, name , opt);
	
	if(win == null){
		alert("팝업차단 기능이 실행되고있습니다.\n차단 해제후 다시 실행해 주시기 바랍니다.");
		return;
	}else{
		win.focus();		
	}
}

/**
 * 팝업 네임값 + 좌표 받아서 처리
 * @param popUrl
 * @param name
 * @param w
 * @param h
 * @param x
 * @param y
 */
function fn_openPopNamePos(popUrl, name, w, h, x, y){
	
	var opt = "left=" + x + ", top=" + y + ", width=" + w + ", height=" + h;
	opt = opt + ", toolbar=no,location=no,directories=no,status=no,menubar=no,addressbar=no";
	opt = opt + ",scrollbars=N";
	opt = opt + ",resizable=N";
	
	var win = window.open(popUrl, name , opt);
	
	if(win == null){
		alert("팝업차단 기능이 실행되고있습니다.\n차단 해제후 다시 실행해 주시기 바랍니다.");
		return;
	}else{
		win.focus();		
	}
}

/* jquery Null 체크 */
function isNull_J(checkItem, strMessage) {
	if (checkItem.val() == "") {
		alert(strMessage);
		checkItem.focus();
		return false;
	}
	return true;
}


/* 시작일 종료일 체크 */
function dateCompare(startDate, endDate, strMessage) {
	
	//시작일 종료일 비교위해 / 제거 
	var startNoPoint = startDate.val().replace(/\//gi,'');
	var endNoPoint = endDate.val().replace(/\//gi,'');
	
	// 시작일 종료일 비교
	if(startNoPoint > endNoPoint) {
		alert(strMessage);
		startDate.focus();
		return false;
	}
	
	return true;
	
}

function fnSubmit(frmNm, action){
	$('#'+frmNm).attr("action", action);
	$('#'+frmNm).submit();
}

function reloadGrid(name){
	var grid;
	var gridRows=-1;

	if(name.length<1){
		grid = Grids[0];
	}else{
		grid = Grids[name];
	}

	gridRows = grid.RowCount;
	grid.ReloadBody();
}

function StringBuilder(value) {
	this.strings = new Array("");
	this.append(value);
}
StringBuilder.prototype.append = function (value){
	if (value)	{
		this.strings.push(value);
	}
};
StringBuilder.prototype.clear = function (){
	this.strings.length = 1;
};
StringBuilder.prototype.toString = function (){
	return this.strings.join("");
};

// 그리드 저장
function saveGrid(gridNm, ment){

	if(ment == "" || ment == undefined){
		ment = "저장하시겠습니까?";
	}
	
	if(Grids[gridNm].ActionValidate()){
 
		if(confirm(ment)){
			Grids[gridNm].ActionSave();
		}else{ 
			Grids[gridNm].ReloadBody();
		}
		
	}
} 

/*Grids.OnAfterSave = function(grid,source){
	if(grid.IO.Result != 0)	return;

	if(grid.Source.Data.Url == undefined){
		grid.Reload();
	}else{
		grid.ReloadBody();
	}
} */

function exportGridAction(gridNm, type){
	if(type.toUpperCase() == 'PDF'){
		Grids[gridNm].ActionExportPDF();
	}else{
		Grids[gridNm].Source.Export.Type=type;
		Grids[gridNm].ActionExport();
	}

	return;
}

function fn_openTarget(gridNm){
	Grids[gridNm].ReloadBody();
}

//이미지 파일 Seq번호
//var imgFileVal = "";
//
//문서 파일 Seq번호
//var docFileVal = "";

function delModiFile(obj){
	var attach = obj.data("attach");
	var idx = obj.data('idx');
	
	//처리내역 번호
	var response = obj.data('response');
	
	if(response == null || response == ""){
		delFileIcon(attach, idx, obj.closest(".fileList").children().prop('id'));
	} else {
		
		//처리내역 번호 설정
		delFileIcon(attach, idx, response);
	}

	//이미지 파일일 경우
	if(idx == 1){
		
		if($('#DEL_IMG_SEQ').val() != '') {
			attach = "," + attach;
		}
		
		$('#DEL_IMG_SEQ').val($('#DEL_IMG_SEQ').val() + attach);
		
	} 
	//문서 파일일 경우
	else if(idx == 2) {
		
		if($('#DEL_DOC_SEQ').val() != '') {
			attach = "," + attach;
		}
		
		$('#DEL_DOC_SEQ').val($('#DEL_DOC_SEQ').val() + attach);
		
	} 
	//공통의 경우
	else {
		
		if(fileVal != '') {
			fileVal += ",";
		}
		
		fileVal += attach;
		
		$('#DEL_SEQ').val(fileVal);
	}
}


function delModiFile(obj, maxFileCnt){
	var attach = obj.data("attach");
	var idx = obj.data('idx');
	
	delFileIcons(attach, idx, obj.closest(".fileList").children().prop('id'), maxFileCnt);
	
	//fileNo set
	if(fileVal != '') {
		fileVal += ",";
	}
	
	fileVal += attach;
	
	$('#DEL_SEQ').val(fileVal);
}

function delFileIcons(attach, idx, currentId, maxFileCnt){
	
	$('#fileLstWrap_'+attach).remove();

	if($("#"+currentId).children().length < maxFileCnt){
		$("input[name=fileList]").eq(idx).attr("disabled", false);
	}
}

function delFileIcon(attach, idx, currentId){
	
	//idx에 따라 이미지 파일, 문서파일을 구분(이미지: 1, 문서 : 2)
	if(currentId == null || currentId == "" || currentId == "undefined"){
		$('#fileLstWrap_'+attach+idx).remove();
	} else {
		//처리내역 첨부파일 toggle처리
		$('#fileLstWrap_'+attach+idx+currentId).toggle();
	}
	
	if (typeof(maxFileCnt) != "undefined"){
		
		if($("#"+currentId).children().length < maxFileCnt){
			$("input[name=fileList]").eq(idx).attr("disabled", false);
		}
	}
	
}

// 프린터
function printGrid(gridNm){
	Grids[gridNm].ActionPrint();

	return;
}

function exportGrid(gridNm, type){
	if(type.toUpperCase() == 'PDF'){
		Grids[gridNm].ActionExportPDF();
	}else{
		Grids[gridNm].Source.Export.Type=type;
		Grids[gridNm].ActionExport();
	}

	return;
}
/**
 * 그리드의 유효성 검사를 한다.
 * []LayOut.jsp 파일에서 <Solid/> 태그에서 "name" 속성이 "validation" 값이 있어야 한다.
 * 
 * --유효성 rule--
 * required; -> 필수,maxLength=3; -> 최대길이값, minLength=3; -> 최소길이값, 
 * 
 * 2. 날짜 체크 ex)workPlaceMgtGridLayout.jsp
 * 1) 날짜 체크할 변수는 Cells에 'date_'를 넣어 줘야 한다.
 *    특수 칼럼은 Solid태그에 값이 NaN으로 나오는 문제가 있음
 *    ex) date_bedata
 * 2)date=yyyy.mm.dd -> 날짜 형식,
 * 3)range=endDate -> 비교 대상 칼럼명
 * 
 * 3. 숫자 체크
 * 1) 날짜 체크할 변수는 Cells에 'num_'를 넣어 줘야 한다. ex) num_bedata
 * 2) maxValue=10; -> 최대값 정수및 실수, minValue=5;-> 최소값 정수및 실수
 *  
 * @param grid 그리드 객체
 * @return bool 
 */
function validateGrid(grid){
	//1. 유효성 체크용 데이타 로드
	var header = grid.Header; //헤더 정보 
	
	var solid = grid.Solid; //<SOLID>
	var validationNoData = solid.firstChild;// id = noData
	
	var validatioName = null;
	var targetCells = null; //유효성 체크 대상 변수
	
	
	var validationRow = null 
    var findValidationRow = validationNoData.nextSibling;// <i name=validatinon ..>
	//validation 용 row를 검색한다.
	while(findValidationRow != null){
    	
    	try{
    		
    		validatioName = findValidationRow["id"];
    		
    	}catch(err) {
    		//alert(err);
    	}
    	
    	if(validatioName == 'validation'){
    		validationRow = findValidationRow
    		break;
    	}
    	findValidationRow = findValidationRow.nextSibling; //다음 row검색
    }
	
	if(validationRow == null){
		alert("유효성 검사 규칙이 없습니다.");
		return false;
	}
	
	targetCells = validationRow.Cells; // string[] : [a,b,c,d]
	//validationRow[targetCells[0]];
	
	//2. 체크 대상 데이타 로드
	var body = grid.Body; //<body>
	var btag = body.firstChild; //<B> 태그
	var trow = btag.firstChild; //<I> 태그 (data 영역) : TRow, 첫번째 체크 대상
	
	//3. trow를 가지고 순회 하며 유효성 검사를 수행한다.	
	while(trow != null){
		
		for(var idx =0; idx < targetCells.length; idx++){
			
			var targetName  = null;
			var validtionRoleColName = null;
			var checkData = null;
			
			if(targetCells[idx].indexOf('_') >= 0){ //date, number 인경우 칼럼명에  date_, num_이 붙어야 한다.
				
				var arrayColumn = targetCells[idx].split('_');
				//console.log('arrayColumn ' + arrayColumn);
				targetName  = arrayColumn[1]; //유효성 체크 지정 칼럼	
			}
			else{
				targetName =targetCells[idx];
			}
			
			validtionRoleColName = targetCells[idx];
			checkData = trow[targetName];
			
			
			var validationRule = validationRow[validtionRoleColName]; //체크 룰 :validationRow["a"] => required;
			
			//유효성 체크 용 함수를 호출 한다.
			if(!validationCheck(checkData, targetName, validationRule, header, trow)){
				
				grid.Focus(trow,targetName); //trow, 칼럼이름
				
				return false;
			}
		}
		trow = trow.nextSibling; //다음 row로 넘어간다.
	}
	return true;
}
/**
 * 유효성 체크 를 수행하는 함수
 * 
 * @param checkData 체크 대상 그리드 data
 * @param targetName 체크 대상 칼럼명
 * @param validationRule 체크 규칙 ex) required;maxValue=10;maxLength=10;date=yyyy.mm.dd
 * @param header 헤더 row정보 칼럼 명을 출력하기 위한 정보
 * @param trow 범위 체크를 하기 위해  row 객체를 파라미터로 전달 ex) trow[sdate] > trow[edate] 리턴하기 위해 참고: workPlaceMgtGridLayout.jsp
 * @return bool
 */

function validationCheck(checkData, targetName, validationRule,header, trow){
	if(validationRule == null){
		alert("유효성 규칙이 지정되지 않았습니다. 컬럼 명:"+targetName);
	}
	var rules=validationRule.split(";"); //[required,maxLength=5]
	//alert("rules=>"+rules);
	
	//rules 만큼 순회하며 checkData를 검증한다.
	for(var idx = 0 ; idx < rules.length ; idx++){
		rule = rules[idx].split("=");//[required] or [maxLength,5]
		//alert("rule=>"+rules[idx]);
		//alert("checkData=> "+checkData+"_체크룰=>"+rule[0]);
		switch(rule[0]){
		
			case 'required':
				if(typeof checkData != "number" && (checkData == null || checkData == "")){
					alert(header[targetName]+"는(은) 필수 입력입니다.");
					return false;
				}
				break;
				
			case 'maxValue':
				
				if(checkData == null || checkData == "") continue; //빈칸이면 다음 validation수행
				
				if(typeof checkData != "number"){
					alert(header[targetName]+"의 최대값 비교는 숫자만 가능합니다.");
					return false;
				}
				
				if( parseFloat(checkData) > parseFloat(rule[1])){
					alert(header[targetName]+"은 값"+rule[1]+"을 초과 할수 없습니다.");
					return false;
				}
				break;
				
			case 'minValue':
				
				if(checkData == null || checkData == "") continue; //빈칸이면 다음 validation수행
				
				if(typeof checkData != "number"){
					alert(header[targetName]+"의 최소값 비교는 숫자만 가능합니다.");
					return false;
				}
				
				if( parseFloat(checkData) < parseFloat(rule[1])){
					alert(header[targetName]+"은 최소"+rule[1]+"이상 입력하셔야 합니다.");
					return false;
				}
				break;
				
			case 'maxLength':
				
				var checkDataTarget = checkData;
				
				if(checkData == null || checkData == "") continue; //빈칸이면 다음 validation수행
				if(typeof checkData != "string"){
					checkDataTarget = new String(checkData);
				}
				
				if( checkDataTarget.length > parseInt(rule[1])){
					alert(header[targetName]+"의 길이는 "+rule[1]+"을 초과 할수 없습니다.");
					return false;
				}
				break;
				
			case 'minLength':
				var checkDataTarget = checkData;
				
				if(checkData == null || checkData == "") continue; //빈칸이면 다음 validation수행
				if(typeof checkData != "string"){
					checkDataTarget = new String(checkData);
				}
				
				if( checkDataTarget.length < parseInt(rule[1])){
					alert(header[targetName]+"의길이는 "+rule[1]+" 보다 길어야 합니다.");
					return false;
				}
				break;
				
			case 'range':
				var endDate = rule[1];
				var arrayColumn = endDate.split('_');
				
				rangeEndName  = arrayColumn[1]	
				if(trow[targetName] >  trow[rangeEndName]){
					alert(header[targetName]+"은 "+header[rangeEndName]+"보다 클수 없습니다.");
					return false;
				}
				break;
				
			default:
				continue; //체크 할 것이 없으면 순회를 계속한다.
		
		}
		

		
	}
	return true;
	
}

$(document).ready(function(){
	//calendar
	/*
	$('.datePic').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '' + getDate(parseInt(date.getMonth())+1) + '' + getDate(date.getDate()));
		}
	}).each(function(){
		if($(this).hasClass('thisMonth')){
			var date = new Date();
			$(this).val(date.getFullYear() + '' + getDate(parseInt(date.getMonth())+1) + '01');
		}else if($(this).hasClass('noneType') == false){
			var date = new Date();
			$(this).val(date.getFullYear() + '' + getDate(parseInt(date.getMonth())+1) + '' + getDate(date.getDate()));
		}
	});
	*/
	
});


function fnAjaxSubCateList(cateVal, subVal){
	var id = $("#frm").find('#CATEGORY');
	var selected = "";
	
	$.ajax({
        url: "/sys/category/readSubCateListAjax.do"
        ,datatype: "json"
        ,data: { TREE:cateVal }
        ,success: function(data) {
            if(data != "") {
            	var html = "";
            	var sb = new StringBuilder();
            	$(id).html('');
            	console.dir(data);
            	$.each(data, function(index, itemData) {
            		if(subVal == itemData.category){
            			selected = "selected";
            		}else{
            			selected = "";
            		}
            		
            		sb.append("<option value=" + itemData.category + " "+selected+">" + itemData.name + "</option>");
            		$(id).html( sb.toString());
            	});
            	
            } else {
            	$(id).html('');
            	$(id).prepend("<option value=''>선택</option>");
            }
        }
    }); 
}

function fnGetImgNo(module){
	$.ajax({
		type: 'POST',
		url: '/sys/attach/getFileNo.do',
		dataType: 'json',
		success: function (data) {
			if(data != '' && data != null) {
				console.dir(data);
				$('#ATTACH_GRP_NO').val(data);
				
				location.href = "aspn://photoSubmit?ATTACH_GRP_NO="+data+"&MODULE="+module+"&DEL_SEQ=0&actionUrl=/sys/attach/AppEditFile.do";
			}
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('데이터 로딩 중 오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
		}
	});
}

function fnAdminGetImgNo(module){
	$.ajax({
		type: 'POST',
		url: '/sys/attach/getFileNo.do',
		dataType: 'json',
		success: function (data) {
			if(data != '' && data != null) {
				console.dir(data);
				$('#ATTACH_GRP_NO').val(data);
				
				fuSubmitFrm();
			}
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('데이터 로딩 중 오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
		}
	});
}

/********************
 * 함수명: fn_amtMask
 * 인자값: price
 * 설 명: 금액 , 처리
 ********************/
function fn_amtMask(num) {
	var num       = num + '';

	var amtStr    = 0;
	var amtLength = num.length;
	var amtThird  = amtLength % 3;

	amtStr = num.substring(0, amtThird);

	while(amtThird < amtLength) {
		if(amtStr != '') {
			amtStr += ',';
		}

		amtStr += num.substring(amtThird, amtThird + 3);
		amtThird += 3;
	}

	return amtStr;
}


//그리드 순번 설정
function setSeq(grid){
	var i = 1;
	for(var r = grid.GetFirst(); r; r = grid.GetNext(r)){
		grid.SetValue(r, "seq", i++, 1);
	}
}

// 주문 첨부파일 삭제
function delModiFile1(obj){
	var attach = obj.data("attach");
	var idx = obj.data('idx');
	
	//처리내역 번호
	var response = obj.data('response');
	
	if(response == null || response == ""){
		delFileIcon(attach, idx, obj.closest(".imgFileList").children().prop('id'));
	} else {
		//처리내역 번호 설정
		delFileIcon(attach, idx, response);
	}

	//이미지 파일일 경우
	if(idx == 1){
		
		if($('#DEL_IMG_SEQ').val() != '') {
			attach = "," + attach;
		}
		
		$('#DEL_IMG_SEQ').val($('#DEL_IMG_SEQ').val() + attach);
		
	} 
	//문서 파일일 경우
	else if(idx == 2) {
		
		if($('#DEL_DOC_SEQ').val() != '') {
			attach = "," + attach;
		}
		
		$('#DEL_DOC_SEQ').val($('#DEL_DOC_SEQ').val() + attach);
		
	} 
	//공통의 경우
	else {
		
		if(fileVal != '') {
			fileVal += ",";
		}
		
		fileVal += attach;
		
		$('#DEL_SEQ').val(fileVal);
	}
}


//로딩 이미지 출력
function callLoadingBar() {

	$.blockUI({ 
		message: $("#displayLoadingImgBox"), 
		css: { 
				top:  ($(window).height()) /2 + 'px', 
				left: ($(window).width()) /2 + 'px', 
				width: '130px',
				background: 'transparent',
				color: '#000000', 
				border: 'none' //테두리 없앰
			} 
		}); 
}

$(document).ajaxStart(function() {
	callLoadingBar();
});

$(document).ajaxStop(function() {
	setTimeout($.unblockUI, 100); //※로딩이 짧을시 점멸현상을 막기위해 약간 인터벌을 둔다
});

$(function () {
	//※ 로딩바 이미지를 Body 태그에 자동 추가
	$("body").append("<img id=\"displayLoadingImgBox\" src=\"/images/com/web/ajax-loader.gif\" width=\"60px;\" height=\"60px;\" style=\"display:none;\" />");
});


// 문자열이 빈 문자열인지 체크하여 결과값을 리턴한다
function isEmpty(str){
    if(typeof str == "undefined" || str == null || str == "")
        return true;
    else
        return false;
}

/* 콤마처리 */
function oneLimitCount(num){
	var num = num + '',
		oneLimitLnth = num.length,
		oneLimitTrd = oneLimitLnth % 3,
		oneLimitStr = 0;

	oneLimitStr = num.substring(0, oneLimitTrd);
	while(oneLimitTrd < oneLimitLnth){
		if(oneLimitStr != '') oneLimitStr += ',';
		oneLimitStr += num.substring(oneLimitTrd, oneLimitTrd + 3);
		oneLimitTrd += 3;
	}
	var minusComma= "-,";
	if(oneLimitStr.indexOf(minusComma) != -1){
		oneLimitStr = oneLimitStr.replace('-,','-');
	}
	oneLimit = oneLimitStr;
	return oneLimitStr;
}

//그리드 폴딩
function fnExpandAll(gridNm){
	
	if($("#Folding").hasClass("folderOpen")== true){
	    Grids[gridNm].ExpandAll(); 
	    $("#Folding").removeClass('icon folderOpen').addClass('icon folderClose');
	    $("#Folding").html("전체닫기");
	  }else{
	    Grids[gridNm].CollapseAll();   
	    $("#Folding").removeClass('icon folderClose').addClass('icon folderOpen');
	    $("#Folding").html("전체열기");
	    
	  }
}

function fnExpandAll02(gridNm02){
	
	if($("#Folding02").hasClass("folderOpen")== true){
	    Grids[gridNm02].ExpandAll(); 
	    $("#Folding02").removeClass('icon folderOpen').addClass('icon folderClose');
	    $("#Folding02").html("전체닫기");
	  }else{
	    Grids[gridNm02].CollapseAll();   
	    $("#Folding02").removeClass('icon folderClose').addClass('icon folderOpen');
	    $("#Folding02").html("전체열기");
	    
	  }
}