/****************************************************************
 * 
 * 파일명 : tree_slide_left.js
 * 설  명 : 공통 JavaScript
 * 
 *    수정일      수정자     Version        Function 명
 * ------------    ---------   -------------  ----------------------------
 * 2017.07.04    이영재      1.0             최초생성
 *
 */
		
/* 트리그리드 3개 - 20.20.60 */
function treeslideLeft_0301(){
	$(window).resize(function(){
		var slideTreeGridW = $('.slideWidth').width();
		$(".slideTreeGrid").width(slideTreeGridW);	
	});
	$(window).trigger('resize');
	
	$(".slideBtn").click(function(){		
		var title = $(document).attr("title");
		if($(this).hasClass("slideHide")){
			$(this).parents(".slideWidth").animate({"width" : "20%"}, 200);
			$(this).prev(".slideArea").show();
			$("#slideWidthRight").animate({"width" : "-=18.5%"}, 200);
			$(this).removeClass("slideHide");
			$(this).attr({title:"닫기"});
		}else{
			$(this).parents(".slideWidth").css({"width" : "1.5%"});
			$(this).prev(".slideArea").hide();
			$("#slideWidthRight").animate({"width" : "+=18.5%"}, 200);
		    $(this).addClass("slideHide");
		    $(this).attr({title:"열기"});
		}
	});
}

/* 트리그리드 2개 - 20.80 */
function treeslideLeft_0201(){
	$(window).resize(function(){
		var slideTreeGridW = $('.slideWidth').width();
		$(".slideTreeGrid").width(slideTreeGridW);
	});
	$(window).trigger('resize');

	$(".slideBtn").click(function(){		
		var title = $(document).attr("title");
		if($(this).hasClass("slideHide")){
			$(this).parents(".slideWidth").animate({"width" : "20%"}, 200);
			$(this).prev(".slideArea").show();
			$("#slideWidthRight").animate({"width" : "80%"}, 200);
			$(this).removeClass("slideHide");
			$(this).attr({title:"닫기"});
		}else{
			$(this).parents(".slideWidth").css({"width" : "1.5%"});
			$(this).prev(".slideArea").hide();
			$("#slideWidthRight").animate({"width" : "+=18%"}, 200);
		    $(this).addClass("slideHide");
		    $(this).attr({title:"열기"});
		}
	});
}

/* 트리그리드 2개 - 45.55 */
function treeslideLeft_0202(){
	$(window).resize(function(){
		var slideTreeGridW = $('.slideWidth').width();
		$(".slideTreeGrid").width(slideTreeGridW);
	});
	$(window).trigger('resize');

	$(".slideBtn").click(function(){		
		var title = $(document).attr("title");
		if($(this).hasClass("slideHide")){
			$(this).parents(".slideWidth").animate({"width" : "45%"}, 200);
			$(this).prev(".slideArea").show();
			$("#slideWidthRight").animate({"width" : "55%"}, 200);
			$(this).removeClass("slideHide");
			$(this).attr({title:"닫기"});
		}else{
			$(this).parents(".slideWidth").css({"width" : "1.5%"});
			$(this).prev(".slideArea").hide();
			$("#slideWidthRight").animate({"width" : "+=43%"}, 200);
		    $(this).addClass("slideHide");
		    $(this).attr({title:"열기"});
		}
	});
}