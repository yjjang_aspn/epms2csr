/****************************************************************
 * 
 * 파일명 : common.js
 * 설  명 : 공통 JavaScript
 * 
 *    수정일      수정자     Version        Function 명
 * ------------    ---------   -------------  ----------------------------
 * 2017.07.04    이영탁      1.0             최초생성
 *
 */


$(document).ready(function(){
	
	// layout for dynamic height
	$(window).resize(function(){
			
		/*
		 * 화면(프레임) 레이아웃
		 */
	
		var winH = $(window).height();
		
		 // #header : 화면 상단의 GNB 영역 
		var headerH = $("#header").outerHeight();
		
		// 글로벌 클래스 네임 : window의 높이값을 쓰려는 곳에 공통 클래스명 사용할 수 있도록 클래스명 'winH' 임의 지정
		$(".winH").height(winH);
		
		// #wrapper : body 태그의 직속 영역이자 화면의 전체를 감싸는 레이아웃 
		$("#wrapper").height(winH);
		
		// #main : #wrapper - #header
		$("#main").height(winH-headerH);
		var mainH = $("#main").height();
		
		var summaryWrapH = $("#summaryWrap").height();
		
		// #container : #main - #summaryWrap
		$("#container").height(mainH-summaryWrapH);
		var containerH = $("#container").height();

		/*
		 * 그리드 레이아웃
		 */
		
		$("#mainIfrm").height(containerH);
		var mainIfrmH = $("#mainIfrm").height();

		var contentsW = $("#contents").width();
		if ($("#contents").width() < 1285) {
			$("#contents").height(winH-15);
		  }
		else {
			$("#contents").height(winH);
		}
		var contentsH = $("#contents").height();
		
		// panel height 설정을 위한 변수
		var panelTit = $(".panel-tit").outerHeight();
		var inqAreaTop = $(".inq-area-top").outerHeight();
		var inqAreaInner = $(".inq-area-inner").outerHeight();
		
		//panel-wrap : 상단 조회 영역 아래,가로 100% 그리드
		var panelWrap = $(".panel-wrap").height();
		$(".panel-wrap").find(".panel-body").height(panelWrap-panelTit);
		
		//panel-wrap01 : 세로100% 그리드(타이틀 포함)
		$(".panel-wrap01").height(contentsH);
		var panelWrap01 = $(".panel-wrap01").height();
		$(".panel-wrap01").find(".panel-body").height(panelWrap01-panelTit-30);
		
		//panel-wrap02 : 상단 조회 영역 + 하단 그리드 영역 - 레포트 > 정비현황 분석 (상세)
		$(".panel-wrap02").height(contentsH-inqAreaTop);
		var panelWrap02 = $(".panel-wrap02").height();
		$(".panel-wrap02").find(".panel-body").height(panelWrap02-75);
		
		$(".panel-wrap02-top").height(panelWrap02/2);
		var panelWrap02Top= $(".panel-wrap02-top").height();
		$(".panel-wrap02-top").find(".panel-body").height(panelWrap02Top);
		$(".panel-wrap02-btm").height(panelWrap02/2-88);
		var panelWrap02Btm = $(".panel-wrap02-btm").height();
		$(".panel-wrap02-btm").find(".panel-body").height(panelWrap02Btm);
		
		//panel-wrap03 : 세로100% 그리드(기본 레이아웃)
		 $(".panel-wrap03").height(contentsH);
		 var panelWrap03 = $(".panel-wrap03").height();
		 $(".panel-wrap03").find(".panel-body").height(panelWrap03-panelTit-20);
		
		//panel-wrap04 : 그리드 영역(타이틀 및 조회 영역 포함)
		$(".panel-wrap04").height(contentsH);
		var panelWrap04 = $(".panel-wrap04").height();
		$(".panel-wrap04").find(".panel-body").height(panelWrap04-panelTit-inqAreaInner-20);
		
		//panel-wrap05 : 세로100% 그리드(타이틀 포함 탭 검색 영역 포함)
		$(".panel-wrap05").height(contentsH);
		var panelWrap05 = $(".panel-wrap05").height();
		$(".panel-wrap05").find(".panel-body").height(panelWrap05-panelTit-73);
		
		//panel-wrap05_01 : 세로100% 그리드(타이틀 포함 탭 검색 영역 포함)
		$(".panel-wrap05_01").height(contentsH);
		var panelwrap05_01 = $(".panel-wrap05_01").height();
		$(".panel-wrap05_01").find(".panel-body").height(panelwrap05_01-panelTit-45);
		
		//panel-wrap06 : 가로100% + 세로100% 전체 영역(이 전체 영역안에 각 div를 넣고 전체 스크롤 - ex) 출장비 )
		$(".panel-wrap06").height(mainIfrmH);
		var panelWrap06 = $(".panel-wrap06").height();
		$(".panel-wrap06").find(".panel-body").height(panelWrap06);
		
		//panel-wrap-top, panel-wrap-btm 상하단 레이아웃
		if ($(".inq-area-inner").length) {
			$(".panel-wrap-btm").height(250);
			var panelWrapBtm = $(".panel-wrap-btm").height();
			$(".panel-wrap-btm").find(".panel-body").height(panelWrapBtm-40);
		  }
		 else {
			$(".panel-wrap-btm").height(250);
			var panelWrapBtm = $(".panel-wrap-btm").height();
			$(".panel-wrap-btm").find(".panel-body").height(panelWrapBtm);
		 }
		$(".panel-wrap-top").height(contentsH-panelWrapBtm-35);
		var panelWrapTop = $(".panel-wrap-top").height();
		$(".panel-wrap-top").find(".panel-body").height(panelWrapTop);
		
		//window popup
		$(".pop-panel").height(containerH);
		var popPanel = $(".pop-panel").height();
		$(".pop-panel").find(".panel-body").height(popPanel-40);
		
		/*트리그리드 버튼 많을 시 왼쪽 width 값 pixel로 fix 후 오른쪽 판넬 너비 구하기*/
		var contentsW = $("#contents").width();
		var leftPanelAreaW = $(".leftPanelArea").width();
		$(".rightPanelArea").width(contentsW-leftPanelAreaW-1);
		
	   // 모달 팝업 높이
		$(".modal").height(contentsH);
		var modalH = $('.modal').height();
		
		$(".modal").find(".modal-dialog").height(modalH-100);
		var modalDialogH = $('.modal-dialog').height();
		
	   var modalHeadH = 51;
	   var modalHeadFootH = 97;
	   
		if($('.modal-footer').hasClass('hasFooter')) {
			$('.modal-body').css({ 'height': 'calc(100% - ' + modalHeadFootH+ 'px)' });
		}else {
			$('.modal-body').css({ 'height': 'calc(100% - ' + modalHeadH+ 'px)' });
	   }
		
		$(".panel-wrap-modal").height(modalDialogH-87);
		var panelWrapModalH = $(".panel-wrap-modal").height();
		$(".panel-wrap-modal").find(".panel-body").height(panelWrapModalH);
		
		//lobipanel expand 시 높이
		$(".expandPortletWrap").height(winH);
		var expandPortletWrapH = $(".expandPortletWrap").height();
		
		$(".expandIfrm").height(expandPortletWrapH);
		var expandIfrmH = $(".expandIfrm").height();
		
		// 내용
		$(".expandPortletH").height(expandPortletWrapH);
		// 상단조회영역 + 내용
		$(".expandPortletH01").height(expandPortletWrapH-40);
		// 상단조회영역 (padding 값이 더 들어갔음) + 내용
		$(".expandPortletH02").height(expandPortletWrapH-57);
		// 탭 + 상단조회영역 + 내용
		$(".expandPortletH03").height(expandPortletWrapH-97);
	});
	$(window).trigger('resize');
	
	// 컨텐츠 영역 예외 스타일 - 스크롤 추가
   $("body").find(".type-scl").closest(".cont-inner").addClass("scl");

   // 메인대시보드 공지 팝업 파일첨부 유무에 따른 컨텐츠 height 값 설정
   $("body").find(".noticePopFile").prev().find(".noticePopCon").addClass("height");
   
   // 메뉴 진입시 좌측에 해당 메뉴 펼침 유지
	var menu = $("#MENU").val();
		// interceptor에서 구한 메뉴코드 값.. 
		// 기존 로직을 URL를 직접 입력 했을 때 메뉴코드를 못불러오는 버그가 있음
	if(menu == null || menu == ''){
		menu =  $("#in_menu_code").val(); 
		if(menu != '' && typeof menu != 'undefined' && menu.length >= 6){
			menu = menu.substring(0,6); 
		}
	}
	
	// 탭 : 정비실적등록 탭
	$(function(){
		$('.popTabSet').each(function(){
			var topDiv=$(this);
			var anchors=topDiv.find('ul.tabs a');
			var panelDivs=topDiv.find('div.panel');
			
			var lastAnchor;
			var lastPanel;
			anchors.show(); //css에서 display:none 했던 tab이 보여짐
			lastAnchor=anchors.filter('.on');
			lastPanel=$(lastAnchor.attr('href'));
			//lastPanel=#panel2-3
			panelDivs.hide();
			lastPanel.show();
			
			anchors.click(function(event){
				event.preventDefault(); //하이퍼링크 동작 무효화
				var currentAnchor=$(this);
				var currentPanel=$(currentAnchor.attr('href'));
				lastAnchor.removeClass('on');
				currentAnchor.addClass('on');
				lastPanel.hide();
				currentPanel.show();
				lastAnchor=currentAnchor;
				lastPanel=currentPanel;
			});
		});
	});
	
	// gnb  메뉴 선택시 selected class 추가
	$(function(){	
		$("#gnb > li").click(function(){
			$("#gnb > li.selected").removeClass("selected");
			$(this).addClass("selected");
		})	
	})	
	
	// 페이지 이동 후 해당 페이지의 메뉴 네비게이션 펼쳐보이기 유지
	$(function(){	
		$("#nav-list li").click(function(){
			$("#nav-list li.active").removeClass("active");
			$(this).addClass("active");
		})	
	})	
	
	if(menu != null){
		var menuNo = menu;
		var forMenuNo = menuNo.substring(0, 3);
		$("#nav-list").find(".menu"+forMenuNo).parent("li").addClass("active");
		$(".menu"+forMenuNo).next(".submenu").show();
		
		$('.pg-move').each(function(index, item) {
			item = $(this).data("menu");
			if(menuNo == item){
				$(this).addClass("selected");
				$(this).parent(".submenu").addClass("open");
			}
		});
	}
	
	//tab
	$('.tab-depth01').on('click', '.btn-tab', function(){
		$(this).parent('li').addClass('on').siblings('li').removeClass('on');
	});
	$('.tab-depth02').on('click', '.btn-tab', function(){
		$(this).parent('li').addClass('on').siblings('li').removeClass('on');
	});
	$('.tab-depth03').on('click', '.btn-tab', function(){
		$(this).parent('li').addClass('on').siblings('li').removeClass('on');
	});
	
	// radio
	$('.rdo-st').each(function(){
		if($(this).find(':radio').attr('checked')){
			$(this).addClass('on');
		}
	}).on('click', function(){
		$(':radio[name="' + $(this).find(':radio').attr('name') + '"]').each(function(){
			$(this).closest('.rdo-st').removeClass('on');
		});
		$(this).addClass('on');
	});
	
	//checkbox checked
    $('body').on('click', '.chk-st', function(){
    	if($(this).find('label').length == 0){
    		if($(this).hasClass('readonly') == false){
        		if($(this).hasClass('on')){
        			$(this).removeClass('on').find('input').removeAttr('checked');
        		}else{
        			$(this).addClass('on').find('input').attr('checked', 'checked');
        		}
        	}
    	}
    }).on('click', '.chk-st label', function(){
		if($(this).closest('.chk-st').hasClass('readonly') == false){
    		if($(this).closest('.chk-st').hasClass('on')){
    			$(this).closest('.chk-st').removeClass('on').find('input').removeAttr('checked');
    		}else{
    			$(this).closest('.chk-st').addClass('on').find('input').attr('checked', 'checked');
    		}
    	}
    });
    
});

//첨부파일 다운로드
$(document).ready(function(){
	$(".attachFileTit>a").on("click", function(){
		if($(this).hasClass("down") == true){
			$(this).removeClass("down").addClass("up");
			$(".attachFileLst").show();
		}else{
			$(".attachFileTit>a.up").removeClass("up").addClass("down");
			$(".attachFileLst").hide();
		}
	});
});

//탭 : 정비원 업무현황, 내 요청현황 expand 시 탭
function openTab(evt, tabName) {
	$('.expandedTabSet').each(function(){
		var i, expandTabPanel, expandTabLink;
		expandTabPanel = document.getElementsByClassName("panel");
		for (i = 0; i < expandTabPanel.length; i++) {
		    expandTabPanel[i].style.display = "none";
		}
		expandTabLink = document.getElementsByClassName("expandedTabLink");
		for (i = 0; i < expandTabLink.length; i++) {
		    expandTabLink[i].className = expandTabLink[i].className.replace(" active", "");
		}
		document.getElementById(tabName).style.display = "block";
		evt.currentTarget.className += " active";
	});
}

function innerLayout(){
	
	var  innerLayoutWrap = $(".inner-layout-wrap").height();
	var innerTop = $(".top").height();
	$(".inner-layout-wrap").css("padding-top",innerTop);
	$(".bottom").height(innerLayoutWrap-innerTop);
	
	//console.log("innerLayoutWrap : " + innerLayoutWrap);
	//console.log("innerTop :" + innerTop);
	
}

function toggleMenu(){ 
	
	// navigation(lnb to snb) toggle
	$(".lnb").click(function(){
		if($(this).data("menu") == "054000"){
			var url = "http://hello.aspn.co.kr/sys/manual/e-Accounting 사용자 메뉴얼.pdf";
			window.open(url,'guide','scrollbars=yes,toolbar=yes,resizable=yes,width=1200,height=800,left=0,top=0');
			return;
		}
	
		$(".lnb").not(this).next(".submenu").hide();
		$(".lnb").not(this).closest("li").removeClass("active");
		
		$(this).next(".submenu").slideToggle("fast",function(){
			$(this).closest(".lnb").toggleClass("active");
		});
	});
	
}

function slideLeft(){

	$(".left-toggle").click(function(){
		
		if($(this).hasClass("on")){
			//openNav
			  $("#aside").width(200);
			  $("#section").addClass("mgn-l-200");
			  $(".left-toggle").removeClass("left-open");
			  $(this).removeClass("on");
		}else{
			//closeNav
		    $("#aside").width(0);
		    $("#section").removeClass("mgn-l-200");
		    $(".left-toggle").addClass("left-open");
		    $(this).addClass("on");
		}
	});
	
}

$(function(){
	
	toggleMenu();
	slideLeft();
	
});

/**********************************************
 * 함수명 : isNull
 * 설  명 :  null check
 * 인  자 : 체크할item(object), alert메시지
 * 사용법 : isNull($("#userId"), "아이디를 입력하세요.")
 */
function isNull(checkItem, strMessage) {
	if (checkItem.val() == "") {
		alert(strMessage);
		checkItem.focus();
		return false;
	}
	return true;
}

/**********************************************
 * 함수명 : fnSubmit
 * 설  명 :  form submit
 * 인  자 : form명, actionURL
 * 사용법 : fnSubmit("frmLogin", "/sys/member/webLogin.do");
 */
function fnSubmit(frmNm, action){							// Sumbit
	$('#'+frmNm).attr("action", action);
	$('#'+frmNm).submit();
}

//그리드 컬럼 선택해서 보여지는 기능
function showColumns(gridNm){
	Grids[gridNm].ActionShowColumns();
    return;
}

//데이트피커 호출
$(function(){
	$('.datepicker').each(function(){
		$(this).datepicker({	
			dateFormat: 'yy/mm/dd',
			changeMonth: false,
			changeYear: false,
			monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
			monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
			dayNames: ['일','월','화','수','목','금','토'],
			dayNamesShort: ['일','월','화','수','목','금','토'],
			dayNamesMin: ['일','월','화','수','목','금','토'],
			showMonthAfterYear: true,
			yearSuffix: ','
		});
	});
});

//replaceAll prototype 선언
String.prototype.replaceAll = function(org, dest) {
    return this.split(org).join(dest);
}

//일자 유효성 체크
function dayValiCheck(startDayObj, endDayObj){
	
	var startDay = parseInt(startDayObj.val().replaceAll("/", "").replaceAll("-", "").replaceAll(".", ""));
	var endDay = parseInt(endDayObj.val().replaceAll("/", "").replaceAll("-", "").replaceAll(".", ""));
	if(startDay > endDay){
		alert("종료일이 시작일보다 이전 일 수 없습니다.");
		return false;
	}	
	return true;
}

//시간 유효성 체크
function timeValiCheck(startTimeObj, endTimeObj){

	if(startTimeObj != "" ){
		if(parseInt(startTimeObj.val().replace(" : ", "")) >= parseInt(endTimeObj.val().replace(" : ", ""))){
			alert("종료시간이 시작시간과 같거나 이전 일 수 없습니다.");
			return false;
		}
	}

	return true;
}

//일자 및 시간 유효성체크
function dateValiCheck(startDayObj, endDayObj, startTimeObj, endTimeObj){
	
	if(dayValiCheck(startDayObj, endDayObj)){
		if(startDayObj == endDayObj){ 
			if(startTimeObj != "" ){
				if(parseInt(startTimeObj.val().replace(" : ", "")) >= parseInt(endTimeObj.val().replace(" : ", ""))){
					alert("종료시간이 시작시간과 같거나 이전 일 수 없습니다.");
					return false;
				}
			}
		}
		return true;
	}else{
		return false;
	}

	
}

//post popup
function fn_openPostPopup(frm, target, url, width, height)
{
	var url    =url;
	var target  = target;
	var status = "toolbar=no,directories=no,scrollbars=no,resizable=no,status=no,menubar=no,width="+width+", height="+height+", top=0,left=20"; 
	window.open("", target,status); //window.open(url,title,status); window.open 함수에 url을 앞에와 같이
	                                          //인수로  넣어도 동작에는 지장이 없으나 form.action에서 적용하므로 생략
	                                          //가능합니다.
	frm.target = target;                    //form.target 이 부분이 빠지면 form값 전송이 되지 않습니다. 
	frm.action = url;                    //form.action 이 부분이 빠지면 action값을 찾지 못해서 제대로 된 팝업이 뜨질 않습니다.
	frm.method = "post";
	frm.submit();     
}