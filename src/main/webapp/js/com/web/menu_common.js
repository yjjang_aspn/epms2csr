$(document).ready(function(){
	
	// 로그 아웃
	$("#logOutBtn").on("click", function(){
		fnSubmit("menuFrm", "/logout.do");
	})
	
	// 메인으로 이동
	$('.ci').on("click",function(){
		fnSubmit("menuFrm", "/main.do");
	});
	
	// 메일 로그인
	$('#mailSubmitBtn').on('click', function(){
		fnEmailLogin();
	});
	
	// 메일 패스워드 엔터키 처리
	$('#mailPwd').on('keyup',function(e){
		if( e.which == 13){
			fnEmailLogin();
		}
	});
	
	// 프로파일 팝업
	$('.user-info').on("click", function(){
		fnOpenLayerWithParam('/mem/member/popProfile.do','popProfile');
	});
	
	//popup close
	$('.popDimm, .btnPopClose, #closePop').on('click', function(){
		popClose($(this).closest('.popArea'));
	});	
	
	$('#closePop').click(function(){
		$('.popDimm').click();			
	});
	
});

// 팝업 닫기
function popClose(popClass){
	popClass.hide();
}

// 이메일 로그인
function fnEmailLogin(){
	if( !isNull_J($('#mailId'), 'ID를 입력해주세요.') ) {
		return false;
	}
	
	if( !isNull_J($('#mailPwd'), 'Password를 입력해주세요.') ) {
		return false;
	}

	var expdate_id = new Date();
	expdate_id.setTime(expdate_id.getTime() + 1000 * 3600 * 24 * 30);   
	setCookie("saveMailId", $("#mailId").val(), expdate_id);
	fnSubmit("mailFrm", "/mailReceiveList.do");
	
}

// 메일 이동
function fnMoveMail(){
	popOpen($('.userId'));
	$('#mailId').val('${sessionScope.ssEmail}');
	$('#mailIdTxt').text('${sessionScope.ssEmail}');
	$('.userId .popWrap').css('height', '172px');
	$('#mailPwd').focus();
}


// 헤더 메뉴 셀렉트 처리
function hearMenuSelect(header_menu){
	$('#gnb_header li').removeClass('selected');
	$('#gnb_' + header_menu).addClass('selected');
}

// 헤더 메뉴 클릭시 링크 처리
function linkHeaderMenu(url, menu_group){
	location.href = url;
}

// 레이어 팝업 오픈
function popOpen(popClass){
	popClass.show();
	popClass.find('.popRmvInp').each(function(){
		$(this).val('');
	});
	if(popClass.find('.popInner').innerHeight() >= $(window).height()){
		popClass.find('.popWrap').css({'top':'20px', 'height':$(window).height()-40, 'margin-top':'0'});
		popClass.find('.popInner').css('max-height', $(window).height()-100);
	}else{
		popClass.find('.popWrap').css({'height':popClass.find('.popInner').innerHeight(), 'margin-top':-(popClass.find('.popInner').innerHeight()/2)});
		popClass.find('.popInner').css('max-height', popClass.find('.popInner').innerHeight());
	}
};




