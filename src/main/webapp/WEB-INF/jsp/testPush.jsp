<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%--
  Class Name : 
  Description : 
  Modification Information
  
  수정일			수정자			수정내용
  ------			------			--------
  2017-09-27		강승상			최초생성
  
  author	: 강승상
  since		: 2017-09-27
--%>
<html>
<head>
	<title>Test Push Message</title>
</head>
<body>
<form id="pushForm" action="testPushAjax.do">
	<label> PUSH_ID :
		<input name="PUSH_ID"
			   value="cBzX7OFm3Gs:APA91bEzeuV603sO6GKENDm2d85uaGuSI2fWrgDuLwGzvp21c6cPz13-UPFme7hCUwK9zWgLqaFQWqHRk7MmLReOx_vJI0N4_oQqumHnEF6wCLv4oi8kptDWu3aAYxbIGUdMFaZhq8Y7"/>
	</label>
	<label> MEMBER :
		<input name="MEMBER" value="sjjung"/>
	</label>
	<label> CD_PLATFORM :
		<input name="CD_PLATFORM" value="A"/>
	</label>
	<br>
	<input name="title" value="TEMP_MESSAGE"/>
	<input name="actionUrl" value="TEMP_ACTIONURL"/>
	<input name="message" value="TMP_MESSAGE"/>
	<input name="topName" value="TMP_TOPNAME"/>
	<br>
	<button>submit</button>
</form>
</body>
</html>
