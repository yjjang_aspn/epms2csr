<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: popPreventiveResultRegForm.jsp
	Description : 예방보전관리 > 예방보전실적 등록 레이어 팝업
	author		: 김영환
	since		: 2018.05.09
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.05.09	 김영환		최초 생성

--%>
<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">
var submitBool = true;
var maxFileCnt = 30;
var msg = "";
var remarks = $("#popPreventiveRegForm_REMARKS").val();

$(document).ready(function(){
	
	$('.panel.type02').each(function () {
		if($(this).find(".tabFixedBtn").length){
			$(this).find(".tabFixedBtn_Con").css({ 'height': 'calc(100% - 51px)'});
		}else {
			$(this).find(".tabFixedBtn_Con").css({ 'height': '100%'});
		}
	});
	
	//최초 정비원리스트 불러와서 콤보박스 세팅
	fnGetWorkMemberList('${userPart}');
	
	//판정이 X일 경우 정비요청 시 필요한 파트, 정비원 리스트 보이기
	$("#CHECK_DECISION").change(function(){
		if(remarks == "01"){
			if($(this).val() == "03") {
				$("#partTR").show();
				$("#workTR").show();
				$("#REQUEST_PART").val('${userPart}');
				$("#REQUEST_MANAGER").val('${userId}');
			} else {
				$("#partTR").hide();
				$("#workTR").hide();
			}
		} 
		else if(remarks == "02") {
			if($(this).val() == "03") {
				$("#partTR").show();
				$("#workTR").show();
				$("#REQUEST_PART").val('${userPart}');
				$("#REQUEST_MANAGER").val('${userId}');
			} else {
				$("#partTR").hide();
			}
		}
		
	});
	
	// 소속 파트를 선택했을때만 정비원 콤보박스 보이기
	$("#REQUEST_PART").change(function() {
		if(remarks == "01"){
			if($(this).val() == '${userPart}') {
				$("#workTR").show();
				$("#REQUEST_MANAGER").val('${userId}');
			} else {
				$("#workTR").hide();
				$("#REQUEST_MANAGER").val("");
			}
		} 
		else if(remakrs == "02") {
			$("#workTR").show();
			$("#REQUEST_MANAGER").val('${userId}');
		}
	});
	
	<%-- 첨부파일 수정버튼 (등록폼 변환) --%>
	$('#modifyBtn').on('click', function(){
		
		$('#viewFrame').hide();
		$('#regFrame').show();
	});

	<%-- 첨부파일 관련 --%>
	for(var i=0; i<1; i++){				<%-- 파일 영역 추가를 고려하여 개발 --%>
		$('#attachFile').append('<input type="file" multiple id="fileList" class="ksUpload" name="fileList">');	
		
		var fileType = 'jpeg |gif |jpg |png |bmp |ppt |pptx |doc |hwp |pdf |xlsx |xls';
		//var fileType = 'jpeg |gif |jpg |png |bmp';
	
		$('#fileList').MultiFile({
			   accept :  fileType
			,	list   : '#detailFileList'+i
			,	max    : 5
			,	STRING : {
					remove: '<img src="/images/com/web/btn_remove.png"/>'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
					text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" />',
					idx	  : i,
				},
		});
		
		<%-- 첨부한 파일의 출력 순서를 자동으로 샛팅 하기 위한 스크립트 - 공통 스크립트로 빠지면 안됨 .개별 항목으로 처리 필수 --%> 
		$('.MultiFile-wrap').change(function(){
			var fileExpIndex = $(this).closest('.fileWrap').find('.fileList > div').children('div:last-child');
			var	fileExp = fileExpIndex.find('.fileExp');
			fileExp.val(fileExpIndex.index() + 1);
		});
	}
	
	<%-- 첨부파일 삭제버튼 --%>
	$('.icn_fileDel').on('click', function(){
		delModiFile($(this));
	});
	
	<%-- 핀장 콤보박스 세팅 --%>
	if("${preventiveResultDtl.CHECK_DECISION}" != '') $("select[name=CHECK_DECISION]").val("${preventiveResultDtl.CHECK_DECISION}");
});

<%-- 권한별 정비요청 정비원목록 조회 --%>
function fnGetWorkMemberList(part){
	$.ajax({
		type : 'POST',
		url : '/epms/repair/request/selectWorkMemberList.do',
		dataType : 'json',
		data : { REMARKS : remarks, PART : part, SUB_ROOT : $("#popPreventiveRegForm_LOCATION").val() },
		success : function(json) {
			if(json.length > 0){
				var html = "<select id='REQUEST_MANAGER' name='REQUEST_MANAGER'>"
						  + "<option value=''>담당자 미배정</option>";
				for(var i=0; i<json.length; i++){
					html += "<option value='"+json[i].USER_ID+"'>"+json[i].NAME+"</option>";					
				}
				html += "</select>";
				
				$("#workSelect").html(html);
			}else{
				alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.');
				return;
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
			loadEnd();
		}
	});
}

<%-- 사용자재 '검색'버튼 --%>
function fnSearchMaterial(){
	
	if($('#PREVENTIVE_RESULT').val() == "" || $('#PREVENTIVE_RESULT').val() == null){
		return;
	}
	
	var url = '/epms/equipment/bom/equipmentBomListData.do'
			+ '?EQUIPMENT=' + $('#popPreventiveRegForm_EQUIPMENT').val()
			+ '&LOCATION=' +  $('#popPreventiveRegForm_LOCATION').val();
	
	Grids.popRepairMaterialList.Source.Data.Url = url;
	Grids.popRepairMaterialList.ReloadBody();
	fnModalToggle('popRepairMaterialList');
}

<%-- 자재추가 버튼 --%>
function fnAddMaterial(gridNm){
	
	var equipment_bom = new Array();
	var material_grp = new Array();
	var material_grp_uid = new Array();
	var material_grp_name = new Array();
	var material = new Array();
	var material_code = new Array();
	var material_name = new Array();
	var unit = new Array();
	var unit_name = new Array();
	
	var selRows = Grids[gridNm].GetSelRows();
	
	if(selRows == null || selRows == ""){
		return;
	}
	
	for(var i=0; i<selRows.length; i++){
		
		if(!(selRows[i].MATERIAL=="" || selRows[i].MATERIAL==null)){
			equipment_bom[i] = selRows[i].EQUIPMENT_BOM;
			material_grp[i] = selRows[i].MATERIAL_GRP;
			material_grp_uid[i] = selRows[i].MATERIAL_GRP_UID;
			material_grp_name[i] = selRows[i].MATERIAL_GRP_NAME;
			material[i] = selRows[i].MATERIAL;
			material_code[i] = selRows[i].MATERIAL_UID;
			material_name[i] = selRows[i].MATERIAL_NAME;
			unit[i] = selRows[i].UNIT;
			unit_name[i] = selRows[i].UNIT_NAME;
		}
	}
	
	addMaterial(equipment_bom, material_grp, material_grp_uid, material_grp_name, material, material_code, material_name, unit, unit_name, i);
	fnModalToggle('popRepairMaterialList');
}

<%-- 추가자재 선택 후 html 작성 --%>
function addMaterial(equipment_bom, material_grp, material_grp_uid, material_grp_name, material, material_uid, material_name, unit, unit_name, cnt){
	var html = "";
	
	for(var i=0; i<cnt; i++){
		
		if($('#usedMaterial').html().indexOf('name="equipmentBomArr" value="' + equipment_bom[i] + '"') < 0 && $('#usedMaterial').html().indexOf('name="equipmentBomUsedArr" value="' + equipment_bom[i] + '"') < 0){	
			html +=
				'<li style="overflow:hidden; margin:5px 0;">'
			+	'	<input type="hidden" class="inp-comm" title="BOMID" name="equipmentBomArr" value="' + equipment_bom[i] + '"	readonly="readonly">'
			+	'	<input type="hidden" class="inp-comm" title="자재ID" name="materialArr" value="' + material[i] + '"	readonly="readonly">'
			+	'	<div class="box-col wd-per-20 f-l">'
			+	'		<div class="inp-wrap readonly">'
			+	'			<input type="text" class="inp-comm" title="자재코드" name="" value="' + material_uid[i] + '"	readonly="readonly">'
			+	'		</div>'
			+	'	</div>'
			+	'	<div class="box-col wd-per-20 f-l">'
			+	'		<div class="inp-wrap readonly mgn-l-5">'
			+	'			<input type="text" class="inp-comm" title="자재명" name="" value="' + material_name[i] + '"	readonly="readonly">'
			+	'		</div>'
			+	'	</div>'
			+	'	<input type="hidden" class="inp-comm" title="단위" name="" value="' + unit[i] + '"	readonly="readonly">'
			+	'	<div class="box-col wd-per-20 f-l">'
			+	'		<div class="inp-wrap readonly mgn-l-5">'
			+	'			<input type="text" class="inp-comm" title="단위명" name="" value="' + unit[i] + '"	readonly="readonly">'
			+	'		</div>'
			+	'	</div>'
			+	'	<div class="box-col wd-per-20 f-l">'
			+	'		<div class="inp-wrap mgn-l-5">'
			+	'			<input type="text" class="inp-comm" title="수량" name="qntyArr"	value="0"	onchange="check_Null(this);"	onkeydown="check_isNum_onkeydown();" autocomplete="off">'
			+	'		</div>'
			+	'	</div>'
			+	'	<a href="#none" class="btnRemove mgn-l-5" onclick="delList($(this))">삭제</a>'
			+	'</li>';
		}
	};
	
	$('#usedMaterial').append(html);
}

<%-- 정비원,자재 삭제버튼 --%>
function delList(delLst){
		delLst.closest('li').remove();
}


<%-- 실적등록 ajax --%>
function fnRegAjax(){
	if(confirm("실적등록 하시겠습니까? \n* 실적등록을 완료하시면 더 이상 수정이 불가능합니다.")){
		msg = "정상적으로 '실적등록' 처리되었습니다.";
	} else {
		return;
	}
	
	submitBool = false;
		
	if (!isNull_J($('#CHECK_DESCRIPTION'), '점검내용을 입력해주세요.')) {
		submitBool = true;
		return false;
	}
	
	if (!isNull_J($('#CHECK_DECISION'), '판정을 선택해주세요.')) {
		submitBool = true;
		return false;
	}
	
	if($("#CHECK_DECISION").val() == "03") {
		if (!isNull_J($('select[name=REQUEST_PART]'), '파트를 선택해주세요.')) {
			submitBool = true;
			return false;
		}
	}
		
	
	var fileCnt = $("input[name=fileGb]").length;
	if (fileCnt > 0) {
		$('#attachExistYn').val("Y");
	} else {
		$('#attachExistYn').val("N");
	}
	
	$('input[name=materialUsedArr]').attr('value',convertMaterialArrToJSON('materialUsedArr', 'qntyUsedArr', 'equipmentBomUsedArr'));
	$('input[name=materialArr]').attr('value',convertMaterialArrToJSON('materialArr', 'qntyArr', 'equipmentBomArr'));
	
	var form = new FormData(document.getElementById('regForm'));
	
	$.ajax({
		type : 'POST',
		url : '/epms/preventive/result/preventiveResultRegAjax.do',
		dataType : 'json',
		data : form,
		processData : false,
		contentType : false,
		success : function(json) {	
			
			<%-- 정상적으로 정비실적 등록시 --%>
			if(json != "" || json != null){
				fnModalToggle('popPreventiveResultRegForm');
				fnReload(msg);
			<%-- 이미 등록된 정비실적일 경우 --%>
			}else{
				alert("처리도중 에러가 발생하였습니다 . 시스템 관리자에게 문의 하시기 바랍니다");
				return;
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
			//loadEnd();
		}
	});
}

<%-- 사용자재 input 값들을 JSON화 --%>
function convertMaterialArrToJSON(material, qnty, equipment){
	
	var tempArrSize = $("input[name='"+material+"']").length;
	var tempArr = new Array();
	
	for(var i = 0; i < tempArrSize; i++){
		var tempObj = new Object();
		tempObj.MATERIAL = $("input[name='"+material+"']")[i].value;
		tempObj.QNTY = $("input[name='"+qnty+"']")[i].value;
		tempObj.EQUIPMENT_BOM = $("input[name='"+equipment+"']")[i].value;
		
		tempArr.push(tempObj);
	}
	
	return JSON.stringify(tempArr);
}


</script>
<div class="modal-dialog root wd-per-60">
	<div class="modal-content">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title">예방보전실적 등록</h4>
		</div>
	 	<div class="modal-body overflow">
           	<div class="modal-bodyIn hgt-per-100" style="padding:0;">
           		<div class="popTabSet">
					<ul class="tabs">
						<li id="repairRequestTab"><a href="#panel1-1" class="on">실적등록</a></li>
					</ul>
           			<div class="panels" style="margin-top: 41px;">
						<div class="panel type02" id="panel1-1">
							<div class="panel-in rel">
							<c:if test="${preventiveResultDtl.MANAGER eq userId}">
								<div class="overflow tabFixedBtn">
									<div class="f-r"  id="btns">
										<a href="#none" class="btn comm st03" onclick="fnRegAjax()">실적등록</a>
									</div>
								</div>
							</c:if>
							<c:if test="${preventiveResultDtl.MANAGER eq null && preventiveResultDtl.LOCATIONPART eq locationPart}">
								<div class="overflow tabFixedBtn">
									<div class="f-r"  id="btns">
										<a href="#none" class="btn comm st03" onclick="fnRegAjax()">실적등록</a>
									</div>
								</div>
							</c:if>
							<div class="tabFixedBtn_Con">
				           		<div class="tabFixedBtn_ConIn">
									<input type="hidden" id="userId" name="userId" value="${userId}"/>
									
									<form id="regForm" name="regForm"  method="post" enctype="multipart/form-data">	
										<input type="hidden" id="attachExistYn" name="attachExistYn" /> 
										<input type="hidden" id="ATTACH_GRP_NO" name="ATTACH_GRP_NO" value="${preventiveResultDtl.ATTACH_GRP_NO}" /> 
										<input type="hidden" id="PREVENTIVE_RESULT" name="PREVENTIVE_RESULT" value="${preventiveResultDtl.PREVENTIVE_RESULT}"/>
										<input type="hidden" id="popPreventiveRegForm_REMARKS" name="REMARKS" value="${preventiveResultDtl.REMARKS}"/>
										<input type="hidden" id="PREVENTIVE_BOM" name="PREVENTIVE_BOM" value="${preventiveResultDtl.PREVENTIVE_BOM}"/>
										<input type="hidden" id="popPreventiveRegForm_LOCATION" name="LOCATION" value="${preventiveResultDtl.LOCATION}"/>
										<input type="hidden" id="MODULE" name="MODULE" value="122"/>
										<input type="hidden" id="flag" name="flag"/>
										<input type="hidden" id="DEL_SEQ" name="DEL_SEQ" value=""/>
											
										<div class="tb-wrap">
											<table class="tb-st">
												<caption class="screen-out">정비실적 상세</caption>
												<colgroup>
													<col width="15%" />
													<col width="35%" />
													<col width="15%" />
													<col width="*" />
												</colgroup>
												<tbody>
													<tr>
														<th scope="row">공장</th>
														<td>
															<div class="inp-wrap readonly wd-per-100">
																<input type="hidden" class="inp-comm" id="popPreventiveRegForm_LINE_UID" name="LINE_UID" value="${preventiveResultDtl.LOCATION_UID}"/>
																<input type="text" class="inp-comm" id="popPreventiveRegForm_LINE_NAME" name="LINE_NAME" readonly="readonly" value="${preventiveResultDtl.LOCATION_NAME}"/>
															</div>
														</td>
														<th scope="row">라인</th>
														<td>
															<div class="inp-wrap readonly wd-per-100">
																<input type="hidden" class="inp-comm" id="popPreventiveRegForm_LINE_UID" name="LINE_UID" value="${preventiveResultDtl.LINE_UID}"/>
																<input type="text" class="inp-comm" id="popPreventiveRegForm_LINE_NAME" name="LINE_NAME" readonly="readonly" value="${preventiveResultDtl.LINE_NAME}"/>
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">설비코드</th>
														<td>
															<div class="inp-wrap readonly wd-per-100">
																<input type="hidden" class="inp-comm" id="popPreventiveRegForm_EQUIPMENT" name="EQUIPMENT" value="${preventiveResultDtl.EQUIPMENT}"/>
																<input type="text" class="inp-comm" id="popPreventiveRegForm_EQUIPMENT_UID" name="EQUIPMENT_UID" readonly="readonly" value="${preventiveResultDtl.EQUIPMENT_UID}"/>
															</div>
														</td>
														<th scope="row">설비명</th>
														<td>
															<div class="inp-wrap readonly wd-per-100">
																<input type="text" class="inp-comm" id="popPreventiveRegForm_EQUIPMENT_NAME" name="EQUIPMENT_NAME" readonly="readonly" value="${preventiveResultDtl.EQUIPMENT_NAME}"/>
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">상태</th>
														<td>
															<div class="inp-wrap readonly wd-per-100">
																<input type="hidden" class="inp-comm" id="popPreventiveRegForm__STATUS" name="STATUS" value="${preventiveResultDtl.STATUS}"/>
																<input type="text" class="inp-comm" id="popPreventiveRegForm_status_name" name="STATUS_NAME" readonly="readonly" value="${preventiveResultDtl.STATUS_NAME}"/>
															</div>
														</td>
														<th scope="row">업무구분</th>
														<td>
															<div class="inp-wrap readonly wd-per-100">
																<input type="hidden" class="inp-comm" id="popPreventiveRegForm_PREVENTIVE_TYPE" name="PREVENTIVE_TYPE" value="${preventiveResultDtl.PREVENTIVE_TYPE}"/>
																<input type="text" class="inp-comm" id="popPreventiveRegForm_PREVENTIVE_TYPE" name="PREVENTIVE_TYPE_NAME" readonly="readonly" value="${preventiveResultDtl.PREVENTIVE_TYPE_NAME}"/>
															</div>
														</td>
														
													</tr>
													<tr>
														<th scope="row">작업유형</th>
														<td>
															<div class="inp-wrap readonly wd-per-100">
																<input type="hidden" class="inp-comm" id="popPreventiveRegForm_WORK_TYPE" name="WORK_TYPE" value="${preventiveResultDtl.WORK_TYPE}"/>
																<input type="text" class="inp-comm" id="popPreventiveRegForm_WORK_TYPE_NAME" name="WORK_TYPE_NAME" readonly="readonly" value="${preventiveResultDtl.WORK_TYPE_NAME}"/>
															</div>
														</td>
														<th scope="row">점검구분</th>
														<td>
															<div class="inp-wrap readonly wd-per-100">
																<input type="hidden" class="inp-comm" id="popPreventiveRegForm_CHECK_TYPE" name="CHECK_TYPE" value="${preventiveResultDtl.CHECK_TYPE}"/>
																<input type="text" class="inp-comm" id="popPreventiveRegForm_CHECK_TYPE_NAME" name="CHECK_TYPE_NAME" readonly="readonly" value="${preventiveResultDtl.CHECK_TYPE_NAME}"/>
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">점검항목</th>
														<td>
															<div class="inp-wrap readonly wd-per-100">
																<input type="text" class="inp-comm" id="popPreventiveRegForm_CHECK_DETAIL" name="CHECK_DETAIL" readonly="readonly" value="${preventiveResultDtl.CHECK_DETAIL}"/>
															</div>
														</td>
														<th scope="row">판단기준</th>
														<td>
															<div class="inp-wrap readonly wd-per-100">
																<input type="text" class="inp-comm" id="popPreventiveRegForm_CHECK_STANDARD" name="CHECK_STANDARD" readonly="readonly" value="${preventiveResultDtl.CHECK_STANDARD}"/>
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">진단장비</th>
														<td>
															<div class="inp-wrap readonly wd-per-100">
																<input type="text" class="inp-comm" id="popPreventiveRegForm_CHECK_TOOL" name="CHECK_TOOL" readonly="readonly" value="${preventiveResultDtl.CHECK_TOOL}"/>
															</div>
														</td>
														<th scope="row">주기</th>
														<td>
															<div class="inp-wrap readonly wd-per-100">
																<input type="text" class="inp-comm" id="popPreventiveRegForm_CHECK_CYCLE" name="CHECK_CYCLE" readonly="readonly" value="${preventiveResultDtl.CHECK_CYCLE}"/>
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">계획일</th>
														<td>
															<div class="inp-wrap readonly wd-per-100">
																<input type="text" class="inp-comm" id="popPreventiveRegForm_DATE_PLAN" name="DATE_PLAN" readonly="readonly" value="${preventiveResultDtl.DATE_PLAN}"/>
															</div>
														</td>
														<th scope="row">점검계획일</th>
														<td>
															<div class="inp-wrap readonly wd-per-100">
																<input type="text" class="inp-comm" id="popPreventiveRegForm_DATE_ASSIGN" name="DATE_ASSIGN" readonly="readonly" value="${preventiveResultDtl.DATE_ASSIGN}"/>
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">담당자</th>
														<td colspan="3">
															<div class="inp-wrap readonly wd-per-100">
																<input type="hidden" class="inp-comm" id="popPreventiveRegForm_MANAGER" name="MANAGER" value="${preventiveResultDtl.MANAGER}"/>
																<input type="text" class="inp-comm" id="popPreventiveRegForm_MANAGER_NAME" name="MANAGER_NAME" readonly="readonly" value="${preventiveResultDtl.MANAGER_NAME}"/>
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">사용자재</th>
														<td colspan="3">
															<input type="hidden" class="inp-comm" id="popPreventiveRegForm_MATERIAL_INOUT" name="MATERIAL_INOUT" value="${preventiveResultDtl.MATERIAL_INOUT}"/>
															<a href="#none" class="btn evn-st01 mgn-t-5" onclick="fnSearchMaterial()">검색</a>
															<ul class="list-st01 usedMaterial" id="usedMaterial">
																<c:forEach var="item" items="${usedMaterial}">
																	<li style="overflow:hidden; margin:5px 0;">
																		<input type="hidden" class="inp-comm"  name="equipmentBomUsedArr" value="${item.EQUIPMENT_BOM}"	readonly="readonly">
																		<input type="hidden" class="inp-comm"  name="materialUsedArr" value="${item.MATERIAL}"	readonly="readonly">
																		<div class="box-col wd-per-20 f-l">
																			<div class="inp-wrap readonly">
																				<input type="text" class="inp-comm" name="" value="${item.MATERIAL_UID}"	readonly="readonly">
																			</div>
																		</div>
																		<div class="box-col wd-per-20 f-l">
																			<div class="inp-wrap readonly mgn-l-5">
																				<input type="text" class="inp-comm"  name="" value="${item.MATERIAL_NAME}"	readonly="readonly">
																			</div>
																		</div>
																		<input type="hidden" class="inp-comm"  name="" value="${item.UNIT}"	readonly="readonly">
																		<div class="box-col wd-per-20 f-l">
																			<div class="inp-wrap readonly mgn-l-5">
																				<input type="text" class="inp-comm"  name="" value="${item.UNIT_NAME}"	readonly="readonly">
																			</div>
																		</div>
																		<div class="box-col wd-per-20 f-l">
																			<div class="inp-wrap mgn-l-5">
																				<input type="text" class="inp-comm" name="qntyUsedArr"	value="${item.QNTY}"	onchange="check_Null(this);"	onkeydown="check_isNum_onkeydown();" autocomplete="off">
																			</div>
																		</div>
																	</li>
																</c:forEach>
															</ul>
														</td>
													</tr>
													<tr>
														<th scope="row">사용자재(미등록)</th>
														<td colspan="3">
															<div class="inp-wrap wd-per-100">
																<input type="text" class="inp-comm" id="popPreventiveRegForm_MATERIAL_UNREG" name="MATERIAL_UNREG" value="<c:out value="${preventiveResultDtl.MATERIAL_UNREG}" escapeXml="true" />"/>
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">점검내용</th>
														<td colspan="3">
															<div class="txt-wrap">
																<div class="txtArea">
																	<textarea cols="" rows="3" id="CHECK_DESCRIPTION" name="CHECK_DESCRIPTION"><c:out value="${preventiveResultDtl.CHECK_DESCRIPTION}" escapeXml="true" /></textarea>
																</div>
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">측정치</th>
														<td>
															<div class="inp-wrap wd-per-100">
																<input type="text" class="inp-comm" id="popRepairResultRegForm_CHECK_VALUE" name="CHECK_VALUE" value="${preventiveResultDtl.CHECK_VALUE}"/>
															</div>
														</td>
														<c:if test="${preventiveResultDtl.CHECK_TYPE eq 01}">
															<th scope="row">판정</th>
															<td>
																<div class="sel-wrap">
																	<tag:combo codeGrp="CHECK_DECISION" name="CHECK_DECISION" choose="Y"/>
																</div>
															</td>
														</c:if>
													</tr>
													<tr id="partTR" style="display:none;">
														<th>처리파트</th>
														<td colspan="3">
															<div class="sel-wrap">
																<tag:combo codeGrp="PART" name="REQUEST_PART" companyId="${ssCompanyId}" choose="Y"/>
															</div>
														</td>
													</tr>
													<tr id="workTR" style="display:none;">
														<th>정비담당자</th>
														<td colspan="3">
															<div class="sel-wrap" id="workSelect">
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">첨부파일</th>
														<td colspan="3" style="padding:3px 15px 0;">
															<div class="bxType01 fileWrap">
																<div class="filebox" id="attachFile"></div>
																<div class="fileList" style="padding:7px 0 5px;">
																	<div id="detailFileList0" class="fileDownLst">
																		
																	</div>
																</div>
															</div>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> <%-- modal-bodyIn : e --%>
		</div> <%-- modal-body : e --%>
	</div> <%-- modal-content : e --%>
</div> <%-- modal : e --%>