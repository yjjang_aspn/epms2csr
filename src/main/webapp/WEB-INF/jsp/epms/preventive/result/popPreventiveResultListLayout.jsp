<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: popPreventiveResultListLayout.jsp
	Description : 예방보전실적등록 : 그리드 레이아웃
    author		: 김영환
    since		: 2018.05.29
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.29	 김영환		최초 생성

--%>

<c:set var="gridId" value="PopPreventiveResultList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		Filtering="1"				Dragging="0"
			Editing="1"				Deleting="0"		Selecting="0"		SuppressCfg="0"
			Paging="1"			  	PageLength="20"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="0"			PasteFocused="3"	CustomScroll="1"
	/>
	
	<Head>
		<Header	id="Header"	Align="center" CanExport="1" CanHide="0"
			PREVENTIVE_RESULT		= "예방보전ID"
		DATE_PLAN				= "계획일"
		DATE_ASSIGN				= "점검계획일"
		STATUS					= "상태"
		MANAGER					= "담당자ID"
		MANAGER_NAME			= "담당자"
		
		PREVENTIVE_BOM			= "예방보전BOMID"
		WORK_TYPE				= "오더유형ID"
		WORK_TYPE_NAME			= "오더유형"
		PART					= "파트"
		PREVENTIVE_TYPE			= "업무구분"
		CHECK_TYPE				= "점검구분"
		CHECK_DETAIL			= "점검항목"
		CHECK_STANDARD			= "판단기준"
		CHECK_TOOL				= "진단장비"
		CHECK_CYCLE				= "주기"
		
		EQUIPMENT				= "설비ID"
		EQUIPMENT_UID			= "설비코드"
		EQUIPMENT_NAME			= "설비명"
		GRADE					= "설비등급"
		LOCATION				= "위치"
		LINE					= "라인ID"
		LINE_UID				= "기능위치"
		LINE_NAME				= "라인"
		
		LOCATIONPART			= "공장파트"
		
		ATTACH_GRP_NO			= "파일그룹번호"
		CHECK_DECISION			= "판정"
		CHECK_VALUE				= "측정치"
		CHECK_DESCRIPTION		= "점검내용"
		MATERIAL_USED			= "사용자재"
		MATERIAL_UNREG			= "사용자재(미등록)"
		
		detail					= "실적상세"
		/>
	</Head>
	
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4" CanHide="0"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>

	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
		
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
	
	<Cols>
		<C Name="PREVENTIVE_RESULT"			Type="Text"		Align="left"		Visible="0"		Width="100"	CanEdit="0"/>
		<C Name="LOCATION"					Type="Enum"		Align="left"		Visible="0"		Width="60"	CanEdit="0" <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>	
		<C Name="STATUS"					Type="Enum"		Align="left"		Visible="1"		Width="90"	CanEdit="0" <tag:enum codeGrp="PREVENTIVE_STATUS"/>/>
		<C Name="LINE"						Type="Text"		Align="center"		Visible="0"		Width="60"	CanEdit="0"/>
		<C Name="LINE_UID"					Type="Text"		Align="left"		Visible="0"		Width="140"	CanEdit="0"/>
		<C Name="LINE_NAME"					Type="Text"		Align="left"		Visible="1"		Width="140"	CanEdit="0"/>
		<C Name="EQUIPMENT"					Type="Text"		Align="left"		Visible="0"		Width="100"	CanEdit="0"/>
		<C Name="EQUIPMENT_UID"				Type="Text"		Align="left"		Visible="0"		Width="80"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT_NAME"			Type="Text"		Align="left"		Visible="1"		Width="220"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="WORK_TYPE"					Type="Text"		Align="center"		Visible="0"		Width="60"	CanEdit="0"/>
		<C Name="WORK_TYPE_NAME"			Type="Text"		Align="left"		Visible="1"		Width="140"	CanEdit="0"/>
		<C Name="PREVENTIVE_TYPE"			Type="Enum"		Align="left"		Visible="1"		Width="90"	CanEdit="0" <tag:enum codeGrp="PREVENTIVE_TYPE"/>/>
		<C Name="PART"						Type="Enum"		Align="left"		Visible="0"		Width="60"	CanEdit="0" <tag:enum codeGrp="PART" companyId="${ssCompanyId}"/>/>
		<C Name="CHECK_TYPE"				Type="Enum"		Align="left"		Visible="1"		Width="90"	CanEdit="0" <tag:enum codeGrp="CHECK_TYPE"/>/>
		<C Name="GRADE"						Type="Text"		Align="left"		Visible="0"		Width="60"	CanEdit="0"/>
		<C Name="CHECK_DETAIL"				Type="Text"		Align="left"		Visible="1"		Width="150"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="CHECK_STANDARD"			Type="Text"		Align="left"		Visible="1"		Width="120"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="CHECK_TOOL"				Type="Text"		Align="left"		Visible="1"		Width="120"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="CHECK_CYCLE"				Type="Int"		Align="center"		Visible="1"		Width="110"	CanEdit="0"/>
		<C Name="LOCATIONPART"				Type="Text"		Align="left"		Visible="0" 	Width="80"	CanEdit="0"	CanHide="0"	CanFilter="0" />
		<C Name="MANAGER"					Type="Text"		Align="left"		Visible="0"		Width="100"	CanEdit="0" />
		<C Name="MANAGER_NAME"				Type="Text"		Align="left"		Visible="1"		Width="70"	CanEdit="0" />
		<C Name="DATE_PLAN"					Type="Date"		Align="center"		Visible="1"		Width="100"	CanEdit="0"	Format="yyyy/MM/dd" />
		<C Name="DATE_ASSIGN"				Type="Date"		Align="center"		Visible="1"		Width="100"	CanEdit="0"	Format="yyyy/MM/dd"  OnChange="batchDatePlan(Grid,Row,Col);"/>
	</Cols>
	
	<RightCols>
		<C Name="ATTACH_GRP_NO"				Type="Text"		Align="left"		Visible="0"		Width="140"	CanEdit="0"/>
		<C Name="CHECK_DECISION"			Type="Enum"		Align="center"		Visible="1"		Width="70"	CanEdit="0" <tag:enum codeGrp="CHECK_DECISION"/>/>
		<C Name="CHECK_VALUE"				Type="Text"		Align="left"		Visible="1"		Width="140"	CanEdit="0"/>
		<C Name="CHECK_DESCRIPTION"			Type="Text"		Align="left"		Visible="1"		Width="140"	CanEdit="0"/>
		<C Name="MATERIAL_USED"				Type="Text"		Align="left"		Visible="1"		Width="150"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="MATERIAL_UNREG"			Type="Text"		Align="left"		Visible="1"		Width="150"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		
		<C Name="detail"					Type="Icon"		Align="center"		Visible="1"		Width="70"	CanEdit="0"	CanExport="1"/>
	</RightCols>	
			
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,항목,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        " 
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
	
	
</Grid>