<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%--
	Class Name	: preventiveResultList.jsp
	Description : 예방보전 > 예방보전실적등록/조회 화면
	author		: 김영환
	since		: 2018.05.09
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.05.09	김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<style>
	.tb-st ul{margin:0; padding:0;}
	.tb-st ul>li{overflow:hidden; margin-top:5px;}
	.tb-st ul>li>input{float:left;}
	.tb-st ul>li>div{float:left;}
	
	.btnRemove{display:inline-block; height:28px; line-height:28px; padding:0 10px; background:#6eb4e5; color:#ffffff;}
</style>

<script type="text/javascript">

<%-- 전역변수 --%>
var focusedRow = null;		<%-- Focus Row : [예방보전 현황] --%>

$(document).ready(function(){
	<%-- 검색 시작일이 종료일보다 이후인지 체크 (이후라면, 종료일을 시작일로 변경) --%>
	$('#startDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#endDt').val($('#startDt').val());
		}
	});
	
	<%-- 검색 종료일이 시작일보다 이전인지 체크 (이전이라면, 시작일을 종료일로 변경) --%>
	$('#endDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#startDt').val($('#endDt').val());
		}
	});
	
	<%-- 예방보전실적현황 '검색'버튼 클릭 --%>
	$('#srcBtn').on('click', function(){
		Grids.PreventiveResultList.Source.Data.Url = "/epms/preventive/result/preventiveResultListData.do?srcType=" + $('#srcType').val()
											   + "&startDt=" + $('#startDt').val()
											   + "&endDt=" + $('#endDt').val();
		Grids.PreventiveResultList.ReloadBody();
	});
	
	<%-- datePic 설정 --%>
	$('.datePic').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	});

	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function (e) {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>
	});
});

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "PreventiveResultList") { <%-- [예방보전 현황] 클릭시 --%>
		<%-- 포커스 시작 --%>
		if(focusedRow != null){
			grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		focusedRow = row;
		
	}
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

function fnReload(msg){
	alert(msg);
	Grids.PreventiveResultList.Source.Data.Url = "/epms/preventive/result/preventiveResultListData.do?srcType=" + $('#srcType').val()
											   + "&startDt=" + $('#startDt').val()
											   + "&endDt=" + $('#endDt').val();
	Grids.PreventiveResultList.ReloadBody();
}

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source,data, func){
	if(grid.id == "PreventiveResultList"){
		if(data != ""){
			callLoadingBar();
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "PreventiveResultList"){
					setTimeout($.unblockUI, 100);
				}
			}
		}
	}
}


</script>
</head>
<body>
<div id="contents">
	<input type="hidden" id="MATERIAL" name="MATERIAL">				<%-- 자재 ID --%>
	
	<div class="fl-box panel-wrap04" style="width:100%;">
		<h5 class="panel-tit" id="repairRequestApprListTitle">예방보전 현황</h5>
		<div class="inq-area-inner type06 ab">
			<div class="wrap-inq">
				<div class="inq-clmn">
					<h4 class="tit-inq">구분</h4>
					<div class="sel-wrap type02">
						<select title="구분" id="srcType" name="srcType">
							<option value="1" selected="selected">점검계획일</option>
							<option value="2">실적등록일</option>
						</select>
					</div>
				</div>
				<div class="inq-clmn">
					<h4 class="tit-inq">검색일</h4>
					<div class="prd-inp-wrap">
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic thisMonth inpCal" readonly="readonly" id="startDt" title="검색시작일" value="${startDt}" >
							</span>
						</span>
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic inpCal" readonly="readonly" id="endDt" title="검색종료일" value="${endDt}">
							</span>
						</span>		
					</div>
				</div>
			</div>
			<a href="#none" id="srcBtn" class="btn comm st01">검색</a>
		</div>
		<div class="panel-body mgn-t-20">
			<div id="PreventiveResultList">
				<bdo	Debug="Error"
						Data_Url="/epms/preventive/result/preventiveResultListData.do?srcType=1&startDt=${startDt}&endDt=${endDt}"
						Layout_Url="/epms/preventive/result/preventiveResultListLayout.do?PART=${part}&LOCATION=${location}"
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=예방보전실적등록/조회 목록.xls&dataName=data"
				>
				</bdo>
			</div>
		</div>
	</div>
</div> <%-- contents 끝 --%>

<%-- 입고조회 --%>
<%@ include file="/WEB-INF/jsp/epms/material/master/popMaterialInputView.jsp"%>	

<%-- 출고조회 --%>
<%@ include file="/WEB-INF/jsp/epms/material/master/popMaterialOutputView.jsp"%>	

<%-- 예방보전실적등록 --%>
<div class="modal fade modalFocus" id="popPreventiveResultRegForm" data-backdrop="static" data-keyboard="false"></div>
<%-- 예방보전실적조회 --%>
<div class="modal fade modalFocus" id="popPreventiveResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 부품명 검색 --%>
<div class="modal fade modalFocus" id="popRepairMaterialList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-50">
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">부품명 검색</h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	           		<div class="modal-section">
						<div class="fl-box wd-per-100">
							<bdo	Debug="Error"
					 				Data_Url=""
									Layout_Url="/epms/repair/result/popRepairMaterialListLayout.do" 
							>
							</bdo>
						</div>
					</div> <%-- modal-section : e --%>
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>			