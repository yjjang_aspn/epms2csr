<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%--
	Class Name	: popPreventiveResultRegForm.jsp
	Description : 예방보전관리 > 예방보전실적 등록 레이어 팝업
	author		: 김영환
	since		: 2018.05.09
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.05.09	 김영환		최초 생성

--%>
<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">
var submitBool = true;
var maxFileCnt = 30;
var msg = "";

$(document).ready(function(){
	
	<%-- 첨부파일 관련 --%>
	for(var i=0; i<1; i++){				<%-- 파일 영역 추가를 고려하여 개발 --%>
		$('#attachFile').append('<input type="file" multiple id="fileList" name="fileList">');	
		
		var fileType = 'jpeg |gif |jpg |png |bmp |ppt |pptx |doc |hwp |pdf |xlsx |xls';
		//var fileType = 'jpeg |gif |jpg |png |bmp';
	
		$('#fileList').MultiFile({
			   accept :  fileType
			,	list   : '#detailFileList'+i
			,	max    : 5
			,	STRING : {
					remove: '<img src="/images/com/web/btn_remove.png"/>'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
					text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" />',
					idx	  : i,
				},
		});
		
		<%-- 첨부한 파일의 출력 순서를 자동으로 샛팅 하기 위한 스크립트 - 공통 스크립트로 빠지면 안됨 .개별 항목으로 처리 필수 --%> 
		$('.MultiFile-wrap').change(function(){
			var fileExpIndex = $(this).closest('.fileWrap').find('.fileList > div').children('div:last-child');
			var	fileExp = fileExpIndex.find('.fileExp');
			fileExp.val(fileExpIndex.index() + 1);
		});
	}
	
	<%-- 파일 다운로드 --%>
	$('.btnFileDwn').on('click', function(){
		var ATTACH_GRP_NO = $("#ATTACH_GRP_NO").val();
		var ATTACH = $(this).data("attach");
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO='+ATTACH_GRP_NO+'&ATTACH='+ATTACH;
		$("#fileDownFrame").attr("src",src);
	});
	
	<%-- 압축 파일 다운로드 --%>
	$('.zipFileBtn').on('click', function(){
		var ATTACH_GRP_NO = $("#ATTACH_GRP_NO").val();
		var src = '/attach/zipFileDownload.do?ATTACH_GRP_NO=' + ATTACH_GRP_NO + '&zipFileName=PreventiveResult';
		$("#fileDownFrame").attr("src",src);
	});
	
	<%-- 첨부파일 삭제버튼 --%>
	$('.icn_fileDel').on('click', function(){
		delModiFile($(this));
	});
	
	<%-- 고장시작시간 --%>
	var date = new Date();
	if("${repairResultDtl.TROUBLE_TIME1}" == "" || "${repairResultDtl.TROUBLE_TIME1}" == null){	
		$('#popRepairResultForm_TROUBLE_TIME1_DATE').val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		$('#popRepairResultForm_TROUBLE_TIME1_HOUR').val(getDate(date.getHours()));
		var min = parseInt(date.getMinutes());
		min = min - (min%5);
		var minstr;
		if(min<10){
			minstr = "0" + min.toString();
		}else{
			minstr = min.toString();
		}
		$('#popRepairResultForm_TROUBLE_TIME1_MINUTE').val(minstr);
	}
	else{
		var trouble_time1 = "${repairResultDtl.TROUBLE_TIME1}";
		$('#popRepairResultForm_TROUBLE_TIME1_DATE').val(trouble_time1.substring(0,4)+"-"+trouble_time1.substring(4,6)+"-"+trouble_time1.substring(6,8));
		$('#popRepairResultForm_TROUBLE_TIME1_HOUR').val(trouble_time1.substring(8,10));
		$('#popRepairResultForm_TROUBLE_TIME1_MINUTE').val(trouble_time1.substring(10,12));
	}
	<%-- 고장종료시간 --%>
	if("${repairResultDtl.TROUBLE_TIME2}" == "" || "${repairResultDtl.TROUBLE_TIME2}" == null){
		$('#popRepairResultForm_TROUBLE_TIME2_DATE').val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		$('#popRepairResultForm_TROUBLE_TIME2_HOUR').val(getDate(date.getHours()));
		var min = parseInt(date.getMinutes());
		min = min - (min%5);
		var minstr;
		if(min<10){
			minstr = "0" + min.toString();
		}else{
			minstr = min.toString();
		}
		$('#popRepairResultForm_TROUBLE_TIME2_MINUTE').val(minstr);
	}
	else{
		var trouble_time2 = "${repairResultDtl.TROUBLE_TIME2}";
		$('#popRepairResultForm_TROUBLE_TIME2_DATE').val(trouble_time2.substring(0,4)+"-"+trouble_time2.substring(4,6)+"-"+trouble_time2.substring(6,8));
		$('#popRepairResultForm_TROUBLE_TIME2_HOUR').val(trouble_time2.substring(8,10));
		$('#popRepairResultForm_TROUBLE_TIME2_MINUTE').val(trouble_time2.substring(10,12));
	}
});

<%-- 이미지 원본크기 보기 --%>
function originFileView(img){ 
	img1= new Image(); 
	img1.src=(img); 
	imgControll(img); 
} 
	  
function imgControll(img){ 
	if((img1.width!=0)&&(img1.height!=0)){ 
		viewImage(img); 
	}else{ 
		controller="imgControll('"+img+"')"; 
		intervalID=setTimeout(controller,20); 
	} 
}

function viewImage(img){ 
	W=img1.width; 
	H=img1.height; 
	O="width="+W+",height="+H+",scrollbars=yes"; 
	imgWin=window.open("","",O); 
	imgWin.document.write("<html><head><title>이미지상세보기</title></head>");
	imgWin.document.write("<body topmargin=0 leftmargin=0>");
	imgWin.document.write("<img src="+img+" onclick='self.close()' style='cursor:pointer;' title ='클릭하시면 창이 닫힙니다.'>");
	imgWin.document.close();
}

</script>
<div class="modal-dialog root wd-per-60">
	<div class="modal-content">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title">예방보전실적 조회</h4>
		</div>
	 	<div class="modal-body overflow" >
           	<div class="modal-bodyIn hgt-per-100" style="padding:0;">
           		<div class="popTabSet">
					<ul class="tabs">
						<li id="repairRequestTab"><a href="#panel2-1" class="on">예방보전실적</a></li>
						<c:if test="${preventiveResultDtl.CHECK_DECISION eq '03' && preventiveResultDtl.STATUS eq '03'}">
						<li id="repairResultTab"><a href="#panel2-2">정비실적</a></li>
						</c:if>
					</ul>
					<div class="panels">
						<div class="panel type01" id="panel2-1">
							<div class="panel-in rel">	
								<form id="regForm" name="regForm"  method="post" enctype="multipart/form-data">	
									<input type="hidden" id="ATTACH_GRP_NO" value="${preventiveResultDtl.ATTACH_GRP_NO }">
									<div class="tb-wrap">
										<table class="tb-st">
											<caption class="screen-out">상세</caption>
											<colgroup>
												<col width="15%" />
												<col width="35%" />
												<col width="15%" />
												<col width="*" />
											</colgroup>
											<tbody>
												<tr>
													<th scope="row">공장</th><!-- 공장 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" name="LINE_NAME" readonly="readonly" value="${preventiveResultDtl.LOCATION_NAME}"/>
														</div>
													</td>
													<th scope="row">라인</th><!-- 라인 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" name="LINE_NAME" readonly="readonly" value="${preventiveResultDtl.LINE_NAME}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">설비코드</th><!-- 설비코드 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" name="EQUIPMENT_UID" readonly="readonly" value="${preventiveResultDtl.EQUIPMENT_UID}"/>
														</div>
													</td>
													<th scope="row">설비명</th><!-- 설비명 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" name="EQUIPMENT_NAME" readonly="readonly" value="${preventiveResultDtl.EQUIPMENT_NAME}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">상태</th><!-- 상태 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" name="STATUS_NAME" readonly="readonly" value="${preventiveResultDtl.STATUS_NAME}"/>
														</div>
													</td>
													<th scope="row">업무구분</th><!-- 업무구분 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" name="PREVENTIVE_TYPE_NAME" readonly="readonly" value="${preventiveResultDtl.PREVENTIVE_TYPE_NAME}"/>
														</div>
													</td>
													
												</tr>
												<tr>
													<th scope="row">작업유형</th><!-- 작업유형 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" name="WORK_TYPE_NAME" readonly="readonly" value="${preventiveResultDtl.WORK_TYPE_NAME}"/>
														</div>
													</td>
													<th scope="row">점검구분</th><!-- 점검구분 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" name="CHECK_TYPE_NAME" readonly="readonly" value="${preventiveResultDtl.CHECK_TYPE_NAME}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">점검항목</th><!-- 점검항목 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" name="CHECK_DETAIL" readonly="readonly" value="${preventiveResultDtl.CHECK_DETAIL}"/>
														</div>
													</td>
													<th scope="row">판단기준</th><!-- 판단기준 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" name="CHECK_STANDARD" readonly="readonly" value="${preventiveResultDtl.CHECK_STANDARD}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">진단장비</th><!-- 진단장비 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" name="CHECK_TOOL" readonly="readonly" value="${preventiveResultDtl.CHECK_TOOL}"/>
														</div>
													</td>
													<th scope="row">주기</th><!-- 주기 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" name="CHECK_CYCLE" readonly="readonly" value="${preventiveResultDtl.CHECK_CYCLE}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">계획일</th><!-- 계획일 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" name="DATE_PLAN" readonly="readonly" value="${preventiveResultDtl.DATE_PLAN}"/>
														</div>
													</td>
													<th scope="row">점검계획일</th><!-- 점검계획일 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" name="DATE_ASSIGN" readonly="readonly" value="${preventiveResultDtl.DATE_ASSIGN}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">담당자</th><!-- 담당자 -->
													<td colspan="3">
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" name="MANAGER_NAME" readonly="readonly" value="${preventiveResultDtl.MANAGER_NAME}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">사용자재</th><!-- 사용자재 -->
													<td colspan="3">
														<input type="hidden" class="inp-comm" id="MATERIAL_INOUT" name="MATERIAL_INOUT" value="${repairResultDtl.MATERIAL_INOUT}"/>
														<ul class="usedMaterial" id="usedMaterial">
															<c:forEach var="item" items="${usedMaterial2}">
																<li style="overflow:hidden; margin:5px 0;">
																	<input type="hidden" class="inp-comm"  name="equipmentBomUsedArr" value="${item.EQUIPMENT_BOM}"	readonly="readonly">
																	<input type="hidden" class="inp-comm"  name="materialUsedArr" value="${item.MATERIAL}"	readonly="readonly">
																	<div class="box-col wd-per-20 f-l">
																		<div class="inp-wrap readonly wd-per-100">
																			<input type="text" class="inp-comm" name="" value="${item.MATERIAL_UID}"	readonly="readonly">
																		</div>
																	</div>
																	<div class="box-col wd-per-20 f-l mgn-l-5">
																		<div class="inp-wrap readonly wd-per-100">
																			<input type="text" class="inp-comm" name="" value="${item.MATERIAL_NAME}"	readonly="readonly">
																		</div>
																	</div>
																	<input type="hidden" class="inp-comm" name="" value="${item.UNIT}"	readonly="readonly">
																	<div class="box-col wd-per-20 f-l mgn-l-5">
																			<div class="inp-wrap readonly wd-per-100">
																			<input type="text" class="inp-comm" name="" value="${item.UNIT_NAME}"	readonly="readonly">
																		</div>
																	</div>
																	<div class="box-col wd-per-10 f-l mgn-l-5">
																		<div class="inp-wrap readonly">
																			<input type="text" class="inp-comm" name="qntyUsedArr"	value="${item.QNTY}"	readonly="readonly">
																		</div>
																	</div>
																</li>
															</c:forEach>
														</ul>
													</td>
												</tr>
												<tr>
													<th scope="row">사용자재(미등록)</th><!-- 사용자재(미등록) -->
													<td colspan="3">
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" readonly="readonly" name="MATERIAL_UNREG" value="<c:out value="${preventiveResultDtl.MATERIAL_UNREG}" escapeXml="true" />"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">점검내용</th><!-- 점검내용 -->
													<td colspan="3">
														<div class="txt-wrap" style="padding:0;">
															<div class="txtArea readonly" style="padding:4px 7px">
																<textarea cols="" rows="3" readonly="readonly" name="CHECK_DESCRIPTION"><c:out value="${preventiveResultDtl.CHECK_DESCRIPTION}" escapeXml="true" /></textarea>
															</div>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">측정치</th><!-- 측정치 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" readonly="readonly" name="CHECK_VALUE" value="${preventiveResultDtl.CHECK_VALUE}"/>
														</div>
													</td>
													<c:if test="${preventiveResultDtl.CHECK_TYPE eq 01}">
														<th scope="row">판정</th>
														<td>
															<div class="inp-wrap readonly wd-per-100">
																<input type="text" class="inp-comm" readonly="readonly" name="CHECK_DECISION" value="${preventiveResultDtl.CHECK_DECISION_NAME}"/>
															</div>
														</td>
													</c:if>
												</tr>
												<c:if test="${preventiveResultDtl.CHECK_DECISION eq 03}">
												<tr id="partTR">
													<th>처리파트</th><!-- 처리파트 -->
													<td colspan="3">
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" readonly="readonly" name="PART_NAME" value="${repairRequestDtl.PART_NAME}"/>
														</div>
													</td>
												</tr>
												<tr id="workTR">
													<th>정비담당자</th><!-- 정비담당자 -->
													<td colspan="3">
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" readonly="readonly" name="REQUEST_MANAGER_NAME" value="${repairRequestDtl.MANAGER_NAME}"/>
														</div>
													</td>
												</tr>
												</c:if>
												<tr>
													<th scope="row">첨부파일</th><!-- 첨부파일 -->
													<td colspan="3">
														<c:choose>
															<c:when test="${fn:length(attachList1)>0 }">
															<div id="previewList" class="wd-per-100">
																<div class="bxType01 fileWrap previewFileLst">
																	<div>
																		<div class="fileList type02">
																			<div id="detailFileList0" class="fileDownLst">
																				<c:forEach var="item" items="${attachList1}" varStatus="idx">
																					<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																						<span class="MultiFile-export">${item.SEQ_DSP}</span> 
																						<span class="MultiFile-title" title="File selected: ${item.NAME}">
																						${item.FILE_NAME} (${item.FILE_SIZE_TXT})
																						</span>
																						<a href="#none" class="btnFileDwn icon download" data-attach="${item.ATTACH}">다운로드</a>
																					</div>		
																				</c:forEach>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
												
															<div id="previewImg" class="wd-per-100">
																<div class="previewFile mgn-t-15">
																	<h3 class="tit-cont preview" id="previewTitle">이미지 미리보기</h3>
																	<ul style="margin-top:5px;">
																		<c:forEach var="item" items="${attachList1}" varStatus="idx">
																			<c:if test = "${item.FORMAT eq 'jpg' || item.FORMAT eq 'png' || item.FORMAT eq 'jpeg' || item.FORMAT eq 'JPG' || item.FORMAT eq 'PNG'}">
																				<li>
																					<img src="/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}" title="클릭하시면 원본크기로 보실 수 있습니다." onclick="originFileView('/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}')" />
																					<p>${item.FILE_NAME} <a href="#none" class="btnFileDwn icon download" data-attach="${item.ATTACH}">다운로드</a></p>
																					
																				</li>
																			</c:if>
																		</c:forEach>
																	</ul>
																</div>
															</div>
															</c:when>
															<c:otherwise>
																<div class="f-l wd-per-100 mgn-t-5">
																※ 등록된 첨부파일이 없습니다.
																</div>
															</c:otherwise>
														</c:choose>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</form>
							</div>
						</div>
						<%-- 정비내용 판넬 : s --%>
						<div class="panel type01" id="panel2-2">
							<div class="panel-in rel">
								<div class="tb-wrap">
									<table class="tb-st">
										<caption class="screen-out">정비실적 상세</caption><!-- 정비실적 상세 -->
										<colgroup>
											<col width="15%" />
											<col width="35%" />
											<col width="15%" />
											<col width="*" />
										</colgroup>
										<tbody>
											<tr>
												<th scope="row">정비유형</th><!-- 정비유형 -->
												<td>
													<div class="inp-wrap readonly wd-per-100">
														<input type="hidden" class="inp-comm" id="REPAIR_TYPE" name="REPAIR_TYPE" value="${repairResultDtl.REPAIR_TYPE}"/>
														<input type="text" class="inp-comm" id="repair_type_name" name="repair_type_name" readonly="readonly" value="${repairResultDtl.REPAIR_TYPE_NAME}"/>
													</div>
												</td>
												<th scope="row">외주</th>
												<td>
													<div class="inp-wrap readonly wd-per-100">
														<input type="text" class="inp-comm" id="OUTSOURCING" name="OUTSOURCING" readonly="readonly" value="<c:out value="${repairResultDtl.OUTSOURCING}" escapeXml="true" />"/>
													</div>
												</td>
											</tr>
						 					<tr>
												<th scope="row">정비원</th><!-- 정비원 -->
												<td colspan="3">
													<ul class="repairMemberList repairMember" id="repairMember">
														<c:forEach var="item" items="${repairRegMember}">
															<li style="overflow:hidden;">
																<div class="userInfo">
																	<div class="user_img">
																		<img src="${item.MEMBER_FILEURL}" onerror="this.src='/images/com/web_v2/userIcon.png'" />
																	</div>
																	<div class="user_name">
																		<div class="name">
																			<input type="hidden" class="inp-comm" title="정비원ID" name="memberArr" value="${item.MEMBER}" readonly="readonly">
																			${item.MEMBER_NAME}
																			<c:if test="${item.USER_ID eq repairResultDtl.MANAGER}">
																				<p class="manager">담당자</p>
																			</c:if>
																		</div>
																		<div class="dep">
																			<span>
																				<input type="hidden" class="inp-comm" title="부서ID" name="" value="${item.DIVISION}"	readonly="readonly">
																				${item.DIVISION_NAME}
																			</span>
																			<span style="padding:0 5px;">/</span>
																			<span>
																				<input type="hidden" class="inp-comm" title="파트" name="" value="${item.PART}"	readonly="readonly">
																				${item.PART_NAME}
																			</span>
																		</div>
																	</div>
																</div>
															</li>	
														</c:forEach>
													</ul>
												</td>
											</tr>						
											<tr>
												<th scope="row">사용자재</th><!-- 사용자재 -->
												<td colspan="3">
													<input type="hidden" class="inp-comm" id="MATERIAL_INOUT" name="MATERIAL_INOUT" value="${repairResultDtl.MATERIAL_INOUT}"/>
													<ul class="usedMaterial" id="usedMaterial">
														<c:forEach var="item" items="${usedMaterial1}">
															<li style="overflow:hidden; margin:5px 0;">
																<input type="hidden" class="inp-comm" title="BOMID" name="equipmentBomUsedArr" value="${item.EQUIPMENT_BOM}"	readonly="readonly">
																<input type="hidden" class="inp-comm" title="자재ID" name="materialUsedArr" value="${item.MATERIAL}"	readonly="readonly">
																<div class="box-col wd-per-20 f-l">
																	<div class="inp-wrap readonly wd-per-100">
																		<input type="text" class="inp-comm" title="장치명" name="" value="${item.MATERIAL_UID}"	readonly="readonly">
																	</div>
																</div>
																<div class="box-col wd-per-20 f-l mgn-l-5">
																	<div class="inp-wrap readonly wd-per-100">
																		<input type="text" class="inp-comm" title="자재명" name="" value="${item.MATERIAL_NAME}"	readonly="readonly">
																	</div>
																</div>
																<input type="hidden" class="inp-comm" title="단위" name="" value="${item.UNIT}"	readonly="readonly">
																<div class="box-col wd-per-20 f-l mgn-l-5">
																	<div class="inp-wrap readonly wd-per-100">
																		<input type="text" class="inp-comm" title="단위명" name="" value="${item.UNIT_NAME}"	readonly="readonly">
																	</div>
																</div>
																<div class="box-col wd-per-10 f-l mgn-l-5">
																	<div class="inp-wrap">
																		<input type="text" class="inp-comm t-r" title="수량" name="qntyUsedArr"	value="${item.QNTY}"	readonly="readonly">
																	</div>
																</div>
															</li>
														</c:forEach>
													</ul>
												</td>
											</tr>
											<tr>
												<th scope="row">사용자재(미등록)</th><!-- 사용자재(미등록) -->
												<td colspan="3">
													<div class="inp-wrap readonly wd-per-100">
														<input type="text" class="inp-comm" id="MATERIAL_UNREG" name="MATERIAL_UNREG"  readonly="readonly" value="<c:out value="${repairResultDtl.MATERIAL_UNREG}" escapeXml="true" />"/>
													</div>
												</td>
											</tr>
											<tr>
												<th scope="row">고장유형</th><!-- 고장유형 -->
												<td>
													<div class="inp-wrap readonly wd-per-100">
														<c:forEach var="item" items="${troubleList1}" >
															<c:if test="${repairResultDtl.TROUBLE_TYPE1 eq item.CODE}">
																<input type="text" class="inp-comm" id="TROUBLE_TYPE1" name="TROUBLE_TYPE1"  readonly="readonly" value="${item.NAME}" />
															</c:if>
														</c:forEach>
													</div>
												</td>
												<th scope="row">조치</th><!-- 조치 -->
												<td>
													<div class="inp-wrap readonly wd-per-100">
														<c:forEach var="item" items="${troubleList2}" >
															<c:if test="${repairResultDtl.TROUBLE_TYPE2 eq item.CODE}">
																<input type="text" class="inp-comm" id="TROUBLE_TYPE1" name="TROUBLE_TYPE1"  readonly="readonly" value="${item.NAME}" />
															</c:if>
														</c:forEach>
													</div>
												</td>
											</tr>
											<tr>
												<th scope="row">작업내용</th><!-- 작업내용 -->
												<td colspan="3">
													<div class="txt-wrap readonly">
														<div class="txtArea">
															<textarea cols="" rows="5" title="정비내용" id="REPAIR_DESCRIPTION" name="REPAIR_DESCRIPTION" readonly="readonly"><c:out value="${repairResultDtl.REPAIR_DESCRIPTION}" escapeXml="true" /></textarea>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<th scope="row">고장시작시간</th><!-- 고장시작시간 -->
												<td>
													<div class="inp-wrap readonly wd-per-100">
														<input type="hidden" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME1" name="TROUBLE_TIME1" title="고장시작일">
														<input type="text" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME1_DATE" name="TROUBLE_TIME1_DATE" title="고장시작일">
													</div>
													<div class="inp-wrap readonly wd-per-40 rel mgn-t-5 f-l">
														<input type="text" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME1_HOUR" name="TROUBLE_TIME1_HOUR" title="고장시작시간">
														<span style="position:absolute; top:5px; right:-15px;">시</span>
													</div>
													<div class="inp-wrap readonly wd-per-40 rel mgn-t-5 mgn-l-30 f-l">
														<input type="text" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME1_MINUTE" name="TROUBLE_TIME1_MINUTE" title="고장시작분">
														<span style="position:absolute; top:5px; right:-15px;">분</span>
													</div>
												</td>
												<th scope="row">고장종료시간</th><!-- 고장종료시간 -->
												<td>
													<div class="inp-wrap readonly wd-per-100">
														<input type="hidden" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME2" name="TROUBLE_TIME2" title="고장종료일">
														<input type="text" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME2_DATE" name="TROUBLE_TIME2_DATE" title="고장종료일">
													</div>
													<div class="inp-wrap readonly wd-per-40 rel mgn-t-5 f-l">
														<input type="text" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME2_HOUR" name="TROUBLE_TIME2_HOUR" title="고장종료시간">
														<span style="position:absolute; top:5px; right:-15px;">시</span>
													</div>
													<div class="inp-wrap readonly wd-per-40 rel mgn-t-5 mgn-l-30 f-l">
														<input type="text" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME2_MINUTE" name="TROUBLE_TIME2_MINUTE" title="고장종료분">
														<span style="position:absolute; top:5px; right:-15px;">분</span>
													</div>
												</td>
											</tr>
											<tr>
												<th scope="row">정비 첨부파일</th><!-- 정비 첨부파일 -->
												<td colspan="3">
													<c:choose>
														<c:when test="${fn:length(attachList2)>0 }">
														<div id="previewList" class="wd-per-100">
															<div class="bxType01 fileWrap previewFileLst">
																<div>
																	<div class="fileList type02">
																		<div id="detailFileList0" class="fileDownLst">
																			<c:forEach var="item" items="${attachList2}" varStatus="idx">
																				<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																					<span class="MultiFile-export">${item.SEQ_DSP}</span> 
																					<span class="MultiFile-title" title="File selected: ${item.NAME}">
																					${item.FILE_NAME} (${item.FILE_SIZE_TXT})
																					</span>
																					<a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a>
																				</div>		
																			</c:forEach>
																		</div>
																	</div>
																</div>
															</div>
														</div>
											
														<div id="repair_previewImg" class="wd-per-100">
															<div class="previewFile mgn-t-15">
																<h3 class="tit-cont preview" id="previewTitle">이미지 미리보기</h3>
																<ul style="margin-top:5px;">
																	<c:forEach var="item" items="${attachList2}" varStatus="idx">
																		<c:if test = "${item.FORMAT eq 'jpg' || item.FORMAT eq 'png' || item.FORMAT eq 'jpeg' || item.FORMAT eq 'JPG' || item.FORMAT eq 'PNG'}">
																			<li>
																				<img src="/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}" title="클릭하시면 원본크기로 보실 수 있습니다." onclick="originFileView('/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}')" />
																				<p>${item.FILE_NAME} <a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a></p>
																				
																			</li>
																		</c:if>
																	</c:forEach>
																</ul>
															</div>
														</div>
														</c:when>
														<c:otherwise>
															<div class="f-l wd-per-100 mgn-t-5">
															※ 등록된 첨부파일이 없습니다.
															</div>
														</c:otherwise>
													</c:choose>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<iframe id="fileDownFrame" style="width:0px;height:0px;border:0px"></iframe>
			</div> <%-- modal-bodyIn : e --%>
		</div> <%-- modal-body : e --%>
	</div> <%-- modal-content : e --%>
</div> <%-- modal : e --%>