<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: popPreventiveResultList.jsp
	Description : 예방보전실적 등록/조회 팝업
    author		: 김영환
    since		: 2018.05.29
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.29	 김영환		최초 생성

--%>

<html>
<head>

<script type="text/javascript">

$(document).ready(function() {
	
});

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

</script>
</head>

<body>
<div class="modal fade modalFocus" id="PopPreventiveResultList" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-80">			
		 <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">예방보전실적 현황</h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	           		
	           		<div class="modal-section">
						<div class="fl-box panel-wrap-modal wd-per-100">
							<div class="panel-body mgn-l-10" id="PopPreventiveResultList">
								<bdo	Debug="Error"
										Layout_Url="/epms/preventive/result/popPreventiveResultListLayout.do"
									>
								</bdo>
							</div>
						</div>
					</div>
					
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>

<%-- 예방보전실적등록 --%>
<div class="modal fade modalFocus" id="popPreventiveResultRegForm" data-backdrop="static" data-keyboard="false"></div>
<%-- 예방보전실적조회 --%>
<div class="modal fade modalFocus" id="popPreventiveResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 부품명 검색 --%>
<div class="modal fade modalFocus" id="popRepairMaterialList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-50">
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">
			   	    <spring:message code='epms.object.name' /> <spring:message code='epms.search' />
			   	</h4><!-- 부품명 검색 -->
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	           		<div class="modal-section">
						<div class="fl-box wd-per-100">
							<bdo	Debug="Error"
					 				Data_Url=""
									Layout_Url="/epms/repair/result/popRepairMaterialListLayout.do" 
							>
							</bdo>
						</div>
					</div> <%-- modal-section : e --%>
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>
</body>
</html>