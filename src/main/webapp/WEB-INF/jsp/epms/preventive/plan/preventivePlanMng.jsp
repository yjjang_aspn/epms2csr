<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ page import="java.util.Calendar"%>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%--
	Class Name	: preventivePlanMng.jsp
	Description : 예방보전관리 > 예방보전일정 관리 및 배정
	author		: 김영환
	since		: 2018.06.12
 
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2018.06.12	 	김영환		최초 생성

--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%> 

<style>
.inp-comm.monthPic {box-sizing:border-box;height:28px;padding:5px 25px 4px 6px;border:1px solid #cdd2da;
	background:#fff url('/images/com/web/ico_cal.png') no-repeat right 5px center;background-size: 17px auto;cursor:pointer;}
</style>

<script type="text/javascript" src="/js/jquery/moment-with-locales.min.js"></script>
<script type="text/javascript" src="/js/jquery/jquery.ui.datepicker.js"></script>
<!-- 예방보전관리 : 예방보전일정 관리 및 배정 -->
<script type="text/javascript">

var year = 1;     <%-- 년 --%>
var month = 1;    <%-- 월 --%>
var yearMonthTit; <%-- 년,월 세팅 --%>
var target_year;  <%-- 선택된 년 --%>
var target_month; <%-- 선택된 월 --%>
var sel_location; <%-- 선택된 공장 --%>
var sel_part;	  <%-- 선택된 파트 --%>
var sel_manager;  <%-- 선택된 담당자 --%>
var user_part;	  <%-- 사용자 파트 --%>
var focusedRow = null;		<%-- Focus Row : [예방보전계획현황] --%>

$(document).ready(function() {
	
	$('.monthPic').monthPicker();
	
	<%-- 서버의 현 날짜를 가져와 초기값으로 세팅 --%>
	<%-- 년도 초기값 세팅 --%>
 	$('#year').val("${startDt}".substring(0,4));
	<%-- 월 초기값 세팅 --%>
 	$('#month').val(parseInt("${startDt}".substring(4,6)));
	<%-- 일 초기갑 세팅 --%>
 	$('#day').val(parseInt("${startDt}".substring(6,8)));
	<%-- 달력 세팅  --%>
	makeCalendar();
		
	<%-- 검색 년,월을 가져와 분할후 달력 년/월 세팅 --%>
	$('#srcBtn').on('click', function(){
		
		var split_searchDt2=$('#searchDt2').val().split('-');
		
		target_year = Number(split_searchDt2[0]);
		target_month = Number(split_searchDt2[1]);
		
		$('#year').val(target_year);
		$('#month').val(target_month);
		
		removeCalendar();
		makeCalendar();
		setGrid(target_year,target_month);
		
	});
	
	<%-- 오더생성 버튼 --%>
	$('#ordBtn').on('click', function(){
		if(confirm("오더를 생성하시겠습니까?")){
			orderMake();
		}
	});
	
	<%-- 셀렉트 박스 변경시 --%>
	$('.inq-area-inner select').on('change', function(){
		$("#srcBtn").click();
		managerListReload();
	});
	
	<%-- monthPicker 선택시 --%>
	$("#monthList").click(function(){
		$("#srcBtn").click();
	});
	
	managerListReload();
	
});

<%-- 오더생성 --%>
function orderMake(gridNm){
	$.ajax({
		type: 'POST',
		async : false,
		url: '/epms/preventive/plan/preventivePlanMakeOrder.do',
		dataType : 'json',
		data: false,
		success: function (json) {
			if(json.RetCode == 'S'){
				alert('오더가 생성 되었습니다.');
				location.reload();
			}else{
				alert("오더 생성에 실패하였습니다.");
			}
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('처리중 오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
		}
	});
}

<%-- 달력 변경시 기존의 생성된 달력을 삭제 --%>   
function removeCalendar(){
	<%-- 기존의 생성된 달력의 행의 갯수를 차례대로 삭제 --%>
	var pre_cal_rows = Number($('#pre_cal').val());
	
	for(var i = 1; i <= pre_cal_rows; i++){
		$('#st_cal').remove();
	}
}

<%-- 달력 세팅 (테이블 구조) --%>
<%-- 달력 생성 필수값 (년,월) --%>
function makeCalendar(){
	<%-- 날짜의 형태 (년,월)를 형 변환  --%>
	year = Number($('#year').val());
	month = Number($('#month').val()-1);
	
	day = $('#day').val();
	sel_part = $('select[name=sel_part]:visible').val();
	sel_location = $('#sel_plant').val();
	sel_manager = $('#sel_manager').val();
	
	<%-- 선택된 년,월에 해당하는 1일의 날짜를 세팅  --%>
	var curDate = new Date(year,month,1)
	
	<%-- 선택된 년,월의 1일에 해당하는 요일 세팅  --%>
	var dayName = curDate.getDay();
	
	<%-- 선택된 월의 마지막 일 세팅  --%>
	var lastDay = new Date(year,month+1,0).getDate();

	<%-- 현재 월의 달력에 필요한 행 구하기  --%>
	var row = Math.ceil((dayName+lastDay)/7);
	
	<%-- 달력에 표기되는 일의 초기 값  --%>
	var dNum = 1; <%-- 해당 달의 일자 초기  --%>
	var html;
	
	<%-- 클래스명에 해당하는 월 설정  --%>
	if(9 > month){
		month = month+1;
		month = '0' + month;
	}else{
		month = month+1;
	}
	
	<%-- 달력 행 만들기  --%>
	for(var i = 1; i <= row; i++){
		html += '<tr id="st_cal">';
		for(var j = 1; j < 8; j++){
			if(i===1 && j <= dayName || dNum > lastDay){
				html += '<td> &nbsp; </td>';
			}else{
				html += '<td><ul><li class="date" style="display:inline-block; height:15px; font-weight:bold;">' + dNum + '일' + '</li>';
				<%-- 클래스명에 해당하는 일 설정   --%>
				if(10 > dNum){
					dNum = '0' + dNum;
				}else{
					dNum;
				}
				html += '<li class="' + year + month + dNum + '"  style="float:right; height:15px; color:blue; font-weight:bold;"></li></ul>';
				html += '<div style="height:40px;text-align:center;cursor:pointer;" class="' + year + month + dNum + '_detail"></div>';
				html += '</td>';

				dNum++;
			}
		}
		html += "</tr>"
	}
	
	$('#cal_tb').append(html);
	$('#pre_cal').val(row);
	
	<%-- 해당된 년,월에 해당하는 예방점검 계획 횟수 조회  --%>
	yearMonthTit = String(year) + "년 " + String(month)+ "월";
	$('#yearMonthTit').text(yearMonthTit);
	$('#cur_year').val(year);
	$('#cur_month').val(month);
	
	$('#searchDt2').val(year+"-"+month);
	
	var paramDate = String(year) + String(month);
	monthlyMaintenanceCount(paramDate,sel_location,sel_part,sel_manager);
}

<%-- 해당된 년,월에 해당하는 예방 점검 계획 횟수 조회 --%>
function monthlyMaintenanceCount(paramDate,sel_location,sel_part,sel_manager){
	
	$.ajax({
		type: 'POST',
		async : false,
		url: '/epms/preventive/plan/preventivePlanAjax.do',
		data: {"searchDt" : paramDate, "LOCATION" : sel_location, "PART" : sel_part, "MANAGER" : sel_manager},
		dataType: 'json',
		success: function (data) {
			<%-- 년,월,일에 해당하는 예방점검 데이터 출력  --%>
			(data.monthlyPreventivePlan).forEach(function (item){
				if(item.MAINTENANCE_PLAN != null || item.MAINTENANCE_PLAN !=''){
					$('.' + item.SEARCHDT).text('     (' + item.COMPLETE + '/' + item.MAINTENANCE_PLAN + ')');
					
				}else{
					$('.' + item.searchDt).text('     (0/0)');
				}
				$('.' + item.SEARCHDT + '_detail').text(item.MAINTENANCE_PLAN);
				$('.' + item.SEARCHDT + '_detail').attr("onClick","dailyMaintenanceList(" + item.SEARCHDT + ")");
				
				<%-- 일별 계획건이 일정 수치 이상 증가시  색상 크기 변경  --%>
				//if(item.maintenance_plan > 2){
					$('.' + item.SEARCHDT + '_detail').attr("style","color:red; font-size:20px; text-align:center; cursor:pointer;");	
				//}
			});
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('처리중 오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
		}
		
	});
	
}

<%-- 년,월,일에 해당하는 예방 점검 리스트 조회 --%>
function dailyMaintenanceList(param){
	sel_location  = $('#sel_plant').val();
	sel_part = $('select[name=sel_part]:visible').val();
	sel_manager = $('#sel_manager').val();
		
	$('#preventivePlanList').removeAttr("style");
	Grids.PreventivePlanList.Source.Data.Url = "/epms/preventive/plan/preventivePlanListData.do?searchDt=" + param + "&PART=" + sel_part + "&LOCATION=" + sel_location+ "&MANAGER=" + sel_manager;
	Grids.PreventivePlanList.Source.Layout.Url = "/epms/preventive/plan/preventivePlanListLayout.do?PART=" + sel_part + "&LOCATION=" + sel_location;
	Grids.PreventivePlanList.Reload();
}

<%-- row 값 계산  --%>
Grids.OnReady = function(grid){
	  var rowCnt = 0;
	  var rows = grid.Rows;
	  for(var key in rows){
	    if(key == "Header" || key == "NoData" || key == "Toolbar"){
	      continue;
	    } 
	    rowCnt ++;
	  }
	  gridDivResize(rowCnt);
}

<%-- 그리드 영역 사이즈 resizing --%>
function gridDivResize(dataSize){

	if(dataSize <= 1){
	  dataSize = 4;
	}
	
	var divSize = dataSize * 10;  
	
	divSize += 160;
	
	if(divSize < 160){
	  divSize = 160;
	}
	
	$("#preventivePlanList").height(divSize);
	$('#preventivePlanList').css('max-height','510px')
  
}
 
<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "PreventivePlanList") {
	
		<%-- 1. 포커스  --%>
		if(focusedRow != null){
			grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow = row;

		var sel_part = row.PART;        <%-- 담당자 일괄 변경시 사용  --%>
		$('#sel_part').val(sel_part);
	}
}

<%-- Validation --%>
function dataValidation(gridNm){
	var cnt = 0;
	var chkCnt = 0;
	var grid = Grids[gridNm];
	
	if(gridNm == "PreventivePlanList"){
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){
			if(row.Changed || row.Added){
				
				cnt++;
				
				<%-- 점검계획일이 만료일 이전인지 체크 --%>
				var date_assign = Grids[gridNm].GetValue(row, "DATE_ASSIGN");		<%-- 점검계획일 --%>
				var date_deadline = Grids[gridNm].GetValue(row, "DATE_DEADLINE");	<%-- 만료일 --%>
				
				if(date_assign > date_deadline){
					chkCnt++;
					Grids[gridNm].SetAttribute(row,"DATE_ASSIGN","Color","#FF6969",1);
				}else{
					Grids[gridNm].SetAttribute(row,"DATE_ASSIGN","Color","#FFFFAA",1);
				}
				
			}
		}
	}	
	
	<%-- 추가 및 변경된 데이터가 없을 경우 --%>
	if(cnt == 0){
		return false;
	}
	
	<%-- 점검계획일 체크 --%>
	if(chkCnt == 0){
		return true;
	}
	else{
		alert("점검계획일은 만료일 이전으로 수정 가능합니다.");
		return false;
	}	
}

<%-- 저장 버튼 --%>
function fnSave(gridNm){
	
	<%-- 유효성 체크 --%>
	if(!dataValidation(gridNm)){
		return;
	}
    
    if(Grids[gridNm].ActionValidate()){
    	
		if(gridNm == "PreventivePlanList"){
			if(confirm("저장하시겠습니까?")){
				Grids[gridNm].ActionSave();
			}
		}
		
    }
    
}

<%-- 그리드 데이터 저장 후 --%> 
Grids.OnAfterSave  = function (grid){
	removeCalendar();
	makeCalendar();
	Grids.PreventivePlanList.ReloadBody();	
};

<%-- 선택된 년,월에 전월로 이동 --%>
function preDate(){
	<%-- 선택된 년,월의 전월로 세팅 --%>
	target_year = Number($('#cur_year').val());
	target_month = Number($('#cur_month').val()-1);

	<%-- 12월,1월 기준으로 년,월 자동 변경 --%>
	if(target_month > 12){
		target_year += 1;
		target_month = 1;
	}
	if(target_month < 1){
		target_year -= 1;
		target_month = 12;
	}
	<%-- 전월에 대한 년,월 세팅 --%>
	$('#year').val(target_year);
	$('#month').val(Number(target_month));
	
	removeCalendar();
	makeCalendar();
	//dailyMaintenanceList();
	
	<%-- 월의 경우 10월보다 작을시 0+월로 변경 --%>
	if(10 > target_month){
		target_month = target_month;
		target_month = '0' + target_month;
	}else{
		target_month = target_month;
	}
	
	yearMonthTit = String(target_year) + '년 ' + String(target_month)+ '월';
	$('#yearMonthTit').text(yearMonthTit);
	setGrid(Number(target_year),Number(target_month));
	
}

<%-- 선택된 년,월에 익월로 이동 --%>
function nextDate(){
	
	<%-- 선택된 년,월의 전월로 세팅 --%>
	target_year = Number($('#cur_year').val());
	target_month = Number($('#cur_month').val())+1;
	
	<%-- 12월,1월 기준으로 년,월 자동 변경 --%>
	if(target_month > 12){
		target_year += 1;
		target_month = 1;
	}
	if(target_month < 1){
		target_year -= 1;
		target_month = 12;
	}
	<%-- 전월에 대한 년,월 세팅 --%>
	$('#year').val(target_year);
	$('#month').val(Number(target_month));
	
	removeCalendar();
	makeCalendar();
	//dailyMaintenanceList();
	
	<%-- 월의 경우 10월보다 작을시 0+월로 변경 --%>
	if(10 > target_month){
		target_month = target_month;
		target_month = '0' + target_month;
	}else{
		target_month = target_month;
	}
	
	yearMonthTit = String(target_year) + '년 ' + String(target_month)+ '월';
	$('#yearMonthTit').text(yearMonthTit);
	
	setGrid(Number(target_year),Number(target_month));
}

<%-- 그리드 세팅 --%>
function setGrid(year,month){
	
    var txtYear = "";
    var txtMonth = "" ;
  
    <%-- 선택된 월의 마지막 일 세팅 --%>
	var LastDate = new Date(year,month,0).getDate();
	if(month>9){
		txtMonth = String(month);
	}else{
		txtMonth = '0' + String(month);
	}
	var startDt = String(year) + txtMonth + "01";
	var endDt = String(year) + txtMonth + String(LastDate);
	
	sel_location  = $('#sel_plant').val();
	sel_part = $('select[name=sel_part]:visible').val();
	
	$('#preventivePlanList').removeAttr("style");
	Grids.PreventivePlanList.Source.Data.Url = "/epms/preventive/plan/preventivePlanListData.do?startDt=" + startDt + "&endDt=" + endDt + "&PART=" + sel_part + "&LOCATION=" + sel_location+ "&MANAGER=" + sel_manager;
	Grids.PreventivePlanList.Source.Layout.Url = "/epms/preventive/plan/preventivePlanListLayout.do?PART=" + sel_part + "&LOCATION=" + sel_location;
	Grids.PreventivePlanList.Reload();
	
}

<%-- 배정일 일괄 변경 --%>
function batchDateAssign(grid,row,col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return
	}
	var changeDate = row.DATE_ASSIGN;
	
	var selRows = Grids.PreventivePlanList.GetSelRows();
	
	for(var i=0; i <selRows.length;i++){
		Grids["PreventivePlanList"].SetValue(selRows[i], "DATE_ASSIGN", changeDate, 1);
	}
}

<%-- 담당자 일괄 변경 --%>
function batchManager(grid,row,col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return 
	}
	var changePerson = row.MANAGER;
	var selRows = Grids.PreventivePlanList.GetSelRows();
	var sel_part = $('#sel_part').val();
	
	for(var i=0; i <selRows.length;i++){
		if(sel_part==selRows[i].PART){
			
			Grids["PreventivePlanList"].SetValue(selRows[i], "MANAGER", changePerson, 1);	
		}
	}

}

<%-- 정비원 리스트 호출 --%>
function managerListReload(){
	
	var sel_location = $('select[name=sel_plant]').val();
	var part = $('select[name=sel_part]:visible').val();
	var remarks = $("#sel_plant").find("option:selected").attr('role');
	sel_manager = $("#sel_manager").val();
	
	$.ajax({
		type: 'POST',
		async : false,
		url: '/epms/preventive/plan/managerListAjax.do',
		data: {"SUB_ROOT" : sel_location, "PART" : part, "REMARKS" : remarks, "sel_manager":sel_manager},
		dataType: 'json',
		success: function (data) {
			
			if(data.managerListOpt.length == 0){
				var html = "<option value=''>" + "전체" + " </option>";
				$("#sel_manager").find("option").remove();
				$("#sel_manager").append(html);
			}else if (data.managerListOpt.length > 0) {
				$("#sel_manager").find("option").remove();
				var html = "<option value=''>" + "전체" + " </option>";
				$.each(data.managerListOpt, function() {
					html += "<option value='"+ this.USER_ID + "'>" + this.NAME + " </option>"
				});
				$("#sel_manager").append(html);
				$("#sel_manager option[value='"+ sel_manager +"']").attr('selected', true);
			}
			
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('처리중 오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
		}
		
	});

}

</script>

</head>
<body>

	<input type="hidden" id="pre_cal" name="pre_cal">
	<input type="hidden" id="cur_year" name="cur_year">
	<input type="hidden" id="cur_month" name="cur_month">
	<input type="hidden" id="year" name="year">
	<input type="hidden" id="month" name="month">
	<input type="hidden" id="day" name="day">
	<input type="hidden" id="REMARKS" name="REMARKS">
	<input type="hidden" id="sel_part" name="sel_part">
		
	<!-- 화면 UI Area Start  -->
	<div class="scl"  style="height:100%">
		<div style="padding:15px;">
	
			<div class="rel">
				<h5 class="panel-tit">예방보전 일정관리/배정</h5>
				<div class="inq-area-inner" style="margin-top:0; padding-top:30px;">
					<ul class="wrap-inq">
						<li class="inq-clmn">
							<h4 class="tit-inq"><spring:message code='epms.location' /></h4><!-- 공장 -->
							<div class="sel-wrap type02" style="width:80px;">
								<select title="공장" id="sel_plant" name="sel_plant" >
									<c:forEach var="item" items="${locationList}">
										<option value="${item.CODE}" role="${item.REMARKS}" <c:if test="${sessionScope.ssLocation eq item.CODE}"> selected="selected"</c:if>>${item.NAME}</option>
									</c:forEach>
								</select> 
							</div>
						</li>
						
						<li class="inq-clmn">
							<h4 class="tit-inq">파트</h4><!-- 파트 -->
							<div class="sel-wrap type02" style="width:70px;">
								<select title="파트" name="sel_part" OnChange="managerListReload()" >
									<option value="">전체</option>
									<c:forEach var="item" items="${partListOpt}">
										<option value="${item.CODE}" "<c:if test="${part eq item.CODE}"> selected="selected"</c:if>>${item.NAME}</option>
									</c:forEach>
								</select>
								
							</div>
						</li>
						
						<li class="inq-clmn">
							<h4 class="tit-inq"><spring:message code='epms.worker' /></h4><!-- 정비원 -->
							<div class="sel-wrap type02" style="width:80px;">
								<select title="정비원" id="sel_manager" name="sel_manager">
								</select>
							</div>
						</li>
						
						<li class="inq-clmn">
							<h4 class="tit-inq">기준월</h4><!-- 기준월 -->
							<div class="prd-inp-wrap" style="width:100px;">
								<input type="text" id="searchDt2" class="inp-comm monthPic" readonly="readonly" title="기준월" value="${searchDt2}">
							</div>
						</li>
						
					</ul>
					<a href="#none" id="srcBtn" class="btn comm st01">검색</a>				
					<a href="#none" id="ordBtn" class="btn comm st01">오더 생성</a>
				</div>
				<div class="mgn-t-20">
					<div style="text-align:center;">
						<a href="#none" class="btn comm st02"  onClick="preDate()">&lt;</a>
						<span class="btn comm " id="yearMonthTit"  style="width:200px; font-size:18px; color:#4c4c4c; font-weight:bold;"></span>
						<a href="#none" class="btn comm st02"  onClick="nextDate()">&gt;</a>
					</div>
					<div class="tb-wrap mgn-t-10">
						<table class="tb-st type01"  id="cal_tb">
							<caption class="screen-out">예방보전일정</caption>
							<colgroup>
								<col width="14.3%"/>
								<col width="14.3%"/>
								<col width="14.3%"/>
								<col width="14.3%"/>
								<col width="14.3%"/>
								<col width="14.3%"/>
								<col width="*"/>
							</colgroup>
							<tr id=dayName>
								<th class="t-c">일</th>
								<th class="t-c">월</th>
								<th class="t-c">화</th>
								<th class="t-c">수</th>
								<th class="t-c">목</th>
								<th class="t-c">금</th>
								<th class="t-c">토</th>
							</tr>	
						</table>
					</div>
				</div>
			</div>
		
			<div class="mgn-t-20 rel">
				<h5 class="panel-tit">예방보전 계획현황</h5>
				<div id="preventivePlanList">
					<bdo	Debug="Error"
							Data_Url="/epms/preventive/plan/preventivePlanListData.do?startDt=${startDt}&endDt=${endDt}&PART=${part}&LOCATION=${location}"
							Layout_Url="/epms/preventive/plan/preventivePlanListLayout.do?PART=${part}&LOCATION=${location}"
							Upload_Url="/epms/preventive/plan/preventivePlanListEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols" 
							Export_Data="data" Export_Type="xls"
							Export_Url="/sys/comm/exportGridData.jsp?File=PreventivePlanList.xls&dataName=data"
							>
					</bdo>
				</div>				
			</div>
		</div>
	</div>
	
</body>
</html>