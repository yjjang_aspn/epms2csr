<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%--
	Class Name	: preventivePlanListData.jsp
	Description : 예방보전관리 > 예방보전일정 관리 및 배정 : 데어터
	author		: 김영환
	since		: 2018.06.12
 
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2018.06.12	 	김영환		최초 생성

--%>

<c:set var="user_part" value="${sessionScope.ssPart}"/>
<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I	
			PREVENTIVE_RESULT				= "${item.PREVENTIVE_RESULT}"
			DATE_PLAN						= "${item.DATE_PLAN}"
			DATE_DEADLINE					= "${item.DATE_DEADLINE}"
			DATE_ASSIGN						= "${item.DATE_ASSIGN}"
			STATUS							= "${item.STATUS}"
			<c:if test="${item.STATUS == '03'}">
				STATUSClass="gridStatus3"
			</c:if>
			<c:if test="${item.STATUS == '02' || item.STATUS == '09'}">
				STATUSClass="gridStatus4"
			</c:if>
			MANAGER							= "${item.MANAGER}"
			
			PREVENTIVE_BOM					= "${item.PREVENTIVE_BOM}"
			WORK_TYPE						= "${item.WORK_TYPE}"
			WORK_TYPE_NAME					= "${fn:replace(item.WORK_TYPE_NAME, '"', '&quot;')}"
			PART							= "${item.PART}"
			PREVENTIVE_TYPE					= "${item.PREVENTIVE_TYPE}"
			CHECK_TYPE						= "${item.CHECK_TYPE}"
			CHECK_DETAIL          			= "${fn:replace(item.CHECK_DETAIL, '"', '&quot;')}"
			CHECK_STANDARD      			= "${fn:replace(item.CHECK_STANDARD, '"', '&quot;')}"
			CHECK_TOOL          			= "${fn:replace(item.CHECK_TOOL, '"', '&quot;')}"
			CHECK_CYCLE         			= "${item.CHECK_CYCLE}"
			
			EQUIPMENT           			= "${item.EQUIPMENT}"
			EQUIPMENT_UID       			= "${item.EQUIPMENT_UID}"
			EQUIPMENT_NAME          		= "${fn:replace(item.EQUIPMENT_NAME, '"', '&quot;')}"
			GRADE			       			= "${item.GRADE}"
			LOCATION						= "${item.LOCATION}"
			LINE	            			= "${item.LINE}"
			LINE_UID						= "${item.LINE_UID}"
			LINE_NAME          				= "${fn:replace(item.LINE_NAME, '"', '&quot;')}"
			
			LOCATIONPART					= "${item.LOCATIONPART}"
			
			<c:choose>
				<c:when test="${item.STATUS ne '01'}">
					DATE_ASSIGNCanEdit = "0"
					MANAGERCanEdit = "0"
					CanSelect = "0"
				</c:when>
				<c:when test="${item.STATUS eq '01' and authType eq 'AUTH02'}">
					DATE_ASSIGNCanEdit = "1"
					MANAGERCanEdit = "1"
					CanSelect = "1"
				</c:when>
				<c:when test="${item.STATUS eq '01' and authType eq 'AUTH03' and item.REMARKS eq '02' and fn:indexOf(sessionScope.ssSubAuth, item.SUB_ROOT) > -1}">
					DATE_ASSIGNCanEdit = "1"
					MANAGERCanEdit = "1"
					CanSelect = "1"
				</c:when>
				<c:when test="${item.STATUS eq '01' and authType eq 'AUTH03' and item.REMARKS eq '01' and fn:indexOf(sessionScope.ssSubAuth, item.SUB_ROOT) > -1 and user_part eq item.PART}">
					DATE_ASSIGNCanEdit = "1"
					MANAGERCanEdit = "1"
					CanSelect = "1"
				</c:when>
				<c:otherwise>
					DATE_ASSIGNCanEdit = "0"
					MANAGERCanEdit = "0"
					CanSelect = "0"
				</c:otherwise>
			</c:choose>
			
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>