<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%--
	Class Name	: preventivePlanListLayout.jsp
	Description : 예방보전관리 > 예방보전일정 관리 및 배정 : 레이아웃
	author		: 김영환
	since		: 2018.06.12
 
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2018.06.12	 	김영환		최초 생성

--%>

<!-- 예방보전관리 - 예방보전BOM 관리 화면 -->
<c:set var="gridId" value="PreventivePlanList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		Filtering="1"				Dragging="0"
			Editing="1"				Deleting="0"		Selecting="1"		SuppressCfg="0"				
			Paging="1"			  	PageLength="20"	  	MaxPages="30"  		AllPages = "0"	
			CopyCols="0"			PasteFocused="3"	CustomScroll="1"
	/>
	
	<Head>
		<Header	id="Header"	Align="center" CanExport="1" CanHide="0"
			PREVENTIVE_RESULT		= "예방보전ID"
			DATE_PLAN				= "계획일"
			DATE_DEADLINE			= "만료일"
			DATE_ASSIGN				= "점검계획일"
			STATUS					= "상태"
			MANAGER					= "담당자"
			
			PREVENTIVE_BOM			= "예방보전BOMID"
			WORK_TYPE				= "오더유형ID"
			WORK_TYPE_NAME			= "오더유형"
			PART					= "파트"
			PREVENTIVE_TYPE			= "업무구분"
			CHECK_TYPE				= "점검구분"
			CHECK_DETAIL			= "점검항목"
			CHECK_STANDARD			= "판단기준"
			CHECK_TOOL				= "진단장비"
			CHECK_CYCLE				= "주기"
			
			EQUIPMENT				= "<spring:message code='epms.system.id'        />"    <%  // 설비ID        %>
			EQUIPMENT_UID			= "<spring:message code='epms.system.uid'       />"    <%  // 설비코드        %>
			EQUIPMENT_NAME			= "<spring:message code='epms.system.name'      />"    <%  // 설비명         %>
			GRADE					= "<spring:message code='epms.system.grade'     />"    <%  // 설비등급        %>
			LOCATION				= "<spring:message code='epms.location'      />"    <%  // 위치          %>
			LINE					= "<spring:message code='epms.category.id'   />"    <%  // 라인ID        %>
			LINE_UID				= "<spring:message code='epms.category.uid'  />"    <%  // 기능위치        %>
			LINE_NAME				= "<spring:message code='epms.category'      />"    <%  // 라인          %>
			                           
			flag					= "<spring:message code='epms.select.flag'      />"    <%  // 선택 여부       %>
			LOCATIONPART			= "<spring:message code='epms.location.part' />"    <%  // 공장파트        %>
		/>
		<Filter	id="Filter" CanExport="1" CanHide="0" CanFocus="1"/>
	</Head>
	
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
	
	<Cols>
		<C Name="PREVENTIVE_RESULT"			Type="Text" Align="left"   Visible="0" Width="100" CanEdit="0"  CanExport="1" CanHide="1" RelWidth="60"/>
		<C Name="MAINTENANCE_BOM"			Type="Text" Align="left"   Visible="0" Width="100" CanEdit="0"  CanExport="1" CanHide="1" RelWidth="60"/>
		<C Name="LOCATION"					Type="Enum" Align="left"   Visible="0" Width="60"  CanEdit="0"  CanExport="1" CanHide="1" RelWidth="60" <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>	
		<C Name="STATUS"					Type="Enum" Align="left"   Visible="1" Width="90"  CanEdit="0"  CanExport="1" CanHide="1" RelWidth="90" <tag:enum codeGrp="PREVENTIVE_STATUS"/>/>
		
		<C Name="WORK_TYPE"					Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0"  CanExport="1" CanHide="1" RelWidth="60"/>
		<C Name="WORK_TYPE_NAME"			Type="Text" Align="left"   Visible="1" Width="140" CanEdit="0"  CanExport="1" CanHide="1" RelWidth="140"/>
		
		<C Name="PREVENTIVE_TYPE"			Type="Enum" Align="left"   Visible="1" Width="90"  CanEdit="0"  CanExport="1" CanHide="1" RelWidth="90" <tag:enum codeGrp="PREVENTIVE_TYPE"/>/>
		<C Name="PART"						Type="Enum" Align="left"   Visible="1" Width="60"  CanEdit="0"  CanExport="1" CanHide="1" RelWidth="60" <tag:enum codeGrp="PART" companyId="${ssCompanyId}"/>/>
		<C Name="CHECK_TYPE"				Type="Enum" Align="left"   Visible="1" Width="90"  CanEdit="0"  CanExport="1" CanHide="1" RelWidth="90" <tag:enum codeGrp="CHECK_TYPE"/>/>
		
		<C Name="LINE"						Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0"  CanExport="1" CanHide="1" RelWidth="60"/>
		<C Name="LINE_UID"					Type="Text" Align="left"   Visible="0" Width="140" CanEdit="0"  CanExport="1" CanHide="1" RelWidth="140"/>
		<C Name="LINE_NAME"					Type="Text" Align="left"   Visible="1" Width="140" CanEdit="0"  CanExport="1" CanHide="1" RelWidth="140" CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT"					Type="Text" Align="left"   Visible="0" Width="100" CanEdit="0"  CanExport="1" CanHide="1" RelWidth="100"/>
		<C Name="EQUIPMENT_UID"				Type="Text" Align="left"   Visible="1" Width="100" CanEdit="0"  CanExport="1" CanHide="1" RelWidth="80"  CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT_NAME"			Type="Text" Align="left"   Visible="1" Width="220" CanEdit="0"  CanExport="1" CanHide="1" RelWidth="220" CaseSensitive="0" WhiteChars=" "/>
		<C Name="GRADE"						Type="Text" Align="left"   Visible="0" Width="60"  CanEdit="0"  CanExport="1" CanHide="1" RelWidth="60"/>
		
		<C Name="CHECK_DETAIL"				Type="Text" Align="left"   Visible="1" Width="150" CanEdit="0"  CanExport="1" CanHide="1" RelWidth="150" CaseSensitive="0" WhiteChars=" "/>
		<C Name="CHECK_STANDARD"			Type="Text" Align="left"   Visible="1" Width="120" CanEdit="0"  CanExport="1" CanHide="1" RelWidth="120" CaseSensitive="0" WhiteChars=" "/>
		<C Name="CHECK_TOOL"				Type="Text" Align="left"   Visible="1" Width="120" CanEdit="0"  CanExport="1" CanHide="1" RelWidth="120" CaseSensitive="0" WhiteChars=" "/>
		<C Name="CHECK_CYCLE"				Type="Int"  Align="center" Visible="1" Width="110" CanEdit="0"  CanExport="1" CanHide="1" RelWidth="110"/>
	</Cols>
	
	<RightCols>
		<C Name="LOCATIONPART"				Type="Text"  Align="left"   Visible="0" Width="80"  CanEdit="0"  CanExport="0" CanHide="0" CanFilter="0"/>
		<C Name="DATE_PLAN"					Type="Date"  Align="center" Visible="1" Width="110" CanEdit="0"  CanExport="1" CanHide="1" Format="yyyy/MM/dd" />
		<C Name="DATE_DEADLINE"				Type="Date"  Align="center" Visible="1" Width="110" CanEdit="0"  CanExport="1" CanHide="1" Format="yyyy/MM/dd" />
		<C Name="DATE_ASSIGN"				Type="Date"  Align="center" Visible="1" Width="110" CanEdit="0"  CanExport="1" CanHide="1" Format="yyyy/MM/dd" OnChange="batchDateAssign(Grid,Row,Col);" />
		<C Name="MANAGER"					Type="Enum"  Align="left"   Visible="1" Width="100" CanEdit="0"  CanExport="1" CanHide="0" Related="LOCATIONPART" Enum='${repairMemberListOpt.NAME}'	EnumKeys='${repairMemberListOpt.CODE}' ${relatedCombo} OnChange="batchManager(Grid,Row,Col);" />	
		<C Name="flag"						Type="Text"  Align="center" Visible="0" Width="80"  CanEdit="0"  CanExport="0" CanHide="0" />
	</RightCols>	
			
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,저장,항목,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1"	Empty="         "
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				저장Type="Html" 저장="&lt;a href='#none' title='저장' class=&quot;treeButton treeSave&quot;&quot;
					onclick='fnSave(&quot;${gridId}&quot;)'>저장&lt;/a>"
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton01 icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
				엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
	
</Grid>