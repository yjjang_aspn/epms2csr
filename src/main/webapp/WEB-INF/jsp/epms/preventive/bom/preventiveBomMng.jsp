<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"	   uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%--
	Class Name	: preventiveBomMng.jsp
	Description : 예방보전BOM 관리 화면
    author		: 서정민
    since		: 2018.05.09
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.09	 서정민		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<!-- 예방보전관리 : 예방보전BOM 관리 -->
<script type="text/javascript">

<%-- 전역변수 --%>
var focusedRow = null;		<%-- Focus Row : [오더유형] --%>
var focusedRow2 = null;		<%-- Focus Row : [오더] --%>
var focusedRow3 = null;		<%-- Focus Row : [예방보전BOM] --%>
var focusedRow4 = null;		<%-- Focus Row : 일괄매핑 팝업 : [라인] --%>
var focusedRow5 = null;		<%-- Focus Row : 일괄매핑 팝업 : [설비] --%>
var callLoadingYn = true;	<%-- 로딩바 호출여부 --%>
var preventiveBomListTitle = null;

$(document).ready(function(){
	<%-- 트리그리드 3개일 때 화면여닫기 실행  --%>
	treeslideLeft_0301();
});

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "CategoryList") {			<%-- 1. [오더유형] 클릭시 --%>
		
		if (focusedRow == null || focusedRow.CATEGORY != row.CATEGORY || focusedRow2 != null || focusedRow == 'undefined'){
			
			<%-- 1-1. 포커스 --%>
			if(focusedRow != null){
				grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
			}
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			
			focusedRow = row;
			focusedRow2 = null;
			focusedRow3 = null;
			
			<%-- 1-2.작업 목록 조회 --%>
			$('#CATEGORY').val(row.CATEGORY);			<%-- 카테고리ID --%>
			$('#CATEGORY_TYPE').val(row.CATEGORY_TYPE);	<%-- 카테고리 유형 : 'LINE' --%>
			$('#TREE').val(row.TREE);					<%-- 구조체 --%>
			$('#LOCATION').val(row.LOCATION);			<%-- 위치(공장)ID --%>
			$('#SUB_ROOT').val(row.SUB_ROOT);			<%-- SUB_ROOT 정보 --%>
			getOrderList();
			
			<%-- 1-3. 예방보전BOM 목록 조회 --%>
			$('#WORK').val("");							<%-- 작업 ID --%>
			preventiveBomListTitle = "예방보전BOM : " + "[ " + row.CATEGORY_NAME + " ]";
			$('#preventiveBomListTitle').html(preventiveBomListTitle);
		}
	}
	else if(grid.id == "PreventiveWorkList") {	<%-- 2. [오더] 클릭시 --%>
	
		if (focusedRow2 == null || focusedRow2.WORK != row.WORK){
			
			<%-- 2-1. 포커스 시작 --%>
			if(focusedRow2 != null){
				grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
			}
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			
			focusedRow2 = row;
			focusedRow3 = null;
			<%-- 2-2. 예방보전 BOM 목록 조회 --%>
			$('#WORK').val(row.WORK);						<%-- 작업 ID --%>
			$('#preventiveBomListTitle').html(preventiveBomListTitle+" - [ " + row.CHECK_DETAIL + " ]");	<%-- 예방보전BOM 타이틀 변경 --%>
			 
			getPreventiveBomList();
		}
	}
	else if(grid.id == "PreventiveBomList") {	<%-- 3. [예방보전BOM] 클릭시 --%>
	
		<%-- 3-1. 포커스  --%>
		if(focusedRow3 != null){
			grid.SetAttribute(focusedRow3,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow3 = row;		
	}
	else if(grid.id == "PopCategoryList") {		<%-- 4. 일괄매핑 팝업 : [라인] 클릭시 --%>
		
		if (focusedRow4 == null || focusedRow4.CATEGORY != row.CATEGORY){
			
			<%-- 4-1. 포커스 --%>
			if(focusedRow4 != null){
				grid.SetAttribute(focusedRow4,null,"Color","#FFFFFF",1);	
			}
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			
			focusedRow4 = row;
			
			<%-- 1-2. 설비 목록 조회 --%>
			$('#POP_CATEGORY_TYPE').val(row.CATEGORY_TYPE);	<%-- 카테고리 유형 : 'LINE' --%>
			$('#POP_TREE').val(row.TREE);					<%-- 구조체 --%>
			$('#equipmentListTitle').html("설비 : " + "[ " + row.CATEGORY_NAME + " ]");
			getEquipmentList();
		}
	}
	else if(grid.id == "PopEquipmentList") {	<%-- 5. 일괄매핑 팝업 : [설비] 클릭시 --%>
	
		<%-- 5-1. 포커스  --%>
		if(focusedRow5 != null){
			grid.SetAttribute(focusedRow5,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow5 = row;		
	}
}

<%-- 선택된 오더유형에 해당되는 오더 조회 --%>
function getOrderList(){
	Grids.PreventiveWorkList.Source.Data.Url = "/epms/preventive/work/preventiveWorkListData.do?TREE=" + $('#TREE').val();
	Grids.PreventiveWorkList.ReloadBody();	
}

<%-- 선택된 오더유형에 해당되는 예방보전BOM 조회 --%>
function getPreventiveBomList(){
	Grids.PreventiveBomList.Source.Data.Url = "/epms/preventive/bom/preventiveBomListData.do?TREE=" + $('#TREE').val()
											+ "&WORK=" + $('#WORK').val();
	Grids.PreventiveBomList.ReloadBody();	
}

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source, data, func){
	
	if(grid.id == "PreventiveWorkList"){			<%-- [설비] 목록 조회시 --%>
		if(data != ""){
			callLoadingBar();
			callLoadingYn = false;
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "PreventiveWorkList"){
					getPreventiveBomList();
				}
			}
		}
	}
	else if(grid.id == "PreventiveBomList"){	<%-- [설비BOM] 목록 조회시 --%>
		if(data != ""){
			if(callLoadingYn == true){
				callLoadingBar();
				callLoadingYn = false;
			}
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "PreventiveBomList"){
					setTimeout($.unblockUI, 100);
					callLoadingYn = true;
				}
			}
		}
	}
	if(grid.id == "PopEquipmentList"){	<%-- 일괄매핑 팝업 : [설비] 목록 조회시 --%>
		if(data != ""){
			callLoadingBar();
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "PopEquipmentList"){
					setTimeout($.unblockUI, 100);
				}
			}
		}
	}
}

<%-- Validation --%>
function dataValidation(gridNm){
	var cnt = 0;
	var chkCnt = 0;
	var grid = Grids[gridNm];
	
	if(gridNm == "PreventiveBomList"){
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow3);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){  
			if(row.Changed || row.Added){
				
				cnt++;
				
				<%-- 주기 --%>
				var CHECK_CYCLE  = grid.GetValue(row, "CHECK_CYCLE");
				if(CHECK_CYCLE=='' || CHECK_CYCLE<0){
					chkCnt++;
					grid.SetAttribute(row,"CHECK_CYCLE","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"CHECK_CYCLE","Color","#FFFFAA",1);
				}
				
				<%-- 차기계획일 --%>
				var DATE_PLAN = grid.GetValue(row, "DATE_PLAN");
				if(DATE_PLAN==''){
					chkCnt++;
					grid.SetAttribute(row,"DATE_PLAN","Color","#FF6969",1);	
				}else{
					grid.SetAttribute(row,"DATE_PLAN","Color","#FFFFAA",1);
				}
			}
		}
	}
	else if(gridNm == "PopEquipmentList"){
		var selRows = Grids[gridNm].GetSelRows();
		
		for(var i=0; i <selRows.length;i++){
			cnt++;
		}
	}
	
	<%-- 추가 및 변경된 데이터가 없을 경우 --%>
	if(cnt == 0){
		return false;
	}
	
	<%-- 필수값 체크 --%>
	if(chkCnt == 0){
		return true;
	}
	else{
		alert("필수값을 입력해주세요.");
		return false;
	}
}

<%-- 예방보전BOM 저장--%>
function fnSave(gridNm){
	
	<%-- 유효성 체크 --%>
    if(!dataValidation(gridNm)){
    	return;
    }
	
	if(Grids[gridNm].ActionValidate()){
		if(gridNm == "PreventiveBomList"){
			if(confirm("예방보전 BOM을 저장하시겠습니까?")){

				Grids[gridNm].ActionSave();

			}
		}
		else if(gridNm == "PopEquipmentList"){
			var selRows = Grids[gridNm].GetSelRows();
			
			for(var i=0; i <selRows.length;i++){
				Grids[gridNm].SetValue(selRows[i], "flag", "insert", 1);
			}
			
			var works = fnGetWork();

			if(Grids[gridNm].ActionValidate()){
				if(confirm("예방보전BOM을 등록하시겠습니까?")){
					Grids[gridNm].Source.Upload.Url = "/epms/preventive/bom/preventiveBomEdit.do?works=" + works;
					Grids[gridNm].ActionSave();
				}
			}
		}
	}
}

<%-- [일괄매핑] : 팝업 호출 --%>
function fnMapping(){
	
	var selRows = Grids.PreventiveWorkList.GetSelRows();
	
	if(selRows.length> 0){
		Grids.PopCategoryList.Source.Data.Url = "/sys/category/categoryData.do?CATEGORY_TYPE=LINE&SUB_ROOT="+$('#SUB_ROOT').val();
		Grids.PopCategoryList.Reload();
		
		Grids.PopEquipmentList.Source.Data.Url = "";
		Grids.PopEquipmentList.Reload();	
		fnModalToggle('PopPreventiveBomMng');
	}else{
		alert("선택된 오더가 없습니다.");
	}
}

<%-- 일괄매핑 팝업 : 선택된 오더 목록 --%>
function fnGetWork(){
	var selRows = Grids.PreventiveWorkList.GetSelRows();
	
	var work = "";
	for(var i=0; i <selRows.length;i++){
		work = work + Grids["PreventiveWorkList"].GetValue(selRows[i], "WORK") + ",";
	}
	work = work.substr(0,work.length-1);
	return work;
}

<%-- 설비BOM 자재 일괄매핑 저장 이후 새로고침 --%>
function fnReload(){
	
	Grids.PreventiveBomList.ReloadBody();
}

<%-- 그리드 저장 이후 처리 --%>
Grids.OnAfterSave = function (grid){
	 if(grid.id == "PreventiveBomList"){
		 
		Grids.PreventiveBomList.ReloadBody();
	}
	 else if(grid.id == "PopEquipmentList"){
			
		Grids.PreventiveBomList.ReloadBody();
		fnModalToggle('PopPreventiveBomMng');
	}
};

<%-- 예방보전BOM 활성여부 변경시 --%>
function fnChangeSts(grid,row,col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return 
	}
	
	if(row.DEL_YN == "Y"){
		grid.SetAttribute(row, "DEL_YN", "Class", "gridStatus4");
	}
	else if(row.DEL_YN == "N"){
		grid.SetAttribute(row, "DEL_YN", "Class", "gridStatus3");
	}
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
    $('#'+name).load(url, function(responseTxt, statusTxt, xhr){
        $(this).modal();
    });
}
</script>
</head>

<body>
<div id="contents">
	<input type="hidden" id="WORK" name="WORK"/>
	<input type="hidden" id="EQUIPMENT_NAME" name="EQUIPMENT_NAME"/>
	<input type="hidden" id="LOCATION" name="LOCATION"/>
	<input type="hidden" id="SUB_ROOT" name="SUB_ROOT"/>
	<input type="hidden" id="TREE" name="TREE"/>
	<input type="hidden" id="CATEGORY" name="CATEGORY"/>
	<input type="hidden" id="CATEGORY_TYPE" name="CATEGORY_TYPE"/>
	
	<div class="fl-box panel-wrap03 slideWidth rel" style="width:20%; height:100%;">
		<div class="slideArea">
			<h5 class="panel-tit">오더 유형</h5>
			<div class="panel-body">
				<div id="categoryList" class="slideTreeGrid">
					<bdo	Debug="Error"
							Data_Url="/sys/category/categoryData.do?CATEGORY_TYPE=WORK"
							Layout_Url="/sys/category/categoryNotEditLayout.do?CATEGORY_TYPE=WORK"
					>
					</bdo>
				</div>
			</div>
		</div>
		<a href="#none" class="slideBtn letter_4 ir-pm" title="닫기">토글</a>
	</div>
	
	<div class="fl-box panel-wrap03 slideWidth rel" style="width:30%; height:100%;">
		<div class="slideArea">
			<h5 class="panel-tit mgn-l-10" id="PreventiveWorkList">오더</h5>
			<div class="panel-body mgn-l-10">
				<div id="preventiveWorkList" class="slideTreeGrid">
					<bdo	Debug="Error"
							Layout_Url="/epms/preventive/bom/preventiveWorkListLayout.do"
							Export_Data="data" Export_Type="xls"
							Export_Url="/sys/comm/exportGridData.jsp?File=PreventiveWorkList.xls&dataName=data"
					>
					</bdo>
				</div>
			</div>
		</div>
		<a href="#none" class="slideBtn type02 ir-pm" title="닫기">토글</a>
	</div>
	 
	<div class="fl-box panel-wrap03" style="width:49.5%; height:100%;" id="slideWidthRight">
		<h5 class="panel-tit mgn-l-10" id="preventiveBomListTitle">예방보전 BOM</h5>
		<div class="panel-body mgn-l-10">
			<div id="PreventiveBomList">
				<bdo	Debug="Error"
		 				Data_Url=""
						Layout_Url="/epms/preventive/bom/preventiveBomListLayout.do"
						Upload_Url="/epms/preventive/bom/preventiveBomEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"	
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=PreventiveBomList.xls&dataName=data"
						>
				</bdo>
			</div>
		</div>
	</div>
</div>

<%-- 예방보전BOM 매핑 설비 조회 --%>
<%@ include file="/WEB-INF/jsp/epms/preventive/bom/popPreventiveBomMng.jsp"%>

</body>
</html>