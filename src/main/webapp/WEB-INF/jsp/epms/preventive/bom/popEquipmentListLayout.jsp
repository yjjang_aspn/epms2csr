<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%--
	Class Name	: popEquipmentListLayout.jsp
	Description : 설비 목록 그리드 레이아웃(예방보전BOM 매핑 팝업)
    author		: 서정민
    since		: 2018.05.09
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.09	 서정민		최초 생성

--%>

<c:set var="gridId" value="PopEquipmentList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1"			SuppressCfg="0"			
			Editing="1"				Deleting="0"		Selecting="1"		Dragging="0"					
			Filtering="1"			StandardFilter="3"	ClearSelected="2"
			Paging="1"			  	PageLength="20"	  	MaxPages="10"  		AllPages="0"				
			CopyCols="3"			PasteFocused="3"					
	/>
	<c:if test="${equipment_hierarchy eq 'true' }">
		<Cfg	MainCol="EQUIPMENT_NAME" />
	</c:if>
	
	<Head>
		<Header	id="Header"	Align="center" CanExport="1" CanHide="0"
			EQUIPMENT				= "<spring:message code='epms.system.id'    />"    <%  // 설비ID"       %>
			LOCATION				= "<spring:message code='epms.location'     />"    <%  // 위치"         %>
			LINE					= "<spring:message code='epms.category.id'  />"    <%  // 라인ID"       %>
			LINE_UID				= "<spring:message code='epms.category.uid' />"    <%  // 기능위치"       %>
			LINE_NAME				= "<spring:message code='epms.category'     />"    <%  // 라인"         %>
			EQUIPMENT				= "<spring:message code='epms.system.id'       />"    <%  // 설비ID"       %>
			EQUIPMENT_UID			= "<spring:message code='epms.system.uid'      />"    <%  // 설비코드"       %>
			EQUIPMENT_NAME			= "<spring:message code='epms.system.name'     />"    <%  // 설비명"        %>
			                           
			GRADE					= "<spring:message code='epms.system.grade'    />"    <%  // 설비등급"       %>
			ASSET_NO				= "<spring:message code='epms.asset.no'   />"    <%  // 자산번호"       %>
			MODEL					= "<spring:message code='epms.model'        />"    <%  // 모델명"        %>
			DIMENSION				= "<spring:message code='epms.spec'         />"    <%  // 크기/치수"      %>
			WEIGHT					= "<spring:message code='epms.qnty'         />"    <%  // 총중량"        %>
			WEIGHT_UNIT				= "<spring:message code='epms.unit'         />"    <%  // 중량단위"       %>
			ACQUISITION_VALUE		= "<spring:message code='epms.amount'       />"    <%  // 취득가액"       %>
			CURRENCY				= "<spring:message code='epms.currency'     />"    <%  // 통화"         %>
			COUNTRY					= "<spring:message code='epms.country'      />"    <%  // 제조국"        %>
			MANUFACTURER			= "<spring:message code='epms.supplier'     />"    <%  // 제조사"        %>
			DATE_ACQUISITION		= "<spring:message code='epms.date.acquire'        />"    <%  // 취득일"        %>
			DATE_INSTALL			= "<spring:message code='epms.date.install'        />"    <%  // 설치일"        %>
			DATE_OPERATE			= "<spring:message code='epms.date.start'   />"    <%  // 가동일"        %>
			DATE_VALID				= "<spring:message code='epms.date.end'     />"    <%  // 유효일"        %>
			COST_CENTER				= "<spring:message code='epms.costcenter'   />"    <%  // 코스트센터"      %>
			MEMO					= "<spring:message code='epms.memo'         />"    <%  // 메모"         %>
			                           
			ATTACH_GRP_NO			= "<spring:message code='epms.image'        />"    <%  // 이미지"        %>
			ATTACH_GRP_NO2			= "<spring:message code='epms.qrCode'       />"    <%  // QR코드"       %>
			ATTACH_GRP_NO3			= "<spring:message code='epms.manual'       />"    <%  // 매뉴얼"        %>
			URL						= "<spring:message code='epms.url'          />"    <%  // URL"        %>
			HISTORY					= "<spring:message code='epms.history'         />"    <%  // 이력"         %>
			DETAIL					= "<spring:message code='epms.detail'          />"    <%  // 상세"         %>
		/>
		<Filter	id="Filter" CanExport="1" CanHide="0" CanFocus="1"/>
	</Head>
	
	<Solid>
		<I	id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4" CanHide="0"
			NAVType="Pager" 
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
	
	<LeftCols>
		<C Name="EQUIPMENT"					Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="LOCATION"					Type="Enum" Align="left"   Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>	
		<C Name="LINE"						Type="Text" Align="center" Visible="0" Width="120" CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="LINE_UID"					Type="Text" Align="left"   Visible="0" Width="120" CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="LINE_NAME"					Type="Text" Align="left"   Visible="0" Width="120" CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="EQUIPMENT_UID"				Type="Text" Align="left"   Visible="1" Width="80"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " OnChange="fnCheckEquipmentUid(Grid,Row,Col);"/>
		<C Name="EQUIPMENT_NAME"			Type="Text" Align="left"   Visible="1" Width="200" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
	</LeftCols>
	
	<Cols>
		<C Name="GRADE"						Type="Enum" Align="left"   Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="GRADE"/>/>
		<C Name="ASSET_NO"					Type="Text" Align="left"   Visible="1" Width="80"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="MODEL"						Type="Text" Align="left"   Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="DIMENSION"					Type="Text" Align="left"   Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="WEIGHT"					Type="Int"  Align="right"  Visible="1" Width="80"  CanEdit="0" CanExport="1" CanHide="1" Format="#,##0"  ExportStyle='mso-number-format:"#,##0";'/>
		<C Name="WEIGHT_UNIT"				Type="Enum" Align="left"   Visible="1" Width="60"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="WEIGHT_UNIT"/>/>
		<C Name="ACQUISITION_VALUE"			Type="Int"  Align="right"  Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" Format="#,##0"  ExportStyle='mso-number-format:"#,##0";'/>
		<C Name="CURRENCY"					Type="Enum" Align="left"   Visible="1" Width="60"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="CURRENCY"/>/>
		<C Name="COUNTRY"					Type="Enum" Align="left"   Visible="1" Width="80"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="COUNTRY"/>/>
		<C Name="MANUFACTURER"				Type="Text" Align="left"   Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="DATE_ACQUISITION"			Type="Date" Align="center" Visible="1" Width="110" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd"/>
		<C Name="DATE_INSTALL"				Type="Date" Align="center" Visible="1" Width="110" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd"/>
		<C Name="DATE_OPERATE"				Type="Date" Align="center" Visible="1" Width="110" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd"/>
		<C Name="DATE_VALID"				Type="Date" Align="center" Visible="1" Width="110" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd"/>
		<C Name="COST_CENTER"				Type="Text" Align="left"   Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="MEMO"						Type="Text" Align="left"   Visible="1" Width="200" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		
		<C Name="ATTACH_GRP_NO"				Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="0" CanHide="1"/>
		<C Name="ATTACH_GRP_NO2"			Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="0" CanHide="1"/>
		<C Name="ATTACH_GRP_NO3"			Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="0" CanHide="1"/>
		<C Name="URL"						Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="0" CanHide="1"/>
		<C Name="HISTORY"					Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="0" CanHide="1"/>
	</Cols>
	
	<RightCols>
		<C Name="DETAIL"					Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="0" CanHide="1"/>
	</RightCols>
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,BOM등록,항목,새로고침"
				EmptyType="Html"	EmptyRelWidth="1"	Empty="        "  
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				BOM등록Type="Html" BOM등록="&lt;a href='#none' title='BOM등록' class=&quot;treeButton treeSave&quot;
					onclick='fnSave(&quot;${gridId}&quot;)'>BOM등록&lt;/a>"
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton01 icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
	/>
		
</Grid>