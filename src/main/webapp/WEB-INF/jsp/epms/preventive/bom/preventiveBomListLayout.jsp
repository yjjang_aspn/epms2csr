<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%--
	Class Name	: preventiveBomListLayout.jsp
	Description : 예방보전BOM 목록 그리드 레이아웃
    author		: 서정민
    since		: 2018.05.09
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.09	서정민		최초 생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="PreventiveBomList"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1"			SuppressCfg="0"			
			Editing="1"				Deleting="0"		Selecting="0"		Dragging="0"					
			Filtering="1"			StandardFilter="3"	ClearSelected="2"
			Paging="1"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"				
			CopyCols="3"			PasteFocused="3"
	/>
	
	<Head>
		<Header	id="Header"	Align="center" CanExport="1" CanHide="0"
			WORK				= "오더ID"
			WORK_TYPE			= "오더유형ID"
			WORK_TYPE_NAME		= "오더유형"
			PREVENTIVE_TYPE		= "업무구분"
			PART				= "파트"
			CHECK_TYPE			= "점검구분"
			CHECK_DETAIL		= "점검항목"
			CHECK_STANDARD		= "판단기준"
			CHECK_TOOL			= "진단장비"
			
			EQUIPMENT			= "<spring:message code='epms.system.id'    />"    <%  // 설비ID"      %>
			EQUIPMENT_UID		= "<spring:message code='epms.system.uid'   />"    <%  // 설비코드"      %>
			EQUIPMENT_NAME		= "<spring:message code='epms.system.name'  />"    <%  // 설비명"       %>
			
			PREVENTIVE_BOM		= "예방보전BOM ID"
			MANAGER				= "담당자"
			CHECK_CYCLE			= "주기"
			DEL_YN				= "사용여부"
			
			PREVENTIVE_RESULT	= "예방보전실적ID"
			DATE_PLAN			= "차기계획일"
			LOCATIONPART		= "파트"
		/>
		<Filter	id="Filter" CanExport="1" CanHide="0" CanFocus="1"/>
	</Head>
	
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
	 
	<Cols>
		<C Name="PREVENTIVE_BOM"			Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="WORK"						Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="WORK_TYPE"					Type="Text" Align="center" Visible="0" Width="100" CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="WORK_TYPE_NAME"			Type="Text" Align="left"   Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="PREVENTIVE_TYPE"			Type="Enum" Align="left"   Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="PREVENTIVE_TYPE"/>/>
		<C Name="PART"						Type="Enum" Align="left"   Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="PART" companyId="${ssCompanyId}"/>/>
		<C Name="CHECK_TYPE"				Type="Enum" Align="left"   Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="CHECK_TYPE"/>/>
		<C Name="CHECK_DETAIL"				Type="Text" Align="left"   Visible="1" Width="180" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="CHECK_STANDARD"			Type="Text" Align="left"   Visible="0" Width="180" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="CHECK_TOOL"				Type="Text" Align="left"   Visible="0" Width="100" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		
		<C Name="EQUIPMENT"					Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="EQUIPMENT_UID"				Type="Text" Align="left"   Visible="1" Width="80"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT_NAME"			Type="Text" Align="left"   Visible="1" Width="180" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
	</Cols>
	
	<RightCols>
		<C Name="MANAGER"					Type="Enum" Align="left"   Visible="1" Width="80"  CanEdit="1" CanExport="1" CanHide="0" Related="LOCATIONPART" Enum='${repairMemberListOpt.NAME}'	EnumKeys='${repairMemberListOpt.CODE}' ${relatedCombo}/>
		<C Name="CHECK_CYCLE"				Type="Int"  Align="center" Visible="1" Width="70"  CanEdit="1" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="PREVENTIVE_RESULT"			Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="DATE_PLAN"					Type="Date" Align="center" Visible="1" Width="110" CanEdit="1" CanExport="1" CanHide="1" Format="yyyy/MM/dd"/>
		<C Name="DEL_YN"					Type="Enum" Align="left"   Visible="1" Width="70"  CanEdit="1" CanExport="1" CanHide="1" <tag:enum codeGrp="DEL_YN"/> OnChange="fnChangeSts(Grid,Row,Col);"/>
		<C Name="LOCATIONPART"				Type="Text" Align="left"   Visible="0" Width="100" CanEdit="0" CanExport="0" CanHide="0" CanFilter="0"/>
	</RightCols>

	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,저장,항목,새로고침,인쇄,엑셀" 
				EmptyType="Html"	EmptyRelWidth="1"	Empty="        " 
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				저장Type="Html" 저장="&lt;a href='#none' title='저장' class=&quot;treeButton treeSave&quot;
					onclick='fnSave(&quot;${gridId}&quot;)'>저장&lt;/a>"
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
				엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
	
	
</Grid>