<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
	Class Name	: preventiveWorkListData.jsp
	Description : 오더 목록 그리드 데이터
    author		: 서정민
    since		: 2018.05.09
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.09	 서정민		최초 생성

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I
			WORK					= "${item.WORK}"
			COMPANY_ID				= "${item.COMPANY_ID}"
			WORK_TYPE				= "${item.WORK_TYPE}"
			WORK_TYPE_NAME			= "${fn:replace(item.WORK_TYPE_NAME, '"', '&quot;')}"
			PREVENTIVE_TYPE			= "${item.PREVENTIVE_TYPE}"
			PART					= "${item.PART}"
			CHECK_TYPE				= "${item.CHECK_TYPE}"
			CHECK_DETAIL			= "${fn:replace(item.CHECK_DETAIL, '"', '&quot;')}"
			CHECK_STANDARD			= "${fn:replace(item.CHECK_STANDARD, '"', '&quot;')}"
			CHECK_TOOL				= "${fn:replace(item.CHECK_TOOL, '"', '&quot;')}"
			DEL_YN					= "${item.DEL_YN}"
			<c:if test='${item.DEL_YN eq "N"}'>
				DEL_YNClass="gridStatus3"
			</c:if>
			<c:if test='${item.DEL_YN eq "Y"}'>
				DEL_YNClass="gridStatus4"
			</c:if>
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>