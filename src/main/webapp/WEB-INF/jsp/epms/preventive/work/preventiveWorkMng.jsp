<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%--
	Class Name	: preventiveWorkMng.jsp
	Description : 오더마스터 관리
    author		: 서정민
    since		: 2018.05.09
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.09	 서정민		최초 생성

--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<!-- 예방보전관리 : 오더마스터관리 -->
<script type="text/javascript">

<%-- 전역변수 --%>
var focusedRow = null;		<%-- Focus Row : [오더유형] --%>
var focusedRow2 = null;		<%-- Focus Row : [오더] --%>
var lastChildRow = null;	<%-- Last Child Row : [오더유형] --%>
var selRows = null;			<%-- 선택된 rows --%>	
var msg = "";

$(document).ready(function(){
	
});

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "CategoryList") {	<%-- [오더유형] 클릭시 --%>

		if (focusedRow == null || focusedRow.CATEGORY != row.CATEGORY || row.Added == 1){
			<%-- 1. 포커스 --%>
			if(focusedRow != null){
				grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
			}
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			
			focusedRow = row;
			focusedRow2 = null;
			lastChildRow = row.lastChild;
			
			if (row.TREE != null && row.TREE != "" && row.Added != 1) {
				
				$('#COMPANY_ID').val(row.COMPANY_ID);		<%-- 회사코드 --%>
				$('#CATEGORY_TYPE').val(row.CATEGORY_TYPE);	<%-- 카테고리 유형 : 'WORK' --%>
				$('#CATEGORY').val(row.CATEGORY);			<%-- 오더유형ID --%>
				$('#CATEGORY_NAME').val(row.CATEGORY_NAME);	<%-- 오더유형명 --%>
				$('#TREE').val(row.TREE);					<%-- 구조체 --%>
				$('#preventiveWorkListTitle').html("오더 : " + "[ " + row.CATEGORY_NAME + " ]");
				getOrderList();
			}
		}
	}
	else if(grid.id == "PreventiveWorkList") {	<%-- [오더] 클릭시 --%>

		<%-- 1. 포커스  --%>
		if(focusedRow2 != null){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow2 = row;
		
	}
}

<%-- 선택된 오더유형에 해당되는 오더 조회 --%>
function getOrderList(){
	Grids.PreventiveWorkList.Source.Data.Url = "/epms/preventive/work/preventiveWorkListData.do?TREE=" + $('#TREE').val()
											 + "&CATEGORY_TYPE=" + $('#CATEGORY_TYPE').val();
	Grids.PreventiveWorkList.ReloadBody();	
}


<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source, data, func){
	if(grid.id == "PreventiveWorkList"){
		if(data != ""){
			callLoadingBar();
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "PreventiveWorkList"){
					setTimeout($.unblockUI, 100);
				}
			}
		}
	}
}

<%-- 추가 버튼 --%>
function addRowGrid(gridNm) {

	if (gridNm == "CategoryList") {
		if (focusedRow == null || focusedRow == 'undefined') {	<%-- 선택된 오더유형이 없을 경우 --%>
			
			return false;
		} else {												<%-- 선택된 오더유형이 있을 경우, 하위 레벨 오더유형 추가 --%>
			
			Grids[gridNm].SetAttribute(focusedRow,null,"Expanded",1,1);
			
			row = Grids[gridNm].AddRow(focusedRow, null, true);
			Grids[gridNm].SetAttribute(row,"CATEGORY_UID","CanEdit","1",1);				<%-- 카테고리코드 수정 가능 --%>
			Grids[gridNm].SetValue(row, "CATEGORY_TYPE", focusedRow.CATEGORY_TYPE, 1);	<%-- 카테고리유형 : 'WORK'(오더유형) --%>
			Grids[gridNm].SetValue(row, "PARENT_CATEGORY", focusedRow.CATEGORY, 1);		<%-- 부모카테고리(오더유형) --%>
			Grids[gridNm].SetValue(row, "TREE", focusedRow.TREE, 1);					<%-- 구조체 --%>
			Grids[gridNm].SetValue(row, "DEPTH", focusedRow.DEPTH + 1, 1);				<%-- 레벨 --%>
			Grids[gridNm].SetValue(row, "COMPANY_ID", focusedRow.COMPANY_ID, 1);		<%-- 회사코드 --%>
			Grids[gridNm].SetValue(row, "LOCATION", focusedRow.LOCATION, 1);			<%-- 공장코드 --%>
			Grids[gridNm].SetValue(row, "SUB_ROOT", focusedRow.SUB_ROOT, 1);			<%-- 공장구분 --%>
			<%-- 출력순서 --%>
			if(lastChildRow == null || lastChildRow == 'undefined'){
				Grids[gridNm].SetValue(row, "SEQ_DSP", "1", 1);					
			}else{
				Grids[gridNm].SetValue(row, "SEQ_DSP", lastChildRow.SEQ_DSP+1, 1);
			}
			lastChildRow = row;
		}
	} else if (gridNm == "PreventiveWorkList") {
		
		row = Grids[gridNm].AddRow(null, Grids[gridNm].GetFirst(),true);
		Grids[gridNm].SetValue(row, "COMPANY_ID", $('#COMPANY_ID').val(), 1);
		Grids[gridNm].SetValue(row, "WORK_TYPE", $('#CATEGORY').val(), 1);
		Grids[gridNm].SetValue(row, "WORK_TYPE_NAME", $('#CATEGORY_NAME').val(), 1);
		Grids[gridNm].SetValue(row, "DEL_YN", "N", 1);
	}
}

<%-- 드래그시 출력순서 세팅 : 드래그한 row의  data 세팅 --%>
Grids.OnRowMove = function (grid, row, oldparent, oldnext){
	
	if(grid.id == "CategoryList"){	<%-- [오더유형]의 경우 --%>
		var location;
		var parent_category;
		var tree;
		var depth;
		

		if(row.parentNode.tagName != 'I'){	<%-- 1레벨로 이동할 경우 --%>
			location = "";
			parent_category = "";
			tree = row.CATEGORY;
			depth = 2;
		}
		else {								<%-- 2레벨 이하로 이동할 경우 --%>
			location = row.parentNode.LOCATION;
			parent_category = row.parentNode.CATEGORY;
			if(row.Added == true){
				tree = row.parentNode.TREE;
			}
			else{
				tree = row.parentNode.TREE+"$"+row.CATEGORY;
			}
			depth = row.parentNode.DEPTH+1;
		}
		
		<%-- 1. 공장코드 --%>
		grid.SetValue(row, "LOCATION", location, 1);	
		
		<%-- 2. 부모카테고리  ID --%>
		grid.SetValue(row, "PARENT_CATEGORY", parent_category, 1);
		
		<%-- 3. 구조체 --%>
		grid.SetValue(row, "TREE", tree, 1);
		
		<%-- 4. 레벨 --%>
		grid.SetValue(row, "DEPTH", depth, 1);
		
		var lineRow = grid.GetFirst(row);
		var endRow = grid.GetLast(row);
		
		if(!(lineRow == null || lineRow == 'undefined')){
			var gap = depth - grid.GetValue(lineRow, "DEPTH") + 1;
			
			for(lineRow; lineRow; lineRow = grid.GetNextVisible(lineRow)){
				
				grid.SetValue(lineRow, "DEPTH", grid.GetValue(lineRow, "DEPTH")+gap, 1);
				
				if(grid.GetValue(lineRow, "CATEGORY") == grid.GetValue(endRow, "CATEGORY")){
					break;
				}
			}
		}
		
		<%-- 5. 출력순서 --%>
		<%-- 5-1. 이동 이전 부모 이하 설비의 출력순서 변경 --%>
		var i =1;
		var lineRow2 = grid.GetFirst(oldparent);
		if(!(lineRow2 == null || lineRow2 == 'undefined')){
			do{
				
				grid.SetValue(lineRow2, "SEQ_DSP", i, 1);
				lineRow2 = lineRow2.nextSibling;
				i++;
				
			}while(!(lineRow2 == null || lineRow2 == 'undefined'));
		}
		<%-- 5-2. 이동 이후 부모 이하 설비의 출력순서 변경 --%>
		i=1;
		var lineRow3 = grid.GetFirst(row.parentNode);
		if(!(lineRow3 == null || lineRow3 == 'undefined')){
			do{
				
				grid.SetValue(lineRow3, "SEQ_DSP", i, 1);
				lineRow3 = lineRow3.nextSibling;
				i++;
				
			}while(!(lineRow3 == null || lineRow3 == 'undefined'));
		}	
	}
};

<%-- Validation --%>
function dataValidation(gridNm){
	var cnt = 0;
	var chkCnt = 0;
	var grid = Grids[gridNm];
	
	if(gridNm == "CategoryList"){
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){  
			if(row.Changed || row.Added){
				
				cnt++;
				
				<%-- 오더유형명 --%>
				var CATEGORY_NAME = grid.GetValue(row, "CATEGORY_NAME");
				if(CATEGORY_NAME==''){
					chkCnt++;
					grid.SetAttribute(row,"CATEGORY_NAME","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"CATEGORY_NAME","Color","#FFFFAA",1);
				}
			}
		}
	}	
	else if(gridNm == "PreventiveWorkList"){
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow2);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){  
			if(row.Changed || row.Added){
				
				cnt++;
				
				<%-- 업무구분 --%>
				var PREVENTIVE_TYPE  = grid.GetValue(row, "PREVENTIVE_TYPE");
				if(PREVENTIVE_TYPE==''){
					chkCnt++;
					grid.SetAttribute(row,"PREVENTIVE_TYPE","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"PREVENTIVE_TYPE","Color","#FFFFAA",1);
				}
				
				<%-- 파트 --%>
				var PART = grid.GetValue(row, "PART");
				if(PART==''){
					grid.SetAttribute(row,"PART","Color","#FF6969",1);	
					cnt++;
				}else{
					grid.SetAttribute(row,"PART","Color","#FFFFAA",1);
				}

				<%-- 점검구분 --%>
				var CHECK_TYPE = grid.GetValue(row, "CHECK_TYPE");
				if(CHECK_TYPE==''){
					chkCnt++;
					grid.SetAttribute(row,"CHECK_TYPE","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"CHECK_TYPE","Color","#FFFFAA",1);
				}
			}
		}
	}
	
	<%-- 추가 및 변경된 데이터가 없을 경우 --%>
	if(cnt == 0){
		return false;
	}
	
	<%-- 필수값 체크 --%>
	if(chkCnt == 0){
		return true;
	}
	else{
		alert("필수값을 입력해주세요.");
		return false;
	}
}

<%-- [저장] 버튼 : 오더유형 및 오더정보 CRUD(관리자) --%>
function fnSave(gridNm){
	
	<%-- 유효성 체크 --%>
    if(!dataValidation(gridNm)){
    	return;
    }
	
	if(Grids[gridNm].ActionValidate()){
		if(gridNm == "CategoryList"){
			if(confirm("오더유형정보를 저장하시겠습니까?")){
	
				Grids[gridNm].ActionSave();
	
			}
		}
		else if(gridNm == "PreventiveWorkList"){
			if(confirm("오더정보를 저장하시겠습니까?")){

				Grids[gridNm].ActionSave();

			}
		}
	}
}

<%-- 그리드 데이터 저장 후 --%> 
Grids.OnAfterSave  = function (grid){
	if(grid.id == "CategoryList"){
		
		Grids.CategoryList.ReloadBody();
		
	}else if(grid.id == "PreventiveWorkList"){
		
		grid.ReloadBody();
		
	}
};

<%-- 오더마스터 활성여부 변경시 --%>
function fnChangeSts(grid,row,col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return 
	}
	
	if(row.DEL_YN == "Y"){
		grid.SetAttribute(row, "DEL_YN", "Class", "gridStatus4");
	}
	else if(row.DEL_YN == "N"){
		grid.SetAttribute(row, "DEL_YN", "Class", "gridStatus3");
	}
}

</script>
</head>
<body>

<!-- 화면 UI Area Start  -->
<div id="contents">
	<input type="hidden" id="CATEGORY" name="CATEGORY"/>			<%-- 카테고리 ID --%>
	<input type="hidden" id="CATEGORY_NAME" name="CATEGORY_NAME"/>	<%-- 카테고리 명 --%>
	<input type="hidden" id="CATEGORY_TYPE" name="CATEGORY_TYPE"/>	<%-- 카테고리 유형 --%>
	<input type="hidden" id="TREE" name="TREE"/>					<%-- 구조체 --%>
	<input type="hidden" id="COMPANY_ID" name="COMPANY_ID"/>		<%-- 회사코드 --%>
	
	<div class="fl-box panel-wrap03" style="width:20%"><!-- 원하는 비율로 직접 지정하여 사용 -->
		<h5 class="panel-tit">오더 유형</h5>
		<div class="panel-body">
			<!-- 트리그리드 : s -->
			<div id="categoryList">
				<bdo	Debug="Error"
						Data_Url="/sys/category/categoryData.do?CATEGORY_TYPE=WORK"
						Layout_Url="/sys/category/categoryLayout.do?CATEGORY_TYPE=WORK"
						Upload_Url="/sys/category/categoryEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"
				>
				</bdo>
			</div>
			<!-- e:grid-wrap -->
			<!-- 트리그리드 : e -->
		</div>
	</div>
	
	<div class="fl-box panel-wrap03" style="width:80%"><!-- 원하는 비율로 직접 지정하여 사용 -->
		<h5 class="panel-tit mgn-l-10" id="preventiveWorkListTitle">오더</h5>
		<div class="panel-body mgn-l-10">
			<!-- 트리그리드 : s -->
			<div id="preventiveWorkList">
				<bdo	Debug="Error"
						Layout_Url="/epms/preventive/work/preventiveWorkMngLayout.do"
						Upload_Url="/epms/preventive/work/preventiveWorkEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"	
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=오더목록.xls&dataName=data"
				>
				</bdo>
			</div>
			<!-- e:grid-wrap -->
			<!-- 트리그리드 : e -->
		</div>
	</div>
	
</div>
</body>
</html>