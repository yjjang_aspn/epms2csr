<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%--
  Class Name : noticeView.jsp
  Description : 공지사항 상세 화면
  Modification Information
 
      수정일                    수정자                   수정내용
   -------     --------    ---------------------------
   2018.04.17    김영환              최초 생성

    author   : 김영환
    since    : 2018.04.17
--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script src="/js/jquery/jquery-migrate-1.2.1.min.js" type="text/javascript" ></script>
<script type="text/javascript">
	var returnType = "";
	var chkBool = "";
	$(document).ready(function() {
		
		// 파일 다운로드
		$('.btnFileDwn').on('click', function(){
			var ATTACH = $(this).data("attach");
			var ATTACH_GRP_NO = $('#ATTACH_GRP_NO').val();
			var src = '/attach/fileDownload.do?ATTACH_GRP_NO='+ ATTACH_GRP_NO + '&ATTACH='+ATTACH;
			$("#fileDownFrame").attr("src",src);
		});
		// Synap 첨부파일 뷰어
// 		$('.btnFileDwn').on('click', function(){
// 			var ATTACH = $(this).data("attach");
// 			var src = '/sys/attach/SynapDocView.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH='+ATTACH;
// 			$("#fileDownFrame").attr("src",src);
// 		});
		
		$('#listBtn').on('click', function(){
			var sub_Auth = $("#sub_Auth").val();
			if(sub_Auth == "N") {
				location.href = "/epms/notice/noticeList.do";
		    } else if(sub_Auth == "Y") {
				location.href = "/epms/notice/noticeMng.do";
		    }
			
		});
		
		$('#updateBtn').on('click', function(){
			fnSubmit("boardDetailForm", "/epms/notice/noticeEditForm.do");
		});
		
		$('#deleteBtn').on('click', function(){
			var crm = confirm("<spring:message code='noticeList.wantDelete' />"); // 게시글을 삭제 하시겠습니까?
			
			if(crm){
				fnSubmit("boardDetailForm", "/epms/notice/deleteNotice.do");				
			}
		});
		
	});

</script>
</head>
<body>
<div class="scl"  style="margin-top:5px; height:99%">
	<div style="padding:15px;">
		<div class="fl-box panel-wrap top">
			<div class="panel-body">
				<div class="mgn-b-15">
				
					<form id="boardDetailForm" name="boardDetailForm" method="post">
						<input type="hidden" id="COMPANY_ID"  name="COMPANY_ID" value="${view.COMPANY_ID }"/>
						<input type="hidden" id="NOTICE"  name="NOTICE" value="${view.NOTICE }"/>
						<input type="hidden" id="ATTACH_GRP_NO"  name="ATTACH_GRP_NO" value="${view.ATTACH_GRP_NO }"/>
						<input type="hidden" name="page" value="${view.page }"/>
						<input type="hidden" id="sub_Auth" name="sub_Auth" value="${auth }"/>
					</form>
	
					<div class="overflow">
						<div class="f-l">
							<a href="#none" id="listBtn" class="btn comm st02"><spring:message code='button.list' /></a> <!-- 목록 -->
						</div>
						<c:if test="${auth eq 'Y'}">
							<div class="f-r">
								<a href="#none" id="updateBtn" class="btn comm st01"><spring:message code='button.modify' /></a> <!-- 수정 -->
								<a href="#none" id="deleteBtn" class="btn comm st02"><spring:message code='button.delete' /></a> <!-- 삭제 -->
							</div>
						</c:if>
					</div>
					
					<div class="viewBoardWrap mgn-t-10">
						<h3 class="viewBoardTit">${view.TITLE }</h3>
						<div class="viewBoardInfo">
							<c:choose>
								<c:when test="${view.TYPE eq '1'}">
									<p class="f-l" style="color:blue; font-weight: bold;"><spring:message code='notice.type.general' /></p> <!-- 일반공지 -->
								</c:when>
								<c:otherwise>
									<p class="f-l" style="color:red; font-weight: bold;"><spring:message code='notice.type.pop' /></p> <!-- 팝업공지 -->
									<p class="f-l" style="color:red; font-weight: bold;">(${view.POPSTART_DT } - ${view.POPEND_DT })</p> <!-- 팝업공지기간 -->
								</c:otherwise>
							</c:choose>
							<p class="f-l"><span>${view.user_nm }</span><span>${view.REG_DT }</span></p>
						</div>
						<div class="viewBoardCon">
							<c:if test="${!empty attachList }">
								<div class="attachFileWrap">
									<div class="attachFileTit">
										<a href="#none" class="down ir-pm"><spring:message code='file.openFile' /></a><!-- 첨부파일 펼치기 -->
										<p><spring:message code='file.file' /><span>${attachSize}<spring:message code='file.fileCnt' /></span></p>
									</div>
									<ul class="attachFileLst" style="display:none;">
										<c:forEach var="item" items="${attachList}" varStatus="idx">
											<li>
												<a href="#none" class="attachDownBtn btnFileDwn" data-attach="${item.ATTACH}">
													<img src="/images/icon/web/attachDownload.gif">
													<span>${item.FILE_NAME} (${item.FILE_SIZE_TXT})</span>
												</a>
											</li>
										</c:forEach>
									</ul>
								</div>
							</c:if>
							<div class="viewContent">
								<!-- 정규식 치환 -->
								${fn:replace(fn:replace(fn:replace(fn:replace(fn:replace(fn:replace(fn:replace(view.CONTENT,'&amp;','&'),'<p>',''),'</p>','<br/>'),'&quot;','"'),'&lt;','<'),'&gt;','>'),'&nbsp;',' ')}
							</div>
						</div>
					</div>
	
					<iframe id="fileDownFrame" style="width:0px;height:0px;border:0px"></iframe>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>