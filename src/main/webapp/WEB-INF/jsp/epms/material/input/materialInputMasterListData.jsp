<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
	Class Name	: materialInputMasterListData.jsp
	Description : 자재정보조회 그리드 데이터
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성

--%>
<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I		
			MATERIAL				= "${item.MATERIAL}"
			LOCATION				= "${item.LOCATION}"
			MATERIAL_TYPE			= "${item.MATERIAL_TYPE}"
			MATERIAL_GRP1			= "${item.MATERIAL_GRP1}"
			MATERIAL_GRP2			= "${item.MATERIAL_GRP2}"
			MATERIAL_GRP3			= "${item.MATERIAL_GRP3}"
			MATERIAL_UID			= "${item.MATERIAL_UID}"
			MATERIAL_NAME			= "${fn:replace(item.MATERIAL_NAME, '"', '&quot;')}"
			MODEL					= "${fn:replace(item.MODEL, '"', '&quot;')}"
			SPEC					= "${fn:replace(item.SPEC, '"', '&quot;')}"
			COUNTRY					= "${item.COUNTRY}"
			MANUFACTURER			= "${fn:replace(item.MANUFACTURER, '"', '&quot;')}"
			MEMO					= "${fn:replace(item.MEMO, '"', '&quot;')}"
			UNIT					= "${item.UNIT}"
			UNIT_NAME				= "${item.UNIT_NAME}"
			
			STOCK_OPTIMAL			= "${item.STOCK_OPTIMAL}"
			<c:choose>
				<c:when test="${item.STOCK_OPTIMAL > 0}">
					STOCK					= "${item.STOCK}"
					STOCK_CONDITION			= "${item.STOCK_CONDITION}"
					<c:if test="${item.STOCK_CONDITION == '1'}">
						STOCK_CONDITIONClass="gridStatus3"
					</c:if>
					<c:if test="${item.STOCK_CONDITION == '2'}">
						STOCKClass="gridStatus4"
						STOCK_CONDITIONClass="gridStatus4"
					</c:if>
				</c:when>
				<c:otherwise>
					STOCK					= ""
					STOCK_CONDITION			= ""
				</c:otherwise>
			</c:choose>
			
			DEL_YN					= "${item.DEL_YN}"
			<c:if test='${item.DEL_YN eq "1"}'>
				DEL_YNClass="gridStatus3"
			</c:if>
			<c:if test='${item.DEL_YN eq "2"}'>
				DEL_YNClass="gridStatus4"
			</c:if>
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>