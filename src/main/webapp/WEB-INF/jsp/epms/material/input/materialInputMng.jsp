<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: materialInputMng.jsp
	Description : 자재관리 > 입고현황 관리 메인화면
	author		: 김영환
	since		: 2018.04.04
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.04	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script type="text/javascript">

var flag = "";				<%-- [입고현황] 삭제 Flag --%>
var focusedRow = null;		<%-- [자재유형] 선택된 ROW --%>
var focusedRow2 = null;		<%-- [자재목록] 선택된 ROW  --%>
var focusedRow3 = null;		<%-- [입고현황] 선택된 ROW  --%>
var callLoadingYn = true;	<%-- 로딩바 호출여부 --%>

$(document).ready(function(){		
	
	<%-- DatePicker 설정 --%>
	$( ".datePic" ).datepicker({
		   dateFormat   : "yy-mm-dd",
	       prevText     : "click for previous months" ,
	       nextText     : "click for next months" ,
	       showOtherMonths   : true ,
	       selectOtherMonths : false
	}).each(function(){
			if($(this).hasClass('thisDay')){
			$(this).val(stDate);
			}
	});

	<%-- 검색 시작일이 종료일보다 이후인지 체크 (이후라면, 종료일을 시작일로 변경) --%>
	$('#startDt').on('change', function(){
		if(parseInt($('#startDt').val(),10) > parseInt($('#endDt').val(),10)){
			$('#endDt').val($('#startDt').val());
		}
	});
	
	<%-- 검색 시작일이 종료일보다 이후인지 체크 (이후라면, 종료일을 시작일로 변경) --%>
	$('#startDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#endDt').val($('#startDt').val());
		}
	});
	
	<%-- 검색 종료일이 시작일보다 이전인지 체크 (이전이라면, 시작일을 종료일로 변경) --%>
	$('#endDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#startDt').val($('#endDt').val());
		}
	});
	
});


<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	if(grid.id == "CategoryList") {	<%-- [라인] 클릭시 --%>
	
		if (focusedRow == null || focusedRow.CATEGORY != row.CATEGORY){
			
			<%-- 1. 포커스 --%>
			if(focusedRow != null){
				grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
			}
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			focusedRow = row;
			
			<%-- 2. 설비 목록 조회 --%>
			$('#CATEGORY').val(row.CATEGORY);			<%-- 카테고리 번호 --%>
			$('#CATEGORY_TYPE').val(row.CATEGORY_TYPE);	<%-- 카테고리 유형 : 'MATERIAL' --%>
			$('#CATEGORY_NAME').val(row.CATEGORY_NAME);	<%-- 카테고리 명 : 라인명 --%>
			$('#TREE').val(row.TREE);					<%-- 구조체 --%>
			$('#materialListTitle').html("자재 : " + "[ " + row.CATEGORY_NAME + " ]");
			
			<%-- 3. 자재번호 초기화 --%>
			$('#MATERIAL').val("");
			$('#materialInListTitle').text("입고 현황 : " + "[ " + row.CATEGORY_NAME + " ]");
			
			fnGetMaterialList();
		}
	<%-- 자재 조회 이벤트 --%>
	}else if(grid.id == "MaterialList3") {
		<%-- 포커스 시작 --%>
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		if(focusedRow2 != null){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		focusedRow2 = row;
		<%-- 포커스 종료 --%>
		$('#MATERIAL').val(row.MATERIAL);
		
		<%-- 입고 현황 조회 --%>
		$('#materialInListTitle').text("입고 현황 : " + "[ " + row.MATERIAL_NAME + " ]");
		fnGetMaterialInputList();
	
	<%-- 입고현황 클릭 이벤트 --%>		
	} else if(grid.id == "MaterialInList") {
		
		<%-- 1. 포커스 --%>
		if(focusedRow3 != null){
			grid.SetAttribute(focusedRow3,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow3 = row;
	}
}
	
<%-- 선택된 라인에 해당되는 설비 조회 --%>
function fnGetMaterialList(){
	Grids.MaterialList3.Source.Data.Url = "/epms/material/master/materialMasterListData.do?TREE=" + $('#TREE').val()
											 + "&CATEGORY=" + $('#CATEGORY').val()
											 + "&CATEGORY_TYPE=" + $('#CATEGORY_TYPE').val()
											 + "&editYn=N";
	Grids.MaterialList3.ReloadBody();	
}

<%-- 선택된 자재/자재유형에 해당되는 입고 조회 --%>
function fnGetMaterialInputList(){
	Grids.MaterialInList.Source.Data.Url = "/epms/material/input/materialInputListData.do?MATERIAL=" + $('#MATERIAL').val()
										 + "&TREE=" + $('#TREE').val()							 
										 + "&startDt=" + $('#startDt').val()
										 + "&endDt=" + $('#endDt').val();
	Grids.MaterialInList.ReloadBody();
}


<%-- 삭제 버튼 --%>
function fnDel(gridNm){
	
	var selRows = Grids[gridNm].GetSelRows();

	for (var i = 0; i < selRows.length; i++) {
		Grids[gridNm].SetValue(selRows[i], "flag", 'del', 1);
	}
	
	if(Grids[gridNm].ActionValidate()){
		
		if(confirm("선택하신 입고내역을 삭제하시겠습니까?")){
			
			msg = "정상적으로 삭제되었습니다.";
			Grids[gridNm].ActionSave();

		}else{
			Grids[gridNm].ReloadBody();
		}
	}
}

<%-- Validation --%>
function dataValidation(gridNm){
	var cnt = 0;
	var grid = Grids[gridNm];
	
	if(gridNm == "MaterialInList"){
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow3);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){  
			
			if(row.Changed || row.Added){
				
				cnt++;
				
			}
		}
	}
	
	<%-- 추가 및 변경된 데이터가 없을 경우 --%>
	if(cnt == 0){
		return false;
	}else{
		return true;
	}
}

<%-- 저장 버튼 --%>
function fnSave(gridNm){
	
	<%-- 유효성 체크 --%>
    if(!dataValidation(gridNm)){
    	return;
    }
	
	if(Grids[gridNm].ActionValidate()){
		
		if(confirm("변경하신 입고내역을 저장하시겠습니까?")){
			
			Grids[gridNm].ActionSave();

		}else{
			Grids[gridNm].ReloadBody();
		}
	}
}

Grids.OnAfterSave  = function (grid){
	
	if(Grids.MaterialList3.Source.Data.Url!=""){
		Grids.MaterialList3.ReloadBody();
	}
	Grids.MaterialInList.ReloadBody();
	
};

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source,data, func){
	if(grid.id == "MaterialList3"){			<%-- [자재] 목록 조회시 --%>
		if(data != ""){
			callLoadingBar();
			callLoadingYn = false;
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "MaterialList3"){
					fnGetMaterialInputList();
				}
			}
		}
	}else if(grid.id == "MaterialInList"){ <%-- [입고현황] 목록 조회시 --%>
		if(data != ""){
			if(callLoadingYn == true){
				callLoadingBar();
				callLoadingYn = false;
			}
		}
		Grids.OnDataReceive  = function(grid, source){
			if(grid.id == "MaterialInList"){
				setTimeout($.unblockUI, 100);
				callLoadingYn = true;
			}
		}
	}
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

</script>
</head>
<body>
<div id="contents">
	<input type="hidden" id="MATERIAL" name="MATERIAL"/>
	<input type="hidden" id="TREE" name="TREE"/>
	<input type="hidden" id="CATEGORY" name="CATEGORY"/>
	<input type="hidden" id="CATEGORY_TYPE" name="CATEGORY_TYPE"/>
	<input type="hidden" id="CATEGORY_NAME" name="CATEGORY_NAME"/>
	
	<div class="fl-box panel-wrap03" style="width:20%">
		<h5 class="panel-tit">자재유형</h5>
		<div class="panel-body">
			<%-- 트리그리드[자재유형] : s --%>
			<div id="categoryList">
				<bdo	Debug="Error"
						Data_Url="/sys/category/categoryData.do?CATEGORY_TYPE=MATERIAL"
						Layout_Url="/sys/category/categoryNotEditLayout.do?CATEGORY_TYPE=MATERIAL"
						Upload_Url="/sys/category/categoryEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"
				>
				</bdo>
			</div>
			<%-- 트리그리드[자재유형] : e --%>
		</div>
	</div>
	
	<div class="fl-box panel-wrap03" style="width:30%;">
		<h5 class="panel-tit mgn-l-10" id="materialListTitle">자재</h5>
		<div class="panel-body mgn-l-10">
			<%-- 트리그리드[자재] : s --%>
			<div id="MaterialList3">
				<bdo	Debug="Error"
						Data_Url=""
						Layout_Url="/epms/material/input/materialInputMasterListLayout.do"
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=MaterialList3.xls&dataName=data"
				>
				</bdo>
			</div>
			<%-- 트리그리드[자재] : e --%>
		</div>
	</div>
	
	<div class="fl-box panel-wrap04" style="width:50%">
		<h5 class="panel-tit mgn-l-10" id="materialInListTitle">입고 현황</h5>
		<div class="inq-area-inner type06 ab">
			<div class="wrap-inq">
				<div class="inq-clmn" id="aterialInOutDate">
					<h4 class="tit-inq" id="materialOut">입고일</h4>
					<div class="prd-inp-wrap">
						<span class="prd-inp">
							<span class="inner">
								<input type="text" id="startDt" class="inp-comm datePic inpCal" readonly="readonly" title="입고시작일" value="${startDt}">
							</span>
						</span>	
						<span class="prd-inp">
							<span class="inner">
								<input type="text" id="endDt" class="inp-comm datePic inpCal" readonly="readonly" title="입고종료일" value="${endDt}">
							</span>
						</span>	
					</div>
				</div>
			</div>
			<a href="#none" id="selData" class="btn comm st01">검색</a>
		</div>
		<div class="panel-body mgn-l-10 mgn-t-20">
			<div id="MaterialInList">
				<bdo	Debug="Error"
						Layout_Url="/epms/material/input/materialInputMngLayout.do"
						Upload_Url="/epms/material/input/materialInputMngEdit.do"  Upload_Xml="2" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols"	
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=입고현황_${startDt}-${endDt}.xls&dataName=data"
						>
				</bdo>
			</div>
		</div>
	</div>
</div>
<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<%-- 첨부파일 조회/수정 --%>
<div class="modal fade modalFocus" id="popFileManageForm"    data-backdrop="static" data-keyboard="false"></div>

</body>
</html>


