<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
	Class Name	: materialInputListData.jsp
	Description : 자재관리 > 입고현황 조회 > 입고 목록 그리드 데이터
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성

--%>
<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I	
			MATERIAL_INOUT_TYPE		= "${item.MATERIAL_INOUT_TYPE}"
			MATERIAL_INOUT			= "${item.MATERIAL_INOUT}"
			MATERIAL_INOUT_ITEM		= "${item.MATERIAL_INOUT_ITEM}"
			DATE_INOUT				= "${item.DATE_INOUT}"
			QNTY					= "${item.QNTY}"
			PRICE					= "${item.PRICE}"			
			MATERIAL				= "${item.MATERIAL}"
			MATERIAL_TYPE			= "${item.MATERIAL_TYPE}"
			MATERIAL_TYPE_NAME		= "${item.MATERIAL_TYPE_NAME}"
			MATERIAL_UID			= "${item.MATERIAL_UID}"
			MATERIAL_NAME			= "${fn:replace(item.MATERIAL_NAME, '"', '&quot;')}"
			SPEC					= "${fn:replace(item.SPEC, '"', '&quot;')}"
			UNIT					= "${item.UNIT}"
			STOCK					= "${item.STOCK}"
			STOCK_OPTIMAL			= "${item.STOCK_OPTIMAL}"
			LOCATION				= "${item.LOCATION}"
			MEMBER					= "${item.MEMBER}"
			MEMBER_NAME				= "${item.MEMBER_NAME}"
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>