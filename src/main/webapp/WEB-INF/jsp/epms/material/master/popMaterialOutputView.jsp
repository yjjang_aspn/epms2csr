<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld"                 %>
<%@ taglib prefix="c"	   uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: popMaterialOutputView.jsp
	Description : 자재관리 > 자재정보조회/관리 > 출고현황 팝업화면
    author		: 김영환
    since		: 2018.03.21
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자	수정내용
	----------  ------	---------------------------
	2018.03.21	김영환	최초생성

--%>

<script type="text/javascript">	
$(document).ready(function(){
	
	<%-- 출고현황 JS : s --%>
	<%-- 검색 시작일이 종료일보다 이후인지 체크 (이후라면, 종료일을 시작일로 변경) --%>
	$('#popMaterialOutputView_startDt').on('change', function(){
		var startDt = $('#popMaterialOutputView_startDt').val().replaceAll("-","");
		var endDt = $('#popMaterialOutputView_endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#popMaterialOutputView_endDt').val($('#popMaterialOutputView_startDt').val());
		}
	});
	
	<%-- 검색 종료일이 시작일보다 이전인지 체크 (이전이라면, 시작일을 종료일로 변경) --%>
	$('#popMaterialOutputView_endDt').on('change', function(){
		var startDt = $('#popMaterialOutputView_startDt').val().replaceAll("-","");
		var endDt = $('#popMaterialOutputView_endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#popMaterialOutputView_startDt').val($('#popMaterialOutputView_endDt').val());
		}
	});
	
	<%-- 출고일 from - to 검색 --%>
	<%-- 출고 현황 조회 --%>
	$('#popMaterialOutputView_selData').on('click', function(){
		Grids.MaterialOutList.Source.Data.Url = "/epms/material/output/materialOutputListData.do?MATERIAL=" + $('#MATERIAL').val()
												 + "&startDt=" + $('#popMaterialOutputView_startDt').val()
												 + "&endDt=" + $('#popMaterialOutputView_endDt').val();
		Grids.MaterialOutList.ReloadBody();

	});
	<%-- 출고현황 JS : e --%>

});
</script>

<%-- 출고현황 Layer : s --%>
<div class="modal fade modalFocus" id="popMaterialOutputView" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-60"> <!-- 원하는 width 값 입력 -->				
		 <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">
			   	    <spring:message code='epms.output' /> <spring:message code='epms.status' /><!-- 출고현황 -->
			   	</h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">

					<div class="modal-section">
						<div class="wd-per-100 rel">
							<div class="ab wrap-inq mgn-l-5">
								<ul class="wrap-inq">
									<li class="inq-clmn">
										<h4 class="tit-inq"><spring:message code='epms.object.type' /> : </h4><!-- 자재유형 -->
										<h4 class="tit-inq" id="popMaterialOutputView_MATERIAL_TYPE"></h4>
									</li>
<!-- 									<li class="inq-clmn"> -->
<!-- 										<h4 class="tit-inq">자재유형 : </h4> -->
<!-- 										<h4 class="tit-inq" id="popMaterialOutputView_MATERIAL_GRP1"></h4> -->
<!-- 									</li> -->
									<li class="inq-clmn">
										<h4 class="tit-inq"><spring:message code='epms.object.uid' /> : </h4><!-- 자재코드 -->
										<h4 class="tit-inq" id="popMaterialOutputView_MATERIAL_UID"></h4>
									</li>
									<li class="inq-clmn">
										<h4 class="tit-inq"><spring:message code='epms.object.name' /> : </h4><!-- 자재명 -->
										<h4 class="tit-inq" id="popMaterialOutputView_MATERIAL_NAME"></h4>
									</li>
								</ul>
							
								<ul class="wrap-inq" style="margin-top: 3px;">		
									<li class="inq-clmn" id=materialInOutDate>
										<h4 class="tit-inq" id="materialIn">출고일 : </h4>
										<div class="prd-inp-wrap">
											<span class="prd-inp">
												<span class="inner">
													<input type="text" id="popMaterialOutputView_startDt" class="inp-comm datePic inpCal" readonly="readonly" title="입고시작일">
												</span>
											</span>	
											<span class="prd-inp">
												<span class="inner">
													<input type="text" id="popMaterialOutputView_endDt" class="inp-comm datePic inpCal" readonly="readonly" title="입고종료일">
												</span>
											</span>	
										</div>
										<a href="#none" class="btn comm st01" id="popMaterialOutputView_selData">검색</a>
									</li>
								</ul>
							</div>	
						</div>	
						<div class="panel-body wd-per-100 mgn-t-25" id="MaterialOutList">	
							<bdo	Debug="Error"
									Data_Url=""
									Layout_Url="/epms/material/output/materialOutputListLayout.do"
									Export_Data="data" Export_Type="xls"
							>
							</bdo>
						</div>	
					</div><%-- 내용 : e --%>
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div> <%-- 출고현황 Layer : e --%>

