<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: materialMasterList.jsp
	Description : 자재관리 > 자재정보조회/관리 > 공용 레이어 관리
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성

--%>

<script type="text/javascript">	
$(document).ready(function(){
	
	$('.datePic').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	}).each(function(){
		if($(this).hasClass('thisMonth')){
			var date = new Date();
			$(this).val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '01');
		}else if($(this).hasClass('noneType') == false){
			var date = new Date();
			$(this).val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	});
	
	<%-- 입고현황 JS : s --%>
	<%-- 검색 시작일이 종료일보다 이후인지 체크 (이후라면, 종료일을 시작일로 변경) --%>
	$('#popMaterialInputView_startDt').on('change', function(){
		if(parseInt($('#popMaterialInputView_startDt').val(),10) > parseInt($('#popMaterialInputView_endDt').val(),10)){
			$('#popMaterialInputView_endDt').val($('#popMaterialInputView_startDt').val());
		}
	});
	
	<%-- 검색 종료일이 시작일보다 이전인지 체크 (이전이라면, 시작일을 종료일로 변경) --%>
	$('#popMaterialInputView_endDt').on('change', function(){
		if(parseInt($('#popMaterialInputView_startDt').val(),10) > parseInt($('#popMaterialInputView_endDt').val(),10)){
			$('#popMaterialInputView_startDt').val($('#popMaterialInputView_endDt').val());
		}
	});
	
	<%-- 입고일 from - to 검색 --%>
	<%-- 입고 현황 조회 --%>
	$('#popMaterialInputView_selData').on('click', function(){
		Grids.MaterialInList.Source.Data.Url = "/epms/material/input/materialInputListData.do?MATERIAL=" + $('#MATERIAL').val()
												 + "&startDt=" + $('#popMaterialInputView_startDt').val()
												 + "&endDt=" + $('#popMaterialInputView_endDt').val();
		Grids.MaterialInList.ReloadBody();

	});
	<%-- 입고현황 JS : e --%>
	
	<%-- 출고현황 JS : s --%>
	<%-- 검색 시작일이 종료일보다 이후인지 체크 (이후라면, 종료일을 시작일로 변경) --%>
	$('#popMaterialOutputView_startDt').on('change', function(){
		if(parseInt($('#popMaterialOutputView_startDt').val(),10) > parseInt($('#popMaterialOutputView_endDt').val(),10)){
			$('#popMaterialOutputView_endDt').val($('#popMaterialOutputView_startDt').val());
		}
	});
	
	<%-- 검색 종료일이 시작일보다 이전인지 체크 (이전이라면, 시작일을 종료일로 변경) --%>
	$('#popMaterialOutputView_endDt').on('change', function(){
		if(parseInt($('#popMaterialOutputView_startDt').val(),10) > parseInt($('#popMaterialOutputView_endDt').val(),10)){
			$('#popMaterialOutputView_startDt').val($('#popMaterialOutputView_endDt').val());
		}
	});
	
	<%-- 출고일 from - to 검색 --%>
	<%-- 출고 현황 조회 --%>
	$('#popMaterialOutputView_selData').on('click', function(){
		Grids.MaterialOutList.Source.Data.Url = "/epms/material/output/materialOutputListData.do?MATERIAL=" + $('#MATERIAL').val()
												 + "&startDt=" + $('#popMaterialOutputView_startDt').val()
												 + "&endDt=" + $('#popMaterialOutputView_endDt').val();
		Grids.MaterialOutList.ReloadBody();

	});
	<%-- 출고현황 JS : e --%>
	
});

</script>
<%-- 첨부파일 조회/수정 --%>
<div class="modal fade modalFocus" id="popFileManageForm"   data-backdrop="static" data-keyboard="false"></div>		

<%-- 자재번호 공용사용 --%>
<input type="hidden" id="MATERIAL" name="MATERIAL">

<%-- 입고현황 Layer : s --%>
<div class="modal fade modalFocus" id="popMaterialInputView" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-60"> <!-- 원하는 width 값 입력 -->				
		 <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">입고현황</h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">

					<div class="fl-box panel-wrap01" style="width:100%">
						<div class="wd-per-100 rel">
							<div class="ab wrap-inq mgn-l-20">
								<ul class="wrap-inq">
									<li class="inq-clmn">
										<h4 class="tit-inq">자재유형 : </h4>
										<h4 class="tit-inq" id="popMaterialInputView_MATERIAL_TYPE"></h4>
									</li>
									<li class="inq-clmn">
										<h4 class="tit-inq">자재코드 : </h4>
										<h4 class="tit-inq" id="popMaterialInputView_MATERIAL_UID"></h4>
									</li>
									<li class="inq-clmn">
										<h4 class="tit-inq">자재명 : </h4>
										<h4 class="tit-inq" id="popMaterialInputView_MATERIAL_NAME"></h4>
									</li>
								</ul>
							
								<ul class="wrap-inq">		
									<li class="inq-clmn" id=materialInOutDate>
										<h4 class="tit-inq" id="materialIn">입고일 : </h4>
										<div class="prd-inp-wrap">
											<span class="prd-inp">
												<span class="inner">
													<input type="text" id="popMaterialInputView_startDt" class="inp-comm datePic inpCal" readonly="readonly" title="입고시작일">
												</span>
											</span>	
											<span class="prd-inp">
												<span class="inner">
													<input type="text" id="popMaterialInputView_endDt" class="inp-comm datePic inpCal" readonly="readonly" title="입고종료일">
												</span>
											</span>	
										</div>
										<a href="#none" class="btn comm st01" id="popMaterialInputView_selData">검색</a>
									</li>
								</ul>
							</div>	
						</div>	
						<div class="auto-h02 mgn-t-30" id="MaterialInList">	
							<bdo	Debug="Error"
									Data_Url=""
									Layout_Url="/epms/material/input/materialInputListLayout.do"
									Export_Data="data" Export_Type="xls"
							>
							</bdo>
						</div>	
					</div><%-- 내용 : e --%>
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div> <%-- 입고현황 Layer : e --%>

<%-- 출고현황 Layer : s --%>
<div class="modal fade modalFocus" id="popMaterialOutputView" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-60"> <!-- 원하는 width 값 입력 -->				
		 <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">출고현황</h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">

					<div class="fl-box panel-wrap01" style="width:100%">
						<div class="wd-per-100 rel">
							<div class="ab wrap-inq mgn-l-20">
								<ul class="wrap-inq">
									<li class="inq-clmn">
										<h4 class="tit-inq">자재유형 : </h4>
										<h4 class="tit-inq" id="popMaterialOutputView_MATERIAL_TYPE"></h4>
									</li>
<!-- 									<li class="inq-clmn"> -->
<!-- 										<h4 class="tit-inq">자재유형 : </h4> -->
<!-- 										<h4 class="tit-inq" id="popMaterialOutputView_MATERIAL_GRP1"></h4> -->
<!-- 									</li> -->
									<li class="inq-clmn">
										<h4 class="tit-inq">자재코드 : </h4>
										<h4 class="tit-inq" id="popMaterialOutputView_MATERIAL_UID"></h4>
									</li>
									<li class="inq-clmn">
										<h4 class="tit-inq">자재명 : </h4>
										<h4 class="tit-inq" id="popMaterialOutputView_MATERIAL_NAME"></h4>
									</li>
								</ul>
							
								<ul class="wrap-inq">		
									<li class="inq-clmn" id=materialInOutDate>
										<h4 class="tit-inq" id="materialIn">출고일 : </h4>
										<div class="prd-inp-wrap">
											<span class="prd-inp">
												<span class="inner">
													<input type="text" id="popMaterialOutputView_startDt" class="inp-comm datePic inpCal" readonly="readonly" title="입고시작일">
												</span>
											</span>	
											<span class="prd-inp">
												<span class="inner">
													<input type="text" id="popMaterialOutputView_endDt" class="inp-comm datePic inpCal" readonly="readonly" title="입고종료일">
												</span>
											</span>	
										</div>
										<a href="#none" class="btn comm st01" id="popMaterialOutputView_selData">검색</a>
									</li>
								</ul>
							</div>	
						</div>	
						<div class="auto-h02 mgn-t-30" id="MaterialInList">	
							<bdo	Debug="Error"
									Data_Url=""
									Layout_Url="/epms/material/output/materialOutputListLayout.do"
									Export_Data="data" Export_Type="xls"
							>
							</bdo>
						</div>	
					</div><%-- 내용 : e --%>
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div> <%-- 출고현황 Layer : e --%>

<%-- 상세내역 조회 --%>
<div class="modal fade modalFocus" id="popMaterialDtl"		data-backdrop="static" data-keyboard="false"></div>		


