<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld"                 %>
<%@ taglib prefix="c"	   uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: materialMasterMng.jsp
	Description : 자재관리 > 자재정보 관리 메인화면
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<script type="text/javascript">

<%-- 전역변수 --%>
var focusedRow  = null;		<%-- Focus Row : [자재유형] --%>
var focusedRow2 = null;		<%-- Focus Row : [자재] --%>
var chkRow = "";
var selDeptRow = null;
var msg = "";
var flag1 = "Y";
var lineRow = null;

$(document).ready(function(){	
	
	<%-- datePic 설정 --%>
	$('.datePic').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	}).each(function(){
		if($(this).hasClass('thisMonth')){
			var date = new Date();
			$(this).val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '01');
		}else if($(this).hasClass('noneType') == false){
			var date = new Date();
			$(this).val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	});
	
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function (e) {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>
	});
	
});

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
		
	}
	
	if(grid.id == "CategoryList") {	<%-- [자재유형] 클릭시 --%>
		
		if (focusedRow == null || focusedRow.CATEGORY != row.CATEGORY || row.Added == 1){
			
			<%-- 1. 포커스 --%>
			if(focusedRow != null){
				grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
			}
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			
			focusedRow = row;
			focusedRow2 = null;
			lastChildRow = row.lastChild;
			
			if (row.TREE != null && row.TREE != "" && row.Added != 1) {
				
				$('#COMPANY_ID').val(row.COMPANY_ID);
				$('#LOCATION').val(row.LOCATION);
				$('#CATEGORY_TYPE').val(row.CATEGORY_TYPE);
				$('#CATEGORY').val(row.CATEGORY);
				$('#CATEGORY_NAME').val(row.CATEGORY_NAME);
				$('#TREE').val(row.TREE);
				$('#materialMasterListTitle').html("<spring:message code='epms.object' /> : " + "[ " + row.CATEGORY_NAME + " ]");  // 자재
				fnGetMaterialList();
				
				<%-- 자재등록 레이어팝업에 회사코드,공장,자재유형값 추가 --%>
				$('#popMaterialAddForm_COMPANY_ID').val(row.COMPANY_ID);
				$('#popMaterialAddForm_LOCATION').val(row.LOCATION);
				$('#popMaterialAddForm_MATERIAL_TYPE').val(row.CATEGORY);
				$('#popMaterialAddForm_MATERIAL_TYPE_NAME').val(row.CATEGORY_NAME);
			}
		}
	}
	else if(grid.id == "MaterialMasterMng") {	<%-- [자재] 클릭시 --%>
	
		<%-- 1. 포커스 --%>
		if(focusedRow2 != null){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow2 = row;
		
	}
}

<%-- 그리드 변경 이벤트 --%>
Grids.OnChange = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return
	}
	
	if(row.Selected == true){
		var selRows = grid.GetSelRows();
		var getValue = grid.GetValue(row, col);
		
		for(var i=0; i <selRows.length;i++){
			grid.SetValue(selRows[i], col, getValue, 1);
		}
	}

}

<%-- 선택된 자재유형에 해당되는 자재 조회 --%>
function fnGetMaterialList(){
	Grids.MaterialMasterMng.Source.Data.Url = "/epms/material/master/materialMasterListData.do?TREE=" + $('#TREE').val()
										    + "&CATEGORY_TYPE=" + $('#CATEGORY_TYPE').val()
										    + "&editYn=Y";
	Grids.MaterialMasterMng.Source.Layout.Url = "/epms/material/master/materialMasterMngLayout.do?LOCATION=" + $('#LOCATION').val();
	Grids.MaterialMasterMng.Reload();	
}

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source,data, func){
	if(grid.id == "MaterialMasterMng"){
		if(data != ""){
			callLoadingBar();
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "MaterialMasterMng"){
					setTimeout($.unblockUI, 100);
				}
			}
		}
	}
}

<%-- 그리드 추가버튼 --%>
function addRowGrid(gridNm) {
	
	if (gridNm == "CategoryList") {
		
		if (focusedRow == null || focusedRow == 'undefined') {	<%-- 선택된 자재유형이 없을 경우 --%>
			return false;
		} else {																		<%-- 선택된 자재유형이 있을 경우, 하위 레벨 자재유형 추가 --%>
			
			Grids[gridNm].SetAttribute(focusedRow,null,"Expanded",1,1);
			
			row = Grids[gridNm].AddRow(focusedRow, null, true);
			Grids[gridNm].SetValue(row, "CATEGORY_TYPE", focusedRow.CATEGORY_TYPE, 1);	<%-- 카테고리유형 : 'MATERIAL'(자재유형) --%>
			Grids[gridNm].SetValue(row, "PARENT_CATEGORY", focusedRow.CATEGORY, 1);		<%-- 부모카테고리(자재유형) --%>
			Grids[gridNm].SetValue(row, "TREE", focusedRow.TREE, 1);					<%-- 구조체 --%>
			Grids[gridNm].SetValue(row, "DEPTH", focusedRow.DEPTH + 1, 1);				<%-- 레벨 --%>
			Grids[gridNm].SetValue(row, "COMPANY_ID", focusedRow.COMPANY_ID, 1);		<%-- 회사코드 --%>
			Grids[gridNm].SetValue(row, "LOCATION", focusedRow.LOCATION, 1);			<%-- 공장코드 --%>
			<%-- 출력순서 --%>
			if(lastChildRow == null || lastChildRow == 'undefined'){
				Grids[gridNm].SetValue(row, "SEQ_DSP", "1", 1);					
			}else{
				Grids[gridNm].SetValue(row, "SEQ_DSP", lastChildRow.SEQ_DSP+1, 1);
			}
			lastChildRow = row;
		}
	} 
	else if (gridNm == "MaterialMasterMng") {
		
		if (focusedRow == null || focusedRow == 'undefined') {	<%-- 선택된 자재유형이 없을 경우 --%>
			return false;
		} else {
			var row = Grids[gridNm].AddRow(null, Grids[gridNm].GetFirst(), true);
			Grids[gridNm].SetValue(row, "COMPANY_ID", $('#COMPANY_ID').val(), 1);	
			Grids[gridNm].SetValue(row, "LOCATION", $('#LOCATION').val(), 1);
			Grids[gridNm].SetValue(row, "MATERIAL_TYPE", $('#CATEGORY').val(), 1);
			Grids[gridNm].SetValue(row, "MATERIAL_TYPE_NAME", $('#CATEGORY_NAME').val(), 1);
			Grids[gridNm].SetValue(row, "DASHBOARD_OPT1", 'N', 1);
			Grids[gridNm].SetValue(row, "DASHBOARD_OPT2", 'N', 1);
			Grids[gridNm].SetValue(row, "DEL_YN", 'N', 1);
		}
	}
}

<%-- 드래그시 출력순서 세팅 : 드래그한 row의  data 세팅 --%>
Grids.OnRowMove = function (grid, row, oldparent, oldnext){
	
	if(grid.id == "CategoryList"){	<%-- [자재유형]의 경우 --%>
		var location;
		var parent_category;
		var tree;
		var depth;
		
		if(row.parentNode.tagName != 'I'){	<%-- 1레벨로 이동할 경우 --%>
			location = "";
			parent_category = "";
			tree = row.CATEGORY;
			depth = 2;
		}
		else {								<%-- 2레벨 이하로 이동할 경우 --%>
			location = row.parentNode.LOCATION;
			parent_category = row.parentNode.CATEGORY;
			if(row.Added == true){
				tree = row.parentNode.TREE;
			}
			else{
				tree = row.parentNode.TREE+"$"+row.CATEGORY;
			}
			depth = row.parentNode.DEPTH+1;
		}
		
		<%-- 1. 공장코드 --%>
		grid.SetValue(row, "LOCATION", location, 1);	
		
		<%-- 2. 부모카테고리  ID --%>
		grid.SetValue(row, "PARENT_CATEGORY", parent_category, 1);
		
		<%-- 3. 구조체 --%>
		grid.SetValue(row, "TREE", tree, 1);
		
		<%-- 4. 레벨 --%>
		grid.SetValue(row, "DEPTH", depth, 1);
		
		var lineRow = grid.GetFirst(row);
		var endRow = grid.GetLast(row);
		
		if(!(lineRow == null || lineRow == 'undefined')){
			var gap = depth - grid.GetValue(lineRow, "DEPTH") + 1;
			
			for(lineRow; lineRow; lineRow = grid.GetNextVisible(lineRow)){
				
				grid.SetValue(lineRow, "DEPTH", grid.GetValue(lineRow, "DEPTH")+gap, 1);
				
				if(grid.GetValue(lineRow, "CATEGORY") == grid.GetValue(endRow, "CATEGORY")){
					break;
				}
			}
		}
		
		<%-- 5. 출력순서 --%>
		<%-- 5-1. 이동 이전 부모 이하 자재유형의 출력순서 변경 --%>
		var i =1;
		var lineRow2 = grid.GetFirst(oldparent);
		if(!(lineRow2 == null || lineRow2 == 'undefined')){
			do{
				
				grid.SetValue(lineRow2, "SEQ_DSP", i, 1);
				lineRow2 = lineRow2.nextSibling;
				i++;
				
			}while(!(lineRow2 == null || lineRow2 == 'undefined'));
		}
		<%-- 5-2. 이동 이후 부모 이하 자재유형의 출력순서 변경 --%>
		i=1;
		var lineRow3 = grid.GetFirst(row.parentNode);
		if(!(lineRow3 == null || lineRow3 == 'undefined')){
			do{
				
				grid.SetValue(lineRow3, "SEQ_DSP", i, 1);
				lineRow3 = lineRow3.nextSibling;
				i++;
				
			}while(!(lineRow3 == null || lineRow3 == 'undefined'));
		}			
	}
};

<%-- [자재유형변경] 버튼 --%>
function fnChangeMaterialType(){
	
	var selRows = Grids.MaterialMasterMng.GetSelRows();
	if(selRows.length > 0){
		Grids.popMaterialTypeList.Source.Data.Url = "/sys/category/categoryData.do?CATEGORY_TYPE=MATERIAL&LOCATION="+$('#LOCATION').val();
		Grids.popMaterialTypeList.Reload();	
		
		fnModalToggle('popMaterialTypeList');
	}
	else{
		alert("자재유형을 변경할 자재를 선택해주세요.");
		return;
	}
}

<%-- 자재유형변경 (자재유형변경 팝업에서 선택이후) --%>
function fnSetLine(CATEGORY, CATEGORY_NAME){
	var selRows = Grids.MaterialMasterMng.GetSelRows();
	
	for(var i=0; i <selRows.length;i++){
		Grids["MaterialMasterMng"].SetValue(selRows[i], "MATERIAL_TYPE_NAME", CATEGORY_NAME, 1);
		Grids["MaterialMasterMng"].SetValue(selRows[i], "MATERIAL_TYPE", CATEGORY, 1);
	}
}

<%-- Validation --%>
function dataValidation(gridNm){
	var cnt = 0;
	var chkCnt = 0;
	var grid = Grids[gridNm];
	
	if(gridNm == "MaterialMasterMng"){

		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow2);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){
			
			cnt++;
			
			if(row.Changed || row.Added){
				
				var MATERIAL_UID = grid.GetValue(row, "MATERIAL_UID");		<%-- 자재코드 --%>
				if(MATERIAL_UID==''){
					chkCnt++;
					grid.SetAttribute(row,"MATERIAL_UID","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"MATERIAL_UID","Color","#FFFFFF",1);
				}
				
				var MATERIAL_NAME = grid.GetValue(row, "MATERIAL_NAME");		<%-- 자재명 --%>
				if(MATERIAL_NAME==''){
					chkCnt++;
					grid.SetAttribute(row,"MATERIAL_NAME","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"MATERIAL_NAME","Color","#FFFFFF",1);
				}
			}
		}
		
	} else if(gridNm == "CategoryList") {
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){
			
			cnt++;
			
			if(row.Changed || row.Added){
				
				var CATEGORY_NAME = grid.GetValue(row, "CATEGORY_NAME");		<%-- 자재유형명 --%>
				
				if(CATEGORY_NAME==''){
					chkCnt++;
					grid.SetAttribute(row,"CATEGORY_NAME","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"CATEGORY_NAME","Color","#FFFFFF",1);
				}
			}
		}
		
	}
	
	<%-- 추가 및 변경된 데이터가 없을 경우 --%>
	if(cnt == 0){
		return false;
	}
	
	<%-- 필수값 체크 --%>
	if(chkCnt == 0){
		return true;
	}
	else{
		alert("필수값을 입력해주세요.");
		return false;
	}
	
}

<%-- [저장] 버튼 : 자재유형정보 및 자재정보 CRUD(관리자) --%>
function fnSave(gridNm){
	
	<%-- 유효성 체크 --%>
    if(!dataValidation(gridNm)){
    	return;
    }
		
	if(Grids[gridNm].ActionValidate()){
		if(gridNm == "CategoryList"){
			if(confirm("자재유형 정보를 저장하시겠습니까?")){
	
				msg = "자재유형 정보가 정상적으로 변경되었습니다.";
				Grids[gridNm].ActionSave();
	
			}
		}
		else if(gridNm == "MaterialMasterMng"){
			if(confirm("자재정보를 저장하시겠습니까?")){

				msg = "자재정보가 정상적으로 변경되었습니다.";
				Grids[gridNm].ActionSave();

			}
		}
	}
}

<%-- 그리드 데이터 저장 후 --%>
Grids.OnAfterSave  = function (grid){
	grid.ReloadBody();
};

<%-- 팝업창 자재등록 및 첨부파일 변경 이후 새로고침 --%>
function fnReload_attachEdit(){
	
	Grids.MaterialMasterMng.Source.Data.Url = "/epms/material/master/materialMasterListData.do?TREE=" + $('#TREE').val()
											 + "&CATEGORY_TYPE=" + $('#CATEGORY_TYPE').val()
											 + "&editYn=Y";
	Grids.MaterialMasterMng.ReloadBody();
	
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- 그리드와 Param 값을 동시에 사용 --%>
function fnOpenLayerWithGrid(url, name){
	$.ajax({
		type: 'POST',
		url: url,
		dataType: 'json',
		processData: false, 
		contentType:false,
		success: function (json) {
			$("#MATERIAL").val(json.MATERIAL);
			$("#"+name+"_startDt").val(isEmpty(json.startDt) == true ? '' : json.startDt);
			$("#"+name+"_endDt").val(  isEmpty(json.endDt)   == true ? '' : json.endDt);
			$("#"+name+"_MATERIAL_TYPE").text(isEmpty(json.item.MATERIAL_TYPE_NAME) == true ? '-' : "["+json.item.MATERIAL_TYPE_NAME+"]");
			$("#"+name+"_MATERIAL_GRP1").text(isEmpty(json.item.MATERIAL_GRP1_NAME) == true ? '-' : "["+json.item.MATERIAL_GRP1_NAME+"]");
			$("#"+name+"_MATERIAL_UID").text( isEmpty(json.item.MATERIAL_UID)       == true ? '-' : "["+json.item.MATERIAL_UID+"]");
			$("#"+name+"_MATERIAL_NAME").text(isEmpty(json.item.MATERIAL_NAME)      == true ? '-' : "["+json.item.MATERIAL_NAME+"]");
			
			if(name == "popMaterialInputView") {
				Grids.MaterialInList.Source.Data.Url = "/epms/material/input/materialInputListData.do?MATERIAL=" + $('#MATERIAL').val()
				 + "&startDt=" + $('#popMaterialInputView_startDt').val()
				 + "&endDt=" + $('#popMaterialInputView_endDt').val();
				
				Grids.MaterialInList.ReloadBody();
			} else if(name == "popMaterialOutputView") {
				Grids.MaterialOutList.Source.Data.Url = "/epms/material/output/materialOutputListData.do?MATERIAL=" + $('#MATERIAL').val()
				 + "&startDt=" + $('#popMaterialOutputView_startDt').val()
				 + "&endDt=" + $('#popMaterialOutputView_endDt').val();
				 
				Grids.MaterialOutList.ReloadBody();
			}
			
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
			loadEnd();
		}
	});
	
	fnModalToggle(name);
	
}

<%-- [자재등록] 레이어 팝업 호출 --%>
function fnAddMaterial(){
	if($('#popMaterialAddForm_MATERIAL_TYPE').val() == ''){
		alert('자재유형을 먼저 선택하세요.');
	}else{
		
		console.log('[자재등록] 레이어 팝업 호출');
		
		// FORM RESET
		$.each( $('#materialAddForm :input') , function(index, elem){
		    
			var tarInput = $(this).attr("id") ;
			
			console.log('# materialAddForm.' + tarInput + ' = ' + $(this).val() );
		    
		    if ( tarInput != 'MODULE' 
		    		&& tarInput != 'popMaterialAddForm_COMPANY_ID'
		    		&& tarInput != 'popMaterialAddForm_LOCATION'
		    		&& tarInput != 'popMaterialAddForm_MATERIAL_TYPE'
		    		&& tarInput != 'popMaterialAddForm_MATERIAL_TYPE_NAME' ) {
		    
		    	if ( tarInput == 'popMaterialAddForm_STOCK' || tarInput == 'popMaterialAddForm_STOCK_OPTIMAL' ) {
		    		
		    		$(this).val( '0' );
		    		
		    	} else {
		    		
			    	$(this).val( '' );
		    	}
		    }
		    
		});
		
		fnModalToggle('popMaterialAddForm');
	}
}

<%-- 자재 활성여부 변경시 --%>
function fnChangeSts(grid,row,col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return 
	}
	
	if(row.DEL_YN == "Y"){
		grid.SetAttribute(row, "DEL_YN", "Class", "gridStatus4");
	}
	else if(row.DEL_YN == "N"){
		grid.SetAttribute(row, "DEL_YN", "Class", "gridStatus3");
	}
}

<%-- SAP:자재그룹 동기화 버튼(자재그룹) --%>
function fnSyncMaterialGroup(){
	if(confirm("자재그룹 정보를 동기화하시겠습니까?")){
		$.ajax({
			type : 'POST',
			url : '/epms/material/master/updateMaterialGroupInfoAjax.do',
			dataType : 'json',
			contentType : false,
			success : function(json) {	
				
				if(json.E_RESULT == 'S'){
					alert("SAP동기화에 성공하였습니다.");
					Grids.CategoryList.ReloadBody();
				}
				else{
					alert("SAP동기화에 실패하였습니다.\n" + json.E_MESSAGE);
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
				setTimeout($.unblockUI, 100);
			}
		});
	}
}

<%-- SAP SAP 동기화 팝업 호출(자재) --%>
function fnPopSyncMaterial(){
	
	fnOpenLayerWithParam('/epms/material/master/popSyncMaterial.do','popSyncMaterial');
}

<%-- SAP:자재마스터 동기화 버튼(자재) --%>
function fnSyncMaterial(sapSync){
	if(confirm("자재 정보를 동기화하시겠습니까?")){
		$.ajax({
			type : 'POST',
			url : '/epms/material/master/updateMaterialInfoAjax.do?SAP_SYNC_FLAG='+sapSync,
			dataType : 'json',
			success : function(json) {
				
				if(json.E_RESULT == 'S'){
					alert("SAP동기화에 성공하였습니다.");
					Grids.MaterialMasterMng.Source.Data.Url = "/epms/material/master/materialMasterListData.do?TREE=" + $('#TREE').val()
															+ "&CATEGORY_TYPE=" + $('#CATEGORY_TYPE').val()
															+ "&editYn=Y";
														
					Grids.MaterialMasterMng.ReloadBody();
					fnModalToggle('popSyncMaterial');
				}
				else{
					alert("SAP동기화에 실패하였습니다.\n" + json.E_MESSAGE);
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
				setTimeout($.unblockUI, 100);
			}
		});
	}
}

</script>
</head>
<body>
<%-- 화면 UI Area Start --%>
<div id="contents">

	<input type="hidden" id="CATEGORY" name="CATEGORY"/>			<%-- 카테고리 ID --%>
	<input type="hidden" id="CATEGORY_NAME" name="CATEGORY_NAME"/>	<%-- 카테고리 명 --%>
	<input type="hidden" id="CATEGORY_TYPE" name="CATEGORY_TYPE"/>	<%-- 카테고리 유형 --%>
	<input type="hidden" id="TREE" name="TREE"/>					<%-- 구조체 --%>
	<input type="hidden" id="COMPANY_ID" name="COMPANY_ID"/>		<%-- 회사코드 --%>
	<input type="hidden" id="LOCATION" name="LOCATION"/>			<%-- 위치(공장ID) --%>
	<input type="hidden" id="MATERIAL" name="MATERIAL">				<%-- 자재 ID --%>
	
	<div class="fl-box panel-wrap03" style="width:20%">
		<h5 class="panel-tit"><spring:message code='epms.object.type' /></h5><!-- 자재유형 -->
		<div class="panel-body">
			<%-- 트리그리드[자재유형] : s --%>
			<div id="categoryList">
				<bdo	Debug="Error"
						Data_Url="/sys/category/categoryData.do?CATEGORY_TYPE=MATERIAL"
						Layout_Url="/sys/category/categoryLayout.do?CATEGORY_TYPE=MATERIAL"
						Upload_Url="/sys/category/categoryEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"
				>
				</bdo>
			</div>
			<%-- 트리그리드[자재유형] : e --%>
		</div>
	</div>
	
	<div class="fl-box panel-wrap03" style="width:80%;">
		<h5 class="panel-tit mgn-l-10" id="materialMasterListTitle"><spring:message code='epms.object' /></h5><!-- 자재 -->
		<div class="panel-body mgn-l-10">
			<%-- 트리그리드[자재] : s --%>
			<div id="materialMasterMng">
				<bdo	Debug="Error"
						Layout_Url="/epms/material/master/materialMasterMngLayout.do"
						Upload_Url="/epms/material/master/materialMasterListEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"			
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=자재목록.xls&dataName=data"
				>
				</bdo>
			</div>
			<%-- 트리그리드[자재] : e --%>
		</div>
	</div><%-- e:panel-body --%>
</div><%-- 화면 UI Area End --%>

<%------------------------------------- 레이어팝업 시작 -------------------------------------%>

<%-- SAP 자재 동기화 팝업 --%>
<div class="modal fade modalFocus" id="popSyncMaterial" data-backdrop="static" data-keyboard="false"></div>	

<%-- 첨부파일 조회/수정 --%>
<div class="modal fade modalFocus" id="popFileManageForm"    data-backdrop="static" data-keyboard="false"></div>	

<%-- 입고조회 --%>
<%@ include file="/WEB-INF/jsp/epms/material/master/popMaterialInputView.jsp"%>	

<%-- 출고조회 --%>
<%@ include file="/WEB-INF/jsp/epms/material/master/popMaterialOutputView.jsp"%>	

<%-- 상세내역 조회 --%>
<div class="modal fade modalFocus" id="popMaterialMasterDtl" data-backdrop="static" data-keyboard="false"></div>	

<%-- 자재등록팝업 --%>						
<%@ include file="/WEB-INF/jsp/epms/material/master/popMaterialAddForm.jsp" %>

<%-- 자재유형변경 팝업 --%>						
<%@ include file="/WEB-INF/jsp/epms/material/master/popMaterialTypeList.jsp"%>

<%-- 정비실적조회 --%>
<div class="modal fade modalFocus" id="popRepairResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 예방보전실적조회 --%>
<div class="modal fade modalFocus" id="popPreventiveResultForm" data-backdrop="static" data-keyboard="false"></div>
</body>
</html>