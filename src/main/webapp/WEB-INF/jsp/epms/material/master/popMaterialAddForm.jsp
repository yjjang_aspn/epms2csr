<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: materialMasterList.jsp
	Description : 자재관리 > 자재정보관리 (자재등록 팝업) 
    author		: 김영환
    since		: 2018.04.11
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.11	 김영환		최초 생성

--%>

<script type="text/javascript">
var submitBool = true;
var maxFileCnt = 30;
var msg = "";

$(document).ready(function(){
	
	<%-- 등록버튼 --%>
	$('#regBtn').on('click', function(){
		if(submitBool){
			fnValidate();	
		}else{
			alert("등록중입니다. 잠시만 기다려 주세요.");
			return false;
		}
	});
	
	<%-- 취소버튼 --%>
	$('#cancelBtn').on('click', function(){
		window.close();
	});
	
	<%-- 첨부파일 관련 --%>
	for(var i=0; i<1; i++){				
		$('#attachFile').append('<input type="file" multiple id="fileList" name="fileList">');	
		
		var fileType = 'jpeg |gif |jpg |png |bmp |ppt |pptx |doc |hwp |pdf |xlsx |xls |txt |dxf |dwg';
	
		$('#fileList').MultiFile({
			   accept :  fileType
			,	list   : '#detailFileList'+i
			,	max    : 5
			,	STRING : {
					remove: '<img src="/images/com/web/btn_remove.png">'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
					text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" />',
					idx	  : i,
			        denied : "$ext 는(은) 업로드 할수 없는 파일확장자입니다."
				},
		});
		
		<%-- 첨부한 파일의 출력 순서를 자동으로 샛팅 하기 위한 스크립트 - 공통 스크립트로 빠지면 안됨 .개별 항목으로 처리 필수 --%> 
		$('.MultiFile-wrap').change(function(){
			var fileExpIndex = $(this).closest('.fileWrap').find('.fileList > div').children('div:last-child');
			var	fileExp = fileExpIndex.find('.fileExp');
			fileExp.val(fileExpIndex.index() + 1);
		});
	}

});

function fnValidate(){
	submitBool = false;
	
	if( !isNull_J($('#popMaterialAddForm_MATERIAL_UID'), '자재코드를 입력해주세요.') ){
		submitBool = true; 
		return false;
	}
	
	if( !isNull_J($('#popMaterialAddForm_MATERIAL_NAME'), '자재명을 입력해주세요.') ){
		submitBool = true; 
		return false;
	}
	var fileCnt = $("input[name=fileGb]").length;
	
	if(fileCnt > 0){
		$('#attachExistYn').val("Y");
	}else{
		$('#attachExistYn').val("N");
	}
	
	var form = new FormData(document.getElementById('materialAddForm'));
	
	$.ajax({
		type: 'POST',
		url: '/epms/material/master/popMaterialAddInsert.do',
		dataType: 'json',
		data: form,
		processData: false, 
		contentType:false,
		success: function (json) {	
			
			if(json > 0){
				msg = "정상적으로 등록 되었습니다.";
				alert(msg);  
				
				submitBool = true;
				
				fnModalToggle('popMaterialAddForm');
				fnReload_attachEdit();
			}else if(json == 0){
				msg = "이미 등록된 자재입니다.";
				alert(msg);
				submitBool = true;
			}
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
		}
	});
}
</script>

<%-- 자재등록 modal : s --%>
<div class="modal fade modalFocus" id="popMaterialAddForm" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-40">				
		 <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">
			   	    <spring:message code='epms.object' /> <spring:message code='epms.regist' />
			   	</h4><!-- 자재등록 -->
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	
					<%-- 페이지 내용 : s --%>
					<form id="materialAddForm" name="materialAddForm"  method="post" enctype="multipart/form-data">	
						
						<input type="hidden" id="attachExistYn" name="attachExistYn" value=""/>		<%-- 첨부파일 저장여부 --%>
						<input type="hidden" id="MODULE" name="MODULE" value="200"/>				<%-- 파일저장 모듈 --%>
						<input type="hidden" id="popMaterialAddForm_COMPANY_ID" name="COMPANY_ID"/>	<%-- 회사코드 --%>
						<input type="hidden" id="popMaterialAddForm_LOCATION" name="LOCATION"/>		<%-- 공장 --%>
						
						<table class="tb-st">
							<caption class="screen-out"><spring:message code='epms.object' /> <spring:message code='epms.regist' /></caption><!-- 자재등록 -->
							<colgroup>
								<col width="30%" />
								<col width="*" />
							</colgroup>
							<tbody>				
								<tr>
									<th><spring:message code='epms.object.type' />(*)</th><!-- 자재유형 -->
									<td>
										<div class="sel-wrap">
											<input type="hidden" class="inp-comm" 		   title="자재유형"  readonly="readonly" id="popMaterialAddForm_MATERIAL_TYPE" name="MATERIAL_TYPE"/>
											<input type="text"   class="inp-comm readonly" title="자재유형명" readonly="readonly" id="popMaterialAddForm_MATERIAL_TYPE_NAME" name="MATERIAL_TYPE_NAME"/>
										</div>	
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.object.code' />(*)</th><!-- 자재코드 -->
									<td>
										<div class="inp-wrap wd-per-100">
											<input type="text" class="inp-comm" title="자재코드" id="popMaterialAddForm_MATERIAL_UID" name="MATERIAL_UID" />
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.object.name' />(*)</th><!-- 자재명 -->
									<td>
										<div class="inp-wrap wd-per-100">
											<input type="text" class="inp-comm" title="자재명" id="popMaterialAddForm_MATERIAL_NAME" name="MATERIAL_NAME" />
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.model' /></th><!-- 모델명 -->
									<td>
										<div class="inp-wrap wd-per-100">
											<input type="text" class="inp-comm" title="모델명" id="popMaterialAddForm_MODEL" name="MODEL" />
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.spec' /></th><!-- 규격 -->
									<td>
										<div class="inp-wrap wd-per-100">
											<input type="text" class="inp-comm" title="규격" id="popMaterialAddForm_SPEC" name="SPEC" />
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.country' /></th><!-- 제조국 -->
									<td>
										<div class="sel-wrap">
											<tag:combo codeGrp="COUNTRY" choose="Y"/>
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.supplier' /></th><!-- 제조사 -->
									<td>
										<div class="inp-wrap wd-per-100">
											<input type="text" class="inp-comm" title="제조사" id="popMaterialAddForm_MANUFACTURER" name="MANUFACTURER" />
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.unit' /></th><!-- 단위 -->
									<td>
										<div class="sel-wrap">
											<tag:combo codeGrp="UNIT" choose="Y"/>
										</div>	
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.safety.stock' /></th><!-- 안전재고 -->
									<td>
										<div class="inp-wrap wd-per-100">
											<input type="hidden" class="inp-comm" title="현재고" id="popMaterialAddForm_STOCK" name="STOCK" value="0"/>
											<input type="text" class="inp-comm" title="안전재고" id="popMaterialAddForm_STOCK_OPTIMAL" name="STOCK_OPTIMAL" value="0"/>
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.memo' /></th><!-- 메모 -->
									<td>
										<div class="txt-wrap">
											<textarea cols="" rows="3" title="메모" id="popMaterialAddForm_MEMO" name="MEMO"></textarea>
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row"><spring:message code='epms.attachfile' /></th><!-- 첨부파일 -->
									<td>
										<div class="fileWrap">
											<div id="attachFile" class="attachFile"></div>
											<div class="fileList">
												<div id="detailFileList0" class="fileDownLst">
												</div>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
					<div class="pop-bottom">
						<a href="#none" class="btn comm st01" data-dismiss="modal" id="cancelBtn">취소</a>
						<a href="#none" class="btn comm st02" id="regBtn">등록</a> 
					</div>
					<%-- 페이지 내용 : e --%>
					
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal-dialog : e --%>
</div>	<%-- 자재등록 modal : e --%>	
