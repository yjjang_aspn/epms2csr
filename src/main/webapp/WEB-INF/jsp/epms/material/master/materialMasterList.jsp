<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld"                 %>
<%@ taglib prefix="c"	   uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: materialMasterList.jsp
	Description : 자재관리 > 자재정보 조회 메인화면
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성
	2018.04.05    김영환		js함수명 정리, 주석 정리

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<script type="text/javascript">

<%-- 전역변수 --%>
var focusedRow  = null;		<%-- Focus Row : [자재유형] --%>
var focusedRow2 = null;		<%-- Focus Row : [자재] --%>

$(document).ready(function(){	
	
	<%-- datePic 설정 --%>
	$('.datePic').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	}).each(function(){
		if($(this).hasClass('thisMonth')){
			var date = new Date();
			$(this).val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '01');
		}else if($(this).hasClass('noneType') == false){
			var date = new Date();
			$(this).val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	});
	
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function (e) {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>
	});
	
});

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "CategoryList") {	<%-- [자재유형] 클릭시 --%>
		
		if (focusedRow == null || focusedRow.CATEGORY != row.CATEGORY || row.Added == 1){
			
			<%-- 1. 포커스 --%>
			if(focusedRow != null){
				grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
			}
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			
			focusedRow = row;
			<%-- 2. 자재 목록 조회 --%>
			$('#COMPANY_ID').val(row.COMPANY_ID);
			$('#LOCATION').val(row.LOCATION);
			$('#CATEGORY').val(row.CATEGORY);			<%-- 카테고리 번호 --%>
			$('#CATEGORY_TYPE').val(row.CATEGORY_TYPE);	<%-- 카테고리 유형 : 'MATERIAL' --%>
			$('#CATEGORY_NAME').val(row.CATEGORY_NAME);	<%-- 카테고리 명 : 라인명 --%>
			$('#TREE').val(row.TREE);					<%-- 구조체 --%>
			$('#materialListTitle').html(" <spring:message code='epms.object' /> : " + "[ " + row.CATEGORY_NAME + " ]");
			fnGetMaterialList();
		}
	}
	else if(grid.id == "MaterialMasterList") {	<%-- [자재] 클릭시 --%>
	
		<%-- 1. 포커스 --%>
		if(focusedRow2 != null){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow2 = row;
		
	}
}

<%-- 선택된 자재유형에 해당되는 자재 조회 --%>
function fnGetMaterialList(){
	Grids.MaterialMasterList.Source.Data.Url = "/epms/material/master/materialMasterListData.do?TREE=" + $('#TREE').val()
											 + "&CATEGORY_TYPE=" + $('#CATEGORY_TYPE').val()
											 + "&editYn=N";
	Grids.MaterialMasterList.Source.Layout.Url = "/epms/material/master/materialMasterListLayout.do?LOCATION=" + $('#LOCATION').val();
	Grids.MaterialMasterList.Reload();	
}

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source,data, func){
	if(grid.id == "MaterialMasterList"){
		if(data != ""){
			callLoadingBar();
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "MaterialMasterList"){
					setTimeout($.unblockUI, 100);
				}
			}
		}
	}
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- 그리드와 Param 값을 동시에 사용 --%>
function fnOpenLayerWithGrid(url, name){
	$.ajax({
		type: 'POST',
		url: url,
		dataType: 'json',
		processData: false, 
		contentType:false,
		success: function (json) {
			$("#MATERIAL").val(json.MATERIAL);
			$("#"+name+"_startDt").val(isEmpty(json.startDt) == true ? '' : json.startDt);
			$("#"+name+"_endDt").val(  isEmpty(json.endDt)   == true ? '' : json.endDt);
			$("#"+name+"_MATERIAL_TYPE").text(isEmpty(json.item.MATERIAL_TYPE_NAME) == true ? '[조회불가]' : "["+json.item.MATERIAL_TYPE_NAME+"]");
			$("#"+name+"_MATERIAL_GRP1").text(isEmpty(json.item.MATERIAL_GRP1_NAME) == true ? '[조회불가]' : "["+json.item.MATERIAL_GRP1_NAME+"]");
			$("#"+name+"_MATERIAL_UID").text( isEmpty(json.item.MATERIAL_UID)       == true ? '[조회불가]' : "["+json.item.MATERIAL_UID+"]");
			$("#"+name+"_MATERIAL_NAME").text(isEmpty(json.item.MATERIAL_NAME)      == true ? '[조회불가]' : "["+json.item.MATERIAL_NAME+"]");
			
			if(name == "popMaterialInputView") {
				Grids.MaterialInList.Source.Data.Url = "/epms/material/input/materialInputListData.do?MATERIAL=" + $('#MATERIAL').val()
				 + "&startDt=" + $('#popMaterialInputView_startDt').val()
				 + "&endDt=" + $('#popMaterialInputView_endDt').val();
				
				Grids.MaterialInList.ReloadBody();
			} else if(name == "popMaterialOutputView") {
				Grids.MaterialOutList.Source.Data.Url = "/epms/material/output/materialOutputListData.do?MATERIAL=" + $('#MATERIAL').val()
				 + "&startDt=" + $('#popMaterialOutputView_startDt').val()
				 + "&endDt=" + $('#popMaterialOutputView_endDt').val();
				 
				Grids.MaterialOutList.ReloadBody();
			}
			
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
			loadEnd();
		}
	});
	
	fnModalToggle(name);
	
}

</script>
</head>
<body>
<%-- 화면 UI Area Start --%>
<div id="contents">
	<input type="hidden" id="CATEGORY"      name="CATEGORY"/>
	<input type="hidden" id="CATEGORY_TYPE" name="CATEGORY_TYPE"/>
	<input type="hidden" id="CATEGORY_NAME" name="CATEGORY_NAME"/>
	<input type="hidden" id="TREE"          name="TREE"/>
	<input type="hidden" id="COMPANY_ID" name="COMPANY_ID"/>		<%-- 회사코드 --%>
	<input type="hidden" id="LOCATION" name="LOCATION"/>			<%-- 위치(공장ID) --%>
	<input type="hidden" id="MATERIAL" name="MATERIAL">				<%-- 자재 ID --%>
	
	<div class="fl-box panel-wrap03" style="width:20%">
		<h5 class="panel-tit">
		    <spring:message code='epms.object.type' />
		</h5>
		<div class="panel-body">
			<%-- 트리그리드[자재유형] : s --%>
			<div id="categoryList">
				<bdo	Debug="Error"
						Data_Url="/sys/category/categoryData.do?CATEGORY_TYPE=MATERIAL"
						Layout_Url="/sys/category/categoryNotEditLayout.do?CATEGORY_TYPE=MATERIAL"
						Upload_Url="/sys/category/categoryEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"
				>
				</bdo>
			</div>
			<%-- 트리그리드[자재유형] : e --%>
		</div>
	</div>
	
	<div class="fl-box panel-wrap03" style="width:80%;">
		<h5 class="panel-tit mgn-l-10" id="materialListTitle">
		    <spring:message code='epms.object' />  <!-- 자재 -->
		</h5>
		<div class="panel-body mgn-l-10">
			<%-- 트리그리드[자재] : s --%>
			<div id="materialMasterList">
				<bdo	Debug="Error"
						Data_Url=""
						Layout_Url="/epms/material/master/materialMasterListLayout.do"
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=자재목록.xls&dataName=data"
				>
				</bdo>
			</div>
			<%-- 트리그리드[자재] : e --%>
		</div>
	</div>
</div><%-- 화면 UI Area End --%>

<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<%-- 첨부파일 조회/수정 --%>
<div class="modal fade modalFocus" id="popFileManageForm"    data-backdrop="static" data-keyboard="false"></div>	

<%-- 입고조회 --%>
<%@ include file="/WEB-INF/jsp/epms/material/master/popMaterialInputView.jsp"%>	

<%-- 출고조회 --%>
<%@ include file="/WEB-INF/jsp/epms/material/master/popMaterialOutputView.jsp"%>	

<%-- 상세내역 조회 --%>
<div class="modal fade modalFocus" id="popMaterialMasterDtl" data-backdrop="static" data-keyboard="false"></div>	

<%-- 정비실적조회 --%>
<div class="modal fade modalFocus" id="popRepairResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 예방보전실적조회 --%>
<div class="modal fade modalFocus" id="popPreventiveResultForm" data-backdrop="static" data-keyboard="false"></div>
