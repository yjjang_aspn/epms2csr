<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	   uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: popMaterialMasterDtl.jsp
	Description : 자재관리 > 자재정보 조회/관리 > 자재 상세조회 레이어 팝업
    author		: 김영환
    since		: 2018.02.08
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.02.08	 김영환		최초 생성

--%>

<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	<%-- 첨부파일 관련 --%>
	for(var i=0; i<1; i++){	
		$('#attachFile').append('<input type="file" multiple id="fileList" name="fileList">');	
		
		var fileType = 'jpeg |gif |jpg |png |bmp |ppt |pptx |doc |hwp |pdf |xlsx |xls';
	
		$('#fileList').MultiFile({
			    accept :  fileType
			,	list   : '#detailFileList'+i
			,	max    : 5
			,	STRING : {
					remove: '<img src="/sys/images/common/btn_remove.png">'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
					text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" />',
					idx	  : i,
			        denied : "$ext 는(은) 업로드 할수 없는 파일확장자입니다."
				},
		});
		
		<%-- 첨부한 파일의 출력 순서를 자동으로 샛팅 하기 위한 스크립트 - 공통 스크립트로 빠지면 안됨 .개별 항목으로 처리 필수 --%> 
		$('.MultiFile-wrap').change(function(){
			var fileExpIndex = $(this).closest('.fileWrap').find('.fileList > div').children('div:last-child');
			var	fileExp = fileExpIndex.find('.fileExp');
			fileExp.val(fileExpIndex.index() + 1);
		});
	}
	
	<%-- 파일 다운로드 --%>
	$('.btnFileDwn').on('click', function(){
		var ATTACH = $(this).data("attach");
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO=${materialDtl.ATTACH_GRP_NO}&ATTACH='+ATTACH;
		$("#fileDownFrame").attr("src",src);
	});
	
	<%-- 안전재고가 0일경우, 재고 숨김 --%>
	if("${materialDtl.STOCK_OPTIMAL}" == "0"){
		$("#trSTOCK").hide();
	}
});

<%-- 이미지 원본크기 보기 --%>
function originFileView(img){ 
	img1= new Image(); 
	img1.src=(img); 
	imgControll(img); 
} 

function imgControll(img){ 
	if((img1.width!=0)&&(img1.height!=0)){ 
		viewImage(img); 
	}else{ 
		controller="imgControll('"+img+"')"; 
		intervalID=setTimeout(controller,20); 
	} 
}

function viewImage(img){ 
	W=img1.width; 
	H=img1.height; 
	O="width="+W+",height="+H+",scrollbars=yes"; 
	imgWin=window.open("","",O); 
	imgWin.document.write("<html><head><title>이미지상세보기</title></head>");
	imgWin.document.write("<body topmargin=0 leftmargin=0>");
	imgWin.document.write("<img src="+img+" onclick='self.close()' style='cursor:pointer;' title ='클릭하시면 창이 닫힙니다.'>");
	imgWin.document.close();
}

</script>

<div class="modal-dialog root wd-per-55">				
	<div class="modal-content">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title">
		   	    <spring:message code='epms.object' /> <spring:message code='epms.detail' />
		   	</h4><!-- 자재마스터 상세 -->
		</div>
		<div class="modal-body" >
			<div class="modal-bodyIn">
				<%-- 페이지 내용 : s --%>
				<form id="materialAddForm" name="materialAddForm"  method="post" enctype="multipart/form-data">	
					
					<input type="hidden" id="attachExistYn" name="attachExistYn" value=""/>		<%-- 첨부파일유무(Y/N) --%>
					<input type="hidden" id="MODULE" name="MODULE" value="20"/>					<%-- 모듈아이디(20:자재) --%>
					<input type="hidden" id="TREE" name="TREE"/>								<%-- 구조체 --%>
					
					<div class="tb-wrap">
						<table class="tb-st">
							<caption class="screen-out"><!-- 자재 상세정보 -->
							    <spring:message code='epms.product' /> <spring:message code='epms.detail_info' />
							</caption>
							<colgroup>
								<col width="30%" />
								<col width="*" />
							</colgroup>
							<tbody>	
<!-- 								<tr> -->
<!-- 									<th>위치</th> -->
<!-- 									<td> -->
<!-- 										<div class="inp-wrap readonly wd-per-100"> -->
<%-- 											<input type="hidden" class="inp-comm" title="위치" readonly="readonly" id="LOCATION" name="LOCATION" value="${materialDtl.LOCATION}" /> --%>
<%-- 											<input type="text" class="inp-comm" title="위치명" readonly="readonly" id="LOCATION_NAME" name="LOCATION_NAME" value="${materialDtl.LOCATION_NAME}"/> --%>
<!-- 										</div> -->
<!-- 									</td> -->
<!-- 								</tr>			 -->
								<tr>
									<th><spring:message code='epms.type' /></th><!-- 자재유형 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="hidden" class="inp-comm" title="자재유형" readonly="readonly" id="MATERIAL_TYPE" name="MATERIAL_TYPE" value="${materialDtl.MATERIAL_TYPE}" />
											<input type="text" class="inp-comm" title="자재유형" readonly="readonly" id="MATERIAL_TYPE_NAME" name="MATERIAL_TYPE_NAME" value="${materialDtl.MATERIAL_TYPE_NAME}"/>
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.code' /></th><!-- 자재코드 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="text" class="inp-comm" title="자재코드" readonly="readonly" id="MATERIAL_UID" name="MATERIAL_UID" value="<c:out value="${materialDtl.MATERIAL_UID}" escapeXml="true"/>"/>
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.name' /></th><!-- 자재명 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="text" class="inp-comm" title="자재명" readonly="readonly" id="MATERIAL_NAME" name="MATERIAL_NAME" value="<c:out value="${materialDtl.MATERIAL_NAME}" escapeXml="true"/>"/>
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.model' /></th><!-- 모델명 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="text" class="inp-comm" title="모델명" readonly="readonly" id="MODEL" name="MODEL" value="<c:out value="${materialDtl.MODEL}" escapeXml="true"/>"/>
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.spec' /></th><!-- 규격 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="text" class="inp-comm" title="규격" readonly="readonly" id="SPEC" name="SPEC" value="<c:out value="${materialDtl.SPEC}" escapeXml="true"/>"/>
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.country' /></th><!-- 제조국 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="hidden" class="inp-comm" title="제조국" readonly="readonly" id="COUNTRY" name="COUNTRY" value="${materialDtl.COUNTRY}"/>
											<input type="text" class="inp-comm" title="제조국" readonly="readonly" id="COUNTRY_NAME" name="COUNTRY_NAME" value="${materialDtl.COUNTRY_NAME}"/>
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.supplier' /></th><!-- 제조사 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="text" class="inp-comm" title="제조사" readonly="readonly" id="MANUFACTURER" name="MANUFACTURER" value="<c:out value="${materialDtl.MANUFACTURER}" escapeXml="true"/>"/>
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.unit' /></th><!-- 단위 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="hidden" class="inp-comm" title="단위" readonly="readonly" id="UNIT" name="UNIT" value="${materialDtl.UNIT}"/>
											<input type="text" class="inp-comm" title="단위" readonly="readonly" id="UNIT_NAME" name="UNIT_NAME" value="${materialDtl.UNIT_NAME}"/>
										</div>
									</td>
								</tr>
								<tr id="trSTOCK">
									<th><spring:message code='epms.stock' /></th><!-- 재고 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="text" class="inp-comm" title="현재고" readonly="readonly" id="STOCK" name="STOCK" value="<c:out value="${materialDtl.STOCK}" escapeXml="true"/>"/>
										</div>
									</td>
								</tr>
								<tr id="trSTOCK_OPTIMAL">
									<th><spring:message code='epms.safety.stock' /></th><!-- 안전재고 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="text" class="inp-comm" title="안전재고" readonly="readonly" id="STOCK_OPTIMAL" name="STOCK_OPTIMAL" value="<c:out value="${materialDtl.STOCK_OPTIMAL}" escapeXml="true"/>"/>
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.memo' /></th><!-- 메모 -->
									<td>
										<div class="txt-wrap readonly">
											<textarea cols="" rows="3" title="메모" readonly="readonly" id="MEMO" name="MEMO"><c:out value="${materialDtl.MEMO}" escapeXml="true"/></textarea>
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row"><spring:message code='epms.attachfile' /></th><!-- 첨부파일 -->
									<td>
										<c:choose>
											<c:when test="${fn:length(attachList)>0 }">
												<div id="previewList" class="wd-per-100">
													<div class="bxType01 fileWrap previewFileLst">
														<div>
															<div class="fileList type02">
																<div id="detailFileList0" class="fileDownLst">
																	<c:forEach var="item" items="${attachList}" varStatus="idx">
																		<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																			<span class="MultiFile-export">${item.SEQ_DSP}</span> 
																			<span class="MultiFile-title" title="File selected: ${item.NAME}">
																			${item.FILE_NAME} (${item.FILE_SIZE_TXT})
																			</span>
																			<a href="#none" class="btnFileDwn icon download" data-attach="${item.ATTACH}">다운로드</a>
																		</div>		
																	</c:forEach>
																</div>
															</div>
														</div>
													</div>
												</div>
		
												<div id="previewImg" class="wd-per-100">
													<div class="previewFile mgn-t-15">
														<h3 class="tit-cont preview" id="previewTitle">
														    <spring:message code='epms.equipment.IMG_PREVIEW' />
														</h3><!-- 이미지 미리보기 -->
														<ul>
															<c:forEach var="item" items="${attachList}" varStatus="idx">
																<c:if test = "${item.FORMAT eq 'jpg' || item.FORMAT eq 'png' || item.FORMAT eq 'jpeg' || item.FORMAT eq 'JPG' || item.FORMAT eq 'PNG'}">
																	<li><!-- 클릭하시면 원본크기로 보실 수 있습니다. -->
																		<img src="/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}" title="<spring:message code='epms.equipment.IMG_CLICK_ORG' />" onclick="originFileView('/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}')" />
																		<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																			<span class="MultiFile-title" title="File selected: ${item.NAME}">
																			<p>${item.FILE_NAME} (${item.FILE_SIZE_TXT})</p>
																			</span>
																			<a href="#none" class="btnFileDwn icon download" data-attach="${item.ATTACH}"><spring:message code='epms.download' /></a><!-- 다운로드 -->
																		</div>
																	</li>
																</c:if>
															</c:forEach>
														</ul>
													</div>
												</div>
											</c:when>
											<c:otherwise>
												<div class="f-l wd-per-100">
													<spring:message code='epms.equipment.NO_ATTACH_FILE' />  <!-- ※ 등록된 첨부파일이 없습니다. -->
												</div>
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					
				</form><%-- 페이지 내용 : e --%>
			</div> <%-- modal-bodyIn : e --%>
		</div> <%-- modal-body : e --%>
	</div> <%-- modal-content : e --%>
</div> <%-- modal : e --%>

<%-- 파일 다운로드 iframe --%>
<iframe id="fileDownFrame" style="width:0px;height:0px;border:0px"></iframe>
