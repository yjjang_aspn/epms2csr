<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
	Class Name	: materialMasterListData.jsp
	Description : 자재관리 > 자재정보 조회/관리 그리드 데이터
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	김영환		최초 생성
	2018.04.10	김영환		자재유형명 추가 (MATERIAL_TYPE_NAME)

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I		
			MATERIAL				= "${item.MATERIAL}"
			COMPANY_ID				= "${item.COMPANY_ID}"
			LOCATION				= "${item.LOCATION}"
			MATERIAL_TYPE			= "${item.MATERIAL_TYPE}"
			MATERIAL_TYPE_NAME		= "${item.MATERIAL_TYPE_NAME}"
			MATERIAL_UID			= "${item.MATERIAL_UID}"
			MATERIAL_NAME			= "${fn:replace(item.MATERIAL_NAME, '"', '&quot;')}"
			MODEL					= "${fn:replace(item.MODEL, '"', '&quot;')}"
			SPEC					= "${fn:replace(item.SPEC, '"', '&quot;')}"
			COUNTRY					= "${item.COUNTRY}"
			MANUFACTURER			= "${fn:replace(item.MANUFACTURER, '"', '&quot;')}"
			WAREHOUSE				= "${item.WAREHOUSE}"
			MEMO					= "${fn:replace(item.MEMO, '"', '&quot;')}"
			UNIT					= "${item.UNIT}"
			UNIT_NAME				= "${item.UNIT_NAME}"
			PASS_DATE				= "${item.PASS_DATE}"
			
			STOCK_OPTIMAL			= "${item.STOCK_OPTIMAL}"
			<c:choose>
				<c:when test="${item.STOCK_OPTIMAL > 0}">
					STOCK					= "${item.STOCK}"
					STOCK_CONDITION			= "${item.STOCK_CONDITION}"
					<c:if test="${item.STOCK_CONDITION == '1'}">
						STOCK_CONDITIONClass="gridStatus3"
					</c:if>
					<c:if test="${item.STOCK_CONDITION == '2'}">
						STOCKClass="gridStatus4"
						STOCK_CONDITIONClass="gridStatus4"
					</c:if>
				</c:when>
				<c:when test="${item.STOCK > 0}">
					STOCK					= "${item.STOCK}"
					STOCK_CONDITION			= "${item.STOCK_CONDITION}"
					<c:if test="${item.STOCK_CONDITION == '1'}">
						STOCK_CONDITIONClass="gridStatus3"
					</c:if>
					<c:if test="${item.STOCK_CONDITION == '2'}">
						STOCKClass="gridStatus4"
						STOCK_CONDITIONClass="gridStatus4"
					</c:if>
				</c:when>
				<c:otherwise>
					STOCK					= ""
					STOCK_CONDITION			= ""
				</c:otherwise>
			</c:choose>
			
			ATTACH_GRP_NO			= "${item.ATTACH_GRP_NO}"
			
			<c:choose>
				<c:when test = '${item.FILE_CNT > 0 && editYn eq "Y"}'>
					FILESwitch="1"  FILEIcon="/images/com/web/modification_icon.gif" 
					FILEOnClickSideIcon="fnOpenLayerWithParam('/attach/popFileManageForm.do?MATERIAL='+Row.MATERIAL+'&ATTACH_GRP_NO='+Row.ATTACH_GRP_NO+'&editYn='+'${editYn}'+'&flag=material','popFileManageForm');"
				</c:when>
				<c:when test = '${item.FILE_CNT eq "0" && editYn eq "Y"}'>
					FILESwitch="1"  FILEIcon="/images/com/web/upload_icon.gif" 
					FILEOnClickSideIcon="fnOpenLayerWithParam('/attach/popFileManageForm.do?MATERIAL='+Row.MATERIAL+'&ATTACH_GRP_NO='+Row.ATTACH_GRP_NO+'&editYn='+'${editYn}'+'&flag=material','popFileManageForm');"
				</c:when>
				<c:when test = '${item.FILE_CNT>0 && editYn eq "N"}'>
					FILESwitch="1"  FILEIcon="/images/com/web/commnet_up3.gif" 
					FILEOnClickSideIcon="fnOpenLayerWithParam('/attach/popFileManageForm.do?MATERIAL='+Row.MATERIAL+'&ATTACH_GRP_NO='+Row.ATTACH_GRP_NO+'&editYn='+'${editYn}'+'&flag=material','popFileManageForm');"
				</c:when>
			</c:choose>
			
			HISTORY_INSwitch="1"  HISTORY_INIcon="/images/com/web/receiving_icon.gif" 
			HISTORY_INOnClickSideIcon="fnOpenLayerWithGrid('/epms/material/master/popMaterialInOutView.do?MATERIAL='+Row.MATERIAL,'popMaterialInputView');"
			HISTORY_OUTSwitch="1"  HISTORY_OUTIcon="/images/com/web/shipping_icon.gif" 
			HISTORY_OUTOnClickSideIcon="fnOpenLayerWithGrid('/epms/material/master/popMaterialInOutView.do?MATERIAL='+Row.MATERIAL,'popMaterialOutputView');"
			
			DETAILSwitch="1"  DETAILIcon="/images/com/web/commnet_up.gif" 
			DETAILOnClickSideIcon="fnOpenLayerWithParam('/epms/material/master/popMaterialMasterDtl.do?MATERIAL='+Row.MATERIAL,'popMaterialMasterDtl');"
			
			DEL_YN					= "${item.DEL_YN}"
			<c:if test='${item.DEL_YN eq "N"}'>
				DEL_YNClass="gridStatus3"
			</c:if>
			<c:if test='${item.DEL_YN eq "Y"}'>
				DEL_YNClass="gridStatus4"
			</c:if>
			
			DASHBOARD_OPT1				= "${item.DASHBOARD_OPT1}"
			DASHBOARD_OPT2				= "${item.DASHBOARD_OPT2}"
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>