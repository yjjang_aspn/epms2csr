<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
	Class Name	: popMaterialImgView.jsp
	Description : 자재관리 > 자재정보 조회/관리 > 첨부파일 조회/수정
	author		: 김영환
	since		: 2018.04.04
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.04	 김영환		최초 생성

--%>
<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">

var fileVal = "";		<%-- 삭제할 파일 idx 저장 변수. 레이어팝업 호출 시 초기화 --%>
var maxFileCnt = 30;	<%-- MultiFile 첨부 가능갯수 --%>

$(document).ready(function(){
	<%-- 수정가능 Y가 아닐경우 편집버튼 숨김 --%>
	if("${param.editYn}" == 'Y') {
		$("#modifyBtn").show();
	}
	
	<%-- 파일 다운로드 --%>
	$('.btnFileDwn').on('click', function(){
		var ATTACH = $(this).data("attach");
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO=${param.ATTACH_GRP_NO}&ATTACH='+ATTACH;
		$("#fileDownFrame2").attr("src",src);
	});
	
	var fileList = new Array();
	<c:forEach items="${fileList}" var="item" >
		var obj = new Object();
		
		obj.ATTACH_GRP_NO = "${item.ATTACH_GRP_NO}";
		obj.ATTACH = "${item.ATTACH}";
		obj.FORMAT = "${item.FORMAT}";
		
		fileList.push(obj);
	</c:forEach>
	var attachImgCnt = 0;
	for(var i=0; i<fileList.length; i++){
		if(fileList[i].FORMAT == "jpg" || fileList[i].FORMAT == "png" || fileList[i].FORMAT == "jpeg" || fileList[i].FORMAT == "JPG" || fileList[i].FORMAT == "PNG"){
			attachImgCnt ++;
		}
	}	
	
	if(attachImgCnt == 0){
		$('#previewTitle').text('첨부된 이미지 파일이 존재하지 않습니다.');
	}
	
	<%-- 등록폼 등록버튼 --%>
	$('#regBtn').on('click', function(){
		var fileModifyCnt = $("input[name=fileGb]").length;
		
		if(fileModifyCnt > 0){
			if ($('#attachExistYn').val() == "Y"){	
				$('#attachExistYn').val("YY");		<%-- ATTACH_GRP_NO가 기존에 존재하였으며, 수정후에도 이미지가 등록되는 경우 --%>	
			}else{		
				$('#attachExistYn').val("NY");		<%-- ATTACH_GRP_NO가 기존에 존재하지 않았으며, 수정후에는 이미지가 등록되는 경우 --%>
			}
		}else{
			if ($('#attachExistYn').val() == "Y"){	
				$('#attachExistYn').val("YN");		<%-- ATTACH_GRP_NO가 기존에 존재하였으며, 수정후에는 이미지가 등록되지 않는 경우 --%>
			}else{									
				$('#attachExistYn').val("NN");		<%-- ATTACH_GRP_NO가 기존에 존재하지 않았으며, 수정후에도 이미지가 등록되지 않는 경우 --%>
			}
		}
		
		<%-- 이미지 전송/자재마스터 수정 --%>
		var form = new FormData(document.getElementById('imgForm'));
		
		$.ajax({
			type : 'POST',
			url : '/epms/material/master/popImgModifyUpdate.do',
			dataType : 'json',
			data : form,
			async : false,
			processData : false,
			contentType : false,
			success : function(json) {	
				if(json > 0){
					fnReload2("수정되었습니다");
					fnModalToggle($("#popMaterialImgView"));
				}else{
					alert("처리도중 에러가 발생하였습니다 . 시스템 관리자에게 문의 하시기 바랍니다");
					return;
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
			}
		});
		
	});
	
	<%-- 수정버튼 (등록폼 변환) --%>
	$('#modifyBtn').on('click', function(){
		$('#fileFrm').hide();
		$('#modifyBtn').hide();
		$('.regFrame').show();
	});
	
	<%-- 등록폼 취소버튼(이미지화면 변환) --%>
	$('#cancelBtn').on('click', function(){
		$('#fileFrm').show();
		$('#modifyBtn').show();
		$('.regFrame').hide();
	});
	
	<%-- 첨부 파일 삭제버튼 --%>
	$('.icn_fileDel').on('click', function(){
		delModiFile($(this));
	});
	
	if($('#ATTACH_GRP_NO').val() != null && $('#ATTACH_GRP_NO').val() != ""){
		$('#attachExistYn').val("Y");
	}else{
		$('#attachExistYn').val("N");
	}
	
	for(var i=0; i<1; i++){	
		$('#attachFile').append('<input type="file" id="fileList" name="fileList">');	
	
		var fileType = 'jpeg |gif |jpg |png |bmp |ppt |pptx |doc |hwp |pdf |xlsx |xls |txt |dxf |dwg';
		$('#fileList').MultiFile({
				accept : fileType
			,	list   : '#detailFileRegList'+i
			,	max    : maxFileCnt
			,	STRING : {
				remove: '<img src="/images/com/web/btn_remove.png">'
					   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
				text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" value="'+i+'" />',
				denied:'$ext는(은) 업로드할 수 없는 파일 확장자입니다.'
			},
		});
		
		<%-- 첨부한 파일의 출력 순서를 자동으로 샛팅 하기 위한 스크립트 - 공통 스크립트로 빠지면 안됨 .개별 항목으로 처리 필수 --%>
		$('.MultiFile-wrap').change(function(){
			var fileExpIndex = $(this).closest('.fileWrap').find('.fileList > div').children('div:last-child');
			var	fileExp = fileExpIndex.find('.fileExp');
			fileExp.val(fileExpIndex.index() + 1);
		});
	}
	
});

<%-- 첨부파일 목록에서 제거 --%>
function delModiFile(obj){
	var attach = obj.data("attach");
	var idx = obj.data('idx');
	
	$('#fileRegLstWrap_'+attach).remove();
	
	if(fileVal != '') {
		fileVal += ",";
	}
	
	fileVal += attach;
	$('#DEL_SEQ').val(fileVal);
}

<%-- 이미지 원본크기 보기 --%>
function originFileView(img){ 
	img1= new Image(); 
	img1.src=(img); 
	imgControll(img); 
} 
	  
function imgControll(img){ 
	if((img1.width!=0)&&(img1.height!=0)){ 
		viewImage(img); 
	}else{ 
		controller="imgControll('"+img+"')"; 
		intervalID=setTimeout(controller,20); 
	} 
}


function viewImage(img){ 
	W=img1.width; 
	H=img1.height; 
	O="width="+W+",height="+H+",scrollbars=yes"; 
	imgWin=window.open("","",O); 
	imgWin.document.write("<html><head><title>이미지상세보기</title></head>");
	imgWin.document.write("<body topmargin=0 leftmargin=0>");
	imgWin.document.write("<img src="+img+" onclick='self.close()' style='cursor:pointer;' title ='클릭하시면 창이 닫힙니다.'>");
	imgWin.document.close();
}

</script>

<div class="modal-dialog root wd-per-40">
	<div class="modal-content" style="height: 500px;">
		<div class="modal-header">
			<button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			<h4 class="modal-title">첨부파일 다운로드</h4>
		</div>
		<div class="modal-body" >
			<div class="modal-bodyIn" >
				<a href="#none" class="btn comm st02" id="modifyBtn" style="display:none;">편집</a> 

				<%-- 첨부파일 리스트/미리보기 : s --%>
				<form id="fileFrm" name="fileFrm" method="post" enctype="multipart/form-data">
					<div id="previewList" class="wd-per-100">
						<div class="bxType01 fileWrap previewFileLst">
							<div>
								<div class="fileList type02">
									<div id="detailFileList0" class="fileDownLst">
										<c:forEach var="item" items="${fileList}" varStatus="idx">
											<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
												<span class="MultiFile-export">${item.SEQ_DSP}</span> 
												<span class="MultiFile-title" title="File selected: ${item.NAME}">
												${item.FILE_NAME} (${item.FILE_SIZE_TXT})
												</span>
												<a href="#none" class="btnFileDwn icon download" data-attach="${item.ATTACH}">다운로드</a>
											</div>		
										</c:forEach>
									</div>
								</div>
							</div>
						</div>
					</div>
		
					<div id="previewImg" class="wd-per-100">
						<div class="previewFile mgn-t-15">
							<h3 class="tit-cont preview" id="previewTitle">이미지 미리보기</h3>
							<ul>
								<c:forEach var="item" items="${fileList}" varStatus="idx">
									<c:if test = "${item.FORMAT eq 'jpg' || item.FORMAT eq 'png' || item.FORMAT eq 'jpeg' || item.FORMAT eq 'JPG' || item.FORMAT eq 'PNG'}">
										<li>
											<img src="/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}" title="클릭하시면 원본크기로 보실 수 있습니다." onclick="originFileView('/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}')" />
											<p>${item.NAME}</p>
										</li>
									</c:if>
								</c:forEach>
							</ul>
						</div>
					</div>
					<%-- 파일 다운로드 iframe --%>
					<iframe id="fileDownFrame2" style="width:0px;height:0px;border:0px"></iframe>
				</form><%-- 첨부파일 리스트/미리보기 : e --%>
				
				<%-- 첨부파일 편집 : s --%>
				<div class="regFrame" style="display:none;">
					<form id="imgForm" name="imgForm" method="post" enctype="multipart/form-data">
						<input type="hidden" id="EQUIPMENT" name="MATERIAL" value="${param.MATERIAL}"/>					 <%-- 자재번호 --%>
						<input type="hidden" id="ATTACH_GRP_NO" name="ATTACH_GRP_NO" value="${param.ATTACH_GRP_NO}"/>	 <%-- 파일그룹번호 --%>
						<input type="hidden" id="attachExistYn" name="attachExistYn" value=""/>							 <%-- 파일 수정사항 체크(Y/N) --%>
						<input type="hidden" id="MODULE" name="MODULE" value="20"/>										 <%-- 모듈아이디(20:자재) --%>
						<input type="hidden" id="DEL_SEQ" name="DEL_SEQ" value="">										 <%-- 삭제할 파일 idx 리스트 --%>
						
						<div class="inq-area-top" style="width:100%">
							<div class="bxType01 fileWrap previewFileLst">
								<div id="attachFile" class="attachFile"></div>
								<div class="fileList">
									<div id="detailFileRegList0" class="fileDownLst">
										<c:forEach var="item" items="${fileList}" varStatus="idx">
											<div class="MultiFile-label" id="fileRegLstWrap_${item.ATTACH}">
												<span class="MultiFile-remove">
													<a href="#none" class="icn_fileDel" data-attach="${item.ATTACH}" data-idx="${item.CD_FILE_TYPE }">
														<img src="/images/com/web/btn_remove.png" />
													</a>
												</span> 
												<span class="MultiFile-title" title="File selected: ${item.NAME}">
													${item.FILE_NAME} (${item.FILE_SIZE_TXT})
												</span>
											</div>
										</c:forEach>
									</div>
								</div>
							</div>
						</div>
						
					</form>
					<a href="#none" class="btn comm st01" id="cancelBtn">취소</a>
					<a href="#none" class="btn comm st02" id="regBtn">등록</a> 
				</div><%-- 첨부파일 편집 : e --%>
			</div> <%-- modal-bodyIn : e --%>
		</div> <%-- modal-body : e --%>
	</div> <%-- modal-content : e --%>
</div> <%-- modal : e --%>
