<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
	Class Name	: popMaterialOrderDtlLayout.jsp
	Description : 자재관리 > 발주등록 > 내 발주등록 현황 > 상세조회 그리드 데이터
	author		: 김영환
	since		: 2018.04.04
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.04	 김영환		최초 생성

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I	
			MATERIAL_INOUT_ITEM		= "${item.MATERIAL_INOUT_ITEM}"
			MATERIAL				= "${item.MATERIAL}"
			MATERIAL_UID			= "${item.MATERIAL_UID}"	
			LOCATION				= "${item.LOCATION}"
			LOCATION_NAME			= "${item.LOCATION_NAME}"
			MATERIAL_TYPE			= "${item.MATERIAL_TYPE}"
			MATERIAL_TYPE_NAME		= "${item.MATERIAL_TYPE_NAME}"
			MATERIAL_NAME			= "${item.MATERIAL_NAME}"
			MODEL					= "${item.MODEL}"
			SPEC					= "${item.SPEC}"
			UNIT					= "${item.UNIT}"
			UNIT_NAME				= "${item.UNIT_NAME}"
			QNTY_PLAN				= "${item.QNTY_PLAN}"
			PRICE					= "${item.PRICE}"
			QNTY					= "${item.QNTY}"
			DATE_INOUT				= "${item.DATE_INOUT}"
			<c:choose>
				<c:when test="${item.QNTY > '0'}">
					DATE_INOUTCanEdit = "1"
				</c:when>
				<c:otherwise>
					DATE_INOUTCanEdit = "0"
				</c:otherwise>
			</c:choose>
			
			<c:choose>
				<c:when test="${item.QNTY_PLAN eq item.QNTY}">
				QNTYClass="gridStatus3"
				</c:when>
				<c:otherwise>
				QNTYClass="gridStatus4"
				</c:otherwise>
			</c:choose>
			MEMO					= "${item.MEMO}"
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>