<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: popMaterialOrderDtlForm.jsp
	Description : 자재관리 > 발주등록 > 내 발주등록 현황 > 상세조회
    author		: 김영환
    since		: 2018.03.21
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자	수정내용
	----------  ------	---------------------------
	2018.03.21	김영환	최초생성

--%>

<script type="text/javascript">	
$(document).ready(function(){
	
});

//row 값 계산
Grids.OnReady = function(grid){
	  var rowCnt = 0;
	  var rows = grid.Rows;
	  for(var key in rows){
	    if(key == "Header" || key == "NoData" || key == "Toolbar"){
	      continue;
	    } 
	    rowCnt ++;
	  }
	  gridDivResize(rowCnt);
}

//그리드 영역 사이즈 resizing
function gridDivResize(dataSize){

	if(dataSize <= 1){
	  dataSize = 4;
	}
	
	var divSize = dataSize * 10;  
	
	divSize += 160;
	
	if(divSize < 160){
	  divSize = 160;
	}
	
	$(".gridDivResize").height(divSize);
	$('.gridDivResize').css('max-height','500px')
}
</script>

<div class="modal fade modalFocus" id="popMaterialOrderDtlForm"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-70"> <!-- 원하는 width 값 입력 -->				
		 <div class="modal-content hgt-per-80" style="min-height:540px;">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">발주 상세</h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	           	

							<div class="tb-wrap">
								<table class="tb-st">
									<colgroup>
										<col width="15%" />
										<col width="35%" />
										<col width="15%" />
										<col width="*" />
									</colgroup>
									<tbody>
										<tr>
											<th scope="row">발주일</th>
											<td><h4 class="tit-inq" id="popMaterialOrderDtlForm_DATE_INOUT"></h4></td>
											<th scope="row">상태</th>
											<td><h4 class="tit-inq" id="popMaterialOrderDtlForm_STATUS"></h4></td>
										</tr>
										<tr>
											<th scope="row">제목</th>
											<td colspan="3"><h4 class="tit-inq" id="popMaterialOrderDtlForm_TITLE"></h4></td>
										</tr>
									</tbody>
								</table>
							</div>	
							<div id="MaterialOrderDtl" class="gridDivResize">	
								<bdo	Debug="Error"
										Data_Url=""
										Layout_Url="/epms/material/order/popMaterialOrderDtlLayout.do?STATUS=01"
										Upload_Url="/epms/material/order/popMaterialOrderDtlEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"
										Export_Data="data" Export_Type="xls"
										Export_Url="/sys/comm/exportGridData.jsp?File=발주상세.xls&dataName=data"
								>
								</bdo>
							</div>
							

				</div>
			</div>
		</div>
	</div>
</div>
