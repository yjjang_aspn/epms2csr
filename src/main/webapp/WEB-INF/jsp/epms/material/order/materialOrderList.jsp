<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: materialInoutList.jsp
	Description : 자재관리 > 발주 등록 메인화면
	author		: 김영환
	since		: 2018.04.04
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.04	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script type="text/javascript">

var flag = "";						<%-- 삭제 구분 (1:수정, 2:삭제)--%>
var focusedRow = null;				<%-- [자재유형] 선택된 ROW --%>
var focusedRow2 = null;				<%-- [자재목록] 선택된 ROW --%>
var focusedRow3 = null;				<%-- [발주목록] 선택된 ROW --%>
var focusedRow4 = null;				<%-- [내 발주현황] 선택된 ROW --%>
var focusedRow5 = null;				<%-- [팝업:발주상세] 선택된 ROW --%>

var orderItemArray = new Array();	<%-- [발주목록] 발주 아이템 array --%>

$(document).ready(function(){
	
	<%-- datePic 설정 --%>
	$('.datePic').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	});
	
	$(window).resize(function(){	
		<%-- 하단 그리드 높이값 --%>
		var bottomH = $(".bottom-contents").outerHeight();
		$(".bottom-contents").find(".panel-body").height(bottomH-13);
	});
	$(window).trigger('resize');
	
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function (e) {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>
	});
	
	<%-- 검색 시작일이 종료일보다 이후인지 체크 (이후라면, 종료일을 시작일로 변경) --%>
	$('#startDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#endDt').val($('#startDt').val());
		}
	});
	
	<%-- 검색 종료일이 시작일보다 이전인지 체크 (이전이라면, 시작일을 종료일로 변경) --%>
	$('#endDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#startDt').val($('#endDt').val());
		}
	});
	
	<%-- 발주등록 현황 기간검색  --%>
	$('#srcBtn').on('click', function(){
		
		var startDt = $("#startDt").val().replaceAll('-', '');
		var endDt = $("#endDt").val().replaceAll('-', '');
		
 		Grids.MaterialOrderList.Source.Data.Url = "/epms/material/order/materialOrderListData.do?startDt=" + $('#startDt').val()
 											    + "&endDt=" + $('#endDt').val();
 		Grids.MaterialOrderList.ReloadBody();
	});
	
});


<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "CategoryList") {		<%-- 1. [자재유형] 클릭시 --%>
		
		if (focusedRow == null || focusedRow.CATEGORY != row.CATEGORY){
			
			<%-- 1-1. 포커스 --%>
			if(focusedRow != null){
				grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
			}
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			
			focusedRow = row;
			
			<%-- 1-2. 자재 목록 조회 --%>
			$('#CATEGORY').val(row.CATEGORY);			<%-- 카테고리 번호 --%>
			$('#CATEGORY_TYPE').val(row.CATEGORY_TYPE);	<%-- 카테고리 유형 : 'MATERIAL' --%>
			$('#CATEGORY_NAME').val(row.CATEGORY_NAME);	<%-- 카테고리 명 : 자재유형 --%>
			$('#TREE').val(row.TREE);					<%-- 구조체 --%>
			$('#materialListTitle').html("자재 : " + "[ " + row.CATEGORY_NAME + " ]");
			fnGetMaterialList();
		}
		
	} else if(grid.id == "MaterialList2") {			<%-- 2. [자재] 클릭시 --%>
		
		<%-- 2-1. 포커스 --%>
		if(focusedRow2 != null){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow2 = row;
	
	
	} else if(grid.id == "MaterialInoutRegList") {	<%-- 3. [발주 내역] 클릭시 --%>
		
		<%-- 3-1. 포커스 --%>
		if(focusedRow3 != null){
			grid.SetAttribute(focusedRow3,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow3 = row;
	
	} else if(grid.id == "MaterialOrderList") {		<%-- 4. [내 발주등록 현황] 클릭시 --%>
		
		<%-- 4-1. 포커스 --%>
		if(focusedRow4 != null){
			grid.SetAttribute(focusedRow4,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow4 = row;
		
	} else if(grid.id == "MaterialOrderDtl") {		<%-- 5. [팝업 : 발주 상세] 클릭시 --%>
	
		<%-- 5-1. 포커스 --%>
		if(focusedRow5 != null){
			grid.SetAttribute(focusedRow5,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow5 = row;
	}
}

<%-- 선택된 라인에 해당되는 설비 조회 --%>
function fnGetMaterialList(){
	Grids.MaterialList2.Source.Data.Url = "/epms/material/master/materialMasterListData.do?TREE=" + $('#TREE').val()
											 + "&CATEGORY=" + $('#CATEGORY').val()
											 + "&CATEGORY_TYPE=" + $('#CATEGORY_TYPE').val()
											 + "&editYn=N";
	Grids.MaterialList2.ReloadBody();	
}

<%-- 그리드 더블클릭 이벤트 --%>
Grids.OnDblClick = function(grid, row, col){
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	<%-- 자재 선택 --%>
	if(grid.id == "MaterialList2") {
		grid.SetValue(row, "flag", '1', 1);
		
		if(grid.ActionValidate()){
			grid.ActionSave();
		}
	} else if(grid.id == "MaterialOrderList"){
		fnOpenLayerWithGrid(row, 'popMaterialOrderDtlForm');
	}
}

<%-- 담기 버튼 --%>
function fnAddMaterial(gridNm){
	
	var selRows = Grids[gridNm].GetSelRows();
	if(selRows == 0) {
		alert("선택된 자재가 없습니다. 확인후 다시 시도하세요.");
		return false;
	}

	for(var i=0; i <selRows.length;i++){
		Grids[gridNm].SetValue(selRows[i], "flag", '1', 1);
	}
	
	if(Grids[gridNm].ActionValidate()){

		if(confirm("자재를 입출고 양식에 추가하시겠습니까?")){
			Grids[gridNm].ActionSave();

		}else{
			Grids[gridNm].ReloadBody();
		}
	}
	
}

<%-- 삭제 버튼 --%>
function fnDel(gridNm){
	
	var selRows = Grids[gridNm].GetSelRows();

	for (var i = 0; i < selRows.length; i++) {
		Grids[gridNm].SetValue(selRows[i], "flag", 'del', 1);
	}
	
	if(Grids[gridNm].ActionValidate()){
		
		<%-- Upload_Url 변경 --%>
		flag = "2";		<%-- 1:등록, 2삭제 --%>
		Grids[gridNm].Source.Upload.Url = "/epms/material/inout/materialInoutMngEdit.do?flag=" + flag;
		
		Grids[gridNm].ActionSave();

	}
}


<%-- 등록 버튼 --%>
function fnReg(gridNm){
	
	focusedRow3 = Grids[gridNm].GetSelRows();
	
	if(focusedRow3.length < 1){
		
		alert("선택된 자재가 없습니다. 확인후 다시 시도하세요.");
		
	} else {
		<%-- 발주등록 레이어팝업 내의 발주목록 초기화 --%>
		$(".orderList").empty();
		orderItemArray = new Array();
		var tableInfo = "";
		
		for(var i = 0; i< focusedRow3.length; i++){
			
			<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
			Grids[gridNm].EndEdit(focusedRow3[i]);
			
			<%-- 금액 입력이 0일 경우 --%>
			if(focusedRow3[i].PRICE == '0' || focusedRow3[i].PRICE == undefined){
				focusedRow3[i].PRICE = '0';
			}
			
			<%-- 수량 입력이 0일 경우 --%>
			if(focusedRow3[i].QNTY == '0' || focusedRow3[i].QNTY == undefined){
				alert("[" + focusedRow3[i].MATERIAL_NAME + "] 자재의 수량을 입력해주세요");
				return false;
				
			<%-- 발주등록 목록 생성 --%>
			} else {
				tableInfo += "<tr class='orderList'>"
						   + 	"<td>" + focusedRow3[i].MATERIAL_NAME + "</td>"
						   + 	"<td>" + focusedRow3[i].QNTY + "</td>"
						   + 	"<td>" + oneLimitCount(focusedRow3[i].PRICE) + "</td>"
				     	   + "<tr>";
				     	   
				var orderItem = new Object();
				orderItem.MATERIAL = focusedRow3[i].MATERIAL;
				orderItem.MATERIAL_INOUT_TEMP = focusedRow3[i].MATERIAL_INOUT_TEMP;
				orderItem.QNTY_PLAN = focusedRow3[i].QNTY;
				orderItem.PRICE	= focusedRow3[i].PRICE;
				
				orderItemArray.push(orderItem);
			}
		}
		
		$("#orderInfo").append(tableInfo);
		fnModalToggle('popMaterialOrderRegForm');
	}
}

<%-- 발주 등록 후 처리 --%>
function fnRegAfterAction(){
	
	fnModalToggle('popMaterialOrderRegForm');
	Grids.MaterialInoutRegList.ReloadBody();
	Grids.MaterialOrderList.ReloadBody();
	
}

<%-- Validation --%>
function dataValidation(gridNm){
	var cnt = 0;
	var chkCnt = 0;
	var grid = Grids[gridNm];
	
	if(gridNm == "MaterialOrderDtl"){
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){
			
			<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
			Grids[gridNm].EndEdit(row);
			
			if(row.Changed || row.Added){
				
				cnt++;

				<%-- 수량이에 따른 날짜 check --%>
				var date_inout = row.DATE_INOUT;
				var qnty = row.QNTY + '';
				
				if(qnty != '0'){
					if(date_inout == null || date_inout == ''){
						chkCnt++;
						grid.SetAttribute(row,"DATE_INOUT","Color","#FF6969",1);
					}else {
						grid.SetAttribute(row,"DATE_INOUT","Color","#FFFFAA",1);
					}
				}else {
					grid.SetAttribute(row,"DATE_INOUT","Color","#FFFFAA",1);
				}
				
				
			}
			
		}
	}

	<%-- 추가 및 변경된 데이터가 없을 경우 --%>
	if(cnt == 0){
		return false;
	}else{
		
		<%-- 입고일 체크 --%>
		if(chkCnt == 0){ <%-- 입고일 변경이 있을때만 저장--%>
			return true;
		}
		else{
			alert('입고일을 입력해주세요.');
			return false;
		}
	}
	
}

<%-- [저장, 완료] 버튼 : 발주 상세 수량 변경 --%>
function fnSave(gridNm, flag){
	
    if(Grids[gridNm].ActionValidate()){
		if(gridNm == "MaterialOrderDtl"){
			// 임시저장
			if(flag == 'save'){
				
				<%-- 유효성 체크 --%>
			    if(!dataValidation(gridNm)){
			    	return;
			    }
				
				if(confirm("저장하시겠습니까?")){
					Grids.MaterialOrderDtl.Source.Upload.Url = "/epms/material/order/popMaterialOrderDtlEdit.do?flag=" + flag;
					Grids[gridNm].ActionSave();
					fnModalToggle('popMaterialOrderDtlForm');
				}
				
			// 입고완료
			} else {
				if(confirm("입고완료 처리하시겠습니까?")){
					Grids.MaterialOrderDtl.Source.Upload.Url = "/epms/material/order/popMaterialOrderDtlEdit.do?flag=" + flag				
															 + "&MATERIAL_INOUT=" + $("#MATERIAL_INOUT").val();
					Grids[gridNm].ActionSave();
					fnModalToggle('popMaterialOrderDtlForm');
				}
			}
		}
    }
	
}

<%-- 그리드와 Param 값을 동시에 사용 --%>
function fnOpenLayerWithGrid(row, name){
	
	$("#popMaterialOrderDtlForm_DATE_INOUT").text(Grids.MaterialOrderList.GetString(row, "DATE_INOUT"));
	$("#popMaterialOrderDtlForm_TITLE").text(row.TITLE);
	$("#MATERIAL_INOUT").val(row.MATERIAL_INOUT);
	
	if(row.STATUS == '01'){
		$("#popMaterialOrderDtlForm_STATUS").text('발주');
		Grids.MaterialOrderDtl.Source.Layout.Url = "/epms/material/order/popMaterialOrderDtlLayout.do?STATUS=01";
		Grids.MaterialOrderDtl.Source.Data.Url = "/epms/material/order/popMaterialOrderDtlData.do?MATERIAL_INOUT=" + row.MATERIAL_INOUT;
		Grids.MaterialOrderDtl.Reload();
	} else {
		$("#popMaterialOrderDtlForm_STATUS").text('입고완료');
		Grids.MaterialOrderDtl.Source.Layout.Url = "/epms/material/order/popMaterialOrderDtlLayout.do?STATUS=02";
		Grids.MaterialOrderDtl.Source.Data.Url = "/epms/material/order/popMaterialOrderDtlData.do?MATERIAL_INOUT=" + row.MATERIAL_INOUT;
		Grids.MaterialOrderDtl.Reload();
	}
			
	fnModalToggle(name);
}

<%-- 발주 수량 또는 단가 수정 시 해당 ROW 자동 선택 --%>
function changeSts(grid,row,col){
	
	<%-- 수량 마이너스 체크 --%>
	var temp_qnty = row.QNTY + '';
	var qnty = temp_qnty.replace("-","");
	
	if(qnty != "undefined" && qnty != "0") grid.SetValue(row, "QNTY", qnty, 1);
	
	<%-- 단가 마이너스 체크 --%>
	var temp_price = row.PRICE + '';
	var price = temp_price.replace("-","");
	
	if(price != "undefined" && price != "0") grid.SetValue(row, "PRICE", price, 1);
	
	<%-- 이미 선택된 ROW가 아니면 데이터 수정 시 ROW 선택 --%>
	if(row.Selected != true){
		Grids['MaterialInoutRegList'].SelectRow(row)
	}
}

<%-- 발주 수량 또는 단가 수정 시 해당 ROW 자동 선택 --%>
function changeSts2(grid,row,col){
	
	<%-- 수량 마이너스 체크 --%>
	var temp_qnty2 = row.QNTY + '';
	var qnty2 = temp_qnty2.replace("-","");
	var qnty_plan  = row.QNTY_PLAN + '';
	
	<%-- 수량이에 따른 날짜 check --%>
	var date_inout = row.DATE_INOUT;

	<%-- 발주수량과 입고수량 같으면 --%>
	if(qnty2==qnty_plan){
		grid.SetAttribute(row,"QNTY","Class","gridStatus3",1);
	}else {
		grid.SetAttribute(row,"QNTY","Class","gridStatus4",1);
	}
	
	if(qnty2 != "undefined" && qnty2 != "0") {
		
		var tem_today = new Date();
		var today = tem_today.toISOString().substring(0, 10);
		
		grid.SetValue(row, "QNTY", qnty2, 1);
		
		if(date_inout == '' || date_inout == null){
			grid.SetValue(row, "DATE_INOUT", today, 1);
		}
		
		grid.SetAttribute(row,"DATE_INOUT","CanEdit","1",1);

	}else {
		grid.SetValue(row, "DATE_INOUT", null, 1);							
		grid.SetAttribute(row,"DATE_INOUT","CanEdit","0",1);				
	}
	
	<%-- 이미 선택된 ROW가 아니면 데이터 수정 시 ROW 선택 --%>
	if(row.Selected != true){
		Grids['MaterialOrderDtl'].SelectRow(row)
	}
}



<%-- 그리드 ActionSave 후 처리 --%>
Grids.OnAfterSave  = function (grid){
	
	if(grid.id == "MaterialList2"){

		Grids.MaterialList2.ReloadBody();
		Grids.MaterialInoutRegList.ReloadBody();
		
	}else if (grid.id == "MaterialInoutRegList"){
		
		if(Grids.MaterialList2.Source.Data.Url != ""){
			Grids.MaterialList2.ReloadBody();
		}
		Grids.MaterialInoutRegList.ReloadBody();
		
	}else if (grid.id == "MaterialOrderDtl"){
		if(Grids.MaterialList2.Source.Data.Url != ""){
			Grids.MaterialList2.ReloadBody();
		}
		Grids.MaterialOrderList.ReloadBody();
	}
};

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source,data, func){
	if(grid.id == "MaterialList2"){		<%-- [자재] 목록 조회시 --%>
		if(data != ""){
			callLoadingBar();
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "MaterialList2"){
					setTimeout($.unblockUI, 100);
				}
			}
		}
	}
	else if(grid.id == "MaterialOrderList"){		<%-- [발주등록현황] 목록 조회시 --%>
	if(data != ""){
		callLoadingBar();
		Grids.OnDataReceive  = function(grid, source){
			if(grid.id == "MaterialOrderList"){
				setTimeout($.unblockUI, 100);
			}
		}
	}
}
}


</script>
</head>
<body>
<div id="contents">
	
		<input type="hidden" id="MATERIAL" name="MATERIAL"/>
		<input type="hidden" id="TREE" name="TREE"/>
		<input type="hidden" id="CATEGORY" name="CATEGORY"/>
		<input type="hidden" id="CATEGORY_TYPE" name="CATEGORY_TYPE"/>
		<input type="hidden" id="CATEGORY_NAME" name="CATEGORY_NAME"/>
		<input type="hidden" id="MATERIAL_INOUT" name="MATERIAL_INOUT"/>
		
		<div class="panel-wrap-top" style="width:100%;">
			<div class="fl-box hgt-per-100" style="width:20%;">
				<h5 class="panel-tit">자재유형</h5>
				<div class="panel-body">
					<%-- 트리그리드[자재유형] : s --%>
					<div id="categoryList">
						<bdo	Debug="Error"
								Data_Url="/sys/category/categoryData.do?CATEGORY_TYPE=MATERIAL"
								Layout_Url="/sys/category/categoryNotEditLayout.do?CATEGORY_TYPE=MATERIAL"
								Upload_Url="/sys/category/categoryEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"
						>
						</bdo>
					</div>
					<%-- 트리그리드[자재유형] : e --%>
				</div>
			</div>
			<div class="fl-box hgt-per-100" style="width:30%;">
				<h5 class="panel-tit mgn-l-10" id="materialListTitle">자재</h5>
				<div class="panel-body mgn-l-10">
					<%-- 트리그리드[자재] : s --%>
					<div id="MaterialList2">
						<bdo	Debug="Error"
								Data_Url=""
								Layout_Url="/epms/material/inout/materialInoutMasterLayout.do"
								Upload_Url="/epms/material/inout/materialInoutMasterEdit.do?MATERIAL_INOUT_TYPE=01" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"			
								Export_Data="data" Export_Type="xls"
								Export_Url="/sys/comm/exportGridData.jsp?File=자재목록.xls&dataName=data"
						>
						</bdo>
					</div>
					<%-- 트리그리드[자재] : e --%>
				</div>
			</div>
			<div class="fl-box hgt-per-100" style="width:50%;">
				<h5 class="panel-tit mgn-l-10">발주 내역</h5>
				<div class="panel-body mgn-l-10">
					<%-- 트리그리드[자재] : s --%>
					<div id="MaterialInoutRegList">
						<bdo	Debug="Error"
				 				Data_Url="/epms/material/inout/materialInoutMngData.do?MATERIAL_INOUT_TYPE=01"
								Layout_Url="/epms/material/inout/materialInoutMngLayout.do"
								Upload_Url="/epms/material/inout/materialInoutMngEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"	
								Export_Data="data" Export_Type="xls"
								Export_Url="/sys/comm/exportGridData.jsp?File=MaterialInoutRegList.xls&dataName=data"
								>
						</bdo>
					</div>
					<%-- 트리그리드[자재] : e --%>
				</div>
			</div>
		</div>
		
		<div class="panel-wrap-btm" style="width:100%;">
			<h5 class="panel-tit mgn-t-10">내 발주등록 현황</h5>
			<div class="inq-area-inner type07 ab">
				<ul class="wrap-inq">
					<li class="inq-clmn">
						<h4 class="tit-inq">검색일</h4>
						<div class="prd-inp-wrap" style="width:300px;">
							<span class="prd-inp" >
								<span class="inner">
									<input type="text" id="startDt" class="inp-comm datePic inpCal" readonly="readonly" title="검색시작일" value="${startDt}">
								</span>
							</span>	
							<span class="prd-inp">
								<span class="inner">
									<input type="text" id="endDt" class="inp-comm datePic inpCal" readonly="readonly" title="검색종료일" value="${endDt}">
								</span>
							</span>	
						</div>
					</li>
				</ul>
				<a href="#none" id="srcBtn" class="btn comm st01">검색</a>
			</div>
			<div class="panel-body pd-t-40">
				<div id="MaterialOrderDtl">
					<bdo	Debug="Error"
							Data_Url="/epms/material/order/materialOrderListData.do?startDt=${startDT}&endDt=${endDt}"
							Layout_Url="/epms/material/order/materialOrderListLayout.do"
							Export_Data="data" Export_Type="xls"
							Export_Url="/sys/comm/exportGridData.jsp?File=발주등록 현황 목록.xls&dataName=data"
					>
					</bdo>
				</div>
			</div>
		</div>
		
</div>

<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<%-- 발주 등록 입력창 --%>
<%@ include file="/WEB-INF/jsp/epms/material/order/popMaterialOrderRegForm.jsp"%>

<%-- 발주 상세현황 --%>
<%@ include file="/WEB-INF/jsp/epms/material/order/popMaterialOrderDtlForm.jsp"%>				
