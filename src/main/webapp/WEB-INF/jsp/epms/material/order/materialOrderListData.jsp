<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
	Class Name	: materialOrderMngData.jsp
	Description : 자재관리 > 발주등록 > 내 발주등록 현황 그리드 데이터
	author		: 김영환
	since		: 2018.04.04
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.04	 김영환		최초 생성

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I	
			MATERIAL_INOUT			= "${item.MATERIAL_INOUT}"	
			DATE_INOUT				= "${item.DATE_INOUT}"
			STATUS					= "${item.STATUS}"
			TITLE					= "${item.TITLE}"
			QNTY_PLAN				= "${item.QNTY_PLAN}"
			QNTY					= "${item.QNTY}"
			QNTY_PLAN_COUNT			= "${item.QNTY_PLAN_COUNT}"
			QNTY_COUNT				= "${item.QNTY_COUNT}"
			DETAILSwitch="1"  DETAILIcon="/images/com/web/commnet_up.gif" 
			DETAILOnClickSideIcon="fnOpenLayerWithGrid(Row, 'popMaterialOrderDtlForm');"
			<c:if test="${item.STATUS eq '02'}">
				STATUSClass="gridStatus3"
			</c:if>
			<c:choose>
				<c:when test="${item.QNTY_PLAN eq item.QNTY}">
				QNTYClass="gridStatus3"
				</c:when>
				<c:otherwise>
				QNTYClass="gridStatus4"
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test="${item.QNTY_PLAN_COUNT eq item.QNTY_COUNT}">
				QNTY_COUNTClass="gridStatus3"
				</c:when>
				<c:otherwise>
				QNTY_COUNTClass="gridStatus4"
				</c:otherwise>
			</c:choose>
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>