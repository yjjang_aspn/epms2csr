<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: popMaterialOrderDtlLayout.jsp
	Description : 자재관리 > 발주등록 > 내 발주등록 현황 > 상세조회 그리드 레이아웃
    author		: 김영환
    since		: 2018.03.21
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자	수정내용
	----------  ------	---------------------------
	2018.03.21	김영환	최초생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="MaterialOrderDtl"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		Filtering="1"				Dragging="0"
			Editing="1"				Deleting="0"		Selecting="0"		SuppressCfg="0"
			CopyCols="0"			PasteFocused="3"	CustomScroll="1"
	/>

	<Header	id="Header"	Align="center"
		MATERIAL_INOUT_ITEM		= "자재입출고내역ID"
		MATERIAL				= "자재ID"
		LOCATION				= "위치코드"
		LOCATION_NAME			= "위치"
		MATERIAL_TYPE			= "자재유형코드"
		MATERIAL_TYPE_NAME		= "자재유형"
		MATERIAL_NAME			= "자재명"
		MATERIAL_UID			= "자재코드"
		MODEL					= "모델명"
		SPEC					= "규격"
		UNIT					= "단위코드"
		UNIT_NAME				= "단위"
		PRICE					= "단가"
		QNTY_PLAN				= "발주수량"
		QNTY					= "입고수량"
		MEMO					= "비고"
		DATE_INOUT				= "입고일"
	/>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<Cols>
		<C Name="MATERIAL_INOUT_ITEM"		Type="Text"		Align="center"		Visible="0" Width="50"	CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="MATERIAL"					Type="Text"		Align="center"		Visible="0" Width="50"	CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="LOCATION"					Type="Text"		Align="center"		Visible="0" Width="50"	CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="LOCATION_NAME"				Type="Text"		Align="left"		Visible="1" Width="70"	CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="MATERIAL_TYPE"				Type="Text"		Align="center"		Visible="0" Width="50"	CanEdit="0" CanExport="0" CanHide="0"/>	
		<C Name="MATERIAL_TYPE_NAME"		Type="Text"		Align="left"		Visible="1" Width="100"	CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="MATERIAL_NAME"				Type="Text"		Align="left"		Visible="1" Width="150"	CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="MATERIAL_UID"				Type="Text"		Align="center"		Visible="1" Width="130"	CanEdit="0" CanExport="0" CanHide="0"/> 
		<C Name="MODEL"						Type="Text"		Align="left"		Visible="1" Width="130"	CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="SPEC"						Type="Text"		Align="left"		Visible="1" Width="130"	CanEdit="0" CanExport="0" CanHide="0"/> 
		<C Name="UNIT"						Type="Text"		Align="center"		Visible="0" Width="50"	CanEdit="0" CanExport="0" CanHide="0"/>
	</Cols>
	
	<RightCols>
		<C Name="UNIT_NAME"					Type="Text"		Align="center"		Visible="1" Width="70"	CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="PRICE"						Type="Int"		Align="right"		Visible="1" Width="100"	CanEdit="0" CanExport="0" CanHide="0" Format="#,##0"/>
		<C Name="QNTY_PLAN"					Type="Text"		Align="center"		Visible="1" Width="70"	CanEdit="0" CanExport="0" CanHide="0"/>
		<c:choose>
			<c:when test="${param.STATUS == 01}"> <%-- 발주 --%>
			<C Name="QNTY"						Type="Int"		Align="center"		Visible="1"	Width="70"	CanEdit="1" CanExport="0" CanHide="0" OnChange="changeSts2(Grid,Row,Col);"/>
			<C Name="DATE_INOUT"				Type="Date"		Align="center"		Visible="1"	Width="110" CanEdit="1"	CanExport="1" CanHide="1" Format="yyyy/MM/dd"/>
			<C Name="MEMO"						Type="Text"		Align="center"		Visible="1"	MinWidth="100" RelWidth="20" CanEdit="1" CanExport="0" CanHide="0"/>
			</c:when>
			<c:when test="${param.STATUS == 02}"> <%-- 입고완료 --%>
			<C Name="QNTY"						Type="Int"		Align="center"		Visible="1"	Width="70"	CanEdit="0" CanExport="0" CanHide="0" OnChange="changeSts2(Grid,Row,Col);"/>
			<C Name="DATE_INOUT"				Type="Date"		Align="center"		Visible="1"	Width="110" CanEdit="0"	CanExport="1" CanHide="1" Format="yyyy/MM/dd"/>
			<C Name="MEMO"						Type="Text"		Align="center"		Visible="1"	MinWidth="100" RelWidth="20" CanEdit="0" CanExport="0" CanHide="0"/>
			</c:when>
		</c:choose>
		
	</RightCols>
	
	<c:choose>
		<c:when test="${param.STATUS == 01}">
		<c:set var="Cells" value="Empty,입고완료,저장,새로고침,인쇄,엑셀"/>
		</c:when>
		<c:when test="${param.STATUS == 02}">
		<c:set var="Cells" value="Empty,새로고침,인쇄,엑셀"/>
		</c:when>
	</c:choose>
	<Toolbar	Space="0"	Styles="2"	Cells="${Cells}"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        " 
				ColumnsType="Button"
				입고완료Type="Html" 입고완료="&lt;a href='#none' title='입고완료' class=&quot;treeButton treeApproval&quot;
					onclick='fnSave(&quot;${gridId}&quot;,&quot;reg&quot;)'>입고완료&lt;/a>"
				저장Type="Html" 저장="&lt;a href='#none' title='저장' class=&quot;treeButton treeSave&quot;
					onclick='fnSave(&quot;${gridId}&quot;,&quot;save&quot;)'>저장&lt;/a>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>