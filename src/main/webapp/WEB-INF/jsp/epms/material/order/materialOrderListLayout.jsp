<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: materialOrderMngLayout.jsp
	Description : 자재관리 > 발주등록 > 내 발주등록 현황 그리드 레이아웃
    author		: 김영환
    since		: 2018.03.21
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자	수정내용
	----------  ------	---------------------------
	2018.03.21	김영환	최초생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="MaterialOrderList"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		Filtering="1"				Dragging="0"
			Editing="1"				Deleting="0"		Selecting="0"		SuppressCfg="0"
			Paging="1"			  	PageLength="10"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="0"			PasteFocused="3"	CustomScroll="1"
	/>

	<Header	id="Header"	Align="center"
		MATERIAL_INOUT			= "자재입출고ID"
		DATE_INOUT				= "발주일"
		STATUS					= "상태"
		TITLE					= "제목"
		QNTY_PLAN				= "발주품목"
		QNTY					= "입고품목"
		QNTY_PLAN_COUNT			= "발주 총수량"
		QNTY_COUNT				= "입고 총수량"
		DETAIL					= "상세조회"
	/>
	
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Pager Visible="0"	CanHide="0"/>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<Cols>
		<C Name="MATERIAL_INOUT"			Type="Text"		Align="center"		Visible="0"		RelWidth="50"	CanEdit="0"		CanExport="1" CanHide="1"/>
		<C Name="DATE_INOUT"				Type="Date"		Align="center"		Visible="1"		RelWidth="50"	CanEdit="0"		CanExport="1" CanHide="1"	Format="yyyy/MM/dd"/>
		<C Name="STATUS"					Type="Enum"		Align="center"		Visible="1"		RelWidth="50"	CanEdit="0" 	CanExport="1" CanHide="1"	Enum='|발주|입고완료'	EnumKeys='|01|02'/>	
		<C Name="TITLE"						Type="Text"		Align="left"		Visible="1"		RelWidth="200"	CanEdit="0"		CanExport="1" CanHide="1"/>
		<C Name="QNTY_PLAN"					Type="Text"		Align="center"		Visible="1"		RelWidth="50"	CanEdit="0"		CanExport="1" CanHide="1"/> 
		<C Name="QNTY"						Type="Text"		Align="center"		Visible="1"		RelWidth="50"	CanEdit="0"		CanExport="1" CanHide="1"/>
		<C Name="QNTY_PLAN_COUNT"			Type="Text"		Align="center"		Visible="1"		RelWidth="50"	CanEdit="0"		CanExport="1" CanHide="1"/> 
		<C Name="QNTY_COUNT"				Type="Text"		Align="center"		Visible="1"		RelWidth="50"	CanEdit="0"		CanExport="1" CanHide="1"/>
		<C Name="DETAIL"					Type="Icon"		Align="center"		Visible="1"		RelWidth="30"	CanEdit="0"		CanExport="1" CanHide="1"/>
	</Cols>
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        " 
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>