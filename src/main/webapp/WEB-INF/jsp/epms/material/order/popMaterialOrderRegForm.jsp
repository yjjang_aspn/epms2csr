<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">

var submitBool = true;

$(document).ready(function() {
	
	<%-- '정비취소'버튼 클릭 --%>
	$('#registBtn').on('click', function(){
		
		if (submitBool) {
			fnRegist();
		} else {
			alert("등록중입니다. 잠시만 기다려 주세요.");
			return false;
		}
		
	});
	
	<%-- '취소'버튼 클릭 --%>
	$('#cancelBtn').on('click', function(){
		fnModalToggle('popMaterialOrderRegForm');
	});
});

<%-- 정비취소 --%>
function fnRegist(){
	
	submitBool = false;
	
	if (!isNull_J($('#TITLE'), '발주 제목을 입력해주세요.')) {
		submitBool = true;
		return false;
	}
	
	if(confirm("발주를 등록하시겠습니까?")){
		$.ajax({
			type: 'POST',
			url: '/epms/material/order/insertMateralOrderList.do',
			data : { "MATERIAL_INOUT_TYPE" : '01',
					 "DATE_INOUT" : $("#DATE_INOUT").val(),
					 "TITLE" : $("#TITLE").val(),
					 "ORDER_ITEM" : JSON.stringify(orderItemArray) },
			dataType: 'json',
			async : false,
			success: function (json) {
				if(json.MATERIAL_INOUT != null && json.MATERIAL_INOUT != ''){
					alert("발주가 등록되었습니다.");
					fnRegAfterAction();
				}
				else{
					alert("발주 등록에 실패하였습니다.");
				}
				submitBool = true;
			},error: function(XMLHttpRequest, textStatus, errorThrown){
				alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
				submitBool = true;
			}
		});
		
	}else{
		submitBool = true;
	}
}
	
</script>

<div class="modal fade modalFocus" id="popMaterialOrderRegForm"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-40">
		<div class="modal-content hgt-per-50" style="min-height:540px;">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">발주 등록</h4>
			</div>
		 	<div class="modal-body">
	           	<div class="modal-bodyIn">
					<form id="cancelForm" name="cancelForm"  method="post" enctype="multipart/form-data">	
						<div class="tb-wrap">
							<table class="tb-st">
								<caption class="screen-out">발주 등록</caption>
									<colgroup>
										<col width="30%" />
										<col width="*" />
									</colgroup>
									<tbody>	
										<tr>
											<th>발주일</th>
											<td>
												<div class="inner">
													<input type="text" class="inp-comm datePic inpCal" title="발주일" id="DATE_INOUT" name="DATE_INOUT" value="${DATE_INOUT}" readonly="readonly"/>
												</div>
											</td>
										</tr>		
										<tr>
											<th>제목</th>
											<td>
												<div class="inp-wrap wd-per-100">
													<input type="text" class="inp-comm" title="제목" id="TITLE" name="TITLE" />
												</div>
											</td>
										</tr>
										<tr>
											<th>발주내용</th>
											<td>
												<table class="tb-st" id="orderInfo">
													<tbody>
														<tr>
															<th>자재명</th>
															<th>발주수량</th>
															<th>단가</th>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>							
									</tbody>
							</table>
						</div>
					</form>
				</div>
			</div>	
			<div class="modal-footer hasFooter">
				<a href="#none" id="cancelBtn" class="btn comm st02" data-dismiss="modal">취소</a>
				<a href="#none" id="registBtn" class="btn comm st01">등록</a>
			</div>
		</div>		
	</div>
</div>
