<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
	Class Name	: materialMasterListData.jsp
	Description : 설비BOM(자재 -> 설비) > 자재정보 조회/관리 그리드 데이터
    author		: 김영환
    since		: 2018.04.18
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.18	김영환		최초 생성

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I		
			MATERIAL				= "${item.MATERIAL}"
			MATERIAL_TYPE			= "${item.MATERIAL_TYPE}"
			MATERIAL_TYPE_NAME		= "${item.MATERIAL_TYPE_NAME}"
			MATERIAL_UID			= "${item.MATERIAL_UID}"
			MATERIAL_NAME			= "${fn:replace(item.MATERIAL_NAME, '"', '&quot;')}"
			MODEL					= "${fn:replace(item.MODEL, '"', '&quot;')}"
			SPEC					= "${fn:replace(item.SPEC, '"', '&quot;')}"
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>