<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: materialMasterListLayout.jsp
	Description : 설비BOM(자재 -> 설비) > 자재정보 조회/관리 그리드 데이터ㄴ
    author		: 김영환
    since		: 2018.04.18
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.18	 김영환		최초 생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="MaterialList"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		Filtering="1"				Dragging="0"
			Editing="0"				Deleting="0"							SuppressCfg="0"				StandardFilter="3"
			CopyCols="0"			PasteFocused="3"	CustomScroll="1"
	/>
	<Head>
		<Header	id="Header"	Align="center"
			MATERIAL				= "<spring:message code='epms.object.id' />"    <%  // 자재ID"    %>
			MATERIAL_TYPE			= "<spring:message code='epms.object.type' />"    <%  // 자재구분"    %>
			MATERIAL_TYPE_NAME		= "<spring:message code='epms.object.type.name' />"    <%  // 자재유형"    %>
			MATERIAL_UID			= "<spring:message code='epms.object.uid' />"    <%  // 자재코드"    %>
			MATERIAL_NAME			= "<spring:message code='epms.object.name' />"    <%  // 자재명"     %>
			MODEL					= "<spring:message code='epms.model' />"    <%  // 모델명"     %>
			SPEC					= "<spring:message code='epms.spec' />"    <%  // 규격"      %>
		/>
		
		<Filter	id="Filter" CanExport="1" CanHide="0" CanFocus="1"/>
	</Head>
	
	<Panel CanHide="0"/>
	
	<Cols>
		<C Name="MATERIAL"					Type="Text"		Align="center"		Visible="0"		Width="70"	CanEdit="0"/>
		<C Name="MATERIAL_TYPE"				Type="Enum"		Align="left"		Visible="0"		Width="70"	CanEdit="0"	CanHide="0"	/> 
		<C Name="MATERIAL_TYPE_NAME"		Type="Text"		Align="left"		Visible="0"		Width="70"	CanEdit="0"	CanHide="0"	/>
		<C Name="MATERIAL_UID"				Type="Text"		Align="left"		Visible="1"		Width="145" CanEdit="0" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="145"/>
		<C Name="MATERIAL_NAME"				Type="Text"		Align="left"		Visible="1"		Width="145"	CanEdit="0" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="145"/>
		<C Name="MODEL"						Type="Text"		Align="left"		Visible="0"		Width="70"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="SPEC"						Type="Text"		Align="left"		Visible="0"		Width="70"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
	</Cols>
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,항목,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        "
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
	
</Grid>