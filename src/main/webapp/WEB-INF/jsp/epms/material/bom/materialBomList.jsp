<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: materialBomList.jsp
	Description : 자재관리 > 자재정보 조회 메인화면
    author		: 김영환
    since		: 2018.04.18
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.18	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<script type="text/javascript">

$(document).ready(function(){
	treeslideLeft_0301(); /* 트리그리드 3개일 때 화면여닫기 실행 */
});

<%-- 전역변수 --%>
var focusedRow = null;		<%-- Focus Row : [자재] --%>
var focusedRow2 = null;		<%-- Focus Row : [설비] --%>
var focusedRow3 = null;		<%-- Focus Row : [설비BOM] --%>
var callLoadingYn = true;	<%-- 로딩바 호출여부 --%>

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	<%-- [자재] 클릭시 --%>
	if(grid.id == "CategoryList") {
		
		if (focusedRow == null || focusedRow.CATEGORY != row.CATEGORY || focusedRow2 != null){
			<%-- 1. 포커스 --%>
			if(focusedRow != null){
				grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
			}
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			
			focusedRow = row;
			focusedRow2 = null;
			focusedRow3 = null;
			
			<%-- 2. 자재 목록 조회 --%>
			$('#CATEGORY').val(row.CATEGORY);			<%-- 카테고리 번호 --%>
			$('#CATEGORY_TYPE').val(row.CATEGORY_TYPE);	<%-- 카테고리 유형 : 'MATERIAL' --%>
			$('#CATEGORY_NAME').val(row.CATEGORY_NAME);	<%-- 카테고리 명 : 라인명 --%>
			$('#TREE').val(row.TREE);					<%-- 구조체 --%>
			$('#MATERIAL').val("");
			$('#materialBomListTitle').html("<spring:message code='epms.system.bom' /> : " + "[ " + row.CATEGORY_NAME + " ]");	<%-- 설비BOM 타이틀 변경 --%>
			fnGetMaterialList();
		}
	}
	
	<%-- 2. 설비BOM(자재->설비) 조회 이벤트 --%>
	if(grid.id == "MaterialList") {
		
		if (focusedRow2 == null || focusedRow2.MATERIAL != row.MATERIAL){
			<%-- 2-1. 포커스 --%>
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			if(focusedRow2 != null){
				grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
			}
			focusedRow2 = row;
			
			<%-- 2-2. 설비 BOM 목록 조회 --%>
			$('#MATERIAL').val(row.MATERIAL);			<%-- 자재 ID --%>
			
			$('#materialBomListTitle').html("<spring:message code='epms.system.bom' /> : " + "[ " + $('#CATEGORY_NAME').val() + " ] - [ " + row.MATERIAL_NAME + " ]");	<%-- 설비BOM 타이틀 변경 --%>
			
			getMaterialBomList();
		}
	}
	
	if(grid.id == "MaterialBomList") {
		<%-- 1. 포커스  --%>
		if(focusedRow3 != null){
			grid.SetAttribute(focusedRow3,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow3 = row;		
	}
}

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source,data, func){
	
	if(grid.id == "MaterialList"){			<%-- [자재] 목록 조회시 --%>
		if(data != ""){
			callLoadingBar();
			callLoadingYn = false;
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "MaterialList"){
					getMaterialBomList();
				}
			}
		}
	}
	else if(grid.id == "MaterialBomList"){	<%-- [설비BOM] 목록 조회시 --%>
		if(data != ""){
			if(callLoadingYn == true){
				callLoadingBar();
				callLoadingYn = false;
			}
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "MaterialBomList"){
					setTimeout($.unblockUI, 100);
					callLoadingYn = true;
				}
			}
		}
	}
}

<%-- 선택된 자재유형에 해당되는 자재 조회 --%>
function fnGetMaterialList(){
	Grids.MaterialList.Source.Data.Url = "/epms/material/bom/materialListData.do?TREE=" + $('#TREE').val()
											 + "&CATEGORY=" + $('#CATEGORY').val()
											 + "&CATEGORY_TYPE=" + $('#CATEGORY_TYPE').val()
											 + "&editYn=N";
	Grids.MaterialList.ReloadBody();	
}

<%-- 선택된 자재에 해당되는 설비BOM 조회 --%>
function getMaterialBomList(){
	Grids.MaterialBomList.Source.Data.Url = "/epms/material/bom/materialBomListData.do?TREE=" + $('#TREE').val()
											+ "&MATERIAL=" + $('#MATERIAL').val()
											+ "&CATEGORY_TYPE=" + $('#CATEGORY_TYPE').val();
	Grids.MaterialBomList.ReloadBody();
}

</script>
</head>
<body>
<%-- 화면 UI Area Start --%>
<div id="contents">
	<input type="hidden" id="CATEGORY"      name="CATEGORY"/>
	<input type="hidden" id="CATEGORY_TYPE" name="CATEGORY_TYPE"/>
	<input type="hidden" id="CATEGORY_NAME" name="CATEGORY_NAME"/>
	<input type="hidden" id="TREE"          name="TREE"/>
	<input type="hidden" id="MATERIAL" 		name="MATERIAL"/>			<%-- 자재 ID --%>
	<input type="hidden" id="MATERIAL_NAME" name="MATERIAL_NAME"/>		<%-- 자재 명 --%>
	
	<div class="fl-box panel-wrap03 slideWidth rel" style="width:20%; height:100%;">
		<div class="slideArea">
			<h5 class="panel-tit"><spring:message code='epms.object.type' /></h5><!-- 자재유형 -->
			<div class="panel-body">
				<div id="categoryList" class="slideTreeGrid">
					<bdo	Debug="Error"
							Data_Url="/sys/category/categoryData.do?CATEGORY_TYPE=MATERIAL"
							Layout_Url="/sys/category/categoryNotEditLayout.do?CATEGORY_TYPE=MATERIAL"
					>
					</bdo>
				</div>
			</div>
		</div>
		<a href="#none" class="slideBtn ir-pm" title="닫기" style="left:110px;">토글</a>
	</div>
	
	<div class="fl-box panel-wrap03 slideWidth rel" style="width:20%; height:100%;">
		<div class="slideArea">
			<h5 class="panel-tit mgn-l-10" id="materialListTitle"><spring:message code='epms.object' /></h5><!-- 자재 -->
			<div class="panel-body mgn-l-10">
				<%-- 트리그리드[자재] : s --%>
				<div id="materialList" class="slideTreeGrid">
					<bdo	Debug="Error"
							Data_Url=""
							Layout_Url="/epms/material/bom/materialListLayout.do"
					>
					</bdo>
				</div>
			</div>
		</div>
		<a href="#none" class="slideBtn type02 ir-pm" title="닫기" style="left:90px;">토글</a>
	</div>
	
	<div class="fl-box panel-wrap03" style="width:59.5%; height:100%;" id="slideWidthRight">
		<h5 class="panel-tit mgn-l-10" id="materialBomListTitle"><spring:message code='epms.system.bom' /></h5><!-- 설비BOM -->
		<div class="panel-body mgn-l-10">
			<!-- 트리그리드[자재BOM] :  -->
			<div id="equipmentList">
				<bdo	Debug="Error"
		 				Data_Url=""
						Layout_Url="/epms/material/bom/materialBomListLayout.do"
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=EquipmentBomList.xls&dataName=data"
						>
				</bdo>
			</div>
			<!-- e:grid-wrap -->
			<!-- 트리그리드 : e -->
		</div>
	</div>
</div><%-- 화면 UI Area End --%>
</body>