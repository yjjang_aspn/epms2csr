<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I		
			MATERIAL				= "${item.MATERIAL}"
			LOCATION				= "${item.LOCATION}"
			CD_MATERIAL_TYPE		= "${item.CD_MATERIAL_TYPE}"
			MATERIAL_GRP1			= "${item.MATERIAL_GRP1}"
			MATERIAL_GRP2			= "${item.MATERIAL_GRP2}"
			MATERIAL_GRP3			= "${item.MATERIAL_GRP3}"
			MATERIAL_UID			= "${item.MATERIAL_UID}"
			MATERIAL_NAME			= "${fn:replace(item.MATERIAL_NAME, '"', '&quot;')}"
			MODEL					= "${fn:replace(item.MODEL, '"', '&quot;')}"
			SPEC					= "${fn:replace(item.SPEC, '"', '&quot;')}"
			COUNTRY					= "${item.COUNTRY}"
			MANUFACTURER			= "${fn:replace(item.MANUFACTURER, '"', '&quot;')}"
			MEMO					= "${fn:replace(item.MEMO, '"', '&quot;')}"
			CD_UNIT					= "${item.CD_UNIT}"
			CD_UNIT_NAME			= "${item.cd_unit_name}"
			
			STOCK_OPTIMAL			= "${item.STOCK_OPTIMAL}"
			<c:choose>
				<c:when test="${item.STOCK_OPTIMAL > 0}">
					STOCK					= "${item.STOCK}"
					STOCK_CONDITION			= "${item.stock_condition}"
					<c:if test="${item.stock_condition == '1'}">
						STOCK_CONDITIONClass="gridStatus3"
					</c:if>
					<c:if test="${item.stock_condition == '2'}">
						STOCKClass="gridStatus4"
						STOCK_CONDITIONClass="gridStatus4"
					</c:if>
				</c:when>
				<c:otherwise>
					STOCK					= ""
					STOCK_CONDITION			= ""
				</c:otherwise>
			</c:choose>
			
			ATTACH_GRP_NO			= "${item.ATTACH_GRP_NO}"
			
			<c:if test='${item.file_cnt>0 || editYn eq "Y"}'>
				fileSwitch="1"  fileIcon="/sys/images/web/commnet_up3.gif" 
				fileOnClickSideIcon="fn_openPopPage('/project/materialMaster/admin/popImgView.do','?MATERIAL='+Row.MATERIAL+'&editYn='+'${editYn}',420,510);"
			</c:if>
			
			history_inSwitch="1"  history_inIcon="/sys/images/web/receiving_icon.gif" 
			history_inOnClickSideIcon="fn_openPopPage('/project/materialMaster/admin/popMaterialInView.do','?MATERIAL='+Row.MATERIAL,880,510);"
			history_outSwitch="1"  history_outIcon="/sys/images/web/shipping_icon.gif" 
			history_outOnClickSideIcon="fn_openPopPage('/project/materialMaster/admin/popMaterialOutView.do','?MATERIAL='+Row.MATERIAL,880,510);"
			
			detailSwitch="1"  detailIcon="/sys/images/web/commnet_up.gif" 
			detailOnClickSideIcon="fn_openPopPage('/project/materialMaster/admin/popMaterialDtl.do','?MATERIAL='+Row.MATERIAL,500,650);"
			
			CD_DEL					= "${item.CD_DEL}"
			<c:if test='${item.CD_DEL eq "1"}'>
				CD_DELClass="gridStatus3"
			</c:if>
			<c:if test='${item.CD_DEL eq "2"}'>
				CD_DELClass="gridStatus4"
			</c:if>
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>