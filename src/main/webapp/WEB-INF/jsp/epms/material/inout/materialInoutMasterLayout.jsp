<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: materialInoutMasterLayout.jsp
	Description : 자재관리 > 입출고 등록 > 자재목록 그리드 레이아웃
    author		: 김영환
    since		: 2018.02.07
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자	수정내용
	----------  ------	---------------------------
	2018.03.21	김영환	최초생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="MaterialList2"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		Filtering="1"				Dragging="0"
			Editing="1"				Deleting="0"		Selecting="1"		SuppressCfg="0"
			Paging="1"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="0"			PasteFocused="3"	CustomScroll="1"
	/>

	<Header	id="Header"	Align="center"
		MATERIAL				= "<spring:message code='epms.object.id' />"  <%  // 자재ID"        %>
		LOCATION				= "<spring:message code='epms.location' />"  <%  // 위치"          %>
		MATERIAL_TYPE			= "<spring:message code='epms.object.type' />"  <%  // 자재유형코드"      %>
		MATERIAL_TYPE_NAME		= "<spring:message code='epms.object.type.name' />"  <%  // 자재유형"        %>
		MATERIAL_UID			= "<spring:message code='epms.object.uid' />"  <%  // 자재코드"        %>
		MATERIAL_NAME			= "<spring:message code='epms.object.name' />"  <%  // 자재명"         %>
		MODEL					= "<spring:message code='epms.model' />"  <%  // 모델명"         %>
		SPEC					= "<spring:message code='epms.spec' />"  <%  // 규격"          %>
		COUNTRY					= "<spring:message code='epms.country' />"  <%  // 제조국"         %>
		MANUFACTURER			= "<spring:message code='epms.supplier' />"  <%  // 제조사"         %>
		STOCK_CONDITION			= "<spring:message code='epms.stock.status' />"  <%  // 재고상태"        %>
		MEMO					= "<spring:message code='epms.memo' />"  <%  // 메모"          %>
		UNIT					= "<spring:message code='epms.unit' />"  <%  // 단위"          %>
		STOCK					= "<spring:message code='epms.stock' />"  <%  // 재고"          %>
		STOCK_OPTIMAL			= "<spring:message code='epms.safety.stock' />"  <%  // 안전재고"        %>
		PASS_DATE    			= "<spring:message code='epms.exceed.day' />"  <%  // 경과일"         %>
		MEMBER					= "<spring:message code='epms.material.MEMBER' />"  <%  // 등록자ID"       %>
		MEMBER_NAME				= "<spring:message code='epms.material.MEMBER_NAME' />"  <%  // 등록자"         %>
		ATTACH_GRP_NO			= "<spring:message code='epms.attachfile.code' />"  <%  // 첨부파일 코드"     %>
		FILE					= "<spring:message code='epms.attachfile' />"  <%  // 이미지"         %>
		flag					= "<spring:message code='epms.select.flag' />"  <%  // 선택구분"        %>

	/>
	
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
	
	<LeftCols>
		<C Name="MATERIAL"					Type="Text"	Align="center" Visible="0" Width="100" CanEdit="0" CanExport="0" CanHide="1"/>
		<C Name="LOCATION"					Type="Text"	Align="left"   Visible="0" Width="70"  CanEdit="0" CanExport="0" CanHide="1"/>	
		<C Name="MATERIAL_TYPE"				Type="Text"	Align="left"   Visible="0" Width="70"  CanEdit="0" CanExport="0" CanHide="1"/>
		<C Name="MATERIAL_TYPE_NAME"		Type="Text"	Align="left"   Visible="0" Width="70"  CanEdit="0" CanExport="0" CanHide="0"/> 
		<C Name="MATERIAL_UID"				Type="Text"	Align="left"   Visible="1" Width="180" CanEdit="0" CanExport="0" CanHide="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="MATERIAL_NAME"				Type="Text"	Align="left"   Visible="1" Width="180" CanEdit="0" CanExport="0" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
	</LeftCols>
	
	<Cols>
		<C Name="MODEL"						Type="Text"	Align="left"   Visible="1" Width="150" CanEdit="0" CanExport="0" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="SPEC"						Type="Text"	Align="left"   Visible="1" Width="200" CanEdit="0" CanExport="0" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="COUNTRY"					Type="Enum"	Align="left"   Visible="1" Width="60"  CanEdit="0" CanExport="0" CanHide="1" <tag:enum codeGrp="COUNTRY"/>/>
		<C Name="MANUFACTURER"				Type="Text"	Align="left"   Visible="1" Width="150" CanEdit="0" CanExport="0" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="MEMO"						Type="Text"	Align="left"   Visible="1" Width="200" CanEdit="0" CanExport="0" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="UNIT"						Type="Enum"	Align="left"   Visible="1" Width="60"  CanEdit="0" CanExport="0" CanHide="1" <tag:enum codeGrp="UNIT"/>/>
		<C Name="STOCK_OPTIMAL"				Type="Int"	Align="center" Visible="1" Width="60"  CanEdit="0" CanExport="0" CanHide="1"/>
		<C Name="STOCK"						Type="Int"	Align="center" Visible="1" Width="60"  CanEdit="0" CanExport="0" CanHide="1"/>
		<C Name="STOCK_CONDITION"			Type="Enum"	Align="left"   Visible="1" Width="70"  CanEdit="0" CanExport="0" CanHide="1" <tag:enum codeGrp="STOCK_CONDITION"/> CanExport="1"/>
		<C Name="PASS_DATE"					Type="Text"	Align="center" Visible="1" Width="80"  CanEdit="0"/>
		<C Name="ATTACH_GRP_NO"				Type="Text"	Align="center" Visible="0" Width="80"  CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="FILE"						Type="Icon"	Align="center" Visible="0" Width="70"  CanEdit="0" CanExport="0" CanHide="1"/>
		<C Name="flag"						Type="Text"	Align="center" Visible="0" Width="70"  CanEdit="0" CanExport="0" CanHide="0"/>
	</Cols>
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,담기,항목,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        " 
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				담기Type="Html" 담기="&lt;a href='#none' title='담기' class=&quot;treeButton treeMove&quot;
					onclick='fnAddMaterial(&quot;MaterialList2&quot;)'>담기&lt;/a>"
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>