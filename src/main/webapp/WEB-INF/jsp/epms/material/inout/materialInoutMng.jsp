<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8" %>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld"                 %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: materialInoutMng.jsp
	Description : 자재관리 > 입출고 등록 메인화면
	author		: 김영환
	since		: 2018.04.04
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.04	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script type="text/javascript">

var flag = "";				<%-- 삭제 구분 (1:수정, 2:삭제)--%>
var focusedRow = null;		<%-- [자재유형] 선택된 ROW --%>
var focusedRow2 = null;		<%-- [자재목록] 선택된 ROW  --%>

$(document).ready(function(){	
	
	<%-- DatePicker 설정 --%>
	$( ".datePic" ).datepicker({
	   dateFormat   : "yy-mm-dd",
       prevText     : "click for previous months" ,
       nextText     : "click for next months" ,
       showOtherMonths   : true ,
       selectOtherMonths : false
	}).each(function(){
		if($(this).hasClass('thisDay')){
			$(this).val(stDate);
		}
	});
	
});


<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	<%-- 자재 조회 이벤트 --%>
	if(grid.id == "CategoryList") {	<%-- [라인] 클릭시 --%>
		
		if (focusedRow == null || focusedRow.CATEGORY != row.CATEGORY){
			
			<%-- 1. 포커스 --%>
			if(focusedRow != null){
				grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
			}
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			
			focusedRow = row;
			<%-- 2. 설비 목록 조회 --%>
			$('#CATEGORY').val(row.CATEGORY);			<%-- 카테고리 번호 --%>
			$('#CATEGORY_TYPE').val(row.CATEGORY_TYPE);	<%-- 카테고리 유형 : 'MATERIAL' --%>
			$('#CATEGORY_NAME').val(row.CATEGORY_NAME);	<%-- 카테고리 명 : 라인명 --%>
			$('#TREE').val(row.TREE);					<%-- 구조체 --%>
			$('#materialListTitle').html("<spring:message code='epms.object' /> : " + "[ " + row.CATEGORY_NAME + " ]");
			fnGetMaterialList();
		}
	<%-- 등록내역 조회 이벤트 --%>
	} else if(grid.id == "MaterialList2") {
		
		<%-- 포커스 시작 --%>
		if(focusedRow2 != row){
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		}
		if(focusedRow2 != null && focusedRow2 != row){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		focusedRow2 = row;
		<%-- 포커스 종료 --%>
	}
}

<%-- 선택된 라인에 해당되는 설비 조회 --%>
function fnGetMaterialList(){
	Grids.MaterialList2.Source.Data.Url = "/epms/material/master/materialMasterListData.do?TREE=" + $('#TREE').val()
											 + "&CATEGORY=" + $('#CATEGORY').val()
											 + "&CATEGORY_TYPE=" + $('#CATEGORY_TYPE').val()
											 + "&editYn=N";
	Grids.MaterialList2.ReloadBody();	
}

<%-- 그리드 더블클릭 이벤트 --%>
Grids.OnDblClick = function(grid, row, col){
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	<%-- 자재 선택 --%>
	if(grid.id == "MaterialList2") {
		grid.SetValue(row, "flag", '1', 1);
		
		if(grid.ActionValidate()){
			grid.ActionSave();
		}
	}
}

<%-- Validation --%>
function dataValidation(gridNm){
	var cnt = 0;
	var grid = Grids[gridNm];
	
	if(gridNm == "MaterialOrderDtl"){
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){  
			
			<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
			Grids[gridNm].EndEdit(row);
			
			if(row.Changed || row.Added){
				cnt++;
			}
		}
	}	
	
	<%-- 추가 및 변경된 데이터가 없을 경우 --%>
	if(cnt == 0){
		return false;
	}else{
		return true;
	}
}

<%-- Validation --%>
function dataValidation(gridNm){
	var cnt = 0;
	var grid = Grids[gridNm];
	
	// 자재목록 담기버튼
	if(gridNm == "MaterialList2"){
		
		cnt = Grids[gridNm].GetSelRows();
		
		if(cnt.length < 1){
			alert("담을 자재를 선택해주세요.");
			return false;
		}
		
	}
	// 등록내역 등록버튼
	else if(gridNm == "MaterialInoutRegList"){
		
		if($("#MATERIAL_INOUT_TYPE").val() != 03 && $("#MATERIAL_INOUT_TYPE").val() != 04){
			alert("입출고 구분을 선택해주세요.");
			return false;
		} else {
			
			var selRows = Grids[gridNm].GetSelRows();
			
			if(selRows.length < 1){
				alert("선택된 자재가 없습니다. 확인후 다시 시도하세요.");
				return false;
			} else{
				cnt = 1;
			}
		}
		
	}
	
	<%-- 추가 및 변경된 데이터가 없을 경우 --%>
	if(cnt == 0){
		return false;
	} else {
		return true;
	}
	
}

<%-- 담기 버튼 --%>
function fnAddMaterial(gridNm){
	
	<%-- 유효성 체크 --%>
    if(!dataValidation(gridNm)){
    	return;
    }
	
	var selRows = Grids[gridNm].GetSelRows();

	for(var i=0; i <selRows.length;i++){
		Grids[gridNm].SetValue(selRows[i], "flag", '1', 1);
	}
	
	if(Grids[gridNm].ActionValidate()){

		if(confirm("자재를 입출고 양식에 추가하시겠습니까?")){
			Grids[gridNm].ActionSave();

		}else{
			Grids[gridNm].ReloadBody();
		}
	}
	
}

<%-- 삭제 버튼 --%>
function fnDel(gridNm){
	
	var selRows = Grids[gridNm].GetSelRows();
	
	if(selRows.length < 1){
		alert("삭제할 자재를 선택해주세요.");
		return false;
	}

	for (var i = 0; i < selRows.length; i++) {
		Grids[gridNm].SetValue(selRows[i], "flag", 'del', 1);
	}
	
	if(Grids[gridNm].ActionValidate()){
		
		if(confirm("선택한 자재를 삭제하시겠습니까?")){
			
			<%-- Upload_Url 변경 --%>
			flag = "2";		<%-- 1:등록, 2삭제 --%>
			Grids[gridNm].Source.Upload.Url = "/epms/material/inout/materialInoutMngEdit.do?flag=" + flag;
			
			Grids[gridNm].ActionSave();

		}else{
			Grids[gridNm].ReloadBody();
		}
	}
}


<%-- 등록 버튼 --%>
function fnReg(gridNm){
	
	<%-- 유효성 체크 --%>
    if(!dataValidation(gridNm)){
    	return;
    }
    
    var selRows = Grids[gridNm].GetSelRows();
	
	if(Grids[gridNm].ActionValidate()){
		<%-- 입출고 구분 분기 (03:임의입고, 04:임의출고) --%>
		if($('#MATERIAL_INOUT_TYPE').val() == 03){	<%-- 임의입고 --%>
			if(confirm("임의입고 등록하시겠습니까?")){
				for (var i = 0; i < selRows.length; i++) {
					Grids[gridNm].SetValue(selRows[i], "flag", 'reg', 1);
				}
				
				<%-- Upload_Url 변경 --%>
				flag = "1";		<%-- 1:등록, 2삭제 --%>
				Grids[gridNm].Source.Upload.Url = "/epms/material/inout/materialInoutMngEdit.do?flag=" + flag
												+ "&MATERIAL_INOUT_TYPE=" + $('#MATERIAL_INOUT_TYPE').val()
												+ "&DATE_INOUT=" + $('#DATE_INOUT').val();
				Grids[gridNm].ActionSave();
			}else{
				Grids[gridNm].ReloadBody();
			}
		} 
		
		else if($('#MATERIAL_INOUT_TYPE').val() == 04){	<%-- 임의입고 --%>
			if(confirm("임의출고 등록하시겠습니까?")){
				for (var i = 0; i < selRows.length; i++) {
					Grids[gridNm].SetValue(selRows[i], "flag", 'reg', 1);
				}
				
				<%-- Upload_Url 변경 --%>
				flag = "1";		<%-- 1:등록, 2삭제 --%>
				Grids[gridNm].Source.Upload.Url = "/epms/material/inout/materialInoutMngEdit.do?flag=" + flag
												+ "&MATERIAL_INOUT_TYPE=" + $('#MATERIAL_INOUT_TYPE').val()
												+ "&DATE_INOUT=" + $('#DATE_INOUT').val();
				Grids[gridNm].ActionSave();
			}else{
				Grids[gridNm].ReloadBody();
			}
		} else{
			alert("입출고 구분을 선택해주세요.");
		}
			
	}
}

<%-- 입/출고 수량 또는 단가 수정 시 해당 ROW 자동 선택 --%>
function changeSts(grid,row,col){
	if(row.Selected != true){
		Grids['MaterialInoutRegList'].SelectRow(row);
	}
}

<%-- 그리드 ActionSave 후 처리 --%>
Grids.OnAfterSave  = function (grid){
	
	if(grid.id == "MaterialList2"){

		Grids.MaterialList2.ReloadBody();
		Grids.MaterialInoutRegList.ReloadBody();
		
	}else if (grid.id == "MaterialInoutRegList"){
		
		if(Grids.MaterialList2.Source.Data.Url != ""){
			Grids.MaterialList2.ReloadBody();
		}
		Grids.MaterialInoutRegList.ReloadBody();
		
	}
};

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source,data, func){
	if(grid.id == "MaterialList2"){		<%-- [자재] 목록 조회시 --%>
		if(data != ""){
			callLoadingBar();
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "MaterialList2"){
					setTimeout($.unblockUI, 100);
				}
			}
		}
	}
}


</script>
</head>
<body>
<div id="contents">
	<input type="hidden" id="MATERIAL" name="MATERIAL"/>
	<input type="hidden" id="TREE" name="TREE"/>
	<input type="hidden" id="CATEGORY" name="CATEGORY"/>
	<input type="hidden" id="CATEGORY_TYPE" name="CATEGORY_TYPE"/>
	<input type="hidden" id="CATEGORY_NAME" name="CATEGORY_NAME"/>
	
	<div class="fl-box panel-wrap03" style="width:15%">
		<h5 class="panel-tit"><spring:message code='epms.object.type' /></h5><!-- 자재유형 -->
		<div class="panel-body">
			<%-- 트리그리드[자재유형] : s --%>
			<div id="categoryList">
				<bdo	Debug="Error"
						Data_Url="/sys/category/categoryData.do?CATEGORY_TYPE=MATERIAL"
						Layout_Url="/sys/category/categoryNotEditLayout.do?CATEGORY_TYPE=MATERIAL"
						Upload_Url="/sys/category/categoryEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"
				>
				</bdo>
			</div>
			<%-- 트리그리드[자재유형] : e --%>
		</div>
	</div>
	
	<div class="fl-box panel-wrap03" style="width:40%;">
		<h5 class="panel-tit mgn-l-10" id="materialListTitle"><spring:message code='epms.object' /></h5><!-- 자재 -->
		<div class="panel-body mgn-l-10">
			<%-- 트리그리드[자재] : s --%>
			<div id="MaterialList2">
				<bdo	Debug="Error"
						Data_Url=""
						Layout_Url="/epms/material/inout/materialInoutMasterLayout.do"
						Upload_Url="/epms/material/inout/materialInoutMasterEdit.do?MATERIAL_INOUT_TYPE=02" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"			
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=자재목록.xls&dataName=data"
				>
				</bdo>
			</div>
			<%-- 트리그리드[자재] : e --%>
		</div>
	</div>
	
	<div class="fl-box panel-wrap04" style="width:45%">
		<h5 class="panel-tit mgn-l-10"><spring:message code='epms.regist.history' /></h5><!-- 등록 내역 -->
		<div class="inq-area-inner type06 ab">
			<div class="wrap-inq">
				<div class="inq-clmn">
					<h4 class="tit-inq">구분</h4>
					<div class="sel-wrap type02">
						<tag:combo codeGrp="MATERIAL_INOUT_TYPE" name="MATERIAL_INOUT_TYPE" choose="Y"/>
					</div>
				</div>
				<div class="inq-clmn" id=materialInOutDate>
					<h4 class="tit-inq" id="materialIn">등록일</h4>
					<div class="prd-inp-wrap">
						<div class="prd-inp">
							<div class="inner">
								<input type="text" id="DATE_INOUT" class="inp-comm datePic inpCal" readonly="readonly" title="등록일" value="${DATE_INOUT}"/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-body mgn-l-10 mgn-t-20">
			<%-- 트리그리드[자재] : s --%>
			<div id="materialInoutReg">
				<bdo	Debug="Error"
		 				Data_Url="/epms/material/inout/materialInoutMngData.do"
						Layout_Url="/epms/material/inout/materialInoutMngLayout.do"
						Upload_Url="/epms/material/inout/materialInoutMngEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"	
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=MaterialInoutRegList.xls&dataName=data"
						>
				</bdo>
			</div>
			<%-- 트리그리드[자재] : e --%>
		</div>
	</div>
</div>
</body>



