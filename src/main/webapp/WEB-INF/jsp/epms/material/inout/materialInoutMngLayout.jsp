<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: materialInoutMngLayout.jsp
	Description : 입출고 등록 - 등록내역 그리드 레이아웃
    author		: 김영환
    since		: 2018.03.21
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자	수정내용
	----------  ------	---------------------------
	2018.03.21	김영환	최초생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="MaterialInoutRegList"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		Filtering="1"				Dragging="0"
			Editing="1"				Deleting="0"		Selecting="1"		SuppressCfg="0"
			CopyCols="0"			PasteFocused="3"	CustomScroll="1"
	/>

	<Header	id="Header"	Align="center"
		MATERIAL_INOUT_TEMP		= "<spring:message code='epms.object.inout.tempId' />"    <%  // 자재입출고임시ID    %>
		MATERIAL				= "<spring:message code='epms.object.id'           />"    <%  // 자재ID"        %>
		MATERIAL_TYPE			= "<spring:message code='epms.object.type'         />"    <%  // 자재유형코드"      %>
		MATERIAL_TYPE_NAME		= "<spring:message code='epms.object.type.name'    />"    <%  // 자재유형"        %>
		MATERIAL_GRP1			= "<spring:message code='epms.object.group1'       />"    <%  // 자재대그룹"       %>
		MATERIAL_NAME			= "<spring:message code='epms.object.name'         />"    <%  // 자재명"         %>
		MODEL					= "<spring:message code='epms.model'               />"    <%  // 모델명"         %>
		SPEC					= "<spring:message code='epms.spec'                />"    <%  // 규격"          %>
		COUNTRY					= "<spring:message code='epms.country'             />"    <%  // 제조국"         %>
		MANUFACTURER			= "<spring:message code='epms.supplier'            />"    <%  // 제조사"         %>
		MEMO					= "<spring:message code='epms.memo'                />"    <%  // 메모"          %>
		STOCK_CONDITION			= "<spring:message code='epms.stock.status'        />"    <%  // 재고상태"        %>
		UNIT					= "<spring:message code='epms.unit'                />"    <%  // 단위"          %>
		STOCK					= "<spring:message code='epms.stock'               />"    <%  // 재고"          %>
		STOCK_OPTIMAL			= "<spring:message code='epms.safety.stock'        />"    <%  // 안전재고"        %>
		PASS_DATE    			= "<spring:message code='epms.exceed.day'          />"    <%  // 경과일"         %>
		LOCATION				= "<spring:message code='epms.location'            />"    <%  // 위치"          %>
		MEMBER					= "<spring:message code='epms.material.MEMBER'        />"    <%  // 등록자ID"       %>
		MEMBER_NAME				= "<spring:message code='epms.material.MEMBER_NAME'   />"    <%  // 등록자"         %>
		QNTY					= "<spring:message code='epms.material.QNTY'          />"    <%  // 수량"          %>
		PRICE					= "<spring:message code='epms.material.UNIT_PRICE'    />"    <%  // 단가"          %>
		MEMBER					= "<spring:message code='epms.material.MEMBER'        />"    <%  // 등록자ID"       %>
		MEMBER_NAME				= "<spring:message code='epms.material.MEMBER_NAME'   />"    <%  // 등록자"         %>
		flag					= "<spring:message code='epms.select.flag'            />"    <%  // 선택구분"        %>
	/>
	
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<Cols>
		<C Name="MATERIAL_INOUT_TEMP"		Type="Text"		Align="center"		Visible="0"		Width="100"	CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="MATERIAL"					Type="Text"		Align="center"		Visible="0"		Width="100"	CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="LOCATION"					Type="Enum"		Align="left"		Visible="0"		Width="100"	CanEdit="0" CanExport="0" CanHide="0" <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>	
		<C Name="MATERIAL_TYPE"				Type="Text"		Align="left"		Visible="0"		Width="70"	CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="MATERIAL_TYPE_NAME"		Type="Text"		Align="left"		Visible="1"		Width="70"	CanEdit="0" CanExport="0" CanHide="0"/> 
		<C Name="MATERIAL_GRP1"				Type="Enum"		Align="left"		Visible="0"		Width="100"	CanEdit="0" CanExport="0" CanHide="0" <tag:enum codeGrp="MATERIAL_GRP1"/>/>	
		<C Name="MATERIAL_NAME"				Type="Text"		Align="left"		Visible="1"		Width="150"	CanEdit="0" CanExport="0" CanHide="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="MODEL"						Type="Text"		Align="left"		Visible="1"		Width="150"	CanEdit="0" CanExport="0" CanHide="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="SPEC"						Type="Text"		Align="left"		Visible="1"		Width="200"	CanEdit="0" CanExport="0" CanHide="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="COUNTRY"					Type="Enum"		Align="left"		Visible="1"		Width="60"	CanEdit="0" CanExport="0" CanHide="0" <tag:enum codeGrp="COUNTRY"/>/>
		<C Name="MANUFACTURER"				Type="Text"		Align="left"		Visible="1"		Width="150"	CanEdit="0" CanExport="0" CanHide="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="MEMO"						Type="Text"		Align="left"		Visible="1"		Width="200"	CanEdit="0" CanExport="0" CanHide="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="UNIT"						Type="Enum"		Align="center"		Visible="1"		Width="60"	CanEdit="0" CanExport="0" CanHide="0" <tag:enum codeGrp="UNIT"/>/>
		<C Name="STOCK_OPTIMAL"				Type="Int"		Align="center"		Visible="1"		Width="60"	CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="STOCK"						Type="Int"		Align="center"		Visible="1"		Width="60"	CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="STOCK_CONDITION"			Type="Enum"		Align="left"		Visible="1"		Width="70"	CanEdit="0" <tag:enum codeGrp="STOCK_CONDITION"/> CanExport="1"/>
		<C Name="PASS_DATE"					Type="Text"		Align="center"		Visible="1"		Width="80"	CanEdit="0"/>
		<C Name="MEMBER"					Type="Text"		Align="center"		Visible="0"		Width="100"	CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="MEMBER_NAME"				Type="Text"		Align="center"		Visible="0"		Width="90"	CanEdit="0" CanExport="0" CanHide="0"/>
	</Cols>
	
	<RightCols>
		<C Name="QNTY"						Type="Int"		Align="center"		Visible="1"		Width="60"	CanEdit="1" CanExport="0" CanHide="0"	 OnChange="changeSts(Grid,Row,Col);" />
		<C Name="PRICE"						Type="Int"		Align="right"		Visible="1"		Width="90"	CanEdit="1"	CanExport="1" Format="#,##0" OnChange="changeSts(Grid,Row,Col);" ExportStyle='mso-number-format:"#,##0";'/>
		<C Name="flag"						Type="Text"		Align="center"		Visible="0"		Width="80"	CanEdit="0" CanExport="0" CanHide="0"/>
	</RightCols>
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,등록,삭제,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        " 
				ColumnsType="Button"
				등록Type="Html" 등록="&lt;a href='#none' title='등록' class=&quot;treeButton treeApproval&quot;
					onclick='fnReg(&quot;MaterialInoutRegList&quot;)'>등록&lt;/p>"
				삭제Type="Html" 삭제="&lt;a href='#none' title='삭제' class=&quot;treeButton treeDelete&quot;
					onclick='fnDel(&quot;MaterialInoutRegList&quot;)'>삭제&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>