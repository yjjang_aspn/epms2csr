<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
	Class Name	: materialOutputListData.jsp
	Description : 자재관리 > 출고현황 조회/관리 > 출고 현황 그리드 데이터
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I	
			MATERIAL_INOUT_TYPE		= "${item.MATERIAL_INOUT_TYPE}"
			<c:if test="${item.CD_MATERIAL_INOUT_TYPE eq 02}">
				DATE_INOUTCanEdit="0"
				QNTYCanEdit="0"
			</c:if>
			MATERIAL_INOUT			= "${item.MATERIAL_INOUT}"
			MATERIAL_INOUT_ITEM		= "${item.MATERIAL_INOUT_ITEM}"
			DATE_INOUT				= "${item.DATE_INOUT}"
			QNTY					= "${item.QNTY}"
			PRICE					= "${item.PRICE}"			
			MATERIAL				= "${item.MATERIAL}"
			MATERIAL_TYPE			= "${item.MATERIAL_TYPE}"
			MATERIAL_TYPE_NAME		= "${item.MATERIAL_TYPE_NAME}"
			MATERIAL_UID			= "${item.MATERIAL_UID}"
			MATERIAL_NAME			= "${fn:replace(item.MATERIAL_NAME, '"', '&quot;')}"
			SPEC					= "${fn:replace(item.SPEC, '"', '&quot;')}"
			UNIT					= "${item.UNIT}"
			STOCK					= "${item.STOCK}"
			STOCK_OPTIMAL			= "${item.STOCK_OPTIMAL}"
			LOCATION				= "${item.LOCATION}"
			MEMBER					= "${item.MEMBER}"
			MEMBER_NAME				= "${item.MEMBER_NAME}"
			CATEGORY_NAME			= "${fn:replace(item.CATEGORY_NAME, '"', '&quot;')}"
			EQUIPMENT_UID			= "${item.EQUIPMENT_UID}"
			EQUIPMENT_NAME			= "${fn:replace(item.EQUIPMENT_NAME, '"', '&quot;')}"
			REPAIR_RESULT			= "${item.REPAIR_RESULT}"
			TARGET_ID				= "${item.TARGET_ID}"
			EQUIPMENT				= "${item.EQUIPMENT}"
			<c:if test="${item.TARGET_TYPE != '' && item.TARGET_TYPE != null}">
				<c:choose>
					<c:when test="${item.TARGET_TYPE eq 'PREVENTIVE'}">
					detailSwitch="1"  detailIcon="/images/com/web/commnet_up.gif" 
					detailOnClickSideIcon="fnOpenLayerWithParam('/epms/preventive/result/popPreventiveResultForm.do?PREVENTIVE_RESULT='+Row.TARGET_ID,'popPreventiveResultForm');"
					</c:when>
					<c:when test="${item.TARGET_TYPE eq 'REPAIR' and item.REPAIR_RESULT != null}">
					detailSwitch="1"  detailIcon="/images/com/web/commnet_up.gif" 
					detailOnClickSideIcon="fnOpenLayerWithParam('/epms/repair/result/popRepairResultForm.do?REPAIR_RESULT='+Row.REPAIR_RESULT+'&EQUIPMENT='+Row.EQUIPMENT,'popRepairResultForm');"
					</c:when>
					<c:otherwise>
					</c:otherwise>
				</c:choose>
			</c:if>
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>