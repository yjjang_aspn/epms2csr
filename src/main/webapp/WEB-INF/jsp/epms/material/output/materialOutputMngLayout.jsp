<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: materialMasterListLayout.jsp
	Description : 자재 목록 그리드 레이아웃 (자재마스터 조회)
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="MaterialOutList"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		Filtering="1"				Dragging="0"
			Editing="1"				Deleting="0"		Selecting="1"		SuppressCfg="0"
			Paging="1"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="0"			PasteFocused="3"	CustomScroll="1"
	/>

	<Header	id="Header"	Align="center"
		MATERIAL_INOUT_TYPE		= "구분"
		MATERIAL_INOUT			= "자재입출고ID"
		MATERIAL_INOUT_ITEM		= "자재입출고아이템ID"
		DATE_INOUT				= "출고일"
		QNTY					= "수량"
		PRICE					= "금액"
		MATERIAL				= "자재ID"
		MATERIAL_TYPE			= "자재유형ID"
		MATERIAL_TYPE_NAME		= "자재유형"
		MATERIAL_UID			= "자재코드"
		MATERIAL_NAME			= "자재명"
		SPEC					= "규격"
		UNIT					= "단위"
		STOCK					= "현재고"
		STOCK_OPTIMAL			= "안전재고"
		LOCATION				= "위치"
		MEMBER					= "등록자ID"
		MEMBER_NAME				= "등록자"
		CATEGORY_NAME			= "라인"
		EQUIPMENT_UID			= "설비코드"
		EQUIPMENT_NAME			= "설비명"
		detail					= "실적상세"
		SUM						= "합계"
		flag					= "삭제구분"
	/>
	
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
		
	<Cols>
		<C Name="MATERIAL_INOUT_TYPE"		Type="Enum"		Align="left"		Visible="1"		Width="70"	CanEdit="0"	Enum="|출고|임의출고" EnumKeys="|02|04"/>
		<C Name="MATERIAL_INOUT"			Type="Text"		Align="center"		Visible="0"		Width="100"	CanEdit="0"	CanHide="0"/>
		<C Name="MATERIAL_INOUT_ITEM"		Type="Text"		Align="center"		Visible="0"		Width="100"	CanEdit="0"	CanHide="0"/>
		<C Name="MATERIAL"					Type="Text"		Align="center"		Visible="0"		Width="100"	CanEdit="0"/>
		<C Name="LOCATION"					Type="Enum"		Align="left"		Visible="0"		Width="80"	CanEdit="0"	<tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>
		<C Name="MATERIAL_TYPE"				Type="Text"		Align="left"		Visible="0"		Width="75"	CanEdit="0"	CanHide="0"/>
		<C Name="MATERIAL_TYPE_NAME"		Type="Text"		Align="left"		Visible="1"		Width="100"	CanEdit="0"	CanHide="0"/>
		<C Name="MATERIAL_TYPE_NAME"		Type="Text"		Align="left"		Visible="1"		Width="100"	CanEdit="0"	CanHide="0"/>
		<C Name="MATERIAL_NAME"				Type="Text"		Align="left"		Visible="1"		Width="250"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="SPEC"						Type="Text"		Align="left"		Visible="0"		Width="400"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="UNIT"						Type="Enum"		Align="left"		Visible="1"		Width="100"	CanEdit="0"	<tag:enum codeGrp="UNIT"/>/>		
		<C Name="MEMBER"					Type="Text"		Align="left"		Visible="0"		Width="100"	CanEdit="0"/>
		<C Name="MEMBER_NAME"				Type="Text"		Align="left"		Visible="1"		Width="110"	CanEdit="0"/>
		<C Name="CATEGORY_NAME"				Type="Text"		Align="left"		Visible="1"		Width="130"	CanEdit="0"/>
		<C Name="EQUIPMENT_UID"				Type="Text"		Align="left"		Visible="1"		Width="90"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT_NAME"			Type="Text"		Align="left"		Visible="1"		Width="190"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>		
	</Cols>
	
	<RightCols>
		<C Name="detail"					Type="Icon"		Align="center"		Visible="1"		Width="70"	CanEdit="0"	CanExport="1"/>
		<C Name="DATE_INOUT"				Type="Date"		Align="center"		Visible="1"		Width="100"	CanEdit="1"	Format="yyyy/MM/dd"  />
		<C Name="QNTY"						Type="Int"		Align="center"		Visible="1"		Width="60"	CanEdit="1"/>
		<C Name="PRICE"						Type="Int"		Align="right"		Visible="0"		Width="90"	CanEdit="0"	CanExport="1"	Format="#,##0"	ExportStyle='mso-number-format:"#,##0";'/>
		<C Name="flag"						Type="Text"		Align="center"		Visible="0"		Width="70"	CanEdit="0"	CanHide="0"/>
		<C Name="SUM"						Type="Int"		Align="right"		Visible="1"		Width="120"	CanEdit="0"	CanExport="1"	Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' Formula="QNTY*PRICE"/>
	</RightCols>
	
	<Foot>
		<I id="Foot" Calculated="1" Spanned="1" detail="합          계" detailType="Text" detailSpan="5" detailAlign="Center" Class="GOHeaderRow GOHeaderText"
												
			SUMFormula="sum()"
		/>
	</Foot>
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,항목,삭제,저장,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        "
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				삭제Type="Html" 삭제="&lt;a href='#none' title='삭제'  class=&quot;defaultButton01 icon delete&quot;
					onclick='fnDel(&quot;${gridId}&quot;)'>삭제&lt;/p>"
				저장Type="Html" 저장="&lt;a href='#none' title='저장' class=&quot;defaultButton01 icon submit&quot;
					onclick='fnSave(&quot;${gridId}&quot;)'>저장&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton01 icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>