<%@ page language="java" contentType="text/html; charset=UTF-8"	 pageEncoding="UTF-8" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: materialOutputList.jsp
	Description : 자재관리 > 출고현황 조회 메인화면
	author		: 김영환
	since		: 2018.04.04
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.04	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script type="text/javascript">

var focusedRow = null;		<%-- [자재유형] 선택된 ROW --%>
var focusedRow2 = null;		<%-- [자재목록] 선택된 ROW  --%>
var focusedRow3 = null;		<%-- [출고현황] 선택된 ROW  --%>
var callLoadingYn = true;	<%-- 로딩바 호출여부 --%>

$(document).ready(function(){		

	<%-- 레이어팝업 히든 시 초기화 --%>
	$('body').on('hidden.bs.modal', '.modal', function () {
        $('.modal-content').empty();
        $(this).removeData('bs.modal');
      });
	
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function (e) {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>	
	});	
	
	<%-- DatePicker 설정 --%>
	$( ".datePic" ).datepicker({
	   dateFormat   : "yy-mm-dd",
       prevText     : "click for previous months" ,
       nextText     : "click for next months" ,
       showOtherMonths   : true ,
       selectOtherMonths : false
	}).each(function(){
		if($(this).hasClass('thisDay')){
			$(this).val(stDate);
		}
	});
	
	<%-- 검색 시작일이 종료일보다 이후인지 체크 (이후라면, 종료일을 시작일로 변경) --%>
	$('#startDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#endDt').val($('#startDt').val());
		}
	});
	
	<%-- 검색 종료일이 시작일보다 이전인지 체크 (이전이라면, 시작일을 종료일로 변경) --%>
	$('#endDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#startDt').val($('#endDt').val());
		}
	});
	
	<%-- 출고일 from - to 검색 --%>
	<%-- 출고 현황 조회 --%>
	$('#selData').on('click', function(){
		if($("#TREE").val() != '' || $("#MATERIAL").val() != ''){
			fnGetMaterialOutputList();	
		}else{
			alert("자재유형 또는 자재를 선택해주세요");
		}
	});
	
});

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "CategoryList") {		<%-- 1. [자재유형] 클릭시 --%>
	
		if (focusedRow == null || focusedRow.CATEGORY != row.CATEGORY || focusedRow2 != null){
			
			<%-- 1-1. 포커스 --%>
			if(focusedRow != null){
				grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
			}
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			
			focusedRow = row;
			
			<%-- 1-2. 자재 목록 조회 --%>
			$('#CATEGORY').val(row.CATEGORY);			<%-- 카테고리 번호 --%>
			$('#CATEGORY_TYPE').val(row.CATEGORY_TYPE);	<%-- 카테고리 유형 : 'MATERIAL_TYPE' --%>
			$('#CATEGORY_NAME').val(row.CATEGORY_NAME);	<%-- 카테고리 명 : 라인명 --%>
			$('#TREE').val(row.TREE);					<%-- 구조체 --%>
			$('#materialListTitle').html("자재 : " + "[ " + row.CATEGORY_NAME + " ]");
			
			<%-- 1-3. 자재번호 초기화 --%>
			$('#MATERIAL').val("");
			$('#materialOutListTitle').text("출고 현황 : " + "[ " + row.CATEGORY_NAME + " ]");
			
			fnGetMaterialList();
		}
		
	} else if(grid.id == "MaterialList3") {		<%-- 2. [자재] 클릭시 --%>
		
		<%-- 2-1. 포커스 시작 --%>
		if(focusedRow2 != null){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		focusedRow2 = row;
		
		$('#MATERIAL').val(row.MATERIAL);
		
		<%-- 2-2. 출고 현황 조회 --%>
		$('#materialOutListTitle').text("출고 현황 : " + "[ " + row.MATERIAL_NAME + " ]");
		fnGetMaterialOutputList();
		
	} else if(grid.id == "MaterialOutList") {		<%-- 3.[출고현황] 클릭시 --%>
		
		<%-- 3-1. 포커스 --%>
		if(focusedRow3 != null){
			grid.SetAttribute(focusedRow3,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow3 = row;
		
	}  
	
}

<%-- 선택된 자재유형에 해당되는 자재 조회 --%>
function fnGetMaterialList(){
	Grids.MaterialList3.Source.Data.Url = "/epms/material/master/materialMasterListData.do?TREE=" + $('#TREE').val()
										+ "&CATEGORY=" + $('#CATEGORY').val()
										+ "&CATEGORY_TYPE=" + $('#CATEGORY_TYPE').val()
										+ "&editYn=N";
	Grids.MaterialList3.ReloadBody();	
}

<%-- 선택된 자재/자재유형에 해당되는 출고 조회 --%>
function fnGetMaterialOutputList(){
	Grids.MaterialOutList.Source.Data.Url = "/epms/material/output/materialOutputListData.do?MATERIAL=" + $('#MATERIAL').val()
										  + "&TREE=" + $('#TREE').val()							  
										  + "&startDt=" + $('#startDt').val()
										  + "&endDt=" + $('#endDt').val();
	Grids.MaterialOutList.ReloadBody();
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

function fnReload_afterCancel(){
	alert("<spring:message code='epms.repair.complete' />가 취소되었습니다.");  // 정비완료
	fnModalToggle('popRepairResultForm');
	$('#popRepairResultForm').empty();
	Grids.MaterialOutList.ReloadBody();
}

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source,data, func){
	if(grid.id == "MaterialList3"){			<%-- [자재] 목록 조회시 --%>
		if(data != ""){
			callLoadingBar();
			callLoadingYn = false;
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "MaterialList3"){
					fnGetMaterialOutputList();
				}
			}
		}
	}else if(grid.id == "MaterialOutList"){ <%-- [입고현황] 목록 조회시 --%>
		if(data != ""){
			if(callLoadingYn == true){
				callLoadingBar();
				callLoadingYn = false;
			}
		}
		Grids.OnDataReceive  = function(grid, source){
			if(grid.id == "MaterialOutList"){
				setTimeout($.unblockUI, 100);
				callLoadingYn = true;
			}
		}
	}
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

</script>
</head>
<body>
<div id="contents">
	<input type="hidden" id="MATERIAL" name="MATERIAL"/>
	<input type="hidden" id="TREE" name="TREE"/>
	<input type="hidden" id="CATEGORY" name="CATEGORY"/>
	<input type="hidden" id="CATEGORY_TYPE" name="CATEGORY_TYPE"/>
	<input type="hidden" id="CATEGORY_NAME" name="CATEGORY_NAME"/>
	
	<div class="fl-box panel-wrap03" style="width:20%">
		<h5 class="panel-tit">자재유형</h5>
		<div class="panel-body">
			<%-- 트리그리드[자재유형] : s --%>
			<div id="categoryList">
				<bdo	Debug="Error"
						Data_Url="/sys/category/categoryData.do?CATEGORY_TYPE=MATERIAL"
						Layout_Url="/sys/category/categoryNotEditLayout.do?CATEGORY_TYPE=MATERIAL"
						Upload_Url="/sys/category/categoryEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"
				>
				</bdo>
			</div>
			<%-- 트리그리드[자재유형] : e --%>
		</div>
	</div>
	
	<div class="fl-box panel-wrap03" style="width:30%;">
		<h5 class="panel-tit mgn-l-10" id="materialListTitle">자재</h5>
		<div class="panel-body mgn-l-10">
			<%-- 트리그리드[자재] : s --%>
			<div id="MaterialList3">
				<bdo	Debug="Error"
						Data_Url=""
						Layout_Url="/epms/material/output/materialOutputMasterListLayout.do"
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=MaterialList3.xls&dataName=data"
				>
				</bdo>
			</div>
			<%-- 트리그리드[자재] : e --%>
		</div>
	</div>
	
	<div class="fl-box panel-wrap04" style="width:50%">
		<h5 class="panel-tit mgn-l-10" id="materialOutListTitle">출고 현황</h5>
		<div class="inq-area-inner type06 ab">
			<div class="wrap-inq">
				<div class="inq-clmn" id="materialInOutDate">
					<h4 class="tit-inq" id="materialOut">출고일</h4>
					<div class="prd-inp-wrap">
						<span class="prd-inp">
							<span class="inner">
								<input type="text" id="startDt" class="inp-comm datePic inpCal" readonly="readonly" title="출고시작일" value="${startDt}">
							</span>
						</span>	
						<span class="prd-inp">
							<span class="inner">
								<input type="text" id="endDt" class="inp-comm datePic inpCal" readonly="readonly" title="출고종료일" value="${endDt}">
							</span>
						</span>	
					</div>
				</div>
			</div>
			<a href="#none" id="selData" class="btn comm st01">검색</a>
		</div>
		<div class="panel-body mgn-l-10 mgn-t-20">
			<%-- 트리그리드[출고현황] : s --%>
			<div id="materialOutList">
				<bdo	Debug="Error"
		 				Data_Url=""
						Layout_Url="/epms/material/output/materialOutputListLayout.do"
						Upload_Url="/epms/material/output/materialOutputListEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols"	
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=출고현황_${startDt}-${endDt}.xls&dataName=data"
						>
				</bdo>
			</div>
			<%-- 트리그리드[출고현황] : e --%>
		</div>
	</div>
</div>
<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<%-- 첨부파일 조회/수정 --%>
<div class="modal fade modalFocus" id="popFileManageForm"    data-backdrop="static" data-keyboard="false"></div>

<%-- 정비실적조회 --%>
<div class="modal fade modalFocus" id="popRepairResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 예방보전실적조회 --%>
<div class="modal fade modalFocus" id="popPreventiveResultForm" data-backdrop="static" data-keyboard="false"></div>
</body>


