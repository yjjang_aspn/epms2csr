<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: materialOutputMasterListLayout.jsp
	Description : 출고현황조회 - 자재마스터 그리드 레이아웃 (자재마스터 조회)
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="MaterialList3"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		Filtering="1"				Dragging="0"
			Editing="1"				Deleting="0"		Selecting="0"		SuppressCfg="0"
			Paging="1"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="0"			PasteFocused="3"	CustomScroll="1"
	/>

	<Header	id="Header"	Align="center"
		MATERIAL				= "자재ID"
		LOCATION				= "위치"
		MATERIAL_TYPE			= "자재유형ID"
		MATERIAL_TYPE_NAME		= "자재유형"
		MATERIAL_UID			= "자재코드"
		MATERIAL_NAME			= "자재명"
		MODEL					= "모델명"
		SPEC					= "규격"
		COUNTRY					= "제조국"
		MANUFACTURER			= "제조사"
		MEMO					= "메모"
		STOCK_CONDITION			= "재고상태"
		UNIT					= "단위"
		STOCK					= "재고"
		STOCK_OPTIMAL			= "안전재고"
		PASS_DATE    			= "경과일"
		MEMBER					= "등록자ID"
		MEMBER_NAME				= "등록자"
		ATTACH_GRP_NO			= "첨부파일 코드"
		FILE					= "첨부파일"
	/>
	
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
	
	<Cols>
		<C Name="MATERIAL"					Type="Text"		Align="center"		Visible="0"		Width="100"		CanEdit="0"/>
		<C Name="LOCATION"					Type="Enum"		Align="left"		Visible="0"		Width="70"		CanEdit="0"	<tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>	
		<C Name="MATERIAL_TYPE"				Type="Text"		Align="left"		Visible="0"		Width="70"		CanEdit="0"	CanHide="0"	/>
		<C Name="MATERIAL_TYPE_NAME"		Type="Text"		Align="left"		Visible="1"		RelWidth="100"	CanEdit="0"	CanHide="0"	/> 
		<C Name="MATERIAL_UID"				Type="Text"		Align="left"   		Visible="1" 	Width="120" 	CanEdit="0" CanExport="0" CanHide="1"/>
		<C Name="MATERIAL_NAME"				Type="Text"		Align="left"		Visible="1"		Width="180"		CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="MODEL"						Type="Text"		Align="left"		Visible="0"		Width="150"		CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
	
		<C Name="UNIT"						Type="Enum"		Align="left"		Visible="1"		Width="60"	CanEdit="0"	<tag:enum codeGrp="UNIT"/>/> 
		<C Name="STOCK_OPTIMAL"				Type="Int"		Align="center"		Visible="1"		Width="60"	CanEdit="0"/>
		<C Name="STOCK"						Type="Text"		Align="center"		Visible="1"		Width="60"	CanEdit="0"/>
		<C Name="STOCK_CONDITION"			Type="Enum"		Align="left"		Visible="1"		Width="70"	CanEdit="0" Enum='|적정|미만'	EnumKeys='|1|2' CanExport="1"/>
		<C Name="PASS_DATE"					Type="Text"	    Align="center"      Visible="1"     Width="80"  CanEdit="0"/>
		
		<C Name="SPEC"						Type="Text"		Align="left"		Visible="1"		Width="200"		CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="COUNTRY"					Type="Enum"		Align="left"		Visible="0"		Width="60"		CanEdit="0" <tag:enum codeGrp="COUNTRY"/>/>
		<C Name="MANUFACTURER"				Type="Text"		Align="left"		Visible="0"		Width="150"		CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="MEMO"						Type="Text"		Align="left"		Visible="1"		Width="200"		CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		
		<C Name="ATTACH_GRP_NO"				Type="Text"		Align="center"		Visible="0"		Width="80"	CanEdit="0"	CanHide="0"/>
		<C Name="FILE"						Type="Icon"		Align="center"		Visible="0"		Width="70"	CanEdit="0"	CanExport="1"/>
	</Cols>

	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,항목,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        "
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>