<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%--
	Class Name	: repairReviewListData.jsp
	Description : 고장관리-설비보전검토리스트 데이터
    author		: 김영환
    since		: 2018.04.10
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2018.04.10		 김영환		최초 생성

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I	
			EQUIPMENT				= "${item.EQUIPMENT}"
			EQUIPMENT_UID			= "${item.EQUIPMENT_UID}"
			EQUIPMENT_NAME			= "${fn:replace(item.EQUIPMENT_NAME, '"', '&quot;')}"
			MEMO					= "${fn:replace(item.MEMO, '"', '&quot;')}"
			GRADE			       	= "${item.GRADE}"
			LOCATION				= "${item.LOCATION}"
			LINE					= "${item.LINE}"
			LINE_NAME				= "${fn:replace(item.LINE_NAME, '"', '&quot;')}"
			MODEL					= "${fn:replace(item.MODEL, '"', '&quot;')}"
			MANUFACTURER			= "${fn:replace(item.MANUFACTURER, '"', '&quot;')}"
			REPAIR_CNT				= "${item.REPAIR_CNT}"
			REPAIR_TOTAL_CNT		= "${item.REPAIR_TOTAL_CNT}"
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>