<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>

<!-- 자재관리 : 자재정보관리 (자재등록 팝업) -->
<script type="text/javascript">
var submitBool = true;
var maxFileCnt = 30;

$(document).ready(function(){
	
	$('#popTitle').html("고장원인분석 기준정보");
	
	<%-- 등록버튼 --%>
	$('#pop_regBtn').on('click', function(){
		if(submitBool){
			fnValidate();	
		}else{
			alert("등록중입니다. 잠시만 기다려 주세요.");
			return false;
		}
	});
	
});

function fnValidate(){
	submitBool = false;
	
	if( !isNull_J($('#PERIOD'), '기간(일)을 입력해주세요.') ) {
		submitBool = true; 
		return false;
	}
	if( !isNull_J($('#FREQUENCY'), '횟수를 입력해주세요.') ){
		submitBool = true; 
		return false;
	}
	
	var form = new FormData(document.getElementById('analysisStandardForm'));
	
	$.ajax({
		type: 'POST',
		url: '/epms/repair/review/analysisStandardUpdate.do',
		dataType: 'json',
		data: form,
		processData: false, 
		contentType:false,
		success: function (json) {	
			toggleModal($("#analysisStandardModal"));
			alert("정상적으로 등록되었습니다");
			location.reload();

		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
			loadEnd();
		}
	});
}
</script>

<!-- 자재등록 화면 모달 : s -->
<div class="modal fade" id="analysisStandardModal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root" style="width:450px;"> <!-- 원하는 width 값 입력 -->				
		 <div class="modal-content" style="height:300px;">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">기준정보</h4>
			</div>
		 	<div class="modal-body">
	           	<div class="modal-bodyIn">

					<!-- 페이지 내용 : s -->
					<form id="analysisStandardForm" name="analysisStandardForm"  method="post" enctype="multipart/form-data">	
						<div class="tb-wrap">
							<table class="tb-st">
								<caption class="screen-out">고장원인분석 기준정보</caption>
								<colgroup>
									<col width="50%" />
									<col width="*" />
								</colgroup>
								<tbody>
									<tr>
										<th>기간(일)</th>
										<td>
											<div class="inp-wrap wd-per-100">
												<input type="text" class="inp-comm" title="기간(일)" id="period" name="PERIOD" value="${period}"/>
											</div>
										</td>
									</tr>
									<tr>
										<th>횟수</th>
										<td>
											<div class="inp-wrap wd-per-100">
												<input type="text" class="inp-comm" title="횟수" id="frequency" name="FREQUENCY" value="${frequency}"/>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</form>
					<!-- 페이지 내용 : e -->
					
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
			<div class="modal-footer hasFooter">
				<a href="#none" class="btn comm st02" data-dismiss="modal" id="cancelBtn">취소</a>
				<a href="#none" class="btn comm st01" id="pop_regBtn">수정</a> 
			</div>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>