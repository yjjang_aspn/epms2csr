<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jstl/fmt"           %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script type="text/javascript" src="/js/jquery/jquery.MultiFile.js"></script>
<script type="text/javascript" src="/js/jquery/jquery-filestyle.js"></script>
<link href="/css/eacc/modalPopup.css" rel="stylesheet" >
<script src="/js/com/web/modalPopup.js"></script>

<!-- 설비관리 - 설비구성 관리(관리자메뉴) -->
<script type="text/javascript">

var row = "";			<%-- 그리드 --%>
var flag = "";			<%-- 1:추가, 2:드래그 --%>
var msg = "";			<%-- 저장 메세지 --%>
var focusedRow = null;	<%-- Focus Row : [설비] --%>
var focusedRow2 = null;	<%-- Focus Row : [정비내역] --%>

$(document).ready(function(){
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function() {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>
	});
	
});

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	
	if(grid.id == "MaintenanceReviewList") {	<%-- [설비] 클릭시 --%>
		
		<%-- 포커스 시작 --%>
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		if(focusedRow != null){
			grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
		}
		focusedRow = row;
		<%-- 포커스 종료 --%>
		
		$('#EQUIPMENT').val(row.EQUIPMENT);
		$('#flag').val(col);
		getRepairResultList();
		
	}
	else if(grid.id == "RepairResultList") { <%-- [정비내역] 클릭시 --%>
		<%-- 포커스 시작 --%>
		if(focusedRow2 != null){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		focusedRow2 = row;
		
	}
	else if(grid.id == "PopRepairAnalysisList") { <%-- [고장원인분석 불러오기] 클릭시 --%>
	
		REPAIR_RESULT = row.REPAIR_RESULT;
		init.getData(row.SAVE_YN, function(result){
			
			init.setData.headData(result.analysisInfo, $("#editForm"));
			init.setData.memberData(result.repairRegMember, $("#editForm"));
			init.setData.itemData(result.analysisItem, $("#editForm"), "edit");
			
		});
		
		fnModalToggle('popRepairAnalysisList');
	}
}

<%-- 선택된 설비에 해당되는 정비내역 조회 --%>
function getRepairResultList(){
	Grids.RepairResultList.Source.Data.Url = "/epms/repair/review/repairResultListData.do?EQUIPMENT=" + $('#EQUIPMENT').val()
										   + "&PERIOD=" + $('#period').val()
										   + "&flag="   + $('#flag').val();
	Grids.RepairResultList.ReloadBody();	
}

<%-- param 전달용 모달 호출 --%>
function layer_open_withParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- 레이어 팝업 토글 --%>
function toggleModal(obj){
	obj.modal('toggle');
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- 확정취소후 모달 닫기 --%>
function fnReload_afterCancel(){
	alert("<spring:message code='epms.repair.complete' />가 취소되었습니다.");  // 정비완료
	fnModalToggle('popRepairResultForm');
	Grids.MaintenanceReviewList.ReloadBody();
	Grids.RepairResultList.ReloadBody();
}

</script>
</head>
<body>

<!-- 화면 UI Area Start  -->
<div id="contents">

<input type="hidden" id="EQUIPMENT" name="EQUIPMENT"/>
<input type="hidden" id="period" 	name="period" 	 value="${period}"/>
<input type="hidden" id="frequency" name="frequency" value="${frequency}"/>
<input type="hidden" id="flag" 		name="flag" 	 value=""/>

<div class="fl-box panel-wrap03" style="width:30%"><!-- 원하는 비율로 직접 지정하여 사용 -->
	<h5 class="panel-tit">최근 ${period}일간 <spring:message code='epms.work' /> ${frequency}회 이상</h5>
	<div class="panel-body">
		<!-- 트리그리드 : s -->
		<div id="MaintenanceReviewList">
			<bdo	Debug="Error"
					Data_Url="/epms/repair/review/repairReviewListData.do"
					Layout_Url="/epms/repair/review/repairReviewListLayout.do"
					
					Export_Data="data" Export_Type="xls"
					Export_Url="/sys/comm/exportGridData.jsp?File=설비별 고장횟수.xls&dataName=data"
					>
			</bdo>
		</div>
		<!-- e:grid-wrap -->
		<!-- 트리그리드 : e -->
	</div>
</div>

<div class="fl-box panel-wrap03" style="width:70%"><!-- 원하는 비율로 직접 지정하여 사용 -->
	<h5 class="panel-tit mgn-l-10"> <spring:message code='epms.work.list' /></h5><!-- 정비내역 -->
	<div class="panel-body mgn-l-10">
		<!-- 트리그리드 : s -->
		<div id="RepairResultList">
			<bdo	Debug="Error"
					Layout_Url="/epms/repair/result/repairResultListLayout.do?flag=maintenanceReview"
					
					Export_Data="data" Export_Type="xls"
					Export_Url="/sys/comm/exportGridData.jsp?File=설비 고장현상.xls&dataName=data"
					>
			</bdo>
		</div>
		<!-- e:grid-wrap -->
		<!-- 트리그리드 : e -->
	</div>
</div>
</div>

<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<%@ include file="/WEB-INF/jsp/epms/repair/review/popAnalysisStandard.jsp"%>	   								<%-- 기준정보 Modal --%>
<%@ include file="/WEB-INF/jsp/epms/repair/result/repairResultCommonModal.jsp"%>								<%-- 정비실적조회/등록 --%>
<div class="modal fade modalFocus" id="popFileManageForm"   data-backdrop="static" data-keyboard="false"></div> <%-- 고장사진보기 팝업 --%>

<%-- 고장원인분석: 불러오기 --%>
<div class="modal fade modalFocus" id="popRepairAnalysisList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-55">		
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">원인분석 목록</h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	           		<div class="modal-section">
						<div class="fl-box panel-wrap-modal wd-per-100">
							<div class="panel-body" id="PopRepairAnalysisList">
								<bdo Debug="Error"
									 Data_Url=""
									 Layout_Url="/epms/repair/analysis/popRepairAnalysisListLayout.do"
								 >
								</bdo>
							</div>
						</div>
					</div> <%-- modal-section : e --%>
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>
</body>
</html>