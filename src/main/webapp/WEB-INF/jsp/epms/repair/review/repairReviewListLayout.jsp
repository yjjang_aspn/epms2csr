<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: repairReviewListLayout.jsp
	Description : 고장관리 - 설비보전검토리스트_그리드 레이아웃
    author		: 김영환
    since		: 2018.03.19
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.19	김영환		최초 생성
	
--%>

<c:set var="gridId" value="MaintenanceReviewList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>

	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1"			SuppressCfg="0"
			Editing="1"				Deleting="0"		Selecting="0"		Dragging="1"
			Filtering="1"			StandardFilter="3"	ClearSelected="2"
			Paging="1"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="0"			PasteFocused="3"
	/>

	<Header	id="Header"	Align="center"
		EQUIPMENT				= "<spring:message code='epms.system.id'        />"    <%  // 설비ID"      %>
		LOCATION				= "<spring:message code='epms.location'      />"    <%  // 위치"        %>
		LINE					= "<spring:message code='epms.category.id'   />"    <%  // 라인ID"      %>
		LINE_NAME				= "<spring:message code='epms.category'      />"    <%  // 라인"        %>
		EQUIPMENT_UID			= "<spring:message code='epms.system.uid'       />"    <%  // 설비코드"      %>
		EQUIPMENT_NAME			= "<spring:message code='epms.system.name'      />"    <%  // 설비명"       %>
		MEMO					= "<spring:message code='epms.memo'          />"    <%  // 메모"        %>
		GRADE					= "<spring:message code='epms.system.grade'     />"    <%  // 설비등급"      %>
		MODEL					= "<spring:message code='epms.model'         />"    <%  // 모델명"       %>
		MANUFACTURER			= "<spring:message code='epms.supplier'      />"    <%  // 제조사"       %>
		REPAIR_TOTAL_CNT		= "<spring:message code='epms.total.occurr.cnt' />"    <%  // 총고장건"      %>
		REPAIR_CNT				= "<spring:message code='epms.urgency.cnt'      />"    <%  // 돌발건"       %>
	/>
	
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	<Pager Visible="0"/>
	
	<Cols>
		<C Name="EQUIPMENT"					Type="Text"		Align="center"		Visible="0"		Width="70"	CanEdit="0"/>
		<C Name="LOCATION"					Type="Enum"		Align="left"		Visible="1"		Width="100"	CanEdit="0" <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>	
		<C Name="LINE"						Type="Text"		Align="center"		Visible="0"		Width="70"	CanEdit="0"/>
		<C Name="LINE_NAME"					Type="Text"		Align="left"		Visible="1"		Width="120"	CanEdit="0"/>
		<C Name="EQUIPMENT_UID"				Type="Text"		Align="left"		Visible="1"		Width="100"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT_NAME"			Type="Text"		Align="left"		Visible="1"		Width="150"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="GRADE"						Type="Enum"		Align="left"		Visible="0"		Width="70"	CanEdit="0"	<tag:enum codeGrp="GRADE"/> />
		<C Name="MODEL"						Type="Text"		Align="left"		Visible="0"		Width="150"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="MANUFACTURER"				Type="Text"		Align="left"		Visible="0"		Width="150"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="MEMO"						Type="Text"		Align="left"		Visible="1"		Width="200"	CanEdit="0"	CaseSensitive="0" WhiteChars=" "/>
	</Cols>
	<RightCols>
		<C Name="REPAIR_CNT"				Type="Text"		Align="center"		Visible="1"		Width="60"	CanEdit="0"/>
		<C Name="REPAIR_TOTAL_CNT"			Type="Text"		Align="center"		Visible="1"		Width="60"	CanEdit="0"/>
	</RightCols>
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,기준정보,항목,새로고침,인쇄,엑셀"
		EmptyType="Html"	EmptyRelWidth="1" Empty="        " 
		ColumnsType="Button"
		CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
		기준정보Type="Html" 기준정보="&lt;a href='#none' title='기준정보' class=&quot;treeButton treeSetting&quot;
						  onclick='fnModalShow(&quot;analysisStandardModal&quot;)'>기준정보&lt;/a>"
		항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
						  onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"								  
		추가Type="Html" 추가="&lt;a href='#none' title='추가' class=&quot;defaultButton01 icon add&quot;
						onclick='fnAddRowGrid(&quot;${gridId}&quot;)'>추가&lt;/a>"
		저장Type="Html" 저장="&lt;a href='#none' title='저장' class=&quot;treeButton treeSave&quot;&quot;
						onclick='fnSave(&quot;${gridId}&quot;)'>저장&lt;/a>"
		새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton01 icon refresh&quot; 
						onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
		인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
						onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
		엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
		
	
</Grid>