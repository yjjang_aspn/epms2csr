<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"           %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<script src="/js/com/web/common.js" type="text/javascript"></script>

<div class="modal-dialog root wd-per-60"> <!-- 원하는 width 값 입력 -->				
	 <div class="modal-content">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title"><spring:message code='epms.repair.cause.analysis' /></h4><!-- 고장원인분석 -->
		</div>
	 	<div class="modal-body overflow rel">
           	<div class="modal-bodyIn hgt-per-100" style="padding:0">
           		
           		<div class="ab" style="top:0; left:0; right:0;">
           			<div class="overflow tabFixedBtn">
	           			<div class="f-l">
	           				<a href="#none" class="btn comm st02" onclick="fnInputValueChk()">이전</a>
	           			</div>
	           			<div class="f-r">
	           				<a href="#none" class="btn comm st01" onclick="">작성</a>
	           				
	           				<a href="#none" class="btn comm st02" onclick="analysisControll.add()">추가</a>
	           				<a href="#none" class="btn comm st02" onclick="">불러오기</a>
	           				<a href="#none" class="btn comm st02" onclick="fnAnaysisSave('N')">임시저장</a>
							<a href="#none" class="btn comm st01" onclick="fnAnaysisSave('Y')">작성완료</a>
	           			</div>
           			</div>
           		</div>
           		
           		<div class="tabFixedBtn_Con">
	           		<div class="tabFixedBtn_ConIn">
		           		<div class="tb-wrap">
							<table class="tb-st">
								<caption class="screen-out"></caption>
								<colgroup>
									<col width="15%" />
									<col width="85%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">제목</th>
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm" name="TITLE" readonly="readonly" value="{{analysisInfo.TITLE}}"/>
											</div>
										</td>
									</tr>
									<tr>
										<th scope="row">분석일</th>
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm inpCal" readonly="readonly" name="DATE_ANALYSIS" value="{{analysisInfo.DATE_ANALYSIS}}">
											</div>
										</td>
									</tr>
									<tr>
										<th scope="row">분석자</th>
										<td>
											<a href="#none" class="btn evn-st01 mgn-t-5" onclick="fnSearchMember()">검색</a>
											<ul class="list-st01 repairMember" id="repairMember">
												<li style="overflow:hidden; margin:5px 0;">
													<input type="hidden" class="inp-comm f-l" title="부서ID" name="" value="{{DIVISION}}" 	readonly="readonly">	
													<div class="box-col wd-per-30 f-l">
														<div class="inp-wrap readonly memberInput wd-per-100">
															<input type="text" class="inp-comm memberInput" title="부서명" name="" value="{{DIVISION_NAME}}"	readonly="readonly">
														</div>
													</div>
													<input type="hidden" class="inp-comm f-l" title="파트" name="" value="{{PART}}"	readonly="readonly">	
													<div class="box-col wd-per-15 f-l">
														<div class="inp-wrap readonly mgn-l-5 memberInput">
															<input type="text" class="inp-comm memberInput" title="파트명" name="" value="{{PART_NAME}}"	readonly="readonly">
														</div>
													</div>
													<input type="hidden" class="inp-comm f-l" title="정비원ID" name="memberArr" value="{{USER_ID}}"	readonly="readonly">
													<div class="box-col wd-per-15 f-l">
														<div class="inp-wrap readonly mgn-l-5 memberInput">
															<input type="text" class="inp-comm memberInput" title="정비원" name="" value="{{MEMBER_NAME}}"	readonly="readonly">
														</div>
													</div>
													<a href="#none" class="btnRemove mgn-l-5" onclick="delList($(this))">삭제</a>
												</li>
											</ul>
										</td>
									</tr>		
									<tr>
										<th scope="row">피해현황</th>
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm" name="DESCRIPTION" readonly="readonly" value="{{analysisInfo.DESCRIPTION}}"/>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						
						<!-- s : 고장원인리스트 -->
						<div class="mgn-t-20">
							<div class="troubleCauseTit">원인대책 리스트</div>
							
							
							<!-- repeat -->
							<div class="tb-wrap mgn-t-10" name="analysisItem">
								<table class="tb-st">
									<colgroup>
										<col width="15%" />
										<col width="*" />
									</colgroup>
									<tbody>
										<tr>
											<th colspan="2" class="troubleCauseListMove">
												<div style="padding-left:5px;">
													<a href="#none" onclick="analysisControll.moveUp(this)" style="padding:0 7px" title="위로 올리기">
														<img src="/images/com/web_v2/up-arrow.png" style="width:17px; heighat:auto;">
													</a>
													<a href="#none" onclick="analysisControll.moveDown(this)" style="padding:0 7px" title="아래로 내리기">
														<img src="/images/com/web_v2/down-arrow.png" style="width:17px; heighat:auto;">
													</a>
													<a href="#none" onclick="analysisControll.remove(this)" style="padding:0 7px" title="삭제">
														<img src="/images/com/web_v2/trash.png" style="width:16px; heighat:auto;">
													</a>
												</div>
											</th>
										</tr>
										<tr>
											<th>원인</th>
											<td>
												<div class="inp-wrap wd-per-100">
													<input type="text" class="inp-comm" name="DESCRIPTION1"/>
												</div>
											</td>
										</tr>
										<tr>
											<th>대책</th>
											<td>
												<div class="inp-wrap wd-per-100">
													<input type="text" class="inp-comm" name="DESCRIPTION2"/>
												</div>
											</td>
										</tr>
										<tr>
											<th>첨부파일</th>
											<td>
												<input type="hidden" name="REPAIR_ANALYSIS_ITEM" value=""/>
												<input type="hidden" name="DEL_SEQ" value=""/>
												<input type="hidden" name="DEL_YN" value="N"/>
												<input type="hidden" name="SEQ_DSP" value=""/>
												<input type="hidden" name="FILE_CNT" value=""/>
												<input type="hidden" name="ATTACH_GRP_NO" value=""/>
												<input type="hidden" name="attachExistYn" value=""/>
												<input type="hidden" name="FILE_LIST_NAME" value=""/>
												<div class="bxType01 fileWrap">
													<div class="filebox"><input type="file" multiple name="fileList"></div>
													<div class="fileList" style="padding:7px 0 5px;">
														<div class="fileDownLst">
														</div>
													</div>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- repeat -->
							
							
						</div>
						<!-- e : 고장원인리스트 -->
						
	           		</div>
	           	</div>
           		
           		
           		
			</div>
		</div>
	</div>		
</div>