<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
	Class Name	: popRepairAnalysisListData.jsp
	Description : 고장관리 > 고장원인분석 > 고장원인분석 불러오기 목록 그리드 데이터
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	김영환		최초 생성

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I		
			REPAIR_ANALYSIS			= "${item.REPAIR_ANALYSIS}"
			REPAIR_RESULT			= "${item.REPAIR_RESULT}"
			TITLE					= "${item.TITLE}"
			DATE_ANALYSIS			= "${item.DATE_ANALYSIS}"
			DESCRIPTION				= "${item.DESCRIPTION}"
			SAVE_YN					= "${item.SAVE_YN}"
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>