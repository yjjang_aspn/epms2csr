<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: popRepairAnalysisListLayout.jsp
	Description : 고장관리 > 고장원인분석 > 고장원인분석 불러오기 목록 그리드 데이터
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="PopRepairAnalysisList"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		Filtering="1"				Dragging="0"
			Editing="1"				Deleting="0"		Selecting="0"		SuppressCfg="0"
			Paging="1"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="0"			PasteFocused="3"	CustomScroll="1"
	/>

	<Header	id="Header"	Align="center"
		REPAIR_ANALYSIS				= "원인분석ID"
		REPAIR_RESULT				= "정비실적ID"
		TITLE						= "제목"
		DATE_ANALYSIS				= "작성일"
		DESCRIPTION					= "피해현황"
		SAVE_YN						= "상태"
	/>
	
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
	
	<Cols>
		<C Name="REPAIR_ANALYSIS"		Type="Text"		Align="left"		Visible="0"		Width="60"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="REPAIR_RESULT"			Type="Text"		Align="left"		Visible="0"		Width="60"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
	 	<C Name="TITLE"					Type="Text"		Align="left"		Visible="1"		Width="200"	CanEdit="0" CaseSensitive="0" WhiteChars=" " RelWidth="300" MinWidth="200"/>
		<C Name="DESCRIPTION"			Type="Text"		Align="left"		Visible="1"		Width="320"	CanEdit="0" CaseSensitive="0" WhiteChars=" " RelWidth="400" MinWidth="300"/>
		<C Name="DATE_ANALYSIS"			Type="Date"		Align="center"		Visible="1"		Width="100"	CanEdit="0" CaseSensitive="0" WhiteChars=" " Format="yyyy/MM/dd"/>
		<C Name="SAVE_YN"				Type="Enum"		Align="center"		Visible="1"		Width="100"	CanEdit="0"	Enum="|작성완료|임시저장" EnumKeys="|Y|N"/>
	</Cols>
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,항목,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        "
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
	
</Grid>