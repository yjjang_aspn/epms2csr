<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: repairAnalysisList.jsp
	Description : 고장관리 > 고장원인분석
	author		: 김영환
	since		: 2018.04.24
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.24	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<style>
	.btnRemove{display:inline-block; height:28px; line-height:28px; padding:0 10px; background:#6eb4e5; color:#ffffff;}
</style>
<script src="/js/com/web/mustache.min.js"></script>
<script type="text/javascript">

<%-- 전역변수 --%>
var msg = "";				<%-- 결과 msg --%>
var focusedRow = null;		<%-- Focus Row : [고장원인분석] --%>

$(document).ready(function(){
	
	<%-- 검색 시작일이 종료일보다 이후인지 체크 (이후라면, 종료일을 시작일로 변경) --%>
	$('#startDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#endDt').val($('#startDt').val());
		}
	});
	
	<%-- 검색 종료일이 시작일보다 이전인지 체크 (이전이라면, 시작일을 종료일로 변경) --%>
	$('#endDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#startDt').val($('#endDt').val());
		}
	});
	
	$('.datePic').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	});
	
	<%-- 고장원인분석 '검색'버튼 클릭 --%>
	$('#srcBtn').on('click', function(){
		fnListReload();
	}); 

	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function (e) {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>
	});
});

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "RepairAnalysisList") { <%-- [고장원인분석] 클릭시 --%>
		<%-- 포커스 시작 --%>
		if(focusedRow != null){
			grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		focusedRow = row;
		
	}
	else if(grid.id == "PopRepairAnalysisList") { <%-- [고장원인분석 불러오기] 클릭시 --%>
		
		REPAIR_RESULT = row.REPAIR_RESULT;
		REPAIR_ANALYSIS = row.REPAIR_ANALYSIS;
		
		init.getData(row.SAVE_YN, function(result){
			
			init.setData.headData(result.analysisInfo, $("#editForm"));
			init.setData.memberData(result.repairRegMember, $("#editForm"));
			init.setData.itemData(result.analysisItem, $("#editForm"), "edit");
			
		});
		
		REPAIR_ANALYSIS = "";
		
		fnModalToggle('popRepairAnalysisList');
	}
	
}

<%-- 목록 초기화 --%>
function fnListReload() {
	Grids.RepairAnalysisList.Source.Data.Url = "/epms/repair/analysis/repairAnalysisListData.do?"
											+ "startDt=" + $('#startDt').val()
											+ "&endDt=" + $('#endDt').val();
	Grids.RepairAnalysisList.ReloadBody();
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source,data, func){
	if(grid.id == "RepairAnalysisList"){
		if(data != ""){
			callLoadingBar();
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "RepairAnalysisList"){
					setTimeout($.unblockUI, 100);
				}
			}
		}
	}
}

function fnReload_afterCancel(){
	alert("<spring:message code='epms.repair.complete' />가 취소되었습니다.");  // 정비완료
	fnModalToggle('popRepairResultForm');
	Grids.RepairAnalysisList.ReloadBody();
}

</script>
</head>
<body>
<div id="contents">
	<div class="fl-box panel-wrap04" style="width:100%;">
		<h5 class="panel-tit" id="repairAnalysisListTitle"><spring:message code='epms.repair.cause.analysis' /></h5><!-- 고장원인분석 -->
		<div class="inq-area-inner type06 ab">
			<div class="wrap-inq">
				<div class="inq-clmn">
					<h4 class="tit-inq">검색일</h4>
					<div class="prd-inp-wrap">
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic thisMonth inpCal" readonly="readonly" id="startDt" title="검색시작일" value="${startDt}">
							</span>
						</span>
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic inpCal" readonly="readonly" id="endDt" title="검색종료일" value="${endDt}">
							</span>
						</span>		
					</div>
				</div>
			</div>
			<a href="#none" id="srcBtn" class="btn comm st01">검색</a>
		</div>
		<div class="panel-body mgn-t-20">
			<div id="RepairAnalysisList">
				<bdo	Debug="Error"
						Data_Url="/epms/repair/analysis/repairAnalysisListData.do?startDt=${startDt}&endDt=${endDt}"
						Layout_Url="/epms/repair/analysis/repairAnalysisListLayout.do?"
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=고장원인분석 목록.xls&dataName=data">
				</bdo>
			</div>
		</div>
	</div>
</div> <%-- contents 끝 --%>

<%-- 정비실적조회 --%>
<div class="modal fade modalFocus" id="popRepairResultForm" data-backdrop="static" data-keyboard="false"></div>							

<%-- 정비원 검색 --%>
<div class="modal fade modalFocus" id="popWorkerList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-25">		
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">
                    <spring:message code='epms.worker' /> <spring:message code='epms.search' />
                </h4><!-- 정비원 검색 -->
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	           		<div class="modal-section">
						<div class="fl-box panel-wrap-modal wd-per-100">
							<div class="panel-body" id="snsList">
								<bdo Debug="Error"
									 Data_Url=""
									 Layout_Url="/epms/repair/result/popWorkerListLayout.do"
								 >
								</bdo>
							</div>
						</div>
					</div> <%-- modal-section : e --%>
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>

<%-- 고장원인분석 불러오기 --%>
<div class="modal fade modalFocus" id="popRepairAnalysisList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-55">		
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">원인분석 목록</h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	           		<div class="modal-section">
						<div class="fl-box panel-wrap-modal wd-per-100">
							<div class="panel-body" id="PopRepairAnalysisList">
								<bdo Debug="Error"
									 Data_Url=""
									 Layout_Url="/epms/repair/analysis/popRepairAnalysisListLayout.do"
								 >
								</bdo>
							</div>
						</div>
					</div> <%-- modal-section : e --%>
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>

<%-- 첨부파일 조회/수정 --%>
<div class="modal fade modalFocus" id="popFileManageForm"   data-backdrop="static" data-keyboard="false"></div>		
