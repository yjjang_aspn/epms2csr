<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%--
	Class Name	: repairAnalysisListData.jsp
	Description : 고장관리 > 고장원인분석
	author		: 김영환
	since		: 2018.04.24
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.24	 김영환		최초 생성

--%>
<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I
			REPAIR_RESULT			= "${item.REPAIR_RESULT}"
			
			LOCATION				= "${item.LOCATION}"
			CATEGORY				= "${item.CATEGORY}"
			CATEGORY_NAME			= "${fn:replace(item.CATEGORY_NAME, '"', '&quot;')}"
			EQUIPMENT				= "${item.EQUIPMENT}"
			EQUIPMENT_UID			= "${item.EQUIPMENT_UID}"
			EQUIPMENT_NAME			= "${fn:replace(item.EQUIPMENT_NAME, '"', '&quot;')}"
			GRADE			       	= "${item.GRADE}"
			MODEL					= "${fn:replace(item.MODEL, '"', '&quot;')}"
			MANUFACTURER			= "${fn:replace(item.MANUFACTURER, '"', '&quot;')}"
			
			REPAIR_REQUEST			= "${item.REPAIR_REQUEST}"
			URGENCY					= "${item.URGENCY}"
			<c:if test="${item.URGENCY == '02'}">
				URGENCYClass="gridStatus4"
				Background="#FFDDDD"
			</c:if>
			REQUEST_TYPE			= "${item.REQUEST_TYPE}"
			REQUEST_TYPE2			= "${item.REQUEST_TYPE2}"
			PART					= "${item.PART}"
			REQUEST_DESCRIPTION		= "${fn:replace(item.REQUEST_DESCRIPTION, '"', '&quot;')}"
			ATTACH_GRP_NO1			= "${item.ATTACH_GRP_NO1}"
			<c:if test='${item.FILE_CNT1>0 || editYn eq "Y"}'>
				file1Switch="1"  file1Icon="/images/com/web/commnet_up3.gif" 
				file1OnClickSideIcon="fnOpenLayerWithParam('/attach/popFileManageForm.do?REPAIR_REQUEST='+Row.REPAIR_REQUEST+'&ATTACH_GRP_NO='+Row.ATTACH_GRP_NO1+'&editYn='+'${editYn}'+'&flag=repair_request','popFileManageForm');"
			</c:if>
			REQUEST_DIV				= "${item.REQUEST_DIV}"
			REQUEST_DIV_NAME		= "${item.REQUEST_DIV_NAME}"
			REQUEST_MEMBER			= "${item.REQUEST_MEMBER}"
			REQUEST_MEMBER_NAME		= "${item.REQUEST_MEMBER_NAME}"
			DATE_REQUEST			= "${item.DATE_REQUEST}"
			APPR_MEMBER				= "${item.APPR_MEMBER}"
			APPR_MEMBER_NAME		= "${item.APPR_MEMBER_NAME}"
			DATE_APPR				= "${item.DATE_APPR}"
			
			REPAIR_TYPE				= "${item.REPAIR_TYPE}"
			REPAIR_STATUS			= "${item.REPAIR_STATUS}"
			<c:if test="${item.REPAIR_STATUS == '03'}">
				REPAIR_STATUSClass="gridStatus3"
			</c:if>
			DATE_ASSIGN				= "${item.DATE_ASSIGN}"
			DATE_PLAN				= "${item.DATE_PLAN}"
			DATE_REPAIR				= "${item.DATE_REPAIR}"
			TROUBLE_TYPE1			= "${item.TROUBLE_TYPE1}"
			TROUBLE_TYPE2			= "${item.TROUBLE_TYPE2}"
			MANAGER					= "${item.MANAGER}"
			MANAGER_NAME			= "${item.MANAGER_NAME}"
			OUTSOURCING				= "${fn:replace(item.OUTSOURCING, '"', '&quot;')}"
			REPAIR_MEMBER			= "${fn:replace(item.REPAIR_MEMBER_NAME, '"', '&quot;')}"
			MATERIAL_USED			= "${fn:replace(item.MATERIAL_USED_NAME, '"', '&quot;')}"
			MATERIAL_UNREG			= "${fn:replace(item.MATERIAL_UNREG, '"', '&quot;')}"
			TROUBLE_TIME1			= "${item.TROUBLE_TIME1}"
			TROUBLE_TIME2			= "${item.TROUBLE_TIME2}"
			trouble_time_hour		= "${item.TROUBLE_TIME_HOUR}"
			trouble_time_minute		= "${item.TROUBLE_TIME_MINUTE}"
			REPAIR_DESCRIPTION		= "${fn:replace(item.REPAIR_DESCRIPTION, '"', '&quot;')}"
			ATTACH_GRP_NO2			= "${item.ATTACH_GRP_NO2}"
			<c:if test='${item.FILE_CNT2>0 || editYn eq "Y"}'>
				file2Switch="1"  file2Icon="/images/com/web/commnet_up3.gif" 
				file2OnClickSideIcon="fnOpenLayerWithParam('/attach/popFileManageForm.do?REPAIR_RESULT='+Row.REPAIR_RESULT+'&ATTACH_GRP_NO='+Row.ATTACH_GRP_NO2+'&editYn='+'${editYn}'+'&flag=repair_result','popFileManageForm');"
			</c:if>
			
			<c:if test='${item.REPAIR_STATUS eq "03" && item.REPAIR_TYPE eq "110"}'>
				analysisSwitch="1"  analysisIcon="/images/com/web/commnet_up.gif" 
				analysisOnClickSideIcon="fnOpenLayerWithParam('/epms/repair/result/popRepairResultForm.do?REPAIR_RESULT='+Row.REPAIR_RESULT+'&EQUIPMENT='+Row.EQUIPMENT+'&PAGE=analysis','popRepairResultForm');"
			</c:if>
			
			REPAIR_ANALYSIS			= "${item.REPAIR_ANALYSIS}"
			SAVE_YN					= "${item.SAVE_YN}"
			<c:if test="${item.SAVE_YN == 'Y'}">
				SAVE_YNClass="gridStatus3"
			</c:if>
			<c:if test="${item.SAVE_YN == 'N'}">
				SAVE_YNClass="gridStatus4"
			</c:if>
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>