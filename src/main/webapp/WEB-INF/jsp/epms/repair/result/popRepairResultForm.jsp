<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: popRepairResultForm.jsp
	Description : 고장관리 > 고장등록 > 정비이력/내 정비요청 현황 > 실적상세 레이어 팝업
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성
	2018.12.20	 김영환		정비원 정비실적 ITEM 추가

--%>
<%@ include file="/com/codeConstants.jsp"%>
<script src="/js/com/web/common.js" type="text/javascript"></script>
<script src="/js/com/web/accordion.js" type="text/javascript"></script>

<script type="text/javascript">
var submitBool = true;		<%-- 유효성검사 --%>
var maxFileCnt = 30;		<%-- 파일 최대갯수 --%>
var msg = "";				<%-- 리턴 메시지 --%>
var REPAIR_RESULT = "${repairResultDtl.REPAIR_RESULT}";
var REPAIR_ANALYSIS = "";
var init = {
		view : function () { <%--조회화면 그리기  --%>
		changeFlag = false;
			this.getData("Y", function(result){
				init.setData.headData(result.analysisInfo, $("#viewForm"))
				init.setData.memberData(result.repairRegMember, $("#viewForm"))
				init.setData.itemData(result.analysisItem, $("#viewForm"))
				
				<%-- 조회화면에서 분석자 목록 삭제버튼 지우기  --%>
	   			$("#viewForm").find(".btnRemove").remove();
			});
		}
		
		, edit : function (STATUS) { <%-- 수정, 등록화면 그리기  --%>
			fnFormToggle();
			this.getData("N", function(result) {
				<%-- 수정화면 Template 비워줌  --%>
				$("#editForm input").val("");
				$("#editForm [name=repairMember]").empty();
				
				<%-- 삭제된 아이템 리스트 비워줌  --%>
				$('#removeItemList').empty();
				
				fileVal = "";
				
				var endDt = result.endDt
				
				<%-- 임시저장된 내용 존재여부 체크 --%>
				if (result["analysisInfo"] != null) {
					$("#REPAIR_ANALYSIS").val(result.analysisInfo.REPAIR_ANALYSIS);
					
					<%-- 임시저장건이 있으면 update 시켜주기 위해 flag값 변경 및 REPAIR_ANALYSIS 값 세팅 --%>
					flag = true;
					var date = result.analysisInfo.UPD_DT;
					var user = result.analysisInfo.USER_NM
					
					<%-- 임시저장 불러오기 취소하면 빈폼 띄우기 --%>
					if(!confirm(date + "에 " + user + "님이 임시저장 하신 내용이 있습니다 불러오시겠습니까?")){
						result = {};
					} 
				} 
				<%-- 수정화면에 Template 그려줌   --%>
				init.setData.headData(result.analysisInfo, $("#editForm"));
				init.setData.memberData(result.repairRegMember, $("#editForm"));
				init.setData.itemData(result.analysisItem, $("#editForm"), "edit");
				
				
				
				<%-- 임시저장 불러온게 아니면 현재날짜 넣어주기 --%>
	   			if(result.analysisInfo == null) { 
					$("#editForm").find("input[name=DATE_ANALYSIS]").val(endDt);
	   			}
				
				<%-- 정렬 순서 초기화 --%>
				analysisControll.sorting();
				
				$('#editForm input:file').each(function(i, o) {
					$(this).attr("name", "fileList" + (i+1)).MultiFile({
							accept : 'ppt |pps |pptx |ppsx |pdf |hwp |xls |xlsx |xlsm | xlt |doc |dot |docx |docm |txt |gif |jpg |jpeg |png'
						,	list   : $(this).closest("div.fileWrap").find('.fileDownLst')
						,	max    : maxFileCnt
						,	STRING : {
							remove: '<img src="/images/com/web/btn_remove.png">'
								   +'<input type="hidden" name="fileGb"  />',
							text  : '<input type="text" class="fileExp" name="fileExp"/>',
							denied:'$ext는(은) 업로드할 수 없는 파일 확장자입니다.'
						},
					});
				});
			});	
				
		}
		, getData : function(SAVE_YN, successCallback) {<%-- 고장원인분석 데이터 호출 --%>
			$.ajax({
				url: '/epms/repair/analysis/popRepairAnalysisInfo.do?REPAIR_RESULT=' + REPAIR_RESULT + '&SAVE_YN=' + SAVE_YN  + '&REPAIR_ANALYSIS=' + REPAIR_ANALYSIS  
	            , type: "get"
	            , dataType: "json"
	            , async: false
	            , success: function (result) {
	            	successCallback(result);
	       		}
	       	 	, error: function (XMLHttpRequest, textStatus, errorThrown) {
	            	alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
	        	}
	       });
		}
		, setData : {
			headData : function(_data, _form) {
				for(var name in _data) {
					switch (name) {
						default :
							_form.find("[name=" + name + "]").val(_data[name]);
							break;
					}
				}
			}
			, memberData : function(_data, _form, _type) {
				
				_form.find("[name=repairMember]").html('');
				$.each(_data, function(i, obj) {
					
					if(_form.attr("id") == "viewForm") var memberTemplate = $("#viewMemberTemplate").html();
					else if (_form.attr("id") == "editForm") var memberTemplate = $("#memberTemplate").html();
					
					memberTemplate = $(memberTemplate);
						for (var name in obj) {
							switch (name) {
								case "USER_ID" :
									memberTemplate.find("[name=memberArr]").val(obj[name]);
									break;
									
								case "MEMBER_NAME" : 
									memberTemplate.find("[name=memberArr]").after(obj[name]);
									break;
									
								case "DIVISION_NAME" :
									memberTemplate.find("[name=DIVISION]").after(obj[name]);
									break;
									
								case "PART_NAME" : 
									memberTemplate.find("[name=PART]").after(obj[name]);
									break;
									
								case "MEMBER_FILEURL" : 
									memberTemplate.find("[name=MEMBER_FILEURL]").attr("src", obj[name]);
									break;
									
								default :
									memberTemplate.find("[name=" + name + "]").val(obj[name]);
									break;
							}
						}
						_form.find("[name=repairMember]").append(memberTemplate);
				});
			}
			, itemData : function(_data, _form, _type) {
				_form.find("[name=analysisItem]").children().not(".troubleCauseTit").remove();
				
				if(!_data || _data.length == 0) 
					_form.find(".troubleCauseTit").hide();
					
				if(_type == "edit") {
					$.each(_data, function(i, obj) {
						var itemTemplate = $("#addItemTemplate").html()
						itemTemplate = $(itemTemplate);
						
							for (var name in obj) {
								switch (name) {
									case "FILE_CNT" :
										if(obj[name] != 0) {
											$.each(obj["ATTACH_LIST"], function(i, o){
												var fileTemplate = $("#fileTemplate3").html();
												fileTemplate = $(fileTemplate);
												for (var name in o) {
													switch (name) {
														case  "ATTACH" :
															fileTemplate.attr("id", "fileRegLstWrap_" + o[name]);
															fileTemplate.find("a.icn_fileDel").attr("data-attach", o[name]).attr("data-idx", o["CD_FILE_TYPE"]);									
															fileTemplate.find("a.btnFileDwn").attr("data-attach", o[name]).attr("data-attachgrpno", o["ATTACH_GRP_NO"]);									
															break;
														case  "NAME" :
															fileTemplate.find("span.MultiFile-title").text(o[name] + " (" + o["FILE_SIZE_TXT"] + ")");					
															break;
														default :
															fileTemplate.find("[name=" + name + "]").text(o[name]);
															break;
													}
												}
												itemTemplate.find('.fileDownLst').append(fileTemplate);
											});
										} 
										break;
									default :
										itemTemplate.find("[name=" + name + "]").val(obj[name]);
										break;
								}
							}
							_form.find("[name=analysisItem]").append(itemTemplate);
							
					});
				} else {
					$.each(_data, function(i, obj) {
						var itemTemplate = $("#viewItemTemplate").html()
						itemTemplate = $(itemTemplate);
							for (var name in obj) {
								switch (name) {
									case "FILE_CNT" :
										if(obj[name] != 0) {
											$.each(obj["ATTACH_LIST"], function(i, o){
												var fileTemplate1 = $("#fileTemplate1").html();
												var fileTemplate2 = $("#fileTemplate2").html();
												fileTemplate1 = $(fileTemplate1);
												fileTemplate2 = $(fileTemplate2);
												for (var name in o) {
													
													switch (name) {
									 				case "ATTACH":
									 					fileTemplate1.attr("id", "fileLstWrap_"+ o[name]);
									 					fileTemplate1.find("a.btnFileDwn").attr("data-attach", o[name]);
									 					break;
									 				case "SEQ_DSP":
									 					fileTemplate1.find("span.MultiFile-export").text(o[name]);
									 					break;
									 				case "NAME":
									 					fileTemplate1.find("span.MultiFile-title").text(o["FILE_NAME"]+"("+o["FILE_SIZE_TXT"]+")");
									 					fileTemplate2.find("p").text(o["FILE_NAME"]+"("+o["FILE_SIZE_TXT"]+")").append('<a href="#none" class="btnFileDwn icon download" data-attach=' + o["ATTACH"]  + '>다운로드</a>');
									 					break;
									 				case "ATTACH_GRP_NO":
									 					fileTemplate2.find("img").attr("src", "/attach/fileDownload.do?ATTACH_GRP_NO="+o[name]+"&ATTACH="+o["ATTACH"]);
									 					fileTemplate2.find("img").attr("onclick", "originFileView('/attach/fileDownload.do?ATTACH_GRP_NO="+o[name]+"&ATTACH="+o["ATTACH"]+"')");
									 					break;
									 				case "FILE_NAME":
									 					fileTemplate2.find("a.btnFileDwn").attr("data-attach", o["ATTACH"]);
									 					break;
								 					}
													itemTemplate.find('[name=defalutFileList]').append(fileTemplate1);
										            if(o["FORMAT"] == 'jpg' || o["FORMAT"] == 'png' || o["FORMAT"] == 'jpeg' || o["FORMAT"] == 'JPG' || o["FORMAT"] == 'PNG'){
										            	itemTemplate.find('[name=previewFileList]').append(fileTemplate2);
										            }
										            itemTemplate.find("[name=fileNull]").remove();
												}
											});
										} else {
											itemTemplate.find("td[name=itemFileList]").children().not("[name=fileNull]").remove();
										}
						
										break;
									default :
										itemTemplate.find("[name=" + name + "]").val(obj[name]);
										break;
								}
							}
							_form.find("[name=analysisItem]").append(itemTemplate);
					});
				}
			}
		}
	}


$(document).ready(function(){
	
	$('.panel.type02').each(function () {
		if($(this).find(".tabFixedBtn").length){
			$(this).find(".tabFixedBtn_Con").css({ 'height': 'calc(100% - 51px)'});
		}else {
			$(this).find(".tabFixedBtn_Con").css({ 'height': '100%'});
		}
	});
	
	$( ".datePic1" ).datepicker({
		   dateFormat   : "yy-mm-dd",
	       prevText     : "click for previous months" ,
	       nextText     : "click for next months" ,
	       showOtherMonths   : true ,
	       selectOtherMonths : false
		}).each(function(){
			if($(this).hasClass('thisDay')){
				$(this).val(stDate);
			}
	});
	
	if("${repairResultDtl.APPR_STATUS}" == "01"){
		$("#repair_status_name").val("대기");
		
	} else if("${repairResultDtl.APPR_STATUS}" == "09"){
		$("#repair_status_name").val("요청취소");
	}
	
	<%-- 돌발정비일 경우에 고장원인분석 탭 보이기  --%>
	if("${repairResultDtl.REPAIR_TYPE}" == "110"){
		init.view();
	}
	
	<%-- 돌발정비 탭 클릭 --%>
	$("#analysisTab").on('click', function(){
		init.view();
	});
	
	<%-- input, select에 change event가 일어날 경우 --%>
	$("#editForm").on('change', 'input, select', function(){
		change = true;
	});
	
	<%-- 고장/정비 시작/종료시간 기본값 세팅 --%>
	fnSetDefaultTime();

	<%-- 첨부파일 관련 --%>
	for(var i=0; i<1; i++){				<%-- 파일 영역 추가를 고려하여 개발 --%>
		$('#attachFile').append('<input type="file" multiple id="fileList" name="fileList">');	
		
		var fileType = 'jpeg |gif |jpg |png |bmp';
	
		$('#fileList').MultiFile({
			   accept :  fileType
			,	list   : '#detailFileList'+i
			,	max    : 5
			,	STRING : {
					remove: '<img src="/sys/images/common/btn_remove.png">'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
					text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" />',
					idx	  : i
				},
		});
		
		<%-- 첨부한 파일의 출력 순서를 자동으로 샛팅 하기 위한 스크립트 - 공통 스크립트로 빠지면 안됨 .개별 항목으로 처리 필수 --%> 
		$('.MultiFile-wrap').change(function(){
			var fileExpIndex = $(this).closest('.fileWrap').find('.fileList > div').children('div:last-child');
			var	fileExp = fileExpIndex.find('.fileExp');
			fileExp.val(fileExpIndex.index() + 1);
		});
	}
	
	<%-- 정비완료 취소버튼 --%>
	$('#repairCancelBtn').on('click', function(){
		fnRepairCompleteCancel();
	});
	
	<%-- 파일 다운로드 --%>
	$('.btnFileDwn').on('click', function(){
		var ATTACH_GRP_NO = $(this).data("attachgrpno");
		var ATTACH = $(this).data("attach");
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO='+ATTACH_GRP_NO+'&ATTACH='+ATTACH;
		$("#fileDownFrame").attr("src",src);
	});
	
	<%-- 첨부파일 삭제버튼 --%>
	
	
	<%-- 고장 첨부파일 중 사진이 아닐경우 미리보기 숨김 --%>
	var fileList1 = new Array();
	<c:forEach items="${attachList1}" var="item" >
		var obj = new Object();
		
		obj.ATTACH_GRP_NO = "${item.ATTACH_GRP_NO}";
		obj.ATTACH = "${item.ATTACH}";
		obj.FORMAT = "${item.FORMAT}";
		
		fileList1.push(obj);
	</c:forEach>
	var attachImgCnt1 = 0;
	for(var i=0; i<fileList1.length; i++){
		if(fileList1[i].FORMAT == "jpg" || fileList1[i].FORMAT == "png" || fileList1[i].FORMAT == "jpeg" || fileList1[i].FORMAT == "JPG" || fileList1[i].FORMAT == "PNG"){
			attachImgCnt1 ++;
		}
	}	
	
	if(fileList1.length > 0){
		if(attachImgCnt1 == 0){
			$('#break_previewImg').hide();
		}
	}
	
	<%-- 정비 첨부파일 중 사진이 아닐경우 미리보기 숨김 --%>
	var fileList2 = new Array();
	<c:forEach items="${attachList2}" var="item" >
		var obj = new Object();
		
		obj.ATTACH_GRP_NO = "${item.ATTACH_GRP_NO}";
		obj.ATTACH = "${item.ATTACH}";
		obj.FORMAT = "${item.FORMAT}";
		
		fileList2.push(obj);
	</c:forEach>
	var attachImgCnt2 = 0;
	for(var i=0; i<fileList2.length; i++){
		if(fileList2[i].FORMAT == "jpg" || fileList2[i].FORMAT == "png" || fileList2[i].FORMAT == "jpeg" || fileList2[i].FORMAT == "JPG" || fileList2[i].FORMAT == "PNG"){
			attachImgCnt2 ++;
		}
	}	
	
	if(fileList2.length > 0){
		if(attachImgCnt2 == 0){
			$('#repair_previewImg').hide();
		}
	}
	
});

<%-- 고장/정비 시작/종료시간 기본값 세팅 --%>
function fnSetDefaultTime(){
	
	var timeArray = ['TROUBLE_TIME1', 'TROUBLE_TIME2', 'REPAIR_TIME1', 'REPAIR_TIME2'];

	$(timeArray).each(function(idx, val) {
		
		var saveData = "";
		if(val == "TROUBLE_TIME1") saveData = "${repairResultDtl.TROUBLE_TIME1}";
		else if(val == "TROUBLE_TIME2") saveData = "${repairResultDtl.TROUBLE_TIME2}";
		else if(val == "REPAIR_TIME1") saveData = "${repairResultDtl.REPAIR_TIME1}";
		else if(val == "REPAIR_TIME2") saveData = "${repairResultDtl.REPAIR_TIME2}";
		
		<%-- 임시저장된 값이 있다면 처리 --%>
		if(saveData != "" || saveData != null){
			$('#popRepairResultForm_' + val + '_DATE').val(saveData.substring(0,4) + "-" + saveData.substring(4,6) + "-" + saveData.substring(6,8));
			$('#popRepairResultForm_' + val + '_HOUR').val(saveData.substring(8,10));
			$('#popRepairResultForm_' + val + '_MINUTE').val(saveData.substring(10,12));
		}
		
	});
}

<%-- 첨부파일 목록에서 제거 --%>
function delModiFile(obj){
	var attach = obj.data("attach");
	var idx = obj.data('idx');
	var fileVal = obj.parents('td').find("input[name='DEL_SEQ']").val();
	
	if(fileVal != '') {
		fileVal += ",";
	}
	
	fileVal += attach;
	obj.parents('td').find("input[name='DEL_SEQ']").val(fileVal);
	
	obj.parent().parent().remove();
}

<%-- '정비완료'건 '승인'으로 상태값 변경 --%>
function fnRepairCompleteCancel(){
	
	var strComplete = "<spring:message code='epms.repair.complete' />" ;  // 정비완료
	
	if(confirm( strComplete + "된 건을 취소하시겠습니까?")){
		$.ajax({
			type : 'POST',
			url : '/epms/repair/result/updateCompleteStatus.do',
			dataType : 'json',
			data : {"REPAIR_RESULT" : REPAIR_RESULT},
			success : function(json) {	
				
				<%-- 정상적으로 정비실적 등록시 --%>
				//if(json.RES_CD == "${SUCCESS}"){
				if(json.E_RESULT == "S"){
					fnReload_afterCancel();
					submitBool = true;
					
				<%-- 이미 등록된 정비실적일 경우 --%>
				}else{
					//alert("처리도중 에러가 발생하였습니다 . 시스템 관리자에게 문의 하시기 바랍니다");
					alert("SAP처리중 에러가 발생하였습니다 .\n" + json.E_MESSAGE);
					submitBool = true;
					return;
				}
				
				
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
				submitBool = true;
			}
		});
	}
}

//=====================================================================================================================================================
//=====================================================================================================================================================
//=====================================================================================================================================================
//=====================================================================================================================================================
<%-- 수정 폼에서 수정했는지 체크  --%>
var changeFlag = false;

<%-- 임시저장 불러온 후 작성완료인지 체크  --%>
var flag = false;
var analysisControll = {
	add : function (){
		changeFlag = true;
		var addItem = $("#addItemTemplate").html();
		addItem = $(addItem);
		
		$("#editForm .troubleCauseTit").show();
		
		$("#editForm [name=analysisItem]").append(addItem);
		
		var seq = $("input:file").length;
		
		$("input:file:last").attr("name", "fileList" + seq).MultiFile({
				accept : 'ppt |pps |pptx |ppsx |pdf |hwp |xls |xlsx |xlsm | xlt |doc |dot |docx |docm |txt |gif |jpg |jpeg |png'
			,	list   : $("input:file:last").closest(".fileWrap").find(".fileDownLst")
			,	max    : maxFileCnt
			,	STRING : {
					remove: '<img src="/images/com/web/btn_remove.png">'
						   +'<input type="hidden" name="fileGb"  />',
					text  : '<input type="text" class="fileExp" name="fileExp"/>',
					denied:'$ext는(은) 업로드할 수 없는 파일 확장자입니다.'
			},
		});
	}
	, remove : function (obj) {
		changeFlag = true;
		var $div = $(obj).closest("div.tb-wrap");
		$div.find('input[name=DEL_YN]').val('Y');
		
		
		<%-- 삭제 아이템 맨 아래로 옮기기  --%>
		$("#removeItemList").append($div);
		
		<%-- 아이템 삭제 후 남은 아이템이 없으면 타이틀 삭제  --%>
		if($("#editForm [name=analysisItem]").children().length == 1) {
			$("#editForm .troubleCauseTit").hide();
		} 
		
		this.sorting();
	} 
	, moveUp : function (obj) {
		changeFlag = true;
		var $div = $(obj).closest("div.tb-wrap");
		
		<%-- 제일 상단일 경우 안움직이게  --%>
		if($div.prev().hasClass("troubleCauseTit"))
			return;
		
		$div.prev().before($div); 
		
		this.sorting();
	}
	, moveDown : function (obj) {
		changeFlag = true;
		var $div = $(obj).closest("div.tb-wrap");
		
		$div.next().after($div);
		this.sorting();
	}
	, sorting : function () {
		$("#editForm").find("div.tb-wrap").each(function(i, o){
			$(this).find("input[name=SEQ_DSP]").val(i);
		});
	}
}

<%-- 정비원 '검색'버튼 --%>
function fnSearchMember(){
	var location = $('#LOCATION2').val();
	var part = $('#PART2').val();
	var remarks = $('#REMARKS').val();
	var url = '/epms/repair/result/popWorkerListData.do'
			  + '?SUB_ROOT=' + location
			  +	'&PART=' + part
			  +	'&REMARKS=' + remarks;
			  
	Grids.WorkerStatList.Source.Data.Url = url;
	Grids.WorkerStatList.ReloadBody();
	fnModalToggle('popWorkerList');

}


<%-- 정비원추가 버튼 --%>
function fnAddMember(gridNm){
	var selRows = Grids[gridNm].GetSelRows();
	
	if(selRows == null || selRows == ""){
		return;
	}
	
	var html = "";
	for(var i=0; i<selRows.length; i++){
		if(!(selRows[i].USER_ID=="" || selRows[i].USER_ID==null)){
			if($("#analysisMember").html().indexOf('name="memberArr" value="'+selRows[i].USER_ID+'"') < 0){
				changeFlag = true;
				html +=
					'<li style="overflow:hidden;">'
				+	'	<div class="repairMemberTit">'
				+	'		<div class="repairMemberTitIn">'
				+	'			<div class="userInfo">'
				+	'				<div class="user_img">'
				+	'					<img src="' + selRows[i].MEMBER_FILEURL + '" onerror="this.src=\'/images/com/web_v2/userIcon.png\'" />'
				+	'				</div>'
				+	'				<div class="user_name">'
				+	'					<div class="name">'			
				+	'						<input type="hidden" class="inp-comm" title="정비원ID" name="memberArr" value="' + selRows[i].USER_ID + '"	readonly="readonly">'
				+	'						' + selRows[i].NAME + ''
				+	'						<input type="hidden" class="inp-comm" title="삭제여부" name="delYnArr" value="N"	readonly="readonly">'
				+	'					</div>'
				+	'					<div class="dep">'
				+	'						<span>'
				+	'							<input type="hidden" class="inp-comm" title="부서ID" name="" value="' + selRows[i].DIVISION + '"	readonly="readonly">'
				+	'							' + selRows[i].DIVISION_NAME + ''
				+ 	'						</span>'
				+	'						<span style="padding:0 5px;">/</span>'
				+	'						<span>'
				+	'							<input type="hidden" class="inp-comm" title="파트" name="" value="' + selRows[i].PART + '"	readonly="readonly">'
				+	'							' + selRows[i].PART_NAME + ''
				+	'						</span>'
				+	'					</div>'
				+	'				</div>'
				+	'			</div>'
				+	' 			<div class="repairMemberBtn2">'
				+	' 				<a href="#none" class="treeButton repairDelete" onclick="delList1($(this))">삭제</a>'
				+	' 			</div>'
				+	'		</div>'
				+	'	</div>'
				+	'</li>';
			}
			// 삭제된 정비원 다시 추가
			else if($('#analysisMember').html().indexOf('name="memberArr" value="'+selRows[i].USER_ID+'"') > 0){
				var $hide_li = $('#analysisMember').find("input[name=memberArr][value="+selRows[i].USER_ID+"]").closest("li");
				if($hide_li.css("display") == "none"){
				   // 삭제여부 -> "N"으로 변경	
				   $hide_li.find("input[name=delYnArr]").val('N');
				   // show()처리
				   $hide_li.show();
				}
			}
		};
		
	}
	$('#analysisMember').append(html);
	fnModalToggle('popWorkerList');
}

<%-- 정비원 삭제버튼 --%>
function delList1(delLst){
	<%-- 담당자 이름 가져오기 --%>
	var name = delLst.closest('li').find("div.name").text().replace("확정완료", '').replace("담당자", '').trim();	
	
	if(confirm( " '"+name+"' <spring:message code='epms.worker' />을 삭제 하시겠습니까?")){
		delLst.closest('li').find("input[name=delYnArr]").val('Y');
		delLst.closest('li').hide();
		alert("'"+name+"' <spring:message code='epms.worker' />이 삭제되었습니다");
	}
	
}

<%-- 자재 삭제버튼 --%>
function delList(delLst){
	changeFlag = true;
	delLst.closest('li').remove();
}

function fnFormToggle(type){
	if(changeFlag) {
		if(!confirm("변경 사항이 있습니다. 저장하지 않고 조회화면으로 돌아가시겠습니까?")) {
			return;
		}
	}
	
	$("#viewForm").toggle();
	$("#editForm").toggle();
	changeFlag = false;
}

<%-- 작성완료, 임시저장  함수 --%>
function fnAnaysisSave(SAVE_YN){
	
	<%-- 정렬 순서 초기화 --%>
	analysisControll.sorting();
	
	var msg = SAVE_YN == 'Y' ? "작성완료 하시겠습니까?" : "임시저장 하시겠습니까?";
	if(confirm(msg)){
		
		<%-- 작성완료 일때만 validation 체크 --%>
		if(SAVE_YN == "Y") {
			<%-- 제목 입력 체크  --%>
			if (!isNull_J($('#editForm').find('input[name=TITLE]'), '제목을 입력해주세요')) {
				return false;
			}
			
			<%-- 정비원 등록 체크 --%>
			if($('#editForm [name=repairMember]').children("li").length == 0){
				alert("분석자를 등록해주세요");
				return false;
			}
		}
		
		var i = 0;
		$("#editForm").find("div[name=analysisItem]").children().not(".troubleCauseTit").each(function() {
			var fileCnt = $(this).find(".MultiFile-label").length;
			$(this).find('input[name=FILE_CNT]').val(fileCnt);
			$(this).find('input[name=attachExistYn]').val(fileCnt > 0 ? "Y" : "N");
			
			var fileListName = $(this).find("input:file").attr("name");
			$(this).find('input[name=FILE_LIST_NAME]').val(fileListName);
			
			i++;
		});
		$("#ITEM_CNT").val(i);
		
		<%-- 저장하려는 건이  임시저장 불러왔는지 체크--%>
		$("#flag").val(flag == true ? "reg" : "N");
		
		$("#SAVE_YN").val(SAVE_YN);
		
		var form = new FormData(document.getElementById('saveForm'));
		$.ajax(
	        {
	            url: '/epms/repair/analysis/popRepairAnalysisSave.do'
				, type : 'POST'
				, dataType : 'json'
				, data : form
				, async : false
				, processData : false
				, contentType : false
	            , success: function (result) {
	            	alert('저장되었습니다.');
	            	init.view();
	            	changeFlag = false;
	            	fnFormToggle();
	            	flag = false;
	        	}
	            , error: function (XMLHttpRequest, textStatus, errorThrown) {
	            	alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
        		}
       });
	}
}

<%-- 불러오기 목록 --%>
function fnSelectAnalysisList(){
	Grids.PopRepairAnalysisList.Source.Data.Url = "/epms/repair/analysis/popRepairAnalysisListData.do?REPAIR_RESULT=" + REPAIR_RESULT;
	Grids.PopRepairAnalysisList.ReloadBody();
	fnModalToggle('popRepairAnalysisList');
}

<%-- 이미지 원본크기 보기 --%>
function originFileView(img){ 
	img1= new Image(); 
	img1.src=(img); 
	imgControll(img); 
} 
	  
function imgControll(img){ 
	if((img1.width!=0)&&(img1.height!=0)){ 
		viewImage(img); 
	}else{ 
		controller="imgControll('"+img+"')"; 
		intervalID=setTimeout(controller,20); 
	} 
}

function viewImage(img){ 
	W=img1.width; 
	H=img1.height; 
	O="width="+W+",height="+H+",scrollbars=yes"; 
	imgWin=window.open("","",O); 
	imgWin.document.write("<html><head><title>이미지상세보기</title></head>");
	imgWin.document.write("<body topmargin=0 leftmargin=0>");
	imgWin.document.write("<img src="+img+" onclick='self.close()' style='cursor:pointer;' title ='클릭하시면 창이 닫힙니다.'>");
	imgWin.document.close();
}

//=====================================================================================================================================================
//=====================================================================================================================================================
//=====================================================================================================================================================
//=====================================================================================================================================================
//=====================================================================================================================================================
//=====================================================================================================================================================
	


</script>

<style>
	.btnRemove{display:inline-block; height:28px; line-height:28px; padding:0 10px; background:#6eb4e5; color:#ffffff;}
</style>
</head>
<body>
<div class="modal-dialog root wd-per-70">				
	<div class="modal-content">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title"><c:if test="${repairResultDtl.URGENCY eq '02'}"><span style="color:red;font-weight:bold;">[긴급] </span></c:if><spring:message code='epms.repair.perform.detail' /></h4><!-- 정비실적 상세 -->
		</div>
	 	<div class="modal-body overflow">
           	<div class="modal-bodyIn hgt-per-100" style="padding:0;">
				<input type="hidden" id="ATTACH_GRP_NO" value="${repairResultDtl.ATTACH_GRP_NO }">
					<div class="popTabSet">
						<ul class="tabs">
							<c:choose>
								<c:when test="${PAGE eq 'analysis'}">
									<li id="repairRequestTab"><a href="#panel2-1"><spring:message code='epms.request.content' /></a></li><!-- 요청내용 -->
									<li id="repairResultTab"><a href="#panel2-2"><spring:message code='epms.work.perform' /></a></li><!-- 정비실적 -->
									<li id="analysisTab"><a href="#panel2-3" class="on"><spring:message code='epms.repair.cause.analysis' /></a></li><!-- 고장원인분석 -->
								</c:when>
								<c:when test="${repairResultDtl.APPR_STATUS eq '02' && repairResultDtl.REPAIR_STATUS eq '03' && repairResultDtl.REPAIR_TYPE eq '110'}">
									<li id="repairRequestTab"><a href="#panel2-1"><spring:message code='epms.request.content' /></a></li><!-- 요청내용 -->
									<li id="repairResultTab"><a href="#panel2-2" class="on"><spring:message code='epms.work.perform' /></a></li><!-- 정비실적 -->
									<li id="analysisTab"><a href="#panel2-3"><spring:message code='epms.repair.cause.analysis' /></a></li><!-- 고장원인분석 -->
								</c:when>
								<c:when test="${repairResultDtl.APPR_STATUS eq '02' && repairResultDtl.REPAIR_STATUS eq '03'}">
									<li id="repairRequestTab"><a href="#panel2-1"><spring:message code='epms.request.content' /></a></li><!-- 요청내용 -->
									<li id="repairResultTab"><a href="#panel2-2" class="on"><spring:message code='epms.work.perform' /></a></li><!-- 정비실적 -->
								</c:when>
								<c:otherwise>
									<li id="repairRequestTab"><a href="#panel2-1" class="on"><spring:message code='epms.request.content' /></a></li><!-- 요청내용 -->
								</c:otherwise>
							</c:choose>
						</ul>
						<div class="panels">
							<div class="panel type01" id="panel2-1">
								<div class="panel-in rel">
									<div class="tb-wrap">
										<table class="tb-st">
											<caption class="screen-out"><spring:message code='epms.repair.perform.detail' /></caption><!-- 정비실적 상세 -->
											<colgroup>
												<col width="15%" />
												<col width="35%" />
												<col width="15%" />
												<col width="*" />
											</colgroup>
											<tbody>
												<tr>
													<input type="hidden" class="inp-comm" id="REMARKS" name="REMARKS" value="${repairResultDtl.REMARKS}"/>
													<th scope="row"><spring:message code='epms.repair.status' /></th><!-- 정비상태 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="REPAIR_STATUS" name="REPAIR_STATUS" value="${repairResultDtl.REPAIR_STATUS}"/>
															<input type="text" class="inp-comm" id="repair_status_name" name="repair_status_name" readonly="readonly" value="${repairResultDtl.REPAIR_STATUS_NAME}"/>
														</div>
													</td>
													<th scope="row"><spring:message code='epms.repair.urgency' /></th><!-- 긴급도 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="popRepairResultRegForm_URGENCY" name="URGENCY" value="${repairResultDtl.URGENCY}"/>
															<input type="text" class="inp-comm" id="popRepairResultRegForm_urgency_name" name="urgency_name" readonly="readonly" value="${repairResultDtl.URGENCY_NAME}" <c:if test="${repairResultDtl.URGENCY eq '02'}"> style="color:red;font-weight:bold;" </c:if>/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row"><spring:message code='epms.location' /></th><!-- 공장 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="LOCATION2" name="LOCATION" value="${repairResultDtl.LOCATION}"/>
															<input type="text" class="inp-comm" id="location_name" name="location_name" readonly="readonly" value="${repairResultDtl.LOCATION_NAME}"/>
														</div>
													</td>
													<th scope="row"><spring:message code='epms.repair.part' /></th><!-- 처리파트 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="PART2" name="PART" value="${repairResultDtl.PART}"/>
															<input type="text" class="inp-comm" id="part_name" name="part_name" readonly="readonly" value="${repairResultDtl.PART_NAME}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row"><spring:message code='epms.category' /></th><!-- 라인 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="CATEGORY" name="CATEGORY" value="${repairResultDtl.CATEGORY}"/>
															<input type="text" class="inp-comm" id="category_name" name="category_name" readonly="readonly" value="${repairResultDtl.CATEGORY_NAME}"/>
														</div>
													</td>
													<th scope="row"><spring:message code='epms.repair.request.date' /></th><!-- 요청일시 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" id="DATE_REQUEST" name="DATE_REQUEST" readonly="readonly" value="${repairResultDtl.DATE_REQUEST}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row"><spring:message code='epms.system.uid' /></th><!-- 설비코드 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="EQUIPMENT" name="EQUIPMENT" value="${repairResultDtl.EQUIPMENT}"/>
															<input type="text" class="inp-comm" id="EQUIPMENT_UID" name="EQUIPMENT_UID" readonly="readonly" value="${repairResultDtl.EQUIPMENT_UID}"/>
														</div>
													</td>
													<th scope="row"><spring:message code='epms.repair.request.part' /></th><!-- 요청부서 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="REQUEST_DIV" name="REQUEST_DIV" value="${repairResultDtl.REQUEST_DIV}"/>
															<input type="text" class="inp-comm" id="request_div_name" name="request_div_name" readonly="readonly" value="${repairResultDtl.REQUEST_DIV_NAME}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row"><spring:message code='epms.system.name' /></th><!-- 설비명 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" id="EQUIPMENT_NAME" name="EQUIPMENT_NAME" readonly="readonly" value="${repairResultDtl.EQUIPMENT_NAME}"/>
														</div>
													</td>
													<th scope="row"><spring:message code='epms.repair.request' /></th><!-- 요청자 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="REQUEST_MEMBER" name="REQUEST_MEMBER" value="${repairResultDtl.REQUEST_MEMBER}"/>
															<input type="text" class="inp-comm" id="request_member_name" name="request_member_name" readonly="readonly" value="${repairResultDtl.REQUEST_MEMBER_NAME}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row"><spring:message code='epms.repair.content' /></th><!-- 고장내용 -->
													<td colspan="3">
														<div class="txt-wrap readonly">
															<div class="txtArea">
																<textarea cols="" rows="5" title="고장내용" id="REQUEST_DESCRIPTION" name="REQUEST_DESCRIPTION" readonly="readonly">${repairResultDtl.REQUEST_DESCRIPTION}</textarea>
															</div>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row"><spring:message code='epms.repair.manager' /></th><!-- 담당자 -->
													<td colspan="3">
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="MANAGER" name="MANAGER" value="${repairResultDtl.MANAGER}"/>
															<input type="text" class="inp-comm" id="manager_name" name="manager_name" readonly="readonly" value="${repairResultDtl.MANAGER_NAME}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row"><spring:message code='epms.material.FILE' /></th><!-- 고장 첨부파일 -->
													<td colspan="3">
														<c:choose>
															<c:when test="${fn:length(attachList1)>0}">
															<div id="previewList" class="wd-per-100">
																<div class="bxType01 fileWrap previewFileLst">
																	<div>
																		<div class="fileList type02">
																			<div id="detailFileList0" class="fileDownLst">
																				<c:forEach var="item" items="${attachList1}" varStatus="idx">
																					<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																						<span class="MultiFile-export">${item.SEQ_DSP}</span> 
																						<span class="MultiFile-title" title="File selected: ${item.NAME}">
																						${item.FILE_NAME} (${item.FILE_SIZE_TXT})
																						</span>
																						<a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a>
																					</div>		
																				</c:forEach>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
					
															<div id="break_previewImg" class="wd-per-100">
																<div class="previewFile mgn-t-15">
																	<h3 class="tit-cont preview" id="previewTitle">이미지 미리보기</h3>
																	<ul style="margin-top:5px;">
																		<c:forEach var="item" items="${attachList1}" varStatus="idx">
																			<c:if test = "${item.FORMAT eq 'jpg' || item.FORMAT eq 'png' || item.FORMAT eq 'jpeg' || item.FORMAT eq 'JPG' || item.FORMAT eq 'PNG'}">
																				<li>
																					<img src="/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}" title="클릭하시면 원본크기로 보실 수 있습니다." onclick="originFileView('/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}')" />
																					<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																						<span class="MultiFile-title" title="File selected: ${item.NAME}">
																						<p>${item.FILE_NAME} (${item.FILE_SIZE_TXT})</p>
																						</span>
																						<a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a>
																					</div>
																				</li>
																			</c:if>
																		</c:forEach>
																	</ul>
																</div>
															</div>
															</c:when>
															<c:otherwise>
																<div class="f-l wd-per-100 mgn-t-5">
																※ 등록된 첨부파일이 없습니다.
																</div>
															</c:otherwise>
														</c:choose>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<%-- 정비내용 판넬 : s --%>
							<div class="panel type02" id="panel2-2">
								<div class="panel-in">
									<c:if test="${repairResultDtl.REPAIR_STATUS eq EPMS_REPAIR_STATUS_COMPLETE and cancelAuth eq 'Y'}">
										<div class="overflow tabFixedBtn">
											<div class="f-r" id="btns">
												<a href="#none" id="repairCancelBtn" class="btn comm st02">확정취소</a>
											</div>
										</div>
									</c:if>
									
									<div class="tabFixedBtn_Con">
						           		<div class="tabFixedBtn_ConIn">
											<div class="tb-wrap">
												<table class="tb-st">
													<caption class="screen-out"><spring:message code='epms.repair.perform.detail' /></caption><!-- 정비실적 상세 -->
													<colgroup>
														<col width="15%" />
														<col width="35%" />
														<col width="15%" />
														<col width="*" />
													</colgroup>
													<tbody>
														<tr>
															<th scope="row"><spring:message code='epms.worker' /></th><!-- 정비원 -->
															<td colspan="3">
																<div class="repairMemBtnArea overflow">
<!-- 																	<a href="#none" class="btn evn-st01" onclick="fnSearchMember()">정비원 검색</a> -->
																	
																	<div class="f-r mgn-t-5">
																		<a href="javascript:repairMemAccSlider.pr(1)">전체 열기</a> | <a href="javascript:repairMemAccSlider.pr(-1)">전체 닫기</a>
																	</div>
																</div>
																	
																<ul class="repairMemberList type02 repairMember" id="repairMember">
																	<c:forEach var="item" items="${repairRegMember}">
																		<li>
																			<div class="repairMemberTit">
																				<div class="repairMemberTitIn">
																					<div class="userInfo">
																						<div class="user_img">
																							<img src="${item.MEMBER_FILEURL}" onerror="this.src='/images/com/web_v2/userIcon.png'" />
																						</div>
																						<div class="user_name">
																							<div class="name">
																								<input type="hidden" class="inp-comm" title="정비원ID" name="memberArr" value="${item.MEMBER}" readonly="readonly">
																								${item.MEMBER_NAME}
																								<c:if test="${item.USER_ID eq repairResultDtl.MANAGER}">
																									<p class="manager">담당자</p>
																								</c:if>
																							</div>
																							<div class="dep">
																								<span>
																									<input type="hidden" class="inp-comm" title="부서ID" name="" value="${item.DIVISION}"	readonly="readonly">
																									${item.DIVISION_NAME}
																								</span>
																								<span style="padding:0 5px;">/</span>
																								<span>
																									<input type="hidden" class="inp-comm" title="파트" name="" value="${item.PART}"	readonly="readonly">
																									${item.PART_NAME}
																								</span>
																							</div>
																						</div>
																					</div>
<!-- 																					<div class="repairMemberBtn"> -->
<!-- 																						<a href="#none" class="treeButton repairAdd" onclick="repairMemberControll.add(this)">추가</a> -->
<!-- 																						<a href="#none" class="treeButton repairTemp" onclick="fnWorkLogReg('N')">임시저장</a> -->
<!-- 																						<a href="#none" class="treeButton repairSave" onclick="fnWorkLogReg('Y')">확정</a> -->
<!-- 																					</div> -->
																				</div>	
																			</div>
																			<div class="repairAccOpenBtn"><a href="#none" title="정비원 상세보기"></a></div>
																			
																			<div class="panel-in" id="editForm2">																				
																					<div class="repairTime">
																						<c:forEach var="workLogItem" items="${workLogItem}">
																						<c:if test="${item.WORK_LOG eq workLogItem.WORK_LOG and item.USER_ID eq workLogItem.USER_ID}">
																						<!-- s : 추가버튼 클릭시 -->
																						<div class="repairTimeAddLst">
																							<div class="overflow">
																								<div class="f-l">
																									<div class="inTit"><spring:message code='epms.repair.time.start' /></div><!-- 정비시작시간 -->
																								</div>
																								<div class="f-l wd-per-15">
																									<div class="inp-wrap readonly wd-per-100">
																										<input type="hidden" class="inp-comm" readonly="readonly" name="DATE_START" title="정비시작일">
																										<input type="text" class="inp-comm" readonly="readonly" name="DATE_START_DATE" title="정비시작일" value="<c:out value="${workLogItem.DATE_START_DATE}" escapeXml="true"/>">
																									</div>
																								</div>
																								
																								<div class="wd-cal-10 rel mgn-l-5 mgn-r-25 f-l inp-wrap readonly">
																									<input type="text" class="inp-comm t-r" readonly="readonly" name="DATE_START_HOUR" title="정비시작시간" value="<c:out value="${workLogItem.DATE_START_HOUR}" escapeXml="true"/>">
																									<span style="position:absolute; top:5px; right:-15px;">시</span>
																								</div>
																								
																								<div class="wd-cal-10 rel mgn-l-5 mgn-r-25 f-l inp-wrap readonly">
																									<input type="text" class="inp-comm t-r" readonly="readonly" title="정비시작분" name="DATE_START_MINUTE" value="<c:out value="${workLogItem.DATE_START_MINUTE}" escapeXml="true"/>">
																									<span style="position:absolute; top:5px; right:-15px;">분</span>
																								</div>
																							</div>
																							<div class="overflow mgn-t-5">
																								<div class="f-l">
																									<div class="inTit"><spring:message code='epms.repair.time.end' /></div><!-- 정비종료시간 -->
																								</div>
																								<div class="f-l wd-per-15">
																									<div class="inp-wrap readonly wd-per-100">
																										<input type="hidden" class="inp-comm" name="DATE_END" title="정비종료일" readonly="readonly">
																										<input type="text" class="inp-comm" name="DATE_END_DATE" title="정비종료일" value="<c:out value="${workLogItem.DATE_END_DATE}" escapeXml="true"/>" readonly="readonly">
																									</div>
																								</div>
																								
																								<div class="wd-cal-10 rel mgn-l-5 mgn-r-25 f-l inp-wrap readonly">
																									<input type="text" class="inp-comm t-r" readonly="readonly" name="DATE_END_HOUR" title="정비시작시간" value="<c:out value="${workLogItem.DATE_END_HOUR}" escapeXml="true"/>">
																									<span style="position:absolute; top:5px; right:-15px;">시</span>
																								</div>
																								
																								<div class="wd-cal-10 rel mgn-l-5 mgn-r-25 f-l inp-wrap readonly">
																									<input type="text" class="inp-comm t-r" readonly="readonly" title="정비종료분" name="DATE_END_MINUTE" value="<c:out value="${workLogItem.DATE_END_MINUTE}" escapeXml="true"/>">
																									<span style="position:absolute; top:5px; right:-15px;">분</span>
																								</div>
																							</div>
																							<div class="overflow mgn-t-5">
																								<div class="f-l">
																									<div class="inTit"><spring:message code='epms.work.content' /></div><!-- 정비내용 -->
																								</div>
																								<div class="f-l wd-per-75">
																									
																									<div class="txt-wrap readonly"> 
																										<div class="txtArea">
																											<textarea cols="" rows="3" title="정비내용" id="ITEM_DESCRIPTION" name="ITEM_DESCRIPTION" readonly="readonly" ><c:out value="${workLogItem.DESCRIPTION}" escapeXml="true" /></textarea>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																						<!-- e : 추가버튼 클릭시 -->
																						</c:if>
																						</c:forEach>
																					</div>
																				</div>
																		</li>
																	</c:forEach>
																</ul>
															</td>
														</tr>
														<tr>
															<th scope="row"><spring:message code='epms.repair.type' /></th><!-- 정비유형 -->
															<td>
																<div class="inp-wrap readonly wd-per-100">
																	<input type="hidden" class="inp-comm" id="REPAIR_TYPE" name="REPAIR_TYPE" value="${repairResultDtl.REPAIR_TYPE}"/>
																	<input type="text" class="inp-comm" id="repair_type_name" name="repair_type_name" readonly="readonly" value="${repairResultDtl.REPAIR_TYPE_NAME}"/>
																</div>
															</td>
															<th scope="row"><spring:message code='epms.outsourcing' /></th><!-- 외주 -->
															<td>
																<div class="inp-wrap readonly wd-per-100">
																	<input type="text" class="inp-comm" id="OUTSOURCING" name="OUTSOURCING" readonly="readonly" value="<c:out value="${repairResultDtl.OUTSOURCING}" escapeXml="true" />"/>
																</div>
															</td>
														</tr>
														<tr>
															<th scope="row"><spring:message code='epms.occurr.time.start' /></th><!-- 고장시작시간 -->
															<td>
																<div class="inp-wrap readonly f-l wd-per-30">
																	<input type="hidden" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME1" name="TROUBLE_TIME1" title="고장시작일">
																	<input type="text" class="inp-comm" readonly="readonly" id="popRepairResultForm_TROUBLE_TIME1_DATE" name="TROUBLE_TIME1_DATE" title="고장시작일">
																</div>
																<div class="inp-wrap readonly wd-cal-25 rel mgn-l-5 mgn-r-25 f-l">
																	<input type="text" class="inp-comm t-r" readonly="readonly" id="popRepairResultForm_TROUBLE_TIME1_HOUR" name="TROUBLE_TIME1_HOUR" title="고장시작시간">
																	<span style="position:absolute; top:5px; right:-15px;">시</span>
																</div>
																<div class="inp-wrap readonly wd-cal-25 rel mgn-l-5 f-l">
																	<input type="text" class="inp-comm t-r" readonly="readonly" id="popRepairResultForm_TROUBLE_TIME1_MINUTE" name="TROUBLE_TIME1_MINUTE" title="고장시작분">
																	<span style="position:absolute; top:5px; right:-15px;">분</span>
																</div>
															</td>
															<th scope="row"><spring:message code='epms.occurr.time.end' /></th><!-- 고장종료시간 -->
															<td>
																<div class="inp-wrap readonly f-l wd-per-30">
																	<input type="hidden" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME2" name="TROUBLE_TIME2" title="고장종료일">
																	<input type="text" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME2_DATE" name="TROUBLE_TIME2_DATE" title="고장종료일">
																</div>
																<div class="inp-wrap readonly wd-cal-25 rel mgn-l-5 mgn-r-25 f-l">
																	<input type="text" class="inp-comm t-r" readonly="readonly" id="popRepairResultForm_TROUBLE_TIME2_HOUR" name="TROUBLE_TIME2_HOUR" title="고장종료시간">
																	<span style="position:absolute; top:5px; right:-15px;">시</span>
																</div>
																<div class="inp-wrap readonly wd-cal-25 rel mgn-l-5 f-l">
																	<input type="text" class="inp-comm t-r" readonly="readonly" id="popRepairResultForm_TROUBLE_TIME2_MINUTE" name="TROUBLE_TIME2_MINUTE" title="고장종료분">
																	<span style="position:absolute; top:5px; right:-15px;">분</span>
																</div>
															</td>
														</tr>						
<!-- 														<tr> -->
<!-- 															<th scope="row">고장유형</th> -->
<!-- 															<td> -->
<!-- 																<div class="inp-wrap readonly wd-per-100"> -->
<%-- 																	<c:forEach var="item" items="${troubleList1}" > --%>
<%-- 																		<c:if test="${repairResultDtl.TROUBLE_TYPE1 eq item.CODE}"> --%>
<%-- 																			<input type="text" class="inp-comm" id="TROUBLE_TYPE1" name="TROUBLE_TYPE1"  readonly="readonly" value="${item.NAME}" /> --%>
<%-- 																		</c:if> --%>
<%-- 																	</c:forEach> --%>
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<th scope="row">조치</th> -->
<!-- 															<td> -->
<!-- 																<div class="inp-wrap readonly wd-per-100"> -->
<%-- 																	<c:forEach var="item" items="${troubleList2}" > --%>
<%-- 																		<c:if test="${repairResultDtl.TROUBLE_TYPE2 eq item.CODE}"> --%>
<%-- 																			<input type="text" class="inp-comm" id="TROUBLE_TYPE1" name="TROUBLE_TYPE1"  readonly="readonly" value="${item.NAME}" /> --%>
<%-- 																		</c:if> --%>
<%-- 																	</c:forEach> --%>
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 														</tr> -->
<!-- 														<tr> -->
<!-- 															<th scope="row">코딩</th> -->
<!-- 															<td> -->
<!-- 																<div class="inp-wrap readonly wd-per-45 rel mgn-t-5 f-l"> -->
<%-- 																	<input type="text" class="inp-comm " readonly="readonly" id="CATALOG_D_GRP" name="CATALOG_D_GRP" value="${repairResultDtl.CATALOG_D_GRP_NAME }"> --%>
<!-- 																</div> -->
<!-- 																<div class="inp-wrap readonly wd-cal-55 rel mgn-t-5 mgn-l-5 f-l"> -->
<%-- 																	<input type="text" class="inp-comm " readonly="readonly" id="CATALOG_D" name="CATALOG_D" value="${repairResultDtl.CATALOG_D_NAME }"> --%>
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<th scope="row">대상(오브젝트 부품)</th> -->
<!-- 															<td> -->
<!-- 																<div class="inp-wrap readonly wd-per-45 rel mgn-t-5 f-l"> -->
<%-- 																	<input type="text" class="inp-comm " readonly="readonly" id="CATALOG_B_GRP" name="CATALOG_B_GRP" value="${repairResultDtl.CATALOG_B_GRP_NAME }"> --%>
<!-- 																</div> -->
<!-- 																<div class="inp-wrap readonly wd-cal-55 rel mgn-t-5 mgn-l-5 f-l"> -->
<%-- 																	<input type="text" class="inp-comm " readonly="readonly" id="CATALOG_B" name="CATALOG_B" value="${repairResultDtl.CATALOG_B_NAME }"> --%>
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 														</tr> -->
<!-- 														<tr> -->
<!-- 															<th scope="row">손상</th> -->
<!-- 															<td> -->
<!-- 																<div class="inp-wrap readonly wd-per-45 rel mgn-t-5 f-l"> -->
<%-- 																	<input type="text" class="inp-comm " readonly="readonly" id="CATALOG_C_GRP" name="CATALOG_C_GRP" value="${repairResultDtl.CATALOG_C_GRP_NAME }"> --%>
<!-- 																</div> -->
<!-- 																<div class="inp-wrap readonly wd-cal-55 rel mgn-t-5 mgn-l-5 f-l"> -->
<%-- 																	<input type="text" class="inp-comm " readonly="readonly" id="CATALOG_C" name="CATALOG_C" value="${repairResultDtl.CATALOG_C_NAME }"> --%>
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 															<th scope="row">원인</th> -->
<!-- 															<td> -->
<!-- 																<div class="inp-wrap readonly wd-per-45 rel mgn-t-5 f-l"> -->
<%-- 																	<input type="text" class="inp-comm " readonly="readonly" id="CATALOG_5_GRP" name="CATALOG_5_GRP" value="${repairResultDtl.CATALOG_5_GRP_NAME }"> --%>
<!-- 																</div> -->
<!-- 																<div class="inp-wrap readonly wd-cal-55 rel mgn-t-5 mgn-l-5 f-l"> -->
<%-- 																	<input type="text" class="inp-comm " readonly="readonly" id="CATALOG_5" name="CATALOG_5" value="${repairResultDtl.CATALOG_5_NAME }"> --%>
<!-- 																</div> -->
<!-- 															</td> -->
<!-- 														</tr> -->
														<tr>
															<th scope="row"><spring:message code='epms.work.content' /></th><!-- 작업내용 -->
															<td colspan="3">
																<div class="txt-wrap readonly">
																	<div class="txtArea">
																		<textarea cols="" rows="5" title="정비내용" id="REPAIR_DESCRIPTION" name="REPAIR_DESCRIPTION" readonly="readonly"><c:out value="${repairResultDtl.REPAIR_DESCRIPTION}" escapeXml="true" /></textarea>
																	</div>
																</div>
															</td>
														</tr>
														<tr>
															<th scope="row"><spring:message code='epms.repair.time.start' /></th><!-- 정비시작시간 -->
															<td>
																<div class="f-l readonly inp-wrap wd-per-50">
																	<input type="hidden" class="inp-comm" readonly="readonly" id="popRepairResultForm_REPAIR_TIME1" name="REPAIR_TIME1" title="정비시작일">
																	<input type="text" class="inp-comm " readonly="readonly" id="popRepairResultForm_REPAIR_TIME1_DATE" name="REPAIR_TIME1_DATE" title="정비시작일">
																</div>
																<div class="inp-wrap readonly wd-cal-25 rel mgn-l-5 mgn-r-25 f-l">
																	<input type="text" class="inp-comm t-r" readonly="readonly" id="popRepairResultForm_REPAIR_TIME1_HOUR" name="REPAIR_TIME1_HOUR" title="정비시작시간">
																	<span style="position:absolute; top:5px; right:-15px;">시</span>
																</div>
																<div class="inp-wrap readonly wd-cal-25 rel mgn-l-5 f-l">
																	<input type="text" class="inp-comm t-r" readonly="readonly" id="popRepairResultForm_REPAIR_TIME1_MINUTE" name="REPAIR_TIME1_MINUTE" title="정비시작분">
																	<span style="position:absolute; top:5px; right:-15px;">분</span>
																</div>
															</td>
															<th scope="row"><spring:message code='epms.repair.time.end' /></th><!-- 정비종료시간 -->
															<td>
																<div class="f-l readonly inp-wrap wd-per-50">
																	<input type="hidden" class="inp-comm " readonly="readonly" id="popRepairResultForm_REPAIR_TIME2" name="REPAIR_TIME2" title="정비종료일">
																	<input type="text" class="inp-comm " readonly="readonly" id="popRepairResultForm_REPAIR_TIME2_DATE" name="REPAIR_TIME2_DATE" title="정비종료일">
																</div>
																<div class="inp-wrap readonly wd-cal-25 rel mgn-l-5 mgn-r-25 f-l">
																	<input type="text" class="inp-comm " readonly="readonly" id="popRepairResultForm_REPAIR_TIME2_HOUR" name="REPAIR_TIME2_HOUR" title="정비종료시간">
																	<span style="position:absolute; top:5px; right:-15px;">시</span>
																</div>
																<div class="inp-wrap readonly wd-cal-25 rel mgn-l-5 f-l">
																	<input type="text" class="inp-comm " readonly="readonly" id="popRepairResultForm_REPAIR_TIME2_MINUTE" name="REPAIR_TIME2_MINUTE" title="정비종료분">
																	<span style="position:absolute; top:5px; right:-15px;">분</span>
																</div>
															</td>
														</tr>
														<tr>
															<th scope="row"><spring:message code='epms.use' /><spring:message code='epms.object' /></th><!-- 사용자재 -->
															<td colspan="3">
																<input type="hidden" class="inp-comm" id="MATERIAL_INOUT" name="MATERIAL_INOUT" value="${repairResultDtl.MATERIAL_INOUT}"/>
																<ul class="usedMaterial" id="usedMaterial">
																	<c:forEach var="item" items="${usedMaterial}">
																		<li style="overflow:hidden; margin:5px 0;">
																			<input type="hidden" class="inp-comm" title="BOMID" name="equipmentBomUsedArr" value="${item.EQUIPMENT_BOM}"	readonly="readonly">
																			<input type="hidden" class="inp-comm" title="자재ID" name="materialUsedArr" value="${item.MATERIAL}"	readonly="readonly">
																			<div class="box-col wd-per-20 f-l">
																				<div class="inp-wrap readonly wd-per-100">
																					<input type="text" class="inp-comm" title="장치명" name="" value="${item.MATERIAL_UID}"	readonly="readonly">
																				</div>
																			</div>
																			<div class="box-col wd-per-20 f-l mgn-l-5">
																				<div class="inp-wrap readonly wd-per-100">
																					<input type="text" class="inp-comm" title="자재명" name="" value="${item.MATERIAL_NAME}"	readonly="readonly">
																				</div>
																			</div>
																			<input type="hidden" class="inp-comm" title="단위" name="" value="${item.UNIT}"	readonly="readonly">
																			<div class="box-col wd-per-20 f-l mgn-l-5">
																				<div class="inp-wrap readonly wd-per-100">
																					<input type="text" class="inp-comm" title="단위명" name="" value="${item.UNIT_NAME}"	readonly="readonly">
																				</div>
																			</div>
																			<div class="box-col wd-per-10 f-l mgn-l-5">
																				<div class="inp-wrap">
																					<input type="text" class="inp-comm t-r" title="수량" name="qntyUsedArr"	value="${item.QNTY}"	readonly="readonly">
																				</div>
																			</div>
																		</li>
																	</c:forEach>
																</ul>
															</td>
														</tr>
														<tr>
															<th scope="row"><spring:message code='epms.use' /><spring:message code='epms.object' /> (<spring:message code='epms.unregist' />)</th><!-- 사용자재(미등록) -->
															<td colspan="3">
																<div class="inp-wrap readonly wd-per-100">
																	<input type="text" class="inp-comm" id="MATERIAL_UNREG" name="MATERIAL_UNREG"  readonly="readonly" value="<c:out value="${repairResultDtl.MATERIAL_UNREG}" escapeXml="true" />"/>
																</div>
															</td>
														</tr>
														<tr>
															<th scope="row"><spring:message code='epms.attach' /></th><!-- 정비 첨부파일 -->
															<td colspan="3">
																<c:choose>
																	<c:when test="${fn:length(attachList2)>0 }">
																	<div id="previewList" class="wd-per-100">
																		<div class="bxType01 fileWrap previewFileLst">
																			<div>
																				<div class="fileList type02">
																					<div id="detailFileList0" class="fileDownLst">
																						<c:forEach var="item" items="${attachList2}" varStatus="idx">
																							<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																								<span class="MultiFile-export">${item.SEQ_DSP}</span> 
																								<span class="MultiFile-title" title="File selected: ${item.NAME}">
																								${item.FILE_NAME} (${item.FILE_SIZE_TXT})
																								</span>
																								<a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a>
																							</div>		
																						</c:forEach>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
														
																	<div id="repair_previewImg" class="wd-per-100">
																		<div class="previewFile mgn-t-15">
																			<h3 class="tit-cont preview" id="previewTitle">이미지 미리보기</h3>
																			<ul style="margin-top:5px;">
																				<c:forEach var="item" items="${attachList2}" varStatus="idx">
																					<c:if test = "${item.FORMAT eq 'jpg' || item.FORMAT eq 'png' || item.FORMAT eq 'jpeg' || item.FORMAT eq 'JPG' || item.FORMAT eq 'PNG'}">
																						<li>
																							<img src="/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}" title="클릭하시면 원본크기로 보실 수 있습니다." onclick="originFileView('/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}')" />
																							<p>${item.FILE_NAME} <a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a></p>
																							
																						</li>
																					</c:if>
																				</c:forEach>
																			</ul>
																		</div>
																	</div>
																	</c:when>
																	<c:otherwise>
																		<div class="f-l wd-per-100 mgn-t-5">
																		※ 등록된 첨부파일이 없습니다.
																		</div>
																	</c:otherwise>
																</c:choose>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<%-- 정비내용 판넬 : e --%>
							
							<%-- 고장원인분석 판넬 : s --%>
							<div class="panel type02" id="panel2-3">
								<div class="panel-in" id="viewForm">
								<c:choose>
									<c:when test="${fn:length(worker)>0}">
										<div class="overflow tabFixedBtn">
						           			<div class="f-r">
						           				<a href="#none" class="btn comm st01" onclick="init.edit('I');">작성</a>
						           			</div>
					           			</div>
				           			</c:when>
				           		</c:choose>
									           		
					           		<div class="tabFixedBtn_Con">
						           		<div class="tabFixedBtn_ConIn">
							           		<div class="tb-wrap">
												<table class="tb-st">
													<caption class="screen-out"></caption>
													<colgroup>
														<col width="15%" />
														<col width="85%" />
													</colgroup>
													<tbody>
														<tr>
															<th scope="row">제목</th>
															<td>
																<div class="inp-wrap readonly wd-per-100">
																	<input type="text" class="inp-comm" name="TITLE" readonly="readonly"/>
																</div>
															</td>
														</tr>
														<tr>
															<th scope="row">분석일</th>
															<td>
																<div class="inp-wrap readonly wd-per-100">
																	<input type="text" class="inp-comm inpCal" name="DATE_ANALYSIS" readonly="readonly">
																</div>
															</td>
														</tr>
														<tr>
															<th scope="row">분석자</th>
															<td>
																<ul class="repairMemberList onlyLst repairMember" name="repairMember" style="padding-top: 10px;">
																</ul>
															</td>
														</tr>		
														<tr>
															<th scope="row">피해현황</th>
															<td>
																<div class="inp-wrap readonly wd-per-100">
																	<input type="text" class="inp-comm" name="DESCRIPTION" readonly="readonly"/>
																</div>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
											
											<!-- s : 고장원인리스트 -->
											<div class="mgn-t-25" name="analysisItem">
												<div class="troubleCauseTit">원인대책 리스트</div>
											</div>
											<!-- e : 고장원인리스트 -->
											
						           		</div>
						           	</div>
									
								</div>
								
								<div id="removeItemList" style="display:none;"></div>
								
								<form id="saveForm" name="saveForm"  method="post" enctype="multipart/form-data">	
								<input type="hidden" id="MODULE" name="MODULE" value="113"/>
								<input type="hidden" id="SAVE_YN" name="SAVE_YN" value=""/>
								<input type="hidden" id="ITEM_CNT" name="ITEM_CNT" value=""/>
								<input type="hidden" id="flag" name="flag" value="N"/>
								<input type="hidden" name="REPAIR_RESULT" value="${repairResultDtl.REPAIR_RESULT}"/> 
								<input type="hidden" id="REPAIR_ANALYSIS" name="REPAIR_ANALYSIS" value=""/>
									<div class="panel-in" id="editForm" style="display:none;">
										<div class="overflow tabFixedBtn">
						           			<div class="f-l">
						           				<a href="#none" class="btn comm st02" onclick="fnFormToggle('P');">이전</a>
						           			</div>
						           			<div class="f-r">
						           				<a href="#none" class="btn comm st02" onclick="analysisControll.add()">추가</a>
						           				<a href="#none" class="btn comm st02" onclick="fnSelectAnalysisList()">불러오기</a>
						           				<a href="#none" class="btn comm st02" onclick="fnAnaysisSave('N')">임시저장</a>
												<a href="#none" class="btn comm st01" onclick="fnAnaysisSave('Y')">작성완료</a>
						           			</div>
					           			</div>
										           		
						           		<div class="tabFixedBtn_Con">
							           		<div class="tabFixedBtn_ConIn">
								           		<div class="tb-wrap">
													<table class="tb-st">
														<caption class="screen-out"></caption>
														<colgroup>
															<col width="15%" />
															<col width="85%" />
														</colgroup>
														<tbody>
															<tr>
																<th scope="row">제목</th>
																<td>
																	<div class="inp-wrap wd-per-100">
																		<input type="text" class="inp-comm" name="TITLE"/>
																	</div>
																</td>
															</tr>
															<tr>
																<th scope="row">분석일</th>
																<td>
																	<div class="inp-wrap wd-per-100">
																		<input type="text" class="inp-comm inpCal datePic1" readonly="readonly" name="DATE_ANALYSIS">
																	</div>
																</td>
															</tr>
															<tr>
																<th scope="row">분석자</th>
																<td>
																	<div class="repairMemBtnArea overflow">
																		<a href="#none" class="btn evn-st01" onclick="fnSearchMember()">검색</a>
																	</div>
																	<ul class="repairMemberList onlyLst repairMember" id="analysisMember" name="repairMember">
																	</ul>
																</td>
															</tr>		
															<tr>
																<th scope="row">피해현황</th>
																<td>
																	<div class="inp-wrap wd-per-100">
																		<input type="text" class="inp-comm" name="DESCRIPTION"/>
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
												
												<!-- s : 고장원인리스트 -->
												<div class="mgn-t-25" name="analysisItem">
													<div class="troubleCauseTit">원인대책 리스트</div>
													
													
													<!-- repeat -->
													<div class="tb-wrap mgn-t-15">
														<table class="tb-st">
															<colgroup>
																<col width="15%" />
																<col width="*" />
															</colgroup>
															<tbody>
																<tr>
																	<th colspan="2" class="troubleCauseListMove">
																		<div style="padding-left:5px;">
																			<a href="#none" onclick="analysisControll.moveUp(this)" style="padding:0 7px" title="위로 올리기">
																				<img src="/images/com/web_v2/up-arrow.png" style="width:17px; heighat:auto;">
																			</a>
																			<a href="#none" onclick="analysisControll.moveDown(this)" style="padding:0 7px" title="아래로 내리기">
																				<img src="/images/com/web_v2/down-arrow.png" style="width:17px; heighat:auto;">
																			</a>
																			<a href="#none" onclick="analysisControll.remove(this)" style="padding:0 7px" title="삭제">
																				<img src="/images/com/web_v2/trash.png" style="width:16px; heighat:auto;">
																			</a>
																		</div>
																	</th>
																</tr>
																<tr>
																	<th>원인</th>
																	<td>
																		<div class="inp-wrap wd-per-100">
																			<input type="text" class="inp-comm" name="DESCRIPTION1"/>
																		</div>
																	</td>
																</tr>
																<tr>
																	<th>대책</th>
																	<td>
																		<div class="inp-wrap wd-per-100">
																			<input type="text" class="inp-comm" name="DESCRIPTION2"/>
																		</div>
																	</td>
																</tr>
																<tr>
																	<th>첨부파일</th>
																	<td>
																		<input type="hidden" name="REPAIR_ANALYSIS_ITEM" value=""/>
																		<input type="hidden" name="DEL_SEQ" value=""/>
																		<input type="hidden" name="DEL_YN" value="N"/>
																		<input type="hidden" name="SEQ_DSP" value=""/>
																		<input type="hidden" name="FILE_CNT" value=""/>
																		<input type="hidden" name="ATTACH_GRP_NO" value=""/>
																		<input type="hidden" name="attachExistYn" value=""/>
																		<input type="hidden" name="FILE_LIST_NAME" value=""/>
																		<div class="bxType01 fileWrap">
																			<div class="filebox"><input type="file" multiple name="fileList"></div>
																			<div class="fileList" style="padding:7px 0 5px;">
																				<div class="fileDownLst">
																				</div>
																			</div>
																		</div>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
													<!-- repeat -->
													
													
												</div>
												<!-- e : 고장원인리스트 -->
												
							           		</div>
							           	</div>
									</div>
								</form>
							</div>
							<%-- 고장원인분석 판넬 : e --%>
						</div>
					</div>
				<iframe id="fileDownFrame" style="width:0px;height:0px;border:0px"></iframe>
			</div> <%-- modal-bodyIn : e --%>
		</div> <%-- modal-body : e --%>
	</div> <%-- modal-content : e --%>
</div> <%-- modal : e --%>

<script type="text/javascript">
	// 정비원 컬럼 아코디언 기능 실행
	var repairMemAccSlider=new TINY.accordion.slider("repairMemAccSlider");
	repairMemAccSlider.init("repairMember","repairAccOpenBtn",0,-1);
</script>

<script type="text/template" id="memberTemplate">
<li style="overflow:hidden;">
	<div class="repairMemberTit">
		<div class="repairMemberTitIn">
			<div class="userInfo">
				<div class="user_img">
					<img src="" name="MEMBER_FILEURL" onerror="this.src='/images/com/web_v2/userIcon.png'" />
				</div>
				<div class="user_name">
					<div class="name">
						<input type="hidden" class="inp-comm" title="정비원ID" name="memberArr" value="" readonly="readonly">
						<input type="hidden" class="inp-comm" title="삭제여부" name="delYnArr" value="N"	readonly="readonly">
					</div>
					<div class="dep">
						<span>
							<input type="hidden" class="inp-comm f-l" title="부서ID" name="DIVISION" readonly="readonly">
						</span>
						<span style="padding:0 5px;">/</span>
						<span>
							<input type="hidden" class="inp-comm f-l" title="파트" name="PART" readonly="readonly">
						</span>
					</div>
				</div>
			</div>
 			<div class="repairMemberBtn2">
 				<a href="#none" class="treeButton repairDelete" onclick="delList1($(this))">삭제</a>
 			</div>
		</div>
	</div>
</li>

</script>

<script type="text/template" id="viewMemberTemplate">
<li style="overflow:hidden;">
	<div class="repairMemberTit">
		<div class="repairMemberTitIn">
			<div class="userInfo">
				<div class="user_img">
					<img src="" name="MEMBER_FILEURL" onerror="this.src='/images/com/web_v2/userIcon.png'" />
				</div>
				<div class="user_name">
					<div class="name">
						<input type="hidden" class="inp-comm" title="정비원ID" name="memberArr" value="" readonly="readonly">
						<input type="hidden" class="inp-comm" title="삭제여부" name="delYnArr" value="N" readonly="readonly">
					</div>
					<div class="dep">
						<span>
							<input type="hidden" class="inp-comm f-l" title="부서ID" name="DIVISION" readonly="readonly">
						</span>
						<span style="padding:0 5px;">/</span>
						<span>
							<input type="hidden" class="inp-comm f-l" title="파트" name="PART" readonly="readonly">
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</li>
</script>

<script type="text/template" id="viewItemTemplate">
<div class="tb-wrap mgn-t-15">
	<table class="tb-st">
		<colgroup>
			<col width="15%" />
			<col width="*" />
		</colgroup>
		<tbody>
			<tr>
				<th>원인</th>
				<td>
					<div class="inp-wrap readonly wd-per-100">
						<input type="text" class="inp-comm" readonly="readonly" name="DESCRIPTION1"/>
					</div>
				</td>
			</tr>
			<tr>
				<th>대책</th>
				<td>
					<div class="inp-wrap readonly wd-per-100">
						<input type="text" class="inp-comm" readonly="readonly" name="DESCRIPTION2"/>
					</div>
				</td>
			</tr>
			<tr>
				<th scope="row">첨부파일</th>
				<td name="itemFileList">
					<div id="previewList" class="wd-per-100">
						<div class="bxType01 fileWrap previewFileLst">
							<div>
								<div class="fileList type02">
									<div id="detailFileList0" class="fileDownLst" name="defalutFileList">
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div id="break_previewImg" class="wd-per-100">
						<div class="previewFile mgn-t-15">
							<h3 class="tit-cont preview" id="previewTitle">이미지 미리보기</h3>
							<ul style="margin-top:5px;" name="previewFileList">
							</ul>
						</div>
					</div>
					<div class="f-l wd-per-100 mgn-t-5" name="fileNull">
					※ 등록된 첨부파일이 없습니다.
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>

</script>

<%-- viewForm에서 첨부파일 리스트 --%>
<script type="text/template" id="fileTemplate1">
<div class="MultiFile-label">
	<span class="MultiFile-export"></span> 
	<span class="MultiFile-title"></span>
	<a href="#none" class="btnFileDwn icon download">다운로드</a>
</div>		
</script>
<%-- viewForm에서 이미지 미리보기 --%>
<script type="text/template" id="fileTemplate2">
<li>
	<img/>
	<p></p>
</li>
</script>
<%-- editForm에서 첨부 파일 리스트 --%>
<script type="text/template" id="fileTemplate3">
<div class="MultiFile-label">
	<span class="MultiFile-remove">
		<a href="#none" onclick="return delModiFile($(this));" class="icn_fileDel" data-attach="{{ATTACH}}" data-idx="{{CD_FILE_TYPE}}">
			<img src="/images/com/web/btn_remove.png" />
		</a>
	</span> 
	<span class="MultiFile-export" name="SEQ_DSP"></span>
	<span class="MultiFile-title"></span>
	<a href="#none" class="btnFileDwn icon download" data-attachgrpno="{{ATTACH_GRP_NO}}" data-attach="{{ATTACH}}"><spring:message code='file.download' /></a>
</div>	
</script>

<script type="text/template" id="addItemTemplate">
<div class="tb-wrap mgn-t-15">
	<table class="tb-st">
		<colgroup>
			<col width="15%" />
			<col width="*" />
		</colgroup>
		<tbody>
			<tr>
				<th colspan="2" class="troubleCauseListMove">
					<div style="padding-left:5px;">
						<a href="#none" onclick="analysisControll.moveUp(this)" style="padding:0 7px" title="위로 올리기">
							<img src="/images/com/web_v2/up-arrow.png" style="width:17px; heighat:auto;">
						</a>
						<a href="#none" onclick="analysisControll.moveDown(this)" style="padding:0 7px" title="아래로 내리기">
							<img src="/images/com/web_v2/down-arrow.png" style="width:17px; heighat:auto;">
						</a>
						<a href="#none" onclick="analysisControll.remove(this)" style="padding:0 7px" title="삭제">
							<img src="/images/com/web_v2/trash.png" style="width:16px; heighat:auto;">
						</a>
					</div>
				</th>
			</tr>
			<tr>
				<th>원인</th>
				<td>
					<div class="inp-wrap wd-per-100">
						<input type="text" class="inp-comm" name="DESCRIPTION1"/>
					</div>
				</td>
			</tr>
			<tr>
				<th>대책</th>
				<td>
					<div class="inp-wrap wd-per-100">
						<input type="text" class="inp-comm" name="DESCRIPTION2"/>
					</div>
				</td>
			</tr>
			<tr>
				<th>첨부파일</th>
				<td>
					<input type="hidden" name="REPAIR_ANALYSIS_ITEM" value=""/>
					<input type="hidden" name="DEL_SEQ" value=""/>
					<input type="hidden" name="DEL_YN" value="N"/>
					<input type="hidden" name="SEQ_DSP" value=""/>
					<input type="hidden" name="FILE_CNT" value=""/>
					<input type="hidden" name="ATTACH_GRP_NO" value=""/>
					<input type="hidden" name="attachExistYn" value=""/>
					<input type="hidden" name="FILE_LIST_NAME" value=""/>
					<div class="bxType01 fileWrap">
						<div class="filebox"><input type="file" multiple name="fileList"></div>
						<div class="fileList" style="padding:7px 0 5px;">
							<div class="fileDownLst">
							</div>
						</div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
</script>