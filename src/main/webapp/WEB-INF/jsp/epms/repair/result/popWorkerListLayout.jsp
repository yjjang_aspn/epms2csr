<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="WorkerStatList"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"  SortIcons="0"       Calculated="1"      CalculateSelected ="1"  NoFormatEscape="1"
			NumberId="1"          DateStrings="2"     SelectingCells="0"  AcceptEnters="1"        Style="Office"
			InEditMode="2"        SafeCSS='1'         NoVScroll="0"       NoHScroll="0"           EnterMode="0"
			Filtering="1"         Dragging="0"        Selecting="1" 	  Deleting="0" 			  Editing ="1"
			Paging="1"			  PageLength="10"	  MaxPages="20"
			CopyCols="0"		  PasteFocused="3"	  CustomScroll="1"
	/>
	
	<Cfg Paging="2" PageLength="15" PageMin="2" Pages="10" PagingFixed="0"/>
	<Head>
		<Header	id="Header"	Align="center" NoEscape="1"
				DIVISION           = "부서ID"
				DIVISION_NAME      = "부서명"
				LOCATION           = "공장코드"
				LOCATION_NAME      = "소속"    <%  // 소속공장 %>
				PART               = "파트"
				PART_NAME          = "파트명"
				USER_ID            = "유저ID"
				NAME               = "이름"
				CHECK_INOUT        = "현황"
				MEMBER_FILEURL     = "프로필 이미지"
		/>
		<Filter	id="Filter"	CanFocus="1" />      
	</Head>
	
	<I Kind='Filter' noticeTrgtRange='1' noticeTypeRange='1'
		subjectResultMask="" subjectResultText=""
		contentsResultMask="" contentsResultText=""/>
		
	<Pager Visible="0"/>
	
	<Cols>
		<C Name="DIVISION"			Type="Text"	RelWidth="100"		CanEdit="1"	Align="center"	Visible="0"	Cursor="Hand"	Background="#ffffff"	CaseSensitive="0"	WhiteChars=" " />
		<C Name="DIVISION_NAME"		Type="Text"	RelWidth="100"		CanEdit="1"	Align="center"	Visible="0"	Cursor="Hand"	Background="#ffffff"	CaseSensitive="0"	WhiteChars=" " />
		<C Name="LOCATION"			Type="Text"	RelWidth="100"		CanEdit="1"	Align="center"	Visible="0"	Cursor="Hand"	Background="#ffffff"	CaseSensitive="0"	WhiteChars=" " />
		<C Name="LOCATION_NAME"		Type="Text"	RelWidth="100"		CanEdit="0"	Align="center"	Visible="1"	Cursor="Hand"	Background="#ffffff"	CaseSensitive="0"	WhiteChars=" " />
		<C Name="PART"				Type="Enum"	RelWidth="100"		CanEdit="0"	Align="center"	Visible="1"	<tag:enum codeGrp="PART" companyId="${ssCompanyId}"/> Background="#ffffff"/>
		<C Name="PART_NAME"			Type="Text"	RelWidth="100"		CanEdit="1"	Align="center"	Visible="0"	Cursor="Hand"	Background="#ffffff"	CaseSensitive="0"	WhiteChars=" " />
		<C Name="USER_ID"			Type="Text"	RelWidth="100"		CanEdit="0"	Align="center"	Visible="0"	Cursor="Hand"	Background="#ffffff"	CaseSensitive="0"	WhiteChars=" " />
		<C Name="NAME"				Type="Text"	RelWidth="120"		CanEdit="0"	Align="center"	Visible="1"	Cursor="Hand"	Background="#ffffff"	CaseSensitive="0"	WhiteChars=" " />
		<C Name="CHECK_INOUT"		Type="Enum"	RelWidth="100"		CanEdit="0"	Align="center" 	Visible="1"	<tag:enum codeGrp="CHECK_INOUT"/>/>
		<C Name="MEMBER_FILEURL"	Type="Text"	RelWidth="120"		CanEdit="0"	Align="center"	Visible="0"	Cursor="Hand"	Background="#ffffff"	CaseSensitive="0"	WhiteChars=" " />		
	</Cols>
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,정비원추가"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        "
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count(7)+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				정비원추가Type="Html" 정비원추가="&lt;a href='#none' title='정비원추가' class=&quot;btn comm st02&quot;
								  onclick='fnAddMember(&quot;${gridId}&quot;)'><spring:message code='epms.worker.add' />&lt;/a>"
				추가Type="Html" 추가="&lt;a href='#none' title='추가' class=&quot;defaultButton01 icon add&quot;
								onclick='addRowGrid(&quot;${gridId}&quot;)'>추가&lt;/a>"
				저장Type="Html" 저장="&lt;a href='#none' title='저장' class=&quot;defaultButton01 icon submit&quot;
								onclick='fnModify(&quot;${gridId}&quot;)'>저장&lt;/a>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton01 icon refresh&quot; 
								onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				PrintType="Html" Print="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
								onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			ExportType="Html" Export="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
								onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>	
</Grid>