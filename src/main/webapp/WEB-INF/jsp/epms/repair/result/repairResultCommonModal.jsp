<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: repairResultCommonModal.jsp
	Description : 고장관리 > 고장등록 > 정비이력 그리드 데이터
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성

--%>
    
<%-- 정비실적등록 --%>
<div class="modal fade modalFocus" id="popRepairResultRegForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 정비실적조회 --%>
<div class="modal fade modalFocus" id="popRepairResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 타파트 요청 --%>
<div class="modal fade modalFocus" id="popRequestPartForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 고장원인분석 등록 --%>
<div class="modal fade modalFocus" id="popRepairAnalysisForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 정비원 검색 --%>
<div class="modal fade modalFocus" id="popWorkerList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-25">		
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">
                    <spring:message code='epms.worker' /> <spring:message code='epms.search' />
                </h4><!-- 정비원 검색 -->
			</div>
	 	<div class="modal-body" >
           	<div class="modal-bodyIn">
           		<div class="modal-section">
					<div class="fl-box wd-per-100">
						<div id="snsList">
							<bdo Debug="Error"
								 Data_Url=""
								 Layout_Url="/epms/repair/result/popWorkerListLayout.do"
							 >
							</bdo>
						</div>
					</div>
				</div> <%-- modal-section : e --%>
			</div> <%-- modal-bodyIn : e --%>
		</div> <%-- modal-body : e --%>
	</div> <%-- modal-content : e --%>
</div> <%-- modal : e --%>
</div>

<%-- 부품명 검색 --%>
<div class="modal fade modalFocus" id="popRepairMaterialList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-50">
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">
                    <spring:message code='epms.object.name' /> <spring:message code='epms.search' />
                </h4><!-- 부품명 검색 -->
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	           		<div class="modal-section">
						<div class="fl-box wd-per-100">
							<bdo	Debug="Error"
					 				Data_Url=""
									Layout_Url="/epms/repair/result/popRepairMaterialListLayout.do" 
							>
							</bdo>
						</div>
					</div> <%-- modal-section : e --%>
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>

</body>
</html>