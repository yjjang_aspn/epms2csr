<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I
			REPAIR_RESULT			= "${item.REPAIR_RESULT}"
			
			LOCATION				= "${item.LOCATION}"
			CATEGORY				= "${item.CATEGORY}"
			CATEGORY_NAME			= "${fn:replace(item.CATEGORY_NAME, '"', '&quot;')}"
			EQUIPMENT				= "${item.EQUIPMENT}"
			EQUIPMENT_UID			= "${item.EQUIPMENT_UID}"
			EQUIPMENT_NAME			= "${fn:replace(item.EQUIPMENT_NAME, '"', '&quot;')}"
			GRADE			       	= "${item.GRADE}"
			MODEL					= "${fn:replace(item.MODEL, '"', '&quot;')}"
			MANUFACTURER			= "${fn:replace(item.MANUFACTURER, '"', '&quot;')}"
			
			REPAIR_REQUEST			= "${item.REPAIR_REQUEST}"
			URGENCY					= "${item.URGENCY}"
			<c:if test="${item.URGENCY == '02'}">
				URGENCYClass="gridStatus4"
				Background="#FFDDDD"
			</c:if>
			REQUEST_TYPE			= "${item.REQUEST_TYPE}"
			REQUEST_TYPE2			= "${item.REQUEST_TYPE2}"
			PART					= "${item.PART}"
			REQUEST_DESCRIPTION		= "${fn:replace(item.REQUEST_DESCRIPTION, '"', '&quot;')}"
			ATTACH_GRP_NO1			= "${item.ATTACH_GRP_NO1}"
			REQUEST_DIV				= "${item.REQUEST_DIV}"
			REQUEST_DIV_NAME		= "${item.REQUEST_DIV_NAME}"
			REQUEST_MEMBER			= "${item.REQUEST_MEMBER}"
			REQUEST_MEMBER_NAME		= "${item.REQUEST_MEMBER_NAME}"
			DATE_REQUEST			= "${item.DATE_REQUEST}"
			APPR_MEMBER				= "${item.APPR_MEMBER}"
			APPR_MEMBER_NAME		= "${item.APPR_MEMBER_NAME}"
			DATE_APPR				= "${item.DATE_APPR}"
			
			REPAIR_TYPE				= "${item.REPAIR_TYPE}"
			REPAIR_STATUS			= "${item.REPAIR_STATUS}"
			<c:choose>
				<c:when test="${item.REPAIR_STATUS == '03'}">
					REPAIR_STATUSClass="gridStatus3"
				</c:when>
				<c:when test="${item.REPAIR_STATUS == '04'}">
					REPAIR_STATUSClass="gridStatus4"
				</c:when>
			</c:choose>
			DATE_ASSIGN				= "${item.DATE_ASSIGN}"
			DATE_PLAN				= "${item.DATE_PLAN}"
			DATE_REPAIR				= "${item.DATE_REPAIR}"
			TROUBLE_TYPE1			= "${item.TROUBLE_TYPE1}"
			TROUBLE_TYPE2			= "${item.TROUBLE_TYPE2}"
			MANAGER					= "${item.MANAGER}"
			MANAGER_NAME			= "${item.MANAGER_NAME}"
			OUTSOURCING				= "${fn:replace(item.OUTSOURCING, '"', '&quot;')}"
			REPAIR_MEMBER			= "${fn:replace(item.REPAIR_MEMBER_NAME, '"', '&quot;')}"
			MATERIAL_USED			= "${fn:replace(item.MATERIAL_USED_NAME, '"', '&quot;')}"
			MATERIAL_UNREG			= "${fn:replace(item.MATERIAL_UNREG, '"', '&quot;')}"
			TROUBLE_TIME1			= "${item.TROUBLE_TIME1}"
			TROUBLE_TIME2			= "${item.TROUBLE_TIME2}"
			TROUBLE_TIME_HOUR		= "${item.TROUBLE_TIME_HOUR}"
			TROUBLE_TIME_MINUTE		= "${item.TROUBLE_TIME_MINUTE}"
			REPAIR_TIME1			= "${item.REPAIR_TIME1}"
			REPAIR_TIME2			= "${item.REPAIR_TIME2}"
			REPAIR_TIME_HOUR		= "${item.REPAIR_TIME_HOUR}"
			REPAIR_TIME_MINUTE		= "${item.REPAIR_TIME_MINUTE}"
			
			E_RESULT				= "${item.E_RESULT}"
			E_AUFNR					= "${item.E_AUFNR}"
			E_MESSAGE				= "${item.E_MESSAGE}"
			
			REPAIR_DESCRIPTION		= "${fn:replace(item.REPAIR_DESCRIPTION, '"', '&quot;')}"
			ATTACH_GRP_NO2			= "${item.ATTACH_GRP_NO2}"
			
			<c:set var="loop_flag" value="no"/>
			<c:set var="repair_result" value="${item.REPAIR_RESULT}"/>
			<c:forEach var="item2" items="${repairRegMember}">
				<c:if test="${repair_result eq item2.WORK_ID and item2.USER_ID eq userId}">
					<c:set var="loop_flag" value="yes" />
				</c:if>
			</c:forEach>
			
			<c:choose>
				<c:when test='${item.REPAIR_STATUS eq "02" and loop_flag eq "yes"}'>
						detailSwitch="1" detailIcon="/images/com/web/commnet_up5.gif"
						detailOnClickSideIcon="fnOpenLayerWithParam('/epms/repair/result/popRepairResultRegForm.do?REPAIR_REQUEST='+Row.REPAIR_REQUEST+'&REPAIR_RESULT='+Row.REPAIR_RESULT+'&EQUIPMENT='+Row.EQUIPMENT,'popRepairResultRegForm');"
				</c:when>
				<c:otherwise>
						detailSwitch="1" detailIcon="/images/com/web/commnet_up6.gif"
						detailOnClickSideIcon="fnOpenLayerWithParam('/epms/repair/result/popRepairResultForm.do?REPAIR_REQUEST='+Row.REPAIR_REQUEST+'&REPAIR_RESULT='+Row.REPAIR_RESULT+'&EQUIPMENT='+Row.EQUIPMENT,'popRepairResultForm');"
				</c:otherwise>
			</c:choose>
			
			<c:if test='${item.REPAIR_STATUS eq "03" && item.REPAIR_TYPE eq "110"}'>
				analysisSwitch="1"  analysisIcon="/images/com/web/commnet_up4.gif" 
				analysisOnClickSideIcon="fnOpenLayerWithParam('/epms/repair/result/popRepairResultForm.do?REPAIR_REQUEST='+Row.REPAIR_REQUEST+'&REPAIR_RESULT='+Row.REPAIR_RESULT+'&EQUIPMENT='+Row.EQUIPMENT,'popRepairResultForm');"
			</c:if>
			
			REPAIR_ANALYSIS			= "${item.REPAIR_ANALYSIS}"
			
			<c:if test='${item.REPAIR_TYPE eq "110" and  item.REPAIR_STATUS eq "03"}'>
			SAVE_YN					= "${item.SAVE_YN}"
				<c:if test="${item.SAVE_YN == 'Y'}">
					SAVE_YNClass="gridStatus3"
				</c:if>
				<c:if test="${item.SAVE_YN == 'N'}">
					SAVE_YNClass="gridStatus4"
				</c:if>
			</c:if>
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>