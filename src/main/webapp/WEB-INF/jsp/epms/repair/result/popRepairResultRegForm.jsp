<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: popRepairResultRegForm.jsp
	Description : 고장관리 > 정비실적 등록 레이어 팝업
	author		: 김영환
	since		: 2018.04.04
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.04	 김영환		최초 생성
	2018.12.20	 김영환		정비원 정비실적 ITEM 추가

--%>

<%@ include file="/com/codeConstants.jsp"%>
<script src="/js/com/web/common.js" type="text/javascript"></script>
<script src="/js/com/web/accordion.js" type="text/javascript"></script>

<script type="text/javascript">
var submitBool = true;
var maxFileCnt = 30;
var msg = "";
var timeArray = ['TROUBLE_TIME1', 'TROUBLE_TIME2', 'REPAIR_TIME1', 'REPAIR_TIME2'];

$(document).ready(function(){
		
	$('.panel.type02').each(function () {
		if($(this).find(".tabFixedBtn").length){
			$(this).find(".tabFixedBtn_Con").css({ 'height': 'calc(100% - 51px)'});
		}else {
			$(this).find(".tabFixedBtn_Con").css({ 'height': '100%'});
		}
	});
	
	//accordion 닫혀있을때 row add 버튼 클릭시 panel 오픈
	$("#repairMember2 .repairAdd").click(function() {
	    $(this).closest('.repairMemberTit').parent().find('.repairTime').css({'height': 'auto','opacity': '1'});
	    $(this).closest('.repairMemberTit').next().removeClass('repairAccOpenBtn').addClass('repairAccCloseBtn');
	});
	
	//정비원 개별실적이 1건만 있을경우 : 버튼 숨김처리
	$('.repairAccOpenBtnHide').on('click', function(){
		if($(this).closest('li').find('.repairTimeAddLst[value="N"]').length==1){
			$('a.repairTimeRowDel.deleteBtn').hide();
		}
		
	});
	
	<%-- DatePicker  --%>
	$('.moDatePic').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	});
	
	<%-- 고장/정비 시작/종료시간 기본값 세팅 --%>
	fnSetDefaultTime();
	
	<%-- '타파트요청'버튼 클릭 --%>
	$('#popRepairResultRegForm_requestBtn').on('click', function(){
		$('#flag').val("request");
		fnSave("request");
	});
	
	<%-- '임시저장'버튼 클릭 --%>
	$('#popRepairResultRegForm_saveBtn').on('click', function(){
		$('#flag').val("SAVE");
		fnSave("SAVE");
	});
	
	<%-- '정비완료'버튼 클릭 --%>
	$('#popRepairResultRegForm_regBtn').on('click', function(){
		$('#flag').val("COMPLETE");
		// 정비원들의 정비 확정여부 부터 체크
		WorkStatusCheck();
// 		fnSave("COMPLETE");
	});
	
	<%-- 첨부파일 수정버튼 (등록폼 변환) --%>
	$('#modifyBtn').on('click', function(){
		
		$('#viewFrame').hide();
		$('#regFrame').show();
	});
	
	<%-- '임시저장'버튼 클릭 --%>
	$('#repairTemp').on('click', function(){
		$('#flag').val("WORKLOG_SAVE");
		fnSave("WORKLOG_SAVE");
	});

	<%-- 첨부파일 관련 --%>
	for(var i=0; i<1; i++){				<%-- 파일 영역 추가를 고려하여 개발 --%>
		$('#attachFile').append('<input type="file" id="fileList" class="ksUpload" name="fileList">');	
		
		//var fileType = 'jpeg |gif |jpg |png |bmp |ppt |pptx |doc |hwp |pdf |xlsx |xls';
		var fileType = 'jpeg |gif |jpg |png |bmp '
					 + '|avi |wmv |mpeg |mpg |mkv |mp4 |tp |ts |asf |asx |flv |mov |3gp '
					 + '|mp3 |ogg |wma |wav |au |rm |mid |flac |m4a |amr ';
	
		$('#fileList').MultiFile({
			   accept :  fileType
			,	list   : '#detailFileRegList'+i
			,	max    : 5
			,	STRING : {
					remove: '<img src="/images/com/web/btn_remove.png"/>'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
					text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" />',
					idx	  : i,
				},
		});
		
		<%-- 첨부한 파일의 출력 순서를 자동으로 샛팅 하기 위한 스크립트 - 공통 스크립트로 빠지면 안됨 .개별 항목으로 처리 필수 --%> 
		$('.MultiFile-wrap').change(function(){
			var fileExpIndex = $(this).closest('.fileWrap').find('.fileList > div').children('div:last-child');
			var	fileExp = fileExpIndex.find('.fileExp');
			fileExp.val(fileExpIndex.index() + 1);
		});
	}
	
	<%-- 파일 다운로드 --%>
	$('.btnFileDwn').on('click', function(){
		var ATTACH_GRP_NO = $(this).data("attachgrpno");
		var ATTACH = $(this).data("attach");
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO='+ATTACH_GRP_NO+'&ATTACH='+ATTACH;
		$("#fileDownFrame").attr("src",src);
	});
	
	<%-- 첨부파일 삭제버튼 --%>
	$('.icn_fileDel').on('click', function(){
		delModiFile($(this));
	});
	
	<%-- 고장 첨부파일 중 사진이 아닐경우 미리보기 숨김 --%>
	var fileList1 = new Array();
	<c:forEach items="${attachList1}" var="item" >
		var obj = new Object();
		
		obj.ATTACH_GRP_NO = "${item.ATTACH_GRP_NO}";
		obj.ATTACH = "${item.ATTACH}";
		obj.FORMAT = "${item.FORMAT}";
		
		fileList1.push(obj);
	</c:forEach>
	var attachImgCnt1 = 0;
	for(var i=0; i<fileList1.length; i++){
		if(fileList1[i].FORMAT == "jpg" || fileList1[i].FORMAT == "png" || fileList1[i].FORMAT == "jpeg" || fileList1[i].FORMAT == "JPG" || fileList1[i].FORMAT == "PNG"){
			attachImgCnt1 ++;
		}
	}	
	
	if(fileList1.length > 0){
		if(attachImgCnt1 == 0){
			$('#break_previewImg').hide();
		}
	}
	
	<%-- 정비 첨부파일 중 사진이 아닐경우 미리보기 숨김 --%>
	var fileList2 = new Array();
	<c:forEach items="${attachList2}" var="item" >
		var obj = new Object();
		
		obj.ATTACH_GRP_NO = "${item.ATTACH_GRP_NO}";
		obj.ATTACH = "${item.ATTACH}";
		obj.FORMAT = "${item.FORMAT}";
		
		fileList2.push(obj);
	</c:forEach>
	var attachImgCnt2 = 0;
	for(var i=0; i<fileList2.length; i++){
		if(fileList2[i].FORMAT == "jpg" || fileList2[i].FORMAT == "png" || fileList2[i].FORMAT == "jpeg" || fileList2[i].FORMAT == "JPG" || fileList2[i].FORMAT == "PNG"){
			attachImgCnt2 ++;
		}
	}	
	
	if(fileList2.length > 0){
		if(attachImgCnt2 == 0){
			$('#repair_previewImg').hide();
		}
	}
	
	<%-- select box 초기값 세팅 --%>
	if("${repairResultDtl.REPAIR_TYPE}" != '') $("select[name=REPAIR_TYPE]").val("${repairResultDtl.REPAIR_TYPE}");
	if("${repairResultDtl.TROUBLE_TYPE1}" != '') $("select[name=TROUBLE_TYPE1]").val("${repairResultDtl.TROUBLE_TYPE1}");
	if("${repairResultDtl.TROUBLE_TYPE2}" != '') $("select[name=TROUBLE_TYPE2]").val("${repairResultDtl.TROUBLE_TYPE2}");
	
	<%-- 담당자가 아니면 정비실적 입력창 비활성화(readOnly) --%>
	if("${repairResultDtl.MANAGER}" != "${userId}"){
		// 정비유형
		$("select[name=REPAIR_TYPE]").attr("disabled", "disabled");
		$(".sel-wrap select").css("-webkit-appearance", "none");
		$("select[name=REPAIR_TYPE]").closest('div').addClass("readOnly");
		
		// 외주
		$("input[name=OUTSOURCING]").attr("disabled", "disabled");
		$("input[name=OUTSOURCING]").closest('div').addClass("readOnly");
		
		// 고장시작시간:DTAE
		$("#popRepairResultRegForm_TROUBLE_TIME1_DATE").attr("disabled", "disabled");
		$("#popRepairResultRegForm_TROUBLE_TIME1_DATE").removeClass("moDatePic");
		$("#popRepairResultRegForm_TROUBLE_TIME1_DATE").css("padding", "4px 8px");
		$("#popRepairResultRegForm_TROUBLE_TIME1_DATE").closest('div').addClass("readOnly");
				
		// 고장시작시간:HOUR
		$("#popRepairResultRegForm_TROUBLE_TIME1_HOUR").attr("disabled", "disabled");
		$("#popRepairResultRegForm_TROUBLE_TIME1_HOUR").closest('div').addClass("readOnly");
		
		// 고장시작시간:MINUTE 
		$("#popRepairResultRegForm_TROUBLE_TIME1_MINUTE").attr("disabled", "disabled");
		$("#popRepairResultRegForm_TROUBLE_TIME1_MINUTE").closest('div').addClass("readOnly");

		// 고장종료시간:DTAE
		$("#popRepairResultRegForm_TROUBLE_TIME2_DATE").attr("disabled", "disabled");
		$("#popRepairResultRegForm_TROUBLE_TIME2_DATE").removeClass("moDatePic");
		$("#popRepairResultRegForm_TROUBLE_TIME2_DATE").css("padding", "4px 8px");
		$("#popRepairResultRegForm_TROUBLE_TIME2_DATE").closest('div').addClass("readOnly");
		
		// 고장종료시간:HOUR
		$("#popRepairResultRegForm_TROUBLE_TIME2_HOUR").attr("disabled", "disabled");
		$("#popRepairResultRegForm_TROUBLE_TIME2_HOUR").closest('div').addClass("readOnly");
		
		// 고장종료시간:MINUTE
		$("#popRepairResultRegForm_TROUBLE_TIME2_MINUTE").attr("disabled", "disabled");
		$("#popRepairResultRegForm_TROUBLE_TIME2_MINUTE").closest('div').addClass("readOnly");
		
		// 고장유형
		$("select[name=TROUBLE_TYPE1]").attr("disabled", "disabled");
		$("select[name=TROUBLE_TYPE1]").closest('div').addClass("readOnly");
		
		// 조치
		$("select[name=TROUBLE_TYPE2]").attr("disabled", "disabled");
		$("select[name=TROUBLE_TYPE2]").closest('div').addClass("readOnly");
		
// 		// 코딩 그룹
// 		$("select[name=CATALOG_D_GRP]").attr("disabled", "disabled");
// 		$("select[name=CATALOG_D_GRP]").closest('div').addClass("readOnly");
		
// 		// 코딩
// 		$("select[name=CATALOG_D]").attr("disabled", "disabled");
// 		$("select[name=CATALOG_D]").closest('div').addClass("readOnly");
		
// 		// 대상(오브젝트 부품) 그룹
// 		$("select[name=CATALOG_B_GRP]").attr("disabled", "disabled");
// 		$("select[name=CATALOG_B_GRP]").closest('div').addClass("readOnly");
		
// 		// 대상(오브젝트 부품)
// 		$("select[name=CATALOG_B]").attr("disabled", "disabled");
// 		$("select[name=CATALOG_B]").closest('div').addClass("readOnly");
		
// 		// 손상 그룹
// 		$("select[name=CATALOG_C_GRP]").attr("disabled", "disabled");
// 		$("select[name=CATALOG_C_GRP]").closest('div').addClass("readOnly");
		
// 		// 손상
// 		$("select[name=CATALOG_C]").attr("disabled", "disabled");
// 		$("select[name=CATALOG_C]").closest('div').addClass("readOnly");
		
// 		// 원인 그룹
// 		$("select[name=CATALOG_5_GRP]").attr("disabled", "disabled");
// 		$("select[name=CATALOG_5_GRP]").closest('div').addClass("readOnly");
		
// 		// 원인
// 		$("select[name=CATALOG_5]").attr("disabled", "disabled");
// 		$("select[name=CATALOG_5]").closest('div').addClass("readOnly");
		
		// 작업내용
		$("textarea[name=REPAIR_DESCRIPTION]").attr("disabled", "disabled");
		$("textarea[name=REPAIR_DESCRIPTION]").closest('div').addClass("readOnly");
		
		// 정비시작시간:DTAE
		$("#popRepairResultRegForm_REPAIR_TIME1_DATE").attr("disabled", "disabled");
		$("#popRepairResultRegForm_REPAIR_TIME1_DATE").removeClass("moDatePic");
		$("#popRepairResultRegForm_REPAIR_TIME1_DATE").css("padding", "4px 8px");
		$("#popRepairResultRegForm_REPAIR_TIME1_DATE").closest('div').addClass("readOnly");
		
		// 정비시작시간:HOUR
		$("#popRepairResultRegForm_REPAIR_TIME1_HOUR").attr("disabled", "disabled");
		$("#popRepairResultRegForm_REPAIR_TIME1_HOUR").closest('div').addClass("readOnly");
		
		// 정비시작시간:MINUTE 
		$("#popRepairResultRegForm_REPAIR_TIME1_MINUTE").attr("disabled", "disabled");
		$("#popRepairResultRegForm_REPAIR_TIME1_MINUTE").closest('div').addClass("readOnly");

		// 정비종료시간:DTAE
		$("#popRepairResultRegForm_REPAIR_TIME2_DATE").attr("disabled", "disabled");
		$("#popRepairResultRegForm_REPAIR_TIME2_DATE").removeClass("moDatePic");
		$("#popRepairResultRegForm_REPAIR_TIME2_DATE").css("padding", "4px 8px");
		$("#popRepairResultRegForm_REPAIR_TIME2_DATE").closest('div').addClass("readOnly");
		
		// 정비종료시간:HOUR
		$("#popRepairResultRegForm_REPAIR_TIME2_HOUR").attr("disabled", "disabled");
		$("#popRepairResultRegForm_REPAIR_TIME2_HOUR").closest('div').addClass("readOnly");
		
		// 정비종료시간:MINUTE
		$("#popRepairResultRegForm_REPAIR_TIME2_MINUTE").attr("disabled", "disabled");
		$("#popRepairResultRegForm_REPAIR_TIME2_MINUTE").closest('div').addClass("readOnly");
		
		// 사용자재 검색 버튼 
		$("#MATERIAL_INOUT").closest('td').find('.btn.evn-st01.mgn-t-5').hide();
		
		// 사용자재 수량
		$("input[name=qntyUsedArr]").attr("disabled", "disabled");
		$("input[name=qntyUsedArr]").closest('div').addClass("readOnly");
		
		// 사용자재 삭재버튼
		$("input[name=qntyUsedArr]").closest('div').parents('div').find('.btnRemove').hide();
		
		// 미등록 사용자재
		$("#MATERIAL_UNREG").attr("disabled", "disabled");
		$("#MATERIAL_UNREG").closest('div').addClass("readOnly");
		
		// 미등록 사용자재 비용
		$("#COST").attr("disabled", "disabled");
		$("#COST").closest('div').addClass("readOnly");
		
		// 미등록 사용자재 비용
		$("#modifyBtn").hide();
		$("#COST").closest('div').addClass("readOnly");

	}
	
	<%-- 고장시작/종료시간 변경 시 정비시작/종료시간 동기화 --%>
	$('.TROUBLE_TIME1, .TROUBLE_TIME2').on('change', function(){

		var statusArray = $(this).attr('id').split('_');
		$("#popRepairResultRegForm_REPAIR_" + statusArray[2] + "_" +statusArray[3]).val($(this).val());
	});
	
// 	$("#CATALOG_D_GRP").val("${repairResultDtl.CATALOG_D_GRP}");
// 	if("${repairResultDtl.CATALOG_D_GRP}" != ""){
// 		var flag = 'CATALOG_D';
// 		var flag_html = 'CATALOG_D';
// 		var upperComcd = "${repairResultDtl.CATALOG_D_GRP}";
		
// 		fnSelectCatalogList(flag, flag_html, upperComcd);
// 		$("#CATALOG_D").val("${repairResultDtl.CATALOG_D}");
// 	} 
	
// 	$("#CATALOG_B_GRP").val("${repairResultDtl.CATALOG_B_GRP}");
// 	if("${repairResultDtl.CATALOG_B_GRP}" != ""){
// 		var flag = 'CATALOG_B';
// 		var flag_html = 'CATALOG_B';
// 		var upperComcd = "${repairResultDtl.CATALOG_B_GRP}";
		
// 		fnSelectCatalogList(flag, flag_html, upperComcd);
// 		$("#CATALOG_B").val("${repairResultDtl.CATALOG_B}");
// 	}
	
// 	$("#CATALOG_C_GRP").val("${repairResultDtl.CATALOG_C_GRP}");
// 	if("${repairResultDtl.CATALOG_C_GRP}" != ""){
// 		var flag = 'CATALOG_C';
// 		var flag_html = 'CATALOG_C';
// 		var upperComcd = "${repairResultDtl.CATALOG_C_GRP}";
		
// 		fnSelectCatalogList(flag, flag_html, upperComcd);
// 		$("#CATALOG_C").val("${repairResultDtl.CATALOG_C}");
// 	}
	
// 	$("#CATALOG_5_GRP").val("${repairResultDtl.CATALOG_5_GRP}");
// 	if("${repairResultDtl.CATALOG_C_GRP}" != ""){
// 		var flag = 'CATALOG_5';
// 		var flag_html = 'CATALOG_5';
// 		var upperComcd = "${repairResultDtl.CATALOG_5_GRP}";
		
// 		fnSelectCatalogList(flag, flag_html, upperComcd);
// 		$("#CATALOG_5").val("${repairResultDtl.CATALOG_5}");
// 	}
	    		
// 	$('#regForm [name=CATALOG_D_GRP], #regForm [name=CATALOG_B_GRP], #regForm [name=CATALOG_C_GRP], #regForm [name=CATALOG_5_GRP]').on('change', function(){
// 		var flag_html = $(this).attr("name");
// 		var flag = "";
// 		var upperComcd = $(this).val();
		
// 		if(flag_html == "CATALOG_D_GRP") {
// 			flag = 'CATALOG_D';
// 			flag_html = 'CATALOG_D';
// 		}
// 		else if(flag_html == "CATALOG_B_GRP"){
// 			flag = 'CATALOG_B';
// 			flag_html = 'CATALOG_B';				
// 		}
// 		else if(flag_html == "CATALOG_C_GRP"){
// 			flag = 'CATALOG_C';
// 			flag_html = 'CATALOG_C';
// 		}
// 		else if(flag_html == "CATALOG_5_GRP"){
// 			flag = 'CATALOG_5';
// 			flag_html = 'CATALOG_5';
// 		}
		
// 		if(upperComcd != ""){

// 			fnSelectCatalogList(flag, flag_html, upperComcd);
			
// 		} else {
// 			var html = "";
// 			html += '<option value="">선택하세요</option>'
// 			$("#"+flag_html).html(html);
// 		}
// 	});
	
});

// 정비원들의 확정여부 체크
function WorkStatusCheck(){
	
	// 정비 확정 여부가 N인 사람들
	var member_name = null;
	var work_status_val = null;
	var del_yn = null;
	var work_status_N_member = new Array();
	var j=0;
	
	for (var i=0; i< $("#repairMember2").children().length ; i++ ){
		// 정비실적 확정 여부
		work_status_val = $("#repairMember2").children().eq(i).find("div.name").children("input[name=WORK_STATUS]").val();
		del_yn = $("#repairMember2").children().eq(i).find("div.name").children("input[name=delYnArr2]").val();
		
		// 정비실적이 확정되지 않았을 경우
		if(work_status_val=='N' && del_yn=='N'){
			member_name = $("#repairMember2").children().eq(i).find("div.name").text().replace("작업완료", '').replace("담당자", '').trim();
			work_status_N_member[j] = member_name;
			j++;
		}
	}
	
	if(work_status_N_member!=''){// 작업완료 되지 않은 정비원이 있을경우
	
		var workStatusMsg = "'" + work_status_N_member + "' "
		                  + "<spring:message code='epms.worker' />(이)가 "
		                  + "<spring:message code='epms.work.perform' />을 확정하지 않으셨습니다.\n"
		                  + "'" + work_status_N_member + "' 에게 '작업완료'를 요청하십시오." ;
	
		if ( alert( workStatusMsg ) ) {
			return;
		}
	
	} else {// 모든 정비원이 작업을 완료 했을 경우
		fnSave("COMPLETE");
	}

}

function fnSelectCatalogList(flag, flag_html, upperComcd){
	var html = "";
	var flag_name = flag+"_NAME";
	
	$.ajax({
		type : 'POST',
		url : '/epms/repair/result/selectComCodeList.do',
		dataType : 'json',
		data : { "flag" : flag, "UPPER_COMCD" : upperComcd },
		success : function(json) {	
			
			<%-- 정상적으로 정비실적 등록시 --%>
			if(json != '' && json != null){
				for(var i=0; i < json.comCodeList.length; i++){
					html += '<option value="'+json.comCodeList[i][flag]+'">' + json.comCodeList[i][flag_name] + '</option>'
				}
				
				$("#"+flag_html).html(html);
			<%-- 이미 등록된 정비실적일 경우 --%>
			}else{
				alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
		}
	});
}

<%-- 첨부파일 목록에서 제거 --%>
function delModiFile(obj){
	var attach = obj.data("attach");
	var idx = obj.data('idx');
	
	$('#fileRegLstWrap_'+attach).remove();
	
	if(fileVal != '') {
		fileVal += ",";
	}
	
	fileVal += attach;
	$('#DEL_SEQ').val(fileVal);
}

<%-- 고장/정비 시작/종료시간 기본값 세팅 --%>
function fnSetDefaultTime(){
	
	$(timeArray).each(function(idx, val) {
		
		var saveData = "";
		if(val == "TROUBLE_TIME1") saveData = "${repairResultDtl.TROUBLE_TIME1}";
		else if(val == "TROUBLE_TIME2") saveData = "${repairResultDtl.TROUBLE_TIME2}";
		else if(val == "REPAIR_TIME1") saveData = "${repairResultDtl.REPAIR_TIME1}";
		else if(val == "REPAIR_TIME2") saveData = "${repairResultDtl.REPAIR_TIME2}";
		
		var date = new Date();
		<%-- 임시저장된 값이 있다면 처리 --%>
		if(saveData == "" || saveData == null){
			$('#popRepairResultRegForm_' + val + '_DATE').val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
			$('#popRepairResultRegForm_' + val + '_HOUR').val(getDate(date.getHours()));
			var min = parseInt(date.getMinutes());
			min = min - (min%5);
			var minstr;
			if(min<10){
				minstr = "0" + min.toString();
			}else{
				minstr = min.toString();
			}
			$('#popRepairResultRegForm_' + val + '_MINUTE').val(minstr);
		}
		else{
			$('#popRepairResultRegForm_' + val + '_DATE').val(saveData.substring(0,4) + "-" + saveData.substring(4,6) + "-" + saveData.substring(6,8));
			$('#popRepairResultRegForm_' + val + '_HOUR').val(saveData.substring(8,10));
			$('#popRepairResultRegForm_' + val + '_MINUTE').val(saveData.substring(10,12));
		}
	});
}

<%-- 정비원 추가 버튼 --%>
function fnAddMember(gridNm){
	
	var division = new Array();
	var division_name = new Array();
	var part = new Array();
	var part_name = new Array();
	var member = new Array();
	var member_name = new Array();
	var member_fileurl = new Array();
	
	var selRows = Grids[gridNm].GetSelRows();
	
	if(selRows == null || selRows == ""){
		return;
	}
	
	for(var i=0; i<selRows.length; i++){
		
		if(!(selRows[i].USER_ID=="" || selRows[i].USER_ID==null)){

			division[i] = selRows[i].DIVISION;
			division_name[i] = selRows[i].DIVISION_NAME;
			part[i] = selRows[i].PART;
			part_name[i] = selRows[i].PART_NAME;
			member[i] = selRows[i].USER_ID;
			member_name[i] = selRows[i].NAME;
			member_fileurl[i] = selRows[i].MEMBER_FILEURL;
		}
	}
	
	addMember(division, division_name, part, part_name, member, member_name, member_fileurl, i);
	fnModalToggle('popWorkerList');
}

<%-- 자재추가 버튼 --%>
function fnAddMaterial(gridNm){
	
	var equipment_bom = new Array();
	var material_grp = new Array();
	var material_grp_uid = new Array();
	var material_grp_name = new Array();
	var material = new Array();
	var material_code = new Array();
	var material_name = new Array();
	var unit = new Array();
	var unit_name = new Array();
	
	var type	= new Array();
	var object	= new Array();
	var objtxt	= new Array();
	
	var selRows = Grids[gridNm].GetSelRows();
	
	if(selRows == null || selRows == ""){
		return;
	}
	
	for(var i=0; i<selRows.length; i++){
		if(!(selRows[i].MATERIAL=="" || selRows[i].MATERIAL==null)){
			
			type[i] = selRows[i].TYPE;
			object[i] = selRows[i].OBJECT;
			objtxt[i] = selRows[i].OBJTXT;
			
			equipment_bom[i] = selRows[i].EQUIPMENT_BOM;
			material_grp[i] = selRows[i].MATERIAL_GRP;
			material_grp_uid[i] = selRows[i].MATERIAL_GRP_UID;
			material_grp_name[i] = selRows[i].MATERIAL_GRP_NAME;
			material[i] = selRows[i].MATERIAL;
 			material_code[i] = selRows[i].MATERIAL_UID;
 			material_name[i] = selRows[i].MATERIAL_NAME;
			unit[i] = selRows[i].UNIT;
			unit_name[i] = selRows[i].UNIT_NAME;			
		}

	}
	
	addMaterial(type, object, objtxt, equipment_bom, material_grp, material_grp_uid, material_grp_name, material, material_code, material_name, unit, unit_name, i);
	fnModalToggle('popRepairMaterialList');
}

<%-- 사용자재 '검색'버튼 --%>
function fnSearchMaterial(){
	
	if($('#REPAIR_RESULT').val() == "" || $('#REPAIR_RESULT').val() == null){
		return;
	}
	
	var url = '/epms/equipment/bom/equipmentBomListData.do'
 			+ '?EQUIPMENT=' + $('#popRepairResultRegForm_EQUIPMENT').val()
			+ '&LOCATION=' + $('#popRepairResultRegForm_LOCATION').val();
	
	Grids.popRepairMaterialList.Source.Data.Url = url;
	Grids.popRepairMaterialList.ReloadBody();
	fnModalToggle('popRepairMaterialList');
}

<%-- 추가자재 선택 후 html 작성 --%>
function addMaterial(type, object, objtxt, equipment_bom, material_grp, material_grp_uid, material_grp_name, material, material_code, material_name, unit, unit_name, cnt){
	var html = "";
	
	for(var i=0; i<cnt; i++){
		if($('#usedMaterial').html().indexOf('name="equipmentBomArr" value="' + equipment_bom[i] + '"') < 0 && $('#usedMaterial').html().indexOf('name="equipmentBomUsedArr" value="' + equipment_bom[i] + '"') < 0){	
			html +=
				'<li style="overflow:hidden; margin:5px 0;">'
			+	'	<input type="hidden" class="inp-comm" title="BOMID" name="equipmentBomArr"  value="' + equipment_bom[i] + '"	readonly="readonly">'
			+	'	<input type="hidden" class="inp-comm" title="자재ID" 	name="materialId" 		value="' + material[i] + '"			readonly="readonly">'
			+	'	<input type="hidden" class="inp-comm" title="자재정보"	name="materialArr" 		value="" readonly="readonly">'
			+	'	<input type="hidden" class="inp-comm" title="자재코드" name="material_uidArr"  value="' + material_code[i]  + '"	readonly="readonly">'
			+	'	<input type="hidden" class="inp-comm" title="단위"    name="unitArr" value="' + unit[i] + '"	readonly="readonly">'
			+	'	<div class="box-col wd-per-20 f-l mgn-l-5">'
			+	'		<div class="inp-wrap readonly wd-per-100">'
			+	'			<input type="text" class="inp-comm" title="자재코드" name="" value="' + material_code[i] + '"	readonly="readonly">'
			+	'		</div>'
			+	'	</div>'
			+	'	<div class="box-col wd-per-20 f-l mgn-l-5">'
			+	'		<div class="inp-wrap readonly wd-per-100">'
			+	'			<input type="text" class="inp-comm" title="자재명" name="" value="' + material_name[i] + '"	readonly="readonly">'
			+	'		</div>'
			+	'	</div>'
			+	'	<div class="box-col wd-per-20 f-l mgn-l-5">'
			+	'		<div class="inp-wrap readonly wd-per-100">'
			+	'			<input type="text" class="inp-comm" title="단위명" name="" value="' + unit[i] + '"	readonly="readonly">'
			+	'		</div>'
			+	'	</div>'
			+	'	<div class="box-col wd-per-10 f-l mgn-l-5">'
			+	'		<div class="inp-wrap wd-per-100">'
			+	'			<input type="text" class="inp-comm t-r" title="수량" name="qntyArr"	value="0"	onchange="check_Null(this);"	onkeydown="check_isNum_onkeydown();" autocomplete="off">'
			+	'		</div>'
			+	'	</div>'
			+	'	<a href="#none" class="btnRemove mgn-l-5" onclick="delList2($(this))">삭제</a>'
			+	'</li>';
		}
		// 삭제된 자재 다시 추가
		else if($('#usedMaterial').html().indexOf('name="equipmentBomArr" value="' + equipment_bom[i] + '"') > 0 || $('#usedMaterial').html().indexOf('name="equipmentBomUsedArr" value="' + equipment_bom[i] + '"') > 0){
			var $hide_li = $('#usedMaterial').find("input[value="+equipment_bom[i]+"]").closest("li");
			if($hide_li.css("display") == "none"){
			   $hide_li.find("input[name=delYnArr]").val('N');
			   $hide_li.show();
			}
		}
	};
	
	$('#usedMaterial').append(html);
}

<%-- 정비원 삭제버튼 --%>
function delList1(delLst){
	<%-- 담당자 이름 가져오기 --%>
	var name   = delLst.closest('li').find("div.name").text().replace("작업완료", '').replace("담당자", '').trim();	
	
	var worker = "<spring:message code='epms.worker' />" ;
	
	if(confirm( " '" + name + " " + worker + "'을 삭제 하시겠습니까?")){  // 정비원
		
		if(delLst.siblings("input[name=WORK_LOG_USER_ID]").val() == $('#MANAGER').val()){
			alert("'담당자'는 삭제할 수 없습니다.");
			return;
		}else{
			delLst.closest('li').find("input[name=delYnArr2]").val('Y');
			delLst.closest('li').hide();
			
			var str = "<spring:message code='epms.repair.complete' />" ;
			
			alert("'" + name + " " + worker + "'(이)가 삭제되었습니다.\n ※'임시저장' 또는 '" + str + "'를 해야 변경내용이 저장됩니다.");  // 정비원 , 정비완료
		}
	
	}
	
}

<%-- 사용자재 삭제버튼 --%>
function delList2(delLst){
	delLst.siblings("input[name=delYnArr]").val('Y');
	delLst.closest('li').hide();
}

<%-- 타파트요청,임시저장,정비완료 --%>
function fnSave(type){
	
	var strComplete = "<spring:message code='epms.repair.complete' />" ;  // 정비완료
	
	if(type == "request"){
		
		msg = "정상적으로 '타파트요청' 처리되었습니다.";
		
		var url = "/epms/repair/result/popRequestPartForm.do"
				  + "?LOCATION=" + $('#popRepairResultRegForm_LOCATION').val()
				  + "&REMARKS=" + $('#popRepairResultRegForm_REMARKS').val()
				  + "&PART=" + $('#popRepairResultRegForm_PART').val()
				  + "&EQUIPMENT=" + $('#popRepairResultRegForm_EQUIPMENT').val()
				  + "&REPAIR_REQUEST_ORG=" + $('#popRepairResultRegForm_REPAIR_REQUEST_ORG').val()
				  + "&URGENCY=" + $('#popRepairResultRegForm_URGENCY').val();
		
		$('#popRequestPartForm').load(url, function(responseTxt, statusTxt, xhr){
			$(this).modal();
		});
	}
	else if (type == "SAVE"){
		if(confirm("임시저장 하시겠습니까?")){
			msg = "정상적으로 '임시저장' 처리되었습니다.";
			fnRegAjax(type);
		}
	}
	else if (type == "COMPLETE"){
		if(confirm("*" + strComplete + "하시면 더 이상 수정이 불가능합니다.")){  
			msg = "정상적으로 '" + strComplete + "' 처리되었습니다.";
			fnRegAjax(type);
		}
	}
}

<%-- 정비원 '검색'버튼 --%>
function fnSearchMember(){
	if($('#REPAIR_RESULT').val() == "" || $('#REPAIR_RESULT').val() == null){
		return;
	}
	
	var url = '/epms/repair/result/popWorkerListData.do'
			  + '?SUB_ROOT=' + $('#popRepairResultRegForm_LOCATION').val()
			  +	'&PART=' + $('#popRepairResultRegForm_PART').val()
			  + '&REMARKS=' + $('#popRepairResultRegForm_REMARKS').val();
	
	Grids.WorkerStatList.Source.Data.Url = url;
	Grids.WorkerStatList.ReloadBody();
	fnModalToggle('popWorkerList');
}

<%-- 임시저장,정비완료 ajax --%>
function fnRegAjax(type){

	console.log('# popRepairResultRegForm.fnRegAjax (' + type + ') called ...');
	
	submitBool = false;
	
	
	$(timeArray).each(function(idx, val) {
		
		var temp =       $('#popRepairResultRegForm_' + val + '_DATE').val() 
		         + " " + $('#popRepairResultRegForm_' + val + '_HOUR').val() 
		         + ":" + $('#popRepairResultRegForm_' + val + '_MINUTE').val() + ":00" ;
		
		$('#popRepairResultRegForm_' + val).val( temp );
		
	});
	
	
	var repair_time =       $('#popRepairResultRegForm_REPAIR_TIME2_DATE').val().substring(0,4) 
	                + "-" + $('#popRepairResultRegForm_REPAIR_TIME2_DATE').val().substring(4,6) 
	                + "-" + $('#popRepairResultRegForm_REPAIR_TIME2_DATE').val().substring(6,8) ;
	
	var repairDateTime = repair_time 
	                   + " " + $('#popRepairResultRegForm_REPAIR_TIME2_HOUR').val() 
	                   + ":" + $('#popRepairResultRegForm_REPAIR_TIME2_MINUTE').val() + ":00" ;
	
	$('#DATE_REPAIR').val( repairDateTime );
	
	if(type == "${FLAG_COMPLETE}"){
		
		if (!isNull_J($('#REPAIR_TYPE'), " <spring:message code='epms.repair.type' />을 선택해주세요. " )) {  // 정비유형
			submitBool = true;
			return false;
		}
		
		if (!isNull_J($('#REPAIR_DESCRIPTION'), " 작업내용을 입력해주세요. ")) {
			submitBool = true;
			return false;
		}
		
		if($('#popRepairResultRegForm_TROUBLE_TIME1').val() > $('#popRepairResultRegForm_TROUBLE_TIME2').val()){
			submitBool = true;
			alert(" <spring:message code='epms.occurr.time.end' />이 <spring:message code='epms.occurr.time.start' />보다 이전이 될 수 없습니다.");  // 고장종료시간 , 고장시작시간
			return false;
		}
		
		if($('#popRepairResultRegForm_REPAIR_TIME1').val() > $('#popRepairResultRegForm_REPAIR_TIME2').val()){
			submitBool = true;
			alert("<spring:message code='epms.repair.time.end' />이 <spring:message code='epms.repair.time.start' />보다 이전이 될 수 없습니다.");  // 정비종료시간 , 정비시작시간
			return false;
		}
		
		var date = new Date();
		var sysdate = date.getFullYear() + '' + getDate(parseInt(date.getMonth())+1) + '' + getDate(date.getDate()) + ' ' + getDate(date.getHours()) + ":" + getDate(date.getMinutes()) + ":" + getDate(date.getSeconds());
		
		if($('#popRepairResultRegForm_TROUBLE_TIME2').val() > sysdate){
			submitBool = true;
			alert(" <spring:message code='epms.occurr.time.end' />이 현재보다 미래일 수 없습니다.");  // 고장종료시간
			return false;
		}
		
		var sysdate = date.getFullYear() + '' + getDate(parseInt(date.getMonth())+1) + '' + getDate(date.getDate()) + ' ' + getDate(date.getHours()) + ":" + getDate(date.getMinutes()) + ":" + getDate(date.getSeconds());
		if($('#popRepairResultRegForm_REPAIR_TIME2').val() > sysdate){
			submitBool = true;
			alert(" <spring:message code='epms.repair.time.end' />이 현재보다 미래일 수 없습니다.");  // 정비종료시간
			return false;
		}
		
		if (!isNull_J($('#TROUBLE_TYPE1'), " <spring:message code='epms.occurr.type' />을 선택해주세요. " )) {  // 고장유형
			submitBool = true;
			return false;
		}
		if (!isNull_J($('#TROUBLE_TYPE2'), " <spring:message code='epms.active' />을 선택해주세요. " )) {  // 조치유형
			submitBool = true;
			return false;
		}
			
	}
	
	var fileCnt = $("input[name=fileGb]").length;
	<%-- 정비사진 유무 세팅 --%>
	if (fileCnt > 0) {
		$('#attachExistYn').val("Y");
	} else {
		$('#attachExistYn').val("N");
	}

	<%-- 정비원 정보 등록 --%>
	$('input[name=memberArr]').attr('value',convertMemberArrToJSON('WORK_LOG', 'WORK_LOG_USER_ID', 'WORK_STATUS', 'delYnArr2'));
	
	$('input[name=materialUsedArr]').attr('value',convertMaterialArrToJSON('unitArr','material_uidUsedArr','usedMaterialId', 'qntyUsedArr', 'equipmentBomUsedArr', 'delYnArr'));
	$('input[name=materialArr]').attr('value',convertMaterialArrToJSON('unitArr','material_uidArr', 'materialId', 'qntyArr', 'equipmentBomArr', ''));
	
	<%-- form 데이터 전송시 WORK_STATUS는 memberArr담겨서 간다 --%>
	$("input[name=WORK_STATUS]").attr('disabled', true);
	
	<%-- 미사용 자재 단가 등록시 콤마(,) 제거 --%>
	$('#COST').val($('#COST').val().replace(/\D/g,""));
	
	var form = new FormData(document.getElementById('regForm'));
	
	$.ajax({
		type : 'POST',
		url : '/epms/repair/result/repairResultRegAjax.do',
		dataType : 'json',
		data : form,
		processData : false,
		contentType : false,
		success : function(json) {	
			
			if (type == "SAVE") { <%-- 임시저장 등록시 --%>
			
				fnModalToggle('popRepairResultRegForm');
				fnReload( msg , 'popRepairResultRegForm' );
				submitBool = true;
				
			} else if(type == "COMPLETE") {
				
				if(json.updateYn == 'SUCCESS'){
				
					if(json.resultDtl.E_RESULT == 'S'){
						fnModalToggle('popRepairResultRegForm');
						fnReload( msg , 'popRepairResultRegForm' );
						submitBool = true;
					}
					else{
						
						alert("처리도중 에러가 발생하였습니다.\n 시스템 관리자에게 문의 하시기 바랍니다.\n" + json.E_MESSAGE);
						submitBool = true;
						return;
					}
				
				}
				else if(json.updateYn == 'NO_REPAIR'){
					alert("<spring:message code='epms.work.perform' /> 상태를 확인해 주시기 바랍니다.");  // 정비실적
					submitBool = true;
					return;
				}
				else if(json.updateYn == 'NO_COMPLETE'){
					alert("개인실적 완료여부 확인해 주시기 바랍니다.");
					submitBool = true;
					return;
				}
				else if(json.updateYn == 'NO_REPAIR_TIME'){
					alert("<spring:message code='epms.repair.time.end' />은 개인작업종료시간보다 이전일 수 없습니다.");  // 정비종료시간
					submitBool = true;
					return;
				}
				else{
					alert("<spring:message code='epms.work.perform' /> 등록에 실패하였습니다.");  // 정비실적
					submitBool = true;
					return;
				}
				// if(json.updateYn == 'SUCCESS')
					
			}
			// if (type == "SAVE")
			
			<%-- 정상적으로 정비실적 등록시 --%>
// 			if(json != '' && json != null){
// 				fnModalToggle('popRepairResultRegForm');
// 				fnReload(msg);
// 				submitBool = true;
			<%-- 이미 등록된 정비실적일 경우 --%>
// 			}else{
// 				alert("처리도중 에러가 발생하였습니다 . 시스템 관리자에게 문의 하시기 바랍니다");
// 				submitBool = true;
// 				return;
// 			}

		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
			submitBool = true;
		}
	});
	
}

<%-- 정비원 개별실적 작업완료 취소 --%>
function fnWorkLogCancel(obj, SAVE_YN){
	
	var msg = null;
	msg = "'작업완료 취소' 하시겠습니까?";
	
	var $div = $(obj).closest("li");
	$div.find("input[name=WORK_STATUS]").val(SAVE_YN);
	
	var WORK_ID   	= $("#REPAIR_RESULT").val();
	var WORK_LOG 	= $div.find("input[name=WORK_LOG]").val();
	var WORK_STATUS = $div.find("input[name=WORK_STATUS]").val();
	
	if(confirm("'작업완료 취소' 하시겠습니까?")){
		$.ajax(
		        {
		            url: '/epms/repair/result/workLogCancelAjax.do'
					, type : 'POST'
					, dataType : 'json'
					, data : { WORK_ID : WORK_ID,
							   WORK_LOG : WORK_LOG,
							   WORK_STATUS : WORK_STATUS
							 }
		            , success: function (result) {
						alert("정상적으로 '작업완료 취소' 되었습니다.");
						submitBool = true;
						$div.find('.repairTime').html('');
						updateWorkLogItemList($div,result,SAVE_YN);
						
		            	
		        	}
		            , error: function (XMLHttpRequest, textStatus, errorThrown) {
		            	alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
	        		}
	       });
		
	}
	
}

<%-- 정비원 개별실적 임시저장 --%>
function fnWorkLogReg(obj, SAVE_YN){
	
	var msg = null;
	
	if(SAVE_YN == 'N') {
		msg = "'임시저장' 하시겠습니까?";
		$("#flag").val('SAVE');
	} else if(SAVE_YN == 'Y') {
		msg = "'작업완료' 하시겠습니까?";
		$("#flag").val('COMPLETE');
		
	}
	var $obj_li = $(obj).closest("li");
	$obj_li.find("input[name=WORK_STATUS]").val(SAVE_YN);
	
			
	if(confirm(msg)){

		<%-- 정비원 개별실적 정보 등록 --%>
		$('input[name=workLogItemArr]').attr('value',convertWorkLogArrToJSON($obj_li, 'USER_ID', 
				'WORK_LOG', 'WORK_LOG_ITEM', 'DATE_START', 'DATE_END', 'ITEM_DESCRIPTION', 'DEL_YN'));

		var form = new FormData(document.getElementById('regForm'));
		
		$.ajax(
	        {
	            url: '/epms/repair/result/workLogRegAjax.do'
				, type : 'POST'
				, dataType : 'json'
				, data : form
				, async : false
				, processData : false
				, contentType : false
	            , success: function (json) {
	            	if(json.updateYn == 'SUCCESS'){
		            	if(SAVE_YN == 'N') {
							msg = "정상적으로 '임시저장'되었습니다.";
						} else if(SAVE_YN == 'Y') {
							msg = "정상적으로 '작업완료'되었습니다.";
						}
		            	
		            	alert(msg);
						submitBool = true;
						$obj_li.find('.repairTime').html('');
						updateWorkLogItemList($obj_li,json,SAVE_YN);
	            	}

					else if(json.updateYn == 'PARAM_NULL'){
						alert("수신 파라미터가 NULL 입니다.");
						submitBool = true;
						return;
					}
					else if(json.updateYn == 'NO_WORK_LOG'){
						alert("등록 가능한 개인실적이 없습니다.");
						submitBool = true;
						return;
					}
					else if(json.updateYn == 'NO_WORK_LOG_TIME'){
						alert("개인작업종료시간은 개인작업시작시간보다 이전일 수 없습니다.");
						submitBool = true;
						return;
					}
					else{
						alert("개인작업실적 등록에 실패하였습니다.");
						submitBool = true;
						return;
					}
					
	        	}
	            , error: function (XMLHttpRequest, textStatus, errorThrown) {
	            	alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
        		}
       });
	}
}

<%-- 정비원 개별실적 임시저장 / 정비완료 / 정비완료 취소시 화면그리기 --%>
function updateWorkLogItemList($obj_li,result,save_type){
	
	$.each(result.workLogItem, function(i, obj) {
		
		var repairMemberItemViewTemplate = null;
		
		if(save_type == "Y"){
			repairMemberItemViewTemplate = $("#repairMemberItemViewTemplateY").html();
		} else if(save_type == "N"){
			repairMemberItemViewTemplate = $("#repairMemberItemViewTemplateN").html();
		}
		repairMemberItemViewTemplate = $(repairMemberItemViewTemplate);
		
			for (var name in obj) {
				switch (name) {
					case "DATE_START_DATE" :
						repairMemberItemViewTemplate.find("[name=DATE_START_DATE]").val(obj[name]);
						break;
						
					case "DATE_START_HOUR" : 
						repairMemberItemViewTemplate.find("[name=DATE_START_HOUR]").val(obj[name]);
						break;
						
					case "DATE_START_MINUTE" :
						repairMemberItemViewTemplate.find("[name=DATE_START_MINUTE]").val(obj[name]);
						break;
						
					case "DATE_END_DATE" :
						repairMemberItemViewTemplate.find("[name=DATE_END_DATE]").val(obj[name]);
						break;
						
					case "DATE_END_HOUR" :
						repairMemberItemViewTemplate.find("[name=DATE_END_HOUR]").val(obj[name]);
						break;
						
					case "DATE_END_MINUTE" :
						repairMemberItemViewTemplate.find("[name=DATE_END_MINUTE]").val(obj[name]);
						break;
						
					case "DESCRIPTION" :
						repairMemberItemViewTemplate.find("[name=ITEM_DESCRIPTION]").val(obj[name]);
						break;
						
					default :
						repairMemberItemViewTemplate.find("[name=" + name + "]").val(obj[name]);
						break;
				}
			}
			$obj_li.find(".repairTime").append(repairMemberItemViewTemplate);
	});
	
	if(save_type=="Y"){
		$obj_li.find('.repairMemberTitIn').css({'border-left':'5px solid #38ca58'});
		$obj_li.find('div.name').append('<span class="fixBtn">작업완료</span>');
		$obj_li.find('div.repairMemberBtn').remove();
		$obj_li.find('div.repairMemberTitIn').append('<div class="repairMemberBtn3"><a href="#none" class="treeButton repairReject" onclick="fnWorkLogCancel(this,\'N\')">작업완료취소</a></div>');
		
	} else if(save_type=="N"){ // 임시저장 또는 확정취소시
		$obj_li.find('.repairMemberTitIn').css({'border-left':''}); //css 제거
		if($obj_li.find('div.repairMemberBtn3').length){
			$obj_li.find('span.fixBtn').remove();
			$obj_li.find('div.repairMemberBtn3').remove();
			
			var html = '';
			
			html +=
				'<div class="repairMemberBtn">'
			+	'	<a href="#none" class="treeButton repairAdd"  onclick="repairMemberControll.add(this)">추가</a>'
			+	'	<a href="#none" class="treeButton repairTemp" onclick="fnWorkLogReg(this,\'N\')">임시저장</a>'
			+	'	<a href="#none" class="treeButton repairSave" onclick="fnWorkLogReg(this,\'Y\')">작업완료</a>'
			+	'</div>';
			
			$obj_li.find('div.repairMemberTitIn').append(html);
		}
	}
	
	<%-- 정비원 개별실적이 1건만 있을경우 : 버튼 숨김처리 --%>
	if($obj_li.find('.repairTimeAddLst[value="N"]').length==1){
		$('a.repairTimeRowDel.deleteBtn').hide();
	};
	
	<%-- 동적 html 생성후 DatePickers 재호출 --%>
	reCallDatePickers();
	
}

<%-- 사용자재 input 값들을 JSON화 --%>
function convertMaterialArrToJSON(unit, material_uid, material, qnty, equipment, delYn){
	
	var tempArrSize = $("input[name='"+material+"']").length;
	var tempArr = new Array();
	
	for(var i = 0; i < tempArrSize; i++){
		var tempObj = new Object();
		tempObj.MATERIAL_UID = $("input[name='"+material_uid+"']")[i].value;
		tempObj.MATERIAL = $("input[name='"+material+"']")[i].value;
		tempObj.QNTY = $("input[name='"+qnty+"']")[i].value;
		tempObj.UNIT = $("input[name='"+unit+"']")[i].value;
		tempObj.EQUIPMENT_BOM = $("input[name='"+equipment+"']")[i].value;
		if(delYn != ''){
			tempObj.DEL_YN = $("input[name='"+delYn+"']")[i].value;
		}
		tempArr.push(tempObj);
	}
	
	return JSON.stringify(tempArr);
}

<%-- 정비원 input 값들을 JSON화 --%>
function convertMemberArrToJSON(work_log, work_log_user_id, work_status, delYn){
	
	var tempArrSize = $("input[name='"+work_log_user_id+"']").length;
	var tempArr = new Array();
	
	for(var i = 0; i < tempArrSize; i++){
		var tempObj = new Object();
		tempObj.WORK_LOG    = $("input[name='"+work_log+"']")[i].value;
		tempObj.WORK_STATUS = $("input[name='"+work_status+"']")[i].value;
		tempObj.USER_ID     = $("input[name='"+work_log_user_id+"']")[i].value;
		if(delYn != ''){
			tempObj.DEL_YN  = $("input[name='"+delYn+"']")[i].value;
		}
		tempArr.push(tempObj);
	}
	
	return JSON.stringify(tempArr);
}




<%-- 정비원 개별실적 input 값들을 JSON화 --%>
function convertWorkLogArrToJSON($obj_li, user_id, work_log, work_log_item, date_start, date_end, item_description, delYn){
	
	var $obj_li = $obj_li;
	var obj_li_lenth = $obj_li.find("input[name='"+date_start+"']").length;
	var tempArrSize = obj_li_lenth;
	var tempArr = new Array();
	
	<%-- 정비원 개별실적 정비시작 시간 --%>
	var date_start_date 	= null;
	var date_start_hour   	= null;
	var date_start_minute   = null;
	
	<%-- 정비원 개별실적 정비종료 시간 --%>
	var date_end_date 	  = null;
	var date_end_hour     = null;
	var date_end_minute   = null;
	
	for(var i = 0; i < tempArrSize; i++){
		var tempObj = new Object();
		
		tempObj.USER_ID       = $obj_li.find("input[name='"+user_id+"']")[0].value;
		
		tempObj.WORK_LOG      =	$obj_li.find("input[name='"+work_log+"']")[0].value;
		
		tempObj.WORK_LOG_ITEM =	$obj_li.find("input[name='"+work_log_item+"']")[i].value;
		                      
		date_start_date		  =	$obj_li.find("input[name=DATE_START_DATE]")[i].value;
		date_start_hour		  =	$obj_li.find("select[name=DATE_START_HOUR] option:selected")[i].value;
		date_start_minute	  =	$obj_li.find("select[name=DATE_START_MINUTE] option:selected")[i].value;
		tempObj.DATE_START    = date_start_date+" " +date_start_hour+":"+date_start_minute+":00";
		                      
		date_end_date		  =	$obj_li.find("input[name=DATE_END_DATE]")[i].value;
		date_end_hour		  =	$obj_li.find("select[name=DATE_END_HOUR] option:selected")[i].value;
		date_end_minute		  =	$obj_li.find("select[name=DATE_END_MINUTE] option:selected")[i].value;
		tempObj.DATE_END 	  = date_end_date+" " +date_end_hour+":"+date_end_minute+":00";
		                      
		tempObj.DESCRIPTION   = $obj_li.find("textarea[name='"+item_description+"']")[i].value;
		                      
		if(delYn != ''){      
			tempObj.DEL_YN    = $obj_li.find("input[name='"+delYn+"']")[i].value;
		}
		
		tempArr.push(tempObj);
	}
	
	return JSON.stringify(tempArr);
}

<%-- 수량 null 체크 --%>
function check_Null(obj){
	if(obj.value == ""){
		obj.value = "0";
	}
}

<%-- 총중량,취득가액 콤마처리 --%>
var rgx1 = /\D/g;  <%-- /[^0-9]/g 와 같은 표현 --%>
var rgx2 = /(\d+)(\d{3})/; 

function getNumber(obj) {
	if(obj.value.charAt(obj.value.length-1) == "."){
		var parts=obj.value.toString().split(".");
		parts[0] = parts[0].replace(rgx1,"");
		obj.value = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ".";
	}
	else{
		var parts=obj.value.toString().split(".");
		parts[0] = parts[0].replace(rgx1,"");
		obj.value = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
	}
}
 
<%-- 총중량,취득가액 숫자 유효성 검사 --%> 
function check_isNum_onkeydown(){

	$(this).val( $(this).val().replace(/[^-0-9.,]/gi,"") );
	$(this).val(number_format($(this).val()));
}

<%-- 이미지 원본크기 보기 --%>
function originFileView(img){ 
	img1= new Image(); 
	img1.src=(img); 
	imgControll(img); 
} 
	  
function imgControll(img){ 
	if((img1.width!=0)&&(img1.height!=0)){ 
		viewImage(img); 
	}else{ 
		controller="imgControll('"+img+"')"; 
		intervalID=setTimeout(controller,20); 
	} 
}

function viewImage(img){ 
	W=img1.width; 
	H=img1.height; 
	O="width="+W+",height="+H+",scrollbars=yes"; 
	imgWin=window.open("","",O); 
	imgWin.document.write("<html><head><title>이미지상세보기</title></head>");
	imgWin.document.write("<body topmargin=0 leftmargin=0>");
	imgWin.document.write("<img src="+img+" onclick='self.close()' style='cursor:pointer;' title ='클릭하시면 창이 닫힙니다.'>");
	imgWin.document.close();
}

<%-- 정비원 workLogItem추가, 삭제 --%>
var repairMemberControll = {
	add : function (obj){
		var date = new Date();
		var min = parseInt(date.getMinutes());
		min = min - (min%5);
		var minstr;
		if(min<10){
			minstr = "0" + min.toString();
		}else{
			minstr = min.toString();
		}
		
		var addItem = $("#repairMemberItemAddTemplate").html();
		addItem = $(addItem);
		var $div = $(obj).closest("li").find("#editForm2").find("div.repairTime").append(addItem);
		var $div2 = $div.find("div.repairTimeAddLst:last");
		$div2.find("input.moDatePic").val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		$div2.find("#popRepairResultRegForm_REPAIR_TIME1_HOUR").val(getDate(date.getHours()));
		$div2.find("#popRepairResultRegForm_REPAIR_TIME1_MINUTE").val(minstr);
		$div2.find("#popRepairResultRegForm_DATE_END_HOUR").val(getDate(date.getHours()));
		$div2.find("#popRepairResultRegForm_DATE_END_MINUTE").val(minstr);
		
		<%-- 동적 html 생성후 DatePickers 재호출 --%>
		reCallDatePickers();
		
		<%-- 아이템이 추가 되었을경우 삭제 버튼 모두 보여줌 --%>
		$('a.repairTimeRowDel.deleteBtn').show();
						
	}
	, remove : function (obj) {
		
		// 실적 아이템 삭제시 DEL_YN에 Y값 넣어주고, 숨기기
		var $div = $(obj).closest("div.repairTimeAddLst");
		$div.find('input[name=DEL_YN]').val('Y');
 		$div.hide();

 		// 실적 아이템이 1개 일때 삭제 버튼 숨기기
 		if($(obj).closest('li').find('input[name=DEL_YN][value="N"]').length==1){
 			$('a.repairTimeRowDel.deleteBtn').hide();
 		}
 		
 		<%-- 삭제 아이템 맨 아래로 옮기기  --%>
// 		$("#removeItemList").append($div);		
	
	} 
	, tempReg : function (obj) {
		
	}
	
}

function reCallDatePickers() {
	
    reCallScript2();
    
    $('.moDatePic').on('click', function () {
        $(this).datepicker({ dateFormat:"yy-mm-dd", selectOtherMonths : false, showOtherMonths:true , prevText : "click for previous months", nextText : "click for next months", showButtonPanel: true, changeMonth: true, changeYear: true, showOn: 'focus' }).focus();
    });
    
}

<%-- 정비원추가 선택 후 html 작성 --%>
function addMember(division, division_name, part, part_name, member, member_name, member_fileurl, cnt){
	
	var date = new Date();
	var html = "";
	
	var min = parseInt(date.getMinutes());
	min = min - (min%5);
	var minstr;
	if(min<10){
		minstr = "0" + min.toString();
	}else{
		minstr = min.toString();
	}
		
	for(var i=0; i<cnt; i++){
		if($('#repairMember2').html().indexOf('name="WORK_LOG_USER_ID" value="'+member[i]+'"') < 0){
			html +=
				'<li style="overflow:hidden;">'
			+	'	<div class="repairMemberTit">'
			+	'		<div class="repairMemberTitIn">'
			+	'			<div class="userInfo">'
			+	'				<div class="user_img">'
			+	'					<img src="'+member_fileurl[i]+'" onerror="this.src=\'/images/com/web_v2/userIcon.png\'" />'
			+	'				</div>'
			+	'				<div class="user_name">'
			+	'					<div class="name">'			
			+	'						<input type="hidden" class="inp-comm" title="작업이력ID" name="WORK_LOG" value="">'
			+	'						<input type="hidden" class="inp-comm" title="등록상태" name="WORK_STATUS" value="N"/>'
			+	'						<input type="hidden" class="inp-comm" title="정비원ID" name="WORK_LOG_USER_ID" value="'+member[i]+'">'
			+	'						'+member_name[i]+''
			+	'						<input type="hidden" class="inp-comm" title="정비원정보" name="memberArr" value=""	readonly="readonly">'
			+	'						<input type="hidden" class="inp-comm" title="삭제여부" name="delYnArr2" value="N"	readonly="readonly">'
			+	'					</div>'
			+	'					<div class="dep">'
			+	'						<span>'
			+	'							<input type="hidden" class="inp-comm" title="부서ID" name="" value="'+division[i]+'"	readonly="readonly">'
			+	'							'+division_name[i]+''
			+ 	'						</span>'
			+	'						<span style="padding:0 5px;">/</span>'
			+	'						<span>'
			+	'							<input type="hidden" class="inp-comm" title="파트" name="" value="'+part[i]+'"	readonly="readonly">'
			+	'							'+part_name[i]+''
			+	'						</span>'
			+	'					</div>'
			+	'				</div>'
			+	'			</div>'
			+	' 			<div class="repairMemberBtn2">'
			+	' 				<a href="#none" class="treeButton repairDelete" onclick="delList1($(this))">삭제</a>'
			+	' 			</div>'
			+	'		</div>'
			+	'	</div>'
			+	'	<div class="repairAccOpenBtn">'
			+	'		<a href="#none" title="<spring:message code='epms.worker' /> 상세보기"></a>'
			+	'	</div>'
			+	'</li>';
			
		}
		// 삭제된 정비원 다시 추가
		else if($('#repairMember2').html().indexOf('name="WORK_LOG_USER_ID" value="'+member[i]+'"') > 0){
			var $hide_li = $('#repairMember2').find("input[name=WORK_LOG_USER_ID][value="+member[i]+"]").closest("li");
			if($hide_li.css("display") == "none"){
			   // 삭제여부 -> "N"으로 변경	
			   $hide_li.find("input[name=delYnArr2]").val('N');
			   // show()처리
			   $hide_li.show();
			}
		}
	};
	
	$('#repairMember2').append(html);
	
	reCallScript1();
	reCallDatePickers();
    
}

<%-- 동적 html 생성후 script 재호출 --%>
function reCallScript1(){
	var script = document.createElement("script");  // create a script DOM node
	script.src = "/js/com/web/accordion.js"; 		// set its src to the provided URL
	document.head.appendChild(script);				// add it to the end of the head section of the page (could change 'head' to 'body' to add it to the end of the body section instead)
}

function reCallScript2(){
	var script = document.createElement("script");  
	script.src = "/js/com/web/common.js"; 			
	document.head.appendChild(script);				
}

<%-- 숫자만 입력 --%>
function getNumber(obj){
    
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(/\D/g,""); //숫자가 아닌것을 제거, 
                                     //즉 [0-9]를 제외한 문자 제거; /[^0-9]/g 와 같은 표현
    num01 = setComma(num02); //콤마 찍기
    obj.value =  num01;
    
}

<%-- 자동 콤마 추가 --%>
function setComma(n) {
    var reg = /(^[+-]?\d+)(\d{3})/;   // 정규식
    n += '';                          // 숫자를 문자열로 변환         
    while (reg.test(n)) {
       n = n.replace(reg, '$1' + ',' + '$2');
    }         
    return n;
}

</script>
<style>
	.inp-comm.moDatePic {box-sizing:border-box;height:28px;padding:5px 25px 4px 6px;border:1px solid #cdd2da; background:#fff url('/images/com/web/ico_cal.png') no-repeat right 5px center;background-size: 17px auto;cursor:pointer;}
</style>

<div class="modal-dialog root wd-per-70">
	<div class="modal-content">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title">
		   	    <c:if test="${repairResultDtl.URGENCY eq '02'}"><span style="color:red;font-weight:bold;">[긴급] </span></c:if>
		   	        <spring:message code='epms.perform.regist' />
		   	</h4><!-- 정비실적 등록 -->
		</div>
	 	<div class="modal-body overflow">
           	<div class="modal-bodyIn hgt-per-100" style="padding:0;">
				<input type="hidden" id="userId" name="userId" value="${userId}"/>
				
					<div class="popTabSet">
						<ul class="tabs">
							<c:choose>
								<c:when test="${repairResultDtl.APPR_STATUS eq '02' && repairResultDtl.REPAIR_STATUS eq '01'}">
									<li id="repairRequestTab"><a href="#panel1-1" class="on">요청내용</a></li>
									<li id="repairResultTab"><a href="#panel1-2">실적등록</a></li>
								</c:when>
								<c:otherwise>
									<li id="repairRequestTab"><a href="#panel1-1">요청내용</a></li>
									<li id="repairResultTab"><a href="#panel1-2" class="on">실적등록</a></li>
								</c:otherwise>
							</c:choose>
						</ul>
						<div class="panels">
							<%-- 요청내용 판넬 : s --%>
							<div class="panel type01" id="panel1-1">
								<div class="panel-in rel">
									<div class="tb-wrap">
										<table class="tb-st">
											<caption class="screen-out">요청내용 상세</caption>
											<colgroup>
												<col width="15%" />
												<col width="35%" />
												<col width="15%" />
												<col width="*" />
											</colgroup>
											<tbody>
												<tr>
													<th scope="row"><spring:message code='epms.repair.status' /></th><!-- 정비상태 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="REPAIR_STATUS" name="REPAIR_STATUS" value="${repairResultDtl.REPAIR_STATUS}"/>
															<input type="text" class="inp-comm" id="repair_status_name" name="repair_status_name" readonly="readonly" value="${repairResultDtl.REPAIR_STATUS_NAME}"/>
														</div>
													</td>
													<th scope="row"><spring:message code='epms.repair.urgency' /></th><!-- 긴급도 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="popRepairResultRegForm_URGENCY" name="URGENCY" value="${repairResultDtl.URGENCY}"/>
															<input type="text" class="inp-comm" id="popRepairResultRegForm_urgency_name" name="urgency_name" readonly="readonly" value="${repairResultDtl.URGENCY_NAME}" <c:if test="${repairResultDtl.URGENCY eq '02'}"> style="color:red;font-weight:bold;" </c:if>/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row"><spring:message code='epms.category' /></th><!-- 공장 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="popRepairResultRegForm_LOCATION" name="LOCATION" value="${repairResultDtl.LOCATION}"/>
															<input type="text" class="inp-comm" id="popRepairResultRegForm_location_name" name="location_name" readonly="readonly" value="${repairResultDtl.LOCATION_NAME}"/>
														</div>
													</td>
													<th scope="row"><spring:message code='epms.repair.part' /></th><!-- 처리파트 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="popRepairResultRegForm_PART" name="PART" value="${repairResultDtl.PART}"/>
															<input type="text" class="inp-comm" id="popRepairResultRegForm_part_name" name="part_name" readonly="readonly" value="${repairResultDtl.PART_NAME}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row"><spring:message code='epms.category' /></th><!-- 라인 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="CATEGORY" name="CATEGORY" value="${repairResultDtl.CATEGORY}"/>
															<input type="text" class="inp-comm" id="category_name" name="category_name" readonly="readonly" value="${repairResultDtl.CATEGORY_NAME}"/>
														</div>
													</td>
													<th scope="row"><spring:message code='epms.repair.request.date' /></th><!-- 요청일시 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" id="DATE_REQUEST" name="DATE_REQUEST" readonly="readonly" value="${repairResultDtl.DATE_REQUEST}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row"><spring:message code='epms.system.uid' /></th><!-- 설비코드 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" id="popRepairResultRegForm_EQUIPMENT_UID" name="EQUIPMENT_UID" readonly="readonly" value="${repairResultDtl.EQUIPMENT_UID}"/>
														</div>
													</td>
													<th scope="row"><spring:message code='epms.repair.request.part' /></th><!-- 요청부서 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="REQUEST_DIV" name="REQUEST_DIV" value="${repairResultDtl.REQUEST_DIV}"/>
															<input type="text" class="inp-comm" id="request_div_name" name="request_div_name" readonly="readonly" value="${repairResultDtl.REQUEST_DIV_NAME}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row"><spring:message code='epms.system.name' /></th><!-- 설비명 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" id="EQUIPMENT_NAME" name="EQUIPMENT_NAME" readonly="readonly" value="${repairResultDtl.EQUIPMENT_NAME}"/>
														</div>
													</td>
													<th scope="row"><spring:message code='epms.repair.request' /></th><!-- 요청자 -->
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="REQUEST_MEMBER" name="REQUEST_MEMBER" value="${repairResultDtl.REQUEST_MEMBER}"/>
															<input type="text" class="inp-comm" id="request_member_name" name="request_member_name" readonly="readonly" value="${repairResultDtl.REQUEST_MEMBER_NAME}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row"><spring:message code='epms.request.content' /></th><!-- 고장내용 -->
													<td colspan="3">
														<div class="txt-wrap readonly">
															<div class="txtArea">
																<textarea cols="" rows="5" title="고장내용" id="REQUEST_DESCRIPTION" name="REQUEST_DESCRIPTION" readonly="readonly">${repairResultDtl.REQUEST_DESCRIPTION}</textarea>
															</div>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row"><spring:message code='epms.repair.manager' /></th><!-- 담당자 -->
													<td colspan="3">
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" id="manager_name" name="manager_name" readonly="readonly" value="${repairResultDtl.MANAGER_NAME}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row"><spring:message code='epms.material.FILE' /></th><!-- 고장 첨부파일 -->
													<td colspan="3">
														<c:choose>
															<c:when test="${fn:length(attachList1)>0}">
															<div id="previewList" class="wd-per-100">
																<div class="bxType01 fileWrap previewFileLst">
																	<div>
																		<div class="fileList type02">
																			<div id="detailFileList0" class="fileDownLst">
																				<c:forEach var="item" items="${attachList1}" varStatus="idx">
																					<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																						<span class="MultiFile-export">${item.SEQ_DSP}</span> 
																						<span class="MultiFile-title" title="File selected: ${item.NAME}">
																						${item.FILE_NAME} (${item.FILE_SIZE_TXT})
																						</span>
																						<a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a>
																					</div>		
																				</c:forEach>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
					
															<div id="break_previewImg" class="wd-per-100">
																<div class="previewFile mgn-t-15">
																	<h3 class="tit-cont preview" id="previewTitle">이미지 미리보기</h3>
																	<ul style="margin-top:5px;">
																		<c:forEach var="item" items="${attachList1}" varStatus="idx">
																			<c:if test = "${item.FORMAT eq 'jpg' || item.FORMAT eq 'png' || item.FORMAT eq 'jpeg' || item.FORMAT eq 'JPG' || item.FORMAT eq 'PNG'}">
																				<li>
																					<img src="/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}" title="클릭하시면 원본크기로 보실 수 있습니다." onclick="originFileView('/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}')" />
																					<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																						<span class="MultiFile-title" title="File selected: ${item.NAME}">
																						<p>${item.FILE_NAME} (${item.FILE_SIZE_TXT})</p>
																						</span>
																						<a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a>
																					</div>
																				</li>
																			</c:if>
																		</c:forEach>
																	</ul>
																</div>
															</div>
															</c:when>
															<c:otherwise>
																<div class="f-l wd-per-100 mgn-t-5">
																※ 등록된 첨부파일이 없습니다.
																</div>
															</c:otherwise>
														</c:choose>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<%-- 요청내용 판넬 : e --%>
							
							<%-- 실적등록 판넬 : s --%>
							<div class="panel type02" id="panel1-2">
								<div class="panel-in">
									
									<c:choose>
										<c:when test="${repairResultDtl.REPAIR_STATUS ne EPMS_REPAIR_STATUS_COMPLETE}">
											<c:if test="${repairResultDtl.MANAGER eq userId}">
												<div class="overflow tabFixedBtn">
								           			<div class="f-r"  id="btns">
								           				<c:if test="${repairResultDtl.REMARKS eq REMARKS_SEPARATE}">
															<a href="#none" id="popRepairResultRegForm_requestBtn" class="btn comm st01">타파트요청</a>
														</c:if>
														<a href="#none" id="popRepairResultRegForm_saveBtn" class="btn comm st02">임시저장</a>	
														<a href="#none" id="popRepairResultRegForm_regBtn" class="btn comm st01"><spring:message code='epms.repair.complete' /></a><!-- 정비완료 -->
													</div>
			           							</div>
											</c:if>
										</c:when>
										<c:otherwise>
											<c:if test="${repairResultDtl.REPAIR_TYPE eq EPMS_REPAIR_TYPE_EM && param.flag ne 'repairHistory'}">
												<div class="overflow tabFixedBtn">
								           			<div class="f-r"  id="btns">
								           				<a href="#none" id="analysisBtn" class="btn comm st02">원인분석</a><!-- 원인분석 -->
								           			</div>
								           		</div>
											</c:if>
										</c:otherwise>
				           			</c:choose>
				           			
				           			<div class="tabFixedBtn_Con">
						           		<div class="tabFixedBtn_ConIn">
											<div class="tb-wrap">
												<div id="removeItemList" style="display:none;"></div>
												<form id="regForm" name="regForm"  method="post" enctype="multipart/form-data">
													<input type="hidden" id="attachExistYn" name="attachExistYn" /> 
													<input type="hidden" id="ATTACH_GRP_NO" name="ATTACH_GRP_NO" value="${repairResultDtl.ATTACH_GRP_NO2}" /> 
													<input type="hidden" id="MODULE" name="MODULE" value="112"/>
													<input type="hidden" class="inp-comm" id="flag" name="flag"/>
													<input type="hidden" class="inp-comm" id="REPAIR_RESULT" name="REPAIR_RESULT" value="${repairResultDtl.REPAIR_RESULT}"/>
													<input type="hidden" class="inp-comm" id="WORK_ID" name="WORK_ID" value="${repairResultDtl.REPAIR_RESULT}"/>
													<input type="hidden" class="inp-comm" id="popRepairResultRegForm_EQUIPMENT" name="EQUIPMENT" value="${repairResultDtl.EQUIPMENT}"/>
													<input type="hidden" class="inp-comm" id="popRepairResultRegForm_REMARKS" name="REMARKS" value="${repairResultDtl.REMARKS}"/>
													<input type="hidden" class="inp-comm" id="REPAIR_REQUEST" name="REPAIR_REQUEST" value="${repairResultDtl.REPAIR_REQUEST}"/>
													<input type="hidden" class="inp-comm" id="popRepairResultRegForm_REPAIR_REQUEST_ORG" name="REPAIR_REQUEST_ORG" value="${repairResultDtl.REPAIR_REQUEST_ORG}"/>
													<input type="hidden" class="inp-comm" id="REQUEST_TYPE" name="REQUEST_TYPE" value="${repairResultDtl.REQUEST_TYPE}"/>
													<input type="hidden" class="inp-comm" id="MANAGER" name="MANAGER" value="${repairResultDtl.MANAGER}"/>
													<input type="hidden" class="inp-comm" id="DATE_REPAIR" name="DATE_REPAIR"/>
													<input type="hidden" class="inp-comm" id="REPAIR_ANALYSIS" name="REPAIR_ANALYSIS" value="${repairResultDtl.REPAIR_ANALYSIS}"/>
													<input type="hidden" class="inp-comm" id="PREVENTIVE_RESULT" name="PREVENTIVE_RESULT" value="${repairResultDtl.PREVENTIVE_RESULT}"/>
													<input type="hidden" id="DEL_SEQ" name="DEL_SEQ" value=""/>
													<table class="tb-st">
														<caption class="screen-out">실적 등록</caption>
														<colgroup>
															<col width="15%" />
															<col width="35%" />
															<col width="15%" />
															<col width="*" />
														</colgroup>
														<tbody>
										 					<tr>
																<th scope="row"><spring:message code='epms.worker' /></th><!-- 정비원 -->
																<td colspan="3">
																	<div class="repairMemBtnArea overflow">
																	
																		<c:if test="${repairResultDtl.MANAGER eq userId}">
																			<a href="#none" class="btn evn-st01" onclick="fnSearchMember()"><spring:message code='epms.worker' /> <spring:message code='epms.search' /></a><!-- 정비원 검색 -->
																		</c:if>
																		
																		<div class="f-r mgn-t-5">
																			<a href="javascript:repairMemAccSlider.pr(1)">전체 열기</a> | <a href="javascript:repairMemAccSlider.pr(-1)">전체 닫기</a>
																		</div>
																	</div>
																	
																	<ul class="repairMemberList repairMember" id="repairMember2">
																		<c:forEach var="item" items="${repairRegMember}">
																			<li>
																				<div class="repairMemberTit">
																					<c:choose>
																						<c:when test="${item.WORK_STATUS eq 'Y'}">
																							<div class="repairMemberTitIn" style="border-left: 5px solid #38ca58;">
																						</c:when>
																						<c:otherwise>
																							<div class="repairMemberTitIn">
																						</c:otherwise>
																					</c:choose>
																						<div class="userInfo">
																							<div class="user_img">
																								<img src="${item.MEMBER_FILEURL}" onerror="this.src='/images/com/web_v2/userIcon.png'" />
																							</div>
																							<div class="user_name">
																								<div class="name">
																									<input type="hidden" class="inp-comm" title="작업이력ID" name="WORK_LOG" value="${item.WORK_LOG}">
																									<input type="hidden" class="inp-comm" title="등록상태" name="WORK_STATUS" value="${item.WORK_STATUS}">
																									<input type="hidden" class="inp-comm" title="정비원정보" name="memberArr" value="" readonly="readonly">
																									<input type="hidden" class="inp-comm" title="정비원ID" name="WORK_LOG_USER_ID" value="${item.USER_ID}" readonly="readonly">
																									<input type="hidden" class="inp-comm" title="삭제여부" name="delYnArr2" value="N"	readonly="readonly">
																									${item.MEMBER_NAME}
																								<c:if test="${item.USER_ID eq repairResultDtl.MANAGER}">
																									<span class="manager">담당자</span>
																								</c:if>
																								
																								<c:if test="${item.WORK_STATUS eq 'Y'}">
																									<span class="fixBtn">작업완료</span>
																								</c:if>
																																															
																								</div>
																								<div class="dep">
																									<span>
																										<input type="hidden" class="inp-comm" title="부서ID" name="" value="${item.DIVISION}"	readonly="readonly">
																										${item.DIVISION_NAME}
																									</span>
																									<span style="padding:0 5px;">/</span>
																									<span>
																										<input type="hidden" class="inp-comm" title="파트" name="" value="${item.PART}"	readonly="readonly">
																										${item.PART_NAME}
																									</span>
																								</div>
																							</div>
																						</div>
																																												
																						<c:if test="${item.USER_ID eq userId and item.WORK_STATUS ne 'Y'}">
																							<div class="repairMemberBtn">
																								<a href="#none" class="treeButton repairAdd"  onclick="repairMemberControll.add(this)">추가</a>
																								<a href="#none" class="treeButton repairTemp" onclick="fnWorkLogReg(this,'N')">임시저장</a>
																								<a href="#none" class="treeButton repairSave" onclick="fnWorkLogReg(this,'Y')">작업완료</a>
																							</div>
																						</c:if>
																						
																						<c:if test="${repairResultDtl.MANAGER eq userId and repairResultDtl.MANAGER ne item.USER_ID and item.WORK_STATUS eq 'N'}">
																							<div class="repairMemberBtn2">
																								<a href="#none" class="treeButton repairDelete" onclick="delList1($(this))">삭제</a>
																							</div>
																						</c:if>
																						
																						<c:if test="${item.USER_ID eq userId and item.WORK_STATUS eq 'Y'}">
																							<div class="repairMemberBtn3">
																								<a href="#none" class="treeButton repairReject" onclick="fnWorkLogCancel(this,'N')">작업완료취소</a>
																							</div>
																						</c:if>
																						
																					</div>
																				</div>
																				<div class="repairAccOpenBtn repairAccOpenBtnHide"><a href="#none" title="작업 상세보기"></a></div>
																				
																				<div class="panel-in" id="editForm2">																				
																					<div class="repairTime">
																						<c:forEach var="workLogItem" items="${workLogItem}">
																						<c:if test="${item.WORK_LOG eq workLogItem.WORK_LOG and item.USER_ID eq workLogItem.USER_ID and workLogItem.DEL_YN eq 'N' }">
																						<!-- s : 추가버튼 클릭시 -->
																						<div class="repairTimeAddLst" value="N">
																							<input type="hidden" class="inp-comm" title="삭제여부"			name="DEL_YN" value="${workLogItem.DEL_YN}"/>
																							<input type="hidden" class="inp-comm" title="정비원ID"		name="USER_ID" value="${workLogItem.USER_ID}">
																							<input type="hidden" class="inp-comm" title="작업이력상세 ID"	name="WORK_LOG_ITEM" value="${workLogItem.WORK_LOG_ITEM}">
																							<input type="hidden" class="inp-comm" title="작업이력상세정보"	name="workLogItemArr" value="">
																							<div class="overflow">
																								<div class="f-l">
																									<div class="inTit"><spring:message code='epms.repair.time.start' /></div><!-- 정비시작시간 -->
																								</div>
																								<div class="f-l wd-per-15">
																								<c:if test="${workLogItem.USER_ID eq userId and item.WORK_STATUS eq 'N'}">
																										<input type="hidden" class="inp-comm moDatePic inpCal" readonly="readonly" name="DATE_START" title="정비시작일">
																										<input type="text" class="inp-comm moDatePic inpCal" readonly="readonly" name="DATE_START_DATE" title="정비시작일" value="<c:out value="${workLogItem.DATE_START_DATE}" escapeXml="true"/>">
																								</c:if>
																								<c:if test="${workLogItem.USER_ID ne userId or item.WORK_STATUS eq 'Y'}">
																									<div class="inp-wrap readonly wd-per-100">
																										<input type="hidden" class="inp-comm" readonly="readonly" name="DATE_START" title="정비시작일">
																										<input type="text" class="inp-comm" readonly="readonly" name="DATE_START_DATE" title="정비시작일" value="<c:out value="${workLogItem.DATE_START_DATE}" escapeXml="true"/>">
																									</div>
																								</c:if>
																								</div>
																								
																								<c:if test="${workLogItem.USER_ID eq userId and item.WORK_STATUS eq 'N'}">
																								<div class="sel-wrap wd-cal-10 rel mgn-l-5 mgn-r-25 f-l">
																									<select title="정비시작시간" name="DATE_START_HOUR">
																										<option value="00" <c:if test="${workLogItem.DATE_START_HOUR eq 00}">selected = "selected"</c:if>>00</option>
																										<option value="01" <c:if test="${workLogItem.DATE_START_HOUR eq 01}">selected = "selected"</c:if>>01</option>
																										<option value="02" <c:if test="${workLogItem.DATE_START_HOUR eq 02}">selected = "selected"</c:if>>02</option>
																										<option value="03" <c:if test="${workLogItem.DATE_START_HOUR eq 03}">selected = "selected"</c:if>>03</option>
																										<option value="04" <c:if test="${workLogItem.DATE_START_HOUR eq 04}">selected = "selected"</c:if>>04</option>
																										<option value="05" <c:if test="${workLogItem.DATE_START_HOUR eq 05}">selected = "selected"</c:if>>05</option>
																										<option value="06" <c:if test="${workLogItem.DATE_START_HOUR eq 06}">selected = "selected"</c:if>>06</option>
																										<option value="07" <c:if test="${workLogItem.DATE_START_HOUR eq 07}">selected = "selected"</c:if>>07</option>
																										<option value="08" <c:if test="${workLogItem.DATE_START_HOUR eq 08}">selected = "selected"</c:if>>08</option>
																										<option value="09" <c:if test="${workLogItem.DATE_START_HOUR eq 09}">selected = "selected"</c:if>>09</option>
																										<option value="10" <c:if test="${workLogItem.DATE_START_HOUR eq 10}">selected = "selected"</c:if>>10</option>
																										<option value="11" <c:if test="${workLogItem.DATE_START_HOUR eq 11}">selected = "selected"</c:if>>11</option>
																										<option value="12" <c:if test="${workLogItem.DATE_START_HOUR eq 12}">selected = "selected"</c:if>>12</option>
																										<option value="13" <c:if test="${workLogItem.DATE_START_HOUR eq 13}">selected = "selected"</c:if>>13</option>
																										<option value="14" <c:if test="${workLogItem.DATE_START_HOUR eq 14}">selected = "selected"</c:if>>14</option>
																										<option value="15" <c:if test="${workLogItem.DATE_START_HOUR eq 15}">selected = "selected"</c:if>>15</option>
																										<option value="16" <c:if test="${workLogItem.DATE_START_HOUR eq 16}">selected = "selected"</c:if>>16</option>
																										<option value="17" <c:if test="${workLogItem.DATE_START_HOUR eq 17}">selected = "selected"</c:if>>17</option>
																										<option value="18" <c:if test="${workLogItem.DATE_START_HOUR eq 18}">selected = "selected"</c:if>>18</option>
																										<option value="19" <c:if test="${workLogItem.DATE_START_HOUR eq 19}">selected = "selected"</c:if>>19</option>
																										<option value="20" <c:if test="${workLogItem.DATE_START_HOUR eq 20}">selected = "selected"</c:if>>20</option>
																										<option value="21" <c:if test="${workLogItem.DATE_START_HOUR eq 21}">selected = "selected"</c:if>>21</option>
																										<option value="22" <c:if test="${workLogItem.DATE_START_HOUR eq 22}">selected = "selected"</c:if>>22</option>
																										<option value="23" <c:if test="${workLogItem.DATE_START_HOUR eq 23}">selected = "selected"</c:if>>23</option>
																									</select>
																									<span style="position:absolute; top:5px; right:-15px;">시</span>
																								</div>
																								</c:if>
																								<c:if test="${workLogItem.USER_ID ne userId or item.WORK_STATUS eq 'Y'}">
																									<div class="wd-cal-10 rel mgn-l-5 mgn-r-25 f-l inp-wrap readonly">
																										<input type="text" class="inp-comm" readonly="readonly" name="DATE_START_HOUR" title="정비시작시간" value="<c:out value="${workLogItem.DATE_START_HOUR}" escapeXml="true"/>">
																										<span style="position:absolute; top:5px; right:-15px;">시</span>
																									</div>
																								</c:if>
																								
																								<c:if test="${workLogItem.USER_ID eq userId and item.WORK_STATUS eq 'N'}">
																								<div class="sel-wrap wd-cal-10 rel mgn-l-5 f-l">
																									<select title="정비시작분" name="DATE_START_MINUTE" >
																										<option value="00" <c:if test="${workLogItem.DATE_START_MINUTE eq 00}">selected = "selected"</c:if>>00</option>
																										<option value="01" <c:if test="${workLogItem.DATE_START_MINUTE eq 01}">selected = "selected"</c:if>>01</option>
																										<option value="02" <c:if test="${workLogItem.DATE_START_MINUTE eq 02}">selected = "selected"</c:if>>02</option>
																										<option value="03" <c:if test="${workLogItem.DATE_START_MINUTE eq 03}">selected = "selected"</c:if>>03</option>
																										<option value="04" <c:if test="${workLogItem.DATE_START_MINUTE eq 04}">selected = "selected"</c:if>>04</option>
																										<option value="05" <c:if test="${workLogItem.DATE_START_MINUTE eq 05}">selected = "selected"</c:if>>05</option>
																										<option value="06" <c:if test="${workLogItem.DATE_START_MINUTE eq 06}">selected = "selected"</c:if>>06</option>
																										<option value="07" <c:if test="${workLogItem.DATE_START_MINUTE eq 07}">selected = "selected"</c:if>>07</option>
																										<option value="08" <c:if test="${workLogItem.DATE_START_MINUTE eq 08}">selected = "selected"</c:if>>08</option>
																										<option value="09" <c:if test="${workLogItem.DATE_START_MINUTE eq 09}">selected = "selected"</c:if>>09</option>
																										<option value="10" <c:if test="${workLogItem.DATE_START_MINUTE eq 10}">selected = "selected"</c:if>>10</option>
																										<option value="11" <c:if test="${workLogItem.DATE_START_MINUTE eq 11}">selected = "selected"</c:if>>11</option>
																										<option value="12" <c:if test="${workLogItem.DATE_START_MINUTE eq 12}">selected = "selected"</c:if>>12</option>
																										<option value="13" <c:if test="${workLogItem.DATE_START_MINUTE eq 13}">selected = "selected"</c:if>>13</option>
																										<option value="14" <c:if test="${workLogItem.DATE_START_MINUTE eq 14}">selected = "selected"</c:if>>14</option>
																										<option value="15" <c:if test="${workLogItem.DATE_START_MINUTE eq 15}">selected = "selected"</c:if>>15</option>
																										<option value="16" <c:if test="${workLogItem.DATE_START_MINUTE eq 16}">selected = "selected"</c:if>>16</option>
																										<option value="17" <c:if test="${workLogItem.DATE_START_MINUTE eq 17}">selected = "selected"</c:if>>17</option>
																										<option value="18" <c:if test="${workLogItem.DATE_START_MINUTE eq 18}">selected = "selected"</c:if>>18</option>
																										<option value="19" <c:if test="${workLogItem.DATE_START_MINUTE eq 19}">selected = "selected"</c:if>>19</option>
																										<option value="20" <c:if test="${workLogItem.DATE_START_MINUTE eq 20}">selected = "selected"</c:if>>20</option>
																										<option value="21" <c:if test="${workLogItem.DATE_START_MINUTE eq 21}">selected = "selected"</c:if>>21</option>
																										<option value="22" <c:if test="${workLogItem.DATE_START_MINUTE eq 22}">selected = "selected"</c:if>>22</option>
																										<option value="23" <c:if test="${workLogItem.DATE_START_MINUTE eq 23}">selected = "selected"</c:if>>23</option>
																										<option value="24" <c:if test="${workLogItem.DATE_START_MINUTE eq 24}">selected = "selected"</c:if>>24</option>
																										<option value="25" <c:if test="${workLogItem.DATE_START_MINUTE eq 25}">selected = "selected"</c:if>>25</option>
																										<option value="26" <c:if test="${workLogItem.DATE_START_MINUTE eq 26}">selected = "selected"</c:if>>26</option>
																										<option value="27" <c:if test="${workLogItem.DATE_START_MINUTE eq 27}">selected = "selected"</c:if>>27</option>
																										<option value="28" <c:if test="${workLogItem.DATE_START_MINUTE eq 28}">selected = "selected"</c:if>>28</option>
																										<option value="29" <c:if test="${workLogItem.DATE_START_MINUTE eq 29}">selected = "selected"</c:if>>29</option>
																										<option value="30" <c:if test="${workLogItem.DATE_START_MINUTE eq 30}">selected = "selected"</c:if>>30</option>
																										<option value="31" <c:if test="${workLogItem.DATE_START_MINUTE eq 31}">selected = "selected"</c:if>>31</option>
																										<option value="32" <c:if test="${workLogItem.DATE_START_MINUTE eq 32}">selected = "selected"</c:if>>32</option>
																										<option value="33" <c:if test="${workLogItem.DATE_START_MINUTE eq 33}">selected = "selected"</c:if>>33</option>
																										<option value="34" <c:if test="${workLogItem.DATE_START_MINUTE eq 34}">selected = "selected"</c:if>>34</option>
																										<option value="35" <c:if test="${workLogItem.DATE_START_MINUTE eq 35}">selected = "selected"</c:if>>35</option>
																										<option value="36" <c:if test="${workLogItem.DATE_START_MINUTE eq 36}">selected = "selected"</c:if>>36</option>
																										<option value="37" <c:if test="${workLogItem.DATE_START_MINUTE eq 37}">selected = "selected"</c:if>>37</option>
																										<option value="38" <c:if test="${workLogItem.DATE_START_MINUTE eq 38}">selected = "selected"</c:if>>38</option>
																										<option value="39" <c:if test="${workLogItem.DATE_START_MINUTE eq 39}">selected = "selected"</c:if>>39</option>
																										<option value="40" <c:if test="${workLogItem.DATE_START_MINUTE eq 40}">selected = "selected"</c:if>>40</option>
																										<option value="41" <c:if test="${workLogItem.DATE_START_MINUTE eq 41}">selected = "selected"</c:if>>41</option>
																										<option value="42" <c:if test="${workLogItem.DATE_START_MINUTE eq 42}">selected = "selected"</c:if>>42</option>
																										<option value="43" <c:if test="${workLogItem.DATE_START_MINUTE eq 43}">selected = "selected"</c:if>>43</option>
																										<option value="44" <c:if test="${workLogItem.DATE_START_MINUTE eq 44}">selected = "selected"</c:if>>44</option>
																										<option value="45" <c:if test="${workLogItem.DATE_START_MINUTE eq 45}">selected = "selected"</c:if>>45</option>
																										<option value="46" <c:if test="${workLogItem.DATE_START_MINUTE eq 46}">selected = "selected"</c:if>>46</option>
																										<option value="47" <c:if test="${workLogItem.DATE_START_MINUTE eq 47}">selected = "selected"</c:if>>47</option>
																										<option value="48" <c:if test="${workLogItem.DATE_START_MINUTE eq 48}">selected = "selected"</c:if>>48</option>
																										<option value="49" <c:if test="${workLogItem.DATE_START_MINUTE eq 49}">selected = "selected"</c:if>>49</option>
																										<option value="50" <c:if test="${workLogItem.DATE_START_MINUTE eq 50}">selected = "selected"</c:if>>50</option>
																										<option value="51" <c:if test="${workLogItem.DATE_START_MINUTE eq 51}">selected = "selected"</c:if>>51</option>
																										<option value="52" <c:if test="${workLogItem.DATE_START_MINUTE eq 52}">selected = "selected"</c:if>>52</option>
																										<option value="53" <c:if test="${workLogItem.DATE_START_MINUTE eq 53}">selected = "selected"</c:if>>53</option>
																										<option value="54" <c:if test="${workLogItem.DATE_START_MINUTE eq 54}">selected = "selected"</c:if>>54</option>
																										<option value="55" <c:if test="${workLogItem.DATE_START_MINUTE eq 55}">selected = "selected"</c:if>>55</option>
																										<option value="56" <c:if test="${workLogItem.DATE_START_MINUTE eq 56}">selected = "selected"</c:if>>56</option>
																										<option value="57" <c:if test="${workLogItem.DATE_START_MINUTE eq 57}">selected = "selected"</c:if>>57</option>
																										<option value="58" <c:if test="${workLogItem.DATE_START_MINUTE eq 58}">selected = "selected"</c:if>>58</option>
																										<option value="59" <c:if test="${workLogItem.DATE_START_MINUTE eq 59}">selected = "selected"</c:if>>59</option>
																									</select>
																									<span style="position:absolute; top:5px; right:-15px;">분</span>
																								</div>
																								</c:if>
																								<c:if test="${workLogItem.USER_ID ne userId or item.WORK_STATUS eq 'Y'}">
																									<div class="wd-cal-10 rel mgn-l-5 mgn-r-25 f-l inp-wrap readonly">
																										<input type="text" class="inp-comm" readonly="readonly" title="정비시작분" name="DATE_START_MINUTE" value="<c:out value="${workLogItem.DATE_START_MINUTE}" escapeXml="true"/>">
																										<span style="position:absolute; top:5px; right:-15px;">분</span>
																									</div>
																								</c:if>
																							</div>
																							<div class="overflow mgn-t-5">
																								<div class="f-l">
																									<div class="inTit"><spring:message code='epms.repair.time.end' /></div><!-- 정비종료시간 -->
																								</div>
																								<div class="f-l wd-per-15">
																									<c:if test="${workLogItem.USER_ID eq userId and item.WORK_STATUS eq 'N'}">
																										<input type="hidden" class="inp-comm moDatePic inpCal" readonly="readonly" name="DATE_END" title="정비종료일">
																										<input type="text" class="inp-comm moDatePic inpCal" readonly="readonly" name="DATE_END_DATE" title="정비종료일" value="<c:out value="${workLogItem.DATE_END_DATE}" escapeXml="true"/>">
																									</c:if>
																									<c:if test="${workLogItem.USER_ID ne userId or item.WORK_STATUS eq 'Y'}">
																										<div class="inp-wrap readonly wd-per-100">
																											<input type="hidden" class="inp-comm" name="DATE_END" title="정비종료일" readonly="readonly">
																											<input type="text" class="inp-comm" name="DATE_END_DATE" title="정비종료일" value="<c:out value="${workLogItem.DATE_END_DATE}" escapeXml="true"/>" readonly="readonly">
																										</div>
																									</c:if>
																								</div>
																								
																								<c:if test="${workLogItem.USER_ID eq userId and item.WORK_STATUS eq 'N'}">
																								<div class="sel-wrap wd-cal-10 rel mgn-l-5 mgn-r-25 f-l">
																									<select title="정비종료시간" name="DATE_END_HOUR" >
																										<option value="00" <c:if test="${workLogItem.DATE_END_HOUR eq 00}">selected = "selected"</c:if>>00</option>
																										<option value="01" <c:if test="${workLogItem.DATE_END_HOUR eq 01}">selected = "selected"</c:if>>01</option>
																										<option value="02" <c:if test="${workLogItem.DATE_END_HOUR eq 02}">selected = "selected"</c:if>>02</option>
																										<option value="03" <c:if test="${workLogItem.DATE_END_HOUR eq 03}">selected = "selected"</c:if>>03</option>
																										<option value="04" <c:if test="${workLogItem.DATE_END_HOUR eq 04}">selected = "selected"</c:if>>04</option>
																										<option value="05" <c:if test="${workLogItem.DATE_END_HOUR eq 05}">selected = "selected"</c:if>>05</option>
																										<option value="06" <c:if test="${workLogItem.DATE_END_HOUR eq 06}">selected = "selected"</c:if>>06</option>
																										<option value="07" <c:if test="${workLogItem.DATE_END_HOUR eq 07}">selected = "selected"</c:if>>07</option>
																										<option value="08" <c:if test="${workLogItem.DATE_END_HOUR eq 08}">selected = "selected"</c:if>>08</option>
																										<option value="09" <c:if test="${workLogItem.DATE_END_HOUR eq 09}">selected = "selected"</c:if>>09</option>
																										<option value="10" <c:if test="${workLogItem.DATE_END_HOUR eq 10}">selected = "selected"</c:if>>10</option>
																										<option value="11" <c:if test="${workLogItem.DATE_END_HOUR eq 11}">selected = "selected"</c:if>>11</option>
																										<option value="12" <c:if test="${workLogItem.DATE_END_HOUR eq 12}">selected = "selected"</c:if>>12</option>
																										<option value="13" <c:if test="${workLogItem.DATE_END_HOUR eq 13}">selected = "selected"</c:if>>13</option>
																										<option value="14" <c:if test="${workLogItem.DATE_END_HOUR eq 14}">selected = "selected"</c:if>>14</option>
																										<option value="15" <c:if test="${workLogItem.DATE_END_HOUR eq 15}">selected = "selected"</c:if>>15</option>
																										<option value="16" <c:if test="${workLogItem.DATE_END_HOUR eq 16}">selected = "selected"</c:if>>16</option>
																										<option value="17" <c:if test="${workLogItem.DATE_END_HOUR eq 17}">selected = "selected"</c:if>>17</option>
																										<option value="18" <c:if test="${workLogItem.DATE_END_HOUR eq 18}">selected = "selected"</c:if>>18</option>
																										<option value="19" <c:if test="${workLogItem.DATE_END_HOUR eq 19}">selected = "selected"</c:if>>19</option>
																										<option value="20" <c:if test="${workLogItem.DATE_END_HOUR eq 20}">selected = "selected"</c:if>>20</option>
																										<option value="21" <c:if test="${workLogItem.DATE_END_HOUR eq 21}">selected = "selected"</c:if>>21</option>
																										<option value="22" <c:if test="${workLogItem.DATE_END_HOUR eq 22}">selected = "selected"</c:if>>22</option>
																										<option value="23" <c:if test="${workLogItem.DATE_END_HOUR eq 23}">selected = "selected"</c:if>>23</option>
																									</select>
																									<span style="position:absolute; top:5px; right:-15px;">시</span>
																								</div>
																								</c:if>
																								
																								<c:if test="${workLogItem.USER_ID ne userId or item.WORK_STATUS eq 'Y'}">
																									<div class="wd-cal-10 rel mgn-l-5 mgn-r-25 f-l inp-wrap readonly">
																										<input type="text" class="inp-comm" readonly="readonly" name="DATE_END_HOUR" title="정비시작시간" value="<c:out value="${workLogItem.DATE_END_HOUR}" escapeXml="true"/>">
																										<span style="position:absolute; top:5px; right:-15px;">시</span>
																									</div>
																								</c:if>
																								
																								<c:if test="${workLogItem.USER_ID eq userId and item.WORK_STATUS eq 'N'}">
																								<div class="sel-wrap wd-cal-10 rel mgn-l-5 f-l">
																									<select title="정비종료분" name="DATE_END_MINUTE" >
																										<option value="00" <c:if test="${workLogItem.DATE_END_MINUTE eq 00}">selected = "selected"</c:if>>00</option>
																										<option value="01" <c:if test="${workLogItem.DATE_END_MINUTE eq 01}">selected = "selected"</c:if>>01</option>
																										<option value="02" <c:if test="${workLogItem.DATE_END_MINUTE eq 02}">selected = "selected"</c:if>>02</option>
																										<option value="03" <c:if test="${workLogItem.DATE_END_MINUTE eq 03}">selected = "selected"</c:if>>03</option>
																										<option value="04" <c:if test="${workLogItem.DATE_END_MINUTE eq 04}">selected = "selected"</c:if>>04</option>
																										<option value="05" <c:if test="${workLogItem.DATE_END_MINUTE eq 05}">selected = "selected"</c:if>>05</option>
																										<option value="06" <c:if test="${workLogItem.DATE_END_MINUTE eq 06}">selected = "selected"</c:if>>06</option>
																										<option value="07" <c:if test="${workLogItem.DATE_END_MINUTE eq 07}">selected = "selected"</c:if>>07</option>
																										<option value="08" <c:if test="${workLogItem.DATE_END_MINUTE eq 08}">selected = "selected"</c:if>>08</option>
																										<option value="09" <c:if test="${workLogItem.DATE_END_MINUTE eq 09}">selected = "selected"</c:if>>09</option>
																										<option value="10" <c:if test="${workLogItem.DATE_END_MINUTE eq 10}">selected = "selected"</c:if>>10</option>
																										<option value="11" <c:if test="${workLogItem.DATE_END_MINUTE eq 11}">selected = "selected"</c:if>>11</option>
																										<option value="12" <c:if test="${workLogItem.DATE_END_MINUTE eq 12}">selected = "selected"</c:if>>12</option>
																										<option value="13" <c:if test="${workLogItem.DATE_END_MINUTE eq 13}">selected = "selected"</c:if>>13</option>
																										<option value="14" <c:if test="${workLogItem.DATE_END_MINUTE eq 14}">selected = "selected"</c:if>>14</option>
																										<option value="15" <c:if test="${workLogItem.DATE_END_MINUTE eq 15}">selected = "selected"</c:if>>15</option>
																										<option value="16" <c:if test="${workLogItem.DATE_END_MINUTE eq 16}">selected = "selected"</c:if>>16</option>
																										<option value="17" <c:if test="${workLogItem.DATE_END_MINUTE eq 17}">selected = "selected"</c:if>>17</option>
																										<option value="18" <c:if test="${workLogItem.DATE_END_MINUTE eq 18}">selected = "selected"</c:if>>18</option>
																										<option value="19" <c:if test="${workLogItem.DATE_END_MINUTE eq 19}">selected = "selected"</c:if>>19</option>
																										<option value="20" <c:if test="${workLogItem.DATE_END_MINUTE eq 20}">selected = "selected"</c:if>>20</option>
																										<option value="21" <c:if test="${workLogItem.DATE_END_MINUTE eq 21}">selected = "selected"</c:if>>21</option>
																										<option value="22" <c:if test="${workLogItem.DATE_END_MINUTE eq 22}">selected = "selected"</c:if>>22</option>
																										<option value="23" <c:if test="${workLogItem.DATE_END_MINUTE eq 23}">selected = "selected"</c:if>>23</option>
																										<option value="24" <c:if test="${workLogItem.DATE_END_MINUTE eq 24}">selected = "selected"</c:if>>24</option>
																										<option value="25" <c:if test="${workLogItem.DATE_END_MINUTE eq 25}">selected = "selected"</c:if>>25</option>
																										<option value="26" <c:if test="${workLogItem.DATE_END_MINUTE eq 26}">selected = "selected"</c:if>>26</option>
																										<option value="27" <c:if test="${workLogItem.DATE_END_MINUTE eq 27}">selected = "selected"</c:if>>27</option>
																										<option value="28" <c:if test="${workLogItem.DATE_END_MINUTE eq 28}">selected = "selected"</c:if>>28</option>
																										<option value="29" <c:if test="${workLogItem.DATE_END_MINUTE eq 29}">selected = "selected"</c:if>>29</option>
																										<option value="30" <c:if test="${workLogItem.DATE_END_MINUTE eq 30}">selected = "selected"</c:if>>30</option>
																										<option value="31" <c:if test="${workLogItem.DATE_END_MINUTE eq 31}">selected = "selected"</c:if>>31</option>
																										<option value="32" <c:if test="${workLogItem.DATE_END_MINUTE eq 32}">selected = "selected"</c:if>>32</option>
																										<option value="33" <c:if test="${workLogItem.DATE_END_MINUTE eq 33}">selected = "selected"</c:if>>33</option>
																										<option value="34" <c:if test="${workLogItem.DATE_END_MINUTE eq 34}">selected = "selected"</c:if>>34</option>
																										<option value="35" <c:if test="${workLogItem.DATE_END_MINUTE eq 35}">selected = "selected"</c:if>>35</option>
																										<option value="36" <c:if test="${workLogItem.DATE_END_MINUTE eq 36}">selected = "selected"</c:if>>36</option>
																										<option value="37" <c:if test="${workLogItem.DATE_END_MINUTE eq 37}">selected = "selected"</c:if>>37</option>
																										<option value="38" <c:if test="${workLogItem.DATE_END_MINUTE eq 38}">selected = "selected"</c:if>>38</option>
																										<option value="39" <c:if test="${workLogItem.DATE_END_MINUTE eq 39}">selected = "selected"</c:if>>39</option>
																										<option value="40" <c:if test="${workLogItem.DATE_END_MINUTE eq 40}">selected = "selected"</c:if>>40</option>
																										<option value="41" <c:if test="${workLogItem.DATE_END_MINUTE eq 41}">selected = "selected"</c:if>>41</option>
																										<option value="42" <c:if test="${workLogItem.DATE_END_MINUTE eq 42}">selected = "selected"</c:if>>42</option>
																										<option value="43" <c:if test="${workLogItem.DATE_END_MINUTE eq 43}">selected = "selected"</c:if>>43</option>
																										<option value="44" <c:if test="${workLogItem.DATE_END_MINUTE eq 44}">selected = "selected"</c:if>>44</option>
																										<option value="45" <c:if test="${workLogItem.DATE_END_MINUTE eq 45}">selected = "selected"</c:if>>45</option>
																										<option value="46" <c:if test="${workLogItem.DATE_END_MINUTE eq 46}">selected = "selected"</c:if>>46</option>
																										<option value="47" <c:if test="${workLogItem.DATE_END_MINUTE eq 47}">selected = "selected"</c:if>>47</option>
																										<option value="48" <c:if test="${workLogItem.DATE_END_MINUTE eq 48}">selected = "selected"</c:if>>48</option>
																										<option value="49" <c:if test="${workLogItem.DATE_END_MINUTE eq 49}">selected = "selected"</c:if>>49</option>
																										<option value="50" <c:if test="${workLogItem.DATE_END_MINUTE eq 50}">selected = "selected"</c:if>>50</option>
																										<option value="51" <c:if test="${workLogItem.DATE_END_MINUTE eq 51}">selected = "selected"</c:if>>51</option>
																										<option value="52" <c:if test="${workLogItem.DATE_END_MINUTE eq 52}">selected = "selected"</c:if>>52</option>
																										<option value="53" <c:if test="${workLogItem.DATE_END_MINUTE eq 53}">selected = "selected"</c:if>>53</option>
																										<option value="54" <c:if test="${workLogItem.DATE_END_MINUTE eq 54}">selected = "selected"</c:if>>54</option>
																										<option value="55" <c:if test="${workLogItem.DATE_END_MINUTE eq 55}">selected = "selected"</c:if>>55</option>
																										<option value="56" <c:if test="${workLogItem.DATE_END_MINUTE eq 56}">selected = "selected"</c:if>>56</option>
																										<option value="57" <c:if test="${workLogItem.DATE_END_MINUTE eq 57}">selected = "selected"</c:if>>57</option>
																										<option value="58" <c:if test="${workLogItem.DATE_END_MINUTE eq 58}">selected = "selected"</c:if>>58</option>
																										<option value="59" <c:if test="${workLogItem.DATE_END_MINUTE eq 59}">selected = "selected"</c:if>>59</option>
																									</select>
																									<span style="position:absolute; top:5px; right:-15px;">분</span>
																								</div>
																								</c:if>
																								
																								<c:if test="${workLogItem.USER_ID ne userId or item.WORK_STATUS eq 'Y'}">
																									<div class="wd-cal-10 rel mgn-l-5 mgn-r-25 f-l inp-wrap readonly">
																										<input type="text" class="inp-comm" readonly="readonly" title="정비종료분" name="DATE_END_MINUTE" value="<c:out value="${workLogItem.DATE_END_MINUTE}" escapeXml="true"/>">
																										<span style="position:absolute; top:5px; right:-15px;">분</span>
																									</div>
																								</c:if>
																							</div>
																							<div class="overflow mgn-t-5">
																								<div class="f-l">
																									<div class="inTit"><spring:message code='epms.work.content' /></div><!-- 정비내용 -->
																								</div>
																								<div class="f-l wd-per-75">
																									<c:if test="${userId eq workLogItem.USER_ID and item.WORK_STATUS eq 'N'}">
																										<div class="txt-wrap">
																											<div class="txtArea">
																												<textarea cols="" rows="3" title="정비내용" id="ITEM_DESCRIPTION" name="ITEM_DESCRIPTION"><c:out value="${workLogItem.DESCRIPTION}" escapeXml="true" /></textarea>
																											</div>
																										</div>
																									</c:if>
																									
																									<c:if test="${userId ne workLogItem.USER_ID or item.WORK_STATUS eq 'Y'}">
																										<div class="txt-wrap readonly"> 
																											<div class="txtArea">
																												<textarea cols="" rows="3" title="정비내용" id="ITEM_DESCRIPTION" name="ITEM_DESCRIPTION" readonly="readonly" ><c:out value="${workLogItem.DESCRIPTION}" escapeXml="true" /></textarea>
																											</div>
																										</div>
																									</c:if>
																								</div>
																							</div>
																							<c:if test="${workLogItem.USER_ID eq userId and item.WORK_STATUS eq 'N' and workLogItem.ROWNUM ne '1'}">
																								<a href="#none" class="repairTimeRowDel deleteBtn" onclick="repairMemberControll.remove(this)"></a>
																							</c:if>
																						</div>
																						<!-- e : 추가버튼 클릭시 -->
																						</c:if>
																						</c:forEach>
																					</div>
																				</div>
																			</li>
																			
																		</c:forEach>
																	</ul>
																	
																</td>
															</tr>
															<tr>
																<th scope="row"><spring:message code='epms.repair.type' /></th><!-- 정비유형 -->
																<td>
																	<div class="sel-wrap repair_type_css">
																		<select title="정비유형" id="REPAIR_TYPE" name="REPAIR_TYPE">
																			<option value="">선택하세요.</option>
																			<c:forEach var="item" items="${repairTypeList}">
																				<option value="${item.COMCD}">${item.COMCD_NM}</option>
																			</c:forEach>
																		</select> 
																	</div>
																</td>
																<th scope="row"><spring:message code='epms.outsourcing' /></th><!-- 외주 -->
																<td>
																	<div class="inp-wrap wd-per-100">
																		<input type="text" class="inp-comm" id="OUTSOURCING" name="OUTSOURCING" value="<c:out value="${repairResultDtl.OUTSOURCING}" escapeXml="true"/>"/>
																	</div>
																</td>
															</tr>
															<tr>
																<th scope="row"><spring:message code='epms.occurr.time.start' /></th><!-- 고장시작시간 -->
																<td>
																	<div class="f-l wd-per-50">
																		<input type="hidden" class="inp-comm moDatePic inpCal" readonly="readonly" id="popRepairResultRegForm_TROUBLE_TIME1" name="TROUBLE_TIME1" title="고장시작일">
																		<input type="text" class="inp-comm moDatePic inpCal TROUBLE_TIME1" readonly="readonly" id="popRepairResultRegForm_TROUBLE_TIME1_DATE" name="TROUBLE_TIME1_DATE" title="고장시작일">
																	</div>
																	<div class="sel-wrap wd-cal-25 rel mgn-l-5 mgn-r-25 f-l">
																		<select title="고장시작일" id="popRepairResultRegForm_TROUBLE_TIME1_HOUR" name="TROUBLE_TIME1_HOUR" class="TROUBLE_TIME1">
																			<option value="00">00</option>
																			<option value="01">01</option>
																			<option value="02">02</option>
																			<option value="03">03</option>
																			<option value="04">04</option>
																			<option value="05">05</option>
																			<option value="06">06</option>
																			<option value="07">07</option>
																			<option value="08">08</option>
																			<option value="09">09</option>
																			<option value="10">10</option>
																			<option value="11">11</option>
																			<option value="12">12</option>
																			<option value="13">13</option>
																			<option value="14">14</option>
																			<option value="15">15</option>
																			<option value="16">16</option>
																			<option value="17">17</option>
																			<option value="18">18</option>
																			<option value="19">19</option>
																			<option value="20">20</option>
																			<option value="21">21</option>
																			<option value="22">22</option>
																			<option value="23">23</option>
																		</select>
																		<span style="position:absolute; top:5px; right:-15px;">시</span>
																	</div>
																	<div class="sel-wrap wd-cal-25 rel mgn-l-5 f-l">
																		<select title="고장시작일" id="popRepairResultRegForm_TROUBLE_TIME1_MINUTE" name="TROUBLE_TIME1_MINUTE" class="TROUBLE_TIME1">
																			<option value="00">00</option>
																			<option value="01">01</option>
																			<option value="02">02</option>
																			<option value="03">03</option>
																			<option value="04">04</option>
																			<option value="05">05</option>
																			<option value="06">06</option>
																			<option value="07">07</option>
																			<option value="08">08</option>
																			<option value="09">09</option>
																			<option value="10">10</option>
																			<option value="11">11</option>
																			<option value="12">12</option>
																			<option value="13">13</option>
																			<option value="14">14</option>
																			<option value="15">15</option>
																			<option value="16">16</option>
																			<option value="17">17</option>
																			<option value="18">18</option>
																			<option value="19">19</option>
																			<option value="20">20</option>
																			<option value="21">21</option>
																			<option value="22">22</option>
																			<option value="23">23</option>
																			<option value="24">24</option>
																			<option value="25">25</option>
																			<option value="26">26</option>
																			<option value="27">27</option>
																			<option value="28">28</option>
																			<option value="29">29</option>
																			<option value="30">30</option>
																			<option value="31">31</option>
																			<option value="32">32</option>
																			<option value="33">33</option>
																			<option value="34">34</option>
																			<option value="35">35</option>
																			<option value="36">36</option>
																			<option value="37">37</option>
																			<option value="38">38</option>
																			<option value="39">39</option>
																			<option value="40">40</option>
																			<option value="41">41</option>
																			<option value="42">42</option>
																			<option value="43">43</option>
																			<option value="44">44</option>
																			<option value="45">45</option>
																			<option value="46">46</option>
																			<option value="47">47</option>
																			<option value="48">48</option>
																			<option value="49">49</option>
																			<option value="50">50</option>
																			<option value="51">51</option>
																			<option value="52">52</option>
																			<option value="53">53</option>
																			<option value="54">54</option>
																			<option value="55">55</option>
																			<option value="56">56</option>
																			<option value="57">57</option>
																			<option value="58">58</option>
																			<option value="59">59</option>
																		</select>
																		<span style="position:absolute; top:5px; right:-15px;">분</span>
																	</div>
																</td>
																<th scope="row"><spring:message code='epms.occurr.time.end' /></th><!-- 고장종료시간 -->
																<td>
																	<div class="f-l wd-per-50">
																		<input type="hidden" class="inp-comm moDatePic inpCal" readonly="readonly" id="popRepairResultRegForm_TROUBLE_TIME2" name="TROUBLE_TIME2" title="고장종료일">
																	<input type="text" class="inp-comm moDatePic inpCal TROUBLE_TIME2" readonly="readonly" id="popRepairResultRegForm_TROUBLE_TIME2_DATE" name="TROUBLE_TIME2_DATE" title="고장종료일">
																	</div>
																	<div class="sel-wrap wd-cal-25 rel mgn-l-5 mgn-r-25 f-l">
																		<select title="고장종료일" id="popRepairResultRegForm_TROUBLE_TIME2_HOUR" name="TROUBLE_TIME2_HOUR" class="TROUBLE_TIME2">
																			<option value="00">00</option>
																			<option value="01">01</option>
																			<option value="02">02</option>
																			<option value="03">03</option>
																			<option value="04">04</option>
																			<option value="05">05</option>
																			<option value="06">06</option>
																			<option value="07">07</option>
																			<option value="08">08</option>
																			<option value="09">09</option>
																			<option value="10">10</option>
																			<option value="11">11</option>
																			<option value="12">12</option>
																			<option value="13">13</option>
																			<option value="14">14</option>
																			<option value="15">15</option>
																			<option value="16">16</option>
																			<option value="17">17</option>
																			<option value="18">18</option>
																			<option value="19">19</option>
																			<option value="20">20</option>
																			<option value="21">21</option>
																			<option value="22">22</option>
																			<option value="23">23</option>
																		</select>
																		<span style="position:absolute; top:5px; right:-15px;">시</span>
																	</div>
																	<div class="sel-wrap wd-cal-25 rel mgn-l-5 f-l">
																		<select title="고장종료일" id="popRepairResultRegForm_TROUBLE_TIME2_MINUTE" name="TROUBLE_TIME2_MINUTE" class="TROUBLE_TIME2">
																			<option value="00">00</option>
																			<option value="01">01</option>
																			<option value="02">02</option>
																			<option value="03">03</option>
																			<option value="04">04</option>
																			<option value="05">05</option>
																			<option value="06">06</option>
																			<option value="07">07</option>
																			<option value="08">08</option>
																			<option value="09">09</option>
																			<option value="10">10</option>
																			<option value="11">11</option>
																			<option value="12">12</option>
																			<option value="13">13</option>
																			<option value="14">14</option>
																			<option value="15">15</option>
																			<option value="16">16</option>
																			<option value="17">17</option>
																			<option value="18">18</option>
																			<option value="19">19</option>
																			<option value="20">20</option>
																			<option value="21">21</option>
																			<option value="22">22</option>
																			<option value="23">23</option>
																			<option value="24">24</option>
																			<option value="25">25</option>
																			<option value="26">26</option>
																			<option value="27">27</option>
																			<option value="28">28</option>
																			<option value="29">29</option>
																			<option value="30">30</option>
																			<option value="31">31</option>
																			<option value="32">32</option>
																			<option value="33">33</option>
																			<option value="34">34</option>
																			<option value="35">35</option>
																			<option value="36">36</option>
																			<option value="37">37</option>
																			<option value="38">38</option>
																			<option value="39">39</option>
																			<option value="40">40</option>
																			<option value="41">41</option>
																			<option value="42">42</option>
																			<option value="43">43</option>
																			<option value="44">44</option>
																			<option value="45">45</option>
																			<option value="46">46</option>
																			<option value="47">47</option>
																			<option value="48">48</option>
																			<option value="49">49</option>
																			<option value="50">50</option>
																			<option value="51">51</option>
																			<option value="52">52</option>
																			<option value="53">53</option>
																			<option value="54">54</option>
																			<option value="55">55</option>
																			<option value="56">56</option>
																			<option value="57">57</option>
																			<option value="58">58</option>
																			<option value="59">59</option>
																		</select>
																		<span style="position:absolute; top:5px; right:-15px;">분</span>
																	</div>
																</td>
															</tr>
															<tr>
																<th scope="row"><spring:message code='epms.occurr.type' /></th><!-- 고장유형 -->
																<td>
																	<div class="sel-wrap">
																		<tag:combo codeGrp="TROUBLE1" name="TROUBLE_TYPE1" choose="Y"/>
																	</div>
																</td>
																<th scope="row"><spring:message code='epms.active' /></th><!-- 조치 -->
																<td>
																	<div class="sel-wrap">
																		<tag:combo codeGrp="TROUBLE2" name="TROUBLE_TYPE2" choose="Y"/>
																	</div>
																</td>
															</tr>
<!-- 															<tr> -->
<!-- 																<th scope="row">코딩</th> -->
<!-- 																<td> -->
<!-- 																	<div class="sel-wrap wd-per-45 rel mgn-t-5 f-l"> -->
<%-- 																		<tag:combo codeGrp="CATALOG_D" name="CATALOG_D_GRP" upperComcd="ISNULL" companyId="${companyId}" choose="Y"/> --%>
<!-- 																	</div> -->
<!-- 																	<div class="sel-wrap wd-cal-55 rel mgn-t-5 mgn-l-5 f-l"> -->
<!-- 																		<select id="CATALOG_D" name="CATALOG_D"> -->
<!-- 																			<option value="00">선택하세요</option> -->
<!-- 																		</select> -->
<!-- 																	</div> -->
<!-- 																</td> -->
<!-- 																<th scope="row">대상(오브젝트 부품)</th> -->
<!-- 																<td> -->
<!-- 																	<div class="sel-wrap wd-per-45 rel mgn-t-5 f-l"> -->
<%-- 																		<tag:combo codeGrp="CATALOG_B" name="CATALOG_B_GRP" upperComcd="ISNULL" companyId="${companyId}" choose="Y"/> --%>
<!-- 																	</div> -->
<!-- 																	<div class="sel-wrap wd-cal-55 rel mgn-t-5 mgn-l-5 f-l"> -->
<!-- 																		<select id="CATALOG_B" name="CATALOG_B"> -->
<!-- 																			<option value="00">선택하세요</option> -->
<!-- 																		</select> -->
<!-- 																	</div> -->
<!-- 																</td> -->
<!-- 															</tr> -->
<!-- 															<tr> -->
<!-- 																<th scope="row">손상</th> -->
<!-- 																<td> -->
<!-- 																	<div class="sel-wrap wd-per-45 rel mgn-t-5 f-l"> -->
<%-- 																		<tag:combo codeGrp="CATALOG_C" name="CATALOG_C_GRP" upperComcd="ISNULL" companyId="${companyId}" choose="Y"/> --%>
<!-- 																	</div> -->
<!-- 																	<div class="sel-wrap wd-cal-55 rel mgn-t-5 mgn-l-5 f-l"> -->
<!-- 																		<select id="CATALOG_C" name="CATALOG_C"> -->
<!-- 																			<option value="00">선택하세요</option> -->
<!-- 																		</select> -->
<!-- 																	</div> -->
<!-- 																</td> -->
<!-- 																<th scope="row">원인</th> -->
<!-- 																<td> -->
<!-- 																	<div class="sel-wrap wd-per-45 rel mgn-t-5 f-l"> -->
<%-- 																		<tag:combo codeGrp="CATALOG_5" name="CATALOG_5_GRP" upperComcd="ISNULL" companyId="${companyId}" choose="Y"/> --%>
<!-- 																	</div> -->
<!-- 																	<div class="sel-wrap wd-cal-55 rel mgn-t-5 mgn-l-5 f-l"> -->
<!-- 																		<select id="CATALOG_5" name="CATALOG_5"> -->
<!-- 																			<option value="00">선택하세요</option> -->
<!-- 																		</select> -->
<!-- 																	</div> -->
<!-- 																</td> -->
<!-- 															</tr> -->
															<tr>
																<th scope="row"><spring:message code='epms.work.content' /></th><!-- 작업내용 -->
																<td colspan="3">
																	<div class="txt-wrap">
																		<div class="txtArea">
																			<textarea cols="" rows="3" title="정비내용" id="REPAIR_DESCRIPTION" name="REPAIR_DESCRIPTION"><c:out value="${repairResultDtl.REPAIR_DESCRIPTION}" escapeXml="true" /></textarea>
																		</div>
																	</div>
																</td>
															</tr>
															<tr>
																<th scope="row"><spring:message code='epms.repair.time.start' /></th><!-- 정비시작시간 -->
																<td>
																	<div class="f-l wd-per-50">
																		<input type="hidden" class="inp-comm moDatePic inpCal" readonly="readonly" id="popRepairResultRegForm_REPAIR_TIME1" name="REPAIR_TIME1" title="정비시작일">
																		<input type="text" class="inp-comm moDatePic inpCal" readonly="readonly" id="popRepairResultRegForm_REPAIR_TIME1_DATE" name="REPAIR_TIME1_DATE" title="정비시작일">
																	</div>
																	<div class="sel-wrap wd-cal-25 rel mgn-l-5 mgn-r-25 f-l">
																		<select title="정비시작시간" id="popRepairResultRegForm_REPAIR_TIME1_HOUR" name="REPAIR_TIME1_HOUR">
																			<option value="00">00</option>
																			<option value="01">01</option>
																			<option value="02">02</option>
																			<option value="03">03</option>
																			<option value="04">04</option>
																			<option value="05">05</option>
																			<option value="06">06</option>
																			<option value="07">07</option>
																			<option value="08">08</option>
																			<option value="09">09</option>
																			<option value="10">10</option>
																			<option value="11">11</option>
																			<option value="12">12</option>
																			<option value="13">13</option>
																			<option value="14">14</option>
																			<option value="15">15</option>
																			<option value="16">16</option>
																			<option value="17">17</option>
																			<option value="18">18</option>
																			<option value="19">19</option>
																			<option value="20">20</option>
																			<option value="21">21</option>
																			<option value="22">22</option>
																			<option value="23">23</option>
																		</select>
																		<span style="position:absolute; top:5px; right:-15px;">시</span>
																	</div>
																	<div class="sel-wrap wd-cal-25 rel mgn-l-5 f-l">
																		<select title="정비시작분" id="popRepairResultRegForm_REPAIR_TIME1_MINUTE" name="REPAIR_TIME1_MINUTE">
																			<option value="00">00</option>
																			<option value="01">01</option>
																			<option value="02">02</option>
																			<option value="03">03</option>
																			<option value="04">04</option>
																			<option value="05">05</option>
																			<option value="06">06</option>
																			<option value="07">07</option>
																			<option value="08">08</option>
																			<option value="09">09</option>
																			<option value="10">10</option>
																			<option value="11">11</option>
																			<option value="12">12</option>
																			<option value="13">13</option>
																			<option value="14">14</option>
																			<option value="15">15</option>
																			<option value="16">16</option>
																			<option value="17">17</option>
																			<option value="18">18</option>
																			<option value="19">19</option>
																			<option value="20">20</option>
																			<option value="21">21</option>
																			<option value="22">22</option>
																			<option value="23">23</option>
																			<option value="24">24</option>
																			<option value="25">25</option>
																			<option value="26">26</option>
																			<option value="27">27</option>
																			<option value="28">28</option>
																			<option value="29">29</option>
																			<option value="30">30</option>
																			<option value="31">31</option>
																			<option value="32">32</option>
																			<option value="33">33</option>
																			<option value="34">34</option>
																			<option value="35">35</option>
																			<option value="36">36</option>
																			<option value="37">37</option>
																			<option value="38">38</option>
																			<option value="39">39</option>
																			<option value="40">40</option>
																			<option value="41">41</option>
																			<option value="42">42</option>
																			<option value="43">43</option>
																			<option value="44">44</option>
																			<option value="45">45</option>
																			<option value="46">46</option>
																			<option value="47">47</option>
																			<option value="48">48</option>
																			<option value="49">49</option>
																			<option value="50">50</option>
																			<option value="51">51</option>
																			<option value="52">52</option>
																			<option value="53">53</option>
																			<option value="54">54</option>
																			<option value="55">55</option>
																			<option value="56">56</option>
																			<option value="57">57</option>
																			<option value="58">58</option>
																			<option value="59">59</option>
																		</select>
																		<span style="position:absolute; top:5px; right:-15px;">분</span>
																	</div>
																</td>
																<th scope="row"><spring:message code='epms.repair.time.end' /></th><!-- 정비종료시간 -->
																<td>
																	<div class="f-l wd-per-50">
																		<input type="hidden" class="inp-comm moDatePic inpCal" readonly="readonly" id="popRepairResultRegForm_REPAIR_TIME2" name="REPAIR_TIME2" title="정비종료일">
																		<input type="text" class="inp-comm moDatePic inpCal" readonly="readonly" id="popRepairResultRegForm_REPAIR_TIME2_DATE" name="REPAIR_TIME2_DATE" title="정비종료일">
																	</div>
																	<div class="sel-wrap wd-cal-25 rel mgn-l-5 mgn-r-25 f-l">
																		<select title="정비종료시간" id="popRepairResultRegForm_REPAIR_TIME2_HOUR" name="REPAIR_TIME2_HOUR">
																			<option value="00">00</option>
																			<option value="01">01</option>
																			<option value="02">02</option>
																			<option value="03">03</option>
																			<option value="04">04</option>
																			<option value="05">05</option>
																			<option value="06">06</option>
																			<option value="07">07</option>
																			<option value="08">08</option>
																			<option value="09">09</option>
																			<option value="10">10</option>
																			<option value="11">11</option>
																			<option value="12">12</option>
																			<option value="13">13</option>
																			<option value="14">14</option>
																			<option value="15">15</option>
																			<option value="16">16</option>
																			<option value="17">17</option>
																			<option value="18">18</option>
																			<option value="19">19</option>
																			<option value="20">20</option>
																			<option value="21">21</option>
																			<option value="22">22</option>
																			<option value="23">23</option>
																		</select>
																		<span style="position:absolute; top:5px; right:-15px;">시</span>
																	</div>
																	<div class="sel-wrap wd-cal-25 rel mgn-l-5 f-l">
																		<select title="정비종료분" id="popRepairResultRegForm_REPAIR_TIME2_MINUTE" name="REPAIR_TIME2_MINUTE">
																			<option value="00">00</option>
																			<option value="01">01</option>
																			<option value="02">02</option>
																			<option value="03">03</option>
																			<option value="04">04</option>
																			<option value="05">05</option>
																			<option value="06">06</option>
																			<option value="07">07</option>
																			<option value="08">08</option>
																			<option value="09">09</option>
																			<option value="10">10</option>
																			<option value="11">11</option>
																			<option value="12">12</option>
																			<option value="13">13</option>
																			<option value="14">14</option>
																			<option value="15">15</option>
																			<option value="16">16</option>
																			<option value="17">17</option>
																			<option value="18">18</option>
																			<option value="19">19</option>
																			<option value="20">20</option>
																			<option value="21">21</option>
																			<option value="22">22</option>
																			<option value="23">23</option>
																			<option value="24">24</option>
																			<option value="25">25</option>
																			<option value="26">26</option>
																			<option value="27">27</option>
																			<option value="28">28</option>
																			<option value="29">29</option>
																			<option value="30">30</option>
																			<option value="31">31</option>
																			<option value="32">32</option>
																			<option value="33">33</option>
																			<option value="34">34</option>
																			<option value="35">35</option>
																			<option value="36">36</option>
																			<option value="37">37</option>
																			<option value="38">38</option>
																			<option value="39">39</option>
																			<option value="40">40</option>
																			<option value="41">41</option>
																			<option value="42">42</option>
																			<option value="43">43</option>
																			<option value="44">44</option>
																			<option value="45">45</option>
																			<option value="46">46</option>
																			<option value="47">47</option>
																			<option value="48">48</option>
																			<option value="49">49</option>
																			<option value="50">50</option>
																			<option value="51">51</option>
																			<option value="52">52</option>
																			<option value="53">53</option>
																			<option value="54">54</option>
																			<option value="55">55</option>
																			<option value="56">56</option>
																			<option value="57">57</option>
																			<option value="58">58</option>
																			<option value="59">59</option>
																		</select>
																		<span style="position:absolute; top:5px; right:-15px;">분</span>
																	</div>
																</td>
															</tr>
															<tr>
																<th scope="row"><spring:message code='epms.use' /><spring:message code='epms.object' /></th><!-- 사용자재 -->
																<td colspan="3">
																	<input type="hidden" class="inp-comm" id="MATERIAL_INOUT" name="MATERIAL_INOUT" value="${repairResultDtl.MATERIAL_INOUT}"/>
																	<a href="#none" class="btn evn-st01 mgn-t-5" onclick="fnSearchMaterial()">검색</a>
																	<ul class="usedMaterial" id="usedMaterial">
																		<c:forEach var="item" items="${usedMaterial}">
																			<li style="overflow:hidden; margin:5px 0;">
																				<input type="hidden" class="inp-comm" title="BOMID" name="equipmentBomUsedArr" value="${item.EQUIPMENT_BOM}"	readonly="readonly">
																				<input type="hidden" class="inp-comm" title="자재ID" name="usedMaterialId" value="${item.MATERIAL}" readonly="readonly">
																				<input type="hidden" class="inp-comm" title="자재ID" name="materialUsedArr" value=""	readonly="readonly">
																				<input type="hidden" class="inp-comm" title="단위"   name="unitArr" 		   value="${item.UNIT}"	readonly="readonly">
																				<div class="box-col wd-per-20 f-l">
																					<div class="inp-wrap readonly wd-per-100">
																						<input type="text" class="inp-comm" title="자재코드" name="material_uidUsedArr" value="${item.MATERIAL_UID}"	readonly="readonly">
																					</div>
																				</div>
																				<div class="box-col wd-per-20 f-l mgn-l-5">
																					<div class="inp-wrap readonly wd-per-100">
																						<input type="text" class="inp-comm" title="자재명" name="" value="${item.MATERIAL_NAME}"	readonly="readonly">
																					</div>
																				</div>
																				<div class="box-col wd-per-20 f-l mgn-l-5">
																					<div class="inp-wrap readonly wd-per-100">
																						<input type="text" class="inp-comm" title="단위명" name="" value="${item.UNIT}"	readonly="readonly">
																					</div>
																				</div>
																				<div class="box-col wd-per-10 f-l  mgn-l-5">
																					<div class="inp-wrap wd-per-100">
																						<input type="text" class="inp-comm t-r" title="수량" name="qntyUsedArr"	value="${item.QNTY}"	onchange="check_Null(this);"	onkeydown="check_isNum_onkeydown();" autocomplete="off">
																					</div>
																				</div>
																				<input type="hidden" class="inp-comm" title="삭제여부" name="delYnArr" value="N"	readonly="readonly">
																				<a href="#none" class="btnRemove mgn-l-5" onclick="delList2($(this))">삭제</a>
																			</li>
																		</c:forEach>
																	</ul>
																</td>
															</tr>
															<tr>
																<th scope="row"><spring:message code='epms.use' /><spring:message code='epms.object' /> (<spring:message code='epms.unregist' />)</th><!-- 사용자재(미등록) -->
																<td >
																	<div class="inp-wrap wd-per-100">
																		<input type="text" class="inp-comm" id="MATERIAL_UNREG" name="MATERIAL_UNREG" value="<c:out value="${repairResultDtl.MATERIAL_UNREG}" escapeXml="true" />"/>
																	</div>
																</td>
																<th scope="row"><spring:message code='epms.cost' /></th><!-- 비용 -->
																<td >
																	<div class="inp-wrap wd-per-95">
																		<input type="text" class="inp-comm inpTxt ar text_right onlyNumber" style="text-align: right;" maxlength="12" onchange="getNumber(this);" onkeyup="getNumber(this);" title="비용" id="COST" name="COST" value="<c:out value="${repairResultDtl.COST}" escapeXml="true" />">
																		<span class="unit">원</span>
																	</div>
																</td>
															</tr>
															<tr>
																<th scope="row"><spring:message code='epms.attach' /></th><!-- 정비 첨부파일 -->
																<td colspan="3">
																	<div class="fileWrap" id="viewFrame">
																		<div class="f-l wd-per-100">
																			<a href="#none" id="modifyBtn" class="btn comm st01 f-l">첨부파일 수정</a>			
																		</div>
																		<c:choose>
																			<c:when test="${fn:length(attachList2)>0 }">
																			<div id="previewList" class="wd-per-100">
																				<div class="bxType01 fileWrap previewFileLst">
																					<div>
																						<div class="fileList type02">
																							<div id="detailFileList0" class="fileDownLst">
																								<c:forEach var="item" items="${attachList2}" varStatus="idx">
																									<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																										<span class="MultiFile-export">${item.SEQ_DSP}</span> 
																										<span class="MultiFile-title" title="File selected: ${item.NAME}">
																										${item.FILE_NAME} (${item.FILE_SIZE_TXT})
																										</span>
																										<a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a>
																									</div>		
																								</c:forEach>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div id="repair_previewImg" class="wd-per-100">
																				<div class="previewFile mgn-t-15">
																					<h3 class="tit-cont preview" id="previewTitle">이미지 미리보기</h3>
																					<ul style="margin-top:5px;">
																						<c:forEach var="item" items="${attachList2}" varStatus="idx">
																							<c:if test = "${item.FORMAT eq 'jpg' || item.FORMAT eq 'png' || item.FORMAT eq 'jpeg' || item.FORMAT eq 'JPG' || item.FORMAT eq 'PNG'}">
																								<li>
																									<img src="/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}" title="클릭하시면 원본크기로 보실 수 있습니다." onclick="originFileView('/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}')" />
																									<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																										<span class="MultiFile-title" title="File selected: ${item.NAME}">
																											<p>${item.FILE_NAME} (${item.FILE_SIZE_TXT})</p>
																										</span>
																										<a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a>
																									</div>
																									
																								</li>
																							</c:if>
																						</c:forEach>
																					</ul>
																				</div>
																			</div>
																			</c:when>
																			<c:otherwise>
																				<div class="f-l wd-per-100 mgn-t-5">
																				※ 등록된 첨부파일이 없습니다.
																				</div>
																			</c:otherwise>
																		</c:choose>
																	</div>
																	<div class="bxType01 fileWrap" id="regFrame" style="display:none;">
																		<div id="attachFile" class="filebox"></div>
																		<div class="fileList">
																			<div id="detailFileRegList0" class="fileDownLst">
																				<c:forEach var="item" items="${attachList2}" varStatus="idx">
																					<div class="MultiFile-label" id="fileRegLstWrap_${item.ATTACH}">
																						<span class="MultiFile-remove">
																							<a href="#none" class="icn_fileDel" data-attach="${item.ATTACH}" data-idx="${item.CD_FILE_TYPE }">
																								<img src="/images/com/web/btn_remove.png" />
																							</a>
																						</span> 
																						<span class="MultiFile-title" title="File selected: ${item.NAME}">
																							${item.FILE_NAME} (${item.FILE_SIZE_TXT})
																						</span>
																					</div>
																				</c:forEach>
																			</div>
																		</div>
																	</div>
																</td>
															</tr>
															</div>
														</tbody>
													</table>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
							<%-- 실적등록 판넬 : e --%>
						
						</div>
					</div>
			</div>
			<iframe id="fileDownFrame" style="width:0px;height:0px;border:0px"></iframe>
		</div>
	</div>
</div>

<script type="text/javascript">
	// 정비원 컬럼 아코디언 기능 실행
	var repairMemAccSlider=new TINY.accordion.slider("repairMemAccSlider");
	repairMemAccSlider.init("repairMember2","repairAccOpenBtn",0,-1);
</script>

<script type="text/template" id="repairMemberItemViewTemplateY">
<!-- s : 추가버튼 클릭시 -->
<div class="repairTimeAddLst" value="N">
	<input type="hidden" class="inp-comm" title="삭제여부"			name="DEL_YN" value=""/>
	<input type="hidden" class="inp-comm" title="정비원ID"		name="USER_ID" value="">
	<input type="hidden" class="inp-comm" title="작업이력상세 ID"	name="WORK_LOG_ITEM" value="">
	<input type="hidden" class="inp-comm" title="작업이력상세정보"	name="workLogItemArr" value="">
	<div class="overflow">
		<div class="f-l">
			<div class="inTit">정비시작시간</div>
		</div>
		<div class="f-l wd-per-15">
			<div class="inp-wrap readonly wd-per-100">
				<input type="hidden" class="inp-comm" readonly="readonly" name="DATE_START" title="정비시작일">
				<input type="text" class="inp-comm" readonly="readonly" name="DATE_START_DATE" title="정비시작일" value="">
			</div>
		</div>
		
		<div class="wd-cal-10 rel mgn-l-5 mgn-r-25 f-l inp-wrap readonly">
			<input type="text" class="inp-comm" readonly="readonly" name="DATE_START_HOUR" title="정비시작시간" value="">
			<span style="position:absolute; top:5px; right:-15px;">시</span>
		</div>
		
		<div class="wd-cal-10 rel mgn-l-5 mgn-r-25 f-l inp-wrap readonly">
			<input type="text" class="inp-comm" readonly="readonly" title="정비시작분" name="DATE_START_MINUTE" value="">
			<span style="position:absolute; top:5px; right:-15px;">분</span>
		</div>
	</div>
	<div class="overflow mgn-t-5">
		<div class="f-l">
			<div class="inTit">정비종료시간</div>
		</div>
		<div class="f-l wd-per-15">
			<div class="inp-wrap readonly wd-per-100">
				<input type="hidden" class="inp-comm" name="DATE_END" title="정비종료일" readonly="readonly">
				<input type="text" class="inp-comm" name="DATE_END_DATE" title="정비종료일" value="">
			</div>
		</div>
						
		<div class="wd-cal-10 rel mgn-l-5 mgn-r-25 f-l inp-wrap readonly">
			<input type="text" class="inp-comm" readonly="readonly" name="DATE_END_HOUR" title="정비시작시간" value="">
			<span style="position:absolute; top:5px; right:-15px;">시</span>
		</div>
		
		<div class="wd-cal-10 rel mgn-l-5 mgn-r-25 f-l inp-wrap readonly">
			<input type="text" class="inp-comm" readonly="readonly" title="정비종료분" name="DATE_END_MINUTE" value="">
			<span style="position:absolute; top:5px; right:-15px;">분</span>
		</div>
	</div>
	<div class="overflow mgn-t-5">
		<div class="f-l">
			<div class="inTit">정비내용</div>
		</div>
		<div class="f-l wd-per-75">
			<div class="txt-wrap readonly"> 
				<div class="txtArea">
					<textarea cols="" rows="3" title="정비내용" id="ITEM_DESCRIPTION" name="ITEM_DESCRIPTION" readonly="readonly" ></textarea>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- e : 추가버튼 클릭시 -->
</script>

<script type="text/template" id="repairMemberItemViewTemplateN">
	<!-- s : 추가버튼 클릭시 -->
	<div class="repairTimeAddLst" value="N">
		<input type="hidden" class="inp-comm" title="삭제여부"			name="DEL_YN" value=""/>
		<input type="hidden" class="inp-comm" title="정비원ID"		name="USER_ID" value="">
		<input type="hidden" class="inp-comm" title="작업이력상세 ID"	name="WORK_LOG_ITEM" value="">
		<input type="hidden" class="inp-comm" title="작업이력상세정보"	name="workLogItemArr" value="">
		<div class="overflow">
			<div class="f-l">
				<div class="inTit">정비시작시간</div>
			</div>
			<div class="f-l wd-per-15">
				<input type="hidden" class="inp-comm moDatePic inpCal" readonly="readonly" name="DATE_START" title="정비시작일">
				<input type="text" class="inp-comm moDatePic inpCal" readonly="readonly" name="DATE_START_DATE" title="정비시작일" value="">
			</div>
			
			<div class="sel-wrap wd-cal-10 rel mgn-l-5 mgn-r-25 f-l">
				<select title="정비시작시간" name="DATE_START_HOUR">
					<option value="00">00</option>
					<option value="01">01</option>
					<option value="02">02</option>
					<option value="03">03</option>
					<option value="04">04</option>
					<option value="05">05</option>
					<option value="06">06</option>
					<option value="07">07</option>
					<option value="08">08</option>
					<option value="09">09</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="21">21</option>
					<option value="22">22</option>
					<option value="23">23</option>
				</select>
				<span style="position:absolute; top:5px; right:-15px;">시</span>
			</div>
			
			<div class="sel-wrap wd-cal-10 rel mgn-l-5 f-l">
				<select title="정비시작분" name="DATE_START_MINUTE" >
					<option value="00">00</option>
					<option value="01">01</option>
					<option value="02">02</option>
					<option value="03">03</option>
					<option value="04">04</option>
					<option value="05">05</option>
					<option value="06">06</option>
					<option value="07">07</option>
					<option value="08">08</option>
					<option value="09">09</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="21">21</option>
					<option value="22">22</option>
					<option value="23">23</option>
					<option value="24">24</option>
					<option value="25">25</option>
					<option value="26">26</option>
					<option value="27">27</option>
					<option value="28">28</option>
					<option value="29">29</option>
					<option value="30">30</option>
					<option value="31">31</option>
					<option value="32">32</option>
					<option value="33">33</option>
					<option value="34">34</option>
					<option value="35">35</option>
					<option value="36">36</option>
					<option value="37">37</option>
					<option value="38">38</option>
					<option value="39">39</option>
					<option value="40">40</option>
					<option value="41">41</option>
					<option value="42">42</option>
					<option value="43">43</option>
					<option value="44">44</option>
					<option value="45">45</option>
					<option value="46">46</option>
					<option value="47">47</option>
					<option value="48">48</option>
					<option value="49">49</option>
					<option value="50">50</option>
					<option value="51">51</option>
					<option value="52">52</option>
					<option value="53">53</option>
					<option value="54">54</option>
					<option value="55">55</option>
					<option value="56">56</option>
					<option value="57">57</option>
					<option value="58">58</option>
					<option value="59">59</option>
				</select>
				<span style="position:absolute; top:5px; right:-15px;">분</span>
			</div>
		</div>
		
		<div class="overflow mgn-t-5">
			<div class="f-l">
				<div class="inTit">정비종료시간</div>
			</div>
			<div class="f-l wd-per-15">
				<input type="hidden" class="inp-comm moDatePic inpCal" readonly="readonly" name="DATE_END" title="정비종료일">
				<input type="text" class="inp-comm moDatePic inpCal" readonly="readonly" name="DATE_END_DATE" title="정비종료일" value="">
			</div>
			
			<div class="sel-wrap wd-cal-10 rel mgn-l-5 mgn-r-25 f-l">
				<select title="정비종료시간" name="DATE_END_HOUR" >
					<option value="00">00</option>
					<option value="01">01</option>
					<option value="02">02</option>
					<option value="03">03</option>
					<option value="04">04</option>
					<option value="05">05</option>
					<option value="06">06</option>
					<option value="07">07</option>
					<option value="08">08</option>
					<option value="09">09</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="21">21</option>
					<option value="22">22</option>
					<option value="23">23</option>
				</select>
				<span style="position:absolute; top:5px; right:-15px;">시</span>
			</div>
			
			<div class="sel-wrap wd-cal-10 rel mgn-l-5 f-l">
				<select title="정비종료분" name="DATE_END_MINUTE" >
					<option value="00">00</option>
					<option value="01">01</option>
					<option value="02">02</option>
					<option value="03">03</option>
					<option value="04">04</option>
					<option value="05">05</option>
					<option value="06">06</option>
					<option value="07">07</option>
					<option value="08">08</option>
					<option value="09">09</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="21">21</option>
					<option value="22">22</option>
					<option value="23">23</option>
					<option value="24">24</option>
					<option value="25">25</option>
					<option value="26">26</option>
					<option value="27">27</option>
					<option value="28">28</option>
					<option value="29">29</option>
					<option value="30">30</option>
					<option value="31">31</option>
					<option value="32">32</option>
					<option value="33">33</option>
					<option value="34">34</option>
					<option value="35">35</option>
					<option value="36">36</option>
					<option value="37">37</option>
					<option value="38">38</option>
					<option value="39">39</option>
					<option value="40">40</option>
					<option value="41">41</option>
					<option value="42">42</option>
					<option value="43">43</option>
					<option value="44">44</option>
					<option value="45">45</option>
					<option value="46">46</option>
					<option value="47">47</option>
					<option value="48">48</option>
					<option value="49">49</option>
					<option value="50">50</option>
					<option value="51">51</option>
					<option value="52">52</option>
					<option value="53">53</option>
					<option value="54">54</option>
					<option value="55">55</option>
					<option value="56">56</option>
					<option value="57">57</option>
					<option value="58">58</option>
					<option value="59">59</option>
				</select>
				<span style="position:absolute; top:5px; right:-15px;">분</span>
			</div>
			
		</div>
		<div class="overflow mgn-t-5">
			<div class="f-l">
				<div class="inTit">정비내용</div>
			</div>
			<div class="f-l wd-per-75">
				<div class="txt-wrap">
					<div class="txtArea">
						<textarea cols="" rows="3" title="정비내용" id="ITEM_DESCRIPTION" name="ITEM_DESCRIPTION"></textarea>
					</div>
				</div>
			</div>
		</div>
		<a href="#none" class="repairTimeRowDel deleteBtn" onclick="repairMemberControll.remove(this)"></a>
	</div>
	<!-- e : 추가버튼 클릭시 -->
</script>

<script type="text/template" id="repairMemberItemAddTemplate">
<div class="repairTimeAddLst">
	<input type="hidden" class="inp-comm" title="삭제여부"			name="DEL_YN" value="N"/>
	<input type="hidden" class="inp-comm" title="작업이력상세 ID"	name="WORK_LOG_ITEM" value="">
	<div class="overflow">
		<div class="f-l">
			<div class="inTit">정비시작시간</div>
		</div>
		<div class="f-l wd-per-15">
			<input type="hidden" class="inp-comm moDatePic inpCal" readonly="readonly" name="DATE_START" title="정비시작일">
			<input type="text" class="inp-comm moDatePic inpCal" readonly="readonly" name="DATE_START_DATE" title="정비시작일">
		</div>
		<div class="sel-wrap wd-cal-10 rel mgn-l-5 mgn-r-25 f-l">
			<select title="정비시작시간" id="popRepairResultRegForm_REPAIR_TIME1_HOUR" name="DATE_START_HOUR">
				<option value="00">00</option>
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				<option value="13">13</option>
				<option value="14">14</option>
				<option value="15">15</option>
				<option value="16">16</option>
				<option value="17">17</option>
				<option value="18">18</option>
				<option value="19">19</option>
				<option value="20">20</option>
				<option value="21">21</option>
				<option value="22">22</option>
				<option value="23">23</option>
			</select>
			<span style="position:absolute; top:5px; right:-15px;">시</span>
		</div>
		<div class="sel-wrap wd-cal-10 rel mgn-l-5 f-l">
			<select title="정비시작분" id="popRepairResultRegForm_REPAIR_TIME1_MINUTE" name="DATE_START_MINUTE">
				<option value="00">00</option>
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				<option value="13">13</option>
				<option value="14">14</option>
				<option value="15">15</option>
				<option value="16">16</option>
				<option value="17">17</option>
				<option value="18">18</option>
				<option value="19">19</option>
				<option value="20">20</option>
				<option value="21">21</option>
				<option value="22">22</option>
				<option value="23">23</option>
				<option value="24">24</option>
				<option value="25">25</option>
				<option value="26">26</option>
				<option value="27">27</option>
				<option value="28">28</option>
				<option value="29">29</option>
				<option value="30">30</option>
				<option value="31">31</option>
				<option value="32">32</option>
				<option value="33">33</option>
				<option value="34">34</option>
				<option value="35">35</option>
				<option value="36">36</option>
				<option value="37">37</option>
				<option value="38">38</option>
				<option value="39">39</option>
				<option value="40">40</option>
				<option value="41">41</option>
				<option value="42">42</option>
				<option value="43">43</option>
				<option value="44">44</option>
				<option value="45">45</option>
				<option value="46">46</option>
				<option value="47">47</option>
				<option value="48">48</option>
				<option value="49">49</option>
				<option value="50">50</option>
				<option value="51">51</option>
				<option value="52">52</option>
				<option value="53">53</option>
				<option value="54">54</option>
				<option value="55">55</option>
				<option value="56">56</option>
				<option value="57">57</option>
				<option value="58">58</option>
				<option value="59">59</option>
			</select>
			<span style="position:absolute; top:5px; right:-15px;">분</span>
		</div>
	</div>
	<div class="overflow mgn-t-5">
		<div class="f-l">
			<div class="inTit">정비종료시간</div>
		</div>
		<div class="f-l wd-per-15">
			<input type="hidden" class="inp-comm moDatePic inpCal" readonly="readonly" name="DATE_END" title="정비종료일">
			<input type="text" class="inp-comm moDatePic inpCal" readonly="readonly" name="DATE_END_DATE" title="정비종료일">
		</div>
		<div class="sel-wrap wd-cal-10 rel mgn-l-5 mgn-r-25 f-l">
			<select title="정비종료시간" id="popRepairResultRegForm_DATE_END_HOUR" name="DATE_END_HOUR">
				<option value="00">00</option>
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				<option value="13">13</option>
				<option value="14">14</option>
				<option value="15">15</option>
				<option value="16">16</option>
				<option value="17">17</option>
				<option value="18">18</option>
				<option value="19">19</option>
				<option value="20">20</option>
				<option value="21">21</option>
				<option value="22">22</option>
				<option value="23">23</option>
			</select>
			<span style="position:absolute; top:5px; right:-15px;">시</span>
		</div>
		<div class="sel-wrap wd-cal-10 rel mgn-l-5 f-l">
			<select title="정비종료분" id="popRepairResultRegForm_DATE_END_MINUTE" name="DATE_END_MINUTE">
				<option value="00">00</option>
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				<option value="13">13</option>
				<option value="14">14</option>
				<option value="15">15</option>
				<option value="16">16</option>
				<option value="17">17</option>
				<option value="18">18</option>
				<option value="19">19</option>
				<option value="20">20</option>
				<option value="21">21</option>
				<option value="22">22</option>
				<option value="23">23</option>
				<option value="24">24</option>
				<option value="25">25</option>
				<option value="26">26</option>
				<option value="27">27</option>
				<option value="28">28</option>
				<option value="29">29</option>
				<option value="30">30</option>
				<option value="31">31</option>
				<option value="32">32</option>
				<option value="33">33</option>
				<option value="34">34</option>
				<option value="35">35</option>
				<option value="36">36</option>
				<option value="37">37</option>
				<option value="38">38</option>
				<option value="39">39</option>
				<option value="40">40</option>
				<option value="41">41</option>
				<option value="42">42</option>
				<option value="43">43</option>
				<option value="44">44</option>
				<option value="45">45</option>
				<option value="46">46</option>
				<option value="47">47</option>
				<option value="48">48</option>
				<option value="49">49</option>
				<option value="50">50</option>
				<option value="51">51</option>
				<option value="52">52</option>
				<option value="53">53</option>
				<option value="54">54</option>
				<option value="55">55</option>
				<option value="56">56</option>
				<option value="57">57</option>
				<option value="58">58</option>
				<option value="59">59</option>
			</select>
			<span style="position:absolute; top:5px; right:-15px;">분</span>
		</div>
	</div>
	<div class="overflow mgn-t-5">
		<div class="f-l">
			<div class="inTit">정비내용</div>
		</div>
		<div class="f-l wd-per-75">
			<div class="txt-wrap">
				<div class="txtArea">
					<textarea cols="" rows="3" title="정비내용" id="ITEM_DESCRIPTION" name="ITEM_DESCRIPTION"></textarea>
				</div>
			</div>
		</div>
	</div>
	
	<a href="#none" class="repairTimeRowDel deleteBtn" onclick="repairMemberControll.remove(this)"></a>
</div>
</script>