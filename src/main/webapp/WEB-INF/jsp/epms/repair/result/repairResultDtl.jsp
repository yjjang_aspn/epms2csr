<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%@ include file="/com/comHeader.jsp"%>			
<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	var maxFileCnt = 30;		<%-- 파일 최대갯수 --%>
	var msg = "";				<%-- 리턴 메시지 --%>

	<%-- 고장시작시간 --%>
	var date = new Date();
	if("${repairResultDtl.TROUBLE_TIME1}" == "" || "${repairResultDtl.TROUBLE_TIME1}" == null){	
		
		$('#popRepairResultForm_TROUBLE_TIME1_DATE').val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		$('#popRepairResultForm_TROUBLE_TIME1_HOUR').val(getDate(date.getHours()));
		var min = parseInt(date.getMinutes());
		min = min - (min%5);
		var minstr;
		if(min<10){
			minstr = "0" + min.toString();
		}else{
			minstr = min.toString();
		}
		$('#popRepairResultForm_TROUBLE_TIME1_MINUTE').val(minstr);
	}
	else{
		var trouble_time1 = "${repairResultDtl.TROUBLE_TIME1}";
		$('#popRepairResultForm_TROUBLE_TIME1_DATE').val(trouble_time1.substring(0,8));
		$('#popRepairResultForm_TROUBLE_TIME1_HOUR').val(trouble_time1.substring(8,10));
		$('#popRepairResultForm_TROUBLE_TIME1_MINUTE').val(trouble_time1.substring(10,12));
	}
	<%-- 고장종료시간 --%>
	if("${repairResultDtl.TROUBLE_TIME2}" == "" || "${repairResultDtl.TROUBLE_TIME2}" == null){
		$('#popRepairResultForm_TROUBLE_TIME2_DATE').val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		$('#popRepairResultForm_TROUBLE_TIME2_HOUR').val(getDate(date.getHours()));
		var min = parseInt(date.getMinutes());
		min = min - (min%5);
		var minstr;
		if(min<10){
			minstr = "0" + min.toString();
		}else{
			minstr = min.toString();
		}
		$('#popRepairResultForm_TROUBLE_TIME2_MINUTE').val(minstr);
	}
	else{
		var trouble_time2 = "${repairResultDtl.TROUBLE_TIME2}";
		$('#popRepairResultForm_TROUBLE_TIME2_DATE').val(trouble_time2.substring(0,8));
		$('#popRepairResultForm_TROUBLE_TIME2_HOUR').val(trouble_time2.substring(8,10));
		$('#popRepairResultForm_TROUBLE_TIME2_MINUTE').val(trouble_time2.substring(10,12));
	}
	
	<%-- 첨부파일 관련 --%>
	for(var i=0; i<1; i++){				<%-- 파일 영역 추가를 고려하여 개발 --%>
		$('#attachFile').append('<input type="file" multiple id="fileList" name="fileList">');	
		
		var fileType = 'jpeg |gif |jpg |png |bmp';
	
		$('#fileList').MultiFile({
			   accept :  fileType
			,	list   : '#detailFileList'+i
			,	max    : 5
			,	STRING : {
					remove: '<img src="/sys/images/common/btn_remove.png">'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
					text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" />',
					idx	  : i
				},
		});
		
		<%-- 첨부한 파일의 출력 순서를 자동으로 샛팅 하기 위한 스크립트 - 공통 스크립트로 빠지면 안됨 .개별 항목으로 처리 필수 --%> 
		$('.MultiFile-wrap').change(function(){
			var fileExpIndex = $(this).closest('.fileWrap').find('.fileList > div').children('div:last-child');
			var	fileExp = fileExpIndex.find('.fileExp');
			fileExp.val(fileExpIndex.index() + 1);
		});
	}
	
	<%-- 파일 다운로드 --%>
	$('.btnFileDwn').on('click', function(){
		var ATTACH_GRP_NO = $(this).data("attachgrpno");
		var ATTACH = $(this).data("attach");
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO='+ATTACH_GRP_NO+'&ATTACH='+ATTACH;
		$("#fileDownFrame").attr("src",src);
	});
	
	<%-- 첨부파일 삭제버튼 --%>
	$('.icn_fileDel').on('click', function(){
		delModiFile($(this));
	});
	
	$(".boxscroll").niceScroll({cursorwidth:"4px",cursorcolor:"#afbfd6",horizrailenabled:false});
	
	$("body").on('click', function(){
		$(".boxscroll").getNiceScroll().resize();
	});
	
});

<%-- 이미지 원본크기 보기 --%>
function originFileView(img){ 
	img1= new Image(); 
	img1.src=(img); 
	imgControll(img); 
} 
	  
function imgControll(img){ 
	if((img1.width!=0)&&(img1.height!=0)){ 
		viewImage(img); 
	}else{ 
		controller="imgControll('"+img+"')"; 
		intervalID=setTimeout(controller,20); 
	} 
}

function viewImage(img){ 
	W=img1.width; 
	H=img1.height; 
	O="width="+W+",height="+H+",scrollbars=yes"; 
	imgWin=window.open("","",O); 
	imgWin.document.write("<html><head><title>이미지상세보기</title></head>");
	imgWin.document.write("<body topmargin=0 leftmargin=0>");
	imgWin.document.write("<img src="+img+" onclick='self.close()' style='cursor:pointer;' title ='클릭하시면 창이 닫힙니다.'>");
	imgWin.document.close();
}
</script>

	<!-- s : 상세 -->
	<div class="expandedTabSet hgt-per-100">
		<ul class="expandedTabs">
			<li class="expandedTabLink active" onclick="openTab(event, 'panel1-1')" id="defaultOpen">요청내용</li>
			<c:if test="${repairResultDtl.REPAIR_STATUS eq '03'}">
				<li class="expandedTabLink" onclick="openTab(event, 'panel1-2')"><spring:message code='epms.work.content' /></li><!-- 정비내용 -->
			</c:if>
		</ul>
		<div class="panels boxscroll">
			<div class="panel" id="panel1-1" style="display:block">
				<div class="panel-in rel">
					<div class="tb-wrap">
						<table class="tb-st">
							<caption class="screen-out"><spring:message code='epms.work.perform.detail' /></caption><!-- 정비실적 상세 -->
							<colgroup>
								<col width="15%" />
								<col width="35%" />
								<col width="15%" />
								<col width="*" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><spring:message code='epms.repair.status' /></th><!-- 정비상태 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="hidden" class="inp-comm" id="REPAIR_STATUS" name="REPAIR_STATUS" value="${repairResultDtl.REPAIR_STATUS}"/>
											<c:choose>
												<c:when test="${repairResultDtl.APPR_STATUS eq '01' || repairResultDtl.APPR_STATUS eq '03' || repairResultDtl.APPR_STATUS eq '09'}">
													<input type="text" class="inp-comm" id="repair_status_name" name="repair_status_name" readonly="readonly" value="${repairResultDtl.APPR_STATUS_NAME}"/>
												</c:when>
												<c:otherwise>
													<input type="text" class="inp-comm" id="repair_status_name" name="repair_status_name" readonly="readonly" value="${repairResultDtl.REPAIR_STATUS_NAME}"/>
												</c:otherwise>
											</c:choose>
										</div>
									</td>
									<th scope="row"><spring:message code='epms.repair.part' /></th><!-- 처리파트 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="hidden" class="inp-comm" id="popRepairResultRegForm_PART" name="PART" value="${repairResultDtl.PART}"/>
											<input type="text" class="inp-comm" id="popRepairResultRegForm_part_name" name="part_name" readonly="readonly" value="${repairResultDtl.PART_NAME}"/>
										</div>
									</td>
									
								</tr>
								<tr>
									<th scope="row"><spring:message code='epms.category' /></th><!-- 공장 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="hidden" class="inp-comm" id="popRepairResultRegForm_LOCATION" name="LOCATION" value="${repairResultDtl.LOCATION}"/>
											<input type="text" class="inp-comm" id="popRepairResultRegForm_location_name" name="location_name" readonly="readonly" value="${repairResultDtl.LOCATION_NAME}"/>
										</div>
									</td>
									<th scope="row"><spring:message code='epms.repair.request.date' /></th><!-- 요청일시 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="text" class="inp-comm" id="DATE_REQUEST" name="DATE_REQUEST" readonly="readonly" value="${repairResultDtl.DATE_REQUEST}"/>
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row"><spring:message code='epms.category' /></th><!-- 라인 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="hidden" class="inp-comm" id="CATEGORY" name="CATEGORY" value="${repairResultDtl.CATEGORY}"/>
											<c:choose>
												<c:when test="${repairResultDtl.APPR_STATUS eq '01' || repairResultDtl.APPR_STATUS eq '03' || repairResultDtl.APPR_STATUS eq '09'}">
													<input type="text" class="inp-comm" id="category_name" name="category_name" readonly="readonly" value="${repairResultDtl.LINE_NAME}"/>
												</c:when>
												<c:otherwise>
													<input type="text" class="inp-comm" id="category_name" name="category_name" readonly="readonly" value="${repairResultDtl.CATEGORY_NAME}"/>
												</c:otherwise>
											</c:choose>
										</div>
									</td>
									<th scope="row"><spring:message code='epms.repair.request.part' /></th><!-- 요청부서 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="hidden" class="inp-comm" id="REQUEST_DIV" name="REQUEST_DIV" value="${repairResultDtl.REQUEST_DIV}"/>
											<input type="text" class="inp-comm" id="request_div_name" name="request_div_name" readonly="readonly" value="${repairResultDtl.REQUEST_DIV_NAME}"/>
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row"><spring:message code='epms.system.uid' /></th><!-- 설비코드 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="hidden" class="inp-comm" id="popRepairResultRegForm_EQUIPMENT" name="EQUIPMENT" value="${repairResultDtl.EQUIPMENT}"/>
											<input type="text" class="inp-comm" id="popRepairResultRegForm_EQUIPMENT_UID" name="EQUIPMENT_UID" readonly="readonly" value="${repairResultDtl.EQUIPMENT_UID}"/>
										</div>
									</td>
									<th scope="row"><spring:message code='epms.repair.request' /></th><!-- 요청자 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="hidden" class="inp-comm" id="REQUEST_MEMBER" name="REQUEST_MEMBER" value="${repairResultDtl.REQUEST_MEMBER}"/>
											<input type="text" class="inp-comm" id="request_member_name" name="request_member_name" readonly="readonly" value="${repairResultDtl.REQUEST_MEMBER_NAME}"/>
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row"><spring:message code='epms.system.name' /></th><!-- 설비명 -->
									<td colspan="3">
										<div class="inp-wrap readonly wd-per-100">
											<input type="text" class="inp-comm" id="EQUIPMENT_NAME" name="EQUIPMENT_NAME" readonly="readonly" value="${repairResultDtl.EQUIPMENT_NAME}"/>
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row"><spring:message code='epms.request.content' /></th><!-- 고장내용 -->
									<td colspan="3">
										<div class="txt-wrap readonly">
											<div class="txtArea">
												<textarea cols="" rows="3" title="고장내용" id="REQUEST_DESCRIPTION" name="REQUEST_DESCRIPTION" readonly="readonly">${repairResultDtl.REQUEST_DESCRIPTION}</textarea>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row"><spring:message code='epms.repair.manager' /></th><!-- 담당자 -->
									<td colspan="3">
										<div class="inp-wrap readonly wd-per-100">
											<input type="hidden" class="inp-comm" id="MANAGER" name="MANAGER" value="${repairResultDtl.MANAGER}"/>
											<input type="text" class="inp-comm" id="manager_name" name="manager_name" readonly="readonly" value="${repairResultDtl.MANAGER_NAME}"/>
										</div>
									</td>
								</tr>
								
								<tr>
									<th scope="row"><spring:message code='epms.attach' /></th><!-- 고장 첨부파일 -->
									<td colspan="3">
										<c:choose>
											<c:when test="${fn:length(attachList1)>0}">
											<div id="previewList" class="wd-per-100">
												<div class="bxType01 fileWrap previewFileLst">
													<div>
														<div class="fileList type02">
															<div id="detailFileList0" class="fileDownLst">
																<c:forEach var="item" items="${attachList1}" varStatus="idx">
																	<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																		<span class="MultiFile-export">${item.SEQ_DSP}</span> 
																		<span class="MultiFile-title" title="File selected: ${item.NAME}">
																		${item.FILE_NAME} (${item.FILE_SIZE_TXT})
																		</span>
																		<a href="#none" class="btnFileDwn icon download" data-attach="${item.ATTACH}">다운로드</a>
																	</div>		
																</c:forEach>
															</div>
														</div>
													</div>
												</div>
											</div>
	
											<div id="previewImg" class="wd-per-100">
												<div class="previewFile mgn-t-15">
													<h3 class="tit-cont preview" id="previewTitle">이미지 미리보기</h3>
													<ul style="margin-top:5px;">
														<c:forEach var="item" items="${attachList1}" varStatus="idx">
															<c:if test = "${item.FORMAT eq 'jpg' || item.FORMAT eq 'png' || item.FORMAT eq 'jpeg' || item.FORMAT eq 'JPG' || item.FORMAT eq 'PNG'}">
																<li>
																	<img src="/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}" title="클릭하시면 원본크기로 보실 수 있습니다." onclick="originFileView('/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}')" />
																	<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																		<span class="MultiFile-title" title="File selected: ${item.NAME}">
																		<p>${item.FILE_NAME} (${item.FILE_SIZE_TXT})</p>
																		</span>
																		<a href="#none" class="btnFileDwn icon download" data-attach="${item.ATTACH}">다운로드</a>
																	</div>
																</li>
															</c:if>
														</c:forEach>
													</ul>
												</div>
											</div>
											</c:when>
											<c:otherwise>
												<div class="f-l wd-per-100 mgn-t-5">
												※ 등록된 첨부파일이 없습니다.
												</div>
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="panel" id="panel1-2" style="display:none">
				<div class="panel-in">
					<div class="wd-per-100 overflow">
						<div class="tb-wrap">
							<table class="tb-st">
								<caption class="screen-out"><spring:message code='epms.work.perform.detail' /></caption><!-- 정비실적 상세 -->
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="*" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><spring:message code='epms.work.type' /></th><!-- 정비유형 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="hidden" class="inp-comm" id="REPAIR_TYPE" name="REPAIR_TYPE" value="${repairResultDtl.REPAIR_TYPE}"/>
												<input type="text" class="inp-comm" id="repair_type_name" name="repair_type_name" readonly="readonly" value="${repairResultDtl.REPAIR_TYPE_NAME}"/>
											</div>
										</td>
										<th scope="row"><spring:message code='epms.outsourcing' /></th><!-- 외주 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm" id="OUTSOURCING" name="OUTSOURCING" readonly="readonly" value="<c:out value="${repairResultDtl.OUTSOURCING}" escapeXml="true" />"/>
											</div>
										</td>
									</tr>
				 					<tr>
										<th scope="row"><spring:message code='epms.workder' /></th><!-- 정비원 -->
										<td colspan="3">
											<ul class="repairMemberList repairMember" id="repairMember">
												<c:forEach var="item" items="${repairRegMember}">
													<li style="overflow:hidden;">
														<div class="userInfo f-l">
															<div class="user_img">
																<img src="${item.MEMBER_FILEURL}" onerror="this.src='/images/com/web_v2/userIcon.png'" />
															</div>
															<div class="user_name">
																<div class="name">
																	<input type="hidden" class="inp-comm f-l" title="정비원ID" name="memberArr" value="${item.MEMBER}"	readonly="readonly">
																	${item.MEMBER_NAME} 
																	<c:if test="${item.USER_ID eq repairResultDtl.MANAGER}">
																		<p class="manager">담당자</p>
																	</c:if>
																</div>
																<div class="dep">
																	<span>
																		<input type="hidden" class="inp-comm f-l" title="부서ID" name="" value="${item.DIVISION}"	readonly="readonly">	
																		${item.DIVISION_NAME}
																	</span>
																	<span style="padding:0 5px;">/</span>
																	<span>
																		<input type="hidden" class="inp-comm f-l" title="파트" name="" value="${item.PART}"	readonly="readonly">
																		${item.PART_NAME}
																	</span>
																</div>
															</div>
														</div>
													</li>
													
													<%-- <li style="overflow:hidden; margin:5px 0;">
														<input type="hidden" class="inp-comm f-l" title="부서ID" name="" value="${item.DIVISION}"	readonly="readonly">	
														<div class="box-col wd-per-30 f-l">
															<div class="inp-wrap readonly wd-per-100">
																<input type="text" class="inp-comm" title="부서명" name="" value="${item.DIVISION_NAME}"	readonly="readonly">
															</div>
														</div>
														<input type="hidden" class="inp-comm f-l" title="파트" name="" value="${item.PART}"	readonly="readonly">	
														<div class="box-col wd-per-15 f-l">
															<div class="inp-wrap readonly mgn-l-5">
																<input type="text" class="inp-comm" title="파트명" name="" value="${item.PART_NAME}"	readonly="readonly">
															</div>
														</div>
														<input type="hidden" class="inp-comm f-l" title="정비원ID" name="memberArr" value="${item.MEMBER}"	readonly="readonly">
														<div class="box-col wd-per-15 f-l">
															<div class="inp-wrap readonly mgn-l-5">
																<input type="text" class="inp-comm" title="정비원" name="" value="${item.MEMBER_NAME}"	readonly="readonly">
															</div>
														</div>
													</li> --%>
												</c:forEach>
											</ul>
										</td>
									</tr>						
									<tr>
										<th scope="row"><spring:message code='epms.use.object' /></th><!-- 사용자재 -->
										<td colspan="3">
											<input type="hidden" class="inp-comm" id="MATERIAL_INOUT" name="MATERIAL_INOUT" value="${repairResultDtl.MATERIAL_INOUT}"/>
											<ul class="usedMaterial" id="usedMaterial">
												<c:forEach var="item" items="${usedMaterial}">
													<li style="overflow:hidden; margin:5px 0;">
														<input type="hidden" class="inp-comm" title="BOMID" name="equipmentBomUsedArr" value="${item.EQUIPMENT_BOM}"	readonly="readonly">
														<input type="hidden" class="inp-comm" title="자재ID" name="materialUsedArr" value="${item.MATERIAL}"	readonly="readonly">
														<div class="box-col wd-per-20 f-l">
															<div class="inp-wrap readonly wd-per-100">
																<input type="text" class="inp-comm" title="장치명" name="" value="${item.MATERIAL_UID}"	readonly="readonly">
															</div>
														</div>
														<div class="box-col wd-per-20 f-l mgn-l-5">
															<div class="inp-wrap readonly wd-per-100">
																<input type="text" class="inp-comm" title="자재명" name="" value="${item.MATERIAL_NAME}"	readonly="readonly">
															</div>
														</div>
														<input type="hidden" class="inp-comm" title="단위" name="" value="${item.UNIT}"	readonly="readonly">
														<div class="box-col wd-per-20 f-l mgn-l-5">
															<div class="inp-wrap readonly wd-per-100">
																<input type="text" class="inp-comm" title="단위명" name="" value="${item.UNIT_NAME}"	readonly="readonly">
															</div>
														</div>
														<div class="box-col wd-per-10 f-l  mgn-l-5">
															<div class="inp-wrap wd-per-100">
																<input type="text" class="inp-comm t-r" title="수량" name="qntyUsedArr"	value="${item.QNTY}"	readonly="readonly">
															</div>
														</div>
													</li>
												</c:forEach>
											</ul>
										</td>
									</tr>
									<tr>
										<th scope="row">
										    <spring:message code='epms.use.object' /> (<spring:message code='epms.unregist' />)
										</th><!-- 사용자재(미등록) -->
										<td colspan="3">
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm" id="MATERIAL_UNREG" name="MATERIAL_UNREG"  readonly="readonly" value="<c:out value="${repairResultDtl.MATERIAL_UNREG}" escapeXml="true" />"/>
											</div>
										</td>
									</tr>
									<tr>
										<th scope="row"><spring:message code='epms.type.occurr' /></th><!-- 고장유형 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<c:forEach var="item" items="${troubleList1}" >
													<c:if test="${repairResultDtl.TROUBLE_TYPE1 eq item.CODE}">
														<input type="text" class="inp-comm" id="TROUBLE_TYPE1" name="TROUBLE_TYPE1"  readonly="readonly" value="${item.NAME}" />
													</c:if>
												</c:forEach>
											</div>
										</td>
										<th scope="row"><spring:message code='epms.active' /></th><!-- 조치 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<c:forEach var="item" items="${troubleList2}" >
													<c:if test="${repairResultDtl.TROUBLE_TYPE2 eq item.CODE}">
														<input type="text" class="inp-comm" id="TROUBLE_TYPE1" name="TROUBLE_TYPE1"  readonly="readonly" value="${item.NAME}" />
													</c:if>
												</c:forEach>
											</div>
										</td>
									</tr>
									<tr>
										<th scope="row"><spring:message code='epms.active.content' /></th><!-- 작업내용 -->
										<td colspan="3">
											<div class="txt-wrap readonly">
												<div class="txtArea">
													<textarea cols="" rows="3" title="정비내용" id="REPAIR_DESCRIPTION" name="REPAIR_DESCRIPTION" readonly="readonly"><c:out value="${repairResultDtl.REPAIR_DESCRIPTION}" escapeXml="true" /></textarea>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<th scope="row"><spring:message code='epms.occurr.time.start' /></th><!-- 고장시작시간 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="hidden" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME1" name="TROUBLE_TIME1" title="고장시작일">
												<input type="text" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME1_DATE" name="TROUBLE_TIME1_DATE" title="고장시작일">
											</div>
											<div class="inp-wrap readonly wd-per-40 rel mgn-t-5 f-l">
												<input type="text" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME1_HOUR" name="TROUBLE_TIME1_HOUR" title="고장시작시간">
												<span style="position:absolute; top:5px; right:-15px;">시</span>
											</div>
											<div class="inp-wrap readonly wd-per-40 rel mgn-t-5 mgn-l-30 f-l">
												<input type="text" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME1_MINUTE" name="TROUBLE_TIME1_MINUTE" title="고장시작분">
												<span style="position:absolute; top:5px; right:-15px;">분</span>
											</div>
										</td>
										<th scope="row"><spring:message code='epms.occurr.time.end' /></th><!-- 고장종료시간 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="hidden" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME2" name="TROUBLE_TIME2" title="고장종료일">
												<input type="text" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME2_DATE" name="TROUBLE_TIME2_DATE" title="고장종료일">
											</div>
											<div class="inp-wrap readonly wd-per-40 rel mgn-t-5 f-l">
												<input type="text" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME2_HOUR" name="TROUBLE_TIME2_HOUR" title="고장종료시간">
												<span style="position:absolute; top:5px; right:-15px;">시</span>
											</div>
											<div class="inp-wrap readonly wd-per-40 rel mgn-t-5 mgn-l-30 f-l">
												<input type="text" class="inp-comm " readonly="readonly" id="popRepairResultForm_TROUBLE_TIME2_MINUTE" name="TROUBLE_TIME2_MINUTE" title="고장종료분">
												<span style="position:absolute; top:5px; right:-15px;">분</span>
											</div>
										</td>
									</tr>
									<tr>
										<th scope="row"><spring:message code='epms.attach' /></th><!-- 정비 첨부파일 -->
										<td colspan="3">
											<c:choose>
												<c:when test="${fn:length(attachList2)>0 }">
												<div id="previewList" class="wd-per-100">
													<div class="bxType01 fileWrap previewFileLst">
														<div>
															<div class="fileList type02">
																<div id="detailFileList0" class="fileDownLst">
																	<c:forEach var="item" items="${attachList2}" varStatus="idx">
																		<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																			<span class="MultiFile-export">${item.SEQ_DSP}</span> 
																			<span class="MultiFile-title" title="File selected: ${item.NAME}">
																			${item.FILE_NAME} (${item.FILE_SIZE} KB)
																			</span>
																			<a href="#none" class="btnFileDwn icon download" data-attach="${item.ATTACH}">다운로드</a>
																		</div>		
																	</c:forEach>
																</div>
															</div>
														</div>
													</div>
												</div>
									
												<div id="previewImg" class="wd-per-100">
													<div class="previewFile mgn-t-15">
														<h3 class="tit-cont preview" id="previewTitle">이미지 미리보기</h3>
														<ul style="margin-top:5px;">
															<c:forEach var="item" items="${attachList2}" varStatus="idx">
																<c:if test = "${item.FORMAT eq 'jpg' || item.FORMAT eq 'png' || item.FORMAT eq 'jpeg' || item.FORMAT eq 'JPG' || item.FORMAT eq 'PNG'}">
																	<li>
																		<img src="/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}" title="클릭하시면 원본크기로 보실 수 있습니다." onclick="originFileView('/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}')" />
																		<p>${item.FILE_NAME} <a href="#none" class="btnFileDwn icon download" data-attach="${item.ATTACH}">다운로드</a></p>
																		
																	</li>
																</c:if>
															</c:forEach>
														</ul>
													</div>
												</div>
												</c:when>
												<c:otherwise>
													<div class="f-l wd-per-100 mgn-t-5">
													※ 등록된 첨부파일이 없습니다.
													</div>
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- e : 상세 -->
