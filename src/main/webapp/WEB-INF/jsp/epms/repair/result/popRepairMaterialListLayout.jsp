<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: popRepairMaterialListLayout.jsp
	Description : 고장관리 > 정비실적등록/조회 > 정비실적 상세 > 자재선택팝업 레이어 팝업
	author		: 김영환
	since		: 2018.04.04
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.04	 김영환		최초 생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="popRepairMaterialList"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		Filtering="1"				Dragging="0"
			Editing="1"				Deleting="0"		Selecting="1"		SuppressCfg="0"
			CopyCols="0"			PasteFocused="3"	CustomScroll="1"
			MainCol="TYPE"
	/>

	<Header	id="Header"	Align="center"
	
		TYPE					= "<spring:message code='epms.type' />"    <%  // 유형"           %>
		OBJECT					= "<spring:message code='epms.code' />"    <%  // 코드"           %>
		OBJTXT					= "<spring:message code='epms.list' />"    <%  // 내역"           %>
	                               
		EQUIPMENT_BOM			= "<spring:message code='epms.system.bom.id' />"    <%  // 설비BOMID"      %>
		                           
		EQUIPMENT				= "<spring:message code='epms.system.id'            />"    <%  // 설비ID         %>
		EQUIPMENT_UID			= "<spring:message code='epms.system.uid'           />"    <%  // 설비코드         %>
		EQUIPMENT_NAME			= "<spring:message code='epms.system.name'          />"    <%  // 설비명          %>
		                           
		MATERIAL_GRP			= "<spring:message code='epms.object.group.id'   />"    <%  // 장치ID"         %>
		MATERIAL_GRP_UID		= "<spring:message code='epms.object.group.uid'  />"    <%  // 장치코드"         %>
		MATERIAL_GRP_NAME		= "<spring:message code='epms.object.group.name' />"    <%  // 장치명"          %>
		                           
		MATERIAL				= "<spring:message code='epms.object.id' />"    <%  // 자재ID"         %>
		LOCATION				= "<spring:message code='epms.location' />"    <%  // 위치"           %>
		MATERIAL_TYPE			= "<spring:message code='epms.object.type' />"    <%  // 자재유형"         %>
		MATERIAL_GRP1			= "<spring:message code='epms.object.group1' />"    <%  // 대그룹"          %>
		MATERIAL_GRP2			= "<spring:message code='epms.object.group2' />"    <%  // 중그룹"          %>
		MATERIAL_GRP3			= "<spring:message code='epms.object.group3' />"    <%  // 소그룹"          %>
		MATERIAL_UID			= "<spring:message code='epms.object.uid' />"    <%  // 자재코드"         %>
		MATERIAL_NAME			= "<spring:message code='epms.object.name' />"    <%  // 자재명"          %>
		FUNCTION				= "<spring:message code='epms.function' />"    <%  // 기능"           %>
		MODEL					= "<spring:message code='epms.model' />"    <%  // 모델명"          %>
		SPEC					= "<spring:message code='epms.spec' />"    <%  // 규격"           %>
		STOCK_CONDITION			= "<spring:message code='epms.stock.status' />"    <%  // 재고상태"         %>
		UNIT					= "<spring:message code='epms.unit' />"    <%  // 단위"           %>
		UNIT_NAME				= "<spring:message code='epms.unit.name' />"    <%  // 단위명"          %>
		STOCK					= "<spring:message code='epms.stock' />"    <%  // 재고"           %>
		STOCK_OPTIMAL			= "<spring:message code='epms.safety.stock' />"    <%  // 안전재고"         %>
		ATTACH_GRP_NO			= "<spring:message code='epms.attachfile.code' />"    <%  // 첨부파일 코드"      %>
		file					= "<spring:message code='epms.attachfile' />"    <%  // 이미지"          %>
		HISTORY_IN				= "<spring:message code='epms.inout' />"    <%  // 입고"           %>
		HISTORY_OUT				= "<spring:message code='epms.output' />"    <%  // 출고"           %>
		flag					= "<spring:message code='epms.del.yn' />"    <%  // 삭제구분"         %>
	/>
	
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<Cols>
		<C Name="TYPE"						Type="Text" Align="left" Visible="0" Width="150" CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="OBJECT"					Type="Text" Align="left" Visible="1" Width="120" CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="OBJTXT"					Type="Text"	Align="left" Visible="1" Width="300" CanEdit="0" CanExport="1" CanHide="1"/>
	
		<C Name="EQUIPMENT_BOM"				Type="Text"		Align="left"		Visible="0"		Width="100"	CanEdit="0"/>
		<C Name="EQUIPMENT"					Type="Text"		Align="left"		Visible="0"		Width="100"	CanEdit="0"/>
		<C Name="EQUIPMENT_UID"				Type="Text"		Align="left"		Visible="0"		Width="100"	CanEdit="0"/>
		<C Name="EQUIPMENT_NAME"			Type="Text"		Align="left"		Visible="0"		Width="150"	CanEdit="0"/>
		<C Name="MATERIAL_GRP"				Type="Text"		Align="left"		Visible="0"		Width="100"	CanEdit="0"/>
		<C Name="MATERIAL_GRP_UID"			Type="Text"		Align="left"		Visible="0"		Width="100"	CanEdit="0"/>
		<C Name="MATERIAL_GRP_NAME"			Type="Text"		Align="left"		Visible="0"		Width="150"	CanEdit="0"/>
		<C Name="LOCATION"					Type="Enum"		Align="left"		Visible="0"		Width="100"	CanEdit="0"	<tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>
		<C Name="MATERIAL_TYPE"				Type="Enum"		Align="left"		Visible="0"		Width="70"	CanEdit="0"	<tag:enum codeGrp="MATERIAL_TYPE"/>/> 
		<C Name="MATERIAL_GRP1"				Type="Enum"		Align="left"		Visible="0"		Width="100"	CanEdit="0"/> 
		<C Name="MATERIAL"					Type="Text"		Align="left"		Visible="0"		Width="100"	CanEdit="0"/>
		<C Name="MATERIAL_UID"				Type="Text"		Align="left"		Visible="0"		Width="100"	CanEdit="0"/>
		<C Name="MATERIAL_NAME"				Type="Text"		Align="left"		Visible="0"		Width="150"	CanEdit="0"/>
		<C Name="FUNCTION"					Type="Text"		Align="left"		Visible="0"		Width="400"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="MODEL"						Type="Text"		Align="left"		Visible="0"		Width="150"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="SPEC"						Type="Text"		Align="left"		Visible="0"		Width="200"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
	</Cols>                                                                              
	                                                                                     
	<RightCols>                                                                          
		<C Name="STOCK_CONDITION"			Type="Enum"		Align="left"		Visible="0"		Width="70"	CanEdit="0" Enum='|적정|미만'	EnumKeys='|1|2' CanExport="0"/>
		<C Name="STOCK"						Type="Int"		Align="center"		Visible="0"		Width="60"	CanEdit="0"/>
		<C Name="STOCK_OPTIMAL"				Type="Int"		Align="center"		Visible="0"		Width="60"	CanEdit="0"/>
		<C Name="UNIT"						Type="Enum"		Align="left"		Visible="0"		Width="60"	CanEdit="0"	<tag:enum codeGrp="UNIT"/>/> 
		<C Name="UNIT_NAME"					Type="Text"		Align="left"		Visible="0"		Width="80"	CanEdit="0"/>
		<C Name="ATTACH_GRP_NO"				Type="Text"		Align="center"		Visible="0"		Width="80"	CanEdit="0"/>
		<C Name="file"						Type="Icon"		Align="center"		Visible="0"		Width="70"	CanEdit="0"	CanExport="0"/>
		<C Name="HISTORY_IN"				Type="Icon"		Align="center"		Visible="0"		Width="70"	CanEdit="0"	CanExport="0"/>
		<C Name="HISTORY_OUT"				Type="Icon"		Align="center"		Visible="0"		Width="70"	CanEdit="0"	CanExport="0"/>
		<C Name="flag"						Type="Text"		Align="center"		Visible="0"		Width="80"	CanEdit="0"/>
	</RightCols>
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,자재추가"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        "
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				자재추가Type="Html" 자재추가="&lt;a href='#none' title='자재추가' class=&quot;btn evn-st01&quot;
								  onclick='fnAddMaterial(&quot;${gridId}&quot;)'><spring:message code='epms.object' /><spring:message code='epms.add' />&lt;/a>"
				추가Type="Html" 추가="&lt;a href='#none' title='추가' class=&quot;defaultButton01 icon add&quot;
								onclick='addRowGrid(&quot;${gridId}&quot;)'>추가&lt;/a>"
				저장Type="Html" 저장="&lt;a href='#none' title='저장' class=&quot;defaultButton01 icon submit&quot;
								onclick='fnModify(&quot;${gridId}&quot;)'>저장&lt;/a>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton01 icon refresh&quot; 
								onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				PrintType="Html" Print="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
								onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			ExportType="Html" Export="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
								onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
	
</Grid>