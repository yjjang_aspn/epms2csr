<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I  
			NAME				= "${item.NAME}"
			USER_ID				= "${item.USER_ID}"
			CHECK_INOUT			= "${item.CHECK_INOUT}"
			<c:if test="${item.CHECK_INOUT == '01'}">
				CHECK_INOUTClass="gridStatus3"
			</c:if>
			LOCATION			= "${item.LOCATION}"
			LOCATION_NAME		= "${item.LOCATION_NAME}"
			PART				= "${item.PART}"
			PART_NAME			= "${item.PART_NAME}"
			DIVISION			= "${item.DIVISION}"
			DIVISION_NAME		= "${item.DIVISION_NAME}"
			MEMBER_FILEURL		= "${item.MEMBER_FILEURL}"
		/>
	</c:forEach>
	</B>
</Body>
</Grid>
