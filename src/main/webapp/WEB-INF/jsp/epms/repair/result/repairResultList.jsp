<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: repairResultList.jsp
	Description : 고장관리 > 정비실적등록/조회 메인화면
	author		: 김영환
	since		: 2018.04.04
 
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.04.04	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<style>
	.btnRemove{display:inline-block; height:28px; line-height:28px; padding:0 10px; background:#6eb4e5; color:#ffffff;}
</style>
<script type="text/javascript">

<%-- 전역변수 --%>
var msg = "";				<%-- 결과 msg --%>
var focusedRow = null;		<%-- Focus Row : [정비실적 현황] --%>

$(document).ready(function(){
	
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function (e) {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>	
	});	
	
	<%-- datePic 설정 --%>
	$('.datePic').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	});
	
	<%-- 검색 시작일이 종료일보다 이후인지 체크 (이후라면, 종료일을 시작일로 변경) --%>
	$('#startDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#endDt').val($('#startDt').val());
		}
	});
	
	<%-- 검색 종료일이 시작일보다 이전인지 체크 (이전이라면, 시작일을 종료일로 변경) --%>
	$('#endDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#startDt').val($('#endDt').val());
		}
	});
	
	<%-- 정비실적현황 '검색'버튼 클릭 --%>
	$('#srcBtn').on('click', function(){
		Grids.RepairResultList.Source.Data.Url = "/epms/repair/result/repairResultListData.do?srcType=" + $('#srcType').val()
											   + "&startDt=" + $('#startDt').val()
											   + "&endDt=" + $('#endDt').val();
		Grids.RepairResultList.ReloadBody();
	});
	
	<%-- 모달 팝업 focus --%>
// 	$('.modalFocus').on('shown.bs.modal', function (e) {
<%-- 		Grids.Focused = null;		그리드 focus 해제 --%>
<%-- 		Grids.Active = null;		그리드 action 해제(스크롤 해제) --%>
// 	});
	
});

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "RepairResultList") { <%-- [정비실적 현황] 클릭시 --%>
		<%-- 포커스 시작 --%>
		if(focusedRow != null){
			grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		focusedRow = row;
		
	}
	else if(grid.id == "PopRepairAnalysisList") { <%-- [고장원인분석 불러오기] 클릭시 --%>
	
		REPAIR_RESULT = row.REPAIR_RESULT;
		REPAIR_ANALYSIS = row.REPAIR_ANALYSIS;
		init.getData(row.SAVE_YN, function(result){
			
			init.setData.headData(result.analysisInfo, $("#editForm"));
			init.setData.memberData(result.repairRegMember, $("#editForm"));
			init.setData.itemData(result.analysisItem, $("#editForm"), "edit");
			
		});
		
		fnModalToggle('popRepairAnalysisList');
	}
}

<%-- 그리드 더블클릭 이벤트 --%>
Grids.OnDblClick = function(grid, row, col){
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	<%-- 정비원 선택 --%>
	if(grid.id == "WorkerStatList") {
		
		var i = 1;
		var division = new Array();
		var division_name = new Array();
		var part = new Array();
		var part_name = new Array();
		var member = new Array();
		var member_name = new Array();
		var member_fileurl = new Array();
		if(!(row.USER_ID=="" || row.USER_ID==null)){
			division[0] = row.DIVISION;
			division_name[0] = row.DIVISION_NAME;
			part[0] = row.PART;
			part_name[0] = row.PART_NAME;
			member[0] = row.USER_ID;
			member_name[0] = row.NAME;
			member_fileurl[0] = row.MEMBER_FILEURL;
		}
		addMember(division, division_name, part, part_name, member, member_name, member_fileurl, i);
		fnModalToggle('popWorkerList');
	}
	
	<%-- 자재 선택 --%>
	if(grid.id == "popRepairMaterialList") {
		
		var i = 1;
		var equipment_bom = new Array();
		var material_grp = new Array();
		var material_grp_uid = new Array();
		var material_grp_name = new Array();
		var material = new Array();
		var material_uid = new Array();
		var material_name = new Array();
		var unit = new Array();
		var unit_name = new Array();
		
		if(!(row.MATERIAL=="" || row.MATERIAL==null)){
			equipment_bom[0] = row.EQUIPMENT_BOM;
			material_grp[0] = row.MATERIAL_GRP;
			material_grp_uid[0] = row.MATERIAL_GRP_UID;
			material_grp_name[0] = row.MATERIAL_GRP_NAME;
			material[0] = row.MATERIAL;
			material_uid[0] = row.MATERIAL_UID;
			material_name[0] = row.MATERIAL_NAME;
			unit[0] = row.UNIT;
			unit_name[0] = row.UNIT_NAME;
		}
		addMaterial(equipment_bom, material_grp, material_grp_uid, material_grp_name, material, material_uid, material_name, unit, unit_name, i);
		fnModalToggle('popRepairMaterialList');
	}
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- 그리드와 Param 값을 동시에 사용 --%>
function fnOpenLayerWithGrid(url, name){
	$.ajax({
		type: 'POST',
		url: url,
		dataType: 'json',
		processData: false, 
		contentType:false,
		success: function (json) {
			$("#MATERIAL").val(json.MATERIAL);
			$("#"+name+"_startDt").val(isEmpty(json.startDt) == true ? '' : json.startDt);
			$("#"+name+"_endDt").val(  isEmpty(json.endDt)   == true ? '' : json.endDt);
			$("#"+name+"_MATERIAL_TYPE").text(isEmpty(json.item.MATERIAL_TYPE_NAME) == true ? '[조회불가]' : "["+json.item.MATERIAL_TYPE_NAME+"]");
			$("#"+name+"_MATERIAL_GRP1").text(isEmpty(json.item.MATERIAL_GRP1_NAME) == true ? '[조회불가]' : "["+json.item.MATERIAL_GRP1_NAME+"]");
			$("#"+name+"_MATERIAL_UID").text( isEmpty(json.item.MATERIAL_UID)       == true ? '[조회불가]' : "["+json.item.MATERIAL_UID+"]");
			$("#"+name+"_MATERIAL_NAME").text(isEmpty(json.item.MATERIAL_NAME)      == true ? '[조회불가]' : "["+json.item.MATERIAL_NAME+"]");
			
			if(name == "popMaterialInputView") {
				Grids.MaterialInList.Source.Data.Url = "/epms/material/input/materialInputListData.do?MATERIAL=" + $('#MATERIAL').val()
				 + "&startDt=" + $('#popMaterialInputView_startDt').val()
				 + "&endDt=" + $('#popMaterialInputView_endDt').val();
				
				Grids.MaterialInList.ReloadBody();
			} else if(name == "popMaterialOutputView") {
				Grids.MaterialOutList.Source.Data.Url = "/epms/material/output/materialOutputListData.do?MATERIAL=" + $('#MATERIAL').val()
				 + "&startDt=" + $('#popMaterialOutputView_startDt').val()
				 + "&endDt=" + $('#popMaterialOutputView_endDt').val();
				 
				Grids.MaterialOutList.ReloadBody();
			}
			
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
		}
	});
	
	fnModalToggle(name);
	
}

function fnReload(msg){
	alert(msg);
	$('#popRepairResultRegForm').empty();
	Grids.RepairResultList.Source.Data.Url = "/epms/repair/result/repairResultListData.do?srcType=" + $('#srcType').val()
										   + "&startDt=" + $('#startDt').val()
										   + "&endDt=" + $('#endDt').val();
	Grids.RepairResultList.ReloadBody();
}

function fnReload_afterCancel(){
	
	alert("<spring:message code='epms.repair.complete' />가 취소되었습니다.");  // 정비완료
	
	fnModalToggle('popRepairResultForm');
	
	$('#popRepairResultForm').empty();
	
	Grids.RepairResultList.ReloadBody();
}


<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source,data, func){
	if(grid.id == "RepairResultList"){
		if(data != ""){
			callLoadingBar();
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "RepairResultList"){
					setTimeout($.unblockUI, 100);
				}
			}
		}
	}
}

<%-- 수량 null 체크 --%>
function check_Null(obj){
	
	if(obj.value == ""){
		obj.value = "0";
	}

}

<%-- 수량 숫자 유효성 검사 --%> 
function check_isNum_onkeydown(){
	
	$(this).val( $(this).val().replace(/[^-0-9.,]/gi,"") );
	$(this).val(number_format($(this).val()));
}

</script>
</head>
<body>
<div id="contents">
	<input type="hidden" id="MATERIAL" name="MATERIAL">				<%-- 자재 ID --%>
	
	<div class="fl-box panel-wrap04" style="width:100%;">
		<h5 class="panel-tit" id="repairRequestApprListTitle"><spring:message code='epms.perform.status' /></h5><!-- 정비실적 현황 -->
		<div class="inq-area-inner type06 ab">
			<div class="wrap-inq">
				<div class="inq-clmn">
					<h4 class="tit-inq">구분</h4>
					<div class="sel-wrap type02">
						<select title="구분" id="srcType" name="srcType">
							<option value="1" selected="selected"><spring:message code='epms.plan.date' /></option><!-- 정비계획일 -->
							<option value="2">실적등록일</option>
						</select>
					</div>
				</div>
				<div class="inq-clmn">
					<h4 class="tit-inq">검색일</h4>
					<div class="prd-inp-wrap">
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic thisMonth inpCal" readonly="readonly" id="startDt" title="검색시작일" value="${startDt}" >
							</span>
						</span>
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic inpCal" readonly="readonly" id="endDt" title="검색종료일" value="${endDt}">
							</span>
						</span>		
					</div>
				</div>
			</div>
			<a href="#none" id="srcBtn" class="btn comm st01">검색</a>
		</div>
		<div class="panel-body mgn-t-20">
			<div id="RepairResultList">
				<bdo	Debug="Error"
						Data_Url="/epms/repair/result/repairResultListData.do?srcType=1&startDt=${startDt}&endDt=${endDt}"
						Layout_Url="/epms/repair/result/repairResultListLayout.do?flag=repairResult"
								
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=정비현황 목록.xls&dataName=data"
				>
				</bdo>
			</div>
		</div>
	</div>
</div> <%-- contents 끝 --%>

<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<%-- 첨부파일 조회/수정 --%>
<div class="modal fade modalFocus" id="popFileManageForm"   data-backdrop="static" data-keyboard="false"></div>

<%-- 입고조회 --%>
<%@ include file="/WEB-INF/jsp/epms/material/master/popMaterialInputView.jsp"%>	

<%-- 출고조회 --%>
<%@ include file="/WEB-INF/jsp/epms/material/master/popMaterialOutputView.jsp"%>		

<%-- 정비실적등록 --%>
<div class="modal fade modalFocus" id="popRepairResultRegForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 정비실적조회 --%>
<div class="modal fade modalFocus" id="popRepairResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 원인분석 조회 --%>
<div class="modal fade modalFocus" id="popRepairAnalysisForm" data-backdrop="static" data-keyboard="false"></div>	

<%-- 타파트 요청 --%>
<div class="modal fade modalFocus" id="popRequestPartForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 정비원 검색 --%>
<div class="modal fade modalFocus" id="popWorkerList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-35">		
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">
			   	    <spring:message code='epms.worker' /> <spring:message code='epms.search' />
			   	</h4><!-- 정비원 검색 -->
			</div>
	 	<div class="modal-body" >
           	<div class="modal-bodyIn">
           		<div class="modal-section">
					<div class="fl-box wd-per-100">
						<div id="snsList">
							<bdo Debug="Error"
								 Data_Url=""
								 Layout_Url="/epms/repair/result/popWorkerListLayout.do"
							 >
							</bdo>
						</div>
					</div>
				</div> <%-- modal-section : e --%>
			</div> <%-- modal-bodyIn : e --%>
		</div> <%-- modal-body : e --%>
	</div> <%-- modal-content : e --%>
</div> <%-- modal : e --%>
</div>

<%-- 부품명 검색 --%>
<div class="modal fade modalFocus" id="popRepairMaterialList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-50">
		<div class="modal-content" style="width: 469px;">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">
                    <spring:message code='epms.object.name' /> <spring:message code='epms.search' />
                </h4><!-- 부품명 검색 -->
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	           		<div class="modal-section">
						<div class="fl-box wd-per-100">
							<bdo	Debug="Error"
					 				Data_Url=""
									Layout_Url="/epms/repair/result/popRepairMaterialListLayout.do" 
							>
							</bdo>
						</div>
					</div> <%-- modal-section : e --%>
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>						

<%-- 고장원인분석 불러오기 --%>
<div class="modal fade modalFocus" id="popRepairAnalysisList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-55">		
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">원인분석 목록</h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	           		<div class="modal-section">
						<div class="fl-box panel-wrap-modal wd-per-100">
							<div class="panel-body" id="PopRepairAnalysisList">
								<bdo Debug="Error"
									 Data_Url=""
									 Layout_Url="/epms/repair/analysis/popRepairAnalysisListLayout.do"
								 >
								</bdo>
							</div>
						</div>
					</div> <%-- modal-section : e --%>
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>
