<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"           %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: popRepairRequestPartForm.jsp
	Description : 고장관리 > 정비실적등록/조회 > 타파트 요청
	author		: 김영환
	since		: 2018.04.04
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.04	 김영환		최초 생성

--%>

<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">

var submitBool = true;

$(document).ready(function() {
	
	<%-- '타파트지원'버튼 클릭 --%>
	$('#registBtn').on('click', function(){
		
		if (submitBool) {
			if(confirm("타파트지원 요청하시겠습니까?")){
				fnRequestAjax();
			}
		} else {
			alert("등록중입니다. 잠시만 기다려 주세요.");
			return false;
		}
		
	});
	
	<%-- '취소'버튼 클릭 --%>
	$('#cancelBtn').on('click', function(){
		fnModalToggle('popRequestPartForm');
	});
});

<%-- 타파트요청 ajax --%>
function fnRequestAjax(){
	submitBool = false;
	
	if (!isNull_J($('#LOCATION'), " <spring:message code='epms.location' /> 정보가 입력되어 있지 않습니다. " )) {  // 위치(PLANT)
		submitBool = true;
		return false;
	}
	if (!isNull_J($('#EQUIPMENT'), " <spring:message code='epms.system.id' /> 정보가 입력되어 있지 않습니다. " )) {  // 설비ID
		submitBool = true;
		return false;
	}
	if (!isNull_J($('#PART'), " 처리파트를 선택해주세요. " )) {  // 처리파트
		submitBool = true;
		return false;
	}
	if (!isNull_J($('#REQUEST_DESCRIPTION'), " <spring:message code='epms.request.content' />을 입력해주세요. " )) {  // 고장내용
		submitBool = true;
		return false;
	}
	
	var form = new FormData(document.getElementById('requestForm'));
	
	$.ajax({
		type : 'POST',
		url : '/epms/repair/result/repairRequestAjax.do',
		dataType : 'json',
		data : form,
		processData : false,
		contentType : false,
		success : function(json) {
			if(json.REPAIR_REQUEST != '' || json.REPAIR_REQUEST != null){
				alert("타파트요청이 정상적으로 등록되었습니다.")
				submitBool = true;
				fnModalToggle('popRequestPartForm');	
			} else {
				alert("처리 도중 오류가 발생하였습니다.\n시스템 관리자에게 문의 바랍니다.");
				submitBool = true;
				return;
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
			submitBool = true;
		}
	});
	
}
	
</script>
<div class="modal-dialog root" style="width:450px;"> 
	 <div class="modal-content" style="height:300px;">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title">타파트 요청</h4>
		</div>
	 	<div class="modal-body" >
           	<div class="modal-bodyIn">
				 
				 <div class="modal-section">
					<form id="requestForm" name="requestForm"  method="post" enctype="multipart/form-data">	
						
						<input type="hidden" class="inp-comm" name="flag"/>
						<input type="hidden" class="inp-comm" name="LOCATION" value="${param.LOCATION}"/>
						<input type="hidden" class="inp-comm" name="REMARKS" value="${param.REMARKS}"/>
						<input type="hidden" class="inp-comm" name="EQUIPMENT" value="${param.EQUIPMENT}"/>
						<input type="hidden" class="inp-comm" name="REPAIR_REQUEST_ORG" value="${param.REPAIR_REQUEST_ORG}"/>
						<input type="hidden" class="inp-comm" name="REQUEST_TYPE" value="03"/>
						
						<table class="tb-st">
							<caption class="screen-out">파트이관</caption>
								<colgroup>
									<col width="30%" />
									<col width="*" />
								</colgroup>
								<tbody>
									<tr>
										<th>긴급도</th>
										<td>
											<div class="tb-row">
												<span class="rdo-st">
													<input type="radio" name="URGENCY" id="URGENCY_01" value="01" <c:if test="${param.URGENCY eq '01'}"> checked </c:if>>
													<label for="URGENCY_01" class="label">일반</label>
												</span>
												<span class="rdo-st mgn-l-50">
													<input type="radio" name="URGENCY" id="URGENCY_02" value="02" <c:if test="${param.URGENCY eq '02'}"> checked </c:if>>
													<label for="URGENCY_02">긴급</label>
												</span>
											</div>
	<!-- 										<div class="sel-wrap"> -->
	<!-- 											<select title="긴급도" id="URGENCY" name="URGENCY" > -->
	<!-- 												<option value="01">일반</option> -->
	<!-- 												<option value="02">긴급</option> -->
	<!-- 											</select>											 -->
	<!-- 										</div> -->
										</td>
									</tr>
									<tr>
										<th scope="row">파트</th>
										<td>
											<div class="sel-wrap">
												<select title="파트" id="PART" name="PART">
													<option value="">선택하세요.</option>
													<c:forEach var="item" items="${partList}" >
														<option value="${item.COMCD}" <c:if test="${param.PART eq item.COMCD_NM}">selected</c:if>>${item.COMCD_NM}</option>
													</c:forEach>
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<th scope="row">요청내용</th>
										<td>
											<div class="txt-wrap">
												<div class="txtArea">
													<textarea cols="" rows="3" title="요청내용" name="REQUEST_DESCRIPTION"></textarea>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
						</table>
					</form>
				</div> <%-- modal-section : e --%>
			</div> <%-- modal-bodyIn : e --%>
		</div> <%-- modal-body : e --%>
		<div class="modal-footer hasFooter">
			<a href="#none" id="cancelBtn" class="btn comm st01">취소</a>
			<a href="#none" id="registBtn" class="btn comm st02">타파트지원</a>
		</div>
	</div> <%-- modal-content : e --%>
</div> <%-- modal : e --%>
					
					
