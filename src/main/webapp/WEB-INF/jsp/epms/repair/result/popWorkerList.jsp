<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<script type="text/javascript">

	Grids.OnDblClick = function(grid, row, col){
		if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
			return;
		}
		//정비원 선택
		if(grid.id == "WorkerStatList") {
			
			var i = 1;
			var division = new Array();
			var division_name = new Array();
			var part = new Array();
			var part_name = new Array();
			var member = new Array();
			var member_name = new Array();
			
			if(!(row.MEMBER=="" || row.MEMBER==null)){
				division[0] = row.DIVISION;
				division_name[0] = row.DIVISION_NAME;
				part[0] = row.PART;
				part_name[0] = row.PART_NAME;
				member[0] = row.MEMBER;
				member_name[0] = row.NAME;
			}
			window.opener.addMember(division, division_name, part, part_name, member, member_name, i);
			window.close();
		}
	}

</script>
<!-- 결재라인 관리 화면 모달 : s -->
<!-- Layer PopUp Start -->	
<div class="modal-dialog root wd-per-30"> <!-- 원하는 width 값 입력 -->				
	 <div class="modal-content">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title">
                    <spring:message code='epms.worker' /> <spring:message code='epms.search' />
                </h4><!-- 정비원 검색 -->
		</div>
	 	<div class="modal-body" >
           	<div class="modal-bodyIn">
           		<div class="modal-section">
           			<!-- 페이지 내용 : s -->
<%-- 				<input type="hidden" id="userId" name="userId" value="${sessionScope.getSsuserId}"/> --%>
					<div class="fl-box wd-per-100">
						<!-- 트리그리드 : s -->
						<div id="snsList">
							<bdo Debug="Error"
								 Data_Url="/epms/repair/result/popWorkerListData.do?LOCATION=${param.LOCATION}&PART=${param.PART}&REMARKS=${param.REMARKS}"
								 Layout_Url="/epms/repair/result/popWorkerListLayout.do"
							 >
							</bdo>
						</div>
						<!-- 트리그리드 : e -->
					</div>
					<!-- 페이지 내용 : e -->
				</div> <%-- modal-section : e --%>
			</div> <%-- modal-bodyIn : e --%>
		</div> <%-- modal-body : e --%>
	</div> <%-- modal-content : e --%>
</div> <%-- modal : e --%>
				

