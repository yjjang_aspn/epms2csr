<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script src="/sys/js/common/jquery-1.10.2.min.js" type="text/javascript" ></script>
<script src="/sys/js/common/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="/sys/js/web/hello_web_common.js" type="text/javascript"></script>
<script src="/sys/js/web/menu_common.js" type="text/javascript"></script>

<!-- 설비마스터 : 설비BOM관리 (자재매핑버튼)> 자재선택 팝업화면 -->
<script type="text/javascript">

var chkRow = "";		//선택 그리드
var msg = "";

$(document).ready(function() {
	
	//검색버튼
	$('#srcBtn').on('click', function(){
		Grids.PopRepairMaterialList.Source.Data.Url = "/epms/material/master/materialMasterListData.do?LOCATION=" + $('#LOCATION').val()
											+ "&CD_MATERIAL_GRP1=" + $('#CD_MATERIAL_GRP1').val();
		Grids.PopRepairMaterialList.ReloadBody();
	});
	
});

//그리드 더블클릭 이벤트
Grids.OnDblClick = function(grid, row, col){
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	//자재 선택
	if(grid.id == "PopRepairMaterialList") {
		
		var i = 1;
		var equipment_bom = new Array();
		var material_grp = new Array();
		var material_grp_uid = new Array();
		var material_grp_name = new Array();
		var material = new Array();
		var material_uid = new Array();
		var material_name = new Array();
		var unit = new Array();
		var unit_name = new Array();
		
		if(!(row.MATERIAL=="" || row.MATERIAL==null)){
			equipment_bom[0] = row.EQUIPMENT_BOM;
			material_grp[0] = row.MATERIAL_GRP;
			material_grp_uid[0] = row.MATERIAL_GRP_UID;
			material_grp_name[0] = row.MATERIAL_GRP_NAME;
			material[0] = row.MATERIAL;
			material_uid[0] = row.MATERIAL_UID;
			material_name[0] = row.MATERIAL_NAME;
			unit[0] = row.UNIT;
			unit_name[0] = row.UNIT_NAME;
		}
		
		addMaterial(equipment_bom, material_grp, material_grp_uid, material_grp_name, material, material_uid, material_name, unit, unit_name, i);
		fnModalToggle('popRepairMaterialList');
	}
}

</script>
</head>
<body>
<div id="contents">
	<input type="hidden" id="EQUIPMENT" name="EQUIPMENT" value="${param.EQUIPMENT}"/>
	
	<div class="fl-box panel-wrap" style="height:100%;"><!-- 원하는 비율로 직접 지정하여 사용 -->
		<!-- <h5 class="panel-tit">자재 목록</h5> -->
		<div class="panel-body">
			<!-- 트리그리드 : s -->
			<bdo	Debug="Error"
	 				Data_Url="/epms/bom/equipmentBom/equipmentBomListData.do?EQUIPMENT=${param.EQUIPMENT}"
					Layout_Url="/epms/repair/result/popRepairMaterialListLayout.do"
				>
			</bdo>
			<!-- 트리그리드 : e -->
		</div>
	</div>
</div> <%-- contents 끝 --%>
</body>
</html>