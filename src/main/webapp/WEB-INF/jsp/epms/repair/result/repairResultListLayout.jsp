<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: repairResultListLayout.jsp
	Description : 정비실적등록/조회 그리드 레이아웃
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="RepairResultList"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		Filtering="1"				Dragging="0"
			Editing="1"				Deleting="0"		Selecting="0"		SuppressCfg="0"
			Paging="1"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="0"			PasteFocused="3"	CustomScroll="1"
	/>

	<Header	id="Header"	Align="center"
		REPAIR_STATUS			= "<spring:message code='epms.repair.status'    />"    <%  // 정비상태"       %>
		REPAIR_TYPE				= "<spring:message code='epms.repair.type'      />"    <%  // 정비유형"       %>
		REPAIR_RESULT			= "<spring:message code='epms.work.id' />"    <%  // 정비ID"       %>
		LOCATION				= "<spring:message code='epms.location'      />"    <%  // 위치"         %>
		CATEGORY				= "<spring:message code='epms.category'      />"    <%  // 기능위치"       %>
		CATEGORY_NAME			= "<spring:message code='epms.category.name' />"    <%  // 라인"         %>
		EQUIPMENT				= "<spring:message code='epms.system.id'        />"    <%  // 설비ID"       %>
		EQUIPMENT_UID			= "<spring:message code='epms.system.uid'       />"    <%  // 설비코드"       %>
		EQUIPMENT_NAME			= "<spring:message code='epms.system.name'      />"    <%  // 설비명"        %>
		GRADE					= "<spring:message code='epms.system.grade'     />"    <%  // 설비등급"       %>
		MODEL					= "<spring:message code='epms.model'         />"    <%  // 모델명"        %>
		MANUFACTURER			= "<spring:message code='epms.system.bom'    />"    <%  // 제조사"        %>
		                           
		REPAIR_REQUEST			= "<spring:message code='epms.work.request.id'      />"    <%  // 정비요청ID"     %>
		URGENCY					= "<spring:message code='epms.repair.urgency'         />"    <%  // 긴급도"        %>
		PART					= "<spring:message code='epms.repair.part'            />"    <%  // 파트"         %>
		REQUEST_TYPE			= "<spring:message code='epms.request.type'           />"    <%  // 요청유형"       %>
		REQUEST_TYPE2			= "<spring:message code='epms.request.type'           />"    <%  // 구분"         %>
		REQUEST_DESCRIPTION		= "<spring:message code='epms.request.content'        />"    <%  // 고장내용"       %>
		ATTACH_GRP_NO1			= "<spring:message code='epms.attachfile.code'           />"    <%  // 첨부파일 코드"    %>
		REQUEST_DIV				= "<spring:message code='epms.repair.request.part.id' />"    <%  // 요청부서ID"     %>
		REQUEST_DIV_NAME		= "<spring:message code='epms.repair.request.part' />"    <%  // 요청부서"       %>
		REQUEST_MEMBER			= "<spring:message code='epms.repair.request.id' />"    <%  // 요청자ID"      %>
		REQUEST_MEMBER_NAME		= "<spring:message code='epms.repair.request' />"    <%  // 요청자"        %>
		DATE_REQUEST			= "<spring:message code='epms.repair.request.date' />"    <%  // 요청일"        %>
		APPR_MEMBER_NAME		= "<spring:message code='epms.appr.member' />"    <%  // 결재자"        %>
		DATE_APPR				= "<spring:message code='epms.appr.date' />"    <%  // 결재일"        %>
		                           
		DATE_ASSIGN				= "<spring:message code='epms.assign.date' />"    <%  // 배정일"        %>
		DATE_PLAN				= "<spring:message code='epms.plan.date' />"    <%  // 정비계획일"      %>
		DATE_REPAIR				= "<spring:message code='epms.repair.date' />"    <%  // 실적등록일"      %>
		OUTSOURCING				= "<spring:message code='epms.outsourcing' />"    <%  // 외주"         %>
		REPAIR_MEMBER			= "<spring:message code='epms.worker' />"    <%  // 정비원"        %>
		MATERIAL_USED			= "<spring:message code='epms.use' /><spring:message code='epms.object' />"    <%  // 사용자재"       %>
		MATERIAL_UNREG			= "<spring:message code='epms.use' /><spring:message code='epms.object' />(<spring:message code='epms.unregist' />)"    <%  // 사용자재(미등록)"  %>
		TROUBLE_TYPE1			= "<spring:message code='epms.type.occurr'  />"    <%  // 고장유형"       %>
		TROUBLE_TYPE2			= "<spring:message code='epms.type.action'  />"    <%  // 조치"         %>
		REPAIR_DESCRIPTION		= "<spring:message code='epms.work.content'          />"    <%  // 작업내용"       %>
		MANAGER					= "<spring:message code='epms.manager.member.id'     />"    <%  // 담당자ID"      %>
		MANAGER_NAME			= "<spring:message code='epms.manager.member'        />"    <%  // 담당자"        %>
		TROUBLE_TIME1			= "<spring:message code='epms.occurr.time.start'     />"    <%  // 고장시작시간"     %>
		TROUBLE_TIME2			= "<spring:message code='epms.occurr.time.end'       />"    <%  // 고장종료시간"     %>
		TROUBLE_TIME_HOUR		= "<spring:message code='epms.occurr.time.hour'      />"    <%  // 고장시간(h)"    %>
		TROUBLE_TIME_MINUTE		= "<spring:message code='epms.occurr.time.min'       />"    <%  // 고장시간(m)"    %>
		REPAIR_TIME1			= "<spring:message code='epms.repair.time.start'     />"    <%  // 정비시작시간"     %>
		REPAIR_TIME2			= "<spring:message code='epms.repair.time.end'       />"    <%  // 정비종료시간"     %>
		REPAIR_TIME_HOUR		= "<spring:message code='epms.repair.time.hour'      />"    <%  // 정비시간(h)"    %>
		REPAIR_TIME_MINUTE		= "<spring:message code='epms.repair.time.min'       />"    <%  // 정비시간(m)"    %>
		ATTACH_GRP_NO2			= "<spring:message code='epms.attachfile.code'          />"    <%  // 첨부파일 코드"    %>
		                           
		REPAIR_ANALYSIS			= "<spring:message code='epms.repair.cause.analysis.id' />"    <%  // 고장원인분석ID   %>
		SAVE_YN					= "<spring:message code='epms.analysis.able'          />"    <%  // 분석여부             %>
		                           
		E_RESULT				= "<spring:message code='epms.sap.flag'               />"    <%  // SAP성공여부    %>
		E_AUFNR					= "<spring:message code='epms.sap.orderno'            />"    <%  // SAP오더번호    %>
		E_MESSAGE				= "<spring:message code='epms.sap.message'            />"    <%  // SAP메시지     %>
		                           
		detail					= "<spring:message code='epms.repair.perform.detail' />"    <%  // 실적상세       %>
		analysis				= "<spring:message code='epms.cause.analysis'        />"    <%  // 원인분석       %>
	/>
	
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
	
	<LeftCols>
	<%--	<C Name="REQUEST_TYPE"				Type="Enum"		Align="left"		Visible="1"		Width="70"	CanEdit="0"	<tag:enum codeGrp="REQUEST_TYPE"/>/> --%>
		<C Name="REQUEST_TYPE2"				Type="Enum"		Align="left"		Visible="1"		Width="70"	CanEdit="0"	Enum='|일상보전|계획보전' EnumKeys='|01|09'/>
		<C Name="URGENCY"					Type="Enum"		Align="left"		Visible="1"		Width="70"	CanEdit="0"	<tag:enum codeGrp="URGENCY"/>/>
		<c:choose>
			<c:when test="${param.flag eq 'repairResult'}">	
		<C Name="REPAIR_STATUS"				Type="Enum"		Align="left"		Visible="1"		Width="70"	CanEdit="0"	<tag:enum codeGrp="REPAIR_STATUS"/>/>
			</c:when>
			<c:when test="${param.flag eq 'maintenanceReview'}">
		<C Name="REPAIR_STATUS"				Type="Enum"		Align="left"		Visible="0"		Width="70"	CanEdit="0"	CanHide="0"	<tag:enum codeGrp="REPAIR_STATUS"/>/>
			</c:when>
			<c:otherwise>
			</c:otherwise>
		</c:choose>
		 
		
		<C Name="REPAIR_RESULT"				Type="Text"		Align="center"		Visible="0"		Width="60"	CanEdit="0"/>
		<C Name="LOCATION"					Type="Enum"		Align="left"		Visible="1"		Width="60"	CanEdit="0" <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>	
		<C Name="CATEGORY"					Type="Text"		Align="center"		Visible="0"		Width="60"	CanEdit="0"/>
		<C Name="CATEGORY_NAME"				Type="Text"		Align="left"		Visible="1"		Width="120"	CanEdit="0"/>
		<C Name="EQUIPMENT"					Type="Text"		Align="center"		Visible="0"		Width="60"	CanEdit="0"/>
		<C Name="EQUIPMENT_UID"				Type="Text"		Align="left"		Visible="1"		Width="80"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT_NAME"			Type="Text"		Align="left"		Visible="1"		Width="200"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
	</LeftCols>
	
	<Cols>
		<c:choose>
			<c:when test="${param.flag eq 'repairResult'}">	
		<C Name="GRADE"						Type="Enum"		Align="left"		Visible="1"		Width="70"	CanEdit="0"	<tag:enum codeGrp="GRADE"/>/>
			</c:when>
			<c:when test="${param.flag eq 'maintenanceReview'}">
		<C Name="GRADE"						Type="Enum"		Align="left"		Visible="0"		Width="70"	CanEdit="0"	<tag:enum codeGrp="GRADE"/>/>
			</c:when>
			<c:otherwise>
			</c:otherwise>
		</c:choose>
		<C Name="MODEL"						Type="Text"		Align="left"		Visible="1"		Width="100"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="MANUFACTURER"				Type="Text"		Align="left"		Visible="1"		Width="100"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		
		<C Name="MATERIAL_USED"				Type="Text"		Align="left"		Visible="1"		Width="150"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="MATERIAL_UNREG"			Type="Text"		Align="left"		Visible="1"		Width="150"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="TROUBLE_TYPE1"				Type="Enum"		Align="left"		Visible="1"		Width="100"	CanEdit="0" <tag:enum codeGrp="TROUBLE1"/>/>
		<C Name="TROUBLE_TYPE2"				Type="Enum"		Align="left"		Visible="1"		Width="100"	CanEdit="0" <tag:enum codeGrp="TROUBLE2"/>/>
		<C Name="REPAIR_DESCRIPTION"		Type="Text"		Align="left"		Visible="1"		Width="200"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="TROUBLE_TIME1"				Type="Date"		Align="center"		Visible="1"		Width="130"	CanEdit="0"	Format="yyyy/MM/dd HH:mm"/>
		<C Name="TROUBLE_TIME2"				Type="Date"		Align="center"		Visible="1"		Width="130"	CanEdit="0"	Format="yyyy/MM/dd HH:mm"/>
		<C Name="TROUBLE_TIME_HOUR"			Type="Int"		Align="center"		Visible="0"		Width="80"	CanEdit="0"/>
		<C Name="TROUBLE_TIME_MINUTE"		Type="Int"		Align="center"		Visible="1"		Width="80"	CanEdit="0"/>
		<C Name="REPAIR_TIME1"				Type="Date"		Align="center"		Visible="1"		Width="130"	CanEdit="0"	Format="yyyy/MM/dd HH:mm"/>
		<C Name="REPAIR_TIME2"				Type="Date"		Align="center"		Visible="1"		Width="130"	CanEdit="0"	Format="yyyy/MM/dd HH:mm"/>
		<C Name="REPAIR_TIME_HOUR"			Type="Int"		Align="center"		Visible="0"		Width="80"	CanEdit="0"/>
		<C Name="REPAIR_TIME_MINUTE"		Type="Int"		Align="center"		Visible="1"		Width="80"	CanEdit="0"/>
		<C Name="ATTACH_GRP_NO2"			Type="Text"		Align="center"		Visible="0"		Width="80"	CanEdit="0"	CanHide="0"/>
		<C Name="REPAIR_TYPE"				Type="Enum"		Align="left"		Visible="1"		Width="70"	CanEdit="0"	<tag:enum codeGrp="REPAIR_TYPE" companyId="${ssCompanyId}"/>/>
		<C Name="REPAIR_MEMBER"				Type="Text"		Align="left"		Visible="1"		Width="120"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="OUTSOURCING"				Type="Text"		Align="left"		Visible="1"		Width="100"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>	
		
		
		<C Name="REPAIR_REQUEST"			Type="Text"		Align="center"		Visible="0"		Width="60"	CanEdit="0"/>
		<C Name="REQUEST_DESCRIPTION"		Type="Text"		Align="left"		Visible="1"		Width="200"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="ATTACH_GRP_NO1"			Type="Text"		Align="center"		Visible="0"		Width="80"	CanEdit="0"	CanHide="0"/>
		<C Name="REQUEST_DIV"				Type="Text"		Align="left"		Visible="0"		Width="60"	CanEdit="0"	CanHide="0"/>
		<C Name="REQUEST_DIV_NAME"			Type="Text"		Align="left"		Visible="1"		Width="100"	CanEdit="0"/>
		<C Name="REQUEST_MEMBER"			Type="Text"		Align="left"		Visible="0"		Width="60"	CanEdit="0"/>
		<C Name="REQUEST_MEMBER_NAME"		Type="Text"		Align="left"		Visible="1"		Width="70"	CanEdit="0"/>
		<C Name="DATE_REQUEST"				Type="Date"		Align="center"		Visible="1"		Width="100"	CanEdit="0"	Format="yyyy/MM/dd"  />
		<C Name="APPR_MEMBER_NAME"			Type="Text"		Align="left"		Visible="1"		Width="70"	CanEdit="0"/>
		<C Name="DATE_APPR"					Type="Date"		Align="center"		Visible="1"		Width="100"	CanEdit="0"	Format="yyyy/MM/dd"  />
		
		 
		<C Name="PART"						Type="Enum"		Align="left"		Visible="1"		Width="60"	CanEdit="0" <tag:enum codeGrp="PART" companyId="${ssCompanyId}"/>/>
		<C Name="DATE_ASSIGN"				Type="Date"		Align="center"		Visible="0"		Width="100"	CanEdit="0"	Format="yyyy/MM/dd"  />
		<c:choose>
			<c:when test="${param.flag eq 'repairResult'}">	
		<C Name="DATE_PLAN"					Type="Date"		Align="center"		Visible="1"		Width="100"	CanEdit="0"	Format="yyyy/MM/dd"  />
			</c:when>
			<c:when test="${param.flag eq 'maintenanceReview'}">
		<C Name="DATE_PLAN"					Type="Date"		Align="center"		Visible="0"		Width="100"	CanEdit="0"	Format="yyyy/MM/dd"  />
			</c:when>
			<c:otherwise>
			</c:otherwise>
		</c:choose>
		<C Name="DATE_REPAIR"				Type="Date"		Align="center"		Visible="1"		Width="100"	CanEdit="0"	Format="yyyy/MM/dd"  />
		
<%-- 	<C Name="MANAGER"					Type="Enum"		Align="left"		Visible="1"		Width="70"	CanEdit="0" Enum='${repairMemberListOpt.MEMBER_NAME}'	EnumKeys='${repairMemberListOpt.MEMBER}'/> --%>	
		<C Name="MANAGER"					Type="Text"		Align="left"		Visible="0"		Width="70"	CanEdit="0" />
		<C Name="MANAGER_NAME"				Type="Text"		Align="left"		Visible="1"		Width="70"	CanEdit="0" />
		
		<C Name="E_RESULT"					Type="Text"		Align="center"		Visible="1"		Width="70"	CanEdit="0" CanHide="1"/>
		<C Name="E_AUFNR"					Type="Text"		Align="center"		Visible="1"		Width="110"	CanEdit="0" CanHide="1"/>
		<C Name="E_MESSAGE"					Type="Text"		Align="left"		Visible="1"		Width="400"	CanEdit="0" CanHide="1"/>
	</Cols>
	
	<RightCols>		
		<C Name="detail"					Type="Icon"		Align="center"		Visible="1"		Width="70"	CanEdit="0"	CanExport="1"/>
		<C Name="REPAIR_ANALYSIS"			Type="Text"		Align="center"		Visible="0"		Width="60"	CanEdit="0"/>
		<C Name="analysis"					Type="Icon"		Align="center"		Visible="0"		Width="70"	CanEdit="0"	CanExport="1"/>
		<C Name="SAVE_YN"					Type="Enum"		Align="center"		Visible="1"		Width="60"	CanEdit="0" Enum="|Y|N" EnumKeys="|Y|N"/>
	</RightCols>
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,항목,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        " 
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>