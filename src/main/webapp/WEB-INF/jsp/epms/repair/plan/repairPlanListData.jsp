<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%--
	Class Name	: repairPlanListData.jsp
	Description : 고장관리-정비일정관리 및 배정: 데이터
    author		: 김영환
    since		: 2018.03.19
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2018.04.10	 	김영환		최초 생성
	2018.05.18	 	김영환		ssSubAuth 권한 수정

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I
			REQUEST_TYPE2			= "${item.REQUEST_TYPE2}"
			REPAIR_RESULT			= "${item.REPAIR_RESULT}"
			REPAIR_STATUS			= "${item.REPAIR_STATUS}"
			SUB_ROOT				= "${item.SUB_ROOT}"
			LINE				    = "${item.LINE}"
			LINE_NAME			    = "${fn:replace(item.LINE_NAME, '"', '&quot;')}"
			EQUIPMENT				= "${item.EQUIPMENT}"
			EQUIPMENT_UID			= "${item.EQUIPMENT_UID}"
			EQUIPMENT_NAME			= "${fn:replace(item.EQUIPMENT_NAME, '"', '&quot;')}"
			
			REPAIR_REQUEST			= "${item.REPAIR_REQUEST}"
			URGENCY					= "${item.URGENCY}"
			<c:if test="${item.URGENCY == '02'}">
				URGENCYClass="gridStatus4"
				Background="#FFDDDD"
			</c:if>
			REQUEST_TYPE			= "${item.REQUEST_TYPE}"
			PART					= "${item.PART}"
			REQUEST_DESCRIPTION		= "${fn:replace(item.REQUEST_DESCRIPTION, '"', '&quot;')}"
			ATTACH_GRP_NO			= "${item.ATTACH_GRP_NO}"
			<c:if test='${item.FILE_CNT>0 || editYn eq "Y"}'>
				fileSwitch="1"  fileIcon="/images/com/web/commnet_up3.gif"
				fileOnClickSideIcon="fnOpenLayerWithParam('/attach/popFileManageForm.do?MATERIAL='+Row.MATERIAL+'&ATTACH_GRP_NO='+Row.ATTACH_GRP_NO+'&editYn='+'${editYn}','popFileManageForm');"
			</c:if>
			
			REQUEST_DIV				= "${item.REQUEST_DIV}"
			REQUEST_DIV_NAME		= "${item.REQUEST_DIV_NAME}"
			REQUEST_MEMBER			= "${item.REQUEST_MEMBER}"
			REQUEST_MEMBER_NAME		= "${item.REQUEST_MEMBER_NAME}"
			DATE_REQUEST			= "${item.DATE_REQUEST}"
			APPR_MEMBER				= "${item.APPR_MEMBER}"
			APPR_MEMBER_NAME		= "${item.APPR_MEMBER_NAME}"
			DATE_APPR				= "${item.DATE_APPR}"
			
			DATE_PLAN				= "${item.DATE_PLAN}"
			MANAGER					= "${item.MANAGER}"
			
			<c:choose>
				<c:when test="${item.REQUEST_TYPE ne '09'}">
					<c:choose>
						<c:when test="${(item.REPAIR_STATUS eq '01' or item.REPAIR_STATUS eq '02') and authType eq 'AUTH02'}">
							DATE_PLANCanEdit = "1"
							MANAGERCanEdit = "1"
							CanSelect = "1"
						</c:when>
						<c:when test="${(item.REPAIR_STATUS eq '01' or item.REPAIR_STATUS eq '02') and authType eq 'AUTH03' 
						and fn:indexOf(sessionScope.ssSubAuth, item.SUB_ROOT) > -1 and item.REMARKS eq '01' and user_part eq item.PART}">
							DATE_PLANCanEdit = "1"
							MANAGERCanEdit = "1"
							CanSelect = "1"
						</c:when>
						<c:when test="${(item.REPAIR_STATUS eq '01' or item.REPAIR_STATUS eq '02') and authType eq 'AUTH03' 
						and fn:indexOf(sessionScope.ssSubAuth, item.SUB_ROOT) > -1 and item.REMARKS eq '02'}">
							DATE_PLANCanEdit = "1"
							MANAGERCanEdit = "1"
							CanSelect = "1"
						</c:when>
						<c:otherwise>
							DATE_PLANCanEdit = "0"
							MANAGERCanEdit = "0"
							CanSelect = "0"
						</c:otherwise>
					</c:choose>
				</c:when>
				
				<%-- 계획보전(SAP-예방보전)일 경우  --%>
				<c:when test="${item.REQUEST_TYPE eq '09'}">
					<c:choose>
						<c:when test="${(item.REPAIR_STATUS eq '01' or item.REPAIR_STATUS eq '02') and authType eq 'AUTH02'}">
							DATE_PLANCanEdit = "1"
							MANAGERCanEdit = "1"
							CanSelect = "1"
						</c:when>
						<c:when test="${(item.REPAIR_STATUS eq '01' or item.REPAIR_STATUS eq '02') and authType eq 'AUTH03' 
						and fn:indexOf(sessionScope.ssSubAuth, item.SUB_ROOT) > -1 and item.REMARKS eq '01' and user_part eq item.PART}">
							DATE_PLANCanEdit = "1"
							MANAGERCanEdit = "1"
							CanSelect = "1"
						</c:when>
						<c:when test="${(item.REPAIR_STATUS eq '01' or item.REPAIR_STATUS eq '02') and authType eq 'AUTH03' 
						and fn:indexOf(sessionScope.ssSubAuth, item.SUB_ROOT) > -1 and item.REMARKS eq '02'}">
							DATE_PLANCanEdit = "1"
							MANAGERCanEdit = "1"
							CanSelect = "1"
						</c:when>
						<c:otherwise>
							DATE_PLANCanEdit = "0"
							MANAGERCanEdit = "0"
							CanSelect = "0"
						</c:otherwise>
					</c:choose>
				</c:when>
				
			</c:choose>
			
			
			<%-- 담당자 배정이 본인인 경우를 제외한 나머지 경우는 조회 페이지 이동 --%>
			<c:choose>
				<c:when test='${item.REPAIR_STATUS eq "02" && item.MANAGER eq userId}'>
					detailSwitch="1"  detailIcon="/images/com/web/commnet_up5.gif" 
					detailOnClickSideIcon="fnOpenLayerWithParam('/epms/repair/result/popRepairResultRegForm.do?REPAIR_REQUEST='+Row.REPAIR_REQUEST+'&REPAIR_RESULT='+Row.REPAIR_RESULT+'&EQUIPMENT='+Row.EQUIPMENT,'popRepairResultRegForm');"
				</c:when>
				<c:otherwise>
					detailSwitch="1"  detailIcon="/images/com/web/commnet_up.gif" 
					detailOnClickSideIcon="fnOpenLayerWithParam('/epms/repair/result/popRepairResultForm.do?REPAIR_REQUEST='+Row.REPAIR_REQUEST+'&REPAIR_RESULT='+Row.REPAIR_RESULT+'&EQUIPMENT='+Row.EQUIPMENT,'popRepairResultForm');"
				</c:otherwise>
			</c:choose>
			
			<%-- 정비 취소 가능 여부 --%>
			<c:choose>
				<%-- 계획보전(SAP-예방보전) or 예방보전의 정비요청시 정비취소 불가   --%>
				<c:when test="${item.REQUEST_TYPE eq '02' or item.REQUEST_TYPE eq '09'}">
					cancelSwitch="0"  cancelIcon=""
					cancelOnClickSideIcon=""
				</c:when>
				
				<%-- 일반 정비요청 --%>
				<c:when test="${item.REQUEST_TYPE ne '02' and item.REQUEST_TYPE ne '09'}">
					<c:choose>
						<c:when test="${fn:indexOf(sessionScope.ssSubAuth, 'AUTH02') > -1 and (item.REPAIR_STATUS eq '01' or item.REPAIR_STATUS eq '02') 
						and item.REQUEST_TYPE != '02' and fn:indexOf(sessionScope.ssSubAuth, item.SUB_ROOT) > -1 }">
							cancelSwitch="1"  cancelIcon="/images/com/web/commnet_up1.gif"
							cancelOnClickSideIcon="fnCancel(Row.REPAIR_RESULT,Row.REPAIR_REQUEST,Row.REQUEST_TYPE);"
						</c:when>
						<c:when test="${fn:indexOf(sessionScope.ssSubAuth, 'AUTH03') > -1 and (item.REPAIR_STATUS eq '01' or item.REPAIR_STATUS eq '02') 
						and item.REQUEST_TYPE != '02' and item.REMARKS eq '01' and fn:indexOf(sessionScope.ssSubAuth, item.SUB_ROOT) > -1 and user_part eq item.PART}">
							cancelSwitch="1"  cancelIcon="/images/com/web/commnet_up1.gif"
							cancelOnClickSideIcon="fnCancel(Row.REPAIR_RESULT,Row.REPAIR_REQUEST,Row.REQUEST_TYPE);"
						</c:when>
						<c:when test="${fn:indexOf(sessionScope.ssSubAuth, 'AUTH03') > -1 and (item.REPAIR_STATUS eq '01' or item.REPAIR_STATUS eq '02') 
						and item.REQUEST_TYPE != '02' and item.REMARKS eq '02' and fn:indexOf(sessionScope.ssSubAuth, item.SUB_ROOT) > -1}">
							cancelSwitch="1"  cancelIcon="/images/com/web/commnet_up1.gif"
							cancelOnClickSideIcon="fnCancel(Row.REPAIR_RESULT,Row.REPAIR_REQUEST,Row.REQUEST_TYPE);"
						</c:when>
						<c:otherwise>
							cancelSwitch="0"  cancelIcon=""
							cancelOnClickSideIcon=""
						</c:otherwise>
					</c:choose>
				</c:when>
			</c:choose>
						
			LOCATIONPART			= "${item.LOCATIONPART}"
			
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>