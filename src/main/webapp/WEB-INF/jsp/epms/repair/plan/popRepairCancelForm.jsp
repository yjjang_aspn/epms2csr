<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">

var submitBool = true;

$(document).ready(function() {
	
	$("#popTitle").html("정비취소");
	
	<%-- '정비취소'버튼 --%>
	$('#rejectBtn').on('click', function(){
		
		if (submitBool) {
			$('#flag').val("reject");
			fnRejectSave();
		} else {
			alert("등록중입니다. 잠시만 기다려 주세요.");
			return false;
		}
		
	});
});

<%-- 정비취소 --%>
function fnRejectSave(){
	
	submitBool = false;
	
	if (!isNull_J($('#CANCEL_DESCRIPTION'), '취소사유를 입력해주세요.')) {
		submitBool = true;
		return false;
	}
	
	if(confirm("정비를 취소하시겠습니까?")){
		fnCancelAjax($('#REPAIR_RESULT').val(),$('#REPAIR_REQUEST').val(),$('#CANCEL_DESCRIPTION').val());
	}
}
	
</script>

<!-- Layer PopUp Start -->	
<div class="modal-dialog root" style="width:450px;"> <!-- 원하는 width 값 입력 -->		
	<div class="modal-content" style="height:200px;">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title">정비취소</h4>
		</div>
	 	<div class="modal-body">
           	<div class="modal-bodyIn" style="height:50px;">
				<form id="cancelForm" name="cancelForm"  method="post" enctype="multipart/form-data">	
					<input type="hidden" class="inp-comm" id="REPAIR_RESULT" name="REPAIR_RESULT" value="${param.REPAIR_RESULT}"/>
					<input type="hidden" class="inp-comm" id="REPAIR_REQUEST" name="REPAIR_REQUEST" value="${param.REPAIR_REQUEST}"/>
						<div class="tb-wrap">
							<table class="tb-st">
								<caption class="screen-out">정비취소</caption>
								<colgroup>
									<col width="30%" />
									<col width="*" />
								</colgroup>
								<tbody>										
									<tr>
										<th scope="row">취소사유</th>
										<td>
											<div class="txt-wrap">
												<div class="txtArea">
													<textarea cols="" rows="3" title="취소사유" id="CANCEL_DESCRIPTION" name="CANCEL_DESCRIPTION"></textarea>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
				</form>
			</div>
		</div>
		<div class="modal-footer hasFooter">
			<a href="#none" id="cancelBtn" class="btn comm st02" data-dismiss="modal">취소</a>
			<a href="#none" id="rejectBtn" class="btn comm st01">정비취소</a>
		</div>
	</div>		
</div>
<!-- Layer PopUp End -->