<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ page import="java.util.Calendar"%>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jstl/fmt"           %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: repairPlanList.jsp
	Description : 고장관리-정비일정관리 및 배정: 메인
    author		: 김영환
    since		: 2018.03.19
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	----	---------------------------
	2018.04.10		김영환		최초 생성

--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%> 

<style>
.inp-comm.monthPic {box-sizing:border-box;height:28px;padding:5px 25px 4px 6px;border:1px solid #cdd2da;
	background:#fff url('/images/com/web/ico_cal.png') no-repeat right 5px center;background-size: 17px auto;cursor:pointer;}
</style>

<script type="text/javascript" src="/js/jquery/moment-with-locales.min.js"></script>

<!-- 예방보전관리 : 예방보전일정 관리 및 배정 -->
<script type="text/javascript">

var year = 1;       <%-- 년 --%>
var month = 1;      <%-- 월 --%>
var yearMonthTit;   <%-- 년,월 세팅 --%>
var target_year;    <%-- 선택된 년 --%>
var target_month;   <%-- 선택된 월 --%>
var sel_requestType;<%-- 선택된 정비요청유형 --%>
var sel_location; 	<%-- 선택된 공장 --%>
var sel_part;	  	<%-- 선택된 파트 --%>
var sel_manager;  	<%-- 선택된 담당자 --%>
var user_part;	  	<%-- 사용자 파트 --%>
var focusedRow = null;		<%-- Focus Row : [예방보전계획현황] --%>

$(document).ready(function() {
	
	<%-- datePic 설정 --%>
	$('.datePic').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	});
	
	<%-- 검색 시작일이 종료일보다 이후인지 체크 (이후라면, 종료일을 시작일로 변경) --%>
	$('#startDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#endDt').val($('#startDt').val());
		}
	});
	
	<%-- 검색 종료일이 시작일보다 이전인지 체크 (이전이라면, 시작일을 종료일로 변경) --%>
	$('#endDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#startDt').val($('#endDt').val());
		}
	});
	
	
	$('.monthPic').monthPicker();
	
	<%-- 서버의 현 날짜를 가져와 초기값으로 세팅 --%>
	<%-- 년도 초기값 세팅 --%>
 	$('#year').val("${startDt}".substring(0,4));
	<%-- 월 초기값 세팅 --%>
 	$('#month').val(parseInt("${startDt}".substring(4,6)));
	<%-- 일 초기갑 세팅 --%>
 	$('#day').val(parseInt("${startDt}".substring(6,8)));
	<%-- 달력 세팅  --%>
	
	makeCalendar();
		
	<%-- 검색 년,월을 가져와 분할후 달력 년/월 세팅 --%>
	$('#srcBtn').on('click', function(){
		
		var split_searchDt2=$('#searchDt2').val().split('-');
		
		target_year = Number(split_searchDt2[0]);
		target_month = Number(split_searchDt2[1]);
		
		$('#year').val(target_year);
		$('#month').val(target_month);
		
		removeCalendar();
		makeCalendar();
		setGrid(target_year,target_month);
		
	});
	
	<%-- 셀렉트 박스 변경시 --%>
	$('.inq-area-inner select').on('change', function(){
		$("#srcBtn").click();
		managerListReload();
	});
	
	<%-- monthPicker 선택시 --%>
	$("#monthList").click(function(){
		$("#srcBtn").click();
	});
	
	managerListReload();
	
	<%-- SAP동기화(예방보전오더생성) 버튼 --%>
	$('#sapSyncBtn').on('click', function(){
		if(confirm("계획보전(SAP_예방정비) 오더를 생성하시겠습니까?")){
			preventiveOrderInsert();
		}
	});
	
});

<%-- SAP동기화(예방보전오더생성) --%>
function preventiveOrderInsert(){
	
	// SAP 동기화시 : 달력 날짜로 동기화
	// I_SPMON(년월) : 필수 입력 파라미터 
	var calendar_date = $('#searchDt2').val().replaceAll('-','').trim(); 
	
	$.ajax({
		type: 'POST',
		async : false,
		url: '/epms/repair/plan/preventiveOrderInsert.do',
		dataType : 'json',
		data: {"I_SPMON" : calendar_date},
		success: function (json) {
			if(json.E_RESULT == 'S'){
				alert(json.E_MESSAGE);
				location.reload();
			}else{
				alert(json.E_MESSAGE);
			}
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('처리중 오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
		}
	});
}



<%-- 달력 변경시 기존의 생성된 달력을 삭제 --%>   
function removeCalendar(){
	<%-- 기존의 생성된 달력의 행의 갯수를 차례대로 삭제 --%>
	var pre_cal_rows = Number($('#pre_cal').val());
	
	for(var i = 1; i <= pre_cal_rows; i++){
		$('#st_cal').remove();
	}
}

<%-- 달력 세팅 (테이블 구조) --%>
<%-- 달력 생성 필수값 (년,월) --%>
function makeCalendar(){
	<%-- 날짜의 형태 (년,월)를 형 변환  --%>
	year = Number($('#year').val());
	month = Number($('#month').val()-1);
	
	day = $('#day').val();
	sel_part = $('select[name=sel_part]:visible').val();
	sel_location = $('#sel_plant').val();
	sel_manager = $('#sel_manager').val();
	sel_requestType = $('select[name=sel_requestType]:visible').val();
		
	<%-- 선택된 년,월에 해당하는 1일의 날짜를 세팅  --%>
	var curDate = new Date(year,month,1)
	
	<%-- 선택된 년,월의 1일에 해당하는 요일 세팅  --%>
	var dayName = curDate.getDay();
	
	<%-- 선택된 월의 마지막 일 세팅  --%>
	var lastDay = new Date(year,month+1,0).getDate();

	<%-- 현재 월의 달력에 필요한 행 구하기  --%>
	var row = Math.ceil((dayName+lastDay)/7);
	
	<%-- 달력에 표기되는 일의 초기 값  --%>
	var dNum = 1; <%-- 해당 달의 일자 초기  --%>
	var html;
	
	<%-- 클래스명에 해당하는 월 설정  --%>
	if(9 > month){
		month = month+1;
		month = '0' + month;
	}else{
		month = month+1;
	}
	
	<%-- 달력 행 만들기  --%>
	for(var i = 1; i <= row; i++){
		html += '<tr id="st_cal">';
		for(var j = 1; j < 8; j++){
			if(i===1 && j <= dayName || dNum > lastDay){
				html += '<td> &nbsp; </td>';
			}else{
				html += '<td><ul><li class="date" style="display:inline-block; height:15px; font-weight:bold;">' + dNum + '일' + '</li>';
				<%-- 클래스명에 해당하는 일 설정   --%>
				if(10 > dNum){
					dNum = '0' + dNum;
				}else{
					dNum;
				}
				html += '<li class="' + year + month + dNum + '"  style="float:right; height:15px; color:blue; font-weight:bold;"></li></ul>';
				html += '<div style="height:40px;text-align:center;cursor:pointer;" class="' + year + month + dNum + '_detail"></div>';
				html += '</td>';

				dNum++;
			}
		}
		html += "</tr>"
	}
	
	$('#cal_tb').append(html);
	$('#pre_cal').val(row);
	
	<%-- 해당된 년,월에 해당하는 예방점검 계획 횟수 조회  --%>
	yearMonthTit = String(year) + "년 " + String(month)+ "월";
	$('#yearMonthTit').text(yearMonthTit);
	$('#cur_year').val(year);
	$('#cur_month').val(month);
	
	$('#searchDt2').val(year+"-"+month);
	var remarks = $("select[name=sel_plant] option:selected").attr("role");
	
	var paramDate = String(year) + String(month);
	monthlyMaintenanceCount(paramDate,sel_location,sel_part,sel_manager,sel_requestType,remarks);
}

<%-- 해당된 년,월에 해당하는 예방 점검 계획 횟수 조회 --%>
function monthlyMaintenanceCount(paramDate,sel_location,sel_part,sel_manager,sel_requestType,remarks){
	
	$.ajax({
		type: 'POST',
		async : false,
		url: '/epms/repair/plan/repairPlanAjax.do',
		data: {searchDt : paramDate,
			   SUB_ROOT : sel_location,
			   PART : sel_part,
			   MANAGER : sel_manager,
			   REQUEST_TYPE : sel_requestType,
			   REMARKS : remarks},
		dataType: 'json',
		success: function (data) {
			<%-- 년,월,일에 해당하는 예방점검 데이터 출력  --%>
			(data.monthlyRepairPlanList).forEach(function (item){
				if(item.DATE_PLAN != null || item.DATE_PLAN !=''){
					$('.' + item.SEARCHDT).text('     (' + item.COMPLETE + '/' + item.DATE_PLAN + ')');
					
				}else{
					$('.' + item.searchDt).text('     (0/0)');
				}
				$('.' + item.SEARCHDT + '_detail').text(item.DATE_PLAN);
				$('.' + item.SEARCHDT + '_detail').attr("onClick","dailyMaintenanceList(" + item.SEARCHDT + ")");
				
				<%-- 일별 계획건이 일정 수치 이상 증가시  색상 크기 변경  --%>
				//if(item.maintenance_plan > 2){
					$('.' + item.SEARCHDT + '_detail').attr("style","color:red; font-size:20px; text-align:center; cursor:pointer;");	
				//}
			});
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('처리중 오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
		}
		
	});
	
}

<%-- 년,월,일에 해당하는 예방 점검 리스트 조회 --%>
function dailyMaintenanceList(param){

	sel_location  = $('#sel_plant').val();
	sel_part = $('select[name=sel_part]:visible').val();
	sel_requestType = $('select[name=sel_requestType]:visible').val();
	sel_manager = $('#sel_manager').val();
	$('#repairPlanList').removeAttr("style");
	Grids.RepairPlanList.Source.Data.Url = "/epms/repair/plan/repairPlanListData.do?searchDt="+param+"&PART="+sel_part+"&SUB_ROOT="+sel_location+"&MANAGER="+sel_manager+"&REQUEST_TYPE="+sel_requestType;
	Grids.RepairPlanList.Source.Layout.Url = "/epms/repair/plan/repairPlanListLayout.do?PART="+sel_part+"&SUB_ROOT="+sel_location;
	Grids.RepairPlanList.Reload();
}

<%-- row 값 계산  --%>
Grids.OnReady = function(grid){
	  var rowCnt = 0;
	  var rows = grid.Rows;
	  for(var key in rows){
	    if(key == "Header" || key == "NoData" || key == "Toolbar"){
	      continue;
	    } 
	    rowCnt ++;
	  }
	  gridDivResize(rowCnt);
}

<%-- 그리드 영역 사이즈 resizing --%>
function gridDivResize(dataSize){

	if(dataSize <= 1){
	  dataSize = 4;
	}
	
	var divSize = dataSize * 10;  
	
	divSize += 160;
	
	if(divSize < 160){
	  divSize = 160;
	}
	
	$("#repairPlanList").height(divSize);
	$('#repairPlanList').css('max-height','510px')
  
}
 
<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "RepairPlanList") {	<%-- [정비일정관리/배정] 클릭시 --%>
	
		<%-- 포커스  --%>
		if(focusedRow != null){
			grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		focusedRow = row;
		
	}
}

<%-- Validation --%>
function dataValidation(gridNm){
	var cnt = 0;
	var chkCnt = 0;
	var grid = Grids[gridNm];
	
	if(gridNm == "PreventivePlanList"){
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){
			if(row.Changed || row.Added){
				
				cnt++;
				
				<%-- 점검계획일이 만료일 이전인지 체크 --%>
				var date_assign = Grids[gridNm].GetValue(row, "DATE_ASSIGN");		<%-- 점검계획일 --%>
				var date_deadline = Grids[gridNm].GetValue(row, "DATE_DEADLINE");	<%-- 만료일 --%>
				
				if(date_assign > date_deadline){
					chkCnt++;
					Grids[gridNm].SetAttribute(row,"DATE_ASSIGN","Color","#FF6969",1);
				}else{
					Grids[gridNm].SetAttribute(row,"DATE_ASSIGN","Color","#FFFFAA",1);
				}
				
			}
		}
	}	
	
	<%-- 추가 및 변경된 데이터가 없을 경우 --%>
	if(cnt == 0){
		return false;
	}
	
	<%-- 점검계획일 체크 --%>
	if(chkCnt == 0){
		return true;
	}
	else{
		alert("점검계획일은 만료일 이전으로 수정 가능합니다.");
		return false;
	}	
}

<%-- 저장 버튼 --%>
function fnSaveGrid(gridNm){
	
	var cnt = 0;
	var grid = Grids[gridNm];
	
	for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){  
		if(row.Changed || row.Added){
			cnt++;
		}
	}
	
	<%-- 추가 및 변경된 데이터가 없을 경우 --%>
	if(cnt == 0){
		return false;
	}
	
	if(confirm("저장하시겠습니까?")){
		Grids[gridNm].ActionSave();
	}else{
		Grids[gridNm].ReloadBody();	
	}
    
}

<%-- 그리드 데이터 저장 후 --%> 
Grids.OnAfterSave  = function (grid){
	removeCalendar();
	makeCalendar();
	Grids.RepairPlanList.ReloadBody();
};

<%-- 선택된 년,월에 전월로 이동 --%>
function preDate(){
	<%-- 선택된 년,월의 전월로 세팅 --%>
	target_year = Number($('#cur_year').val());
	target_month = Number($('#cur_month').val()-1);

	<%-- 12월,1월 기준으로 년,월 자동 변경 --%>
	if(target_month > 12){
		target_year += 1;
		target_month = 1;
	}
	if(target_month < 1){
		target_year -= 1;
		target_month = 12;
	}
	<%-- 전월에 대한 년,월 세팅 --%>
	$('#year').val(target_year);
	$('#month').val(Number(target_month));
	
	removeCalendar();
	makeCalendar();
	//dailyMaintenanceList();
	
	<%-- 월의 경우 10월보다 작을시 0+월로 변경 --%>
	if(10 > target_month){
		target_month = target_month;
		target_month = '0' + target_month;
	}else{
		target_month = target_month;
	}
	
	yearMonthTit = String(target_year) + '년 ' + String(target_month)+ '월';
	$('#yearMonthTit').text(yearMonthTit);
	setGrid(Number(target_year),Number(target_month));
	
}

<%-- 선택된 년,월에 익월로 이동 --%>
function nextDate(){
	
	<%-- 선택된 년,월의 전월로 세팅 --%>
	target_year = Number($('#cur_year').val());
	target_month = Number($('#cur_month').val())+1;
	
	<%-- 12월,1월 기준으로 년,월 자동 변경 --%>
	if(target_month > 12){
		target_year += 1;
		target_month = 1;
	}
	if(target_month < 1){
		target_year -= 1;
		target_month = 12;
	}
	<%-- 전월에 대한 년,월 세팅 --%>
	$('#year').val(target_year);
	$('#month').val(Number(target_month));
	
	removeCalendar();
	makeCalendar();
	//dailyMaintenanceList();
	
	<%-- 월의 경우 10월보다 작을시 0+월로 변경 --%>
	if(10 > target_month){
		target_month = target_month;
		target_month = '0' + target_month;
	}else{
		target_month = target_month;
	}
	
	yearMonthTit = String(target_year) + '년 ' + String(target_month)+ '월';
	$('#yearMonthTit').text(yearMonthTit);
	
	setGrid(Number(target_year),Number(target_month));
}

<%-- 그리드 세팅 --%>
function setGrid(year,month){
	
    var txtYear = "";
    var txtMonth = "" ;
  
    <%-- 선택된 월의 마지막 일 세팅 --%>
	var LastDate = new Date(year,month,0).getDate();
	if(month>9){
		txtMonth = String(month);
	}else{
		txtMonth = '0' + String(month);
	}
	
	var startDt = String(year) + txtMonth + "01";
	var endDt = String(year) + txtMonth + String(LastDate);
	
	var sel_location = $('select[name=sel_plant]').val();
	var sle_part = $('select[name=sel_part]:visible').val();
	var sel_requestType = $('select[name=sel_requestType]:visible').val(); 
	var remarks = $("#sel_plant").find("option:selected").attr('role'); 
	
	$('#repairPlanList').removeAttr("style");
	Grids.RepairPlanList.Source.Data.Url = "/epms/repair/plan/repairPlanListData.do?srcType=2&startDt="+startDt+"&endDt="+endDt+"&PART="+sel_part+"&SUB_ROOT="+sel_location+"&MANAGER="+sel_manager+"&REQUEST_TYPE="+sel_requestType;
	Grids.RepairPlanList.Source.Layout.Url = "/epms/repair/plan/repairPlanListLayout.do?PART=" + sel_part + "&SUB_ROOT=" + sel_location + "&REMARKS=" + remarks;
	Grids.RepairPlanList.Reload();
	
}

<%-- 담당자 일괄 변경 --%>
function batchManager(grid,row,col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return 
	}
	
	var changePerson = row.MANAGER;
	var selRows = Grids.RepairPlanList.GetSelRows();
// 	var sel_part = $('#sel_part').val();
	
	for(var i=0; i <selRows.length;i++){
// 		if(sel_part==selRows[i].PART){
		Grids["RepairPlanList"].SetValue(selRows[i], "MANAGER", changePerson, 1);
// 		}
	}

}

<%-- 계획일 일괄 변경 --%>
function batchDatePlan(grid,row,col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return 
	}
	
	var changeDate = row.DATE_PLAN;
	var selRows = Grids.RepairPlanList.GetSelRows();
	
	for(var i=0; i <selRows.length;i++){
		Grids["RepairPlanList"].SetValue(selRows[i], "DATE_PLAN", changeDate, 1);
	}
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- 정비 취소 모달 호출 --%>
function fnCancel(REPAIR_RESULT,REPAIR_REQUEST,REQUEST_TYPE){

 	if(REQUEST_TYPE == "<c:out value="${EPMS_REQUEST_TYPE_PREVENTIVE}"/>"){
 		alert("예방정비건은 취소할 수 없습니다.");
 	}else {
 		var url = "/epms/repair/plan/popRepairCancelForm.do?"
			  + "REPAIR_RESULT=" + REPAIR_RESULT
			  + "&REPAIR_REQUEST=" + REPAIR_REQUEST
			  + "&REQUEST_TYPE=" + REQUEST_TYPE;
		$('#repairPlanCancleFormModal').load(url, function(responseTxt, statusTxt, xhr){			
			$(this).modal();
		});
		
 	}
}
 
<%-- 정비 취소 --%>
function fnCancelAjax(REPAIR_RESULT, REPAIR_REQUEST, CANCEL_DESCRIPTION){
	
	$.ajax({
		type : 'POST',
		url : '/epms/repair/plan/repairDelete.do',
		dataType : 'json',
		data : {REPAIR_RESULT:REPAIR_RESULT, REPAIR_REQUEST:REPAIR_REQUEST, CANCEL_DESCRIPTION:CANCEL_DESCRIPTION},
		success : function(json) {
			
			if(json!=null){
				toggleModal($("#repairPlanCancleFormModal"));
				fnReload_afterCancel();
			} else {
				alert("정비 취소에 실패하였습니다.");
			}
			
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
			loadEnd();
		}
	});
}

<%-- 모달 토글 --%>
function toggleModal(obj){
	obj.modal('toggle');
}

<%-- 팝업창 정비취소 등록 이후 새로고침 --%>
function fnReload_afterCancel(){
	Grids.RepairPlanList.ReloadBody();
	removeCalendar();
	makeCalendar();
	alert("정상적으로 취소 되었습니다.");
}

<%-- 정비원 리스트 호출 --%>
function managerListReload(){
	
	var sel_location = $('select[name=sel_plant]').val();
	var part = $('select[name=sel_part]:visible').val();
	var remarks = $("#sel_plant").find("option:selected").attr('role');
	sel_manager = $("#sel_manager").val();
	
	$.ajax({
		type: 'POST',
		async : false,
		url: '/epms/preventive/plan/managerListAjax.do',
		data: {"SUB_ROOT" : sel_location, "PART" : part, "REMARKS" : remarks, "sel_manager":sel_manager},
		dataType: 'json',
		success: function (data) {
			
			if(data.managerListOpt.length == 0){
				var html = "<option value=''>" + "전체" + " </option>";
				$("#sel_manager").find("option").remove();
				$("#sel_manager").append(html);
			}else if (data.managerListOpt.length > 0) {
				$("#sel_manager").find("option").remove();
				var html = "<option value=''>" + "전체" + " </option>";
				$.each(data.managerListOpt, function() {
					html += "<option value='"+ this.USER_ID + "'>" + this.NAME + " </option>"
				});
				$("#sel_manager").append(html);
				$("#sel_manager option[value='"+ sel_manager +"']").attr('selected', true);
			}
			
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('처리중 오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
		}
		
	});

}

function fnReload(msg){
	
	alert(msg);
	Grids.RepairPlanList.ReloadBody()
// 	Grids.RepairPlanList.Source.Data.Url = "/epms/repair/plan/repairPlanListData.do?srcType=" + $('#srcType').val()
// 										 + "&startDt=" + $('#startDt').val()
// 										 + "&endDt=" + $('#endDt').val();
// 	Grids.RepairPlanList.Reload();
}

</script>

</head>
<body>

	<input type="hidden" id="pre_cal" name="pre_cal">
	<input type="hidden" id="cur_year" name="cur_year">
	<input type="hidden" id="cur_month" name="cur_month">
	<input type="hidden" id="year" name="year">
	<input type="hidden" id="month" name="month">
	<input type="hidden" id="day" name="day">
	<input type="hidden" id="REMARKS" name="REMARKS">
	<input type="hidden" id="sel_part" name="sel_part">
		
	<!-- 화면 UI Area Start  -->
	<div class="scl"  style="height:100%">
		<div style="padding:15px;">
	
			<div class="rel">
				<h5 class="panel-tit">일정관리/배정</h5><!-- 정비 일정관리/배정 -->
				<div class="inq-area-inner" style="margin-top:0; padding-top:30px;">
					<ul class="wrap-inq">
					
						<li class="inq-clmn">
							<h4 class="tit-inq">구분</h4> 
							<div class="sel-wrap type02" style="width:80px;">
								<select title="구분" id="sel_requestType" name="sel_requestType">
									<option value="">전체</option>
									<c:forEach var="item" items="${requestTypeList}">
										<option value="${item.COMCD}" <c:if test="${item.COMCD eq 01}"> selected="selected"</c:if>>${item.COMCD_NM}</option>
									</c:forEach>
								</select> 
							</div>
						</li>
					
						<li class="inq-clmn">
							<h4 class="tit-inq"><spring:message code='epms.category' /></h4> <!-- 공장 -->
							<div class="sel-wrap type02" style="width:80px;">
								<select title="공장" id="sel_plant" name="sel_plant" >
									<option value="">전체</option>
									<c:forEach var="item" items="${locationList}">
										<option value="${item.CODE}" role="${item.REMARKS}" <c:if test="${sessionScope.ssLocation eq item.CODE}"> selected="selected"</c:if>>${item.NAME}</option>
									</c:forEach>
								</select> 
							</div>
						</li>
						
						<li class="inq-clmn">
							<h4 class="tit-inq">파트</h4>
							<div class="sel-wrap type02" style="width:70px;">
								<select title="파트" name="sel_part" OnChange="managerListReload()" >
									<option value="">전체</option>
									<c:forEach var="item" items="${partListOpt}">
										<option value="${item.CODE}" "<c:if test="${part eq item.CODE}"> selected="selected"</c:if>>${item.NAME}</option>
									</c:forEach>
								</select>
								
							</div>
						</li>
						
						<li class="inq-clmn">
							<h4 class="tit-inq"><spring:message code='epms.worker' /></h4><!-- 정비원 -->
							<div class="sel-wrap type02" style="width:80px;">
								<select title="<spring:message code='epms.worker' />" id="sel_manager" name="sel_manager">
								</select>
							</div>
						</li>
						
						<li class="inq-clmn">
							<h4 class="tit-inq">기준월</h4>
							<div class="prd-inp-wrap" style="width:100px;">
								<input type="text" id="searchDt2" class="inp-comm monthPic" readonly="readonly" title="기준월" value="${searchDt2}">
							</div>
						</li>
						
					</ul>
					<a href="#none" id="srcBtn" class="btn comm st01">검색</a>
					<!-- 
					<a href="#none" id="sapSyncBtn" class="btn comm st01" style="margin-left: 7px;">SAP 동기화</a>
					 -->
				</div>
				<div class="mgn-t-20">
					<div style="text-align:center;">
						<a href="#none" class="btn comm st02"  onClick="preDate()">&lt;</a>
						<span class="btn comm " id="yearMonthTit"  style="width:200px; font-size:18px; color:#4c4c4c; font-weight:bold;"></span>
						<a href="#none" class="btn comm st02"  onClick="nextDate()">&gt;</a>
					</div>
					<div class="tb-wrap mgn-t-10">
						<table class="tb-st type01"  id="cal_tb">
							<caption class="screen-out">예방보전일정</caption>
							<colgroup>
								<col width="14.3%"/>
								<col width="14.3%"/>
								<col width="14.3%"/>
								<col width="14.3%"/>
								<col width="14.3%"/>
								<col width="14.3%"/>
								<col width="*"/>
							</colgroup>
							<tr id=dayName>
								<th class="t-c">일</th>
								<th class="t-c">월</th>
								<th class="t-c">화</th>
								<th class="t-c">수</th>
								<th class="t-c">목</th>
								<th class="t-c">금</th>
								<th class="t-c">토</th>
							</tr>	
						</table>
					</div>
				</div>
			</div>
		
			<div class="mgn-t-20 rel">
				<h5 class="panel-tit">계획현황</h5>
				<!-- 트리그리드 : s -->
				<div id="repairPlanList">
					<bdo	Debug="Error"
							Data_Url="/epms/repair/plan/repairPlanListData.do?srcType=2&startDt=${startDt}&endDt=${endDt}&SUB_ROOT=${location}&PART=${part}&REQUEST_TYPE=01"
							Layout_Url="/epms/repair/plan/repairPlanListLayout.do"
							Upload_Url="/epms/repair/plan/repairPlanListEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"			
							Export_Data="data" Export_Type="xls"
							Export_Url="/sys/comm/exportGridData.jsp?File=정비일정관리목록.xls&dataName=data"
					>
					</bdo>
				</div>
				<!-- 트리그리드 : e -->				
			</div>
		</div>
	</div>
	
<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<div class="modal fade modalFocus" id="repairPlanCancleFormModal" data-backdrop="static" data-keyboard="false"></div>	<%-- 정비취소 모달 --%>
<div class="modal fade modalFocus" id="popFileManageForm"   data-backdrop="static" data-keyboard="false"></div> 		<%-- 고장사진 보기 모달 --%>

<%-- 타파트 요청 --%>
<div class="modal fade modalFocus" id="popRequestPartForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 정비실적등록 --%>
<div class="modal fade modalFocus" id="popRepairResultRegForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 정비실적조회 --%>
<div class="modal fade modalFocus" id="popRepairResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 정비원 검색 --%>
<div class="modal fade modalFocus" id="popWorkerList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-35">		
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">
                    <spring:message code='epms.worker' /> <spring:message code='epms.search' />
                </h4><!-- 정비원 검색 -->
			</div>
	 	<div class="modal-body" >
           	<div class="modal-bodyIn">
           		<div class="modal-section">
					<div class="fl-box wd-per-100">
						<div id="snsList">
							<bdo Debug="Error"
								 Data_Url=""
								 Layout_Url="/epms/repair/result/popWorkerListLayout.do"
							 >
							</bdo>
						</div>
					</div>
				</div> <%-- modal-section : e --%>
			</div> <%-- modal-bodyIn : e --%>
		</div> <%-- modal-body : e --%>
	</div> <%-- modal-content : e --%>
</div> <%-- modal : e --%>
</div>

<%-- 부품명 검색 --%>
<div class="modal fade modalFocus" id="popRepairMaterialList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-50">
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">
                    <spring:message code='epms.object.name' /> <spring:message code='epms.search' />
                </h4><!-- 부품명 검색 -->
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	           		<div class="modal-section">
						<div class="fl-box wd-per-100">
							<bdo	Debug="Error"
					 				Data_Url=""
									Layout_Url="/epms/repair/result/popRepairMaterialListLayout.do" 
							>
							</bdo>
						</div>
					</div> <%-- modal-section : e --%>
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>
	
</body>
</html>