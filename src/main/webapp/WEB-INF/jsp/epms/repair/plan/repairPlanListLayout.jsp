<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: repairPlanListLayout.jsp
	Description : 고장관리 - 정비일정관리 및 배정: 그리드 레이아웃 
    author		: 김영환
    since		: 2018.03.19
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2018.03.19		김영환		최초 생성
	2018.05.18	 	김영환		ssSubAuth 권한 수정
	
--%>

<c:set var="gridId" value="RepairPlanList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1"			SuppressCfg="0"
			Editing="1"				Deleting="0"		Selecting="1"		Dragging="0"
			Filtering="1"			StandardFilter="3"	ClearSelected="2"
			Paging="1"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="0"			PasteFocused="3"
	/>
	
	<c:choose>
		<c:when test="${fn:indexOf(sessionScope.ssSubAuth, 'AUTH02') > -1}">
			<Cfg Editing="1"/>
		</c:when>
		<c:when test="${fn:indexOf(sessionScope.ssSubAuth, 'AUTH03') > -1}">
			<Cfg Editing="1"/>
		</c:when>
		<c:otherwise>
			<Cfg Editing="0"/>
		</c:otherwise>
	</c:choose>

		<Header	id="Header"	Align="center"
			REQUEST_TYPE2			= "<spring:message code='epms.repair.status'    />"    <%  // 구분"           %>
			REPAIR_STATUS			= "<spring:message code='epms.repair.status'    />"    <%  // 정비상태"         %>
			REPAIR_RESULT			= "<spring:message code='epms.repair.id'        />"    <%  // 정비ID"         %>
			SUB_ROOT				= "<spring:message code='epms.location'      />"    <%  // 위치"           %>
			LINE				    = "<spring:message code='epms.category'      />"    <%  // 기능위치"         %>
			LINE_NAME			    = "<spring:message code='epms.category.name' />"    <%  // 라인"           %>
			EQUIPMENT				= "<spring:message code='epms.system.id'        />"    <%  // 설비ID"         %>
			EQUIPMENT_UID			= "<spring:message code='epms.system.uid'       />"    <%  // 설비코드"         %>
			EQUIPMENT_NAME			= "<spring:message code='epms.system.name'      />"    <%  // 설비명"          %>
			                           
			REPAIR_REQUEST			= "<spring:message code='epms.work.request.id' />"    <%  // 정비요청ID"       %>
			URGENCY					= "<spring:message code='epms.repair.urgency' />"    <%  // 긴급도"          %>
			REQUEST_TYPE			= "<spring:message code='epms.request.type' />"    <%  // 요청유형"         %>
			PART					= "<spring:message code='epms.repair.part'         />"    <%  // 파트"           %>
			REQUEST_DESCRIPTION		= "<spring:message code='epms.request.content'     />"    <%  // 고장내용"         %>
			ATTACH_GRP_NO			= "<spring:message code='epms.attachfile.code' />"    <%  // 첨부파일 코드"      %>
			file					= "<spring:message code='epms.attachfile' />"    <%  // 고장사진"         %>
			REQUEST_DIV				= "<spring:message code='epms.repair.request.part.id' />"    <%  // 요청부서ID"       %>
			REQUEST_DIV_NAME		= "<spring:message code='epms.repair.request.part' />"    <%  // 요청부서"         %>
			REQUEST_MEMBER			= "<spring:message code='epms.repair.request.id' />"    <%  // 요청자ID"        %>
			REQUEST_MEMBER_NAME		= "<spring:message code='epms.repair.request' />"    <%  // 요청자"          %>
			DATE_REQUEST			= "<spring:message code='epms.repair.request.date' />"    <%  // 요청일"          %>
			APPR_MEMBER				= "<spring:message code='epms.appr.member.id' />"    <%  // 결재자ID"        %>
			APPR_MEMBER_NAME		= "<spring:message code='epms.appr.member' />"    <%  // 결재자"          %>
			DATE_APPR				= "<spring:message code='epms.appr.date' />"    <%  // 결재일"          %>
			                           
			DATE_PLAN				= "<spring:message code='epms.plan.date' />"    <%  // 계획일"	         %>
			MANAGER					= "<spring:message code='epms.repair.manager' />"    <%  // 담당자"          %>
			detail					= "<spring:message code='epms.repair.perform.detail' />"    <%  // 실적상세"         %>
			cancel					= "<spring:message code='epms.request.cancel' />"    <%  // 정비취소"         %>
			LOCATIONPART			= "<spring:message code='epms.location.part' />"    <%  // 공장파트"         %>
		/>
	
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
	
	<Cols>
		<C Name="REQUEST_TYPE2"				Type="Enum" Align="left"   Visible="1" Width="70" CanEdit="0" CanExport="1" CanHide="1" Enum='${repairRequestTypeListOpt.COMCD_NM}' EnumKeys='${repairRequestTypeListOpt.COMCD}'/>
		<C Name="URGENCY"					Type="Enum" Align="left"   Visible="1" Width="70" CanEdit="0" <tag:enum codeGrp="URGENCY"/>/>
		<C Name="REPAIR_STATUS"				Type="Enum" Align="left"   Visible="1" Width="70" CanEdit="0" CanExport="1" CanHide="1" Enum='${repairStatusListOpt.NAME}' EnumKeys='${repairStatusListOpt.CODE}'/>
			
		<C Name="REPAIR_RESULT"				Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="0" />
		<C Name="SUB_ROOT"					Type="Enum" Align="left"   Visible="1" Width="80"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/> />	
		<C Name="LINE"					    Type="Text" Align="center" Visible="0" Width="80"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="LINE_NAME"				    Type="Text" Align="left"   Visible="0" Width="140" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT"					Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="EQUIPMENT_UID"				Type="Text" Align="left"   Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT_NAME"			Type="Text" Align="left"   Visible="1" RelWidth="77" CanEdit="0" CanExport="1" CanHide="1" MinWidth="218" RelWidth="218" CaseSensitive="0" WhiteChars=" "/>
		
		<C Name="REPAIR_REQUEST"			Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="0" />
		<C Name="REQUEST_TYPE"				Type="Enum" Align="left"   Visible="0" Width="70"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="REQUEST_TYPE"/>/>
		<C Name="REQUEST_DESCRIPTION"		Type="Text" Align="left"   Visible="1" RelWidth="500" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="ATTACH_GRP_NO"				Type="Text" Align="center" Visible="0" Width="80"  CanEdit="0" CanExport="1" CanHide="0" />
		<C Name="file"						Type="Icon" Align="center" Visible="0" Width="70"  CanEdit="0" CanExport="1" CanHide="1" />
		
		<C Name="REQUEST_DIV"				Type="Text" Align="left"   Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="0" />
		<C Name="REQUEST_DIV_NAME"			Type="Text" Align="left"   Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="REQUEST_MEMBER"			Type="Text" Align="left"   Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="REQUEST_MEMBER_NAME"		Type="Text" Align="left"   Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="DATE_REQUEST"				Type="Date" Align="center" Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd" />
		<C Name="APPR_MEMBER"				Type="Text" Align="left"   Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="APPR_MEMBER_NAME"			Type="Text" Align="left"   Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="DATE_APPR"					Type="Date" Align="center" Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd" />
		
	</Cols>
	
	<RightCols>
		<C Name="PART"						Type="Enum" Align="left"   Visible="1" Width="60"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="PART" companyId="${ssCompanyId}"/> />
		<C Name="DATE_PLAN"					Type="Date" Align="center" Visible="1" Width="110" CanEdit="1" CanExport="1" CanHide="1" Format="yyyy/MM/dd" OnChange="batchDatePlan(Grid,Row,Col);" />
		<C Name="MANAGER"					Type="Enum" Align="left"   Visible="1" Width="70"  CanEdit="1" CanExport="1" CanHide="1" Related="LOCATIONPART" Enum='${repairMemberListOpt.NAME}'	EnumKeys='${repairMemberListOpt.CODE}' ${relatedCombo} OnChange="batchManager(Grid,Row,Col);"/>
		<C Name="detail"					Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="cancel"					Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/> 
		<C Name="LOCATIONPART"				Type="Text" Align="left"   Visible="0" Width="80"  CanEdit="0" CanExport="1" CanHide="0" CanFilter="0"/>
	</RightCols>
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,저장,항목,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        "
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				추가Type="Html" 추가="&lt;a href='#none' title='추가' class=&quot;defaultButton01 icon add&quot;
					onclick='addRowGrid(&quot;${gridId}&quot;)'>추가&lt;/a>"
				저장Type="Html" 저장="&lt;a href='#none' title='저장' class=&quot;treeButton treeSave&quot;&quot;
					onclick='fnSaveGrid(&quot;${gridId}&quot;)'>저장&lt;/a>"
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton01 icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>