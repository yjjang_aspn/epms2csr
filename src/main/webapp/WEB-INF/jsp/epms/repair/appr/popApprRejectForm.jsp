<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">

var submitBool = true;

$(document).ready(function() {
	
	$("#popTitle").html("반려");
	
	<%-- '반려'버튼 클릭 --%>
	$('#popApprRejectForm_rejectBtn').on('click', function(){
		
		if (submitBool) {
			fnRejectSave();
		} else {
			alert("등록중입니다. 잠시만 기다려 주세요.");
			return false;
		}
		
	});
});

<%-- 반려  --%>
function fnRejectSave(){
	
	submitBool = false;
	
	if (!isNull_J($('#POP_REJECT_DESCRIPTION'), '반려사유를 입력해주세요.')) {
		submitBool = true;
		return false;
	}
	
	if(confirm("반려 처리하시겠습니까?")){
		var modalName = "repairApprRejectFormModal";
		fnApprAjax($('#PART').val(),$('#POP_REJECT_DESCRIPTION').val(),"","",modalName);
	}
}
	
</script>

<!-- 결재라인 관리 화면 모달 : s -->
<!-- Layer PopUp Start -->	 
<div class="modal-dialog root" style="width:450px;"> <!-- 원하는 width 값 입력 -->				
	 <div class="modal-content" style="height:220px;">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title">반려</h4>
		</div>
	 	<div class="modal-body">
           	<div class="modal-bodyIn">

				<form id="apprForm" name="apprForm"  method="post" enctype="multipart/form-data">	
					<div class="tb-wrap">
						<table class="tb-st">
							<caption class="screen-out">반려</caption>
							<colgroup>
								<col width="30%" />
								<col width="*" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">반려사유</th>
									<td>
										<div class="txt-wrap">
											<div class="txtArea">
												<textarea cols="" rows="3" title="반려사유" id="POP_REJECT_DESCRIPTION" name="POP_REJECT_DESCRIPTION"></textarea>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</form>
			</div>
		</div>	
		<div class="modal-footer hasFooter">
			<a href="#none" id="cancelBtn" class="btn comm st02" data-dismiss="modal">취소</a>
			<a href="#none" id="popApprRejectForm_rejectBtn" class="btn comm st01">반려</a>
		</div>
	</div>
</div>
<!-- Layer PopUp End -->