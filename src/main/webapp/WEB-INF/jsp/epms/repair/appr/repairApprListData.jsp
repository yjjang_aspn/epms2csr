<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%--
	Class Name	: repairApprListData.jsp
	Description : 고장관리-정비요청결제: 데이터
    author		: 김영환
    since		: 2018.04.10
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2018.04.10	 	김영환		최초 생성

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I
			
			REPAIR_REQUEST			= "${item.REPAIR_REQUEST}"
			URGENCY					= "${item.URGENCY}"
			<c:if test="${item.URGENCY == '02'}">
				URGENCYClass="gridStatus4"
				Background="#FFDDDD"
			</c:if>
			LOCATION				= "${item.LOCATION}"
			CATEGORY				= "${item.CATEGORY}"
			LINE_NAME			    = "${fn:replace(item.LINE_NAME, '"', '&quot;')}"
			EQUIPMENT				= "${item.EQUIPMENT}"
			EQUIPMENT_UID			= "${item.EQUIPMENT_UID}"
			EQUIPMENT_NAME			= "${fn:replace(item.EQUIPMENT_NAME, '"', '&quot;')}"
			
			REQUEST_TYPE			= "${item.REQUEST_TYPE}"
			REPAIR_REQUEST_ORG		= "${item.REPAIR_REQUEST_ORG}"
			
			APPR_STATUS				= "${item.APPR_STATUS}"
			<c:if test="${item.APPR_STATUS == '02'}">
				APPR_STATUSClass="gridStatus3"
			</c:if>
			<c:if test="${item.APPR_STATUS == '03' || item.APPR_STATUS == '04' || item.APPR_STATUS == '09'}">
				APPR_STATUSClass="gridStatus4"
			</c:if>
			
			REQUEST_DESCRIPTION		= "${fn:replace(item.REQUEST_DESCRIPTION, '"', '&quot;')}"
			ATTACH_GRP_NO			= "${item.ATTACH_GRP_NO}"
			<c:if test='${item.FILE_CNT > 0 || editYn eq "Y" }'>
				fileSwitch="1"  fileIcon="/images/com/web/commnet_up3.gif" 
				fileOnClickSideIcon="fnOpenLayerWithParam('/attach/popFileManageForm.do?MATERIAL='+Row.MATERIAL+'&ATTACH_GRP_NO='+Row.ATTACH_GRP_NO+'&editYn='+'${editYn}','popFileManageForm');"
			</c:if> 
						
			REQUEST_DIV				= "${item.REQUEST_DIV}"
			REQUEST_DIV_NAME		= "${item.REQUEST_DIV_NAME}"
			DATE_REQUEST			= "${item.DATE_REQUEST}"
			REQUEST_MEMBER			= "${item.REQUEST_MEMBER}"
			REQUEST_MEMBER_NAME		= "${item.REQUEST_MEMBER_NAME}"
			DATE_APPR				= "${item.DATE_APPR}"
			APPR_MEMBER				= "${item.APPR_MEMBER}"
			APPR_MEMBER_NAME		= "${item.APPR_MEMBER_NAME}"
			PART					= "${item.PART}"
			REJECT_DESCRIPTION		= "${fn:replace(item.REJECT_DESCRIPTION, '"', '&quot;')}"
			
			CANCEL_DESCRIPTION		= "${fn:replace(item.CANCEL_DESCRIPTION, '"', '&quot;')}"
			DATE_CANCEL				= "${item.DATE_CANCEL}"
			CANCEL_MEMBER			= "${item.CANCEL_MEMBER}"
			CANCEL_MEMBER_NAME		= "${item.CANCEL_MEMBER_NAME}"
			
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>