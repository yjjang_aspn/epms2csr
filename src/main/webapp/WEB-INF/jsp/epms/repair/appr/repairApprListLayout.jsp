<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld"                 %>
<%@ taglib prefix="c"	   uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: repairApprList.jsp
	Description : 고장관리 - 정비요청결제: 그리드 레이아웃
    author		: 김영환
    since		: 2018.03.19
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.19	김영환		최초 생성
	
--%>

<c:set var="gridId" value="RepairApprList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1"			SuppressCfg="0"
			Editing="1"				Deleting="0"		Selecting="0"		Dragging="0"
			Filtering="1"			StandardFilter="3"	ClearSelected="2"
			Paging="1"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="0"			PasteFocused="3"
	/>

		<Header	id="Header"	Align="center"
			REPAIR_REQUEST			= "<spring:message code='epms.work.request.id'         />"    <%  //정비요청ID"        %>
			URGENCY					= "<spring:message code='epms.repair.urgency'            />"    <%  //긴급도"           %>
			LOCATION				= "<spring:message code='epms.location'               />"    <%  //위치"            %>
			CATEGORY				= "<spring:message code='epms.category'               />"    <%  //기능위치"          %>
			LINE_NAME			    = "<spring:message code='epms.category.name'          />"    <%  //라인"            %>
			EQUIPMENT				= "<spring:message code='epms.system.id'                 />"    <%  //설비ID"          %>
			EQUIPMENT_UID			= "<spring:message code='epms.system.uid'                />"    <%  //설비코드"          %>
			EQUIPMENT_NAME			= "<spring:message code='epms.system.name'               />"    <%  //설비명"           %>
			                           
			PART					= "<spring:message code='epms.repair.part'               />"    <%  //파트"            %>
			REQUEST_TYPE			= "<spring:message code='epms.request.type'       />"    <%  //요청유형"          %>
			REQUEST_TYPE2			= "<spring:message code='epms.kind'                      />"    <%  //구분"            %>
			REPAIR_REQUEST_ORG		= "<spring:message code='epms.work.request.id.org' />"    <%  //원정비요청ID"       %>
			APPR_STATUS				= "<spring:message code='epms.appr.status'               />"    <%  //결재상태"          %>
			DATE_REQUEST			= "<spring:message code='epms.repair.request.date'       />"    <%  //요청일"           %>
			REQUEST_DESCRIPTION		= "<spring:message code='epms.request.content'           />"    <%  //고장내용"          %>
			REJECT_DESCRIPTION		= "<spring:message code='epms.reject.reason'             />"    <%  //반려사유"          %>
			                           
			REQUEST_DIV				= "<spring:message code='epms.repair.request.part.id'    />"    <%  //요청부서ID"        %>
			REQUEST_DIV_NAME		= "<spring:message code='epms.repair.request.part'       />"    <%  //요청부서"          %>
			REQUEST_MEMBER			= "<spring:message code='epms.repair.request.id'         />"    <%  //요청자ID"         %>
			REQUEST_MEMBER_NAME		= "<spring:message code='epms.repair.request'            />"    <%  //요청자"           %>
			                          
			DATE_APPR				= "<spring:message code='epms.appr.date'                 />"    <%  //결재일"           %>
			APPR_MEMBER				= "<spring:message code='epms.appr.member.id'            />"    <%  //결재자ID"         %>
			APPR_MEMBER_NAME		= "<spring:message code='epms.appr.member'               />"    <%  //결재자"           %>
			                           
			DATE_CANCEL				= "<spring:message code='epms.request.cancel.date'      />"    <%  //정비취소일"         %>
			CANCEL_MEMBER			= "<spring:message code='epms.request.cancel.member.id' />"    <%  //취소등록자ID"       %>
			CANCEL_MEMBER_NAME		= "<spring:message code='epms.request.cancel.member'    />"    <%  //취소등록자"         %>
			                           
			ATTACH_GRP_NO			= "<spring:message code='epms.attachfile.code'             />"    <%  //첨부파일 코드"       %>
			file					= "<spring:message code='epms.attachfile'                  />"    <%  //첨부파일"          %>
			HISTORY					= "<spring:message code='epms.history'               />"    <%  //이력"            %>
		/>
	
	<Solid>
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
	
	<LeftCols>
		<C Name="APPR_STATUS"				Type="Enum" Align="left"   Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="APPR_STATUS"/>/>
	</LeftCols>
	
	<Cols>
		<C Name="REQUEST_TYPE"				Type="Enum" Align="left"   Visible="0" Width="70"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="REQUEST_TYPE"/>/>
		<C Name="REQUEST_TYPE2"				Type="Enum" Align="left"   Visible="0" Width="70"  CanEdit="0" Enum='|일상보전|계획보전' EnumKeys='|01|09'/>
		<C Name="URGENCY"					Type="Enum" Align="left"   Visible="1" Width="70"  CanEdit="0" <tag:enum codeGrp="URGENCY"/>/>
		
		<C Name="REPAIR_REQUEST"			Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="REPAIR_REQUEST_ORG"		Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />		
		<C Name="LOCATION"					Type="Enum" Align="left"   Visible="1" Width="60"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>	
		<C Name="CATEGORY"					Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="LINE_NAME"				    Type="Text" Align="left"   Visible="1" Width="120" CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="EQUIPMENT"					Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="EQUIPMENT_UID"				Type="Text" Align="left"   Visible="1" Width="80"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT_NAME"			Type="Text" Align="left"   Visible="1" Width="180" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		
		<C Name="PART"						Type="Enum" Align="left"   Visible="1" Width="60"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="PART" companyId="${ssCompanyId}"/>/>
		<C Name="DATE_REQUEST"				Type="Date" Align="center" Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd"  />
		<C Name="REQUEST_DIV"				Type="Text" Align="left"   Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="0" />
		<C Name="REQUEST_DIV_NAME"			Type="Text" Align="left"   Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="REQUEST_MEMBER"			Type="Text" Align="left"   Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="REQUEST_MEMBER_NAME"		Type="Text" Align="left"   Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="REQUEST_DESCRIPTION"		Type="Text" Align="left"   Visible="1" Width="200" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		
		<C Name="DATE_APPR"					Type="Date" Align="center" Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd" />
		<C Name="APPR_MEMBER"				Type="Text" Align="left"   Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="APPR_MEMBER_NAME"			Type="Text" Align="left"   Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="REJECT_DESCRIPTION"		Type="Text" Align="left"   Visible="1" Width="200" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		
		<C Name="DATE_CANCEL"				Type="Date" Align="center" Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd" />
		<C Name="CANCEL_MEMBER"				Type="Text" Align="left"   Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="CANCEL_MEMBER_NAME"		Type="Text" Align="left"   Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="ATTACH_GRP_NO"				Type="Text" Align="center" Visible="0" Width="80"  CanEdit="0" CanExport="1" CanHide="0" />
		<C Name="file"						Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" />
	</Cols>
		
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,항목,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        " 
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				추가Type="Html" 추가="&lt;a href='#none' title='추가' class=&quot;defaultButton01 icon add&quot;
					onclick='addRowGrid(&quot;${gridId}&quot;)'>추가&lt;/a>"
				저장Type="Html" 저장="&lt;a href='#none' title='저장' class=&quot;treeButton treeSave&quot;&quot;
					onclick='saveGrid(&quot;${gridId}&quot;)'>저장&lt;/a>"
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton01 icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
	
</Grid>