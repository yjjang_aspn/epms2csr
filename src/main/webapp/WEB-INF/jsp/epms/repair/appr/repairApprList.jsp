<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jstl/fmt"           %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>

<%--
	Class Name	: repairApprList.jsp
	Description : 고장관리-정비요청결제: 메인
    author		: 김영환
    since		: 2018.04.10
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2018.04.10	 	김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<style>
.emergency {color:red!important;font-weight:bold;}
</style>

<!-- 고장관리 : 정비요청결재 -->
<script type="text/javascript">

<%-- 전역변수 --%>
var msg = "";				<%-- 결과 메세지 --%>
var focusedRow = null;		<%-- Focus Row : [정비일정관리/배정] --%>

$(document).ready(function(){
	
	<%-- DatePicker --%>
	$('.datePic').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	});
	
	<%-- 검색 시작일이 종료일보다 이후인지 체크 (이후라면, 종료일을 시작일로 변경) --%>
	$('#startDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#endDt').val($('#startDt').val());
		}
	});
	
	<%-- 검색 종료일이 시작일보다 이전인지 체크 (이전이라면, 시작일을 종료일로 변경) --%>
	$('#endDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#startDt').val($('#endDt').val());
		}
	});
	
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function() {		<%-- 모달이 사용자에게 보여졌을 때  --%>
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>
	});
	
	$('#btns').hide(); 						<%-- 파트이관, 반려, 승인 버튼모두 --%>
	$('#trAPPR').hide();					<%-- 결제일, 결제자 --%>
	$('#trCANCEL').hide();					<%-- 정비취소일, 취소등록자 --%>
	$('#trREJECT_DESCRIPTION').hide();		<%-- 반려사유 --%>
	$('#trCANCEL_DESCRIPTION').hide();		<%-- 취소사유 --%>
	
	<%-- '검색' 클릭 --%>
	$('#srcBtn').on('click', function(){
		Grids.RepairApprList.Source.Data.Url = "/epms/repair/appr/repairApprListData.do?startDt=" + $('#startDt').val()
											 + "&endDt=" + $('#endDt').val();
		Grids.RepairApprList.ReloadBody();
	});
	
	<%-- '파트이관' 클릭 --%>
	$('#changeBtn').on('click', function(){
		$('#flag').val("<c:out value="${FLAG_UPDATE}"/>");
		fnSave("<c:out value="${FLAG_UPDATE}"/>");
	});
	
	<%-- '반려' 클릭 --%>
	$('#rejectBtn').on('click', function(){		
		$('#flag').val("<c:out value="${FLAG_REJECT}"/>");
		fnSave("<c:out value="${FLAG_REJECT}"/>");
	});
	
	<%-- '승인' 클릭 --%>
	$('#apprBtn').on('click', function(){
		$('#flag').val("<c:out value="${FLAG_APPR}"/>");
		fnSave("<c:out value="${FLAG_APPR}"/>");
	});
	 
	<%-- 파일 다운로드  --%>
	$('#fileNotNull').on('click','.btnFileDwn', function(){
		var ATTACH = $(this).data("attach");
		var ATTACH_GRP_NO = $('#ATTACH_GRP_NO').val();
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO='+ATTACH_GRP_NO+'&ATTACH='+ATTACH;
		$("#fileDownFrame").attr("src",src);
	});
	
	<%-- 일괄 다운로드(압축 파일) --%>
	$('#fileNotNull').on('click', '.zipFileBtn', function(){
		var ATTACH_GRP_NO = $('#ATTACH_GRP_NO').val();
		var src = '/attach/zipFileDownload.do?ATTACH_GRP_NO='+ATTACH_GRP_NO;
		$("#fileDownFrame").attr("src",src);
	});
	
}); <%-- $(document).ready 종료 --%>

<%-- 그리드 클릭 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "RepairApprList") { <%-- [결재현황] 클릭시 --%>
		<%-- 포커스 시작 --%>
		if(focusedRow != null){
			grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		focusedRow = row;
		
		<%-- 상세정보 조회 --%>
		getRepairRequestDtlAjax(row.REPAIR_REQUEST);
	}
}

<%-- 선택된 정비요청의 상세정보  조회 --%>
function getRepairRequestDtlAjax(REPAIR_REQUEST){
	
	$.ajax({
		type: 'POST',
		url: '/epms/repair/appr/repairRequestDtlAjax.do',
		data: { REPAIR_REQUEST : REPAIR_REQUEST},
		dataType: 'json',
		success: function (json) {
			if(json != "" && json != null) {
				
				<%-- 결재상태가 '대기'일 경우만 버튼 활성화 --%>
				if(json.repairRequestDtl.APPR_STATUS == "<c:out value="${EPMS_APPR_STATUS_WAIT}"/>"){<%-- 결재상태가 '대기'일 경우 --%>
					$('#btns').show();         <%-- 파트이관, 반려, 승인 버튼모두 --%>
					$('#changeBtn').show();	   <%-- 파트이관 --%>
					$('#rejectBtn').show();
					if(json.repairRequestDtl.REMARKS == "<c:out value="${REMARKS_UNITED}"/>"){		 <%-- 비고(공장파트구분 01:파트구분,02:파트통합) --%>
						$('#changeBtn').hide();
					}
					
					//예방보전이나 일일보전의 경우 반려버튼 비활성화
					if(json.repairRequestDtl.REQUEST_TYPE == "02" || json.repairRequestDtl.REQUEST_TYPE == "09"){ <%-- 예방보전:02, 일일보전:09) --%>
						$('#rejectBtn').hide();
					}
				}else{
					$('#btns').hide();
				}
				
				if(json.repairRequestDtl.APPR_STATUS == "<c:out value="${EPMS_APPR_STATUS_WAIT}"/>"){					<%-- 결제상태가 '대기'인 경우 --%> 
					$('#trAPPR').hide();								<%-- 결제일, 결제자 --%>
					$('#trCANCEL').hide();                              <%-- 정비취소일, 취소등록자 --%>
					$('#trREJECT_DESCRIPTION').hide();					<%-- 반려사유 --%>
					$('#trCANCEL_DESCRIPTION').hide();					<%-- 취소사유 --%>
				}else if(json.repairRequestDtl.APPR_STATUS == "<c:out value="${EPMS_APPR_STATUS_APPR}"/>"){	    		<%-- 결제상태가 '승인'인 견우 --%>
					$('#trAPPR').show();
					$('#trCANCEL').hide();
					$('#trREJECT_DESCRIPTION').hide();
					$('#trCANCEL_DESCRIPTION').hide();					
				}else if(json.repairRequestDtl.APPR_STATUS == "<c:out value="${EPMS_APPR_STATUS_REJECT}"/>"){			<%-- 결제상태가 '반려'인 경우 --%>
					$('#trAPPR').show();					
					$('#trCANCEL').hide();
					$('#trREJECT_DESCRIPTION').show();
					$('#trCANCEL_DESCRIPTION').hide();
				}else if(json.repairRequestDtl.APPR_STATUS == "<c:out value="${EPMS_APPR_STATUS_CANCEL_REPAIR}"/>"){	<%-- 결제상태가 '정비취소'인 경우 --%>
					$('#trAPPR').show();
					$('#trCANCEL').show();
					$('#trREJECT_DESCRIPTION').hide();
					$('#trCANCEL_DESCRIPTION').show();
				}else if(json.repairRequestDtl.APPR_STATUS == "<c:out value="${EPMS_APPR_STATUS_CANCEL_REQUEST}"/>"){	<%-- 결제상태가 '정비취소'일 경우 --%>
					$('#trAPPR').show();
					$('#trCANCEL').show();
					$('#trREJECT_DESCRIPTION').hide();
					$('#trCANCEL_DESCRIPTION').show();
				}
				$('#REPAIR_REQUEST').val(json.repairRequestDtl.REPAIR_REQUEST);            <%-- 정비요청ID --%>
				$('#REPAIR_RESULT').val(json.repairRequestDtl.REPAIR_RESULT);              <%-- 정비실적ID --%>
				$('#APPR_STATUS').val(json.repairRequestDtl.APPR_STATUS);                  <%-- 결재상태 --%>
				$('#APPR_STATUS_NAME').val(json.repairRequestDtl.APPR_STATUS_NAME);        <%-- 결재상태명 --%>
				$('#URGENCY').val(json.repairRequestDtl.URGENCY);                          <%-- 긴급도 --%>
				$('#URGENCY_NAME').val(json.repairRequestDtl.URGENCY_NAME);                <%-- 긴급도명 --%>
				if(json.repairRequestDtl.URGENCY =='02'){
					$('#URGENCY_NAME').addClass("emergency");
				}
				else{
					$('#URGENCY_NAME').removeClass("emergency");
				}
				$('#DATE_REQUEST').val(json.repairRequestDtl.DATE_REQUEST);                <%-- 요청일 --%>
				$('#REQUEST_DIV').val(json.repairRequestDtl.REQUEST_DIV);                  <%-- 요청부서ID --%>
				$('#REQUEST_DIV_NAME').val(json.repairRequestDtl.REQUEST_DIV_NAME);        <%-- 요청부서 --%>
				$('#REQUEST_MEMBER').val(json.repairRequestDtl.REQUEST_MEMBER);            <%-- 요청자ID --%>
				$('#REQUEST_MEMBER_NAME').val(json.repairRequestDtl.REQUEST_MEMBER_NAME);  <%-- 요청자 --%>
				$('#LOCATION').val(json.repairRequestDtl.LOCATION);                        <%-- 위치 --%>
				$('#LOCATION_NAME').val(json.repairRequestDtl.LOCATION_NAME);              <%-- 위치명 --%>
				$('#LINE').val(json.repairRequestDtl.LINE);                                <%-- 라인 --%>
				$('#LINE_NAME').val(json.repairRequestDtl.LINE_NAME);                      <%-- 라인명 --%>
				$('#EQUIPMENT').val(json.repairRequestDtl.EQUIPMENT);                      <%-- 설비ID --%>
				$('#EQUIPMENT_UID').val(json.repairRequestDtl.EQUIPMENT_UID);              <%-- 설비코드 --%>
				$('#EQUIPMENT_NAME').val(json.repairRequestDtl.EQUIPMENT_NAME);            <%-- 설비명 --%>
				$('#REQUEST_DESCRIPTION').text(json.repairRequestDtl.REQUEST_DESCRIPTION); <%-- 고장내용 --%>
				
				$('#DATE_APPR').val(json.repairRequestDtl.DATE_APPR);                      <%-- 결재일 --%>
				$('#APPR_MEMBER').val(json.repairRequestDtl.APPR_MEMBER);                  <%-- 결재자ID --%>
				$('#APPR_MEMBER_NAME').val(json.repairRequestDtl.APPR_MEMBER_NAME);        <%-- 결재자 --%>
				$('#REJECT_DESCRIPTION').text(json.repairRequestDtl.REJECT_DESCRIPTION);   <%-- 반려사유 --%>
				$('#CANCEL_DESCRIPTION').text(json.repairRequestDtl.CANCEL_DESCRIPTION);   <%-- 반려사유 --%>
				
				$('#DATE_CANCEL').val(json.repairRequestDtl.DATE_CANCEL);                  <%-- 정비취소일 --%>
				$('#CANCEL_MEMBER').val(json.repairRequestDtl.CANCEL_MEMBER);              <%-- 취소등록자ID --%>
				$('#CANCEL_MEMBER_NAME').val(json.repairRequestDtl.CANCEL_MEMBER_NAME);    <%-- 취소등록자 --%>
				
				$('#PART').val(json.repairRequestDtl.PART);                                <%-- 파트 --%>
				$('#PART_NAME').val(json.repairRequestDtl.PART_NAME);                      <%-- 파트명 --%>
				$('#REMARKS').val(json.repairRequestDtl.REMARKS);                          <%-- 비고 --%>
				$('#PREVENTIVE_RESULT').val(json.repairRequestDtl.PREVENTIVE_RESULT);      <%-- 정비요청ID --%>
				$('#ATTACH_GRP_NO').val(json.repairRequestDtl.ATTACH_GRP_NO);      		   <%-- 첨부파일 그룹 넘버 --%>
				$('#SUB_ROOT').val(json.repairRequestDtl.SUB_ROOT);  	    		   	   <%-- 공장정보 --%>
				
				var html = "";
				$("#canvas2").empty();
	            $("#detailFileList0").empty();
	            $("#fileNotNull").find('.zipFileBtn').remove();
	            
				if(json.attachList != null){
					
					$("#fileNull").hide()
					$("#fileNotNull").show()
					$("#previewImg").show()
					$("#fileNotNull").prepend('<a href="#none" value="11" class="zipFileBtn btn evn-st01 mgn-t-5" >일괄 다운로드</a>');
					
					$.each(json.attachList, function (i, o) {
						var _tr1 = $("#template1").html();
						var _tr2 = $("#template2").html();
					
						    _tr1 = $(_tr1);
						    _tr2 = $(_tr2);
						
				            for (var name in o) {
				            	
				            	switch (name) {
					 				case "ATTACH":
					 					_tr1.attr("id", "fileLstWrap_"+ o[name]);
					 					_tr1.find("a.btnFileDwn").attr("data-attach", o[name]);
					 					break;
					 				case "SEQ_DSP":
					 					_tr1.find("span.MultiFile-export").text(o[name]);
					 					break;
					 				case "NAME":
					 					_tr1.find("span.MultiFile-title").text(o["FILE_NAME"]+"("+o["FILE_SIZE_TXT"]+")");
					 					_tr2.find("p").text(o["FILE_NAME"]+"("+o["FILE_SIZE_TXT"]+")").append('<a href="#none" class="btnFileDwn icon download" data-attach=' + o["ATTACH"]  + '>다운로드</a>');
					 					break;
					 				case "ATTACH_GRP_NO":
					 					_tr2.find("img").attr("src", "/attach/fileDownload.do?ATTACH_GRP_NO="+o[name]+"&ATTACH="+o["ATTACH"]);
					 					_tr2.find("img").attr("onclick", "originFileView('/attach/fileDownload.do?ATTACH_GRP_NO="+o[name]+"&ATTACH="+o["ATTACH"]+"')");
					 					break;
					 				case "FILE_NAME":
					 					_tr2.find("a.btnFileDwn").attr("data-attach", o["ATTACH"]);
					 					break;
					 					
				 				} //end switch문
				            } //end for문
				            
			            $("#canvas2").append(_tr1);
				            
			            if(o["FORMAT"] == 'jpg' || o["FORMAT"] == 'png' || o["FORMAT"] == 'jpeg' || o["FORMAT"] == 'JPG' || o["FORMAT"] == 'PNG'){
							$("#detailFileList0").append(_tr2); <%-- '이미지'파일인 경우만 미리보기 --%>
			            }
			            
				    });
					
					if($("#detailFileList0").has('li').size()==0){ 
						$("#previewImg").hide();
					}
					
				}else if (json.attachList == null) {
					$("#fileNull").show();
					$("#fileNotNull").hide();
				}
			}
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('리스트 로딩 중 오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
			loadEnd();
		}
	});
}

<%-- 파트이관,반려,승인 --%>
function fnSave(type){
	var part     = $('#PART').val();
	var location = $('#LOCATION').val();
	var remarks  = $('#REMARKS').val();
	var sub_root  = $('#SUB_ROOT').val();
	
	if(type == "<c:out value="${FLAG_UPDATE}"/>"){          <%-- 파트이관일 경우 --%>
		msg = "정상적으로 파트이관 처리되었습니다.";
		var url = "/epms/repair/appr/popApprChangeForm.do?"
			    + "PART=" + part;
		$('#repairApprChangeFormModal').load(url, function(responseTxt, statusTxt, xhr){
			$(this).modal();
		});
	}
	else if (type == "<c:out value="${FLAG_REJECT}"/>"){   <%-- 반려일 경우 --%>
		msg = "정상적으로 반려 처리되었습니다.";
		var url = "/epms/repair/appr/popApprRejectForm.do?"
			    + "PART=" + part;
		$('#repairApprRejectFormModal').load(url, function(responseTxt, statusTxt, xhr){
			$(this).modal();
		});
	}
	else if (type == "<c:out value="${FLAG_APPR}"/>"){     <%-- 승인일 경우 --%>	
		msg = "정상적으로 승인 처리되었습니다.";
		var url = "/epms/repair/appr/popApprOkForm.do?"
			    + "PART=" + part + "&LOCATION=" + location + "&REMARKS=" + remarks + "&SUB_ROOT=" + sub_root;
		$('#repairApprOkFormModal').load(url, function(responseTxt, statusTxt, xhr){
			$(this).modal();	
		});
	}
}

<%-- 승인,반려,파트이관 ajax --%>
function fnApprAjax(PART,REJECT_DESCRIPTION,MANAGER,DATE_PLAN,MODAL_NAME){
	
	var repair_request     = $('#REPAIR_REQUEST').val();
	var repair_result      = $('#REPAIR_RESULT').val();
	var preventive_result  = $('#PREVENTIVE_RESULT').val();
	var remarks = $('#REMARKS').val();
	var sub_root = $('#SUB_ROOT').val();
	
	$.ajax({
		type : 'POST',
		url : '/epms/repair/appr/repairApprAjax.do',
		dataType : 'json',
		data : { REPAIR_REQUEST: repair_request, 
				 REPAIR_RESULT: repair_result, 
				 flag:$('#flag').val(), 
				 PART: PART, 
				 REJECT_DESCRIPTION: REJECT_DESCRIPTION, 
				 MANAGER: MANAGER, 
				 DATE_PLAN: DATE_PLAN,
				 PREVENTIVE_RESULT: preventive_result, 
				 REMARKS:remarks,
				 SUB_ROOT:sub_root },
		async : false,
		success : function(json) {
			if(json == null){
				msg = "요청건이 존재하지 않습니다.";
			}
			else{
				if(json.apprYn != "Y"){
					if(json.requestDtl.APPR_STATUS == "02"){
						msg = "이미 승인된 요청건입니다.";
					}
					else if(json.requestDtl.APPR_STATUS == "03"){
						msg = "이미 반려된 요청건입니다.";
					}
					else{
						msg = "이미 처리된 요청건입니다.";
					}
				}
			}
			
			if(!(MODAL_NAME=='' || MODAL_NAME==null)){
				toggleModal($('#'+MODAL_NAME));
			}
			fnReload();
			
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
			setTimeout($.unblockUI, 100);
		}
	});
}

<%-- 모달 토글 --%>
function toggleModal(obj){
	obj.modal('toggle');
}

<%-- 새로고침 --%>
function fnReload(){
	var startDt = $('#startDt').val();
	var endDt   = $('#endDt').val();
	var repair_request = $('#REPAIR_REQUEST').val()
	
	Grids.RepairApprList.Source.Data.Url = "/epms/repair/appr/repairApprListData.do?startDt="+ startDt
			                             + "&endDt=" + endDt;
	Grids.RepairApprList.ReloadBody();
	getRepairRequestDtlAjax(repair_request);
	alert(msg);
}

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source,data, func){
	if(grid.id == "RepairApprList"){
		if(data != ""){
			callLoadingBar();
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "RepairApprList"){
					setTimeout($.unblockUI, 100);
				}
			}
		}
	}
}

<%-- 첨부파일 다운로드 팝업 호출--%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- 이미지 원본크기 보기 --%>
function originFileView(img){ 
	img1= new Image(); 
	img1.src=(img); 
	imgControll(img); 
} 

function imgControll(img){ 
	if((img1.width!=0)&&(img1.height!=0)){ 
		viewImage(img); 
	}else{ 
		controller="imgControll('"+img+"')"; 
		intervalID=setTimeout(controller,20); 
	} 
}

function viewImage(img){ 
	W=img1.width; 
	H=img1.height; 
	O="width="+W+",height="+H+",scrollbars=yes"; 
	imgWin=window.open("","",O); 
	imgWin.document.write("<html><head><title>이미지상세보기</title></head>");
	imgWin.document.write("<body topmargin=0 leftmargin=0>");
	imgWin.document.write("<img src="+img+" onclick='self.close()' style='cursor:pointer;' title ='클릭하시면 창이 닫힙니다.'>");
	imgWin.document.close();
}

</script>
</head>
<body>

<!-- 화면 UI Area Start  -->
<div id="contents">

	<div class="fl-box panel-wrap04" style="width:50%">
		<h5 class="panel-tit" id="RepairApprListTitle">결재 현황</h5>	
		<div class="inq-area-inner type06 ab">
			<div class="wrap-inq">
				<div class="inq-clmn" id="aterialInOutDate">
					<h4 class="tit-inq" >요청일</h4>
					<div class="prd-inp-wrap">
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic thisMonth inpCal" readonly="readonly" id="startDt" title="검색시작일" value="${startDt}" >
							</span>
						</span>
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic inpCal" readonly="readonly" id="endDt" title="검색종료일" value="${endDt}">
							</span>
						</span>		
					</div>
				</div>
				<!-- e:inq-clmn -->
			</div>
			<!-- wrap-inq -->
			<a href="#none" id="srcBtn" class="btn comm st01">검색</a>
		</div>
		<!-- e:inq-area-inner -->
		<div class="panel-body mgn-t-20">
			<!-- 트리그리드 : s -->
			<div id="RepairApprList">
				<bdo	Debug="Error"
						Data_Url="/epms/repair/appr/repairApprListData.do?startDt=${startDt}&endDt=${endDt}"
						Layout_Url="/epms/repair/appr/repairApprListLayout.do"
						
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=결재현황 목록.xls&dataName=data"
				>
				</bdo>
			</div>
			<!-- 트리그리드 : e -->
		</div>
	</div>
	
	<form id="apprForm" name="apprForm"  method="post" enctype="multipart/form-data">
		<input type="hidden" class="inp-comm" id="flag" 			 name="flag"/>                  <%-- 플래그  --%>
		<input type="hidden" class="inp-comm" id="REPAIR_REQUEST"    name="REPAIR_REQUEST"/> 		<%-- 정비요청ID --%>
		<input type="hidden" class="inp-comm" id="REMARKS" 			 name="REMARKS"/>               <%-- 파트통합/분리 --%>
		<input type="hidden" class="inp-comm" id="REPAIR_RESULT" 	 name="REPAIR_RESULT"/>   		<%-- 정비실적ID --%>
		<input type="hidden" class="inp-comm" id="PREVENTIVE_RESULT" name="PREVENTIVE_RESULT"/>     <%-- 정비실적ID --%>
		<input type="hidden" class="inp-comm" id="ATTACH_GRP_NO" 	 name="ATTACH_GRP_NO" "/>
		<input type="hidden" class="inp-comm" id="SUB_ROOT" 	 	 name="SUB_ROOT" "/>			<%-- 공장정보 --%>
	</form>
	
	<div class="panel-wrap05" style="float:right; width:50%">
		<h5 class="panel-tit mgn-l-10"  id="repairRequestDetailTitle"><spring:message code='epms.request.detail' /></h5><!-- 정비요청 상세 -->
		<div class="panel-body mgn-l-10 mgn-t-40" style="overflow-y:auto;overflow-x:hidden;">
			<div id="btns"  class="ab" style="top:0; right:0;">
				<a href="#none" id="changeBtn" class="btn comm st02">파트이관</a>
				<a href="#none" id="rejectBtn" class="btn comm st02">반려</a>
				<a href="#none" id="apprBtn" class="btn comm st01">승인</a>
			</div>
			<div class="tb-wrap" >
				<table class="tb-st">
					<caption class="screen-out"><spring:message code='epms.request.detail' /></caption><!-- 정비요청 상세 -->
					<colgroup>
						<col width="15%" />
						<col width="35%" />
						<col width="15%" />
						<col width="*" />
					</colgroup>
					<tbody>
						<tr>
							<th scope="row"><spring:message code='epms.appr.status' /></th><!-- 결재상태 -->
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="hidden" class="inp-comm" id="APPR_STATUS" name="APPR_STATUS"/>
									<input type="text" class="inp-comm" id="APPR_STATUS_NAME" name="APPR_STATUS_NAME" readonly="readonly"/>
								</div>
							</td>
							<th scope="row"><spring:message code='epms.repair.urgency' /></th><!-- 긴급도 -->
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="hidden" class="inp-comm" id="URGENCY" name="URGENCY"/>
									<input type="text" class="inp-comm" id="URGENCY_NAME" name="URGENCY_NAME" readonly="readonly"/>
								</div>
							</td>
						</tr>
						<tr>
							<th scope="row"><spring:message code='epms.category' /></th><!-- 공장 -->
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="hidden" class="inp-comm" id="LOCATION" name="LOCATION"/>
									<input type="text" class="inp-comm" id="LOCATION_NAME" name="LOCATION_NAME" readonly="readonly"/>
								</div>
							</td>
							<th scope="row"><spring:message code='epms.repair.part' /></th><!-- 처리파트 -->
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="hidden" class="inp-comm" id="PART" name="PART"/>
									<input type="text" class="inp-comm" id="PART_NAME" name="PART_NAME" readonly="readonly"/>
								</div>
							</td>
						</tr>
						<tr>
							<th scope="row"><spring:message code='epms.category' /></th><!-- 라인 -->
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="hidden" class="inp-comm" id="LINE" name="LINE"/>
									<input type="text" class="inp-comm" id="LINE_NAME" name="LINE_NAME" readonly="readonly"/>
								</div>
							</td>
							<th scope="row"><spring:message code='epms.repair.request.date' /></th><!-- 요청일시 -->
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="text" class="inp-comm" id="DATE_REQUEST" name="DATE_REQUEST" readonly="readonly"/>
								</div>
							</td>
						</tr>
						<tr>
							<th scope="row"><spring:message code='epms.system.uid' /></th><!-- 설비코드 -->
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="hidden" class="inp-comm" id="EQUIPMENT" name="EQUIPMENT"/>
									<input type="text" class="inp-comm" id="EQUIPMENT_UID" name="EQUIPMENT_UID" readonly="readonly"/>
								</div>
							</td>
							<th scope="row"><spring:message code='epms.repair.request.part' /></th><!-- 요청부서 -->
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="hidden" class="inp-comm" id="REQUEST_DIV" name="REQUEST_DIV"/>
									<input type="text" class="inp-comm" id="REQUEST_DIV_NAME" name="REQUEST_DIV_NAME" readonly="readonly"/>
								</div>
							</td>
						</tr>
						<tr>
							<th scope="row"><spring:message code='epms.system.name' /></th><!-- 설비명 -->
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="text" class="inp-comm" id="EQUIPMENT_NAME" name="EQUIPMENT_NAME" readonly="readonly"/>
								</div>
							</td>
							<th scope="row"><spring:message code='epms.repair.request' /></th><!-- 요청자 -->
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="hidden" class="inp-comm" id="REQUEST_MEMBER" name="REQUEST_MEMBER"/>
									<input type="text" class="inp-comm" id="REQUEST_MEMBER_NAME" name="REQUEST_MEMBER_NAME" readonly="readonly"/>
								</div>
							</td>
						</tr>
						<tr>
							<th scope="row"><spring:message code='epms.request.content' /></th><!-- 고장내용 -->
							<td colspan="3">
								<div class="txt-wrap readonly">
									<div class="txtArea">
										<textarea class="inp-comm" cols="" rows="5" title="고장내용" id="REQUEST_DESCRIPTION" name="REQUEST_DESCRIPTION" readonly="readonly"></textarea>
									</div>
								</div>
							</td>
						</tr>
						<tr id="trAPPR">
							<th scope="row"><spring:message code='epms.appr.date' /></th><!-- 결재일 -->
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="text" class="inp-comm" id="DATE_APPR" name="DATE_APPR" readonly="readonly"/>
								</div>
							</td>
							<th scope="row"><spring:message code='epms.appr.member' /></th><!-- 결재자 -->
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="hidden" class="inp-comm" id="APPR_MEMBER" name="APPR_MEMBER"/>
									<input type="text" class="inp-comm" id="APPR_MEMBER_NAME" name="APPR_MEMBER_NAME" readonly="readonly"/>
								</div>
							</td>
						</tr>
						<tr id="trCANCEL">
							<th id="thCANCEL" scope="row"><spring:message code='epms.repair.cancel.date' /></th><!-- 정비취소일 -->
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="text" class="inp-comm" id="DATE_CANCEL" name="DATE_CANCEL" readonly="readonly"/>
								</div>
							</td>
							<th scope="row"><spring:message code='epms.repair.cancel.member' /></th><!-- 취소등록자 -->
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="hidden" class="inp-comm" id="CANCEL_MEMBER" name="CANCEL_MEMBER"/>
									<input type="text" class="inp-comm" id="CANCEL_MEMBER_NAME" name="CANCEL_MEMBER_NAME" readonly="readonly"/>
								</div>
							</td>
						</tr>
						<tr id="trREJECT_DESCRIPTION">
							<th scope="row"><spring:message code='epms.reject.reason' /></th><!-- 반려사유 -->
							<td colspan="3">
								<div class="txt-wrap readonly">
									<div class="txtArea">
										<textarea class="inp-comm" cols="" rows="3" title="반려사유" id="REJECT_DESCRIPTION" name="REJECT_DESCRIPTION" readonly="readonly"></textarea>
									</div>
								</div>
							</td>
						</tr>
						<tr id="trCANCEL_DESCRIPTION">
							<th scope="row"><spring:message code='epms.cancel.reason' /></th><!-- 취소사유 -->
							<td colspan="3">
								<div class="txt-wrap readonly">
									<div class="txtArea">
										<textarea class="inp-comm" cols="" rows="3" title="취소사유" id="CANCEL_DESCRIPTION" name="CANCEL_DESCRIPTION" readonly="readonly"></textarea>
									</div>
								</div>
							</td>
						</tr>
						<tr id="fileData">
							<th scope="row"><spring:message code='epms.attach' /></th><!-- 첨부파일 -->
							<td colspan="3">
								<div id="fileNotNull" style="display:none;">
									<div class="bxType01 fileWrap previewFileLst">
										<div class="fileList type02" id="viewFrame">
											<div class="fileDownLst" id="canvas2">
												
											</div>
										</div>
									</div>
										
									<div id="previewImg" class="wd-per-100">	
										<div class="previewFile mgn-t-15">
										<h3 class="tit-cont preview" id="previewTitle">이미지 미리보기</h3>
										<ul style="margin-top:5px;">
											<div id="detailFileList0" class="fileDownLst">
												
											</div>
										</ul>
										</div>
									</div>
								</div>
								<div class="f-l wd-per-100 mgn-t-5" id="fileNull">
									※ 등록된 첨부파일이 없습니다.
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<iframe id="fileDownFrame" style="width:0px;height:0px;border:0px"></iframe>

</div>

<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<div class="modal fade modalFocus" id="repairApprChangeFormModal" data-backdrop="static" data-keyboard="false"></div>	<%-- 파트이관 모달 --%>
<div class="modal fade modalFocus" id="repairApprRejectFormModal" data-backdrop="static" data-keyboard="false"></div>	<%-- 반려 모달 --%>
<div class="modal fade modalFocus" id="repairApprOkFormModal" data-backdrop="static" data-keyboard="false"></div>		<%-- 승인 모달 --%>
<div class="modal fade modalFocus" id="popFileManageForm"   data-backdrop="static" data-keyboard="false"></div> 		<%-- 고장사진 보기 모달 --%>
</body>

<script id="template1" type="text/template">
<div class="MultiFile-label">
	<span class="MultiFile-export"></span> 
	<span class="MultiFile-title">
	</span>
	<a href="#none" class="btnFileDwn icon download">다운로드</a>
</div>
</script>

<script id="template2" type="text/template">
	<li>
		<img/>
		<p></p>
	</li>
</script>
</html>
