<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">

var submitBool = true;

$(document).ready(function() {
		
	$("#popTitle").html("파트이관");
	
	<%-- '파트이관'버튼 --%>
	$('#popApprChangeForm_changeBtn').on('click', function(){
		if (submitBool) {
			fnChangeSave();
		} else {
			alert("등록중입니다. 잠시만 기다려 주세요.");
			return false;
		}
	});
});

<%-- '파트이관'처리 --%>
function fnChangeSave(){
	submitBool = false;
	if (!isNull_J($('#POP_PART'), '이관할 파트를 선택해주세요.')) {
		submitBool = true;
		return false;
	}
	
	if(confirm("파트이관 하시겠습니까?")){
		var modalName = "repairApprChangeFormModal";
		fnApprAjax($('#POP_PART').val(),"","","",modalName);
	}
}
	
</script>

<!-- Layer PopUp Start -->
<div class="modal-dialog root" style="width:450px;"> <!-- 원하는 width 값 입력 -->						
	 <div class="modal-content" style="height:200px;">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title">파트이관</h4>
		</div>
	 	<div class="modal-body">
           	<div class="modal-bodyIn">

				<div class="tb-wrap">
					<table class="tb-st">
						<caption class="screen-out">파트이관</caption>
						<colgroup>
							<col width="30%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row">파트</th>
								<td>
									<div class="sel-wrap">
										<select title="파트" id="POP_PART" name="POP_PART">
											<option value="">선택하세요.</option>
											<c:forEach var="item" items="${partList}" >
												<option value="${item.COMCD}" <c:if test="${param.PART eq item.COMCD}">selected</c:if>>${item.COMCD_NM}</option>
											</c:forEach>
										</select>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>	
		<div class="modal-footer hasFooter">
			<a href="#none" id="cancelBtn" class="btn comm st02" data-dismiss="modal">취소</a>
			<a href="#none" id="popApprChangeForm_changeBtn" class="btn comm st01">파트이관</a>
		</div>
	</div>		
</div>
<!-- 좌측 : e -->
<!-- Layer PopUp End -->