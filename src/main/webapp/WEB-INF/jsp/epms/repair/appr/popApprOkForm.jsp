<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">

var submitBool = true;

$(document).ready(function() {
	
	<%-- DatePicker 설정 --%>
	$( ".datePic" ).datepicker({
	   dateFormat   : "yy-mm-dd",
       prevText     : "click for previous months" ,
       nextText     : "click for next months" ,
       showOtherMonths   : true ,
       selectOtherMonths : false
	});
	
	<%-- 승인버튼 --%>
	$('#popApprOkBtn').on('click', function(){
		if (submitBool) {
			fnApprOkSave();
		} else {
			alert("등록중입니다. 잠시만 기다려 주세요.");
			return false;
		}
	});
	
});

<%-- 승인처리 --%>
function fnApprOkSave(){
	submitBool = false;
		
	if(confirm("승인 하시겠습니까?")){
		var modalName = "repairApprOkFormModal";
		fnApprAjax("","",$('#POP_REPAIR_MEMBER').val(),$('#DTAE_PLAN').val(),modalName);
	}
}


	
</script>

<!-- Layer PopUp Start -->
<div class="modal-dialog root" style="width:450px;"> <%-- 원하는 width 값 입력 --%>			
	 <div class="modal-content" style="height:220px;"> <%-- 원하는 height 값 입력 --%>
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title">승인</h4>
		</div>
	 	<div class="modal-body">
           	<div class="modal-bodyIn">

				<div class="tb-wrap">
					<table class="tb-st">
						<caption class="screen-out">승인</caption>
							<colgroup>
								<col width="30%" />
								<col width="*" />
							</colgroup>
							<tbody>								
								<tr>
									<th scope="row">정비계획일</th>
									<td>
										<input type="hidden" class="inp-comm datePic inpCal" readonly="readonly" id="POP_DTAE_PLAN" name="POP_DTAE_PLAN" title="정비계획일">
										<input type="text" class="inp-comm datePic inpCal" readonly="readonly" id="DTAE_PLAN" name="DTAE_PLAN" title="정비계획일" value="${today}">
									</td>
								</tr>
								<tr>
									<th scope="row">담당자</th>
									<td>
										<div class="sel-wrap">
											<select title="담당자" id="POP_REPAIR_MEMBER" name="POP_REPAIR_MEMBER">
												<option value="">담당자 미배정</option>
												<c:forEach var="item" items="${REPAIR_MEMBER_LIST}">
													<option value="${item.USER_ID}" <c:if test="${userId eq item.USER_ID}">selected</c:if>>${item.NAME}</option>
												</c:forEach>
											</select>
										</div>
									</td>
								</tr>
							</tbody>
					</table>
				</div>
			</div>
		</div>	
		<div class="modal-footer hasFooter">
			 <a href="#none" id="cancelBtn" class="btn comm st02" data-dismiss="modal">취소</a>
			<a href="#none" id="popApprOkBtn" class="btn comm st01">승인</a>
		</div>
	</div>		
</div>
<!-- 좌측 : e -->
<!-- Layer PopUp End -->