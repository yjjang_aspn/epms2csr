<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: popRepairRequestForm.jsp
	Description : 고장관리 > 고장요청 레이어 팝업
	author		: 김영환
	since		: 2018.04.04
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.04	 김영환		최초 생성

--%>
<%@ include file="/com/codeConstants.jsp"%>
<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){
	
	<%-- ///////////////////////////////////////// --%>
	<%-- //// 접속 권한별 정비원 출력 리스트 정리 시작 //// --%>
	<%-- ///////////////////////////////////////// --%>
	
	<%-- 최초 접속 시 정비원 선택창 HIDE --%>
	$("#workTR").hide();
	
	<%-- 일반 사용자일 경우 --%>
	if("${AUTH}" == 'AUTH01'){
		
		if("${PART}" != ""){
			<%-- 최초 접속 시 본인 파트 설정 --%>
			$("#PART").val("${PART}");
		}
		
		<%-- 파트구분 상관없이 hide --%>
		$("#workTR").hide();
		
	<%-- 정비원일 경우 --%>
	} else if("${AUTH}" == 'AUTH03'){
		<%-- 파트 통합일 경우 --%>
		if("${REMARKS}" == '02'){
			
			if("${PART}" != ""){
				<%-- 최초 접속 시 본인 파트 설정 --%>
				$("#PART").val("${PART}");
			}
			
			<%-- 권한별 정비원 목록 --%>
			fnGetWorkMemberList("");
			
			$("#workTR").show();
			
		<%-- 파트 구분일 경우 --%>
		} else {
			
			if("${PART}" != ""){
				<%-- 최초 접속 시 본인 파트 설정 --%>
				$("#PART").val("${PART}");
			}
			
			<%-- 권한별 정비원 목록 --%>
			fnGetWorkMemberList("${PART}");
			
			$("#workTR").show();
			
		}
	<%-- 팀장일 경우 --%>
	} else {
		<%-- 파트 통합일 경우 --%>
		if("${REMARKS}" == '02'){
			$("#workTR").show();
			
			<%-- 권한별 정비원 목록 --%>
			fnGetWorkMemberList("");
			
		<%-- 파트 구분일 경우 --%>
		} else {
			var html = "<select id='MANAGER' name='MANAGER'>"
					  +  "<option value='00'>파트를 선택하세요.</option>"
					  + "</select>";
			
			$("#workSelect").html(html);
			$("#workTR").show();
		}
	}
	
	<%-- 파트정보가 바뀔 경우 권한별 입력제한 --%>
	$( "#PART" ).change(function() {
		<%-- 파트 구분일 경우 --%>
		if("${REMARKS}" == '01'){
			
			<%-- 정비원일 경우 --%>
			if("${AUTH}" == 'AUTH03'){
				
				<%-- 본인이 소속한 파트가 아니라면 --%>
				if( $("#PART").val() != "${PART}" ){
					$("#workTR").hide();
				} else {
					$("#workTR").show();
					
					<%-- 권한별 정비원 목록 --%>
					fnGetWorkMemberList($("#PART").val());
				}
			
			<%-- 파트장일 경우 --%>
			} else if("${AUTH}" == 'AUTH02'){
				
				<%-- 권한별 정비원 목록 --%>
				fnGetWorkMemberList($("#PART").val());
			}
		}
		
	});
	
	<%-- ///////////////////////////////////////// --%>
	<%-- ////  접속 권한별 정비원 출력 리스트 정리 끝  //// --%>
	<%-- ///////////////////////////////////////// --%>
		
	<%-- 등록버튼 --%>
	$('#reqeustBtn').on('click', function() {
		if (submitBool) {
			fnRegValidate();
		} else {
			alert("등록중입니다. 잠시만 기다려 주세요.");
			return false;
		}
	});
	
	<%-- 취소버튼 --%>
	$('#cancelBtn').on('click', function() {
		fnModalToggle('popRepairRequestForm');
	});
	
	<%-- 첨부파일 관련 --%>
	for(var i=0; i<1; i++){				<%-- 파일 영역 추가를 고려하여 개발 --%>
		$('#attachFile').append('<input type="file" class="ksUpload" multiple id="fileList" name="fileList">');	
		
		var fileType = 'jpeg |gif |jpg |png |bmp '
					 + '|avi |wmv |mpeg |mpg |mkv |mp4 |tp |ts |asf |asx |flv |mov |3gp '
					 + '|mp3 |ogg |wma |wav |au |rm |mid |flac |m4a |amr ';
	
		$('#fileList').MultiFile({
			   accept :  fileType
			,	list   : '#detailFileList'+i
			,	max    : 5
			,	STRING : {
					remove: '<img src="/images/com/web/btn_remove.png">'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
					text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" />',
					idx	  : i,
				},
		});
		
		<%-- 첨부한 파일의 출력 순서를 자동으로 샛팅 하기 위한 스크립트 - 공통 스크립트로 빠지면 안됨 .개별 항목으로 처리 필수 --%> 
		$('.MultiFile-wrap').change(function(){
			var fileExpIndex = $(this).closest('.fileWrap').find('.fileList > div').children('div:last-child');
			var	fileExp = fileExpIndex.find('.fileExp');
			fileExp.val(fileExpIndex.index() + 1);
		});
	}
});

<%-- 정비요청등록 --%>
function fnRegValidate() {

	submitBool = false;

	if (!isNull_J($('#LOCATION'), " <spring:message code='epms.location' /> 정보가 입력되어 있지 않습니다. " )) {  // 위치(PLANT)
		submitBool = true;
		return false;
	}
	if (!isNull_J($('#CATEGORY'), " <spring:message code='epms.category' /> 정보가 입력되어 있지 않습니다. " )) {  // 라인
		submitBool = true;
		return false;
	}
	if (!isNull_J($('#EQUIPMENT'), " <spring:message code='epms.system.id' /> 정보가 입력되어 있지 않습니다. " )) {  // 설비ID
		submitBool = true;
		return false;
	}
	if (!isNull_J($('#EQUIPMENT_UID'), " <spring:message code='epms.system.uid' /> 정보가 입력되어 있지 않습니다. " )) {  // 설비코드
		submitBool = true;
		return false;
	}
	if (!isNull_J($('#EQUIPMENT_NAME'), " <spring:message code='epms.system.name' /> 정보가 입력되어 있지 않습니다. " )) {  // 설비명
		submitBool = true;
		return false;
	}
	if (!isNull_J($('#PART'), " <spring:message code='epms.repair.part' />를 선택해주세요. " )) {  // 처리파트
		submitBool = true;
		return false;
	}
	if (!isNull_J($('#REQUEST_DESCRIPTION'), " <spring:message code='epms.request.content' />을 입력해주세요. " )) {   // 고장내용
		submitBool = true;
		return false;
	}
	
	
	var fileCnt = $("input[name=fileGb]").length;
	
	if (fileCnt > 0) {
		$('#attachExistYn').val("Y");
	} else {
		$('#attachExistYn').val("N");
	}
	
	
	var form = new FormData(document.getElementById('repairRequestForm'));
	
	// 정비요청
	if(confirm(" <spring:message code='epms.work.request' /> 하시겠습니까?")){  // 요청
	
		$.ajax({
			type : 'POST',
			url : '/epms/repair/request/popRepairRequestInsert.do',
			dataType : 'json',
			async : false,
			data : form,
			processData : false,
			contentType : false,
			success : function(json) {
				if(json.REPAIR_REQUEST != '' || json.REPAIR_REQUEST != null){
					fnReload();
					submitBool = true;
				}else{
					alert("처리 도중 오류가 발생하였습니다.\n시스템 관리자에게 문의 바랍니다.");
					submitBool = true;
					return;
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
				submitBool = true;
				loadEnd();
			}
		});
		
	}
	// if(confirm("요청 하시겠습니까?"))
	
}

<%-- 권한별 정비요청 정비원목록 조회 --%>
function fnGetWorkMemberList(part){
	$.ajax({
		type : 'POST',
		url : '/epms/repair/request/selectWorkMemberList.do',
		dataType : 'json',
		data : { PART : part, LOCATION : focusedRow.LOCATION, SUB_ROOT : $('#SUB_ROOT').val(), REMARKS : $('#REMARKS').val() },
		success : function(json) {
			if(json.length > 0){
				var html = "<select id='MANAGER' name='MANAGER'>"
						  + "<option value='00'>담당자 미배정</option>";
				for(var i=0; i<json.length; i++){
					if("${AUTH}" == 'AUTH03' && "${userId}" == json[i].USER_ID){
						html += "<option value='"+json[i].USER_ID+"' selected>"+json[i].NAME+"</option>";
					} else {
						html += "<option value='"+json[i].USER_ID+"'>"+json[i].NAME+"</option>";	
					}
				}
				html += "</select>";
				
				$("#workSelect").html(html);
			}else{
				var html = "<select id='MANAGER' name='MANAGER'>"
					  +     	"<option value='00'>담당자 미배정</option>"
					  +	    "</select>";
				$("#workSelect").html(html);
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
			loadEnd();
		}
	});
}
</script>

<div class="modal-dialog root wd-per-40">	
	 <div class="modal-content hgt-per-80" style="min-height:540px;">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title"><spring:message code='epms.work.request' /></h4><!-- 정비요청 -->
		</div>
	 	<div class="modal-body">
           	<div class="modal-bodyIn">
				<form id="repairRequestForm" name="repairRequestForm"  method="post" enctype="multipart/form-data">	
					<input type="hidden" id="attachExistYn" name="attachExistYn" /> 
					<input type="hidden" id="MODULE" name="MODULE" value="111" />
					<input type="hidden" id="REQUEST_TYPE" name="REQUEST_TYPE" value="01"/>
					<input type="hidden" id="REMARKS" name="REMARKS" value="${equipmentDtl.REMARKS}"/>
					
					<div class="tb-wrap">
						<table class="tb-st">
							<caption class="screen-out"><spring:message code='epms.work.request' /></caption><!-- 정비요청 -->
							<colgroup>
								<col width="30%" />
								<col width="*" />
							</colgroup>
							<tbody>				
								<tr>
									<th><spring:message code='epms.location' /></th><!-- 위치 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="hidden" class="inp-comm" title="위치" readonly="readonly" id="LOCATION" name="LOCATION" value="${equipmentDtl.LOCATION}" />
											<input type="text" class="inp-comm" title="위치명" id="LOCATION_NAME" name="LOCATION_NAME" readonly="readonly" value="${equipmentDtl.LOCATION_NAME}"/>
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.category' /></th><!-- 라인 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="hidden" class="inp-comm" readonly="readonly" title="기능위치" id="LINE" name="LINE" value="${equipmentDtl.LINE}"/>
											<input type="text" class="inp-comm" title="라인" id="LINE_NAME" name="LINE_NAME" readonly="readonly" value="${equipmentDtl.LINE_NAME}"/>
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.system.uid' /></th><!-- 설비코드 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="hidden" class="inp-comm" readonly="readonly" title="설비ID" id="EQUIPMENT" name="EQUIPMENT" value="${equipmentDtl.EQUIPMENT}"/>
											<input type="text" class="inp-comm" title="설비코드" id="EQUIPMENT_UID" name="EQUIPMENT_UID" readonly="readonly" value="${equipmentDtl.EQUIPMENT_UID}"/>
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.system.name' /></th><!-- 설비명 -->
									<td>
										<div class="inp-wrap readonly wd-per-100">
											<input type="text" class="inp-comm" title="설비명" id="EQUIPMENT_NAME" name="EQUIPMENT_NAME" readonly="readonly" value="${equipmentDtl.EQUIPMENT_NAME}"/>
										</div>
									</td>
								</tr>
								<tr id="urgencyTR">
									<th><spring:message code='epms.repair.urgency' /></th><!-- 긴급도 -->
									<td>
										<div class="tb-row">
											<span class="rdo-st">
												<input type="radio" name="URGENCY" id="URGENCY_01" value="01" checked >
												<label for="URGENCY_01" class="label">일반</label>
											</span>
											<span class="rdo-st mgn-l-50">
												<input type="radio" name="URGENCY" id="URGENCY_02" value="02">
												<label for="URGENCY_02">긴급</label>
											</span>
										</div>
<!-- 										<div class="sel-wrap"> -->
<!-- 											<select title="긴급도" id="URGENCY" name="URGENCY" > -->
<!-- 												<option value="01">일반</option> -->
<!-- 												<option value="02">긴급</option> -->
<!-- 											</select>											 -->
<!-- 										</div> -->
									</td>
								</tr>
								<tr id="partTR">
									<th><spring:message code='epms.repair.part' /></th><!-- 처리파트 -->
									<td>
										<div class="sel-wrap">
											<tag:combo codeGrp="PART" companyId="${ssCompanyId}" choose="Y"/>
										</div>
									</td>
								</tr>
								<tr id="workTR">
									<th><spring:message code='epms.repair.manager' /></th><!-- 담당자 -->
									<td>
										<div class="sel-wrap" id="workSelect">
										</div>
									</td>
								</tr>
								<tr>
									<th><spring:message code='epms.request.content' /></th><!-- 고장내용 -->
									<td>
										<div class="txt-wrap">
											<textarea cols="" rows="3" title="고장내용" id="REQUEST_DESCRIPTION" name="REQUEST_DESCRIPTION"></textarea>
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row"><spring:message code='epms.attachfile' /></th><!-- 첨부파일 -->
									<td>
										<div class="fileWrap">
											<div id="attachFile" class="filebox"></div>
											<div class="fileList">
												<div id="detailFileList0" class="fileDownLst">
												</div>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</form>
				
			</div>
		</div>	
		<div class="modal-footer hasFooter">
			<a href="#none" class="btn comm st02" data-dismiss="modal" id="cancelBtn">취소</a>
			<a href="#none" class="btn comm st01" id="reqeustBtn"><spring:message code='epms.work.request' /></a> <!-- 정비요청 -->
		</div>
	</div>		
</div>

