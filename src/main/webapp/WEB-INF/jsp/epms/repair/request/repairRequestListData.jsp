<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
	Class Name	: repairRequestListLayout.jsp
	Description : 고장관리 > 고장등록 > 내 정비요청 현황 그리드 데이터
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		 수정자	       수정내용
	----------  --------	---------------------------
	2018.03.20	 김영환	       최초 생성

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I
			
			REPAIR_REQUEST			= "${item.REPAIR_REQUEST}"
			LOCATION				= "${item.LOCATION}"
			CATEGORY				= "${item.CATEGORY}"
			CATEGORY_NAME			= "${fn:replace(item.CATEGORY_NAME, '"', '&quot;')}"
			EQUIPMENT				= "${item.EQUIPMENT}"
			EQUIPMENT_UID			= "${item.EQUIPMENT_UID}"
			EQUIPMENT_NAME			= "${fn:replace(item.EQUIPMENT_NAME, '"', '&quot;')}"
			
			URGENCY					= "${item.URGENCY}"
			<c:if test="${item.URGENCY == '02'}">
				URGENCYClass="gridStatus4"
				Background="#FFDDDD"
			</c:if>
			REQUEST_TYPE			= "${item.REQUEST_TYPE}"
			REPAIR_REQUEST_ORG		= "${item.REPAIR_REQUEST_ORG}"
			APPR_STATUS				= "${item.APPR_STATUS}"
			<c:choose>
				<c:when test="${item.APPR_STATUS == '02'}">
				APPR_STATUSClass="gridStatus3"
				</c:when>
				<c:when test="${item.APPR_STATUS == '03'}">
				APPR_STATUSClass="gridStatus4"
				</c:when>
				<c:when test="${item.APPR_STATUS == '04'}">
				APPR_STATUSClass="gridStatus4"
				</c:when>
				<c:when test="${item.APPR_STATUS == '09'}">
				APPR_STATUSClass="gridStatus4"
				</c:when>
			</c:choose>
			
			REQUEST_DESCRIPTION		= "${fn:replace(item.REQUEST_DESCRIPTION, '"', '&quot;')}"
			ATTACH_GRP_NO			= "${item.ATTACH_GRP_NO}"
			
			REQUEST_DIV				= "${item.REQUEST_DIV}"
			REQUEST_DIV_NAME		= "${item.REQUEST_DIV_NAME}"
			DATE_REQUEST			= "${item.DATE_REQUEST}"
			REQUEST_MEMBER			= "${item.REQUEST_MEMBER}"
			REQUEST_MEMBER_NAME		= "${item.REQUEST_MEMBER_NAME}"
			DATE_APPR				= "${item.DATE_APPR}"
			APPR_MEMBER				= "${item.APPR_MEMBER}"
			APPR_MEMBER_NAME		= "${item.APPR_MEMBER_NAME}"
			PART					= "${item.PART}"
			REJECT_DESCRIPTION		= "${fn:replace(item.REJECT_DESCRIPTION, '"', '&quot;')}"
			
			
			
			REPAIR_RESULT			= "${item.REPAIR_RESULT}"
			REPAIR_TYPE				= "${item.REPAIR_TYPE}"
			REPAIR_STATUS			= "${item.REPAIR_STATUS}"
			<c:if test="${item.REPAIR_STATUS == '03'}">
				REPAIR_STATUSClass="gridStatus3"
			</c:if>
			DATE_REPAIR				= "${item.DATE_REPAIR}"
			MANAGER					= "${item.MANAGER}"
			MANAGER_NAME			= "${item.MANAGER_NAME}"
			
			<%-- 담당자 배정이 본인인 경우를 제외한 나머지 경우는 조회 페이지 이동 --%>
			<c:choose>
				<c:when test='${item.REPAIR_STATUS eq "02" && item.MANAGER eq userId}'>
					detailSwitch="1"  detailIcon="/images/com/web/commnet_up5.gif" 
					detailOnClickSideIcon="fnOpenLayerWithParam('/epms/repair/result/popRepairResultRegForm.do?REPAIR_REQUEST='+Row.REPAIR_REQUEST+'&REPAIR_RESULT='+Row.REPAIR_RESULT+'&EQUIPMENT='+Row.EQUIPMENT,'popRepairResultRegForm');"
				</c:when>
				<c:otherwise>
					detailSwitch="1"  detailIcon="/images/com/web/commnet_up.gif" 
					detailOnClickSideIcon="fnOpenLayerWithParam('/epms/repair/result/popRepairResultForm.do?REPAIR_REQUEST='+Row.REPAIR_REQUEST+'&REPAIR_RESULT='+Row.REPAIR_RESULT+'&EQUIPMENT='+Row.EQUIPMENT,'popRepairResultForm');"
				</c:otherwise>
			</c:choose>		
			
			REPAIR_ANALYSIS			= "${item.REPAIR_ANALYSIS}"
			
			<c:if test="${item.APPR_STATUS eq '02' && item.REPAIR_STATUS eq '03' && item.REPAIR_TYPE eq '110'}">
				SAVE_YN					= "${item.SAVE_YN}"
				<c:if test="${item.SAVE_YN == 'Y'}">
					SAVE_YNClass="gridStatus3"
				</c:if>
				<c:if test="${item.SAVE_YN == 'N'}">
					SAVE_YNClass="gridStatus4"
				</c:if>
			</c:if>
			
			<c:if test="${item.APPR_STATUS eq '01' && item.REQUEST_TYPE ne '02'}">
				cancelSwitch="1"  cancelIcon="/images/com/web/commnet_up1.gif" 
				cancelOnClickSideIcon="fnCancel(Row.REPAIR_REQUEST, Row.REQUEST_TYPE);"
			</c:if>
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>