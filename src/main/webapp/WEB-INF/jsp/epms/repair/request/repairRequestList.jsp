<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld"                 %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: repairRequestList.jsp
	Description : 고장관리 > 고장등록 메인화면
	author		: 김영환
	since		: 2018.04.04
 
	<< 개정이력(Modification Information) >>
	수정일 		 수정자		수정내용
	----------  ------	---------------------------
	2018.04.04	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<style>
	.tb-st ul{margin:0; padding:0;}
	.tb-st ul>li{overflow:hidden; margin-top:5px;}
	.tb-st ul>li>input{float:left;}
	
	.btnRemove{display:inline-block; height:28px; line-height:28px; padding:0 10px; background:#6eb4e5; color:#ffffff;}
</style>
<script type="text/javascript">

<%-- 전역변수 --%>
var focusedRow = null;		<%-- Focus Row : [라인] --%>
var focusedRow2 = null;		<%-- Focus Row : [설비] --%>
var focusedRow3 = null;		<%-- Focus Row : [정비이력] --%>
var focusedRow4 = null;		<%-- Focus Row : [내 정비요청 현황] --%>
var callLoadingYn = true;	<%-- 로딩바 호출여부 --%>
var submitBool = true;		<%-- 유효성검사 --%>
var maxFileCnt = 30;		<%-- 파일 갯수 최대값 --%>


$(document).ready(function(){
	$(window).resize(function(){	
		
		<%-- 하단 그리드 높이값 --%>
		var bottomH = $(".bottom-contents").outerHeight();
		$(".bottom-contents").find(".panel-body").height(bottomH-13);
	});
	$(window).trigger('resize');
	
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function (e) {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>
	});
	
});


<%-- 정비요청버튼 --%>
function fnReg(){
	
	var catVal = $('#CATEGORY').val() ;
	
	console.log('# repairRequestList.fnReg() called ... $(#CATEGORY).val(' + catVal + ') ');
	
	if ( (catVal == undefined || typeof catVal == 'undefined' ) || catVal  == null || catVal == '' ) {
		
		alert("<spring:message code='epms.category' />을 선택해주세요.");  // 라인
		return;
	}
	
	var url = "/epms/repair/request/popRepairRequestForm.do"
			  + "?LOCATION=" + $('#LOCATION').val()
			  + "&CATEGORY=" + $('#CATEGORY').val()
			  + "&EQUIPMENT=" + $('#EQUIPMENT').val();
	
		url += "&" + new Date().getTime() ;
		
		console.log('# popRepairRequestForm url (' + url + ') ');
		
		
		/**
		 * 작업요청 팝업 화면 (popRepairRequestForm)
		 * popRepairRequestForm URL parameters
		 * 
		 * @param { LOCATION  } 위치코드
		 * @param { CATEGORY  } 카테고리 
		 * @param { EQUIPMENT } 설비코드
		 * @returns 
		 */
		fnOpenLayerWithParam( url , 'popRepairRequestForm' );
}


<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "CategoryList") {	<%-- 1. [라인] 클릭시 --%>
		
		<%-- 1-1. 포커스 시작 --%>
		if(focusedRow != null){
			grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		focusedRow = row;
		
		<%-- 1-2. 설비 목록 조회 --%>
		$('#TREE').val(row.TREE);
		$('#CATEGORY_TYPE').val(row.CATEGORY_TYPE);
		var treeArray = $('#TREE').val().split('$');
		$('#LOCATION').val(treeArray[1]);
		$('#CATEGORY').val(row.CATEGORY);
		$('#CATEGORY_NAME').val(row.CATEGORY_NAME);
		$('#SUB_ROOT').val(row.SUB_ROOT);
		$('#EQUIPMENT').val('');
		$('#equipmentListTitle').html("<spring:message code='epms.system' /> : " + "[ " + row.CATEGORY_NAME + " ]");
		$('#equipmentHistoryListTitle').html("<spring:message code='epms.work.history' /> : " + "[ " + row.CATEGORY_NAME + " ]");
		
		getEquipmentList();
		getEquipmentHistoryList();
	}
	else if(grid.id == "EquipmentList3"){	<%-- 2. [설비] 클릭시 --%>
		
		<%-- 2-1. 포커스 시작 --%>
		if(focusedRow2 != null){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		focusedRow2 = row;
		
		<%-- 2-2. 정비이력 목록 조회 --%>
		$('#EQUIPMENT').val(row.EQUIPMENT);
		$('#EQUIPMENT_NAME').val(row.EQUIPMENT_NAME);
		$('#equipmentHistoryListTitle').html("<spring:message code='epms.work.history' /> : " + "[ " + row.EQUIPMENT_NAME + " ]");
		
		getEquipmentHistoryList();
	}
	else if(grid.id == "RepairHistoryList") {	<%-- 3. [정비이력] 클릭시 --%>
	
		<%-- 3-1. 포커스  --%>
		if(focusedRow3 != null){
			grid.SetAttribute(focusedRow3,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow3 = row;
		
	}
	else if(grid.id == "RepairRequestList") {	<%-- 4. [내 정비요청 현황] 클릭시 --%>
	
		<%-- 4-1. 포커스  --%>
		if(focusedRow4 != null){
			grid.SetAttribute(focusedRow4,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow4 = row;
		
	}
}

<%-- 선택된 라인에 해당되는 설비 조회 --%>
function getEquipmentList(){
	Grids.EquipmentList3.Source.Data.Url = "/epms/equipment/master/equipmentMasterListData.do?TREE=" + $('#TREE').val()
										 + "&CATEGORY_TYPE=LINE" 
										 + "&editYn=N";
	Grids.EquipmentList3.ReloadBody();	
}

<%-- 선택된 설비에 해당되는 설비이력 조회 --%>
function getEquipmentHistoryList(){
	Grids.RepairHistoryList.Source.Data.Url = "/epms/repair/request/repairHistoryListData.do?EQUIPMENT=" + $('#EQUIPMENT').val()
											+ "&TREE=" + $('#TREE').val();
	Grids.RepairHistoryList.ReloadBody();	
}

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source, data, func){
	
	if(grid.id == "EquipmentList3"){			<%-- [설비] 목록 조회시 --%>
		if(data != ""){
			callLoadingBar();
			callLoadingYn = false;
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "EquipmentList3"){
					getEquipmentBomList();
				}
			}
		}
	}
	else if(grid.id == "RepairHistoryList"){	<%-- [정비이력] 목록 조회시 --%>
		
	    if(data != ""){
			if(callLoadingYn == true){
				callLoadingBar();
				callLoadingYn = false;
			}
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "RepairHistoryList"){
					setTimeout($.unblockUI, 100);
					callLoadingYn = true;
				}
			}
			// Grids.OnDataReceive
		}
	    // if(data != "")
	    	
	}
	
}


<%-- 팝업창 고장등록 이후 새로고침 --%>
function fnReload(){  // for popRepairRequestForm , popRepairResultRegForm
	
	var a     =   arguments ;
	var msg   = ( a.length > 0  ?  a[0]  :  ''  ) ; 
	var modal = ( a.length > 1  ?  a[1]  :  ''  ) ; 
	
	console.log('# repairRequestList.fnReload( ' + msg + ' , ' + modal + ' ) called ...');
	
	alert("정상적으로 등록 되었습니다.");

	// for popRepairResultRegForm
	if ( modal != 'popRepairResultRegForm' ) {
		
		fnModalToggle('popRepairRequestForm');  // modalpopup.js
	}
	
	// modal focus 제거
 	//$('#popRepairRequestForm').modal('toggle');
	
	Grids.RepairRequestList.ReloadBody();
}


<%-- 고장등록 취소 --%>
function fnCancel(REPAIR_REQUEST, REQUEST_TYPE){
	
	if(REQUEST_TYPE == '${EPMS_REQUEST_TYPE_PREVENTIVE}'){
		alert("예방정비는 취소하실 수 없습니다.");
		return false;
	} else {
		// 정비요청
		if(confirm("요청건을 취소하시겠습니까?")){
			$.ajax({
				type : 'POST',
				url : '/epms/repair/request/repairRequestDelete.do',
				dataType : 'json',
				data : {REPAIR_REQUEST:REPAIR_REQUEST},
				success : function(json) {	
					if(json != '' && json != null){
						Grids.RepairRequestList.ReloadBody();	
						alert("요청건이 정상적으로 삭제되었습니다.");
					}
					else{
						alert("요청 취소에 실패하였습니다.");
					}
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
					loadEnd();
				}
			});
		}
	}
}


<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- 모달 호출 (Param 전달 및 그리드 호출) --%>
function fnOpenLayerWithGrid(url, name){
	$.ajax({
		type: 'POST',
		url: url,
		dataType: 'json',
		processData: false, 
		contentType:false,
		success: function (json) {
			
			if(name == 'popEquipmentHistoryList'){		<%-- 기기이력 팝업일 경우 --%>
			
				$("#"+name+"_EQUIPMENT").val(isEmpty(json.item.EQUIPMENT)==true ? '' :json.item.EQUIPMENT);
				$("#"+name+"_LOCATION_NAME").text(isEmpty(json.item.LOCATION_NAME)==true ? '' : "["+json.item.LOCATION_NAME+"]");
				$("#"+name+"_LINE_NAME").text(isEmpty(json.item.LINE_NAME)==true ? '' : "["+json.item.LINE_NAME+"]");
				$("#"+name+"_EQUIPMENT_NAME").text(isEmpty(json.item.EQUIPMENT_NAME)==true ? '' : "["+json.item.EQUIPMENT_NAME+"]");
				$("#"+name+"_EQUIPMENT_UID").text(isEmpty(json.item.EQUIPMENT_UID)==true ? '' : "["+json.item.EQUIPMENT_UID+"]");
				
				Grids.EquipmentHistoryList.Source.Data.Url = "/epms/equipment/history/equipmentHistoryListData.do?EQUIPMENT=" + $('#popEquipmentHistoryList_EQUIPMENT').val();
				Grids.EquipmentHistoryList.ReloadBody();
			}
			
			fnModalToggle(name);
		
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
			loadEnd();
		}
	});
}

</script>
</head>
<body>
<div id="contents">
        
		<input type="hidden" id="TREE" name="TREE"/>						<%-- 구조체 --%>
		<input type="hidden" id="CATEGORY_TYPE" name="CATEGORY_TYPE"/>		<%-- 카테고리 유형 --%>
		<input type="hidden" id="CATEGORY" name="CATEGORY"/>				<%-- 카테고리ID --%>
		<input type="hidden" id="CATEGORY_NAME" name="CATEGORY_NAME"/>		<%-- 카테고리명 --%>
		<input type="hidden" id="LOCATION" name="LOCATION"/>				<%-- 위치 --%>
		<input type="hidden" id="EQUIPMENT" name="EQUIPMENT"/>				<%-- 설비ID --%>
		<input type="hidden" id="EQUIPMENT_NAME" name="EQUIPMENT_NAME"/>	<%-- 설비명 --%>
		<input type="hidden" id="SUB_ROOT" name="SUB_ROOT"/>				<%-- SUB_ROOT --%>
		
		<div class="panel-wrap-top" style="width:100%;">
			<div class="fl-box hgt-per-100" style="width:20%;">
				<h5 class="panel-tit"><spring:message code='epms.category' /></h5><!-- 라인 -->
				<div class="panel-body">
					<div id="categoryList">
						<bdo	Debug	  ="Error"
								Data_Url  ="/sys/category/categoryData.do?CATEGORY_TYPE=LINE"
								Layout_Url="/sys/category/categoryNotEditLayout.do?CATEGORY_TYPE=LINE"
						>
						</bdo>
					</div>
				</div>
			</div>
			<div class="fl-box hgt-per-100" style="width:30%;">
				<h5 class="panel-tit mgn-l-10" id="equipmentListTitle"><spring:message code='epms.system' /></h5><!-- 설비 -->
				<div class="panel-body mgn-l-10">
					<div id="EquipmentList3">
						<bdo	Debug="Error"
								Data_Url=""	
								Layout_Url="/epms/repair/request/repairRequestEquipmentLayout.do"
								Export_Data="data" Export_Type="xls"
								Export_Url="/sys/comm/exportGridData.jsp?File=설비목록.xls&dataName=data"
						>
						</bdo>
					</div>
				</div>
			</div>
			<div class="fl-box hgt-per-100" style="width:50%;">
				<h5 class="panel-tit mgn-l-10" id="equipmentHistoryListTitle"><spring:message code='epms.work.history' /></h5><!-- 정비이력 -->
				<div class="panel-body mgn-l-10">
					<div id="RepairHistoryList">
						<bdo	Debug="Error"
								Layout_Url="/epms/repair/request/repairHistoryListLayout.do"
								Export_Data="data" Export_Type="xls"
								Export_Url="/sys/comm/exportGridData.jsp?File=정비이력목록.xls&dataName=data"
						>
						</bdo>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-wrap-btm" style="width:100%;">
			<h5 class="panel-tit mgn-t-10"><spring:message code='epms.myRequest.status' /></h5><!-- 내 정비요청 현황 -->
			<div class="panel-body">
				<div id="repairRequestList">
					<bdo	Debug="Error"
							Data_Url="/epms/repair/request/repairRequestListData.do"
							Layout_Url="/epms/repair/request/repairRequestListLayout.do"
							Export_Data="data" Export_Type="xls"
							Export_Url="/sys/comm/exportGridData.jsp?File=정비요청현황 목록.xls&dataName=data"
					>
					</bdo>
				</div>
			</div>
		</div>

</div>


<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<%-- 첨부파일 조회/수정 --%>
<div class="modal fade modalFocus" id="popFileManageForm"   data-backdrop="static" data-keyboard="false"></div>	

<%-- 기기이력 --%>
<%@ include file="/WEB-INF/jsp/epms/equipment/history/popEquipmentHistoryList.jsp"%>

<%-- 정비요청 --%>
<div class="modal fade modalFocus" id="popRepairRequestForm" data-backdrop="static" data-keyboard="false"></div>		

<%-- 정비실적등록 --%>
<div class="modal fade modalFocus" id="popRepairResultRegForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 정비실적조회 --%>
<div class="modal fade modalFocus" id="popRepairResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 타파트 요청 --%>
<div class="modal fade modalFocus" id="popRequestPartForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 고장원인분석 등록 --%>
<div class="modal fade modalFocus" id="popRepairAnalysisForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 정비원 검색 --%>
<div class="modal fade modalFocus" id="popWorkerList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-25">		
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">
                    <spring:message code='epms.worker' /> <spring:message code='epms.search' />
                </h4><!-- 정비원 검색 -->
			</div>
	 	<div class="modal-body" >
           	<div class="modal-bodyIn">
           		<div class="modal-section">
					<div class="fl-box wd-per-100">
						<div id="snsList">
							<bdo Debug="Error"
								 Data_Url=""
								 Layout_Url="/epms/repair/result/popWorkerListLayout.do"
							 >
							</bdo>
						</div>
					</div>
				</div> <%-- modal-section : e --%>
			</div> <%-- modal-bodyIn : e --%>
		</div> <%-- modal-body : e --%>
	</div> <%-- modal-content : e --%>
</div> <%-- modal : e --%>
</div>

<%-- 부품명 검색 --%>
<div class="modal fade modalFocus" id="popRepairMaterialList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-30">
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">
                    <spring:message code='epms.object.name' /> <spring:message code='epms.search' />
                </h4><!-- 부품명 검색 -->
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	           		<div class="modal-section">
						<div class="fl-box wd-per-100">
							<bdo	Debug="Error"
					 				Data_Url=""
									Layout_Url="/epms/repair/result/popRepairMaterialListLayout.do" 
							>
							</bdo>
						</div>
					</div> <%-- modal-section : e --%>
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>
