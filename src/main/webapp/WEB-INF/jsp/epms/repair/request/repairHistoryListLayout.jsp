<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: repairHistoryListLayout.jsp
	Description : 고장관리 > 고장등록 > 정비이력 그리드 레이아웃
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="RepairHistoryList"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		Filtering="1"				Dragging="0"
			Editing="1"				Deleting="0"		Selecting="0"		SuppressCfg="0"
			Paging="1"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="0"			PasteFocused="3"	CustomScroll="1"
	/>

	<Header	id="Header"	Align="center"
		EQUIPMENT				= "<spring:message code='epms.system.id'                />"    <%  // 설비ID     %>
		EQUIPMENT_UID			= "<spring:message code='epms.system.uid'               />"    <%  // 설비코드       %>
		EQUIPMENT_NAME			= "<spring:message code='epms.system.name'              />"    <%  // 설비명          %>
		REPAIR_RESULT			= "<spring:message code='epms.repair.id'                />"    <%  // 정비ID     %>
		REPAIR_TYPE				= "<spring:message code='epms.repair.type'              />"    <%  // 정비유형        %>
		PART					= "<spring:message code='epms.repair.part'              />"    <%  // 파트             %>
		MATERIAL_USED_NAME		= "<spring:message code='epms.use.object'               />"    <%  // 부품             %>
		TROUBLE_TYPE1			= "<spring:message code='epms.type.occurr'     />"    <%  // 고장유형        %>
		TROUBLE_TYPE2			= "<spring:message code='epms.type.action'     />"    <%  // 조치             %>
		REPAIR_DESCRIPTION		= "<spring:message code='epms.repair.content'           />"    <%  // 작업내용        %>
		TROUBLE_TIME1			= "<spring:message code='epms.occurr.time.start'     />"    <%  // 고장시작시간   %>
		TROUBLE_TIME2			= "<spring:message code='epms.occurr.time.end'     />"    <%  // 고장종료시간   %>
		MANAGER					= "<spring:message code='epms.manager.member.id'        />"    <%  // 담당자ID    %>
		MANAGER_NAME			= "<spring:message code='epms.manager.member'           />"    <%  // 담당자          %>
		                           
		detail					= "<spring:message code='epms.repair.perform.detail' />"    <%  // 실적상세     %>
	/>
	
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	<Pager Visible="0"/>
	
	<Cols>
		<C Name="EQUIPMENT"					Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="EQUIPMENT_UID"				Type="Text" Align="left"   Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT_NAME"			Type="Text" Align="left"   Visible="1" Width="180" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="REPAIR_RESULT"				Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="REPAIR_TYPE"				Type="Enum" Align="left"   Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="REPAIR_TYPE" companyId="${ssCompanyId}"/>/>
		<C Name="PART"						Type="Enum" Align="left"   Visible="1" Width="60"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="PART" companyId="${ssCompanyId}"/>/>
		<C Name="MATERIAL_USED_NAME"		Type="Text" Align="left"   Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="TROUBLE_TYPE1"				Type="Enum" Align="left"   Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="TROUBLE1"/>/>
		<C Name="TROUBLE_TYPE2"				Type="Enum" Align="left"   Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="TROUBLE2"/>/>
		<C Name="REPAIR_DESCRIPTION"		Type="Text" Align="left"   Visible="1" Width="200" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="TROUBLE_TIME1"				Type="Date" Align="center" Visible="1" Width="130" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd HH:mm"/>
		<C Name="TROUBLE_TIME2"				Type="Date" Align="center" Visible="1" Width="130" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd HH:mm"/>
		<C Name="MANAGER"					Type="Text" Align="left"   Visible="0" Width="70"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="MANAGER_NAME"				Type="Text" Align="left"   Visible="0" Width="70"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>	
	</Cols>
	
	<RightCols>
		<C Name="detail"					Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" />
	</RightCols>
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,항목,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1"
				ColumnsType="Button"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>