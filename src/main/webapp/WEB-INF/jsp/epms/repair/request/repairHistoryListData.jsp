<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
	Class Name	: repairHistoryListLayout.jsp
	Description : 고장관리 > 고장등록 > 정비이력 그리드 데이터
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I
			REPAIR_RESULT			= "${item.REPAIR_RESULT}"
			EQUIPMENT				= "${item.EQUIPMENT}"
			EQUIPMENT_UID			= "${item.EQUIPMENT_UID}"
			EQUIPMENT_NAME			= "${item.EQUIPMENT_NAME}"
			REPAIR_TYPE				= "${item.REPAIR_TYPE}"
			PART					= "${item.PART}"
			TROUBLE_TYPE1			= "${item.TROUBLE_TYPE1}"
			TROUBLE_TYPE2			= "${item.TROUBLE_TYPE2}"
			REPAIR_DESCRIPTION		= "${fn:replace(item.REPAIR_DESCRIPTION, '"', '&quot;')}"
			TROUBLE_TIME1			= "${item.TROUBLE_TIME1}"
			TROUBLE_TIME2			= "${item.TROUBLE_TIME2}"
			MANAGER					= "${item.MANAGER}"
			MANAGER_NAME			= "${item.MANAGER_NAME}"
			MATERIAL_USED_NAME		= "${item.MATERIAL_USED_NAME}"
			detailSwitch="1"  detailIcon="/images/com/web/commnet_up.gif" 
			detailOnClickSideIcon="fnOpenLayerWithParam('/epms/repair/result/popRepairResultForm.do?REPAIR_RESULT='+Row.REPAIR_RESULT+'&EQUIPMENT='+Row.EQUIPMENT+'&flag=repairHistory','popRepairResultForm');"
			
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>