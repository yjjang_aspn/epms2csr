<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: repairRequestListLayout.jsp
	Description : 고장관리 > 고장등록 > 내 정비요청 현황 그리드 레이아웃
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		 수정자	       수정내용
	----------  --------	---------------------------
	2018.03.20	 김영환	       최초 생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="RepairRequestList"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1"			SuppressCfg="0"	
			Editing="1"				Deleting="0"		Selecting="0"		Dragging="0"			
			Filtering="1"			StandardFilter="3"	ClearSelected="2"
			Paging="2"			  	PageLength="10"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="0"			PasteFocused="3"	
	/>

	<Header	id="Header"	Align="center"
		REPAIR_REQUEST			= "<spring:message code='epms.work.request.id' />"    <%  // 정비요청ID"       %>
		LOCATION				= "<spring:message code='epms.location'       />"    <%  // 위치"           %>
		CATEGORY				= "<spring:message code='epms.category.id'    />"    <%  // 라인ID"         %>
		CATEGORY_NAME			= "<spring:message code='epms.category'       />"    <%  // 라인"           %>
		EQUIPMENT				= "<spring:message code='epms.system.id'         />"    <%  // 설비ID"         %>
		EQUIPMENT_UID			= "<spring:message code='epms.system.uid'        />"    <%  // 설비코드"         %>
		EQUIPMENT_NAME			= "<spring:message code='epms.system.name'       />"    <%  // 설비명"          %>
		                          
		URGENCY					= "<spring:message code='epms.repair.urgency'            />"    <%  // 긴급도          %>
		PART					= "<spring:message code='epms.repair.part'               />"    <%  // 파트             %>
		REQUEST_TYPE			= "<spring:message code='epms.kind'                      />"    <%  // 구분             %>
		REPAIR_REQUEST_ORG		= "<spring:message code='epms.work.request.id.org' />"    <%  // 원정비요청ID %>
		APPR_STATUS				= "<spring:message code='epms.appr.status' />"                  <%  // 결재상태        %>
		DATE_REQUEST			= "<spring:message code='epms.request.date' />"                 <%  // 요청일          %>
		REQUEST_DESCRIPTION		= "<spring:message code='epms.request.content' />"              <%  // 고장내용       %>
		                          
		DATE_APPR				= "<spring:message code='epms.appr.date'          />"    <%  // 결재일          %>
		APPR_MEMBER				= "<spring:message code='epms.appr.member.id'     />"    <%  // 결재자ID    %>
		APPR_MEMBER_NAME		= "<spring:message code='epms.appr.member'        />"    <%  // 결재자          %>
		REJECT_DESCRIPTION		= "<spring:message code='epms.reject.reason'      />"    <%  // 반려사유        %>
		                           
		REQUEST_DIV				= "<spring:message code='epms.repair.request.part.id' />"    <%  // 요청부서ID"       %>
		REQUEST_DIV_NAME		= "<spring:message code='epms.repair.request.part.name' />"    <%  // 요청부서"         %>
		REQUEST_MEMBER			= "<spring:message code='epms.repair.request.id' />"    <%  // 요청자ID"        %>
		REQUEST_MEMBER_NAME		= "<spring:message code='epms.repair.request.name' />"    <%  // 요청자"          %>
		ATTACH_GRP_NO			= "<spring:message code='epms.attachfile'                    />"    <%  // 첨부파일 코드"      %>
		                           
		REPAIR					= "<spring:message code='epms.work.id'       />"    <%  // 정비ID      %>
		REPAIR_STATUS			= "<spring:message code='epms.repair.status'          />"    <%  // 정비상태         %>
		MANAGER					= "<spring:message code='epms.manager.member.id'      />"    <%  // 담당자ID     %>
		MANAGER_NAME			= "<spring:message code='epms.manager.member'         />"    <%  // 담당자            %>
		REPAIR_TYPE				= "<spring:message code='epms.repair.type'            />"    <%  // 정비유형          %>
		DATE_REPAIR				= "<spring:message code='epms.repair.date'            />"    <%  // 실적등록일        %>
		REPAIR_ANALYSIS			= "<spring:message code='epms.repair.cause.analysis.id' />"    <%  // 고장원인분석ID %>
		SAVE_YN					= "<spring:message code='epms.analysis.able'   />"    <%  // 분석여부           %>
		detail					= "<spring:message code='epms.repair.perform.detail'  />"    <%  // 실적상세           %>
		analysis				= "<spring:message code='epms.repair.cause.analysis'  />"    <%  // 원인분석           %>
		cancel					= "<spring:message code='epms.request.cancel'         />"    <%  // 요청취소           %>
	/>
	
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	<Pager Visible="0"/>
	
	<LeftCols>
		<C Name="APPR_STATUS"				Type="Enum" Align="left"   Visible="1" Width="80"  CanEdit="0" CanExport="1" CanHide="1"<tag:enum codeGrp="APPR_STATUS"/>/> 
		<C Name="REQUEST_TYPE"				Type="Enum" Align="left"   Visible="0" Width="80"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="REQUEST_TYPE"/>/>
		<C Name="URGENCY"					Type="Enum" Align="left"   Visible="1" Width="70"  CanEdit="0" <tag:enum codeGrp="URGENCY"/>/>
		
		<C Name="REPAIR_REQUEST"			Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="LOCATION"					Type="Enum" Align="left"   Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>	
		<C Name="CATEGORY"					Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="CATEGORY_NAME"				Type="Text" Align="left"   Visible="1" Width="130" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT"					Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="EQUIPMENT_UID"				Type="Text" Align="left"   Visible="1" Width="90"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT_NAME"			Type="Text" Align="left"   Visible="1" Width="190" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
	</LeftCols>
	
	<Cols>
		<C Name="PART"						Type="Enum" Align="left"   Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="PART" companyId="${ssCompanyId}"/>/>
		<C Name="REPAIR_REQUEST_ORG"		Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="DATE_REQUEST"				Type="Date" Align="center" Visible="1" Width="110" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd" />
		<C Name="REQUEST_DESCRIPTION"		Type="Text" Align="left"   Visible="1" Width="440" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="ATTACH_GRP_NO"				Type="Text" Align="center" Visible="0" Width="80"  CanEdit="0" CanExport="1" CanHide="1" />
		
		<C Name="REQUEST_DIV"				Type="Text" Align="left"   Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="REQUEST_DIV_NAME"			Type="Text" Align="left"   Visible="0" Width="100" CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="REQUEST_MEMBER"			Type="Text" Align="left"   Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="REQUEST_MEMBER_NAME"		Type="Text" Align="left"   Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		
		<C Name="DATE_APPR"					Type="Date" Align="center" Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd"  />
		<C Name="APPR_MEMBER"				Type="Text" Align="left"   Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="APPR_MEMBER_NAME"			Type="Text" Align="left"   Visible="1" Width="60"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="REJECT_DESCRIPTION"		Type="Text" Align="left"   Visible="1" Width="210" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
	</Cols>
	
	<RightCols>
		<C Name="REPAIR"					Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="REPAIR_STATUS"				Type="Enum" Align="left"   Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="REPAIR_STATUS"/>/> 
		<C Name="MANAGER"					Type="Text" Align="left"   Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="MANAGER_NAME"				Type="Text" Align="left"   Visible="1" Width="60"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="REPAIR_TYPE"				Type="Enum" Align="left"   Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="REPAIR_TYPE" companyId="${ssCompanyId}"/>/> 
		<C Name="DATE_REPAIR"				Type="Date" Align="center" Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd"  />
		<C Name="detail"					Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" />
	<%--	<C Name="analysis"					Type="Icon" Align="center" Visible="0" Width="70"  CanEdit="0" CanExport="1" CanHide="1" /> --%>
	<%--	<C Name="SAVE_YN"					Type="Enum" Align="center" Visible="1" Width="60"  CanEdit="0" CanExport="1" CanHide="1" Enum="|Y|N" EnumKeys="|Y|N"/> --%>
		<C Name="cancel"					Type="Icon" Align="center" Visible="1" Width="80"  CanEdit="0" CanExport="1" CanHide="1" />
	</RightCols>
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,항목,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        "
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>