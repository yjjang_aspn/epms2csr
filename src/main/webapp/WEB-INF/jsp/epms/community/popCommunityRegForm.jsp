<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: popCommunityRegForm.jsp
	Description : 게시판 > 등록 팝업
	author		: 서정민
	since		: 2019.02.19
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2019.02.19	서정민		최초 생성

--%>
<%@ include file="/com/codeConstants.jsp"%>
<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">

var fileVal = '';

$(document).ready(function(){
	
	<%-- 타이틀 --%>
	if("${param.TYPE}" == 'CHECK'){
		$("#modal_title").html('일일점검 등록');
	}
		
	<%-- 등록버튼 --%>
	$('#regBtn').on('click', function() {
		if (submitBool) {
			fnRegValidate();
		} else {
			alert("등록중입니다. 잠시만 기다려 주세요.");
			return false;
		}
	});
	
	<%-- 취소버튼 --%>
	$('#cancelBtn').on('click', function() {
		fnModalToggle('popCommunityRegForm');
	});
	
	<%-- 첨부파일 수정버튼 (등록폼 변환) --%>
	$('#modifyBtn').on('click', function(){
		
		$('#popViewFrame').hide();
		$('#popRegFrame').show();
	});
	
	<%-- DatePicker  --%>
	$('.moDatePic').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	});
	
	<%-- 첨부파일 관련 --%>
	for(var i=0; i<1; i++){				<%-- 파일 영역 추가를 고려하여 개발 --%>
		$('#attachFile').append('<input type="file" class="ksUpload" multiple id="fileList" name="fileList">');	
		
// 		var fileType = 'jpeg |gif |jpg |png |bmp ';
		var fileType = 'jpeg |gif |jpg |png |bmp '
			 + '|avi |wmv |mpeg |mpg |mkv |mp4 |tp |ts |asf |asx |flv |mov |3gp '
			 + '|mp3 |ogg |wma |wav |au |rm |mid |flac |m4a |amr ';
	
		$('#fileList').MultiFile({
			   accept :  fileType
			,	list   : '#detailFileRegList'+i
			,	max    : 5
			,	STRING : {
					remove: '<img src="/images/com/web/btn_remove.png">'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
					text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" />',
					idx	  : i,
				},
		});
		
		<%-- 첨부한 파일의 출력 순서를 자동으로 샛팅 하기 위한 스크립트 - 공통 스크립트로 빠지면 안됨 .개별 항목으로 처리 필수 --%> 
		$('.MultiFile-wrap').change(function(){
			var fileExpIndex = $(this).closest('.fileWrap').find('.fileList > div').children('div:last-child');
			var	fileExp = fileExpIndex.find('.fileExp');
			fileExp.val(fileExpIndex.index() + 1);
		});
	}
	

	<%-- 파일 다운로드 --%>
	$('.btnFileDwn').on('click', function(){
		var ATTACH_GRP_NO = $(this).data("attachgrpno");
		var ATTACH = $(this).data("attach");
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO='+ATTACH_GRP_NO+'&ATTACH='+ATTACH;
		$("#fileDownFrame2").attr("src",src);
	});
	
	<%-- 첨부파일 삭제버튼 --%>
	$('.icn_fileDel').on('click', function(){
		delModiFile($(this));
	});
	
	
	if("${param.flag}" == 'reg'){			<%-- '등록' 일 경우 --%>
		
		var content = '';
		if("${param.TYPE}" == 'SUGGESTION'){
			content = '1. 문제점 : \n\n'
			        + '2. 개선안 : \n\n'
			        + '3. 실행 여부 : 즉 실천 또는 의뢰 \n\n';
		}
		else if("${param.TYPE}" == 'ACCIDENT'){
			content = '1. 작업/설비 개소 : \n\n'
			        + '2. 예상되는 사고내용 : \n\n'
			        + '3. 개선대책(없을 시 공백) : \n\n';
		}
		
		$('#popCommunityRegForm_CONTENT').val(content);
	
		<%-- 첨부파일 등록폼 --%>
		$('#popViewFrame').hide();
		$('#popRegFrame').show();
	}
	else if("${param.flag}" == 'update'){	<%-- '수정' 일 경우 --%>
		
		<%-- 첨부파일 중 사진이 아닐경우 미리보기 숨김 --%>
		var fileList = new Array();
		<c:forEach items="${attachList}" var="item" >
			var obj = new Object();
			
			obj.ATTACH_GRP_NO = "${item.ATTACH_GRP_NO}";
			obj.ATTACH = "${item.ATTACH}";
			obj.FORMAT = "${item.FORMAT}";
			
			fileList.push(obj);
		</c:forEach>
		var attachImgCnt = 0;
		for(var i=0; i<fileList.length; i++){
			if(fileList[i].FORMAT == "jpg" || fileList[i].FORMAT == "png" || fileList[i].FORMAT == "jpeg" || fileList[i].FORMAT == "JPG" || fileList[i].FORMAT == "PNG"){
				attachImgCnt ++;
			}
		}	
		
		if(fileList.length > 0){
			if(attachImgCnt == 0){
				$('#previewImg').hide();
			}
		}
	}
	
});

<%-- 첨부파일 목록에서 제거 --%>
function delModiFile(obj){
	var attach = obj.data("attach");
	var idx = obj.data('idx');
	
	$('#fileRegLstWrap_'+attach).remove();
	
	if(fileVal != '') {
		fileVal += ",";
	}
	
	fileVal += attach;
	$('#DEL_SEQ').val(fileVal);
}

<%-- 게시판 등록 --%>
function fnRegValidate() {
	submitBool = false;

	if (!isNull_J($('#popCommunityRegForm_CONTENT'), '내용을 입력해주세요.')) {
		submitBool = true;
		return false;
	}
	
	var fileCnt = $("input[name=fileGb]").length;
	if (fileCnt > 0) {
		$('#popCommunityRegForm_attachExistYn').val("Y");
	} else {
		$('#popCommunityRegForm_attachExistYn').val("N");
	}
	
	$('#popCommunityRegForm_COST').val($('#popCommunityRegForm_COST').val().replace(/\D/g,""));
	
	var form = new FormData(document.getElementById('communityRegForm'));
	
	var url = '';
	if("${param.flag}" == 'reg'){			<%-- '등록' 일 경우 --%>
		url = '/epms/community/insertCommunity.do';
	}
	else if("${param.flag}" == 'update'){	<%-- '수정' 일 경우 --%>
	url = '/epms/community/updateCommunity.do';
	}
	
	if(confirm("등록 하시겠습니까?")){
		$.ajax({
			type : 'POST',
			url : url,
			dataType : 'json',
			async : false,
			data : form,
			processData : false,
			contentType : false,
			success : function(json) {
				if(json.COMMUNITY != '' || json.COMMUNITY != null){
					fnReload('${param.flag}','popCommunityRegForm', json.COMMUNITY);
					submitBool = true;
				}else{
					alert("처리 도중 오류가 발생하였습니다.\n시스템 관리자에게 문의 바랍니다.");
					submitBool = true;
					return;
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
				submitBool = true;
				loadEnd();
			}
		});
	}
	else{
		submitBool = true;
	}
}

function getNumber(obj){
    
    var num01;
    var num02;
    num01 = obj.value;
    num02 = num01.replace(/\D/g,""); //숫자가 아닌것을 제거, 
                                     //즉 [0-9]를 제외한 문자 제거; /[^0-9]/g 와 같은 표현
    num01 = setComma(num02); //콤마 찍기
    obj.value =  num01;
    
}
function setComma(n) {
    var reg = /(^[+-]?\d+)(\d{3})/;   // 정규식
    n += '';                          // 숫자를 문자열로 변환         
    while (reg.test(n)) {
       n = n.replace(reg, '$1' + ',' + '$2');
    }         
    return n;
}

</script>

<div class="modal-dialog root wd-per-40">	
	 <div class="modal-content hgt-per-80" style="min-height:540px;">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title">게시판 등록</h4>
		</div>
	 	<div class="modal-body">
           	<div class="modal-bodyIn">
				<form id="communityRegForm" name="communityRegForm"  method="post" enctype="multipart/form-data">
					<input type="hidden" id="popCommunityRegForm_attachExistYn" name="attachExistYn" /> 
					<input type="hidden" id="popCommunityRegForm_MODULE" name="MODULE" value="81" />
					<input type="hidden" id="popCommunityRegForm_ATTACH_GRP_NO" name="ATTACH_GRP_NO" value="${communityDtl.ATTACH_GRP_NO}" />
					<input type="hidden" id="popCommunityRegForm_COMMUNITY" name="COMMUNITY" value="${param.COMMUNITY}" />
					<input type="hidden" id="popCommunityRegForm_TYPE" name="TYPE" value="${param.TYPE}"/>
					<input type="hidden" id="DEL_SEQ" name="DEL_SEQ" value=""/>
					
					<div class="tb-wrap">
						<table class="tb-st">
							<caption class="screen-out">게시판 등록</caption>
							<colgroup>
								<col width="20%" />
								<col width="30%" />
								<col width="20%" />
								<col width="*" />
							</colgroup>
							<tbody>				
								<tr>
									<th>공장</th>
									<td colspan="3">
										<div class="sel-wrap">
											<select title="구분" id="popCommunityRegForm_LOCATION" name="LOCATION">
												<c:if test="${fn:length(locationList) > 0 }">
													<c:forEach var="item" items="${locationList}">
														<option value="${item.CODE}" <c:if test="${communityDtl.LOCATION eq item.CODE}"> selected="selected"</c:if>>${item.NAME}</option>
													</c:forEach>
												</c:if>
											</select>
										</div>
									</td>
								</tr>		
								<tr>
									<th>점검일</th>
									<td>
										<div class="f-l wd-per-100">
											<input type="text" class="inp-comm moDatePic inpCal" readonly="readonly" id="popCommunityRegForm_DATE_CHECK" name="DATE_CHECK" value="${communityDtl.DATE_CHECK}"/>
										</div>
									</td>
									<th>비용</th>
									<td>
										<div class="inp-wrap type02" style="width:90%">  
											<input type="text" class="inpTxt ar text_right onlyNumber" style="text-align: right;" maxlength="12" onchange="getNumber(this);" onkeyup="getNumber(this);" title="비용" id="popCommunityRegForm_COST" name="COST" <c:if test="${communityDtl.COST ne null}">value="${communityDtl.COST}"</c:if>/>
											<span class="unit">원</span>
										</div>
									</td>
								</tr>
								<tr>
									<th>내용</th>
									<td colspan="3">
										<div class="txt-wrap">
											<textarea cols="" rows="15" title="고장내용" id="popCommunityRegForm_CONTENT" name="CONTENT"><c:if test="${communityDtl.CONTENT ne null}">${communityDtl.CONTENT}</c:if></textarea>
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row">첨부파일</th>
									<td colspan="3">
										<div class="fileWrap" id="popViewFrame">
											<div class="f-l wd-per-100">
												<a href="#none" id="modifyBtn" class="btn comm st01 f-l">첨부파일 수정</a>			
											</div>
											<c:choose>
												<c:when test="${fn:length(attachList)>0 }">
												<div id="previewList" class="wd-per-100">
													<div class="bxType01 fileWrap previewFileLst">
														<div>
															<div class="fileList type02">
																<div id="detailFileList0" class="fileDownLst">
																	<c:forEach var="item" items="${attachList}" varStatus="idx">
																		<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																			<span class="MultiFile-export">${item.SEQ_DSP}</span> 
																			<span class="MultiFile-title" title="File selected: ${item.NAME}">
																			${item.FILE_NAME} (${item.FILE_SIZE_TXT})
																			</span>
																			<a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a>
																		</div>		
																	</c:forEach>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div id="previewImg" class="wd-per-100">
													<div class="previewFile mgn-t-15">
														<h3 class="tit-cont preview" id="previewTitle">이미지 미리보기</h3>
														<ul style="margin-top:5px;">
															<c:forEach var="item" items="${attachList}" varStatus="idx">
																<c:if test = "${item.FORMAT eq 'jpg' || item.FORMAT eq 'png' || item.FORMAT eq 'jpeg' || item.FORMAT eq 'JPG' || item.FORMAT eq 'PNG'}">
																	<li>
																		<img src="${item.FILE_URL}" title="클릭하시면 원본크기로 보실 수 있습니다." onclick="originFileView('${item.FILE_URL}')" />
																		<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																			<span class="MultiFile-title" title="File selected: ${item.NAME}">
																				<p>${item.FILE_NAME} (${item.FILE_SIZE_TXT})</p>
																			</span>
																			<a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a>
																		</div>
																	</li>
																</c:if>
															</c:forEach>
														</ul>
													</div>
												</div>
												</c:when>
												<c:otherwise>
													<div class="f-l wd-per-100 mgn-t-5">
													※ 등록된 첨부파일이 없습니다.
													</div>
												</c:otherwise>
											</c:choose>
										</div>
										<div class="bxType01 fileWrap" id="popRegFrame" style="display:none;">
											<div id="attachFile" class="filebox"></div>
											<div class="fileList">
												<div id="detailFileRegList0" class="fileDownLst">
													<c:forEach var="item" items="${attachList}" varStatus="idx">
														<div class="MultiFile-label" id="fileRegLstWrap_${item.ATTACH}">
															<span class="MultiFile-remove">
																<a href="#none" class="icn_fileDel" data-attach="${item.ATTACH}" data-idx="${item.CD_FILE_TYPE }">
																	<img src="/images/com/web/btn_remove.png" />
																</a>
															</span> 
															<span class="MultiFile-title" title="File selected: ${item.NAME}">
																${item.FILE_NAME} (${item.FILE_SIZE_TXT})
															</span>
														</div>
													</c:forEach>
												</div>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</form>
			</div>
			<iframe id="fileDownFrame2" style="width:0px;height:0px;border:0px"></iframe>
		</div>	
		<div class="modal-footer hasFooter">
			<a href="#none" class="btn comm st02" data-dismiss="modal" id="cancelBtn">취소</a>
			<a href="#none" class="btn comm st01" id="regBtn">등록</a> 
		</div>
	</div>		
</div>

