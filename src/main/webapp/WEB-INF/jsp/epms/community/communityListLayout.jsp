<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: communityListLayout.jsp
	Description : 게시판 : 그리드 레이아웃
    author		: 서정민
    since		: 2019.02.19
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2019.02.19	서정민		최초 생성
	
--%>

<c:set var="gridId" value="CommunityList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1"			SuppressCfg="0"
			Editing="1"				Deleting="0"		Selecting="0"		Dragging="0"
			Filtering="1"			StandardFilter="3"	ClearSelected="2"
			Paging="1"			  	PageLength="5"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="0"			PasteFocused="3"
	/>

		<Header	id="Header"	Align="center"
			
			RN						= "No."
			COMMUNITY				= "게시물 ID"
			COMPANY_ID				= "회사코드"
			LOCATION				= "공장"
			TYPE					= "유형"
			CONTENT				    = "내용"
			COST					= "비용"
			DATE_CHECK				= "점검일"
			ATTACH_GRP_NO			= "첨부파일"
			
			REG_ID					= "작성자 ID"
			REG_MEMBER_NAME		    = "작성자"
			REG_DT					= "작성일"
			UPD_ID					= "최종수정자 ID"
			UPD_MEMBER_NAME		    = "최종수정자"
			UPD_DT					= "최종수정일"
		/>
	
	<Solid>
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
	
	<LeftCols>
		<C Name="RN"						Type="Text" Align="center" Visible="1" Width="40"  CanEdit="0" CanExport="1" CanHide="1" />
	</LeftCols>
	
	<Cols>
		<C Name="COMMUNITY"					Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="COMPANY_ID"				Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="0" CanHide="0" />
		<C Name="LOCATION"					Type="Enum" Align="left"   Visible="1" Width="60"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>
		<C Name="DATE_CHECK"				Type="Date" Align="center" Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd" />
		<C Name="TYPE"						Type="Text" Align="left"   Visible="0" Width="60"  CanEdit="0" CanExport="0" CanHide="0" />
		<C Name="CONTENT"			    	Type="Lines"Align="left"   Visible="1" Width="400" CanEdit="0" CanExport="1" CanHide="1" RelWidth="400" MinWidth="400"/>
		<C Name="COST"						Type="Int"  Align="right"  Visible="1" Width="90"  CanEdit="0" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";'/>
		<C Name="ATTACH_GRP_NO"				Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" />
	
		
		<C Name="REG_ID"					Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" />
		<C Name="REG_MEMBER_NAME"			Type="Text" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="REG_DT"					Type="Date" Align="center" Visible="0" Width="100" CanEdit="0" CanExport="0" CanHide="0" Format="yyyy/MM/dd" />
		
		<C Name="UPD_ID"					Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="0" CanHide="0" />
		<C Name="UPD_MEMBER_NAME"			Type="Text" Align="center" Visible="0" Width="70"  CanEdit="0" CanExport="0" CanHide="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="UPD_DT"					Type="Date" Align="center" Visible="0" Width="100" CanEdit="0" CanExport="0" CanHide="0" Format="yyyy/MM/dd" />
		
	</Cols>
	
	<Foot>
		<I id="Foot" Calculated="1" Spanned="1"  LOCATION="합          계" LOCATIONType="Text" LOCATIONSpan="4" LOCATIONAlign="Center" Class="GOHeaderRow GOHeaderText"
			LOCATIONFormula='"합          계 ( " + count() + " 건 )"'									
			COSTFormula="sum()"
		/>
	</Foot>
		
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,등록,항목,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        " 
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				등록Type="Html" 등록="&lt;a href='#none' title='등록' class=&quot;treeButton treeRegister&quot;
					onclick='fnCommunityRegForm(&quot;reg&quot;)'>등록&lt;/p>"
				추가Type="Html" 추가="&lt;a href='#none' title='추가' class=&quot;defaultButton01 icon add&quot;
					onclick='addRowGrid(&quot;${gridId}&quot;)'>추가&lt;/a>"
				저장Type="Html" 저장="&lt;a href='#none' title='저장' class=&quot;treeButton treeSave&quot;&quot;
					onclick='saveGrid(&quot;${gridId}&quot;)'>저장&lt;/a>"
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton01 icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
	
</Grid>