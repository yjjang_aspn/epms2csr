<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%--
	Class Name	: communityList.jsp
	Description : 게시판 : 메인
    author		: 서정민
    since		: 2019.02.19
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2019.02.19	 	서정민		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<!-- 고장관리 : 정비요청결재 -->
<script type="text/javascript">

<%-- 전역변수 --%>
var msg = "";				<%-- 결과 메세지 --%>
var focusedRow = null;		<%-- Focus Row : [정비일정관리/배정] --%>
var submitBool = true;		<%-- 유효성검사 --%>

$(document).ready(function(){
	
	<%-- 타이틀 --%>
	if("${TYPE}" == 'CHECK'){
		$("#CommunityListTitle").html('일일점검');
	}
	
	<%-- DatePicker --%>
	$('.datePic').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	});
	
	<%-- 검색 시작일이 종료일보다 이후인지 체크 (이후라면, 종료일을 시작일로 변경) --%>
	$('#startDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#endDt').val($('#startDt').val());
		}
	});
	
	<%-- 검색 종료일이 시작일보다 이전인지 체크 (이전이라면, 시작일을 종료일로 변경) --%>
	$('#endDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#startDt').val($('#endDt').val());
		}
	});
	
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function() {		<%-- 모달이 사용자에게 보여졌을 때  --%>
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>
	});
	
	<%-- 수정,삭제 버튼 숨김 --%>
	$('#btns').hide(); 				
	
	<%-- '검색' 클릭 --%>
	$('#srcBtn').on('click', function(){
		Grids.CommunityList.Source.Data.Url = "/epms/community/communityListData.do?TYPE=${TYPE}"
											+ "&LOCATION=" + $('#sel_plant').val()
											+ "&startDt=" + $('#startDt').val()
											+ "&endDt=" + $('#endDt').val();
		Grids.CommunityList.ReloadBody();
	});
	
	<%-- '수정' 클릭 --%>
	$('#updateBtn').on('click', function(){
		fnCommunityRegForm('update');
	});
	
	<%-- '삭제' 클릭 --%>
	$('#deleteBtn').on('click', function(){
		fnDeleteCommunity();
	});
	
	<%-- 파일 다운로드  --%>
	$('#fileNotNull').on('click','.btnFileDwn', function(){
		var ATTACH = $(this).data("attach");
		var ATTACH_GRP_NO = $('#ATTACH_GRP_NO').val();
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO='+ATTACH_GRP_NO+'&ATTACH='+ATTACH;
		$("#fileDownFrame").attr("src",src);
	});
	
	<%-- 일괄 다운로드(압축 파일) --%>
	$('#fileNotNull').on('click', '.zipFileBtn', function(){
		var ATTACH_GRP_NO = $('#ATTACH_GRP_NO').val();
		var src = '/attach/zipFileDownload.do?ATTACH_GRP_NO='+ATTACH_GRP_NO;
		$("#fileDownFrame").attr("src",src);
	});
	
}); <%-- $(document).ready 종료 --%>

<%-- 그리드 클릭 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "CommunityList") { <%-- [게시판 목록] 클릭시 --%>
	
		if (focusedRow == null || focusedRow.COMMUNITY != row.COMMUNITY || row.Added == 1){
			
			<%-- 포커스 시작 --%>
			if(focusedRow != null){
				grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
			}
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			focusedRow = row;
			
			$('#COMMUNITY').val(row.COMMUNITY);
			
			<%-- 상세정보 조회 --%>
			getCommunityDtl(row.COMMUNITY);
		}
	}
}

<%-- 선택된 정비요청의 상세정보  조회 --%>
function getCommunityDtl(COMMUNITY){
	
	$.ajax({
		type: 'POST',
		url: '/epms/community/communityDtl.do',
		data: { COMMUNITY : COMMUNITY },
		dataType: 'json',
		success: function (json) {
			if(json != "" && json != null) {
				
				<%-- 본인이 작성한 게시물일 경우 버튼 활성화 --%>
				if(json.communityDtl.REG_ID == '${sessionScope.ssUserId}'){
					
					$('#btns').show();         <%-- 수정 삭제 버튼 --%>
				}else{
					$('#btns').hide();
				}
				
				$('#COMMUNITY').val(json.communityDtl.COMMUNITY);				<%-- 게시판ID --%>
				$('#REG_ID').val(json.communityDtl.REG_ID);						<%-- 작성자ID --%>
				$('#LOCATION_NAME').val(json.communityDtl.LOCATION_NAME);		<%-- 공장 --%>
				$('#REG_MEMBER_NAME').val(json.communityDtl.REG_MEMBER_NAME);	<%-- 작성자 --%>
				$('#DATE_CHECK').val(json.communityDtl.DATE_CHECK);				<%-- 작성일 --%>
				$('#COST').val(json.communityDtl.COST + " 원");					<%-- 비용 --%>
				$('#CONTENT').text(json.communityDtl.CONTENT);					<%-- 내용 --%>
				$('#ATTACH_GRP_NO').val(json.communityDtl.ATTACH_GRP_NO);		<%-- 첨부파일 그룹번호 --%>
				
				var html = "";
				$("#canvas2").empty();
	            $("#previewFileList").empty();
	            $("#fileNotNull").find('.zipFileBtn').remove();
	            
				if(json.attachList != null){
					
					$("#fileNull").hide()
					$("#fileNotNull").show()
					$("#previewImg").show()
					$("#fileNotNull").prepend('<a href="#none" value="11" class="zipFileBtn btn evn-st01 mgn-t-5" >일괄 다운로드</a>');
					
					$.each(json.attachList, function (i, o) {
						var _tr1 = $("#template1").html();
						var _tr2 = $("#template2").html();
					
						    _tr1 = $(_tr1);
						    _tr2 = $(_tr2);
						
				            for (var name in o) {
				            	
				            	switch (name) {
					 				case "ATTACH":
					 					_tr1.attr("id", "fileLstWrap_"+ o[name]);
					 					_tr1.find("a.btnFileDwn").attr("data-attach", o[name]);
					 					break;
					 				case "SEQ_DSP":
					 					_tr1.find("span.MultiFile-export").text(o[name]);
					 					break;
					 				case "NAME":
					 					_tr1.find("span.MultiFile-title").text(o["FILE_NAME"]+"("+o["FILE_SIZE_TXT"]+")");
					 					_tr2.find("p").text(o["FILE_NAME"]+"("+o["FILE_SIZE_TXT"]+")").append('<a href="#none" class="btnFileDwn icon download" data-attach=' + o["ATTACH"]  + '>다운로드</a>');
					 					break;
					 				case "ATTACH_GRP_NO":
					 					_tr2.find("img").attr("src", "/attach/fileDownload.do?ATTACH_GRP_NO="+o[name]+"&ATTACH="+o["ATTACH"]+"&EditTime="+new Date().getTime());
					 					_tr2.find("img").attr("onclick", "originFileView('/attach/fileDownload.do?ATTACH_GRP_NO="+o[name]+"&ATTACH="+o["ATTACH"]+"&EditTime="+new Date().getTime()+"')");
					 					break;
					 				case "FILE_NAME":
					 					_tr2.find("a.btnFileDwn").attr("data-attach", o["ATTACH"]);
					 					break;
					 					
				 				} //end switch문
				            } //end for문
				            
			            $("#canvas2").append(_tr1);
				            
			            if(o["FORMAT"] == 'jpg' || o["FORMAT"] == 'png' || o["FORMAT"] == 'jpeg' || o["FORMAT"] == 'JPG' || o["FORMAT"] == 'PNG'){
							$("#previewFileList").append(_tr2); <%-- '이미지'파일인 경우만 미리보기 --%>
			            }
			            
				    });
					
					if($("#previewFileList").has('li').size()==0){ 
						$("#previewImg").hide();
					}
					
				}else if (json.attachList == null) {
					$("#fileNull").show();
					$("#fileNotNull").hide();
				}
			}
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('리스트 로딩 중 오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
			loadEnd();
		}
	});
}

<%-- 등록 버튼 : 등록팝업 호출 --%>
function fnCommunityRegForm(flag){
	
	var url = "/epms/community/popCommunityRegForm.do"
			+ "?TYPE=${TYPE}"
			+ "&COMMUNITY=" + $('#COMMUNITY').val()
			+ "&flag="+flag
			+ "&"+new Date().getTime();
		
	fnOpenLayerWithParam(url, 'popCommunityRegForm');
}

<%-- 삭제 ajax --%>
function fnDeleteCommunity(){
	
	submitBool = false;

	if(confirm("삭제 하시겠습니까?")){
		$.ajax({
			type : 'POST',
			url : '/epms/community/deleteCommunity.do',
			dataType : 'json',
			async : false,
			data : { COMMUNITY : $('#COMMUNITY').val() },
			success : function(json) {
				if(json.delYn == 'N'){
					alert("게시물이 존재하지 않습니다.");
				}
				else if(json.delYn == 'Y'){
					
					fnReload('delete','',$('#COMMUNITY').val());
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
				submitBool = true;
				loadEnd();
			}
		});
	}
	else{
		submitBool = true;
	}
	
}

<%-- 모달 토글 --%>
function toggleModal(obj){
	obj.modal('toggle');
}

<%-- 새로고침 --%>
function fnReload(type, modal, community){
	
	if(!(modal=='' || modal==null)){
		toggleModal($('#'+modal));
	}
	
	Grids.CommunityList.ReloadBody();
	getCommunityDtl(community);
	
	if(type == 'reg'){
		msg = "정상적으로 등록되었습니다.";
	}
	else if (type == 'update'){
		msg = "정상적으로 수정되었습니다.";
	}
	else if (type == 'delete'){
		msg = "정상적으로 삭제되었습니다.";
	}
	
	alert(msg);
}

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source,data, func){
	if(grid.id == "RepairApprList"){
		if(data != ""){
			callLoadingBar();
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "RepairApprList"){
					setTimeout($.unblockUI, 100);
				}
			}
		}
	}
}

<%-- 첨부파일 다운로드 팝업 호출--%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- 이미지 원본크기 보기 --%>
function originFileView(img){ 
	img1= new Image(); 
	img1.src=(img); 
	imgControll(img); 
} 

function imgControll(img){ 
	if((img1.width!=0)&&(img1.height!=0)){ 
		viewImage(img); 
	}else{ 
		controller="imgControll('"+img+"')"; 
		intervalID=setTimeout(controller,20); 
	} 
}

function viewImage(img){ 
	W=img1.width; 
	H=img1.height; 
	O="width="+W+",height="+H+",scrollbars=yes"; 
	imgWin=window.open("","",O); 
	imgWin.document.write("<html><head><title>이미지상세보기</title></head>");
	imgWin.document.write("<body topmargin=0 leftmargin=0>");
	imgWin.document.write("<img src="+img+" onclick='self.close()' style='cursor:pointer;' title ='클릭하시면 창이 닫힙니다.'>");
	imgWin.document.close();
}

</script>
</head>
<body>

<!-- 화면 UI Area Start  -->
<div id="contents">

	<div class="fl-box panel-wrap04" style="width:65%">
		<h5 class="panel-tit" id="CommunityListTitle">게시판 목록</h5>	
		<div class="inq-area-inner type06 ab">
			<div class="wrap-inq">
				<div class="inq-clmn">
					<h4 class="tit-inq"><spring:message code='epms.location' /></h4><!-- 공장 -->
					<div class="sel-wrap type02">
						<select title="구분" id="sel_plant" name="sel_plant">
							<c:if test="${fn:length(locationList) > 0 }">
								<c:if test="${fn:length(locationList) > 1 }">
								<option value="All" >전체</option>
								</c:if>
								<c:forEach var="item" items="${locationList}">
									<option value="${item.CODE}" role="${item.REMARKS}" <c:if test="${sessionScope.ssLocation eq item.CODE}"> selected="selected"</c:if>>${item.NAME}</option>
								</c:forEach>
							</c:if>
						</select>
					</div>
				</div>
				<div class="inq-clmn" id="aterialInOutDate">
					<h4 class="tit-inq" >등록일</h4>
					<div class="prd-inp-wrap">
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic thisMonth inpCal" readonly="readonly" id="startDt" title="검색시작일" value="${startDt}" >
							</span>
						</span>
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic inpCal" readonly="readonly" id="endDt" title="검색종료일" value="${endDt}">
							</span>
						</span>		
					</div>
				</div>
				<!-- e:inq-clmn -->
			</div>
			<!-- wrap-inq -->
			<a href="#none" id="srcBtn" class="btn comm st01">검색</a>
		</div>
		<!-- e:inq-area-inner -->
		<div class="panel-body mgn-t-20">
			<!-- 트리그리드 : s -->
			<div id="RepairApprList">
				<bdo	Debug="Error"
						Data_Url="/epms/community/communityListData.do?TYPE=${TYPE}&startDt=${startDt}&endDt=${endDt}&LOCATION=${location}"
						Layout_Url="/epms/community/communityListLayout.do?TYPE=${TYPE}"
						
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=게시판 목록.xls&dataName=data"
				>
				</bdo>
			</div>
			<!-- 트리그리드 : e -->
		</div>
	</div>
	
	<form id="communityForm" name="communityForm"  method="post" enctype="multipart/form-data">
		
		<input type="hidden" class="inp-comm" id="TYPE" name="TYPE" value="${TYPE}"/> 		<%-- 게시판 유형(ACCIDENT:아차사고, SUGGESTION:제안) --%>
		<input type="hidden" class="inp-comm" id="COMMUNITY" name="COMMUNITY"/> 			<%-- 게시판 ID --%>
		<input type="hidden" class="inp-comm" id="ATTACH_GRP_NO" name="ATTACH_GRP_NO"/>
		
	</form>
	
	<div class="panel-wrap05" style="float:right; width:35%">
		<h5 class="panel-tit mgn-l-10"  id="repairRequestDetailTitle">상세 내역</h5>
		<div class="panel-body mgn-l-10 mgn-t-40" style="overflow-y:auto;overflow-x:hidden;">
			<div id="btns"  class="ab" style="top:0; right:0;">
				<a href="#none" id="deleteBtn" class="btn comm st02">삭제</a>
				<a href="#none" id="updateBtn" class="btn comm st01">수정</a>
			</div>
			<div class="tb-wrap" >
				<table class="tb-st">
					<caption class="screen-out">상세 내역</caption>
					<colgroup>
						<col width="15%" />
						<col width="35%" />
						<col width="15%" />
						<col width="*" />
					</colgroup>
					<tbody>
						<tr>
							<th scope="row"><spring:message code='epms.location' /></th><!-- 공장 -->
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="text" class="inp-comm" id="LOCATION_NAME" name="LOCATION_NAME" readonly="readonly"/>
								</div>
							</td>
							<th scope="row">작성자</th>
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="text" class="inp-comm" id="REG_MEMBER_NAME" name="REG_MEMBER_NAME" readonly="readonly"/>
								</div>
							</td>
						</tr>
						<tr>
							<th scope="row">점검일</th>
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="text" class="inp-comm" id="DATE_CHECK" name="DATE_CHECK" readonly="readonly"/>
								</div>
							</td>
							<th scope="row">비용</th>
							<td>
								<div class="inp-wrap readonly wd-per-100">
									<input type="text" class="inp-comm" id="COST" name="COST" readonly="readonly" style="text-align: right;"/>
								</div>
							</td>
						</tr>
<!-- 						<tr> -->
<!-- 							<th scope="row">제목</th> -->
<!-- 							<td colspan="3"> -->
<!-- 								<div class="inp-wrap readonly wd-per-100"> -->
<!-- 									<input type="text" class="inp-comm" id="TITLE" name="TITLE" readonly="readonly"/> -->
<!-- 								</div> -->
<!-- 							</td> -->
<!-- 						</tr> -->
						<tr>
							<th scope="row">내용</th>
							<td colspan="3">
								<div class="txt-wrap readonly">
									<div class="txtArea">
										<textarea class="inp-comm" cols="" rows="20" id="CONTENT" name="CONTENT" readonly="readonly"></textarea>
									</div>
								</div>
							</td>
						</tr>
						<tr id="fileData">
							<th scope="row">첨부파일</th>
							<td colspan="3">
								<div id="fileNotNull" style="display:none;">
									<div class="bxType01 fileWrap previewFileLst">
										<div class="fileList type02" id="viewFrame">
											<div class="fileDownLst" id="canvas2">
												
											</div>
										</div>
									</div>
										
									<div id="previewImg" class="wd-per-100">	
										<div class="previewFile mgn-t-15">
										<h3 class="tit-cont preview" id="previewTitle">이미지 미리보기</h3>
										<ul style="margin-top:5px;">
											<div id="previewFileList" class="fileDownLst">
												
											</div>
										</ul>
										</div>
									</div>
								</div>
								<div class="f-l wd-per-100 mgn-t-5" id="fileNull">
									※ 등록된 첨부파일이 없습니다.
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<iframe id="fileDownFrame" style="width:0px;height:0px;border:0px"></iframe>

</div>

<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<%-- 게시판 등록 --%>
<div class="modal fade modalFocus" id="popCommunityRegForm" data-backdrop="static" data-keyboard="false"></div>	

<%-- 첨부파일 조회 --%>
<div class="modal fade modalFocus" id="popFileManageForm"   data-backdrop="static" data-keyboard="false"></div> 	
	
</body>

<script id="template1" type="text/template">
<div class="MultiFile-label">
	<span class="MultiFile-export"></span> 
	<span class="MultiFile-title">
	</span>
	<a href="#none" class="btnFileDwn icon download">다운로드</a>
</div>
</script>

<script id="template2" type="text/template">
	<li>
		<img/>
		<p></p>
	</li>
</script>
</html>
