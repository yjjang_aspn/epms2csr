<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%--
	Class Name	: communityListData.jsp
	Description : 게시판 : 데이터
    author		: 서정민
    since		: 2019.02.19
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2019.02.19	 	서정민		최초 생성

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I
			Height="90"
			MaxHeight="90"
			RN						= "${item.RN}"
			COMMUNITY				= "${item.COMMUNITY}"
			COMPANY_ID				= "${item.COMPANY_ID}"
			LOCATION				= "${item.LOCATION}"
			TYPE					= "${item.TYPE}"
			TITLE				    = "${fn:replace(item.TITLE, '"', '&quot;')}"
			CONTENT				    = "${fn:replace(item.CONTENT, '"', '&quot;')}"
			COST				    = "${item.COST}"
			DATE_CHECK				= "${item.DATE_CHECK}"
			ATTACH_GRP_NO			= "${item.ATTACH_GRP_NO}"
			<c:if test='${item.FILE_CNT > 0}'>
				ATTACH_GRP_NOVAlign="middle"
				ATTACH_GRP_NOSwitch="1"  ATTACH_GRP_NOIcon="/images/com/web/commnet_up3.gif" 
				ATTACH_GRP_NOOnClickSideIcon="fnOpenLayerWithParam('/attach/popFileManageForm.do?ATTACH_GRP_NO='+Row.ATTACH_GRP_NO+'&editYn='+'${editYn}','popFileManageForm');"
			</c:if> 
			
			REG_ID					= "${item.REG_ID}"
			REG_MEMBER_NAME		    = "${fn:replace(item.REG_MEMBER_NAME, '"', '&quot;')}"
			REG_DT					= "${item.REG_DT}"
			UPD_ID					= "${item.UPD_ID}"
			UPD_MEMBER_NAME		    = "${fn:replace(item.UPD_MEMBER_NAME, '"', '&quot;')}"
			UPD_DT					= "${item.UPD_DT}"
			
			
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>