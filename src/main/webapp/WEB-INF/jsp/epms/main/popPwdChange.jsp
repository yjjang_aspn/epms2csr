<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: popPwdChange.jsp
	Description : 비밀변호 변경
	author		: 김영환
	since		: 2018.09.03
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.09.03	 김영환		최초 생성

--%>
<%@ include file="/com/codeConstants.jsp"%>
<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
});

</script>

<div class="modal fade modalFocus" id="popPwdChange"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-35 hgt-per-90">
		<div class="modal-content hgt-per-40" style="min-height:300px; width: 477px;">
			<div class="modal-header">
			   	<h4 class="modal-title" id="modal_title">비밀번호 변경</h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
					<!-- 페이지 내용 : s -->
					<!-- 사용자 정보 -->
						<div class="profile-wrap">
							<form id="chgForm" name="chgForm" method="post" enctype="multipart/form-data">
								<div class="top-area">
									
									<div class="PinfoArea">
										<p class="bold-St">  <span style="color:#2886b4; font-size:18px; font-weight:500;">비밀번호를 변경해주세요.</span> </p>
										<p><span>*</span>비밀번호는  <span style="color:red">영문, 숫자, 특수문자를 조합하여 8자 이상</span>으로 설정하셔야 합니다. </p>
									</div>
								</div>
								<input type="hidden" id="PASSWORD" name="PASSWORD"/>
							</form>
							
							<!-- top-area -->
							<div class="bottom-area">
								<div class="tb-wrap type03">
									<table class="tb-st">
										<caption></caption>
										<colgroup>
											<col width="35%" />
											<col width="*" />
										</colgroup>
										<tbody>
											
											<tr class="pwChgArea">
												<th>신규 비밀번호</th>
												<td>
													<div class="inp-wrap type03">
														<input type="password" title="" id="user_pwd" name="user_pwd" />
													</div>
												</td>
											</tr>
											<tr class="pwChgArea">
												<th>비밀번호 확인</th>
												<td>
													<div class="inp-wrap type03">
														<input type="password" title="" id="crmChgPwd" name="" />
													</div>
												</td>
											</tr>
											
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="pwdSave wd-per-100">
								<a href="#none" id="chgBtn">계정 정보 저장</a>
							</div>
							
						</div>
						<!-- e:prifile-wrap -->
					<!-- 페이지 내용 : e -->
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>					
</div>	