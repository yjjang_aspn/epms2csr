<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: mainTroubleListDtl.jsp
	Description : 대시보드 > 돌발정비현황 상세페이지
	author		: 김영환
	since		: 2018.06.21
 
	<< 개정이력(Modification Information) >>
	수정일 		 수정자		수정내용
	----------  ------	---------------------------
	2018.06.21	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
</head>
<body>

	<div class="expandPortletWrap">
		<div class="expandSchWrap rel">
			<div class="wrap-inq">	
				<div class="inq-clmn">
					<h4 class="tit-inq">공장</h4>
					<div class="sel-wrap type02" style="width:120px">
						<select title="공장" id="LOCATION" name="LOCATION" >
							<c:choose>
								<c:when test="${fn:length(locationList) > 0}">
									<c:forEach var="item" items="${locationList}">
										<option value="${item.CODE}" role="${item.REMARKS}" <c:if test="${sessionScope.ssLocation eq item.CODE}"> selected="selected"</c:if>>${item.NAME}</option>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<option value="" selected="selected">권한필요</option>
								</c:otherwise>
							</c:choose>
						</select>
					</div>
				</div>
			</div>
			
			<div class="monthMoveWrap ab">
				<div class="monthMoveBtn prev"><div id="yearPrev"><</div></div>
				<div class="monthMoveInfo"><span id="yearMoveInfo"></span></div>
				<div class="monthMoveBtn next"><div id="yearNext">></div></div>
			</div>
		</div>
		
		<div class="boxscroll expandPortletH02">	
			<div class="expandChartDtl">
				<div class="expandChartDtlIn">
					<div class="mgn-t-10">
						<div class="expandChart" style="height:34vh;">
							<div class="expandChartTit">전 파트</div>
							<canvas id="troubleListTotChart"></canvas>
						</div>
					</div>
					<div class="mgn-t-10">
						<div class="expandChart" style="height:34vh;">
							<div class="expandChartTit">기계</div>
							<canvas id="troubleListPart01Chart"></canvas>
						</div>
					</div>
					<div class="mgn-t-10">
						<div class="expandChart" style="height:34vh;">
							<div class="expandChartTit">전기</div>
							<canvas id="troubleListPart02Chart"></canvas>
						</div>
					</div>
					<div class="mgn-t-10">
						<div class="expandChart" style="height:34vh;">
							<div class="expandChartTit">시설</div>
							<canvas id="troubleListPart03Chart"></canvas>
						</div>
					</div>
					<div class="mgn-t-10">
						<div class="expandChart" style="height:34vh;">
							<div class="expandChartTit">생산</div>
							<canvas id="troubleListPart04Chart"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div>
	
	</div>
	
<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<%-- 정비실적 --%>
<%@ include file="/WEB-INF/jsp/epms/main/popMainRepairResultList.jsp"%>

<%-- 정비실적조회 --%>
<div class="modal fade modalFocus" id="popRepairResultForm" data-backdrop="static" data-keyboard="false"></div> 

<script type="text/javascript">
var year = 1;			<%-- [YYYY 연도] 전역변수 --%>
var today = new Date();
var month = today.getMonth() + 1
var YYYYMM = today.getFullYear() + '-' + (month < 10 ? "0" + month : month);
var thisYearID = [];
var lastYearID = [];
var LOCATION = "";
var REMARKS = "";

var barOption = {
	maintainAspectRatio: false,
	responsive: true,
	scales: {
		yAxes: [{
			gridLines: {
			},
			ticks: {
				beginAtZero:true,
				fontFamily: "'Open Sans Bold', sans-serif",
				fontSize:11
			},
			scaleLabel:{
				display:false
			}
		}],
		xAxes: [{
			barPercentage: 0.7,
			gridLines: {
				display:false,
					color: "#fff",
					zeroLineColor: "#fff",
					zeroLineWidth: 0
			},
			ticks: {
				fontFamily: "'Open Sans Bold', sans-serif",
				fontSize:11
			}
		}]
    },
    'onClick' : function (event) {
    	
    	fnOpenLayerWithGrid(this.canvas.id);
    }
}

var ctx = document.getElementById('troubleListPart01Chart');

var troubleListPart01Chart = new Chart(ctx, {
	type: 'bar',
    data: {
      labels: ["1월","2월","3월","4월","5월","6월","7월","8월","9월","10월","11월","12월"],
      datasets: [
        { 
          label: year,
          backgroundColor: "rgba(62,149,205, 1)",
		  hoverBackgroundColor: "rgba(62,149,205, 0.5)",
          data: [],
        }, {
          label: year-1,
          backgroundColor: "rgba(142,94,162, 1)",
		  hoverBackgroundColor: "rgba(142,94,162, 0.5)",
          data: [],
          fill: false
        }
      ]
    },
    options: barOption
});

var ctx = document.getElementById('troubleListPart02Chart');

var troubleListPart02Chart = new Chart(ctx, {
	type: 'bar',
    data: {
      labels: ["1월","2월","3월","4월","5월","6월","7월","8월","9월","10월","11월","12월"],
      datasets: [
        { 
      	  label: year,
          backgroundColor: "rgba(62,149,205, 1)",
  		  hoverBackgroundColor: "rgba(62,149,205, 0.5)",
          data: [],
        }, {
       	  label: year-1,
          backgroundColor: "rgba(142,94,162, 1)",
  		  hoverBackgroundColor: "rgba(142,94,162, 0.5)",
          data: [],
          fill: false
        }
      ]
    },
    options: barOption
});

var ctx = document.getElementById('troubleListPart03Chart');

var troubleListPart03Chart = new Chart(ctx, {
	type: 'bar',
    data: {
      labels: ["1월","2월","3월","4월","5월","6월","7월","8월","9월","10월","11월","12월"],
      datasets: [
        { 
      	  label: year,
          backgroundColor: "rgba(62,149,205, 1)",
  		  hoverBackgroundColor: "rgba(62,149,205, 0.5)",
          data: [],
        }, {
       	  label: year-1,
          backgroundColor: "rgba(142,94,162, 1)",
  		  hoverBackgroundColor: "rgba(142,94,162, 0.5)",
          data: [],
          fill: false
        }
      ]
    },
    options: barOption
});

var ctx = document.getElementById('troubleListPart04Chart');

var troubleListPart04Chart = new Chart(ctx, {
	type: 'bar',
    data: {
      labels: ["1월","2월","3월","4월","5월","6월","7월","8월","9월","10월","11월","12월"],
      datasets: [
        { 
      	  label: year,
          backgroundColor: "rgba(62,149,205, 1)",
  		  hoverBackgroundColor: "rgba(62,149,205, 0.5)",
          data: [],
        }, {
       	  label: year-1,
          backgroundColor: "rgba(142,94,162, 1)",
  		  hoverBackgroundColor: "rgba(142,94,162, 0.5)",
          data: [],
          fill: false
        }
      ]
    },
    options: barOption
});

var ctx = document.getElementById('troubleListTotChart');

var troubleListTotChart = new Chart(ctx, {
	type: 'bar',
    data: {
      labels: ["1월","2월","3월","4월","5월","6월","7월","8월","9월","10월","11월","12월"],
      datasets: [
        { 
      	  label: year,
          backgroundColor: "rgba(62,149,205, 1)",
  		  hoverBackgroundColor: "rgba(62,149,205, 0.5)",
          data: [],
        }, {
       	  label: year-1,
          backgroundColor: "rgba(142,94,162, 1)",
  		  hoverBackgroundColor: "rgba(142,94,162, 0.5)",
          data: [],
          fill: false
        }
      ]
    },
    options: barOption
});

$(document).ready(function(){		
	$(".boxscroll").niceScroll({cursorwidth:"4px",cursorcolor:"#afbfd6",horizrailenabled:false});
	
	<%-- 현재 날짜 설정 --%>
	var date = new Date();
	year = date.getFullYear();
	
	<%-- 화면 데이터 입력--%>
	fnSetData();
	
	$('.monthMoveBtn.next').hide();
	
	<%-- 이전 달로 이동 --%>
	$('#yearPrev').on('click', function(){
		year = parseInt(year) - 1;
		<%-- 화면 데이터 입력--%>
		fnSetData();
		$('.monthMoveBtn.next').show();
	});
	
	<%-- 다음 달로 이동 --%>
	$('#yearNext').on('click', function(){
		year = parseInt(year) + 1;
		<%-- 화면 데이터 입력--%>
		fnSetData();
		<%-- 조회한 날짜가 현재일 경우 다음 날짜선택버튼 숨김 --%>
		if(year >= date.getFullYear()){
			$('.monthMoveBtn.next').hide();
		} else{
			$('.monthMoveBtn.next').show();
		}
	});
	
	<%-- 공장 값 변경 시 --%>
	$('#LOCATION').on('change', function(){
		<%-- 화면 데이터 입력--%>
		fnSetData();
	});
});

<%-- 화면 내 대시보드/차트 데이터 세팅 --%>
function fnSetData(){
	
	$("#yearMoveInfo").text(year + "년 ");
	LOCATION = $("#LOCATION").val();
	REMARKS = $("select[name=LOCATION] option:selected").attr("role");

	$.ajax({
		type : 'POST',
		url : '/epms/main/troubleListChartData.do',
		cache : false,
		dataType : 'json',
		data : { REMARKS 			: REMARKS
				,PARAM_LOCATION 	: LOCATION
				,PARAM_YEAR 		: year},
		async : false,
		error : function() {
			alert("처리중 오류가 발생 하였습니다.");
		},
		success : function(json) {
			
			thisYearID = [];
			lastYearID = [];
			
			var thisYearTroubleTot = [], thisYearTroublePart01 = [], thisYearTroublePart02 = [], thisYearTroublePart03 = [], thisYearTroublePart04 = [];  
			var lastYearTroubleTot = [], lastYearTroublePart01 = [], lastYearTroublePart02 = [], lastYearTroublePart03 = [], lastYearTroublePart04 = [];
			
			$.each(json.thisYearTroubleList, function(idx, val){
				thisYearTroubleTot.push(val.TOT);
				thisYearTroublePart01.push(val.PART01);
				thisYearTroublePart02.push(val.PART02);
				thisYearTroublePart03.push(val.PART03);
				thisYearTroublePart04.push(val.PART04);
				
				thisYearID.push(val.YYYYMM);
			});
			
			$.each(json.lastYearTroubleList, function(idx, val){
				lastYearTroubleTot.push(val.TOT);
				lastYearTroublePart01.push(val.PART01);
				lastYearTroublePart02.push(val.PART02);
				lastYearTroublePart03.push(val.PART03);
				lastYearTroublePart04.push(val.PART04);
				
				lastYearID.push(val.YYYYMM);
			});
			
			troubleListPart01Chart.data.datasets[0].label = year;
			troubleListPart01Chart.data.datasets[1].label = year-1;
			troubleListPart01Chart.data.datasets[0].data = thisYearTroublePart01;
			troubleListPart01Chart.data.datasets[1].data = lastYearTroublePart01;
			troubleListPart01Chart.update();
			
			troubleListPart02Chart.data.datasets[0].label = year;
			troubleListPart02Chart.data.datasets[1].label = year-1;
			troubleListPart02Chart.data.datasets[0].data = thisYearTroublePart02;
			troubleListPart02Chart.data.datasets[1].data = lastYearTroublePart02;
			troubleListPart02Chart.update();
			
			troubleListPart03Chart.data.datasets[0].label = year;
			troubleListPart03Chart.data.datasets[1].label = year-1;
			troubleListPart03Chart.data.datasets[0].data = thisYearTroublePart03;
			troubleListPart03Chart.data.datasets[1].data = lastYearTroublePart03;
			troubleListPart03Chart.update();
			
			troubleListPart04Chart.data.datasets[0].label = year;
			troubleListPart04Chart.data.datasets[1].label = year-1;
			troubleListPart04Chart.data.datasets[0].data = thisYearTroublePart04;
			troubleListPart04Chart.data.datasets[1].data = lastYearTroublePart04;
			troubleListPart04Chart.update();
			
			troubleListTotChart.data.datasets[0].label = year;
			troubleListTotChart.data.datasets[1].label = year-1;
			troubleListTotChart.data.datasets[0].data = thisYearTroubleTot;
			troubleListTotChart.data.datasets[1].data = lastYearTroubleTot;
			troubleListTotChart.update();
		
		}		
	});
}

<%-- 대시보드/차트 클릭 시 상세 실적 리스트 --%>
function fnOpenLayerWithGrid(chart){

	var STATUS = "";
	var PART = "";
	var companyId = "${companyId}";
	REMARKS = $("select[name=LOCATION] option:selected").attr("role");
	
	var firstPoint = "";
	
	if(chart == "troubleListTotChart"){
		var activePoints = troubleListTotChart.getElementAtEvent(event);
	    firstPoint = activePoints[0];
	    PART = "";
	    
	    $("#popMainRepairResultList_title").text(firstPoint._model.datasetLabel + "년 " + firstPoint._model.label + " 돌발정비 현황(전 파트)");
	    
	}else if(chart == "troubleListPart01Chart"){
		var activePoints = troubleListPart01Chart.getElementAtEvent(event);
	    firstPoint = activePoints[0];
	    PART = companyId+"_01";
	    $("#popMainRepairResultList_title").text(firstPoint._model.datasetLabel + "년 " + firstPoint._model.label + " 돌발정비 현황(기계)");
	    
	}else if(chart == "troubleListPart02Chart"){
		var activePoints = troubleListPart02Chart.getElementAtEvent(event);
	    firstPoint = activePoints[0];
	    PART = companyId+"_02";
	    $("#popMainRepairResultList_title").text(firstPoint._model.datasetLabel + "년 " + firstPoint._model.label + " 돌발정비 현황(전기)");
	    
	}else if(chart == "troubleListPart03Chart"){
		var activePoints = troubleListPart03Chart.getElementAtEvent(event);
	    firstPoint = activePoints[0];
	    PART = companyId+"_03";
	    $("#popMainRepairResultList_title").text(firstPoint._model.datasetLabel + "년 " + firstPoint._model.label + " 돌발정비 현황(시설)");
	    
	}else if(chart == "troubleListPart04Chart"){
		var activePoints = troubleListPart04Chart.getElementAtEvent(event);
	    firstPoint = activePoints[0];
	    PART = companyId+"_04";
	    $("#popMainRepairResultList_title").text(firstPoint._model.datasetLabel + "년 " + firstPoint._model.label + " 돌발정비 현황(생산)");
	}
	
	firstPoint._model.datasetLabel == year ? STATUS = thisYearID[firstPoint._index] : STATUS = lastYearID[firstPoint._index];
		
   	Grids.PopMainRepairResultList.Source.Data.Url = "/epms/main/popRepairResultListOptData.do?&DATE_REQUEST="+STATUS
												  + "&CHART=troubleListChart"
   												  + "&REMARKS="+REMARKS
   												  + "&LOCATION="+LOCATION
									   			  + "&PART="+PART
   											  	  + "&flag=Y";
	Grids.PopMainRepairResultList.Reload();
	fnModalToggle('popMainRepairResultList');
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

</script>

</body>
</html>