<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%--
	Class Name	: mainDashBoard.jsp
	Description : 설비관리 메인대시보드 화면
    author		: 서정민
    since		: 2018.06.01
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.06.01	 서정민		최초 생성

--%>
 <!DOCTYPE html>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<link  href="/css/com/web_v2/slickSlider/slick.css" rel="stylesheet" type="text/css">
<script src="/js/com/web_v2/lobipanel.js" type="text/javascript"></script>
<script src="/js/com/web/mainCommon.js" type="text/javascript"></script>
<script src="/js/chartjs/Chart.min.js" type="text/javascript"></script>
<script src="/css/com/web_v2/slickSlider/slick.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
	
	<%-- 전체 loading bar intercept 회피 --%>
	$.ajaxSetup({global: false});
	
	//포틀릿 옵션
	$('#lobipanel-multiple').find('.panel').lobiPanel({
        sortable: false,
        stateful: true,
		editTitle: false,
		reload: false,
		changeStyle: false,
		unpin: false
    });
	
	//스크롤 스타일 변경
	$(".boxscroll").niceScroll(".inboxscroll", {cursorwidth:"6px",cursorcolor:"#afbfd6"});
	$("body").on('click', function(){
		$(".boxscroll").getNiceScroll().resize();
	});
	
	//최초 로드시 expand 화면 전체 숨김
	$('.portlet-max').hide();
	
	<%-- 정비원 또는 팀장권한일 경우에만 상세페이지 조회 가능 --%>
	if("${fn:indexOf(sessionScope.ssSubAuth, 'AUTH02')}" < 0 && "${fn:indexOf(sessionScope.ssSubAuth, 'AUTH03')}" < 0){
		$('.expand').hide();
	}
	
	//expand 버튼 클릭시
	$('.expand').on('click', function(){
		
		var id = $(this).parent().parent().parent().parent().parent().find('.lobipanel-body').attr('id');
		if($(this).find('.fa-compress').length > 0){
		
			//포틀렛 max
			if(id == "mainRepairResultList"){ //정비실적 현황
				document.getElementById("mainRepairResultListIfrm").src = '/epms/main/mainRepairResultListDtl.do';
				$('#mainRepairResultList').find('.portlet-max').show();
				$('#mainRepairResultList').find('.portlet-min').hide();
			}else if(id == "mainTroubleList"){ //돌발정비 현황
				document.getElementById("mainTroubleListIfrm").src = '/epms/main/mainTroubleListDtl.do';
				$('#mainTroubleList').find('.portlet-max').show();
				$('#mainTroubleList').find('.portlet-min').hide();
			}else if(id == "mainMtbfMttr"){ //MTBF.MTTR
  				document.getElementById("mainMtbfMttrIfrm").src = '/epms/main/mainMtbfMttrDtl.do';
				$('#mainMtbfMttr').find('.portlet-max').show();
				$('#mainMtbfMttr').find('.portlet-min').hide();
			}else if(id == "mainMaterialList"){ //재고부족 자재
				document.getElementById("mainMaterialListIfrm").src = '/epms/material/order/materialOrderList.do';
				$('#mainMaterialList').find('.portlet-max').show();
				$('#mainMaterialList').find('.portlet-min').hide();
// 			}else if(id == "mainPreventiveList"){ //예방보전 현황
// 				document.getElementById("mainPreventiveListIfrm").src = '/epms/main/mainPreventiveListDtl.do';
// 				$('#mainPreventiveList').find('.portlet-max').show();
// 				$('#mainPreventiveList').find('.portlet-min').hide();
			}else if(id == "mainMaterialMasterList"){ //자재 재고현황
				document.getElementById("mainMaterialMasterListIfrm").src = '/epms/material/order/materialOrderList.do';
				$('#mainMaterialMasterList').find('.portlet-max').show();
				$('#mainMaterialMasterList').find('.portlet-min').hide();
			}else if(id == "mainWorkStatList"){ //정비원 업무현황
				document.getElementById("mainWorkStatListIfrm").src = '/epms/main/mainWorkStatListDtl.do';
				$('#mainWorkStatList').find('.portlet-max').show();
				$('#mainWorkStatList').find('.portlet-min').hide();
			}else if(id == "mainMyReqList"){ //내요청현황
				document.getElementById("mainMyReqListIfrm").src = '/epms/main/mainMyReqListDtl.do';
				$('#mainMyReqList').find('.portlet-max').show();
				$('#mainMyReqList').find('.portlet-min').hide();
			}
				
		}else{
			
			//포틀렛 min
			if(id == "mainRepairResultList"){ //정비실적 현황
				document.getElementById("mainRepairResultListIfrm").src = '';
				$('#mainRepairResultList').find('.portlet-min').show();
				$('#mainRepairResultList').find('.portlet-max').hide();
			}else if(id == "mainTroubleList"){ //돌발정비 현황
				document.getElementById("mainTroubleListIfrm").src = '';
				$('#mainTroubleList').find('.portlet-min').show();
				$('#mainTroubleList').find('.portlet-max').hide();
			}else if(id == "mainMtbfMttr"){ //MTBF.MTTR
				document.getElementById("mainMtbfMttrIfrm").src = '';
				$('#mainMtbfMttr').find('.portlet-min').show();
				$('#mainMtbfMttr').find('.portlet-max').hide();
			}else if(id == "mainMaterialList"){ //재고부족 자재
				document.getElementById("mainMaterialListIfrm").src = '';
				$('#mainMaterialList').find('.portlet-min').show();
				$('#mainMaterialList').find('.portlet-max').hide();
			}else if(id == "mainPreventiveList"){ //예방보전 현황
				document.getElementById("mainPreventiveListIfrm").src = '';
				$('#mainPreventiveList').find('.portlet-min').show();
				$('#mainPreventiveList').find('.portlet-max').hide();
			}else if(id == "mainMaterialMasterList"){ //자재 재고현황
				document.getElementById("mainMaterialMasterListIfrm").src = '';
				$('#mainMaterialMasterList').find('.portlet-min').show();
				$('#mainMaterialMasterList').find('.portlet-max').hide();
			}else if(id == "mainWorkStatList"){ //정비원 업무현황
				document.getElementById("mainWorkStatListIfrm").src = '';
				$('#mainWorkStatList').find('.portlet-min').show();
				$('#mainWorkStatList').find('.portlet-max').hide();
			}else if(id == "mainMyReqList"){ //내요청현황
				document.getElementById("mainMyReqListIfrm").src = '';
				$('#mainMyReqList').find('.portlet-min').show();
				$('#mainMyReqList').find('.portlet-max').hide();
			}
			
		}
	});
	
	$("#mainTroubleList").on('click', '.troubleTablinks', function(){
		$('#mainTroubleList .troubleTablinks').removeClass("active");
		$(this).addClass("active");
		
		var eqCnt = 0;
		$('#mainTroubleList .troubleTablinks').each(function(i){
			if($(this).hasClass("active") == true) 
				eqCnt = i
		});
		
		$('#mainTroubleList .troubleTabcontent').hide();
		$('#mainTroubleList .troubleTabcontent').eq(eqCnt).show();
		
	});
	
	$("#mainWorkStatList").on('click', '.workerTablinks', function(){
		$('#mainWorkStatList .workerTablinks').removeClass("active");
		$(this).addClass("active");
		
		var eqCnt = 0;
		$('#mainWorkStatList .workerTablinks').each(function(i){
			if($(this).hasClass("active") == true) 
				eqCnt = i
		});
		
		$('#mainWorkStatList .workerTabcontent').hide();
		$('#mainWorkStatList .workerTabcontent').eq(eqCnt).show();
		
	});
	
	$(".mainSlider").slick({
		autoplay: true, //자동슬라이드
		pauseOnHover: false,
		dots: true,
		speed: 1000,
		autoplaySpeed: 10000,
		arrows: true,
		prevArrow: '<div class="slick-prev"><img src="/images/com/web_v2/sliderPrev.png"></div>',
	    nextArrow: '<div class="slick-next"><img src="/images/com/web_v2/sliderNext.png"></div>',
	    fade: true
	});
	
	var xpaused = false;

	$(".mainSliderPlayBtn").on("click", function() {
		if( xpaused ) {
	  	$(".mainSlider").slick("play");
	  	$(".mainSliderPlayBtn").text("슬라이드 정지");
	  } else {
	  	$(".mainSlider").slick("pause");
	  	$(".mainSliderPlayBtn").text("슬라이드 재생");
	  }
	  xpaused = !xpaused;
	  $(this).toggleClass("paused");
	});
	
	<%-- 공지팝업 --%>
	if(parent.counter<=1){
		openWin('noticePop');	
	}
	
	<%-- //////////////////////////////////////////////// --%>
	<%-- ///////////// 대시보드 데이터 호출 시작 ///////////// --%>
	<%-- /////////////////////////////////////////////// --%>
	
	var portletStatus = {"1":"ON","2":"ON","3":"ON","4":"ON","5":"ON","6":"ON","7":"ON","8":"ON"};
	
	<%-- 도메인 저장소 값 유무 체크 --%>
	if(localStorage.length > 0){
		$.each(localStorage, function(i, o) {
			var object = "";
			
			if(typeof o === 'string'){
				
				//console.log('# mainDashBoard.jsp [localStorage]' + localStorage + '(' + o + ') ');
				// 문자열 " " (Double Quotation Marks) 처리  =  Unexpected token # in JSON at position 0
				//object = JSON.parse( '"' + o + '"' );
				
				<%-- onClose된 포틀릿이 있다면 OFF로 변경--%>
				if(object.state == "onClose"){
					var key = i.substr(25);
					portletStatus[key] = 'OFF';
				}
			}
		});
	}
	
	<%-- localStorage의 포틀릿 상태가 onClose인 경우를 제외하고 Chartjs 실행 --%>
	if(portletStatus['1'] == 'ON') chartDataAjax('repairResult');	<%-- 정비실적 현황 그래프 --%>
	if(portletStatus['2'] == 'ON') chartDataAjax('repairEM');		<%-- 돌발정비 현황 정보--%>
	if(portletStatus['3'] == 'ON') fnGetMtbfMttrInfo();				<%-- MTBF/MTTR 정보 --%>
<%-- 	if(portletStatus['4'] == 'ON') materialShortageAjax();			재고부족 자재 목록 --%>
<%-- 	if(portletStatus['5'] == 'ON') chartDataAjax('preventive');		예방보전 현황 그래프 --%>
	if(portletStatus['6'] == 'ON') fnGetWorkerInfo();				<%-- 정비원 업무현황 정보--%>
	if(portletStatus['7'] == 'ON') fnGetRepairRequestList();		<%-- 내 요청 현황 목록 --%>
<%-- 	if(portletStatus['8'] == 'ON') materialMasterAjax();			자재 재고현황 목록 --%>
		
	<%-- /////////////////////////////////////////////// --%>
	<%-- ///////////// 대시보드 데이터 호출 끝 ////////////// --%>
	<%-- ////////////////////////////////////////////// --%>
});

<%-- 정비원 목록 부서별 탭 생성 --%>
function fnGetWorkerInfo(){
	$.ajax({
		type : 'POST',
		url : '/epms/main/wokerInfo.do',
		cache : false,
		dataType : 'json',
		data : {},
		async : false,
		error : function() {
			alert("처리중 오류가 발생 하였습니다.");
		},
		success : function(json) {
			
			var _div = $("#mainWorkStatList").find("div.workerTabcontentWrap .inboxscroll");
			
			$.each(json.partInfo, function(i, o) {
				var _template = $("#workerTabTemplate").html();
				_template = $(_template);
				for( var name in o ) {
	            	switch (name) {
            			case "NAME" :
            				var activeClass = "";
            				if(o.CODE == json.ssPart) {
            					activeClass = "active";
            				} else if(o.CODE == "ALL" && json.ssPart == "") {
            					activeClass = "active";
            				}
            				_div.parent().prev('div.workerTab').append('<button class="workerTablinks ' + activeClass + '" data-part="' + o["CODE"] + '">' + o[name] + '</button>');
            				break;
		 				default:
		 					_template.find("[name=" + name + "]").text(o[name]);
		 				break;
	 				}
				}
				
				var _ul = _template.find("ul.lobipanel-list");
				$.each(json.wokerInfo, function(idx, val) {
					if(o.CODE == "ALL" || o.CODE == val.PART){
						var _li = $("#workerInfoTemplate").html();
						_li = $(_li);
						for( var name in val ) {
			            	switch (name) {
			            		case "USER_NAME" :
			            			var job_grade_name = (!val["JOB_GRADE_NAME"]) ? "" : val["JOB_GRADE_NAME"];
			            			_li.find("[name=user]").text(val["USER_NAME"] + "\t" + job_grade_name);
			            			break;
			            		case "PART_NAME" :
			            			var division_name = (!val["DIVISION_NAME"]) ? "" : val["DIVISION_NAME"];
			            			_li.find("[name=dep]").text(val["PART_NAME"] + "파트 \t" + division_name);
			            			break;
			            		case "MEMBER_FILEURL" :
			            			_li.find("[name=MEMBER_FILEURL]").attr("src", val[name]);
				 				default:
				 					_li.find("[name=" + name + "]").text(val[name]);
				 				break;
			 				}
						}
						_ul.append(_li);
					}
				});
				_div.append(_template);
			});
			
			var eqCnt = 0;
			$('#mainWorkStatList .workerTablinks').each(function(i){
				if($(this).hasClass("active") == true) 
					eqCnt = i
			});
			
			$('#mainWorkStatList .workerTabcontent').hide();
			$('#mainWorkStatList .workerTabcontent').eq(eqCnt).show();
			
// 			var ssPart = json.ssPart;
// 			if(ssPart == '') {
// 				_div.find('.workerTabcontent').eq(0).show();
// 			} else {
// 				ssPart = ssPart.substr(6,7);
// 				alert(ssPart);
// 				_div.find('.workerTabcontent').eq(ssPart).show();
// 			}
			$('divMainWorkStatList').show();
		}		
	});
}

<%-- MTBF/MTTR 정보 --%>
function fnGetMtbfMttrInfo(){
	$.ajax({
		type : 'POST',
		url : '/epms/main/mtbfMttrInfo.do',
		cache : false,
		dataType : 'json',
		data : {},
		async : false,
		error : function() {
			alert("처리중 오류가 발생 하였습니다.");
		},
		success : function(json) {
			var _div = $("#mainMtbfMttr");
			var preMonthAvailability;
			$.each(json.mtbfMttrInfo, function(i, o) {
				for( var name in o ) {
					switch (name) {
						case "AVAILABILITY" :
							if(i == 0) {
								_div.find("[name=" + name + "]").text(o[name]);
							} else {
								prevAvailability = o[name] - _div.find("[name=" + name + "]").text();
								var updownClass = "";
								
								if( prevAvailability > 0 ) {
									updownClass = "up";
									prevAvailability = "+" + prevAvailability.toFixed(2);
								} else if( prevAvailability < 0) {
									updownClass = "down";
									prevAvailability = prevAvailability.toFixed(2);
								} else if (prevAvailability == 0){
									prevAvailability = "-";
								}
								_div.find("[name=" + name + "]").text(o[name] + "%").next().addClass(updownClass).children().text(prevAvailability + "%p");
								_div.parent().find(".availabilityBar-inner").attr("data-percent", o[name] + "%");
		            			
								<%-- MTBF MTTR 가용도 그래프 --%>
								var _bars = [].slice.call(document.querySelectorAll('.availabilityBar-inner'));
								_bars.map(function(bar, index) {
									bar.style.width = bar.getAttribute('data-percent');
								});
							}
							break;
						default:
							_div.find("[name=" + name + "]").text(o[name]);
							break;
					}
				}
			});
			$('#divMainMtbfMttr').show();
		}		
	});
}

<%-- 차트 관련 Ajax --%>
function chartDataAjax(type) {
	$.ajax({
		type : 'POST',
		url : '/epms/main/' + type + 'ChartData.do',
		cache : false,
		dataType : 'json',
		data : {},
		async : false,
		error : function() {
			alert("처리중 오류가 발생 하였습니다.");
		},
		success : function(json) {
			if(type == "preventive") {
				preventiveChart(json);
				$('#divPreventiveChart').show();
			}else if(type == "repairResult") {
				repairResultChart(json);
				$('divMainRepairResultList').show();
			}else if(type == "repairEM"){
				repairEMChart(json);
			}
		}		
	});
}

<%-- 돌발정비 현황 그래프 작성 --%>
function repairEMChart(json) {
	
	var _div = $("#mainTroubleList").find("div.troubleTabset");
	
	$.each(json.repairEM, function(idx, arr){
		<%-- 차트ID 생성 --%>
		var canvasID = '';
		if(idx == '0') canvasID = 'repairEMChartTot';
		else canvasID = 'repairEMChart' + idx;
		
		<%-- 템플릿 생성 --%>
		var _template = $("#repairEMTemplate").html();
		_template = $(_template);
		
		<%-- 탭 생성 --%>
		for( var name in arr ) {
        	switch (name) {
    			case "PART_NAME" :
    				var activeClass = "";
    				
    				if(arr.PART == json.ssPart) {
    					activeClass = "active";
    				} else if((arr.PART == undefined || arr.PART == "") && json.ssPart == "") {
    					activeClass = "active";
    				}
    				_div.find('div.troubleTab').append('<button class="troubleTablinks ' + activeClass + '" data-part="' + arr["PART"] + '">' + arr[name] + '</button>');
    				break;
				}
		}
		
		<%-- 차트 생성 --%>
		_template.find("[id=canvas]").html("<canvas id=" + canvasID + "></canvas>");
		_div.append(_template);
		
		var ctx = document.getElementById(canvasID);
		
		var data = [arr.JAN, arr.FEB, arr.MAR, arr.APR, arr.MAY, arr.JUN, arr.JUL, arr.AUG, arr.SEP, arr.OCT, arr.NOV, arr.DEC];
		
		var backgroundColours = [];
		var date = new Date();
		var month = date.getMonth();
		for(var i=0; i<data.length; i++){
			if(i == month){
				backgroundColours[i] = "rgba(244, 208, 12, 1)";
			}
			else{
				backgroundColours[i] = "rgba(0, 147, 209, 1)";
			}
		}
		
		var myChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: ["1월","2월","3월","4월","5월","6월","7월","8월","9월","10월","11월","12월"],
				datasets: [{
					label: '2018',			
					data: data,
					backgroundColor: backgroundColours,
					}]
			},
		    options: {
		    	maintainAspectRatio: false,
					responsive: true,
					tooltips: {
						enabled: true,
						intersect: false,
						titleFontSize: 11,
						bodyFontSize: 11
					},
					hover :{
						animationDuration:0
					},
					scales: {
						yAxes: [{
							stacked: true,
							gridLines: {
							},
							ticks: {
								beginAtZero:true,
								fontFamily: "'Open Sans Bold', sans-serif",
								fontSize:11
							},
							scaleLabel:{
								display:false
							}
						}],
						xAxes: [{
							barPercentage: 0.7,
							gridLines: {
								display:false,
									color: "#fff",
									zeroLineColor: "#fff",
									zeroLineWidth: 0
							},
							ticks: {
								fontFamily: "'Open Sans Bold', sans-serif",
								fontSize:11
							}
						}]
					},
					legend:{
						display:false,
						position : 'right',
						labels:{
							fontFamily: "'Open Sans Bold', sans-serif",
							fontSize:11
						}
					}
				}
		});
	});
	var eqCnt = 0;
	$('#mainTroubleList .troubleTablinks').each(function(i){
		if($(this).hasClass("active") == true) 
			eqCnt = i
	});
	
	$('#mainTroubleList .troubleTabcontent').hide();
	$('#mainTroubleList .troubleTabcontent').eq(eqCnt).show();
// 	var ssPart = json.ssPart;
// 	if(ssPart == '') {
// 		_div.find('.troubleTabcontent').eq(0).show();
// 	} else {
// 		ssPart = ssPart.substr(5,6);
// 		_div.find('.troubleTabcontent').eq(ssPart).show();
// 	}
	$('divMainTroubleList').show();
}

<%-- 예방보전 현황 그래프 작성 --%>
function preventiveChart(json) {
	var comCode = json.comCode;   <%-- PREVENTIVE_STATUS :  01 = 대기 , 02 = 정비필요, 03 = 정비완료, 09 = 미점검완료 --%>
	var ctx = document.getElementById('preventive');
	var part = [], complete = [], uncheck = [], plan = [];
	$.each(json.preventive, function(i, o){
		part.push(o.PART_NAME);
		complete.push(o.COMPLETE_CNT);
		uncheck.push(o.UNCHECK_CNT);
		plan.push(o.PLAN_CNT);
	});
	var myChart = new Chart(ctx, {
		type: 'horizontalBar',
		data: {
			labels: part,
			datasets: [{
				label: [comCode['03']],			
				data: complete,
				backgroundColor: "rgba(0, 147, 209, 1)",
				hoverBackgroundColor: "rgba(0, 147, 209, 0.5)"
				},{
				label: [comCode['01']],
				data: plan,
				backgroundColor: "rgba(244, 208, 12, 1)",
				hoverBackgroundColor: "rgba(2244, 208, 12, 0.5)"
				},{
				label: [comCode['09']],
				data: uncheck,
				backgroundColor: "rgba(242, 99, 95, 1)",
				hoverBackgroundColor: "rgba(242, 99, 95, 0.5)"
				}]
		},
		options: chartOption
	});
}

<%-- 정비실적 현황 그래프 작성 --%>
function repairResultChart(json) {
	var ctx = document.getElementById('repairResult');
	var part = [], request = [], complete = [], em = [], ongoing = [], excess = [], appr = [], reject = [], cancel = [];
	$.each(json.repairResult, function(i, o){
		part.push(o.PART_NAME);
		request.push(o.REPAIR_REQUEST_CNT);
		complete.push(o.REPAIR_COMPLETE_CNT);
		em.push(o.REPAIR_EM_CNT);
		ongoing.push(o.REPAIR_ONGOING_CNT);
		excess.push(o.REPAIR_EXCESS_CNT);
		appr.push(o.REPAIR_APPR_CNT);
		reject.push(o.REPAIR_REJECT_CNT);
		cancel.push(o.REPAIR_CANCEL_CNT);
	});
	var myChart = new Chart(ctx, {
		type: 'horizontalBar',
		data: {
			labels: part,
			datasets: [{
				label: ["<spring:message code='epms.repair.complete' />"],  // 정비완료
				data: complete,
				backgroundColor: "rgba(0, 147, 209, 1)",
				hoverBackgroundColor: "rgba(0, 147, 209, 0.5)"
				},{
				label: ['진행중'],
				data: ongoing,
				backgroundColor: "rgba(244, 208, 12, 1)",
				hoverBackgroundColor: "rgba(244, 208, 12, 0.5)"
				},{
				label: ['결제대기'],			
				data: appr,
				backgroundColor: "rgba(198, 200, 202, 1)",
				hoverBackgroundColor: "rgba(198, 200, 202, 0.5)"
				},{
				label: ['반려'],
				data: reject,
				backgroundColor: "rgba(242, 99, 95, 1)",
				hoverBackgroundColor: "rgba(242, 99, 95, 0.5)"
				},{
				label: ["<spring:message code='epms.work.cancel' />"],  // 정비취소
				data: cancel,
				backgroundColor: "rgba(241, 75, 35, 1)",
				hoverBackgroundColor: "rgba(241, 75, 35, 0.5)"
				}]
		},
		options: chartOption
	});
}


var chartOption = {
	maintainAspectRatio: false,
	responsive: true,
	tooltips: {
		enabled: true,
		intersect: false,
		titleFontSize: 11,
		bodyFontSize: 11
	},
	hover :{
		animationDuration:0
	},
	scales: {
		xAxes: [{
			ticks: {
				beginAtZero:true,
				fontFamily: "'Open Sans Bold', sans-serif",
				fontSize:11
			},
			scaleLabel:{
				display:false
			},
			gridLines: {
			}, 
			stacked: true
		}],
		yAxes: [{
			gridLines: {
				display:false,
					color: "#fff",
					zeroLineColor: "#fff",
					zeroLineWidth: 0
			},
			ticks: {
				fontFamily: "'Open Sans Bold', sans-serif",
				fontSize:11
			},
			stacked: true
		}]
	},
	legend:{
		display:false,
		position : 'right',
		labels:{
			fontFamily: "'Open Sans Bold', sans-serif",
			fontSize:11
		}
	}
}

<%-- 재고부족 자재 --%>
function materialShortageAjax(){
	
	var html = "";
	$.ajax({
		type : 'POST',
		url : '/epms/main/materialShortageAjax.do',
		dataType : 'json',
		async : false,
		success : function(json) {
			if(json.materialShortageList != "" && json.materialShortageList != null) {
				(json.materialShortageList).forEach(function (item){
					
					html += '<tr>';
					html +=		'<td>';
					html +='		<span class="lobi-ellipsis main">'
					html +='			<span class="lobi-list-title wd-per-90">'+ item.MATERIAL_UID +'</span>';
					html +='		</span>';
					html +='	</td>';
					html +='	<td>';
					html +='		<span class="lobi-ellipsis main">';
					html +='			<span class="lobi-list-title wd-per-90">'+ item.MATERIAL_NAME +'</span>';
					html +='		</span>';
					html +='	</td>';
					html +='	<td>';
					html +='		<span>'+ item.STOCK_OPTIMAL +'</span>';
					html +='	</td>';
					html +='	<td>';
					html +='		<span>'+ item.STOCK +'</span>';
					html +='	</td>';
					html +='	<td>';
					html +='		<span>'+ item.PASS_DATE +'</span>';
					html +='	</td>';
					html +='</tr>';
				});
				
				$('#materialShortageList').html(html);
				
			} else {
				html += '<div class="empty-state">';
				html += 	'<p class="empty-state-copy">조회된 데이터가 없습니다.</p>';
				html += '</div>';
				
				$('#tbodyMaterialShortageList').html(html);
			}
			
			$('#divMainMaterialList').show();
		},
		error : function() {
			alert("처리중 오류가 발생 하였습니다.");
		}
	});
}

<%-- 자재 재고현황 --%>
function materialMasterAjax(){
	var html = "";
	$.ajax({
		type : 'POST',
		url : '/epms/main/materialMasterAjax.do',
		dataType : 'json',
		async : false,
		success : function(json) {
			if(json.materialMasterList != "" && json.materialMasterList != null) {
				(json.materialMasterList).forEach(function (item){
					html += '<tr>';
					html +=		'<td>';
					html +='		<span class="lobi-ellipsis main">'
					html +='			<span class="lobi-list-title wd-per-90">'+ item.MATERIAL_UID +'</span>';
					html +='		</span>';
					html +='	</td>';
					html +='	<td>';
					html +='		<span class="lobi-ellipsis main">';
					html +='			<span class="lobi-list-title wd-per-90">'+ item.MATERIAL_NAME +'</span>';
					html +='		</span>';
					html +='	</td>';
					html +='	<td>';
					html +='		<span>'+ item.STOCK_OPTIMAL +'</span>';
					html +='	</td>';
					html +='	<td>';
					html +='		<span>'+ item.STOCK +'</span>';
					html +='	</td>';
					html +='	<td>';
					html +='		<span>'+ item.PASS_DATE +'</span>';
					html +='	</td>';
					html +='</tr>';
					
				});
				
				$('#materialMasterList').html(html);

			} else {
				html += '<div class="empty-state">';
				html += 	'<p class="empty-state-copy">조회된 데이터가 없습니다.</p>';
				html += '</div>';
				
				$('#tbodyMaterialMasterList').html(html);
			}
			
			$('#divMainMaterialMasterList').show();
			
		},
		error : function() {
			alert("처리중 오류가 발생 하였습니다.");
		}
	});
}

<%-- 내 요청 현황 목록 --%>
function fnGetRepairRequestList(){
	
	var html = "";
	
	$.ajax({
		type : 'POST',
		url : '/epms/main/repairRequestList.do',
		dataType : 'json',
		async : false,
		processData : false,
		contentType : false,
		success : function(json) {
			if(json.REQUEST_LIST != "" && json.REQUEST_LIST != null) {
				(json.REQUEST_LIST).forEach(function (item){
					html += '<div class="myReqLst">';
					if(item.APPR_STATUS == '01'){
					html +=		'<div class="myReqLstTit waiting">';
					html +=			'<div class="f-l">'
					html +=				'<div class="port-status waiting">';
					}else if(item.APPR_STATUS == '02' && item.REPAIR_STATUS == '01'){
					html +=		'<div class="myReqLstTit accept">';
					html +=			'<div class="f-l">'
					html +=				'<div class="port-status accept">';
					}else if(item.APPR_STATUS == '02' && item.REPAIR_STATUS == '02'){
					html +=		'<div class="myReqLstTit assign">';
					html +=			'<div class="f-l">'
					html +=				'<div class="port-status assign">';
					}else if(item.APPR_STATUS == '02' && item.REPAIR_STATUS == '03'){
					html +=		'<div class="myReqLstTit complete">';
					html +=			'<div class="f-l">'
					html +=				'<div class="port-status complete">';
					}else if(item.APPR_STATUS == '03' || item.APPR_STATUS == '04'){
					html +=		'<div class="myReqLstTit reject">';
					html +=			'<div class="f-l">'
					html +=				'<div class="port-status reject">';
					}else if(item.APPR_STATUS == '09'){
					html +=		'<div class="myReqLstTit reqCancel">';
					html +=			'<div class="f-l">'
					html +=				'<div class="port-status reqCancel">';
					}
					html +=					'<span>' + item.STATUS_NAME + '</span>';
					html +=				'</div>'
					html +=			'</div>'
					html +=			'<div class="f-l">'
					html +=				'<p class="materialName">' + item.EQUIPMENT_UID + '<br>' + item.EQUIPMENT_NAME + '</p>';
					html +=				'<p class="materialPst">' + item.LOCATION_NAME + ' > ' + item.LINE_NAME + '</p>';
					html +=			'</div>';
					html +=			'<div class="f-r">';
					html +=				'처리파트 : ' + item.PART_NAME;
					html +=			'</div>';
					html +=		'</div>';
					html +=		'<div class="myReqLstCon">';
					html +=			'<div class="timeline">';
					html +=				'<div class="timelineBody">';
					html +=					'<span class="timelineImg"><img src="' + item.REQUEST_MEMBER_FILEURL + '" alt="'+ item.REQUEST_MEMBER_GRADE +'" onerror="this.src=\'/images/com/web_v2/userIcon.png\'"></span>';
					html +=					'<div class="timelineInfo">';
					html +=						'<p class="timelineUser">' + item.REQUEST_MEMBER_NAME + ' ' + item.REQUEST_MEMBER_GRADE + ' <span>' + item.REQUEST_MEMBER_DIV + '</span></p>';
					html +=						'<p class="timelineTime">' + item.DATE_REQUEST_HHMI + ' | ' + item.DATE_REQUEST_YYMMDD + '</p> ';
					html +=					'</div>';
					html +=					'<div class="timelineMsg">' + item.REQUEST_DESCRIPTION + '</div>';
					html +=				'</div>';
					html +=			'</div>';
					if(item.APPR_STATUS != '01' && item.APPR_STATUS != '09'){
					html +=			'<div class="timeline">';
					html +=				'<div class="timelineBody">';
					html +=					'<span class="timelineImg"><img src="' + item.RESPONSE_MEMBER_FILEURL + '" alt="'+ item.RESPONSE_MEMBER_GRADE +'" onerror="this.src=\'/images/com/web_v2/userIcon.png\'"></span>';
					html +=					'<div class="timelineInfo">';
					html +=						'<p class="timelineUser">' + item.RESPONSE_MEMBER_NAME + ' ' + item.RESPONSE_MEMBER_GRADE + ' <span>' + item.RESPONSE_MEMBER_DIV + '</span></p>';
					html +=						'<p class="timelineTime">' + item.DATE_RESPONSE_HHMI + ' | ' + item.DATE_RESPONSE_YYMMDD + '</p> ';
					html +=					'</div>';
					html +=					'<div class="timelineMsg answer">' + item.RESPONSE_DESCRIPTION + '</div>';
					html +=				'</div>';
					html +=			'</div>';
					}
					html +=		'</div>';
					html +=	'</div>';
				});
				
			} else {
				html += '<div class="empty-state">';
				html += 	'<p class="empty-state-copy">조회된 데이터가 없습니다.</p>';
				html += '</div>';
			}
			
			$('#mainMyReqListDetail').html(html);
			$('#divMainMyReqList').show();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			submitBool = true;
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
		}
	});
}
</script>

</head>
<body>
	
	<!-- start : 위젯 -->
	<div id="lobipanelWrap">
		<div id="lobipanel">
			<div id="lobipanel-multiple">
			
			
				<div class="mainSlider slider" id="divSlider">
					<!-- s : slide 1page -->
					<div class="lobi-row">
 						<div class="f-l wd-per-67">
							<div class="lobi-col lobipanel-parent" data-inner-id="lobipanel-parent1">
								<div class="panel" data-inner-id="lobipanel-panel1" id="divMainRepairResultList" style="display:none;">
									<div class="panel-heading">
										<div class="panel-title">
											<h4><spring:message code='epms.work.perform' /> 현황</h4><!-- 정비실적 현황 -->
										</div>
									</div>
									<div class="lobipanel-body" id="mainRepairResultList">
										<div class="portlet-min hgt-per-100">
											<div class="portletChart">
												<canvas id="repairResult"></canvas>
											</div>
										</div>
										<div class="portlet-max hgt-per-100">
											<iframe id="mainRepairResultListIfrm" name="portletMaxIfrm" marginwidth="0px" marginheight="0px" frameborder="no"  scrolling="no" align="center" style="width:100%;"
											 src=""></iframe>
										</div>
									</div>
								</div>
							</div>
							<div class="lobi-col lobipanel-parent" data-inner-id="lobipanel-parent2">
								<div class="panel" data-inner-id="lobipanel-panel3" id="divMainMtbfMttr" style="display:none;">
									<div class="panel-heading">
										<div class="panel-title">
											<h4>MTBF/MTTR</h4>
										</div>
									</div>
									<div class="lobipanel-body" id="mainMtbfMttr">
										<div class="portlet-min">
											<div class="mtWrap">
												<div class="mtWrapTop overflow">
													<div class="f-l">
														<div class="mtTit">MTBF (평균고장간격)</div>
														<div class="mtCon"><span name="MTBF"></span> (시간)</div>
													</div>
													<div class="f-l">
														<div class="mtTit">MTTR (평균수리시간)</div>
														<div class="mtCon"><span name="MTTR"></span> (분)</div>
													</div>
												</div>
												<div class="mtWrapBtm">
													<div class="availabilityTit">가용도</div>
													<div class="availabilityPer" name="AVAILABILITY"></div>
													<div class="availabilityCompare">전월대비<span name="AVAILABILITY_DIFF"></span></div>
													<div class="availabilityBar">
														<div class="availabilityBar-inner" data-percent=""></div>
													</div>
												</div>
											</div>
										</div>
										<div class="portlet-max hgt-per-100">
											<iframe id="mainMtbfMttrIfrm" name="portletMaxIfrm" marginwidth="0px" marginheight="0px" frameborder="no"  scrolling="no" align="center" style="width:100%;"
											 src=""></iframe>
										</div>
									</div>
								</div>
							</div>
							<div class="lobi-col lobipanel-parent wd-per-100" data-inner-id="lobipanel-parent3">
								<div class="panel" data-inner-id="lobipanel-panel2" id="divMainTroubleList" style="display:none;">
									<div class="panel-heading">
										<div class="panel-title">
											<h4><spring:message code='epms.work.urgency' /> 현황</h4><!-- 돌발정비 현황 -->
										</div>
									</div>
									<div class="lobipanel-body" id="mainTroubleList">
										<div class="portlet-min hgt-per-100">
											<div class='troubleTabset hgt-per-100'>
												<div class="troubleTab">
												</div>
											</div>
										</div>
										<div class="portlet-max hgt-per-100">
											<iframe id="mainTroubleListIfrm" name="portletMaxIfrm" marginwidth="0px" marginheight="0px" frameborder="no"  scrolling="no" align="center" style="width:100%;"
											 src=""></iframe>
										</div>
									</div>
								</div>
							</div>
 						</div>
						<div class="lobi-col lobipanel-parent wd-per-33" data-inner-id="lobipanel-parent4">
							<div class="panel" data-inner-id="lobipanel-panel6" id="divMainWorkStatList" style="display:none;">
								<div class="panel-heading">
									<div class="panel-title">
										<h4><spring:message code='epms.worker' /> 업무현황</h4><!-- 정비원 업무현황 -->
									</div>
								</div>
								<div class="lobipanel-body type02 " id="mainWorkStatList">
									<div class="portlet-min hgt-per-100">
										<div class="workerTabset hgt-per-100">
											<div class="workerTab">
											</div>
											<div class="workerTabcontentWrap boxscroll">
												<div class="inboxscroll">
													
												</div>
											</div>
										</div>
									</div>
									<div class="portlet-max hgt-per-100">
										<iframe id="mainWorkStatListIfrm" name="portletMaxIfrm" marginwidth="0px" marginheight="0px" frameborder="no"  scrolling="no" align="center" style="width:100%;"
										 src=""></iframe>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- e : slide 1page -->
					
					<!-- s : slide 2page -->
					<c:if test="${fn:length(locationList) > 0}">
						<c:forEach var="item" items="${locationList}">
							<div class="lobi-row">
								<div class="lobi-col type02 lobipanel-parent" data-inner-id="lobipanel-parent5">
									<div class="panel">
										<div class="panel-heading">
											<div class="panel-title">
												<h4><spring:message code='epms.work.perform' /> 현황 상세 리포트</h4><!-- 정비실적 현황 상세 리포트 -->
											</div>
										</div>
										<div class="lobipanel-body type02">
											<div class="hgt-per-100">
												<iframe  style="width:100%;height:100%;" src="/epms/main/mainRepairResultListDtl.do?type=mainSlide&LOCATION=${item.CODE}" marginwidth="0px" marginheight="0px" frameborder="no"  scrolling="no" align="center"></iframe>
											</div>
										</div>
									</div>
								</div>
							</div>
						
						</c:forEach>
					</c:if>
					
					<!-- e : slide 2page -->
					
					<!-- s : slide 3page -->
<!-- 					<div class="lobi-row"> -->
<!-- 						<div class="lobi-col type02 lobipanel-parent" data-inner-id="lobipanel-parent6"> -->
<!-- 							<div class="panel"> -->
<!-- 								<div class="panel-heading"> -->
<!-- 									<div class="panel-title"> -->
<!-- 										<h4>예방보전 현황 상세 리포트</h4> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 								<div class="lobipanel-body type02"> -->
<!-- 									<div class="hgt-per-100"> -->
<!-- 										<iframe  style="width:100%;height:100%;" src="/epms/main/mainPreventiveListDtl.do" marginwidth="0px" marginheight="0px" frameborder="no"  scrolling="no" align="center"></iframe> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 					</div> -->
					<!-- e : slide 3page -->
					
				</div>
				
				<div class="mainSliderPlayBtn">슬라이드 정지</div>
				

<%-- 							<div class="panel" data-inner-id="lobipanel-panel5" id="divPreventiveChart" style="display:none;">
								<div class="panel-heading">
									<div class="panel-title" id="preventiveChart">
										<h4>예방보전 현황</h4>
									</div>
								</div>
								<div class="lobipanel-body type02" id="mainPreventiveList">
									<div class="portlet-min hgt-per-100">
										<div class="portletChart">
											<canvas id="preventive"></canvas>
										</div>
									</div>
									<div class="portlet-max hgt-per-100">
										<iframe id="mainPreventiveListIfrm" name="portletMaxIfrm" marginwidth="0px" marginheight="0px" frameborder="no"  scrolling="no" align="center" style="width:100%;"
										 src=""></iframe>
									</div>
								</div>
							</div>	 --%>					
				
<!-- 						<div class="lobi-col lobipanel-parent" data-inner-id="lobipanel-parent4"> -->
<!-- 							<div class="panel" data-inner-id="lobipanel-panel7" id="divMainMyReqList" style="display:none;"> -->
<!-- 								<div class="panel-heading"> -->
<!-- 									<div class="panel-title"> -->
<!-- 										<h4>내 요청 현황</h4> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 								<div class="lobipanel-body type02 boxscroll" id="mainMyReqList"> -->
<!-- 									<div class="portlet-min inboxscroll"> -->
<!-- 										<div class="myReqLstWrap"> -->
<!-- 											<div class="myReqLstWrap-in" id="mainMyReqListDetail"> -->
<!-- 											</div> -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 									<div class="portlet-max hgt-per-100"> -->
<!-- 										<iframe id="mainMyReqListIfrm" name="portletMaxIfrm" marginwidth="0px" marginheight="0px" frameborder="no"  scrolling="no" align="center" style="width:100%;" -->
<!-- 										 src=""></iframe> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 						<div class="panel" data-inner-id="lobipanel-panel4" id="divMainMaterialList" style="display:none;"> -->
<!-- 							<div class="panel-heading"> -->
<!-- 								<div class="panel-title"> -->
<!-- 									<h4>재고부족 자재</h4> -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 							<div class="lobipanel-body boxscroll" id="mainMaterialList"> -->
<!-- 								<div class="portlet-min hgt-per-100"> -->
<!-- 									<div class="tb-wrap main hgt-per-100"> -->
<!-- 										<table class="tb-st"> -->
<%-- 											<colgroup> --%>
<%-- 												<col width="24%" /> --%>
<%-- 												<col width="33%" /> --%>
<%-- 												<col width="15%" /> --%>
<%-- 												<col width="15%" /> --%>
<%-- 												<col width="13%" /> --%>
<%-- 											</colgroup> --%>
<!-- 											<thead> -->
<!-- 												<tr> -->
<!-- 													<th>자재코드</th> -->
<!-- 													<th>자재명</th> -->
<!-- 													<th>안전재고</th> -->
<!-- 													<th>재고</th> -->
<!-- 													<th>경과일</th> -->
<!-- 												</tr> -->
<!-- 											</thead> -->
<!-- 										</table> -->
<!-- 										<div id="tbodyMaterialShortageList" class="boxscroll"> -->
<!-- 											<table class="tb-st inboxscroll"> -->
<%-- 												<colgroup> --%>
<%-- 													<col width="24%" /> --%>
<%-- 													<col width="33%" /> --%>
<%-- 													<col width="15%" /> --%>
<%-- 													<col width="15%" /> --%>
<%-- 													<col width="13%" /> --%>
<%-- 												</colgroup> --%>
<!-- 												<tbody id="materialShortageList"> -->
<!-- 												</tbody> -->
<!-- 											</table> -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 								<div class="portlet-max hgt-per-100"> -->
<!-- 									<iframe id="mainMaterialListIfrm" name="portletMaxIfrm" marginwidth="0px" marginheight="0px" frameborder="no"  scrolling="no" align="center" style="width:100%;" -->
<!-- 									 src=""></iframe> -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 						</div> -->

						
<!-- 						<div class="panel" data-inner-id="lobipanel-panel8" id="divMainMaterialMasterList" style="display:none;"> -->
<!-- 							<div class="panel-heading"> -->
<!-- 								<div class="panel-title"> -->
<!-- 									<h4>자재 재고현황</h4> -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 							<div class="lobipanel-body" id="mainMaterialMasterList"> -->
<!-- 								<div class="portlet-min hgt-per-100"> -->
<!-- 									<div class="tb-wrap main hgt-per-100"> -->
<!-- 										<table class="tb-st"> -->
<%-- 											<colgroup> --%>
<%-- 												<col width="24%" /> --%>
<%-- 												<col width="33%" /> --%>
<%-- 												<col width="15%" /> --%>
<%-- 												<col width="15%" /> --%>
<%-- 												<col width="13%" /> --%>
<%-- 											</colgroup> --%>
<!-- 											<thead> -->
<!-- 												<tr> -->
<!-- 													<th>자재코드</th> -->
<!-- 													<th>자재명</th> -->
<!-- 													<th>안전재고</th> -->
<!-- 													<th>재고</th> -->
<!-- 													<th>경과일</th> -->
<!-- 												</tr> -->
<!-- 											</thead> -->
<!-- 										</table> -->
<!-- 										<div id="tbodyMaterialMasterList" class="boxscroll"> -->
<!-- 											<table class="tb-st inboxscroll"> -->
<%-- 												<colgroup> --%>
<%-- 													<col width="24%" /> --%>
<%-- 													<col width="33%" /> --%>
<%-- 													<col width="15%" /> --%>
<%-- 													<col width="15%" /> --%>
<%-- 													<col width="13%" /> --%>
<%-- 												</colgroup> --%>
<!-- 												<tbody id="materialMasterList"> -->
<!-- 												</tbody> -->
<!-- 											</table> -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 								<div class="portlet-max hgt-per-100"> -->
<!-- 									<iframe id="mainMaterialMasterListIfrm" name="portletMaxIfrm" marginwidth="0px" marginheight="0px" frameborder="no"  scrolling="no" align="center" style="width:100%;" -->
<!-- 									 src=""></iframe> -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 						</div> -->
					
					
			</div>
		</div>
	</div>
	<!-- end : 위젯 -->
	
	<!-- 공지팝업 시작 -->
	<c:choose>
		<c:when test="${empty popNoticeList }">
		</c:when>
		<c:otherwise>
			<div id="noticePop" class="noticePop">
				<div class="noticePopWrap">
					<div class="noti-slider-wrap">
						<div class="noti-view">
							<ul class="noti-slider">
								<c:forEach var="item" items="${popNoticeList}" varStatus="status">
									<li>
										<div class="noticePopTit"><span>${item.TITLE }</span></div>
										<div class="noticePopCon">	
											${fn:replace(fn:replace(fn:replace(fn:replace(fn:replace(fn:replace(fn:replace(item.CONTENT,'&amp;','&'),'<p>',''),'</p>','<br/>'),'&quot;','"'),'&lt;','<'),'&gt;','>'),'&nbsp;',' ')}
										</div>
									</li>
								</c:forEach>
							</ul>
							
							<div class="noti-controls">
								<ul class="noti-cont-list">
									<c:forEach var="item" items="${popNoticeList}" varStatus="status">
										<c:choose>
											<c:when test="${status.index eq 0 }">
												<li class="active"><span></span></li>
											</c:when>
											<c:otherwise>
												<li><span></span></li>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</ul>
							</div>
						</div>														
					</div>
				</div>
				<div class="notiFooter">
					<span class="f-l">
						<input type="checkbox" name="close" id="oneDayNone" onclick="javascript:closeWin('noticePop', 1);"/>
						<label for="oneDayNone">하루동안 이 창을 열지 않음</label>
					</span>
					<span class="f-r">
						<a href="#none" onclick="javascript:closeWin('noticePop', 0);">X&nbsp;&nbsp;닫기</a>
					</span>
				</div>
			</div>
			<!-- 공지팝업 끝 -->
		</c:otherwise>
	</c:choose>

</body>
<script type="text/template" id="workerInfoTemplate">
	<li>
		<div class="userInfo f-l">
			<div class="user_img"><img src="" name="MEMBER_FILEURL" alt="" onerror="this.src='/images/com/web_v2/userIcon.png'"/></div>
			<div class="user_name">
				<span class="name" name="user"></span>
				<span class="dep" name="dep"></span>
			</div>
		</div>
		<div class="workStatus f-r">
			<ul>
				<li>
					<span class="workStatusTit"><spring:message code='epms.work' /></span>
					<span class="workStatusCon type01" title="계획건" name="REPAIR_ONGOING_CNT"></span>
					<span class="workStatusCon type02" title="완료건" name="REPAIR_EXCESS_CNT"></span>
				</li>
			</ul>
		</div>
	</li>
</script>
<script type="text/template" id="workerTabTemplate">
	<div class="workerTabcontent" style="display:none;">
		<ul class="lobipanel-list">
		</ul>
	</div>
</script>
<script type="text/template" id="repairEMTemplate">
	<div class="troubleTabcontent" style="display:none;">
		<div class="hgt-per-100">
			<div class="portletChart" id="canvas">
			
			</div>
		</div>
	</div>
</script>
</html>