<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: popRepairResultRegForm.jsp
	Description : 고장관리 > 정비실적 등록 레이어 팝업
	author		: 김영환
	since		: 2018.04.04
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.04	 김영환		최초 생성

--%>
<style>
	.inp-comm.popRepairResultRegForm_datePic {box-sizing:border-box;height:28px;padding:5px 25px 4px 6px;border:1px solid #cdd2da;
	background:#fff url('/images/com/web/ico_cal.png') no-repeat right 5px center;background-size: 17px auto;cursor:pointer;}
</style>
<%@ include file="/com/codeConstants.jsp"%>
<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">
var submitBool = true;
var maxFileCnt = 30;
var msg = "";

$(document).ready(function(){
	
	<%-- DatePicker 설정 --%>
	$( ".popRepairResultRegForm_datePic" ).datepicker({
	   dateFormat   : "yy-mm-dd",
       prevText     : "click for previous months" ,
       nextText     : "click for next months" ,
       showOtherMonths   : true ,
       selectOtherMonths : false
	}).each(function(){
		if($(this).hasClass('thisDay')){
			$(this).val(stDate);
		}
	});
	
	<%-- 고장시작시간 --%>
	var date = new Date();
	if("${repairResultDtl.TROUBLE_TIME1}" == "" || "${repairResultDtl.TROUBLE_TIME1}" == null){
		$('#popRepairResultRegForm_TROUBLE_TIME1_DATE').val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		$('#popRepairResultRegForm_TROUBLE_TIME1_HOUR').val(getDate(date.getHours()));
		var min = parseInt(date.getMinutes());
		min = min - (min%5);
		var minstr;
		if(min<10){
			minstr = "0" + min.toString();
		}else{
			minstr = min.toString();
		}
		$('#popRepairResultRegForm_TROUBLE_TIME1_MINUTE').val(minstr);
	}
	else{
		var trouble_time1 = "${repairResultDtl.TROUBLE_TIME1}";
		$('#popRepairResultRegForm_TROUBLE_TIME1_DATE').val(trouble_time1.substring(0,4) + "-" + trouble_time1.substring(4,6) + "-" + trouble_time1.substring(6,8));
		$('#popRepairResultRegForm_TROUBLE_TIME1_HOUR').val(trouble_time1.substring(8,10));
		$('#popRepairResultRegForm_TROUBLE_TIME1_MINUTE').val(trouble_time1.substring(10,12));
	}
	<%-- 고장종료시간 --%>
	if("${repairResultDtl.TROUBLE_TIME2}" == "" || "${repairResultDtl.TROUBLE_TIME2}" == null){
		$('#popRepairResultRegForm_TROUBLE_TIME2_DATE').val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		$('#popRepairResultRegForm_TROUBLE_TIME2_HOUR').val(getDate(date.getHours()));
		var min = parseInt(date.getMinutes());
		min = min - (min%5);
		var minstr;
		if(min<10){
			minstr = "0" + min.toString();
		}else{
			minstr = min.toString();
		}
		$('#popRepairResultRegForm_TROUBLE_TIME2_MINUTE').val(minstr);
	}
	else{
		var trouble_time2 = "${repairResultDtl.TROUBLE_TIME2}";
		$('#popRepairResultRegForm_TROUBLE_TIME2_DATE').val(trouble_time2.substring(0,4) + "-" + trouble_time2.substring(4,6) + "-" + trouble_time2.substring(6,8));
		$('#popRepairResultRegForm_TROUBLE_TIME2_HOUR').val(trouble_time2.substring(8,10));
		$('#popRepairResultRegForm_TROUBLE_TIME2_MINUTE').val(trouble_time2.substring(10,12));
	}
	
	<%-- '타파트요청'버튼 클릭 --%>
	$('#popRepairResultRegForm_requestBtn').on('click', function(){
		$('#flag').val("request");
		fnSave("request");
	});
	
	<%-- '임시저장'버튼 클릭 --%>
	$('#popRepairResultRegForm_saveBtn').on('click', function(){
		$('#flag').val("SAVE");
		fnSave("SAVE");
	});
	
	<%-- '정비완료'버튼 클릭 --%>
	$('#popRepairResultRegForm_regBtn').on('click', function(){
		$('#flag').val("COMPLETE");
		fnSave("COMPLETE");
	});
	
	<%-- 첨부파일 수정버튼 (등록폼 변환) --%>
	$('#modifyBtn').on('click', function(){
		
		$('#viewFrame').hide();
		$('#regFrame').show();
	});

	<%-- 첨부파일 관련 --%>
	for(var i=0; i<1; i++){				<%-- 파일 영역 추가를 고려하여 개발 --%>
		$('#attachFile').append('<input type="file" id="fileList" class="ksUpload" name="fileList">');	
		
		//var fileType = 'jpeg |gif |jpg |png |bmp |ppt |pptx |doc |hwp |pdf |xlsx |xls';
		var fileType = 'jpeg |gif |jpg |png |bmp';
	
		$('#fileList').MultiFile({
			   accept :  fileType
			,	list   : '#detailFileRegList'+i
			,	max    : 5
			,	STRING : {
					remove: '<img src="/images/com/web/btn_remove.png"/>'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
					text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" />',
					idx	  : i,
				},
		});
		
		<%-- 첨부한 파일의 출력 순서를 자동으로 샛팅 하기 위한 스크립트 - 공통 스크립트로 빠지면 안됨 .개별 항목으로 처리 필수 --%> 
		$('.MultiFile-wrap').change(function(){
			var fileExpIndex = $(this).closest('.fileWrap').find('.fileList > div').children('div:last-child');
			var	fileExp = fileExpIndex.find('.fileExp');
			fileExp.val(fileExpIndex.index() + 1);
		});
	}
	
	<%-- 파일 다운로드 --%>
	$('.btnFileDwn').on('click', function(){
		var ATTACH_GRP_NO = $(this).data("attachgrpno");
		var ATTACH = $(this).data("attach");
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO='+ATTACH_GRP_NO+'&ATTACH='+ATTACH;
		$("#fileDownFrame").attr("src",src);
	});
	
	<%-- 첨부파일 삭제버튼 --%>
	$('.icn_fileDel').on('click', function(){
		delModiFile($(this));
	});
	
	<%-- 고장 첨부파일 중 사진이 아닐경우 미리보기 숨김 --%>
	var fileList1 = new Array();
	<c:forEach items="${attachList1}" var="item" >
		var obj = new Object();
		
		obj.ATTACH_GRP_NO = "${item.ATTACH_GRP_NO}";
		obj.ATTACH = "${item.ATTACH}";
		obj.FORMAT = "${item.FORMAT}";
		
		fileList1.push(obj);
	</c:forEach>
	var attachImgCnt1 = 0;
	for(var i=0; i<fileList1.length; i++){
		if(fileList1[i].FORMAT == "jpg" || fileList1[i].FORMAT == "png" || fileList1[i].FORMAT == "jpeg" || fileList1[i].FORMAT == "JPG" || fileList1[i].FORMAT == "PNG"){
			attachImgCnt1 ++;
		}
	}	
	
	if(fileList1.length > 0){
		if(attachImgCnt1 == 0){
			$('#break_previewImg').hide();
		}
	}
	
	<%-- 정비 첨부파일 중 사진이 아닐경우 미리보기 숨김 --%>
	var fileList2 = new Array();
	<c:forEach items="${attachList2}" var="item" >
		var obj = new Object();
		
		obj.ATTACH_GRP_NO = "${item.ATTACH_GRP_NO}";
		obj.ATTACH = "${item.ATTACH}";
		obj.FORMAT = "${item.FORMAT}";
		
		fileList2.push(obj);
	</c:forEach>
	var attachImgCnt2 = 0;
	for(var i=0; i<fileList2.length; i++){
		if(fileList2[i].FORMAT == "jpg" || fileList2[i].FORMAT == "png" || fileList2[i].FORMAT == "jpeg" || fileList2[i].FORMAT == "JPG" || fileList2[i].FORMAT == "PNG"){
			attachImgCnt2 ++;
		}
	}	
	
	if(fileList2.length > 0){
		if(attachImgCnt2 == 0){
			$('#repair_previewImg').hide();
		}
	}
	
	<%-- select box 초기값 세팅 --%>
	if("${repairResultDtl.REPAIR_TYPE}" != '') $("select[name=REPAIR_TYPE]").val("${repairResultDtl.REPAIR_TYPE}");
	if("${repairResultDtl.TROUBLE_TYPE1}" != '') $("select[name=TROUBLE_TYPE1]").val("${repairResultDtl.TROUBLE_TYPE1}");
	if("${repairResultDtl.TROUBLE_TYPE2}" != '') $("select[name=TROUBLE_TYPE2]").val("${repairResultDtl.TROUBLE_TYPE2}");
});

<%-- 정비원추가 버튼 --%>
function fnAddMember(gridNm){
	
	var division = new Array();
	var division_name = new Array();
	var part = new Array();
	var part_name = new Array();
	var member = new Array();
	var member_name = new Array();
	
	var selRows = Grids[gridNm].GetSelRows();
	
	if(selRows == null || selRows == ""){
		return;
	}
	
	for(var i=0; i<selRows.length; i++){
		
		if(!(selRows[i].USER_ID=="" || selRows[i].USER_ID==null)){

			division[i] = selRows[i].DIVISION;
			division_name[i] = selRows[i].DIVISION_NAME;
			part[i] = selRows[i].PART;
			part_name[i] = selRows[i].PART_NAME;
			member[i] = selRows[i].USER_ID;
			member_name[i] = selRows[i].NAME;
			
		}
	}
	
	addMember(division, division_name, part, part_name, member, member_name, i);
	fnModalToggle('popWorkerList');
}

<%-- 자재추가 버튼 --%>
function fnAddMaterial(gridNm){
	
	var equipment_bom = new Array();
	var material_grp = new Array();
	var material_grp_uid = new Array();
	var material_grp_name = new Array();
	var material = new Array();
	var material_code = new Array();
	var material_name = new Array();
	var unit = new Array();
	var unit_name = new Array();
	
	var selRows = Grids[gridNm].GetSelRows();
	
	if(selRows == null || selRows == ""){
		return;
	}
	
	for(var i=0; i<selRows.length; i++){
		
		if(!(selRows[i].MATERIAL=="" || selRows[i].MATERIAL==null)){
			equipment_bom[i] = selRows[i].EQUIPMENT_BOM;
			material_grp[i] = selRows[i].MATERIAL_GRP;
			material_grp_uid[i] = selRows[i].MATERIAL_GRP_UID;
			material_grp_name[i] = selRows[i].MATERIAL_GRP_NAME;
			material[i] = selRows[i].MATERIAL;
			material_code[i] = selRows[i].MATERIAL_UID;
			material_name[i] = selRows[i].MATERIAL_NAME;
			unit[i] = selRows[i].UNIT;
			unit_name[i] = selRows[i].UNIT_NAME;
		}
	}
	
	addMaterial(equipment_bom, material_grp, material_grp_uid, material_grp_name, material, material_code, material_name, unit, unit_name, i);
	fnModalToggle('popRepairMaterialList');
}

<%-- 정비원추가 선택 후 html 작성 --%>
function addMember(division, division_name, part, part_name, member, member_name, cnt){
	
	var html = "";

	for(var i=0; i<cnt; i++){
		if($('#repairMember').html().indexOf('name="memberArr" value="' + member[i] + '"') < 0){
			html +=
				'<li style="overflow:hidden; margin:5px 0;">'
			+	'	<input type="hidden" class="inp-comm f-l" title="부서ID" name="" value="' + division[i] + '"	readonly="readonly">'		
			+	'	<div class="box-col wd-per-30 f-l">'
			+	'		<div class="inp-wrap readonly">'
			+	'			<input type="text" class="inp-comm" title="부서명" name="" value="' + division_name[i] + '"	readonly="readonly">'
			+	'		</div>'
			+	'	</div>'
			+	'	<input type="hidden" class="inp-comm f-l" title="파트" name="" value="' + part[i] + '"	readonly="readonly">'
			+	'	<div class="box-col wd-per-15 f-l">'
			+	'		<div class="inp-wrap readonly mgn-l-5">'
			+	'			<input type="text" class="inp-comm" title="파트명" name="" value="' + part_name[i] + '"	readonly="readonly">'
			+	'		</div>'
			+	'	</div>'
			+	'	<input type="hidden" class="inp-comm f-l" title="정비원ID" name="memberArr" value="' + member[i] + '"	readonly="readonly">'
			+	'	<div class="box-col wd-per-15 f-l">'
			+	'		<div class="inp-wrap readonly mgn-l-5">'
			+	'			<input type="text" class="inp-comm" title="정비원" name="" value="' + member_name[i] + '"	readonly="readonly">'
			+	'		</div>'
			+	'	</div>'
			+	'	<a href="#none" class="btnRemove mgn-l-5" onclick="delList1($(this))">삭제</a>'
			+	'</li>';
		}
	};
	
	$('#repairMember').append(html);
	
}

<%-- 사용자재 '검색'버튼 --%>
function fnSearchMaterial(){
	
	if($('#REPAIR_RESULT').val() == "" || $('#REPAIR_RESULT').val() == null){
		return;
	}
	
	var url = '/epms/equipment/bom/equipmentBomListData.do'
			+ '?EQUIPMENT=' + $('#popRepairResultRegForm_EQUIPMENT').val(); 
	
	Grids.popRepairMaterialList.Source.Data.Url = url;
	Grids.popRepairMaterialList.ReloadBody();
	fnModalToggle('popRepairMaterialList');
}

<%-- 추가자재 선택 후 html 작성 --%>
function addMaterial(equipment_bom, material_grp, material_grp_uid, material_grp_name, material, material_uid, material_name, unit, unit_name, cnt){
	var html = "";
	
	for(var i=0; i<cnt; i++){
		
		if($('#usedMaterial').html().indexOf('name="equipmentBomArr" value="' + equipment_bom[i] + '"') < 0 && $('#usedMaterial').html().indexOf('name="equipmentBomUsedArr" value="' + equipment_bom[i] + '"') < 0){	
			html +=
				'<li style="overflow:hidden; margin:5px 0;">'
			+	'	<input type="hidden" class="inp-comm" title="BOMID" name="equipmentBomArr" value="' + equipment_bom[i] + '"	readonly="readonly">'
			+	'	<input type="hidden" class="inp-comm" title="자재ID" name="materialArr" value="' + material[i] + '"	readonly="readonly">'
			+	'	<div class="box-col wd-per-20 f-l">'
			+	'		<div class="inp-wrap readonly">'
			+	'			<input type="text" class="inp-comm" title="장치명" name="" value="' + material_uid[i] + '"	readonly="readonly">'
			+	'		</div>'
			+	'	</div>'
			+	'	<div class="box-col wd-per-20 f-l">'
			+	'		<div class="inp-wrap readonly mgn-l-5">'
			+	'			<input type="text" class="inp-comm" title="자재명" name="" value="' + material_name[i] + '"	readonly="readonly">'
			+	'		</div>'
			+	'	</div>'
			+	'	<input type="hidden" class="inp-comm" title="단위" name="" value="' + unit[i] + '"	readonly="readonly">'
			+	'	<div class="box-col wd-per-20 f-l">'
			+	'		<div class="inp-wrap readonly mgn-l-5">'
			+	'			<input type="text" class="inp-comm" title="단위명" name="" value="' + unit_name[i] + '"	readonly="readonly">'
			+	'		</div>'
			+	'	</div>'
			+	'	<div class="box-col wd-per-20 f-l">'
			+	'		<div class="inp-wrap mgn-l-5">'
			+	'			<input type="text" class="inp-comm" title="수량" name="qntyArr"	value="0"	onchange="check_Null(this);"	onkeydown="check_isNum_onkeydown();" autocomplete="off">'
			+	'		</div>'
			+	'	</div>'
			+	'	<a href="#none" class="btnRemove mgn-l-5" onclick="delList1($(this))">삭제</a>'
			+	'</li>';
		}
	};
	
	$('#usedMaterial').append(html);
}

<%-- 정비원,신규자재 삭제버튼 --%>
function delList1(delLst){
	if(delLst.siblings("input[name=memberArr]").val() == $('#MANAGER').val()){
		alert("담당자는 삭제할 수 없습니다.");
		return;
	}else{
		delLst.closest('li').remove();
	}
}

<%-- 사용자재 삭제버튼 --%>
function delList2(delLst){
	delLst.siblings("input[name=delYnArr]").val('Y');
	delLst.closest('li').hide();
}

<%-- 타파트요청,임시저장,정비완료 --%>
function fnSave(type){
	
	var strComplete = "<spring:message code='epms.repair.complete' />" ;  // 정비완료
	
	if(type == "request"){
		
		msg = "정상적으로 '타파트요청' 처리되었습니다.";
		
		var url = "/epms/repair/result/popRequestPartForm.do"
				  + "?LOCATION=" + $('#popRepairResultRegForm_LOCATION').val()
				  + "&REMARKS=" + $('#popRepairResultRegForm_REMARKS').val()
				  + "&PART=" + $('#popRepairResultRegForm_PART').val()
				  + "&EQUIPMENT=" + $('#popRepairResultRegForm_EQUIPMENT').val()
				  + "&REPAIR_REQUEST_ORG=" + $('#popRepairResultRegForm_REPAIR_REQUEST_ORG').val();

		$('#popRequestPartForm').load(url, function(responseTxt, statusTxt, xhr){
			$(this).modal();
		});
	}
	else if (type == "SAVE"){
		if(confirm("임시저장 하시겠습니까?")){
			msg = "정상적으로 '임시저장' 처리되었습니다.";
			fnRegAjax(type);
		}
	}
	else if (type == "COMPLETE"){
		if(confirm( strComplete + " 등록하시겠습니까? \n* " + strComplete + "하시면 더 이상 수정이 불가능합니다.")){
			msg = "정상적으로 '" + strComplete + "' 처리되었습니다.";
			fnRegAjax(type);
		}
	}
}

<%-- 정비원 '검색'버튼 --%>
function fnSearchMember(){
	if($('#REPAIR_RESULT').val() == "" || $('#REPAIR_RESULT').val() == null){
		return;
	}
	
	var url = '/epms/repair/result/popWorkerListData.do'
			  + '?LOCATION=' + $('#popRepairResultRegForm_LOCATION').val()
			  +	'&PART=' + $('#popRepairResultRegForm_PART').val()
			  + '&REMARKS=' + $('#popRepairResultRegForm_REMARKS').val();
	
	Grids.WorkerStatList.Source.Data.Url = url;
	Grids.WorkerStatList.ReloadBody();
	fnModalToggle('popWorkerList');
}

<%-- 임시저장,정비완료 ajax --%>
function fnRegAjax(type){
	
	submitBool = false;
	
	$('#popRepairResultRegForm_TROUBLE_TIME1').val($('#popRepairResultRegForm_TROUBLE_TIME1_DATE').val() + " " + $('#popRepairResultRegForm_TROUBLE_TIME1_HOUR').val() + ":" + $('#popRepairResultRegForm_TROUBLE_TIME1_MINUTE').val() + ":00");
	$('#popRepairResultRegForm_TROUBLE_TIME2').val($('#popRepairResultRegForm_TROUBLE_TIME2_DATE').val() + " " + $('#popRepairResultRegForm_TROUBLE_TIME2_HOUR').val() + ":" + $('#popRepairResultRegForm_TROUBLE_TIME2_MINUTE').val() + ":00");
	var trouble_time = $('#popRepairResultRegForm_TROUBLE_TIME2_DATE').val().substring(0,4) + "-" + $('#popRepairResultRegForm_TROUBLE_TIME2_DATE').val().substring(4,6) + "-" + $('#popRepairResultRegForm_TROUBLE_TIME2_DATE').val().substring(6,8);
	$('#DATE_REPAIR').val(trouble_time + " " + $('#popRepairResultRegForm_TROUBLE_TIME2_HOUR').val() + ":" + $('#popRepairResultRegForm_TROUBLE_TIME2_MINUTE').val() + ":00");
	
	if(type == "${FLAG_COMPLETE}"){
		
		if (!isNull_J($('#REPAIR_TYPE'), '정비유형을 선택해주세요.')) {
			submitBool = true;
			return false;
		}
		
		if (!isNull_J($('#REPAIR_DESCRIPTION'), '작업내용을 입력해주세요.')) {
			submitBool = true;
			return false;
		}
		
		if($('#popRepairResultRegForm_TROUBLE_TIME1').val() > $('#popRepairResultRegForm_TROUBLE_TIME2').val()){
			submitBool = true;
			alert("고장종료시간이 고장시작시간보다 이전이 될 수 없습니다.");
			return false;
		}
		
		var date = new Date();
		var sysdate = date.getFullYear() + '' + getDate(parseInt(date.getMonth())+1) + '' + getDate(date.getDate()) + ' ' + getDate(date.getHours()) + ":" + getDate(date.getMinutes()) + ":" + getDate(date.getSeconds());
		if($('#popRepairResultRegForm_TROUBLE_TIME2').val() > sysdate){
			submitBool = true;
			alert("고장종료시간이 현재보다 미래일 수 없습니다.");
			return false;
		}
		
		if (!isNull_J($('#TROUBLE_TYPE1'), '고장유형을 선택해주세요.')) {
			submitBool = true;
			return false;
		}
		if (!isNull_J($('#TROUBLE_TYPE2'), '조치유형을 선택해주세요.')) {
			submitBool = true;
			return false;
		}	
			
	}
	
	var fileCnt = $("input[name=fileGb]").length;
	<%-- 정비사진 유무 세팅 --%>
	if (fileCnt > 0) {
		$('#attachExistYn').val("Y");
	} else {
		$('#attachExistYn').val("N");
	}
	
	$('input[name=materialUsedArr]').attr('value',convertMaterialArrToJSON('materialUsedArr', 'qntyUsedArr', 'equipmentBomUsedArr', 'delYnArr'));
	$('input[name=materialArr]').attr('value',convertMaterialArrToJSON('materialArr', 'qntyArr', 'equipmentBomArr', ''));
	
	var form = new FormData(document.getElementById('regForm'));
	
	$.ajax({
		type : 'POST',
		url : '/epms/repair/result/repairResultRegAjax.do',
		dataType : 'json',
		data : form,
		processData : false,
		contentType : false,
		success : function(json) {	
			
			<%-- 정상적으로 정비실적 등록시 --%>
			if(json != '' && json != null){
				fnModalToggle('popRepairResultRegForm');
				fnReload(msg);
				submitBool = true;
			<%-- 이미 등록된 정비실적일 경우 --%>
			}else{
				alert("처리도중 에러가 발생하였습니다 . 시스템 관리자에게 문의 하시기 바랍니다");
				submitBool = true;
				return;
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
			submitBool = true;
		}
	});
}

<%-- 사용자재 input 값들을 JSON화 --%>
function convertMaterialArrToJSON(material, qnty, equipment, delYn){
	
	var tempArrSize = $("input[name='"+material+"']").length;
	var tempArr = new Array();
	
	for(var i = 0; i < tempArrSize; i++){
		var tempObj = new Object();
		tempObj.MATERIAL = $("input[name='"+material+"']")[i].value;
		tempObj.QNTY = $("input[name='"+qnty+"']")[i].value;
		tempObj.EQUIPMENT_BOM = $("input[name='"+equipment+"']")[i].value;
		if(delYn != ''){
			tempObj.DEL_YN = $("input[name='"+delYn+"']")[i].value;
		}
		tempArr.push(tempObj);
	}
	
	return JSON.stringify(tempArr);
}

<%-- 수량 null 체크 --%>
function check_Null(obj){
	if(obj.value == ""){
		obj.value = "0";
	}
}

<%-- 총중량,취득가액 콤마처리 --%>
var rgx1 = /\D/g;  <%-- /[^0-9]/g 와 같은 표현 --%>
var rgx2 = /(\d+)(\d{3})/; 

function getNumber(obj) {
	if(obj.value.charAt(obj.value.length-1) == "."){
		var parts=obj.value.toString().split(".");
		parts[0] = parts[0].replace(rgx1,"");
		obj.value = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ".";
	}
	else{
		var parts=obj.value.toString().split(".");
		parts[0] = parts[0].replace(rgx1,"");
		obj.value = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
	}
}
 
<%-- 총중량,취득가액 숫자 유효성 검사 --%> 
function check_isNum_onkeydown(){

	$(this).val( $(this).val().replace(/[^-0-9.,]/gi,"") );
	$(this).val(number_format($(this).val()));
}

</script>
<div class="modal-dialog root wd-per-60">
	<div class="modal-content">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title">정비실적 등록</h4>
		</div>
	 	<div class="modal-body overflow">
           	<div class="modal-bodyIn hgt-per-100" style="padding:0;">
				<input type="hidden" id="userId" name="userId" value="${userId}"/>
				
					<div class="popTabSet">
						<ul class="tabs">
							<li><a href="#panel4-1" class="on">요청내용</a></li>
							<%-- 대시보드에선 수정하지 않으므로 임시 제거 --%>
							<%--<li><a href="#panel4-2">실적등록</a></li> --%>	
						</ul>
						<div class="panels">
							<div class="panel type01" id="panel4-1">
								<div class="panel-in rel">
									<div class="tb-wrap">
										<table class="tb-st">
											<caption class="screen-out">정비실적 상세</caption>
											<colgroup>
												<col width="15%" />
												<col width="35%" />
												<col width="15%" />
												<col width="*" />
											</colgroup>
											<tbody>
												<tr>
													<th scope="row">정비상태</th>
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="REPAIR_STATUS" name="REPAIR_STATUS" value="${repairResultDtl.REPAIR_STATUS}"/>
															<input type="text" class="inp-comm" id="repair_status_name" name="repair_status_name" readonly="readonly" value="${repairResultDtl.REPAIR_STATUS_NAME}"/>
														</div>
													</td>
													<th scope="row">처리파트</th>
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="popRepairResultRegForm_PART" name="PART" value="${repairResultDtl.PART}"/>
															<input type="text" class="inp-comm" id="popRepairResultRegForm_part_name" name="part_name" readonly="readonly" value="${repairResultDtl.PART_NAME}"/>
														</div>
													</td>
													
												</tr>
												<tr>
													<th scope="row">공장</th>
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="popRepairResultRegForm_LOCATION" name="LOCATION" value="${repairResultDtl.LOCATION}"/>
															<input type="text" class="inp-comm" id="popRepairResultRegForm_location_name" name="location_name" readonly="readonly" value="${repairResultDtl.LOCATION_NAME}"/>
														</div>
													</td>
													<th scope="row">요청일시</th>
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" id="DATE_REQUEST" name="DATE_REQUEST" readonly="readonly" value="${repairResultDtl.DATE_REQUEST}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">라인</th>
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="CATEGORY" name="CATEGORY" value="${repairResultDtl.CATEGORY}"/>
															<input type="text" class="inp-comm" id="category_name" name="category_name" readonly="readonly" value="${repairResultDtl.CATEGORY_NAME}"/>
														</div>
													</td>
													<th scope="row">요청부서</th>
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="REQUEST_DIV" name="REQUEST_DIV" value="${repairResultDtl.REQUEST_DIV}"/>
															<input type="text" class="inp-comm" id="request_div_name" name="request_div_name" readonly="readonly" value="${repairResultDtl.REQUEST_DIV_NAME}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">설비코드</th>
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="popRepairResultRegForm_EQUIPMENT" name="EQUIPMENT" value="${repairResultDtl.EQUIPMENT}"/>
															<input type="text" class="inp-comm" id="popRepairResultRegForm_EQUIPMENT_UID" name="EQUIPMENT_UID" readonly="readonly" value="${repairResultDtl.EQUIPMENT_UID}"/>
														</div>
													</td>
													<th scope="row">요청자</th>
													<td>
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="REQUEST_MEMBER" name="REQUEST_MEMBER" value="${repairResultDtl.REQUEST_MEMBER}"/>
															<input type="text" class="inp-comm" id="request_member_name" name="request_member_name" readonly="readonly" value="${repairResultDtl.REQUEST_MEMBER_NAME}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">설비명</th>
													<td colspan="3">
														<div class="inp-wrap readonly wd-per-100">
															<input type="text" class="inp-comm" id="EQUIPMENT_NAME" name="EQUIPMENT_NAME" readonly="readonly" value="${repairResultDtl.EQUIPMENT_NAME}"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">고장내용</th>
													<td colspan="3">
														<div class="txt-wrap readonly">
															<div class="txtArea">
																<textarea cols="" rows="3" title="고장내용" id="REQUEST_DESCRIPTION" name="REQUEST_DESCRIPTION" readonly="readonly">${repairResultDtl.REQUEST_DESCRIPTION}</textarea>
															</div>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">담당자</th>
													<td colspan="3">
														<div class="inp-wrap readonly wd-per-100">
															<input type="hidden" class="inp-comm" id="MANAGER" name="MANAGER" value="${repairResultDtl.MANAGER}"/>
															<input type="text" class="inp-comm" id="manager_name" name="manager_name" readonly="readonly" value="${repairResultDtl.MANAGER_NAME}"/>
														</div>
													</td>
												</tr>
												
												<tr>
													<th scope="row">고장 첨부파일</th>
													<td colspan="3">
														<c:choose>
															<c:when test="${fn:length(attachList1)>0}">
															<div id="previewList" class="wd-per-100">
																<div class="bxType01 fileWrap previewFileLst">
																	<div>
																		<div class="fileList type02">
																			<div id="detailFileList0" class="fileDownLst">
																				<c:forEach var="item" items="${attachList1}" varStatus="idx">
																					<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																						<span class="MultiFile-export">${item.SEQ_DSP}</span> 
																						<span class="MultiFile-title" title="File selected: ${item.NAME}">
																						${item.FILE_NAME} (${item.FILE_SIZE_TXT})
																						</span>
																						<a href="#none" class="btnFileDwn icon download" data-attach="${item.ATTACH}">다운로드</a>
																					</div>		
																				</c:forEach>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
					
															<div id="break_previewImg" class="wd-per-100">
																<div class="previewFile mgn-t-15">
																	<h3 class="tit-cont preview" id="previewTitle">이미지 미리보기</h3>
																	<ul style="margin-top:5px;">
																		<c:forEach var="item" items="${attachList1}" varStatus="idx">
																			<c:if test = "${item.FORMAT eq 'jpg' || item.FORMAT eq 'png' || item.FORMAT eq 'jpeg' || item.FORMAT eq 'JPG' || item.FORMAT eq 'PNG'}">
																				<li>
																					<img src="/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}" title="클릭하시면 원본크기로 보실 수 있습니다." onclick="originFileView('/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}')" />
																					<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																						<span class="MultiFile-title" title="File selected: ${item.NAME}">
																						<p>${item.FILE_NAME} (${item.FILE_SIZE_TXT})</p>
																						</span>
																						<a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a>
																					</div>
																				</li>
																			</c:if>
																		</c:forEach>
																	</ul>
																</div>
															</div>
															</c:when>
															<c:otherwise>
																<div class="f-l wd-per-100 mgn-t-5">
																※ 등록된 첨부파일이 없습니다.
																</div>
															</c:otherwise>
														</c:choose>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<%-- 실적등록 판넬 : s --%>
							<div class="panel type01" id="panel4-2">
								<div class="panel-in rel">
									<div class="ab overflow" style="top:-45px; right:0;">
										<c:choose>
											<c:when test="${repairResultDtl.REPAIR_STATUS ne EPMS_REPAIR_STATUS_COMPLETE}">
												<c:if test="${repairResultDtl.MANAGER eq userId}">
													<div class="f-r" id="btns">
														<c:if test="${repairResultDtl.REMARKS eq REMARKS_SEPARATE}">
															<a href="#none" id="popRepairResultRegForm_requestBtn" class="btn comm st01">타파트요청</a>
														</c:if>
														<a href="#none" id="popRepairResultRegForm_saveBtn" class="btn comm st02">임시저장</a>	
														<a href="#none" id="popRepairResultRegForm_regBtn" class="btn comm st03"><spring:message code='epms.repair.complete' /></a><!-- 정비완료 -->
													</div>
												</c:if>
											</c:when>
											<c:otherwise>
												<c:if test="${repairResultDtl.REPAIR_TYPE eq EPMS_REPAIR_TYPE_EM && param.flag ne 'repairHistory'}">
													<div class="f-r" id="btns">
														<a href="#none" id="analysisBtn" class="btn comm st02">원인분석</a>
													</div>
												</c:if>
											</c:otherwise>
										</c:choose>
									</div>
									
									<div class="tb-wrap">
										<form id="regForm" name="regForm"  method="post" enctype="multipart/form-data">	
											<input type="hidden" id="attachExistYn" name="attachExistYn" /> 
											<input type="hidden" id="ATTACH_GRP_NO" name="ATTACH_GRP_NO" value="${repairResultDtl.ATTACH_GRP_NO2}" /> 
											<input type="hidden" id="MODULE" name="MODULE" value="13"/>
											<input type="hidden" class="inp-comm" id="flag" name="flag"/>
											<input type="hidden" class="inp-comm" id="REPAIR_RESULT" name="REPAIR_RESULT" value="${repairResultDtl.REPAIR_RESULT}"/>
											<input type="hidden" class="inp-comm" id="popRepairResultRegForm_REMARKS" name="REMARKS" value="${repairResultDtl.REMARKS}"/>
											<input type="hidden" class="inp-comm" id="REPAIR_REQUEST" name="REPAIR_REQUEST" value="${repairResultDtl.REPAIR_REQUEST}"/>
											<input type="hidden" class="inp-comm" id="popRepairResultRegForm_REPAIR_REQUEST_ORG" name="REPAIR_REQUEST_ORG" value="${repairResultDtl.REPAIR_REQUEST_ORG}"/>
											<input type="hidden" class="inp-comm" id="REQUEST_TYPE" name="REQUEST_TYPE" value="${repairResultDtl.REQUEST_TYPE}"/>
											<input type="hidden" class="inp-comm" id="DATE_REPAIR" name="DATE_REPAIR"/>
											<input type="hidden" class="inp-comm" id="REPAIR_ANALYSIS" name="REPAIR_ANALYSIS" value="${repairResultDtl.REPAIR_ANALYSIS}"/>
											<input type="hidden" class="inp-comm" id="PREVENTIVE_RESULT" name="PREVENTIVE_RESULT" value="${repairResultDtl.PREVENTIVE_RESULT}"/>
											<input type="hidden" id="DEL_SEQ" name="DEL_SEQ" value=""/>
											<table class="tb-st">
												<caption class="screen-out">실적 등록</caption>
												<colgroup>
													<col width="15%" />
													<col width="35%" />
													<col width="15%" />
													<col width="*" />
												</colgroup>
												<tbody>
												<tr>
													<th scope="row">정비유형</th>
													<td>
														<div class="sel-wrap repair_type_css">
															<tag:combo codeGrp="REPAIR_TYPE" companyId="${ssCompanyId}" choose="Y"/>
														</div>
													</td>
													<th scope="row">외주</th>
													<td>
														<div class="inp-wrap wd-per-100">
															<input type="text" class="inp-comm" id="OUTSOURCING" name="OUTSOURCING" value="<c:out value="${repairResultDtl.OUTSOURCING}" escapeXml="true" />"/>
														</div>
													</td>
												</tr>
							 					<tr>
													<th scope="row">정비원</th>
													<td colspan="3">
														<a href="#none" class="btn evn-st01 mgn-t-5" onclick="fnSearchMember()">검색</a>
														<ul class="list-st01 repairMember" id="repairMember">
															<c:forEach var="item" items="${repairRegMember}">
																<li style="overflow:hidden; margin:5px 0;">
																	<input type="hidden" class="inp-comm f-l" title="부서ID" name="" value="${item.DIVISION}"	readonly="readonly">	
																	<div class="box-col wd-per-30 f-l">
																		<div class="inp-wrap readonly wd-per-100">
																			<input type="text" class="inp-comm" title="부서명" name="" value="${item.DIVISION_NAME}"	readonly="readonly">
																		</div>
																	</div>
																	<input type="hidden" class="inp-comm f-l" title="파트" name="" value="${item.PART}"	readonly="readonly">	
																	<div class="box-col wd-per-15 f-l">
																		<div class="inp-wrap readonly mgn-l-5">
																			<input type="text" class="inp-comm" title="파트명" name="" value="${item.PART_NAME}"	readonly="readonly">
																		</div>
																	</div>
																	<input type="hidden" class="inp-comm f-l" title="정비원ID" name="memberArr" value="${item.USER_ID}"	readonly="readonly">
																	<div class="box-col wd-per-15 f-l">
																		<div class="inp-wrap readonly mgn-l-5">
																			<input type="text" class="inp-comm" title="정비원" name="" value="${item.MEMBER_NAME}"	readonly="readonly">
																		</div>
																	</div>
																	<c:if test="${item.USER_ID ne repairResultDtl.MANAGER}">
																		<a href="#none" class="btnRemove mgn-l-5" onclick="delList1($(this))">삭제</a>
																	</c:if>
																</li>
															</c:forEach>
														</ul>
													</td>
												</tr>						
												<tr>
													<th scope="row">사용자재</th>
													<td colspan="3">
														<input type="hidden" class="inp-comm" id="MATERIAL_INOUT" name="MATERIAL_INOUT" value="${repairResultDtl.MATERIAL_INOUT}"/>
														<a href="#none" class="btn evn-st01 mgn-t-5" onclick="fnSearchMaterial()">검색</a>
														<ul class="list-st01 usedMaterial" id="usedMaterial">
															<c:forEach var="item" items="${usedMaterial}">
																<li style="overflow:hidden; margin:5px 0;">
																	<input type="hidden" class="inp-comm" title="BOMID" name="equipmentBomUsedArr" value="${item.EQUIPMENT_BOM}"	readonly="readonly">
																	<input type="hidden" class="inp-comm" title="자재ID" name="materialUsedArr" value="${item.MATERIAL}"	readonly="readonly">
																	<div class="box-col wd-per-20 f-l">
																		<div class="inp-wrap readonly">
																			<input type="text" class="inp-comm" title="자재코드" name="" value="${item.MATERIAL_UID}"	readonly="readonly">
																		</div>
																	</div>
																	<div class="box-col wd-per-20 f-l">
																		<div class="inp-wrap readonly mgn-l-5">
																			<input type="text" class="inp-comm" title="자재명" name="" value="${item.MATERIAL_NAME}"	readonly="readonly">
																		</div>
																	</div>
																	<input type="hidden" class="inp-comm" title="단위" name="" value="${item.UNIT}"	readonly="readonly">
																	<div class="box-col wd-per-20 f-l">
																		<div class="inp-wrap readonly mgn-l-5">
																			<input type="text" class="inp-comm" title="단위명" name="" value="${item.UNIT_NAME}"	readonly="readonly">
																		</div>
																	</div>
																	<div class="box-col wd-per-20 f-l">
																		<div class="inp-wrap mgn-l-5">
																			<input type="text" class="inp-comm" title="수량" name="qntyUsedArr"	value="${item.QNTY}"	onchange="check_Null(this);"	onkeydown="check_isNum_onkeydown();" autocomplete="off">
																		</div>
																	</div>
																	<input type="hidden" class="inp-comm" title="삭제여부" name="delYnArr" value="N"	readonly="readonly">
																	<a href="#none" class="btnRemove mgn-l-5" onclick="delList2($(this))">삭제</a>
																</li>
															</c:forEach>
														</ul>
													</td>
												</tr>
												<tr>
													<th scope="row">사용자재(미등록)</th>
													<td colspan="3">
														<div class="inp-wrap wd-per-100">
															<input type="text" class="inp-comm" id="MATERIAL_UNREG" name="MATERIAL_UNREG" value="<c:out value="${repairResultDtl.MATERIAL_UNREG}" escapeXml="true" />"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">고장유형</th>
													<td>
														<div class="sel-wrap">
															<tag:combo codeGrp="TROUBLE1" name="TROUBLE_TYPE1" choose="Y"/>
														</div>
													</td>
													<th scope="row">조치</th>
													<td>
														<div class="sel-wrap">
															<tag:combo codeGrp="TROUBLE2" name="TROUBLE_TYPE2" choose="Y"/>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">작업내용</th>
													<td colspan="3">
														<div class="txt-wrap">
															<div class="txtArea">
																<textarea cols="" rows="3" title="정비내용" id="REPAIR_DESCRIPTION" name="REPAIR_DESCRIPTION"><c:out value="${repairResultDtl.REPAIR_DESCRIPTION}" escapeXml="true" /></textarea>
															</div>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">고장시작시간</th>
													<td>
														<input type="hidden" class="inp-comm popRepairResultRegForm_datePic inpCal" readonly="readonly" id="popRepairResultRegForm_TROUBLE_TIME1" name="TROUBLE_TIME1" title="고장시작일">
														<input type="text" class="inp-comm popRepairResultRegForm_datePic inpCal" readonly="readonly" id="popRepairResultRegForm_TROUBLE_TIME1_DATE" name="TROUBLE_TIME1_DATE" title="고장시작일">
														<div class="sel-wrap wd-per-40 rel mgn-t-5 f-l">
															<select title="고장시작일" id="popRepairResultRegForm_TROUBLE_TIME1_HOUR" name="TROUBLE_TIME1_HOUR">
																<option value="00">00</option>
																<option value="01">01</option>
																<option value="02">02</option>
																<option value="03">03</option>
																<option value="04">04</option>
																<option value="05">05</option>
																<option value="06">06</option>
																<option value="07">07</option>
																<option value="08">08</option>
																<option value="09">09</option>
																<option value="10">10</option>
																<option value="11">11</option>
																<option value="12">12</option>
																<option value="13">13</option>
																<option value="14">14</option>
																<option value="15">15</option>
																<option value="16">16</option>
																<option value="17">17</option>
																<option value="18">18</option>
																<option value="19">19</option>
																<option value="20">20</option>
																<option value="21">21</option>
																<option value="22">22</option>
																<option value="23">23</option>
															</select>
															<span style="position:absolute; top:5px; right:-15px;">시</span>
														</div>
														<div class="sel-wrap wd-per-40 rel mgn-t-5 mgn-l-30 f-l">
															<select title="고장시작일" id="popRepairResultRegForm_TROUBLE_TIME1_MINUTE" name="TROUBLE_TIME1_MINUTE">
																<option value="00">00</option>
																<option value="05">05</option>
																<option value="10">10</option>
																<option value="15">15</option>
																<option value="20">20</option>
																<option value="25">25</option>
																<option value="30">30</option>
																<option value="35">35</option>
																<option value="40">40</option>
																<option value="45">45</option>
																<option value="50">50</option>
																<option value="55">55</option>
															</select>
															<span style="position:absolute; top:5px; right:-15px;">분</span>
														</div>
													</td>
													<th scope="row">고장종료시간</th>
													<td>
														<input type="hidden" class="inp-comm popRepairResultRegForm_datePic inpCal" readonly="readonly" id="popRepairResultRegForm_TROUBLE_TIME2" name="TROUBLE_TIME2" title="고장종료일">
														<input type="text" class="inp-comm popRepairResultRegForm_datePic inpCal" readonly="readonly" id="popRepairResultRegForm_TROUBLE_TIME2_DATE" name="TROUBLE_TIME2_DATE" title="고장종료일">
														<div class="sel-wrap wd-per-40 rel mgn-t-5 f-l">
															<select title="고장종료일" id="popRepairResultRegForm_TROUBLE_TIME2_HOUR" name="TROUBLE_TIME2_HOUR">
																<option value="00">00</option>
																<option value="01">01</option>
																<option value="02">02</option>
																<option value="03">03</option>
																<option value="04">04</option>
																<option value="05">05</option>
																<option value="06">06</option>
																<option value="07">07</option>
																<option value="08">08</option>
																<option value="09">09</option>
																<option value="10">10</option>
																<option value="11">11</option>
																<option value="12">12</option>
																<option value="13">13</option>
																<option value="14">14</option>
																<option value="15">15</option>
																<option value="16">16</option>
																<option value="17">17</option>
																<option value="18">18</option>
																<option value="19">19</option>
																<option value="20">20</option>
																<option value="21">21</option>
																<option value="22">22</option>
																<option value="23">23</option>
															</select>
															<span style="position:absolute; top:5px; right:-15px;">시</span>
														</div>
														<div class="sel-wrap wd-per-40 rel mgn-t-5 mgn-l-30 f-l">
															<select title="고장종료일" id="popRepairResultRegForm_TROUBLE_TIME2_MINUTE" name="TROUBLE_TIME2_MINUTE">
																<option value="00">00</option>
																<option value="05">05</option>
																<option value="10">10</option>
																<option value="15">15</option>
																<option value="20">20</option>
																<option value="25">25</option>
																<option value="30">30</option>
																<option value="35">35</option>
																<option value="40">40</option>
																<option value="45">45</option>
																<option value="50">50</option>
																<option value="55">55</option>
															</select>
															<span style="position:absolute; top:5px; right:-15px;">분</span>
														</div>
													</td>
												</tr>
												<tr>
													<th scope="row">정비 첨부파일</th>
													<td colspan="3">
														<div class="fileWrap" id="viewFrame">
															<div class="f-l wd-per-100">
																<a href="#none" id="modifyBtn" class="btn comm st01 f-l">첨부파일 수정</a>			
															</div>
															<c:choose>
																<c:when test="${fn:length(attachList2)>0 }">
																<div id="previewList" class="wd-per-100">
																	<div class="bxType01 fileWrap previewFileLst">
																		<div>
																			<div class="fileList type02">
																				<div id="detailFileList0" class="fileDownLst">
																					<c:forEach var="item" items="${attachList2}" varStatus="idx">
																						<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																							<span class="MultiFile-export">${item.SEQ_DSP}</span> 
																							<span class="MultiFile-title" title="File selected: ${item.NAME}">
																							${item.FILE_NAME} (${item.FILE_SIZE_TXT})
																							</span>
																							<a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a>
																						</div>		
																					</c:forEach>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
													
																<div id="repair_previewImg" class="wd-per-100">
																	<div class="previewFile mgn-t-15">
																		<h3 class="tit-cont preview" id="previewTitle">이미지 미리보기</h3>
																		<ul style="margin-top:5px;">
																			<c:forEach var="item" items="${attachList2}" varStatus="idx">
																				<c:if test = "${item.FORMAT eq 'jpg' || item.FORMAT eq 'png' || item.FORMAT eq 'jpeg' || item.FORMAT eq 'JPG' || item.FORMAT eq 'PNG'}">
																					<li>
																						<img src="/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}" title="클릭하시면 원본크기로 보실 수 있습니다." onclick="originFileView('/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}')" />
																						<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																							<span class="MultiFile-title" title="File selected: ${item.NAME}">
																								<p>${item.FILE_NAME} (${item.FILE_SIZE_TXT})</p>
																							</span>
																							<a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a>
																						</div>
																						
																					</li>
																				</c:if>
																			</c:forEach>
																		</ul>
																	</div>
																</div>
																</c:when>
																<c:otherwise>
																	<div class="f-l wd-per-100 mgn-t-5">
																	※ 등록된 첨부파일이 없습니다.
																	</div>
																</c:otherwise>
															</c:choose>
														</div>
														<div class="bxType01 fileWrap" id="regFrame" style="display:none;">
															<div id="attachFile" class="filebox"></div>
															<div class="fileList">
																<div id="detailFileRegList0" class="fileDownLst">
																	<c:forEach var="item" items="${attachList2}" varStatus="idx">
																		<div class="MultiFile-label" id="fileRegLstWrap_${item.ATTACH}">
																			<span class="MultiFile-remove">
																				<a href="#none" class="icn_fileDel" data-attach="${item.ATTACH}" data-idx="${item.CD_FILE_TYPE }">
																					<img src="/images/com/web/btn_remove.png" />
																				</a>
																			</span> 
																			<span class="MultiFile-title" title="File selected: ${item.NAME}">
																				${item.FILE_NAME} (${item.FILE_SIZE_TXT})
																			</span>
																		</div>
																	</c:forEach>
																</div>
															</div>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</form>
								</div>
							</div>
							<%-- 실적등록 판넬 : e --%>
						</div>
					</div>
					
				</form>
				
				<iframe id="fileDownFrame" style="width:0px;height:0px;border:0px"></iframe>
			</div> <%-- modal-bodyIn : e --%>
		</div> <%-- modal-body : e --%>
	</div> <%-- modal-content : e --%>
</div> <%-- modal : e --%>