<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: mainRepairResultListDtl.jsp
	Description : 대시보드 > 정비실적 현황 상세페이지
	author		: 김영환
	since		: 2018.06.18
 
	<< 개정이력(Modification Information) >>
	수정일 		 수정자		수정내용
	----------  ------	---------------------------
	2018.06.18	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

</head>
<body>
	<div class="expandPortletWrap bgWhite">
		<div class="expandSchWrap rel">
			<div class="wrap-inq">	
				<div class="inq-clmn">
					<h4 class="tit-inq"><spring:message code='epms.location' /></h4><!-- 공장 -->
					<div class="sel-wrap type02" style="width:120px">
						<select title="공장" id="LOCATION" name="LOCATION" >
							<c:choose>
								<c:when test="${fn:length(locationList) > 0}">
									<c:choose>
										<c:when test="${param.type eq 'mainSlide'}">
											<c:forEach var="item" items="${locationList}">
												<c:if test="${param.LOCATION eq item.CODE}">
												<option value="${item.CODE}" role="${item.REMARKS}"  selected="selected">${item.NAME}</option>
												</c:if>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<c:forEach var="item" items="${locationList}">
												<option value="${item.CODE}" role="${item.REMARKS}" <c:if test="${sessionScope.ssLocation eq item.CODE}"> selected="selected"</c:if>>${item.NAME}</option>
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									<option value="" selected="selected">권한필요</option>
								</c:otherwise>
							</c:choose>
						</select>
					</div>
				</div>
				<div class="inq-clmn">
					<h4 class="tit-inq">파트</h4>
					<div class="sel-wrap type02" style="width:80px">
						<tag:combo codeGrp="PART" name="PART" companyId="${ssCompanyId}" all="Y"/>
					</div>
				</div>
			</div>
			
			<div class="monthMoveWrap ab">
				<div class="monthMoveBtn prev"><div id="monthPrev"><</div></div>
				<div class="monthMoveInfo"><span id="monthMoveInfo"></span></div>
				<div class="monthMoveBtn next"><div id="monthNext">></div></div>
			</div>
		</div>
		
		<div class="expandPortletH02">
			<div class="exapndCountDtl">
				<ul class="statusDtlWrap" id="dashboardInfo">
					<li class="statusDtl v-center" id="REPAIR_REQUEST_LI">
						<div>
							<p class="statusTit"><spring:message code='epms.work.request' /><p><!-- 정비요청 -->
							<p class="statusCont" name="REPAIR_REQUEST_CNT" id="REPAIR_REQUEST"></p>
						</div>
					</li>	
					<li class="statusDtl v-center" id="REPAIR_COMPLETE_LI">
						<div>
							<p class="statusTit"><spring:message code='epms.repair.complete' /><p><!-- 정비완료 -->
							<p class="statusCont" name="REPAIR_COMPLETE_CNT" id="REPAIR_COMPLETE"></p>
							<p class="stutusWarning"><spring:message code='epms.work.urgency' /><span class="fontRed" name="REPAIR_EM_CNT"></p><!-- 돌발정비 -->
						</div>
					</li>							
					<li class="statusDtl v-center" id="REPAIR_ONGOING_LI">
						<div>
							<p class="statusTit">진행중<p>
							<p class="statusCont" name="REPAIR_ONGOING_CNT" id="REPAIR_ONGOING"></p>
						</div>
						<p class="stutusWarning">지연 <span class="fontRed" name="REPAIR_EXCESS_CNT"></p>
					</li>
					<li class="statusDtl v-center" id="REPAIR_APPR_LI">
						<div>
							<p class="statusTit">결제대기<p>
							<p class="statusCont" name="REPAIR_APPR_CNT" id="REPAIR_APPR"></p>
						</div>
					</li>
					<li class="statusDtl v-center" id="REPAIR_REJECT_LI">
						<div>
							<p class="statusTit">반려<p>
							<p class="statusCont" name="REPAIR_REJECT_CNT" id="REPAIR_REJECT"></p>
						</div>
					</li>
					<li class="statusDtl v-center" id="REPAIR_CANCEL_LI">
						<div>
							<p class="statusTit"><spring:message code='epms.work.cancel' /><p><!-- 정비취소 -->
							<p class="statusCont" name="REPAIR_CANCEL_CNT" id="REPAIR_CANCEL"></p>
						</div>
					</li>
				</ul>
			</div>
			
			<div class="expandChartDtl">
				<div class="expandChartDtlIn">
					<div class="f-l wd-per-35">
						<div id="repairTypeChartDiv" class="expandChart" style="height:62vh;">
							<div class="expandChartTit"><spring:message code='epms.work.type' /></div><!-- 정비 유형 -->
							<canvas id="repairType"></canvas>
						</div>
					</div>
					<div class="f-l wd-per-65">
						<div class="mgn-l-10">
							<div>
								<div id="troubleTimeChartDiv" class="expandChart" style="height:30vh;">
									<div class="expandChartTit"><spring:message code='epms.occurr.time' /></div><!-- 고장 시간 -->
									<canvas id="troubleTime"></canvas>
								</div>
							</div>
							<div style="margin-top:2vh">
								<div class="wd-per-50 f-l">
									<div id="troubleType1ChartDiv" class="expandChart" style="height:30vh;">
										<div class="expandChartTit"><spring:message code='epms.occurr.type' /></div><!-- 고장 유형 -->
										<canvas id="troubleType1"></canvas>
									</div>
								</div>
								<div class="wd-per-50 f-l">
									<div id="troubleType2ChartDiv" class="expandChart mgn-l-10" style="height:30vh;">
										<div class="expandChartTit"><spring:message code='epms.work.type' /></div><!-- 조치 유형 -->
										<canvas id="troubleType2"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
					
		</div>
	</div>
<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<%-- 정비실적조회 --%>
<div class="modal fade modalFocus" id="popRepairResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 정비실적등록 --%>
<div class="modal fade modalFocus" id="popRepairResultRegForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 정비요청/결재 --%>
<%@ include file="/WEB-INF/jsp/epms/main/popMainRepairApprList.jsp"%>

<%-- 정비실적 --%>
<%@ include file="/WEB-INF/jsp/epms/main/popMainRepairResultList.jsp"%> 

<%-- 고장사진(첨부파일) --%>
<div class="modal fade modalFocus" id="popFileManageForm"   data-backdrop="static" data-keyboard="false"></div>

<%-- 정비원 검색 --%>
<div class="modal fade modalFocus" id="popWorkerList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-25">		
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">
                    <spring:message code='epms.worker' /> <spring:message code='epms.search' />
                </h4><!-- 정비원 검색 -->
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	           		<div class="modal-section">
						<div class="fl-box panel-wrap-modal wd-per-100">
							<div class="panel-body" id="snsList">
								<bdo Debug="Error"
									 Data_Url=""
									 Layout_Url="/epms/repair/result/popWorkerListLayout.do"
								 >
								</bdo>
							</div>
						</div>
					</div> <%-- modal-section : e --%>
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>


</body>
<script type="text/javascript">
var year = 1;													<%-- [YYYY 연도] 전역변수 --%>
var month = 1;													<%-- [MM 월] 전역변수 --%>
var month_rare = 1;												<%-- [가공 전 월] 전역변수 --%>
var repairType   = document.getElementById("repairType");		<%-- [정비유형 차트 데이터] 전역변수 --%>
var troubleTime  = document.getElementById("troubleTime");		<%-- [정비시간 차트 데이터] 전역변수 --%>
var troubleType1 = document.getElementById("troubleType1");		<%-- [정비 조치유형 차트 데이터] 전역변수 --%>
var troubleType2 = document.getElementById("troubleType2"); 	<%-- [정비 대처유형 차트 데이터] 전역변수 --%>

<%-- bar형태 차트 옵션 --%>
var barOption = {
	maintainAspectRatio: false,
	responsive: true,
	tooltips: {
		enabled: true,
		intersect: false,
		titleFontSize: 11,
		bodyFontSize: 11,
		callbacks: {
			label: function(tooltipItem, data) {
				var allData = data.datasets[tooltipItem.datasetIndex].data;
				var tooltipLabel = data.labels[tooltipItem.index];
				var tooltipData = allData[tooltipItem.index];
				var total = 0;
				for (var i=0; i<allData.length; i++) {
					total += allData[i];
				}
				var tooltipPercentage = Math.round((tooltipData / total) * 100);
				
				return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
			}
		}
	},
	hover :{
		animationDuration:0
	},
	scales: {
		yAxes: [{
			stacked: true,
			gridLines: {
			},
			ticks: {
				beginAtZero:true,
				fontFamily: "'Open Sans Bold', sans-serif",
				fontSize:11
			},
			scaleLabel:{
				display:false
			}
		}],
		xAxes: [{
			barPercentage: 0.5,
			gridLines: {
				display:false,
					color: "#fff",
					zeroLineColor: "#fff",
					zeroLineWidth: 0
			},
			ticks: {
				fontFamily: "'Open Sans Bold', sans-serif",
				fontSize:11
			}
		}]
	},
	legend:{
		display:false,
		position : 'right',
		labels:{
			fontFamily: "'Open Sans Bold', sans-serif",
			fontSize:11
		}
	},
    'onClick' : function (event) {

    	if(this.canvas.id == 'repairType'){
        	fnOpenLayerWithGrid("Y", "repairTypeChart", "");
           
   	 	}else if(this.canvas.id == 'troubleTime'){
   	 		fnOpenLayerWithGrid("Y", "troubleTimeChart", "");
   	 	}
    }
}

<%-- pie형태 차트 옵션 --%>
var pieOption = {
	maintainAspectRatio: false,
	responsive: true,
	tooltips: {
		enabled: true,
		intersect: false,
		titleFontSize: 11,
		bodyFontSize: 11,
		callbacks: {
			label: function(tooltipItem, data) {
				var allData = data.datasets[tooltipItem.datasetIndex].data;
				var tooltipLabel = data.labels[tooltipItem.index];
				var tooltipData = allData[tooltipItem.index];
				var total = 0;
				for (var i=0; i<allData.length; i++) {
					total += allData[i];
				}
				var tooltipPercentage = Math.round((tooltipData / total) * 100);
				
				return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
			}
		}
	},
	hover :{
		animationDuration:0
	},
	legend:{
		display:true,
		position : 'right',
		labels:{
			boxWidth:12,
			fontFamily: "'Open Sans Bold', sans-serif",
			fontSize:11
		}
	},
    'onClick' : function (event) {
    	
    	if(this.canvas.id == 'troubleType1'){
    		fnOpenLayerWithGrid("Y", "troubleType1Chart", "");
    		
    	}else if(this.canvas.id == 'troubleType2'){
			fnOpenLayerWithGrid("Y", "troubleType2Chart", "");
    	}
    }
}

<%-- 정비유형 그래프 작성 --%>
var repairTypeChart = new Chart(repairType, {
		type: 'bar'
		, data: {
			labels : [],
			datasets : [
			    {
					label: "실적건수",
					data: [],
					backgroundColor: ["rgba(242, 99, 95, 1)", "rgba(244, 208, 12, 1)",  "rgba(75, 192, 192, 1)", "rgba(183, 80, 144, 1)"],
					hoverBackgroundColor: ["rgba(242, 99, 95, 0.5)", "rgba(244, 208, 12, 0.5)", "rgba(75, 192, 192, 0.5)", "rgba(183, 80, 144, 0.5)"],
					borderWidth:1
				}
			]
		}
		, options: barOption
	});


<%-- 고장시간 그래프 작성 --%>
var	troubleTimeChart = new Chart(troubleTime, {
		type: 'bar'
		, data: {
			labels : [],
			datasets : [
			    {
			    	label: '실적 건수',			
			    	data: [],
			    	backgroundColor: "rgba(0, 147, 209, 1)",
			    	hoverBackgroundColor: "rgba(0, 147, 209, 0.5)"
				}
			]
		}
		, options: barOption
	});

<%-- 고장유형 그래프 작성 --%>
var troubleType1Chart = new Chart(troubleType1, {
		type: 'pie'
		, data: {
			labels : [],
			datasets : [
			    {
			    	label: ['실적 건수'],			
			    	data: [],
			    	backgroundColor: [],
			    	hoverBackgroundColor: []
			    }
			]
		},
		options: pieOption
	});

<%-- 조치유형 그래프 작성 --%>
var troubleType2Chart = new Chart(troubleType2, {
		type: 'pie'
		, data: {
			labels : [],
			datasets : [
			    {
			    	label: ['실적 건수'],			
			    	data: [],
			    	backgroundColor: [],
			    	hoverBackgroundColor: []
			    }
			]
		},
		options: pieOption
	});

$(document).ready(function(){			
	$(".boxscroll").niceScroll({cursorwidth:"4px",cursorcolor:"#afbfd6",horizrailenabled:false});
	
	<%-- 현재 날짜 설정 --%>
	var date = new Date();
	year = date.getFullYear();
	month_rare = parseInt(date.getMonth()) + 1;
	
	<%-- 화면 데이터 입력--%>
	fnSetData();
	
	<%-- default가 현재 날짜이므로 다음달 선택 불가 --%>
	$('.monthMoveBtn.next').hide();
	
	<%-- 이전 달로 이동 --%>
	$('#monthPrev').on('click', function(){
		<%-- 1월일 경우 년도 차감 --%>
		if(month_rare == 1){
			year = parseInt(year) - 1;
			month_rare = 12
		}else{
			month_rare = parseInt(month_rare) - 1;
		}
		
		<%-- 조회한 날짜가 현재일 경우 다음 날짜선택버튼 숨김 --%>
		if(year == date.getFullYear() && month_rare == date.getMonth()+1){
			$('.monthMoveBtn.next').hide();
		} else{
			$('.monthMoveBtn.next').show();
		}
		
		<%-- 화면 데이터 입력--%>
		fnSetData();
	});
	
	<%-- 다음 달로 이동 --%>
	$('#monthNext').on('click', function(){
		<%-- 12월일 경우 년도 증가 --%>
		if(month_rare == 12){
			year = parseInt(year) + 1;
			month_rare = 1
		}else{
			month_rare = parseInt(month_rare) + 1;
		}
		
		<%-- 조회한 날짜가 현재일 경우 다음 날짜선택버튼 숨김 --%>
		if(year == date.getFullYear() && month_rare == date.getMonth()+1){
			$('.monthMoveBtn.next').hide();
		} else{
			$('.monthMoveBtn.next').show();
		}
		
		<%-- 화면 데이터 입력--%>
		fnSetData();
	});
	
	<%-- 공장,파트 값 변경 시 --%>
	$('#LOCATION,#PART').on('change', function(){
		<%-- 화면 데이터 입력--%>
		fnSetData();
	});
	
	<%-- 정비 카운트 클릭 시 --%>
	$(".statusDtl").on('click', function(){
		if($(this).find('.statusCont').text() != 0){
			var param = $(this).find('.statusCont').attr('id');
			fnOpenLayerWithGrid("N", '', param);
		}
	})
});

<%-- 화면 내 대시보드/차트 데이터 세팅 --%>
function fnSetData(){
	month_rare < 10 ? month = "0"+month_rare : month = month_rare;
	
	$("#monthMoveInfo").text(year + "년 " + month + "월");
	
	var yearMonth = year+""+month;
	var location = $("#LOCATION").val();
	var part = $("#PART").val();
	var remarks = $("select[name=LOCATION] option:selected").attr("role");
	
	fnGetRepairResultInfo(yearMonth, location, part, remarks);
}

<%-- 검색 일자별 정비현황 대시보드 --%>
function fnGetRepairResultInfo(date, location, part, remarks){
	$.ajax({
		type : 'POST',
		url : '/epms/main/selectRepairResultInfo.do',
		cache : false,
		dataType : 'json',
		data : { DATE_REQUEST : date
				 , REMARKS  : remarks
				 , LOCATION : location
				 , PART 	: part },
		async : false,
		error : function() {
			alert("처리중 오류가 발생 하였습니다.");
		},
		success : function(json) {
			var repairInfo = json.repairInfo;
			var _ul = $("#dashboardInfo");
			
			for( var name in repairInfo ) {
            	switch (name) {
	 				default:
	 					_ul.find("[name=" + name + "]").text(repairInfo[name]);
	 				break;
 				}
			}
			
			<%-- 차트 생성 --%>
			drawChart(json);
		}		
	});
}

<%-- 랜덤 색상 생성 --%>
var dynamicColors = function(){
	var r = Math.floor(Math.random()* (255-200+1))+200;
	var g = Math.floor(Math.random()* (255-200+1))+200;
	var b = Math.floor(Math.random()* (255-200+1))+200;

	return ["rgba("+r+","+g+","+b+", 1)", "rgba("+r+","+g+","+b+", 0.5)"];
}

<%-- 차트별 데이터 비교 전역변수 --%>
var repairTypeID = [];
var troubleTimeID = [];
var troubleType1ID = [];
var troubleType2ID = [];

<%-- 차트 데이터 생성 --%>
function drawChart(json) {
	
	<%-- 배열 초기화 --%>
	repairTypeID = [];  
	troubleTimeID = []; 
	troubleType1ID = [];
	troubleType2ID = [];
	
	<%-- 정비유형 --%>
	var repairTypeData = [], repairTypeLabel = [];
	
	$.each(json.REPAIR_TYPE, function(idx, val){
		repairTypeData.push(val.REPAIR_TYPE_CNT);
		repairTypeLabel.push(val.REPAIR_TYPE_NAME);
		
		repairTypeID.push(val.REPAIR_TYPE);
	});
	
	repairTypeChart.data.labels = repairTypeLabel;
	repairTypeChart.data.datasets[0].data = repairTypeData;
	repairTypeChart.update();
	
	
	
	<%-- 고장시간 --%>
	var troubleTimeData = [json.TROUBLE_TIME.MIN30, json.TROUBLE_TIME.MIN60, json.TROUBLE_TIME.MIN120, json.TROUBLE_TIME.MIN240, json.TROUBLE_TIME.MIN480, json.TROUBLE_TIME.MINOVER];
	var troubleTimeLabel = ['30분미만', '30분~1시간', '1~2시간', '2~4시간', '4~8시간', '8시간 초과'];
	
	$.each(troubleTimeLabel, function(idx, val){
		troubleTimeID.push(idx);
	});
	
	troubleTimeChart.data.labels = troubleTimeLabel;
	troubleTimeChart.data.datasets[0].data = troubleTimeData;
	troubleTimeChart.update();
	
	<%-- 고장유헝 --%>
	var troubleType1Data = [], troubleType1Label = [], troubleType1Color = [], troubleType1Hover = [];
	
	$.each(json.TROUBLE_TYPE1, function(idx, val){
		troubleType1Label.push(val.TROUBLE_TYPE1_NAME);
		troubleType1Data.push(val.TROUBLE_TYPE1_COUNT);
		
		var tempColor = dynamicColors();
		troubleType1Color.push(tempColor[0]);
		troubleType1Hover.push(tempColor[1]);
		
		troubleType1ID.push(val.TROUBLE_TYPE1);
	});
	
	troubleType1Chart.data.labels = troubleType1Label;
	troubleType1Chart.data.datasets[0].data = troubleType1Data;
	troubleType1Chart.data.datasets[0].backgroundColor = troubleType1Color;
	troubleType1Chart.data.datasets[0].hoverBackgroundColor = troubleType1Hover;
	troubleType1Chart.update();
	
	<%-- 조치유헝 --%>
	var troubleType2Data = [], troubleType2Label = [], troubleType2Color = [], troubleType2Hover = [];
	
	$.each(json.TROUBLE_TYPE2, function(idx, val){
		troubleType2Label.push(val.TROUBLE_TYPE2_NAME);
		troubleType2Data.push(val.TROUBLE_TYPE2_COUNT);
		
		var tempColor = dynamicColors();
		troubleType2Color.push(tempColor[0]);
		troubleType2Hover.push(tempColor[1]);
		
		troubleType2ID.push(val.TROUBLE_TYPE2);
	});
	
	troubleType2Chart.data.labels = troubleType2Label;
	troubleType2Chart.data.datasets[0].data = troubleType2Data;
	troubleType2Chart.data.datasets[0].backgroundColor = troubleType2Color;
	troubleType2Chart.data.datasets[0].hoverBackgroundColor = troubleType2Hover;
	troubleType2Chart.update();
}

<%-- 대시보드/차트 클릭 시 상세 실적 리스트 --%>
function fnOpenLayerWithGrid(flag, chart, value){

	var PARAM_DATE = year +""+ month;
	var PART = $("#PART").val() == "all" ? "" : $("#PART").val();
	var STATUS = ""
	
	if(flag == "Y"){
		if(chart == "repairTypeChart"){
			var activePoints = repairTypeChart.getElementsAtEvent(event);
		    var firstPoint = activePoints[0];
		    STATUS = repairTypeID[firstPoint._index];
		    
		}else if(chart == "troubleTimeChart"){
			var activePoints = troubleTimeChart.getElementsAtEvent(event);
		    var firstPoint = activePoints[0];
		    STATUS = troubleTimeID[firstPoint._index];
		    
		}else if(chart == "troubleType1Chart"){
			var activePoints = troubleType1Chart.getElementsAtEvent(event);
		    var firstPoint = activePoints[0];
		    STATUS = troubleType1ID[firstPoint._index];
		    
		}else if(chart == "troubleType2Chart"){
			var activePoints = troubleType2Chart.getElementsAtEvent(event);
		    var firstPoint = activePoints[0];
		    STATUS = troubleType2ID[firstPoint._index];
		}
		
	} else {
		STATUS = value;
	}
	
	if (value == "REPAIR_REQUEST"){ <%-- 정비요청 클릭시 --%>
		$("#popMainRepairApprList_title").text("정비현황(정비요청)");
	
	}else if (value == "REPAIR_APPR") { <%-- 대기 클릭시 --%>
		$("#popMainRepairApprList_title").text("정비현황(결제대기)");
		
	}else if (value == "REPAIR_REJECT") { <%-- 반려 클릭시 --%>
		$("#popMainRepairApprList_title").text("정비현황(반려)");
		
	}else if (value == "REPAIR_CANCEL") { <%-- 정비취소 클릭시 --%>
		$("#popMainRepairApprList_title").text("정비현황(정비취소)");
		
	}else if (value == "REPAIR_COMPLETE") { <%-- 정비실적 정비완료 클릭시 --%>
		$("#popMainRepairResultList_title").text("정비현황(정비완료)");
		
	}else if (value == "REPAIR_ONGOING") { <%-- 정비실적 진행중 클릭시 --%>
		$("#popMainRepairResultList_title").text("정비현황(진행중)");
		
	}else if (chart == "repairTypeChart") { <%-- 정비유형 차트 클릭 --%>
		$("#popMainRepairResultList_title").text("정비현황(정비유형)");
		
	}else if (chart == "troubleTimeChart") { <%-- 고장시간 차트 클릭 --%>
		$("#popMainRepairResultList_title").text("정비현황(고장시간)");
		
	}else if (chart == "troubleType1Chart") { <%-- 고장유형 차트 클릭 --%>
		$("#popMainRepairResultList_title").text("정비현황(고장유형)");
		
	}else if (chart == "troubleType2Chart") { <%-- 조치유형 차트 클릭 --%>
		$("#popMainRepairResultList_title").text("정비현황(조치유형)");
	}
    
	if(value == "REPAIR_REQUEST" || value == "REPAIR_APPR" || value == "REPAIR_CANCEL" || value == "REPAIR_REJECT"){
		Grids.PopMainRepairApprList.Source.Data.Url = "/epms/main/popRepairApprListOptData.do?APPR_STATUS="+STATUS
    												+ "&PARAM_DATE="+PARAM_DATE
    												+ "&PART="+PART
    												+ "&LOCATION="+$("#LOCATION").val()
    												+ "&REMARKS="+$("select[name=LOCATION] option:selected").attr("role");
		
 		Grids.PopMainRepairApprList.Reload();
		fnModalToggle('popMainRepairApprList');
    } else {
    	Grids.PopMainRepairResultList.Source.Data.Url = "/epms/main/popRepairResultListOptData.do?&STATUS="+STATUS
    												  + "&DATE_REQUEST="+PARAM_DATE
													  + "&PART="+PART
													  + "&LOCATION="+$("#LOCATION").val()
													  + "&flag="+flag
													  + "&CHART="+chart
													  + "&REMARKS="+$("select[name=LOCATION] option:selected").attr("role");
    	
		Grids.PopMainRepairResultList.Reload();
		fnModalToggle('popMainRepairResultList');
    }
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

</script>
</html>