<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: mainWorkStatListDtl.jsp
	Description : 대시보드 > 정비원 업무현황 상세페이지
	author		: 김영환
	since		: 2018.06.18
 
	<< 개정이력(Modification Information) >>
	수정일 		 수정자		수정내용
	----------  ------	---------------------------
	2018.06.18	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

</head>
<body>

	<div class="expandPortletWrap">
		<div class="expand-workStatList">
			<div class="f-l wd-per-30">
				<div class="factoryTit">
					<div class="wrap-inq">	
						<div class="inq-clmn">
							<h4 class="tit-inq">공장</h4>
							<div class="sel-wrap type02" style="width:120px">
								<select title="공장" id="LOCATION" name="LOCATION" >
									<c:choose>
										<c:when test="${fn:length(locationList) > 0}">
											<c:forEach var="item" items="${locationList}">
												<option value="${item.CODE}" role="${item.REMARKS}" <c:if test="${sessionScope.ssLocation eq item.CODE}"> selected="selected"</c:if>>${item.NAME}</option>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<option value="" selected="selected">권한필요</option>
										</c:otherwise>
									</c:choose>
								</select>
							</div>
						</div>
						<div class="inq-clmn">
							<h4 class="tit-inq">파트</h4>
							<div class="sel-wrap type02" style="width:80px">
								<tag:combo codeGrp="PART" name="PART" companyId="${ssCompanyId}" all="Y"/>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div id="mainWorkerList" class="expandPortletH01 boxscroll">
					<ul class="lobipanel-list">
					</ul>
				</div>
			</div>
			<div class="f-l wd-per-70">
				<div class="expandedTabSet">
					<ul class="expandedTabs">
						<li class="expandedTabLink active" onclick="openTab(event, 'panel1-1')" id="defaultOpen"><spring:message code='epms.perform.status' /></li><!-- 정비실적 현황 -->
<!-- 						<li class="expandedTabLink" onclick="openTab(event, 'panel1-2')">예방보전실적 현황</li> -->
					</ul>
 					<div class="panels">
 					
 						<!-- s : panel1-1 -->
						<div class="panel" id="panel1-1" style="display:block">
							<div class="panel-in rel">
								<div class="workMonthMoveWrap">
									<div class="monthMoveWrap ab">
										<div class="monthMoveBtn prev"><div class="monthPrev"><</div></div>
										<div class="monthMoveInfo"><span class="monthMoveInfo"></span></div>
										<div class="monthMoveBtn next"><div class="monthNext">></div></div>
									</div>
								</div>
								<div class="boxscroll expandPortletH03">
									<div class="exapndCountDtl">
										<ul class="statusDtlWrap" id="repair_dashboardInfo">
											<li class="statusDtl v-center" id="REPAIR_ASSIGN_LI">
												<div>
													<p class="statusTit">업무배정<p>
													<p class="statusCont" name="REPAIR_ASSIGN_CNT" id="REPAIR_ASSIGN"></p>
												</div>
											</li>	
											<li class="statusDtl v-center" id="REPAIR_COMPLETE_LI">
												<div>
													<p class="statusTit"><spring:message code='epms.repair.complete' /><p><!-- 정비완료 -->
													<p class="statusCont" name="REPAIR_COMPLETE_CNT" id="REPAIR_COMPLETE"></p>
													<p class="stutusWarning">돌발정비 <span class="fontRed" name="REPAIR_EM_CNT"></p>
												</div>
											</li>							
											<li class="statusDtl v-center" id="REPAIR_ONGOING_LI">
												<div>
													<p class="statusTit">정비진행중<p>
													<p class="statusCont" name="REPAIR_ONGOING_CNT" id="REPAIR_ONGOING"></p>
												</div>
												<p class="stutusWarning">지연 <span class="fontRed" name="REPAIR_EXCESS_CNT"></p>
											</li>
										</ul>
									</div>
									<div class="expandChartDtl wd-per-95">
										<div class="expandChartDtlIn">
											<div class="f-l wd-per-35">
												<div id="repairTypeChartDiv" class="expandChart" style="height:64vh;">
													<div class="expandChartTit">정비 유형</div>
													<canvas id="repairTypeChart"></canvas>
												</div>
											</div>
											<div class="f-l wd-per-65">
												<div class="mgn-l-10">
													<div id="troubleTimeChartDiv" class="expandChart" style="height:32vh;">
														<div class="expandChartTit">고장 시간</div>
														<canvas id="troubleTimeChart"></canvas>
													</div>
													<div style="margin-top:1vh">
														<div id="troubleType1ChartDiv" class="expandChart wd-per-50 f-l" style="height:31vh;">
															<div class="expandChartTit">고장 유형</div>
															<canvas id="troubleType1Chart"></canvas>
														</div>
														<div class="wd-per-50 f-l">
															<div id="troubleType2ChartDiv" class="expandChart mgn-l-10" style="height:31vh;">
																<div class="expandChartTit">조치 유형</div>
																<canvas id="troubleType2Chart"></canvas>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- e : panel1-1 -->
						
						 <!-- s : panel1-2 -->
						<div class="panel" id="panel1-2" style="display:none">
							<div class="panel-in rel">
								<div class="workMonthMoveWrap">
									<div class="monthMoveWrap ab">
										<div class="monthMoveBtn prev"><div class="monthPrev"><</div></div>
										<div class="monthMoveInfo"><span class="monthMoveInfo"></span></div>
										<div class="monthMoveBtn next"><div class="monthNext">></div></div>
									</div>
								</div>
								<div class="boxscroll expandPortletH03">
									<div class="exapndCountDtl">
										<ul class="statusDtlWrap" id="preventive_dashboardInfo">
											<li class="statusDtl v-center" id="PREVENTIVE_PLAN_LI">
												<div>
													<p class="statusTit">예방보전 계획<p>
													<p class="statusCont" name="PREVENTIVE_PLAN_CNT" id="PREVENTIVE_PLAN"></p>
												</div>
											</li>	
											<li class="statusDtl v-center" id="REPAIR_COMPLETE_LI">
												<div>
													<p class="statusTit">점검완료<p>
													<p class="statusCont" name="PREVENTIVE_COMPLETE_CNT" id="PREVENTIVE_COMPLETE"></p>
													<p class="stutusWarning">정비필요 <span class="fontRed" name="PREVENTIVE_REPAIR_REQUEST_CNT"></p>
												</div>
											</li>	
											<li class="statusDtl v-center" id="PREVENTIVE_ONGOING_LI">
												<div>
													<p class="statusTit">진행중<p>
													<p class="statusCont" name="PREVENTIVE_ONGOING_CNT" id="PREVENTIVE_ONGOING"></p>
												</div>
												<p class="stutusWarning">지연 <span class="fontRed" name="PREVENTIVE_EXCESS_CNT"></p>
											</li>
											<li class="statusDtl v-center" id="PREVENTIVE_UNCHECK_LI">
												<div>
													<p class="statusTit">미점검<p>
													<p class="statusCont" name="PREVENTIVE_UNCHECK_CNT" id="PREVENTIVE_UNCHECK"></p>
												</div>
											</li>						
										</ul>
									</div>
									<div class="expandChartDtl wd-per-95">
										<div class="expandChartDtlIn">
											<div class="f-l wd-per-35">
												<div id="repairTypeChartDiv" class="expandChart" style="height:32vh;">
													<div class="expandChartTit">TBM</div>
													<canvas id="preventiveTBM"></canvas>
												</div>
												<div style="margin-top:1vh">
													<div id="repairTypeChartDiv" class="expandChart " style="height:31vh;">
														<div class="expandChartTit">CBM</div>
														<canvas id="preventiveCBM"></canvas>
													</div>
												</div>
											</div>
											<div class="f-l wd-per-65">
												<div class="mgn-l-10">
													<div>
														<div id="troubleTimeChartDiv" class="expandChart" style="height:32vh;">
															<div class="expandChartTit">계획일 초과현황</div>
															<canvas id="preventiveDayOverChart"></canvas>
														</div>
													</div>
													<div style="margin-top:1vh">
														<div class="wd-per-50 f-l">
															<div id="troubleType1ChartDiv" class="expandChart" style="height:31vh;">
																<div class="expandChartTit">CBM 판정</div>
														<canvas id="preventiveCBM_DECISION"></canvas>
															</div>
														</div>
														<div class="wd-per-50 f-l">
															<div id="troubleType2ChartDiv" class="expandChart mgn-l-10" style="height:31vh;">
																<div class="expandChartTit">예방정비 수행현황</div>
																<canvas id="preventivePerformChart"></canvas>
															</div>
														</div>
													</div>
												</div>
											</div>									
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- e : panel1-2 -->
						
					</div>
				</div>
			</div>
		</div>
	</div>
<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<%-- 정비실적조회 --%>
<div class="modal fade modalFocus" id="popRepairResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 정비실적등록 --%>
<div class="modal fade modalFocus" id="popRepairResultRegForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 정비요청/결재 --%>
<%@ include file="/WEB-INF/jsp/epms/main/popMainRepairApprList.jsp"%>

<%-- 정비실적 --%>
<%@ include file="/WEB-INF/jsp/epms/main/popMainRepairResultList.jsp"%> 

<%-- 예방보전실적조회 --%>
<div class="modal fade modalFocus" id="popPreventiveResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 예방보전 계획 리스트 --%>
<%@ include file="/WEB-INF/jsp/epms/main/popMainPreventivePlanList.jsp"%>
</body>
<script type="text/javascript">
var year = 1;													<%-- [YYYY 연도] 전역변수 --%>
var month = 1;													<%-- [MM 월] 전역변수 --%>
var month_rare = 1;												<%-- [가공 전 월] 전역변수 --%>
var MANAGER = "";												<%-- [정비원ID] 전역변수 --%>
var CHECK_TYPE = "";
var CHECK_DECISION = "";

<%-- 차트별 데이터 비교 전역변수 --%>
var repairTypeID = [];
var troubleTimeID = [];
var troubleType1ID = [];
var troubleType2ID = [];
var tbmDataID = [];
var cbmDataID = [];
var cbmDecisionDataID = [];
var performDataID = [];

<%-- bar형태 차트 옵션 --%>
var barOption = {
	maintainAspectRatio: false,
	responsive: true,
	tooltips: {
		enabled: true,
		intersect: false,
		titleFontSize: 11,
		bodyFontSize: 11,
		callbacks: {
			label: function(tooltipItem, data) {
				var allData = data.datasets[tooltipItem.datasetIndex].data;
				var tooltipLabel = data.labels[tooltipItem.index];
				var tooltipData = allData[tooltipItem.index];
				var total = 0;
				for (var i=0; i<allData.length; i++) {
					total += allData[i];
				}
				var tooltipPercentage = Math.round((tooltipData / total) * 100);
				
				return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
			}
		}
	},
	hover :{
		animationDuration:0
	},
	scales: {
		yAxes: [{
			stacked: true,
			gridLines: {
			},
			ticks: {
				beginAtZero:true,
				fontFamily: "'Open Sans Bold', sans-serif",
				fontSize:11
			},
			scaleLabel:{
				display:false
			}
		}],
		xAxes: [{
			barPercentage: 0.5,
			gridLines: {
				display:false,
					color: "#fff",
					zeroLineColor: "#fff",
					zeroLineWidth: 0
			},
			ticks: {
				fontFamily: "'Open Sans Bold', sans-serif",
				fontSize:11
			}
		}]
	},
	legend:{
		display:false,
		position : 'right',
		labels:{
			fontFamily: "'Open Sans Bold', sans-serif",
			fontSize:11
		}
	},
    'onClick' : function (event) {

    	if(this.canvas.id == 'repairTypeChart'){
			fnOpenLayerWithGrid("Y", "repairTypeChart", "");
           
   	 	}else if(this.canvas.id == 'troubleTimeChart'){ 
   	 		fnOpenLayerWithGrid("Y", "troubleTimeChart", "");
   	 		
   	 	}else if(this.canvas.id == 'preventiveTBM'){
   	 		fnOpenLayerWithGrid("Y", "preventiveTBM", "");
	        
   	 	}else if(this.canvas.id == 'preventiveCBM'){
   	 		fnOpenLayerWithGrid("Y", "preventiveCBM", "");
	        
   	 	} else if(this.canvas.id == 'preventiveDayOverChart'){
   	 		fnOpenLayerWithGrid("Y", "preventiveDayOverChart", "");
   	 	}
    }
}

<%-- pie형태 차트 옵션 --%>
var pieOption = {
	maintainAspectRatio: false,
	responsive: true,
	tooltips: {
		enabled: true,
		intersect: false,
		titleFontSize: 11,
		bodyFontSize: 11,
		callbacks: {
			label: function(tooltipItem, data) {
				var allData = data.datasets[tooltipItem.datasetIndex].data;
				var tooltipLabel = data.labels[tooltipItem.index];
				var tooltipData = allData[tooltipItem.index];
				var total = 0;
				for (var i=0; i<allData.length; i++) {
					total += allData[i];
				}
				var tooltipPercentage = Math.round((tooltipData / total) * 100);
				
				return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
			}
		}
	},
	hover :{
		animationDuration:0
	},
	legend:{
		display:true,
		position : 'right',
		labels:{
			boxWidth:12,
			fontFamily: "'Open Sans Bold', sans-serif",
			fontSize:11
		}
	},
    'onClick' : function (event) {
    	
    	if(this.canvas.id == 'troubleType1Chart'){
    		fnOpenLayerWithGrid("Y", "troubleType1Chart", "");
    		
    	}else if(this.canvas.id == 'troubleType2Chart'){
			fnOpenLayerWithGrid("Y", "troubleType2Chart", "");
			
    	}else if(this.canvas.id == 'preventiveCBM_DECISION'){
    		fnOpenLayerWithGrid("Y", "preventiveCBM_DECISION", "");
    		
    	}else if(this.canvas.id == 'preventivePerformChart'){
			fnOpenLayerWithGrid("Y", "preventivePerformChart", "");
    	}
    }
}

<%-- 정비유형 그래프 작성 --%>
var ctx = document.getElementById("repairTypeChart");
var repairTypeChart = new Chart(ctx, {
		type: 'bar'
		, data: {
			labels : [],
			datasets : [
			    {
					label: "실적건수",
					data: [],
					backgroundColor: ["rgba(242, 99, 95, 1)", "rgba(244, 208, 12, 1)",  "rgba(75, 192, 192, 1)", "rgba(183, 80, 144, 1)"],
					hoverBackgroundColor: ["rgba(242, 99, 95, 0.5)", "rgba(244, 208, 12, 0.5)", "rgba(75, 192, 192, 0.5)", "rgba(183, 80, 144, 0.5)"],
					borderWidth:1
				}
			]
		}
		, options: barOption
	});


<%-- 고장시간 그래프 작성 --%>
var ctx = document.getElementById("troubleTimeChart");
var	troubleTimeChart = new Chart(ctx, {
		type: 'bar'
		, data: {
			labels : [],
			datasets : [
			    {
			    	label: '실적 건수',			
			    	data: [],
			    	backgroundColor: "rgba(0, 147, 209, 1)",
			    	hoverBackgroundColor: "rgba(0, 147, 209, 0.5)"
				}
			]
		}
		, options: barOption
	});

<%-- 고장유형 그래프 작성 --%>
var ctx = document.getElementById("troubleType1Chart");
var troubleType1Chart = new Chart(ctx, {
		type: 'pie'
		, data: {
			labels : [],
			datasets : [
			    {
			    	label: ['실적 건수'],			
			    	data: [],
			    	backgroundColor: [],
			    	hoverBackgroundColor: []
			    }
			]
		},
		options: pieOption
	});

<%-- 조치유형 그래프 작성 --%>
var ctx = document.getElementById("troubleType2Chart");
var troubleType2Chart = new Chart(ctx, {
		type: 'pie'
		, data: {
			labels : [],
			datasets : [
			    {
			    	label: ['실적 건수'],			
			    	data: [],
			    	backgroundColor: [],
			    	hoverBackgroundColor: []
			    }
			]
		},
		options: pieOption
	});
	
<%-- CBM 그래프 작성 --%>
var ctx = document.getElementById("preventiveCBM");
var mycbmChart = new Chart(ctx, {
	    type: 'bar'
	    , data: {
			labels: ["진행중", "점검완료", "미점검"],
			datasets: [
				{
					label: "CBM",							
					backgroundColor: ["rgba(244, 208, 12, 1)", "rgba(0, 147, 209, 1)", "rgba(242, 99, 95, 1)"],
					hoverBackgroundColor: ["rgba(244, 208, 12, 0.5)", "rgba(0, 147, 209, 0.5)", "rgba(242, 99, 95, 0.5)"], 
					borderWidth:1,
					data: []
				}
			]
		}
	    , options : barOption
	});
	
<%-- TBM 그래프 작성 --%>
var ctx = document.getElementById("preventiveTBM");
var mytbmChart = new Chart(ctx, {
	    type: 'bar'
	    , data: {
	    	labels: ["진행중", "점검완료", "미점검"],
			datasets: [
				{
					label: "TBM",							
					backgroundColor: ["rgba(244, 208, 12, 1)", "rgba(0, 147, 209, 1)", "rgba(242, 99, 95, 1)"],
					hoverBackgroundColor: ["rgba(244, 208, 12, 0.5)", "rgba(0, 147, 209, 0.5)", "rgba(242, 99, 95, 0.5)"], 
					borderWidth:1,
					data: []
				}
			]
		}
	    , options : barOption
	});

<%-- CBM 판정 그래프 작성 --%>
var ctx = document.getElementById("preventiveCBM_DECISION");
var pieChart = new Chart(ctx, {
	    type: 'pie'
	    , data: {
			labels: [],
			datasets: [
				{
					backgroundColor: ["rgba(0, 147, 209, 1)", "rgba(244, 208, 12, 1)", "rgba(242, 99, 95, 1)"],
					hoverBackgroundColor: ["rgba(0, 147, 209, 0.5)", "rgba(244, 208, 12, 0.5)", "rgba(242, 99, 95, 0.5)"], 
					borderWidth:1,
					data: []
				}
			]
		}
	    , options : pieOption
	});

<%-- 예방정비 수행현황 그래프 작성 --%>
var ctx = document.getElementById("preventivePerformChart");
var performChart = new Chart(ctx, {
	    type: 'doughnut'
	    , data: {
			labels: [],
			datasets: [
				{
					backgroundColor: ["rgba(0, 147, 209, 1)", "rgba(242, 99, 95, 1)"],
					hoverBackgroundColor: ["rgba(0, 147, 209, 0.5)", "rgba(242, 99, 95, 0.5)"], 
					borderWidth:1,
					data: []
				}
			]
		}
		, options : pieOption
	});
	
<%-- 계획일 초과현황 그래프 --%>
var ctx = document.getElementById("preventiveDayOverChart");
var	dayOverChart = new Chart(ctx, {
		type: 'bar'
		, data: {
			labels : ["정상점검", "1~3일", "4~6일", "1주", "2주 이상", "미점검"],
			datasets : [
			    {
			    	label: '건수',			
			    	data: [],
			    	backgroundColor: "rgba(0, 147, 209, 1)",
			    	hoverBackgroundColor: "rgba(0, 147, 209, 0.5)"
				}
			]
		}
		, options: barOption
	});

$(document).ready(function(){			
	
	$(".boxscroll").niceScroll({cursorwidth:"4px",cursorcolor:"#afbfd6",horizrailenabled:false});
	$("body").on('click', function(){
		$(".boxscroll").getNiceScroll().resize();
	});
	
	<%-- 현재 날짜 설정 --%>
	var date = new Date();
	year = date.getFullYear();
	month_rare = parseInt(date.getMonth()) + 1;
	
	month_rare < 10 ? month = "0"+month_rare : month = month_rare;
	$(".monthMoveInfo").text(year + "년 " + month + "월");
	
	<%-- 정비원 리스트 초기화 --%>
	fnGetWorkerInfo();
	
	<%-- default가 현재 날짜이므로 다음달 선택 불가 --%>
	$('.monthMoveBtn.next').hide();
	
	<%-- 이전 달로 이동 --%>
	$('.monthPrev').on('click', function(){
		<%-- 1월일 경우 년도 차감 --%>
		if(month_rare == 1){
			year = parseInt(year) - 1;
			month_rare = 12
		}else{
			month_rare = parseInt(month_rare) - 1;
		}
		
		month_rare < 10 ? month = "0"+month_rare : month = month_rare;
		
		<%-- 조회한 날짜가 현재일 경우 다음 날짜선택버튼 숨김 --%>
		if(year == date.getFullYear() && month_rare == date.getMonth()+1){
			$('.monthMoveBtn.next').hide();
		} else{
			$('.monthMoveBtn.next').show();
		}
		
		<%-- 정비원 리스트 초기화 --%>
		fnGetWorkerInfo();
		
		<%-- 화면 데이터 입력--%>
		fnSetData();
	});
	
	<%-- 다음 달로 이동 --%>
	$('.monthNext').on('click', function(){
		<%-- 12월일 경우 년도 증가 --%>
		if(month_rare == 12){
			year = parseInt(year) + 1;
			month_rare = 1
		}else{
			month_rare = parseInt(month_rare) + 1;
		}
		
		month_rare < 10 ? month = "0"+month_rare : month = month_rare;
		
		<%-- 조회한 날짜가 현재일 경우 다음 날짜선택버튼 숨김 --%>
		if(year == date.getFullYear() && month_rare == date.getMonth()+1){
			$('.monthMoveBtn.next').hide();
		} else{
			$('.monthMoveBtn.next').show();
		}
		
		<%-- 정비원 리스트 초기화 --%>
		fnGetWorkerInfo();
		
		<%-- 화면 데이터 입력--%>
		fnSetData();
	});
	
	$("#mainWorkerList").on('click','li[name=workerInfo]', function(){
		$(".expand-workStatList .lobipanel-list > li.click").removeClass("click");
		$(this).addClass("click");
		
		MANAGER = $(this).attr("id");

		<%-- 화면 데이터 입력--%>
		fnSetData();
	});
	
	<%-- 공장,파트 값 변경 시 --%>
	$('#LOCATION, #PART').on('change', function(){
		<%-- 정비원 ID 초기화 --%>
		MANAGER = "";
		
		<%-- 정비원 리스트 초기화 --%>
		fnGetWorkerInfo();
		
		<%-- 화면 데이터 입력--%>
		fnSetData();
	});
	
	<%-- 정비 카운트 클릭 시 --%>
	$(".statusDtl").on('click', function(){
		if($(this).find('.statusCont').text() != 0){
			var chart = $(this).attr('id');
			var param = $(this).find('.statusCont').attr('id');
			fnOpenLayerWithGrid("N", chart, param);
		}
	})
});

<%-- 정비원 업무현황 정보--%>
function fnGetWorkerInfo(){
	
	var LOCATION = $("#LOCATION").val();
	var PART = $("#PART").val();
	var REMARKS = $("select[name=LOCATION] option:selected").attr("role");
	
	year = year.toString();
	month = month.toString();
	
	$.ajax({
		type : 'POST',
		url : '/epms/main/wokerInfo.do',
		cache : false,
		dataType : 'json',
		data : { REMARKS : REMARKS
			     , LOCATION : LOCATION
			     , PART : PART
			     , PARAM_DATE : year+month},
		async : false,
		error : function() {
			alert("처리중 오류가 발생 하였습니다.");
		},
		success : function(json) {
			$(".lobipanel-list").empty();

			var _ul = $("#mainWorkerList").find("ul.lobipanel-list");
			$.each(json.wokerInfo, function(i, o) {
				var _li = $("#workerInfoTemplate").html();
				_li = $(_li);
				for( var name in o ) {
	            	switch (name) {
	            		case "USER_ID" :
	            			if(MANAGER != ""){
	            				_li.attr("id", o["USER_ID"]);
	            				_li.addClass("click");
	            			}else{
	            				_li.attr("id", o["USER_ID"]);
	            			}
	            			break;
	            		case "USER_NAME" :
	            			_li.find("[name=user]").text(o["USER_NAME"] + "\t" + o["JOB_GRADE_NAME"]);
	            			break;
	            		case "PART_NAME" :
	            			_li.find("[name=dep]").text(o["PART_NAME"] + "파트 \t" + o["DIVISION_NAME"]);
	            			break;
	            		case "MEMBER_FILEURL" :
	            			_li.find("[name=MEMBER_FILEURL]").attr("src", o[name]);
	            			break;
		 				default:
		 					_li.find("[name=" + name + "]").text(o[name]);
		 				break;
	 				}
				}
				_ul.append(_li);
			});
			
		}		
	});
}

<%-- 화면 내 대시보드/차트 데이터 세팅 --%>
function fnSetData(){
	
	month_rare < 10 ? month = "0"+month_rare : month = month_rare;
	
	$(".monthMoveInfo").text(year + "년 " + month + "월");
	
	if(MANAGER != "" && MANAGER != null){
		var yearMonth = year+""+month;
		var location = $("#LOCATION").val();
		var part = $("#PART").val();
		var remarks = $("select[name=LOCATION] option:selected").attr("role");
		
		fnGetRepairResultInfo(yearMonth, location, part, remarks);

// 		fnGetPreventiveInfo(yearMonth, location, part, remarks);
	}
}

<%-- 검색 일자별 정비현황 대시보드 --%>
function fnGetRepairResultInfo(date, location, part, remarks){
	$.ajax({
		type : 'POST',
		url : '/epms/main/selectRepairResultInfo.do',
		cache : false,
		dataType : 'json',
		data : { DATE_PLAN : date
				 , REMARKS  : remarks 
				 , LOCATION : location
				 , PART 	: part
				 , MANAGER  : MANAGER },
		async : false,
		error : function() {
			alert("처리중 오류가 발생 하였습니다.");
		},
		success : function(json) {
			var repairInfo = json.repairInfo;
			var _ul = $("#repair_dashboardInfo");
			
			for( var name in repairInfo ) {
            	switch (name) {
	 				default:
	 					_ul.find("[name=" + name + "]").text(repairInfo[name]);
	 				break;
 				}
			}
			
			<%-- 정비 차트 생성 --%>
			drawRepairChart(json);
		}		
	});
}

<%-- 검색 일자별 예방보전 현황 대시보드 --%>
function fnGetPreventiveInfo(date, location, part, remarks){
	$.ajax({
		type : 'POST',
		url : '/epms/main/mainPreventiveListDtlAjax.do',
		cache : false,
		dataType : 'json',
		data : { PARAM_DATE 		: date
				 , REMARKS 			: remarks
				 , PARAM_LOCATION 	: location
				 , PARAM_PART 		: part == "all" ? "" : part
				 , MANAGER			: MANAGER },
		async : false,
		error : function() {
			alert("처리중 오류가 발생 하였습니다.");
		},
		success : function(json) {
			var preventiveInfo = json.preventiveInfo;
			
			var _ul = $("#preventive_dashboardInfo");
			
			for( var name in preventiveInfo ) {
            	switch (name) {
	 				default:
	 					_ul.find("[name=" + name + "]").text(preventiveInfo[name]);
	 				break;
 				}
			}
			
			<%-- 예방보전 차트 생성 --%>
			drawPreventiveChart(json);
			
		}		
	});
}

<%-- 랜덤 색상 생성 --%>
var dynamicColors = function(){
	var r = Math.floor(Math.random()* (255-200+1))+200;
	var g = Math.floor(Math.random()* (255-200+1))+200;
	var b = Math.floor(Math.random()* (255-200+1))+200;

	return ["rgba("+r+","+g+","+b+", 1)", "rgba("+r+","+g+","+b+", 0.5)"];
}

<%-- 정비관련 차트 데이터 생성 --%>
function drawRepairChart(json) {
	
	<%-- 배열 초기화 --%>
	repairTypeID = [];  
	troubleTimeID = []; 
	troubleType1ID = [];
	troubleType2ID = [];
	
	<%-- 정비유형 --%>
	var repairTypeData = [], repairTypeLabel = [];
	
	$.each(json.REPAIR_TYPE, function(idx, val){
		repairTypeData.push(val.REPAIR_TYPE_CNT);
		repairTypeLabel.push(val.REPAIR_TYPE_NAME);
		
		repairTypeID.push(val.REPAIR_TYPE);
	});
	
	repairTypeChart.data.labels = repairTypeLabel;
	repairTypeChart.data.datasets[0].data = repairTypeData;
	repairTypeChart.update();
	
	<%-- 고장시간 --%>
	var troubleTimeData = [json.TROUBLE_TIME.MIN30, json.TROUBLE_TIME.MIN60, json.TROUBLE_TIME.MIN120, json.TROUBLE_TIME.MIN240, json.TROUBLE_TIME.MIN480, json.TROUBLE_TIME.MINOVER];
	var troubleTimeLabel = ['30분미만', '30분~1시간', '1~2시간', '2~4시간', '4~8시간', '8시간 초과'];
	
	$.each(troubleTimeLabel, function(idx, val){
		troubleTimeID.push(idx);
	});
	
	troubleTimeChart.data.labels = troubleTimeLabel;
	troubleTimeChart.data.datasets[0].data = troubleTimeData;
	troubleTimeChart.update();
	
	<%-- 고장유헝 --%>
	var troubleType1Data = [], troubleType1Label = [], troubleType1Color = [], troubleType1Hover = [];
	
	$.each(json.TROUBLE_TYPE1, function(idx, val){
		troubleType1Label.push(val.TROUBLE_TYPE1_NAME);
		troubleType1Data.push(val.TROUBLE_TYPE1_COUNT);
		
		var tempColor = dynamicColors();
		troubleType1Color.push(tempColor[0]);
		troubleType1Hover.push(tempColor[1]);
		
		troubleType1ID.push(val.TROUBLE_TYPE1);
	});
	
	troubleType1Chart.data.labels = troubleType1Label;
	troubleType1Chart.data.datasets[0].data = troubleType1Data;
	troubleType1Chart.data.datasets[0].backgroundColor = troubleType1Color;
	troubleType1Chart.data.datasets[0].hoverBackgroundColor = troubleType1Hover;
	troubleType1Chart.update();
	
	<%-- 조치유형 --%>
	var troubleType2Data = [], troubleType2Label = [], troubleType2Color = [], troubleType2Hover = [];
	
	$.each(json.TROUBLE_TYPE2, function(idx, val){
		troubleType2Label.push(val.TROUBLE_TYPE2_NAME);
		troubleType2Data.push(val.TROUBLE_TYPE2_COUNT);
		
		var tempColor = dynamicColors();
		troubleType2Color.push(tempColor[0]);
		troubleType2Hover.push(tempColor[1]);
		
		troubleType2ID.push(val.TROUBLE_TYPE2);
	});
	
	troubleType2Chart.data.labels = troubleType2Label;
	troubleType2Chart.data.datasets[0].data = troubleType2Data;
	troubleType2Chart.data.datasets[0].backgroundColor = troubleType2Color;
	troubleType2Chart.data.datasets[0].hoverBackgroundColor = troubleType2Hover;
	troubleType2Chart.update();
	
}

<%-- 예방보전 차트 데이터 생성 --%>
function drawPreventiveChart(json){
	
	<%-- 배열 초기화 --%>
	tbmDataID = [];
	cbmDataID = [];
	cbmDecisionDataID = [];
	performDataID = [];
	mycbmChart.data.datasets[0].data = [];
	mytbmChart.data.datasets[0].data = [];
	pieChart.data.datasets[0].data = [];
	performChart.data.datasets[0].data = [];
	
	<%-- CBM차트 --%>
	$.each(json.cbmChartInfo, function(i, obj) {
		tbmDataID.push(obj.CHECK_TYPE);
		mycbmChart.data.datasets[0].data.push(obj.CHECK_TYPE_CNT);
	});
	mycbmChart.update();
	
	<%-- TBM차트 --%>
	$.each(json.tbmChartInfo, function(i, obj) {
		cbmDataID.push(obj.CHECK_TYPE);
		mytbmChart.data.datasets[0].data.push(obj.CHECK_TYPE_CNT);
	});
	mytbmChart.update();
	
	var cbmDecisionData = [], cbmDecisionLabel = [], cbmDecisionCnt = 0;
	
	<%-- CBM판정 --%>
	$.each(json.cbmDecisionChartInfo, function(i, obj) {
		cbmDecisionLabel.push(obj.CHECK_DECISION_NAME);
		cbmDecisionDataID.push(obj.CHECK_DECISION);
		cbmDecisionData.push(obj.DECISION_CNT);
		if(obj.DECISION_CNT == '0') cbmDecisionCnt = cbmDecisionCnt + 1;
	});
	if(cbmDecisionCnt != 3) {
		pieChart.data.labels = cbmDecisionLabel;
		pieChart.data.datasets[0].data = cbmDecisionData;
	} else {
		pieChart.data.labels = '';
		pieChart.data.datasets[0].data = '';
	}
	pieChart.update();
	
	var performData = [], performLabel = [], performCnt = 0;
	
	<%-- 예방정비 수행현황 --%>
	$.each(json.performChartInfo, function(i, obj) {
		performDataID.push(obj.STATUS);
		performLabel.push(obj.STATUS_NM);
		performData.push(obj.STATUS_CNT);
		if(obj.STATUS_CNT == '0') performCnt = performCnt + 1;
	});
	if(performCnt != 2) {
		performChart.data.labels = performLabel;
		performChart.data.datasets[0].data = performData;
	} else {
		performChart.data.labels = '';
		performChart.data.datasets[0].data = '';
	}
	performChart.update();
	
	<%-- 계획일 초과현황 --%>
	dayOverChart.data.datasets[0].data = [json.dayOverChartInfo.DAY1
										, json.dayOverChartInfo.DAY3
										, json.dayOverChartInfo.DAY6
										, json.dayOverChartInfo.DAY13
										, json.dayOverChartInfo.DAYOVER
										, json.dayOverChartInfo.PREVENTIVE_UNCHECK_CNT]
	dayOverChart.update();
}

<%-- 대시보드/차트 클릭 시 상세 실적 리스트 --%>
function fnOpenLayerWithGrid(chartYn, chart, value){

	var PARAM_DATE = year +""+ month;
	var PART = $("#PART").val() == "all" ? "" : $("#PART").val();
	var STATUS = "";
	var flag = "";		<%-- Y일경우 차트데이터, N일경우 카운트 데이터 --%>
	var sort = "";		<%-- 정비실적/예방보전 구분 --%>
	var title = "";
	var tabsID = "";
	
	//tabsID = ($('.tabs').find('.on').attr("href"));
	tabsID = $('.expandedTabs').find('.active').attr("onclick").substr(16,8);

	if(chartYn == "Y"){
		title = $("#" + chart).prev().text();
		
		if(chart == "repairTypeChart"){
			var activePoints = repairTypeChart.getElementsAtEvent(event);
		    var firstPoint = activePoints[0];
		    STATUS = repairTypeID[firstPoint._index];
		    
		}else if(chart == "troubleTimeChart"){
			var activePoints = troubleTimeChart.getElementsAtEvent(event);
		    var firstPoint = activePoints[0];
		    STATUS = troubleTimeID[firstPoint._index];
		    
		}else if(chart == "troubleType1Chart"){
			var activePoints = troubleType1Chart.getElementsAtEvent(event);
		    var firstPoint = activePoints[0];
		    STATUS = troubleType1ID[firstPoint._index];
		    
		}else if(chart == "troubleType2Chart"){
			var activePoints = troubleType2Chart.getElementsAtEvent(event);
		    var firstPoint = activePoints[0];
		    STATUS = troubleType2ID[firstPoint._index];
		    
		}else if(chart == 'preventiveTBM'){
			CHECK_TYPE = "02";
			var activePoints = mytbmChart.getElementsAtEvent(event);
	        var firstPoint = activePoints[0];
	        STATUS = tbmDataID[firstPoint._index];
	        STATUS == "03" ? STATUS = "(02, 03)" : STATUS;
   	 	}else if(chart == 'preventiveCBM'){
			CHECK_TYPE = "01";
			var activePoints = mycbmChart.getElementsAtEvent(event);
	        var firstPoint = activePoints[0];
	        STATUS = cbmDataID[firstPoint._index];
	        STATUS == "03" ? STATUS = "(02, 03)" : STATUS;
   	 	}else if(chart == 'preventiveDayOverChart'){
			var activePoints = dayOverChart.getElementsAtEvent(event);
	        var firstPoint = activePoints[0];
	        if(firstPoint._index == 5) {
	        	STATUS = "09";
	        	flag = "99";
	        } else {
	        	STATUS = "";
		        flag = firstPoint._index;
	        }
	        STATUS == "03" ? STATUS = "(02, 03)" : STATUS;
   	 	}else if(chart == 'preventiveCBM_DECISION'){
	   	 	var activePoints = pieChart.getElementsAtEvent(event);
	        var firstPoint = activePoints[0];
			CHECK_DECISION = cbmDecisionDataID[firstPoint._index];
	        
   	 	}else if(chart == 'preventivePerformChart'){
			var activePoints = performChart.getElementsAtEvent(event);
	        var firstPoint = activePoints[0];
			CHECK_DECISION = "03";
			STATUS = performDataID[firstPoint._index];
   	 	}
		
	} else {
		title = $("#" + chart).find('.statusTit').text();		
		STATUS = value;
	}
	
	if(tabsID == "panel1-1"){
		$('#popMainRepairResultList_title').text("정비현황(" + title + ")");
		
		Grids.PopMainRepairResultList.Source.Data.Url = "/epms/main/popRepairResultListOptData.do?&STATUS="+STATUS
													  + "&DATE_PLAN="+PARAM_DATE
													  + "&PART="+PART
													  + "&LOCATION="+$("#LOCATION").val()
													  + "&flag="+chartYn
													  + "&CHART="+chart
													  + "&MANAGER="+MANAGER;
		Grids.PopMainRepairResultList.Reload();
		fnModalToggle('popMainRepairResultList');
		
	} else {
		$('#popMainPreventivePlanList_title').text("예방보전 계획현황(" + title + ")");
		flag == undefined ? flag = "" : flag = flag;
		
		if(value=="PREVENTIVE_ONGOING" || STATUS=="01"){
			Grids.PopMainPreventivePlanList.Source.Layout.Url = "/epms/main/popMainPreventivePlanListLayout.do?LOCATION=${sessionScope.ssLocation}";
		}else {
			Grids.PopMainPreventivePlanList.Source.Layout.Url = "/epms/main/popMainPreventiveResultListLayout.do?LOCATION=${sessionScope.ssLocation}";
		}
		
		Grids.PopMainPreventivePlanList.Source.Data.Url = "/epms/main/popMainPreventivePlanListData.do?LOCATION=" + $("#LOCATION").val() 
														+ "&startDt=" + PARAM_DATE 
														+ "&PART=" + PART 
														+ "&CHECK_TYPE=" + CHECK_TYPE 
														+ "&CHECK_DECISION=" + CHECK_DECISION 
														+ "&PREVENTIVE_STATUS=" + STATUS 
														+ "&flag=" + flag
														+ "&MANAGER="+MANAGER;
		Grids.PopMainPreventivePlanList.Reload();
		fnModalToggle('popMainPreventivePlanList');
		
		CHECK_TYPE = "";
		CHECK_DECISION = "";
		flag = "";
	}
   
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}
</script>
<script type="text/template" id="workerInfoTemplate">
<li name="workerInfo" id="">
	<div class="userInfo f-l">
		<div class="user_img"><img src="" name="MEMBER_FILEURL" alt="" onerror="this.src='/images/com/web_v2/userIcon.png'"/></div>
		<div class="user_name">
			<span class="name" name="user"></span>
			<span class="dep" name="dep"></span>
		</div>
	</div>
	<div class="workStatus f-r">
		<ul>
			<li>
				<span class="workStatusTit">정비</span>
				<span class="workStatusCon type01" title="" name="REPAIR_ONGOING_CNT"></span>
				<span class="workStatusCon type02" title="" name="REPAIR_EXCESS_CNT"></span>
			</li>
		</ul>
	</div>
</li>
</script>
</html>