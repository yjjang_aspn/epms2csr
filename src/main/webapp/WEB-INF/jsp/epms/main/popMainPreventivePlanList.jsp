<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>

<%--
	Class Name	: popMainPreventivePlanList.jsp
	Description : 메인 대시 보드 - 예방보전 현황 레이아웃
    author		: 김영환
    since		: 2018.06.19
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2018.06.19	 	김영환		최초 생성

--%>

<!-- 고장관리 : 정비요청결재 -->
<script type="text/javascript">

$(document).ready(function(){
	
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function (e) {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>	
	});	
});

<%-- Validation --%>
function dataValidation(gridNm){
	var cnt = 0;
	var chkCnt = 0;
	var grid = Grids[gridNm];
	
	if(gridNm == "PopMainPreventivePlanList"){
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){
			if(row.Changed || row.Added){
				
				cnt++;
				
				<%-- 점검계획일이 만료일 이전인지 체크 --%>
				var date_assign = Grids[gridNm].GetValue(row, "DATE_ASSIGN");		<%-- 점검계획일 --%>
				var date_deadline = Grids[gridNm].GetValue(row, "DATE_DEADLINE");	<%-- 만료일 --%>
				
				if(date_assign > date_deadline){
					chkCnt++;
					Grids[gridNm].SetAttribute(row,"DATE_ASSIGN","Color","#FF6969",1);
				}else{
					Grids[gridNm].SetAttribute(row,"DATE_ASSIGN","Color","#FFFFAA",1);
				}
				
			}
		}
	}	
	
	<%-- 추가 및 변경된 데이터가 없을 경우 --%>
	if(cnt == 0){
		return false;
	}
	
	<%-- 점검계획일 체크 --%>
	if(chkCnt == 0){
		return true;
	}
	else{
		alert("점검계획일은 만료일 이전으로 수정 가능합니다.");
		return false;
	}	
}

<%-- 저장 버튼 --%>
function fnSave(gridNm){
	
	<%-- 유효성 체크 --%>
	if(!dataValidation(gridNm)){
		return;
	}
    
    if(Grids[gridNm].ActionValidate()){
    	
		if(gridNm == "PopMainPreventivePlanList"){
			if(confirm("저장하시겠습니까?")){
				Grids[gridNm].ActionSave();
			}
		}
		
    }
    
}

<%-- 배정일 일괄 변경 --%>
function batchDateAssign(grid,row,col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return
	}
	var changeDate = row.DATE_ASSIGN;
	
	var selRows = Grids.PopMainPreventivePlanList.GetSelRows();
	
	for(var i=0; i <selRows.length;i++){
		Grids["PopMainPreventivePlanList"].SetValue(selRows[i], "DATE_ASSIGN", changeDate, 1);
	}
}

<%-- 담당자 일괄 변경 --%>
function batchManager(grid,row,col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return 
	}
	var changePerson = row.MANAGER;
	
	var selRows = Grids.PopMainPreventivePlanList.GetSelRows();
	
	for(var i=0; i <selRows.length;i++){
		Grids["PopMainPreventivePlanList"].SetValue(selRows[i], "MANAGER", changePerson, 1);
	}
}

<%-- 그리드 데이터 저장 후 --%> 
Grids.OnAfterSave  = function (grid){
	Grids.PopMainPreventivePlanList.ReloadBody();	
};

</script>

<div class="modal fade modalFocus" id="popMainPreventivePlanList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-80"> <!-- 원하는 width 값 입력 -->				
		 <div class="modal-content hgt-per-80" style="min-height:540px;">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="popMainPreventivePlanList_title">예방보전 계획현황</h4>
			</div>
			<div class="modal-body" >
	           	<div class="modal-bodyIn" style="height:calc(100% - 30px)">
						
					<!-- 트리그리드 : s -->
					<div id="preventivePlanList" class="hgt-per-100">
						<bdo	Debug="Error"
										Layout_Url="/epms/main/popMainPreventivePlanListLayout.do?PART=${sessionScope.ssPart}&LOCATION=${sessionScope.ssLocation}"
										Upload_Url="/epms/preventive/plan/preventivePlanListEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols" 
										Export_Data="data" Export_Type="xls"
										Export_Url="/sys/comm/exportGridData.jsp?File=PreventivePlanList.xls&dataName=data"
										>
								</bdo>
					</div>
					<!-- 트리그리드 : e -->

				</div>
			</div>
		</div>
	</div>
</div>
						
				