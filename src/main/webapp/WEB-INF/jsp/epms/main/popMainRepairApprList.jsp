<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>

<%--
	Class Name	: popMainRepairApprList.jsp
	Description : 대시보드-정비요청결제
    author		: 김영환
    since		: 2018.06.18
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2018.06.18	 	김영환		최초 생성

--%>

<!-- 고장관리 : 정비요청결재 -->
<script type="text/javascript">

var msg = "";
var focusedRow = null;

$(document).ready(function(){
		 
	<%-- 파일 다운로드  --%>
	$('#fileNotNull').on('click','.btnFileDwn', function(){
		var ATTACH = $(this).data("attach");
		var ATTACH_GRP_NO = $('#ATTACH_GRP_NO').val();
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO='+ATTACH_GRP_NO+'&ATTACH='+ATTACH;
		$("#fileDownFrame").attr("src",src);
	});
	
});

<%-- row 값 계산 --%>
Grids.OnReady = function(grid){
	  var rowCnt = 0;
	  var rows = grid.Rows;
	  for(var key in rows){
	    if(key == "Header" || key == "NoData" || key == "Toolbar"){
	      continue;
	    } 
	    rowCnt ++;
	  }
	  gridDivResize(rowCnt);
}

<%-- 그리드 영역 사이즈 resizing --%>
function gridDivResize(dataSize){

	if(dataSize <= 1){
	  dataSize = 4;
	}
	
	var divSize = dataSize * 10;  
	
	divSize += 160;
	
	if(divSize < 160){
	  divSize = 160;
	}
	
	$(".gridDivResize").height(divSize);
	$('.gridDivResize').css('max-height','500px')
}

<%-- 새로고침 --%>
function fnReload(){
	Grids.RepairApprList.Source.Data.Url = "/epms/repair/appr/repairApprListData.do?startDt="+ $('#startDt').val() 
			                             + "&endDt=" + $('#endDt').val();
	Grids.RepairApprList.ReloadBody();
	alert(msg);
}

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source,data, func){
	if(grid.id == "RepairApprList"){
		if(data != ""){
			callLoadingBar();
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "RepairApprList"){
					setTimeout($.unblockUI, 100);
				}
			}
		}
	}
}

</script>

<div class="modal fade modalFocus" id="popMainRepairApprList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-90"> <!-- 원하는 width 값 입력 -->				
		 <div class="modal-content hgt-per-80" style="min-height:540px;">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="popMainRepairApprList_title">정비 요청목록</h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn" style="height:calc(100% - 30px)">
						
					<!-- 트리그리드 : s -->
					<div id="PopMainRepairApprList" class="hgt-per-100">
						<bdo	Debug="Error"
								Layout_Url="/epms/main/popMainRepairApprListLayout.do"
								Export_Data="data" Export_Type="xls"
								Export_Url="/sys/comm/exportGridData.jsp?File=결재현황 목록.xls&dataName=data"
						>
						</bdo>
					</div>
					<!-- 트리그리드 : e -->

				</div>
			</div>
		</div>
	</div>
</div>
	