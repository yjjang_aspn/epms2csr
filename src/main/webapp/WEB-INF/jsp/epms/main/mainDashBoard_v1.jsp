<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%--
  Class Name : mainDashBoard.jsp
  Description : eacc 메인대시보드 화면
  Modification Information
 
      수정일         수정자                   수정내용
   -------    --------    ---------------------------
   2017.07.31    정순주              최초 생성

    author   : 정순주
    since    : 2017.07.31
 --%>
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>메인대시보드</title>
	
	<%@ include file="/com/comHeader.jsp"%>
	<link rel="stylesheet" href="/css/eacc/modalPopup.css">
	<script src="/js/com/web/modalPopup.js"></script>
	<script src="/js/jquery/jquery-filestyle.js" type="text/javascript" ></script>
	<script src="/js/jquery/jquery.MultiFile.js" type="text/javascript"></script>
	<script src="/js/jquery/jquery.ajaxsubmit.js" type="text/javascript"></script>
	<script src="/js/com/web/mainCommon.js" type="text/javascript"></script>
	
	<script>
		var maxFileCnt = 1;
		$(window).on('load', function(){
			$('.pwChgArea').hide(); /* 비밀번호 변경 관련 테이블 */
			$('#infoSaveBtn').hide(); /* 저장 버튼 */
		});
		
		$(document).ready(function() {
			
			/* 첨부파일 */
			for(var i=0; i<1; i++) {
				$('#attachFile'+i).append('<input type="file" multiple id="ex_file'+i+'" name="fileList" class="ksUpload with-preview">'); //class에 multi with-preview를 설정 해줘야 업로드전 이미지 미리보기 기능 활설화
				
				$('#ex_file'+i).MultiFile({
					accept : 'gif |jpg |png |jpeg'
		           ,list   : '#detailFileList'+i
		           ,max    : maxFileCnt
		           ,STRING : {
						denied    : "$ext 는(은) 업로드 할수 없는 파일확장자입니다."
						,toomany   : "업로드할 수 있는 파일의 개수는 최대 $max개 입니다."
						,duplicate : "$file 은 이미 선택된 파일입니다."
						,remove    : '<img src="/images/com/web/btn_remove.png">'
		                       		+ '<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'"/>'
			            ,text      : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" value="'+i+'"/>'
		              },
				});
				
				$('.MultiFile-wrap').attr("id", "jquery"+i);
			}
			
			//첨부파일 삭제
			$('.icn_fileDel').on('click', function() {
				delModiFile($(this));
			});
		
			//이미지 파일 다운로드
			$('.btnFileDwn').on('click', function() {
				var ATTACH        = $(this).data("attach");
				var ATTACH_GRP_NO = $(this).data("attach_grp_no");
				location.href = '/attach/fileDownload.do?ATTACH_GRP_NO=' + ATTACH_GRP_NO + '&ATTACH=' + ATTACH;
			});
			
			/* 비밀번호 변경 버튼 */
			$('#passwordChgBtn').on('click', function(){
				var pwCtl = $('#passwordChgBtn').text();
				if(pwCtl == "비밀번호 변경"){
					$('#passwordChgBtn').text("비밀번호 변경 취소"); /* 버튼 변경 */
					$('.pwChgArea').show(); /* 비밀번호 변경 관련 테이블 */
					$("#infoSaveBtn").show(); /* 저장 버튼 */
					$("#infoCloseBtn").hide(); /* 닫기 버튼 */
				}else{
					$('#passwordChgBtn').text("비밀번호 변경");  /* 버튼 변경 */
					$('.pwChgArea').hide(); /* 비밀번호 변경 관련 테이블 */
					$('#newPw').val(""); /* 비밀번호 변경 관련 테이블 */
					$('#checkPw').val(""); /* 비밀번호 변경 관련 테이블 */
					$("#infoSaveBtn").hide(); /* 저장 버튼 */
					$('#infoCloseBtn').show(); /* 닫기 버튼 */
				}
			});
			
			/* 직인 수정 버튼 */
			$('#changeSealBtn').on('click', function(){
				$("#infoSaveBtn").show(); /* 저장 버튼 */
				$("#infoCloseBtn").hide(); /* 닫기 버튼 */
				$(".sealImageEdit").show(); /* 직인 수정 */
				$(".sealImageView").remove(); /* 직인 확인 */
			});
			
			/* 취소 버튼 */
			$('#cancelBtn').on('click', function(){
				fn_reloadPage();
			});
			
			/* 닫기 버튼 */
			$('#closeBtn').on('click', function(){
				window.close();
			});
			
			/* 저장 버튼 */
			$('#saveBtn').on('click', function(){
				var returnValue = "T"; /* Return 값 */
				
				/* 직인 첨부파일 */
				var fileCnt = $(".MultiFile-label").length-1;
				if(fileCnt > 0){
					if(fileCnt > maxFileCnt){
						alert("업로드할 수 있는 파일의 개수는 최대 " + maxFileCnt + "개 입니다.");
						returnValue = "F";
						return;
					}else{
						$('#sealAttachExistYn').val("Y");
					}
				} else {
					$('#sealAttachExistYn').val("N");
				}
				
				if($('#passwordChgBtn').text() == "비밀번호 변경 취소"){
					var newPw = $('#newPw').val(); /* 신규 비밀번호 */
					var checkPw = $('#checkPw').val(); /* 비밀번호 확인 */
					
					if(newPw == "" || checkPw == "") {
						alert("비밀번호를 입력해주세요.");
						returnValue = "F";
						return;
					}
					
					if(newPw != checkPw){
						alert("비밀번호를 다시 확인해주세요.");
						returnValue = "F";
						return;
					}
				}
				
				if(returnValue == "T"){
					if(confirm("수정된 계정 정보를 저장하시겠습니까?")){
						/* 계정 정보 수정 */
						$('#userInfoForm').attr('action', '/mem/member/editProfileAjax.do').ajaxSubmit(function(json){
							if(json.resultCd == "T"){
								alert("계정 정보가 수정되었습니다.");
								fn_reloadPage();
							} else {
								alert("계정 정보 수정에 실패하였습니다.")
								fn_reloadPage();
							}
						});
					}
				}
			});
			
			/* 이미지 버튼 */
			$('#imgChgBtn').on('click', function(){
				var attachGrpNo = "${memberInfo.ATTACH_GRP_NO}";
				var userId = "${memberInfo.USER_ID}";
				fn_openPop("/attach/fileUserUdateForm.do", "?MODULE=9&maxFileCnt=1&ATTACH=1&ATTACH_GRP_NO="+attachGrpNo+"&userId="+userId, 300, 300);
			});
			
		});
		
		/* 페이지 새로고침 */
		function fn_reloadPage(){
			window.location.reload(true);
		}
		
		/* 증빙 항목에서 제거 처리 */
		function delModiFile(obj){
			var attach = obj.data("attach");
			var idx = obj.data('idx');
			
			$('#fileLstWrap_'+attach).remove();
			
			//fileNo set
			if(fileVal != '') {
				fileVal += ",";
			}
			
			fileVal += attach;
			$('#DEL_IMG_SEQ').val(fileVal);
		}
		
		/* 내가 올린 결재 모달 팝업 오픈 */
		function fn_openApprovalModal(APPR_NDX){
			var url = "/hello/eacc/approval/myApprovalDtl.do?APPR_NDX="+APPR_NDX;
			url += "&"+new Date().getTime();
			
			$('#myApprovalDtlModal').load(url, function(responseTxt, statusTxt, xhr){
				$(this).modal({});
				var contH = $("#container").height();
				$('.modal-bodyIn').height(contH-160);
			});
		}
		
	</script>
	
	<style>
		.thumbnail{height      : 100px;
			       line-height : 70px;
				   margin-top  : 10px;
			       text-align  : center;
			       width       : 120px;
		} 
		#container {overflow-y:auto !important; background:#e4ebf3;}
	</style>
</head>

<body>
	<div id="wrapper">
		
		<div id="main" style="top: 0px;">
			<section id="section">
				<div id="container" class="">
					<div class="">
						
							<!-- 페이지 내용 : s -->
							
							<div class="eacc-dsbd-wrap">
								<div class="eacc-dsbd-wrap-in">
									
									<!-- 메인비주얼 / 내카드정보 / 미작성내역 -->
									<div class="eacc-dsbd-top">
									
										<!-- S : 메인비주얼 -->
										<div class="eacc-box-wrap wd33">
											<div class="eacc-visual">
												<div class="visual-img">
													<img src="/images/com/new_eAcc/mainvisual.png" alt="헬로컴퍼니 이어카운팅" />
												</div>
												<div class="visual-info">
													<strong>ASPN <br> EXPENSE</strong>
													<span class="ir-pm borderSt">border style</span>
													<span class="mainUserInfo">
														<span class="mainUserImg">
															<c:choose>
																<c:when test="${memberInfo.ATTACH_GRP_NO eq null or memberInfo.ATTACH_GRP_NO eq ''}">
																</c:when>
																<c:otherwise>
																	<img class="comtUsrBtn" src="/attach/thumbFileDownload.do?ATTACH_GRP_NO=${memberInfo.ATTACH_GRP_NO}&ATTACH=1" alt="user image" />
																</c:otherwise>
															</c:choose>
														</span>
														<span class="name">${memberInfo.NAME}</span>
														<span class="dep">${memberInfo.DIVISION_NM} ${memberInfo.JOB_GRADE}</span>
													</span>
													<a href="#none" class="eacc-btn st02" id="profileChange">내 정보 변경</a>
												</div>
											</div>
										</div>
										<!-- E : 메인비주얼 -->
										
										<!-- S : 내 카드 정보 -->
										<div class="eacc-box-wrap wd33">
											<div class="eacc-card">
												<div class="eacc-card-tit">
													<h3>My Account</h3>
													<div class="cardDropdown">
														<span>내 카드 목록</span>
														<a href="/hello/eacc/cardBill/cardList.do" class="eacc-btn st01 cardDropbtn">보유카드 총 <span>${fn:length(cardList)}</span>개</a>
														<div class="cardDropdown-content">
														<c:forEach var="item" items="${cardList}">
															<a href="#">${item.BANK_NAME}&nbsp;&nbsp;|&nbsp;&nbsp;${fn:substring(item.CARD_NUM,0,4)}-${fn:substring(item.CARD_NUM,4,8)}-${fn:substring(item.CARD_NUM,8,12)}-${fn:substring(item.CARD_NUM,12,16)}</a>
														</c:forEach>
														</div>
													</div>
												</div>
												<div class="card-slider-wrap">
													<div class="card-view">
														<ul class="card-slider">
															<c:forEach var="item" items="${cardList}">
																<li>
																	<div class="card">
																		<div class="card-tarjeta st01">
																			<div class="card-bank">${item.BANK_NAME}</div>
																			<div class="card-numers">
																				<h4 class="numbers-tarje">${fn:substring(item.CARD_NUM,0,4)}</h4>
																				<h4 class="numbers-tarje">${fn:substring(item.CARD_NUM,4,8)}</h4>
																				<h4 class="numbers-tarje">${fn:substring(item.CARD_NUM,8,12)}</h4>
																				<h4 class="numbers-tarje">${fn:substring(item.CARD_NUM,12,16)}</h4>
																			</div>
																			<div class="card-nom-tarjet">
																				<span>CARD HOLDER</span>  
																				<h4>${item.USERNAME}</h4>
																			</div>
																			<div class="card-fech-exp-tarjet" >
																				<span>EXPIRATION DATE</span>  
																				<h4 style="margin-top: 18px;">${fn:substring(item.AVAIL_TERM,4,6)}&nbsp;/&nbsp;${fn:substring(item.CARD_NUM,2,4)}</h4>
																			</div>
																		</div>
																		<div class="card-limit">
																			<div>
																				<p>카드 총한도</p>
																				<p><fmt:formatNumber value="${item.USE_ONELMT}" pattern="#,###.##"/><span>원</span></p>
																			</div>
																			<div>
																				<p>잔여한도</p>
																				<p><fmt:formatNumber value="${item.ONE_LIMIT}" pattern="#,###.##"/><span>원</span></p>
																			</div>
																		</div>
																	</div>
																</li>
															</c:forEach>
														</ul>
														<div class="card-controls">
															<ul class="card-cont-list">
																<!-- 보여주는 개수 최대 5개로 한정 -->
																<c:forEach var="item" items="${cardList}" varStatus="status">
																	<c:choose>
																		<c:when test="${status.index eq 0 }">
																			<li class="active"><span></span></li>
																		</c:when>
																		<c:when test="${status.index ne 0 and status.index < 5 }">
																			<li><span></span></li>
																		</c:when>
																		<c:otherwise>
																		</c:otherwise>
																	</c:choose>
																</c:forEach>
															</ul>
														</div>
													</div>														
												</div>
											</div>
										</div>
										<!-- E : 내 카드 정보 -->
										
										<!-- S : 미작성내역 / 퀵메뉴 -->
										<div class="top-right-area wd32">
											<div class="eacc-box-wrap">
												<ul class="eacc-nowrite-wrap">
													<li class="nowrite-list01">
														<a href="/hello/eacc/cardBill/cardMasterList.do">
															<span>법인카드 미작성내역</span>
															<c:if test="${countOverElapseCard ne '0'}">
																<span class="alertDate" >
																	<span class="alertDateBtn" >
																		<img src="/images/com/new_eAcc/warningIcon.png" alt="초과일수">
																	</span>
																	<span class="alertDate-content">
																		<span>총 ${countOverElapseCard }건 경과일 초과</span>
																	</span>
																</span>
															</c:if>
															<span>${countUnpostingCard }<span>건</span></span>
														</a>
													</li>
													<li class="nowrite-list02">
														<a href="/hello/eacc/taxBill/taxMasterList.do">
														<span>세금계산서 미작성내역</span>
														<c:if test="${countUnpostingTax ne '0'}">
															<span class="alertDate" >
																<span class="alertDateBtn" >
																	<img src="/images/com/new_eAcc/warningIcon.png" alt="초과일수">
																</span>
																<span class="alertDate-content">
																	<span>총 ${countOverElapseTax }건 경과일 초과</span>
																</span>
															</span>
														</c:if>
														<span>${countUnpostingTax }<span>건</span></span>
														</a>
													</li>
													<li class="nowrite-list03">
														<a href="/hello/eacc/approval/nonLayReportList.do">
															<span>미상신목록</span>
															<span>${countNonLayReport }<span>건</span></span>
														</a>
													</li>
													<li class="nowrite-list04">
														<a href="/hello/eacc/approval/waitApprovalList.do">
															<span>내가 받은 결재</span>
															<c:if test="${countTodayWaitApproval ne '0'}">
																<span class="alertDate type02" >
																	<span class="alertDateBtn type02" >
																		<img src="/images/com/new_eAcc/newIcon.png" alt="업데이트">
																	</span>
																	<span class="alertDate-content">
																		<span>${countTodayWaitApproval }건 업데이트</span>
																	</span>
																</span>
															</c:if>
															<span>${countWaitApproval }<span>건</span></span>
														</a>
													</li>
												</ul>
											</div>

											<div class="eacc-box-wrap" style="margin-top:10px !important;">
												<ul class="eacc-quick-wrap">
													<li><a href="/hello/eacc/etcBill/etcBillWrite.do">
														<span><img src="/images/com/new_eAcc/quick01.png"/></span>
														<span>일반전표작성</span>
													</a></li>
													<li><a href="/hello/eacc/approval/myApprovalList.do">
														<span><img src="/images/com/new_eAcc/quick02.png"/></span>
														<span>내가올린결재</span>
													</a></li>
													<li><a href="#none">
														<span><img src="/images/com/new_eAcc/quick03.png"/></span>
														<span>도움말</span>
													</a></li>
												</ul>
											</div>
										</div>	
										<!-- E : 미작성내역 / 퀵메뉴 -->
																			
									</div>
									
									<!-- 내가올린결재 / 비용예산조회 / 일정 등 -->
									<div class="eacc-dsbd-btm">
									
										<!-- S : 내가올린결재 -->
										<div class="eacc-box-wrap wd33">
											<div class="eacc-board">
												<div class="eacc-board-tit">
													<h3>내가 올린 결재</h3>
												</div>
												<c:choose>
													<c:when test="${empty myApprovalList }">
														<div class="t-c mgn-t-100">
															<img src="/images/com/new_eAcc/icon_noresult.png" alt="등록된 결재가 없습니다." width="130px" height="auto"/>
															<div>등록된 결재가 없습니다.</div>
														</div>
													</c:when>
													<c:otherwise>
														<div class="eacc-board-con">
															<table summary="총 11개의 내가 올린 결재 내역을 볼 수 있습니다.">
																<caption class="screen-out">내가올린결재</caption>
																<colgroup>
																	<col width="19%">
																	<col width="*">
																	<col width="18%">
																	<col width="8%">
																</colgroup>
																<c:forEach var="item" items="${myApprovalList}" varStatus="status">
																	<tr>
																		<td><span class="eacc-board-btn state01">${item.APPR_KIND }</span></td>
																		<td class="t-l">
																			<a href="#" class="board-ellipsis" onclick="fn_openApprovalModal(${item.APPR_NDX});">
																				<span class="f-l">${item.TITLE}</span>
																				<span class="f-r">${item.REG_DT}</span>
																			</a>
																		</td>
																		<td>
																			<c:choose>
																				<c:when test="${item.APPR_H_STS eq '상신'}">
																					<span class="eacc-board-btn state02">${item.APPR_H_STS}</span>
																				</c:when>
																				<c:otherwise>
																					<span class="eacc-board-btn state03">${item.APPR_H_STS}</span>
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td class="f-l"><a href="#none" class="eacc-push"><img src="/images/com/new_eAcc/push.png" alt="푸시" /></a></td>
																	</tr>
																</c:forEach>
															</table>
														</div>
													</c:otherwise>
												</c:choose>
												<a href="/hello/eacc/approval/myApprovalList.do" class="board-more-btn" >+</a>
											</div>
										</div>
										<!-- E : 내가올린결재 -->
										
										<!-- S : 비용예산조회 -->
										<div class="eacc-box-wrap wd33">
											<div class="eacc-board">
												<div class="eacc-board-tit">
													<h3><font style="wieght:bold; color:#37aad9"> ${memberInfo.KTEXT }</font> 비용 예산 조회</h3>
												</div>
												<div class="eacc-board-con budgetSch">
													<table class="scrolltbody">
														<caption class="screen-out">비용예산조회</caption>
														<thead>
															<tr>
																<th style="text-align:center;">계정코드명</th>
																<th style="text-align:center;">가용 예산</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class="t-l">
																	여비교통비
																</td>
																<td class="t-r">
																	100,000 원
																</td>
															</tr>
															<tr>
																<td class="t-l">
																	통신비
																</td>
																<td class="t-r">
																	75,000 원
																</td>
															</tr>
															<tr>
																<td class="t-l">
																	소모품비
																</td>
																<td class="t-r">
																	10,000 원
																</td>
															</tr>
															<tr>
																<td class="t-l">
																	접대비
																</td>
																<td class="t-r">
																	150,000 원
																</td>
															</tr>
															<tr>
																<td class="t-l">
																	잡비
																</td>
																<td class="t-r">
																	200,000 원
																</td>
															</tr>
														</tbody>
													</table>
												</div>
<!-- 												<a href="#none" class="board-more-btn" >+</a> -->
											</div>
										</div>
										<!-- E : 비용예산조회 -->
										
										<div class="btm-right-area wd32">
											<!-- S : 공지사항 -->
											<div class="eacc-box-wrap">
												<div class="eacc-board notice">
													<div class="eacc-board-tit">
														<h3>공지사항</h3>
													</div>
													<c:choose>
														<c:when test="${empty noticeList }">
															<div class="t-c mgn-t-50">
																<img src="/images/com/new_eAcc/icon_noresult.png" alt="등록된 공지사항이 없습니다." width="130px" height="auto"/>
																<div class="mgn-t-10">등록된 공지사항이 없습니다.</div>
															</div>
														</c:when>
														<c:otherwise>
															<div class="eacc-board-con">
																<ul class="eacc-notice-wrap">
																	<c:forEach var="item" items="${noticeList}">
																		<li>
																			<a href="/hello/eacc/notice/noticeView.do?NOTICE_NDX=${item.NOTICE_NDX }" class="board-ellipsis">
																				<span class="f-l">
																					<span class="blueTxt">${item.DIVISION_NM } | ${item.user_nm }</span>
																					<span>${item.TITLE }</span>
																				</span>
																				<span class="f-r date">${item.REG_DT }</span>
																			</a>
																		</li>
																	</c:forEach>
																	</ul>
																</div>
																<a href="/hello/eacc/notice/noticeList.do" class="board-more-btn" >+</a>
														</c:otherwise>
													</c:choose>
												</div>
											</div>
											<!-- E : 공지사항 -->
									
											<!-- S : 일정 -->
											<div class="eacc-box-wrap schedule">
												<div class="eacc-schedule">
													<div class="eacc-schedule-left">
														<p><span class="bigFont">${fn:substring(today,6,8)}</span><span>일</span><p>
														<p>${fn:substring(today,0,4)}년 &nbsp;${fn:substring(today,4,6)}월</p>
														<p>${fn:substring(today,8,11)}</p>
													</div>
													<div class="eacc-schedule-right">
														<ul class="eacc-schedule-list">
															<!--
																데이터 없을 때
															 <li style="background:none; padding-left:0;">
																<img src="/images/com/new_eAcc/icon_noschedule.png" alt="오늘 일정이 없습니다." style="vertical-align:middle" />
																<span class="mgn-t-10 mgn-l-5">오늘 일정이 없습니다.</span>
															</li> -->
															<c:if test="${payDt eq 'Y' }">
																<li>경비 지급일입니다.</li>
															</c:if>
															<c:if test="${writeDeadLine eq 'Y' }">
																<li>내달 ${writeDeadLineDate }일 까지 경비 상신 기간입니다.</li>
															</c:if>
															<c:if test="${apprDeadLine eq 'Y' }">
																<li>내달 ${apprDeadLineDate }일까지 경비 승인 기간입니다.</li>
															</c:if>
														</ul>
													</div>
												</div>
											</div>
											<!-- E : 일정 -->
										</div>
										
									</div>
									
									<!-- 푸터 -->
									<div class="eacc-footer">
										본 사이트는 Chrome에 최적화 되어 있습니다.  Explore의 경우 9버전 이하에서는 호환성 관련 문제가 발생 할 수 있으니,  10버전 이상에서 사용하시길 권장합니다.
									</div>
							
								</div>
							</div>

							<!-- 페이지 내용 : e -->
						
						
					</div>
					<!--e:container/inner-->		
				</div>
				<!--e:container-->
			</section>
			<!--e:section-->
		</div>
		<!--e:main-->
	</div>
	<!-- e:wrapper -->

	<!-- 공지팝업 시작 -->
	<c:choose>
		<c:when test="${empty popNoticeList }">
		</c:when>
		<c:otherwise>
			<div id="noticePop" class="noticePop">
				<div class="noticePopWrap">
					<div class="noti-slider-wrap">
						<div class="noti-view">
							<ul class="noti-slider">
								<c:forEach var="item" items="${popNoticeList}" varStatus="status">
									<li>
										<div class="noticePopTit"><span>${item.TITLE }</span></div>
										<div class="noticePopCon">	
											${fn:replace(fn:replace(fn:replace(fn:replace(fn:replace(item.CONTENT,'<p>',''),'</p>','<br/>'),'&quot;','"'),'&lt;','<'),'&gt;','>')}
										</div>
									</li>
								</c:forEach>
							</ul>
							
							<div class="noti-controls">
								<ul class="noti-cont-list">
									<c:forEach var="item" items="${popNoticeList}" varStatus="status">
										<c:choose>
											<c:when test="${status.index eq 0 }">
												<li class="active"><span></span></li>
											</c:when>
											<c:otherwise>
												<li><span></span></li>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</ul>
							</div>
						</div>														
					</div>
				</div>
				<div class="notiFooter">
					<span class="f-l">
						<input type="checkbox" name="close" id="oneDayNone" onclick="javascript:closeWin('noticePop', 1);"/>
						<label for="oneDayNone">하루동안 이 창을 열지 않음</label>
					</span>
					<span style="float:right">
						<a href="#none" onclick="javascript:closeWin('noticePop', 0);">X&nbsp;&nbsp;닫기</a>
					</span>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
	
	<!-- 내 정보 변경 팝업 시작 -->
	<div id="profilePop" class="profilePop">
		<div class="profilePopWrap">
			<div class="profilePopTit">내 정보</div>
			<div class="profilePopCon">	
				<!-- 페이지 내용 : s -->
				<form id="userInfoForm" method="post" action="" enctype="multipart/form-data">
					<div class="profile-wrap" style="padding:0;">
						<div class="top-area">
							<div class="user-pic">
								<a href="#none" id="imgChgBtn">
									<c:choose>
										<c:when test="${memberInfo.ATTACH_GRP_NO eq null or memberInfo.ATTACH_GRP_NO eq ''}">
											<img src="/images/com/web/bg_prsn.png" alt="NO IMAGE"/>
											<input type="hidden" id="ATTACH_GRP_NO" name="ATTACH_GRP_NO" value=""/>
										</c:when>
										<c:otherwise>
											<img class="comtUsrBtn" src="/attach/thumbFileDownload.do?ATTACH_GRP_NO=${memberInfo.ATTACH_GRP_NO}&ATTACH=1" title="이미지변경" />
											<input type="hidden" id="ATTACH_GRP_NO" name="ATTACH_GRP_NO" value="${memberInfo.ATTACH_GRP_NO}"/>
										</c:otherwise>
									</c:choose>
								</a>
							</div>
							<div class="user-i">
								<span class="name">${memberInfo.NAME}</span> <span style="font-size:13px;">${memberInfo.JOB_GRADE}</span><br />
								<span class="group">${memberInfo.DIVISION_NM}</span>
							</div>
						</div>	
						<!-- top-area -->
						<div class="bottom-area">
							<div class="tb-wrap type03">
								<table class="tb-st">
									<caption></caption>
									<colgroup>
										<col width="35%" />
										<col width="*" />
									</colgroup>
									<tbody>
										<tr>
											<th>사원번호(ID)</th>
											<td>
												<input type="text" id="userId" name="userId" value="${memberInfo.USER_ID}" placeholder="사원번호(ID)" readonly="readonly"/>
											</td>
										</tr>
										<tr>
											<th>코스트센터</th>
											<td>
												<input type="text" value="${memberInfo.KTEXT} (${memberInfo.KOSTL }) " placeholder="코스트센터" readonly="readonly"/>
												<input type="hidden" id="KTEXT" name="KTEXT" value="${memberInfo.KTEXT}"/>
												<input type="hidden" id="KOSTL" name="KOSTL" value="${memberInfo.KOSTL}"/>
											</td>
										</tr>
										<tr>
											<th>내선번호</th>
											<td>
												<input type="text" id="TELEPHONE" name="TELEPHONE" value="${memberInfo.TELEPHONE}" placeholder="내선번호" readonly="readonly"/>
											</td>
										</tr>
										<tr>
											<th>휴대폰번호</th>
											<td>
												<input type="text" id="PHONE" name="PHONE" value="${memberInfo.PHONE}" placeholder="휴대폰번호" readonly="readonly"/>
											</td>
										</tr>
										<tr>
											<th>이메일</th>
											<td>
												<input type="text" id="EMAIL" name="EMAIL" value="${memberInfo.EMAIL}" placeholder="이메일" readonly="readonly"/>
											</td>
										</tr>
										<tr class="pwChg">
											<th>비밀번호 변경</th>
											<td>
												<a href="#none" class="btn comm st01" id="passwordChgBtn">비밀번호 변경</a>
											</td>
										</tr>
										<tr class="pwChgArea" style="display: none">
											<th>신규 비밀번호</th>
											<td>
												<div class="inp-wrap">
													<input type="password" id="newPw" name="newPw"/>
												</div>
											</td>
										</tr>
										<tr class="pwChgArea" style="display: none">
											<th>비밀번호 확인</th>
											<td>
												<div class="inp-wrap">
													<input type="password" id="checkPw" name="checkPw"/>
												</div>
											</td>
										</tr>
										<tr>
											<th>직인<br><span class="pd-l-10">(gif, png, jpg, jpeg)</span></th>
											<td>
												<input type="hidden" id="MODULE" name="MODULE" value="10"/>
												<input type="hidden" id="sealAttachExistYn" name="sealAttachExistYn" value=""/>
												<input type="hidden" id="DEL_IMG_SEQ" name="DEL_IMG_SEQ" value=""/>
												<input type="hidden" id="SEAL_ATTACH_GRP_NO" name="SEAL_ATTACH_GRP_NO" value="${memberInfo.SEAL_ATTACH_GRP_NO}"/>

												<!-- s:썸네일 -->
												<div id="thumbnail" class="sealImageView">
													<a href="#none" class="btn comm st01" id="changeSealBtn">직인 수정</a>
													<c:forEach var="item" items="${attachImgList}" varStatus="idx">
														<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
															<!-- 파일 이미지 -->
															<a href="/attach/thumbFileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}" class="preview">
																<img src="/attach/thumbFileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}" class="thumbnail" alt="thumbnail" />
															</a>
															<!-- 첨부파일 출력 순서 display:none으로 설정 -->
															<span class="MultiFile-export" style="display:none;">
																<input type="hidden" value="${item.SEQ_DSP}"/>
															</span> 
															<!-- 파일명  -->
															<span class="MultiFile-title" style="display: none;" title="File selected: ${item.NAME}">
																<c:choose>
																	<c:when test="${fn:length(item.NAME) > 10}">
																		${fn:substring(item.NAME, 0, 10)}...${fn:substringAfter(item.NAME, ".")} <br>
																	</c:when>
																	<c:otherwise>
																		${item.NAME}.${fn:substringAfter(item.NAME, ".")}<br>
																	</c:otherwise>
																</c:choose>
															</span>
															<!-- 다운로드  -->
<%-- 																	<a href="#none" class="imgBtnFileDwn icon download" data-attach="${item.ATTACH}" data-attach_grp_no="${item.ATTACH_GRP_NO}">다운로드</a> --%>
														</div>		
													</c:forEach>
												</div>
												<div class="sealImageEdit" style="display: none">
													<div class="bxType01 fileWrap">
														<div class="filebox" id="attachFile0"></div>
														<div class="fileList" style="padding:7px 0 5px;">
															<div id="detailFileList0" class="fileDownLst">
																<c:forEach var="item" items="${attachImgList}" varStatus="idx">
																	<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																		<span class="MultiFile-remove">
																			<a href="#none" class="icn_fileDel" style="height:10px; line-height:10px;" data-attach="${item.ATTACH}" data-idx="${item.CD_FILE_TYPE }">
																				<img src="/images/com/web/btn_remove.png" />
																			</a>
																		</span> 
																		<span class="MultiFile-export" style="display: none"> ${item.SEQ_DSP} </span>
																		<span class="MultiFile-title" title="File selected: ${item.NAME}">
																		${item.NAME}(${item.FILE_SIZE}kb)
																		</span>
																		<img class='MultiFile-preview'/>
																		<a href="#none" class="btnFileDwn icon download" data-attach="${item.ATTACH}">다운로드</a>
																	</div>	
																</c:forEach>
															</div>
														</div>
													</div>
												</div>
												<!-- e:썸네일 -->
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<!-- e:bottom-area -->
						<div class="btn-area mgn-b-15" id="infoSaveBtn">
							<a href="#none" class="btn comm st01" id="saveBtn">저장</a>
							<a href="#none" class="btn comm st02" id="cancelBtn">취소</a>
						</div>
					</div>
					<!-- e:prifile-wrap -->
				</form>
				<!-- 페이지 내용 : e -->
			</div>
			<div class="profilePopFooter">
				<a href="#none" onclick="javascript:closeWin('profilePop', 0);">X&nbsp;&nbsp;닫기</a>	
			</div>
		
		</div>
	</div>

	<!-- 전표 상신화면 모달 시작-->
	<div class="modal fade" id="myApprovalDtlModal" data-backdrop="static" data-keyboard="false"></div>
	<!-- 전표 상신화면 모달 끝-->
		
	<script type="text/javascript"> 
		$('#profileChange').on('click', function(){
			openWin('profilePop');
		});
		
		openWin('noticePop');		
	</script>  
	<!-- 공지팝업 끝 -->
	
</body>
</html>