<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: mainMyReqList.jsp
	Description : 대시보드 > 내 요청 현황 상세페이지
	author		: 김영환
	since		: 2018.06.18
 
	<< 개정이력(Modification Information) >>
	수정일 		 수정자		수정내용
	----------  ------	---------------------------
	2018.06.18	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<script type="text/javascript">
	$(document).ready(function(){
		$(".boxscroll").niceScroll({cursorwidth:"4px",cursorcolor:"#afbfd6",horizrailenabled:false});
		
		$(".myReqLstWrap .myReqLst").click(function(){
			$(".myReqLstWrap .myReqLst.click").removeClass("click");
			$(this).addClass("click");
		})	
		
	});
	
	function getRepairResultDtl(repair_request, appr_status){
		document.getElementById("repairResultDtl").src = '/epms/repair/result/repairResultDtl.do?REPAIR_REQUEST='+repair_request+'&APPR_STATUS='+appr_status;
	}
</script>
</head>
<body>

	<div class="expandPortletWrap">
		<div class="expand-myReqList">
			<div class="f-l wd-per-30">
				<div class="boxscroll left expandPortletH">
					<div class="myReqLstWrap">
						<div class="myReqLstWrap-in">
							<c:choose>
								<c:when test="${fn:length(REQUEST_LIST) < 1}">
									<div class="empty-state" style="overflow: hidden;">
										<p class="empty-state-copy">조회된 데이터가 없습니다.</p>
									</div>
								</c:when>
								<c:otherwise>
									<c:forEach var="item" items="${REQUEST_LIST}" varStatus="idx">
									<div class="myReqLst" onclick="getRepairResultDtl('${item.REPAIR_REQUEST}', '${item.APPR_STATUS}')">
										<c:choose>
											<c:when test="${item.APPR_STATUS eq '01'}">
												<div class="myReqLstTit waiting">
													<div class="f-l">
														<div class="port-status waiting">
											</c:when>
											<c:when test="${item.APPR_STATUS eq '02' && item.REPAIR_STATUS eq '01'}">
												<div class="myReqLstTit accept">
													<div class="f-l">
														<div class="port-status accept">
											</c:when>
											<c:when test="${item.APPR_STATUS eq '02' && item.REPAIR_STATUS eq '02'}">
												<div class="myReqLstTit assign">
													<div class="f-l">
														<div class="port-status assign">
											</c:when>
											<c:when test="${item.APPR_STATUS eq '02' && item.REPAIR_STATUS eq '03'}">
												<div class="myReqLstTit complete">
													<div class="f-l">
														<div class="port-status complete">
											</c:when>
											<c:when test="${item.APPR_STATUS eq '03' || item.APPR_STATUS eq '04'}">
												<div class="myReqLstTit reject">
													<div class="f-l">
														<div class="port-status reject">
											</c:when>
											<c:when test="${item.APPR_STATUS eq '09'}">
												<div class="myReqLstTit reqCancel">
													<div class="f-l">
														<div class="port-status reqCancel">
											</c:when>
										</c:choose>
													<span>${item.STATUS_NAME}</span>
												</div>
											</div>
											<div class="f-l">
												<p class="materialName">${item.EQUIPMENT_UID}<br>${item.EQUIPMENT_NAME}</p>
												<p class="materialPst">${item.LOCATION_NAME } > ${item.LINE_NAME}</p>
											</div>
											<div class="f-r">
												처리파트 : ${item.PART_NAME}
											</div>
										</div>
										<div class="myReqLstCon">
											<div class="timeline">
												<div class="timelineBody">
													<span class="timelineImg"><img src="${item.REQUEST_MEMBER_FILEURL}" onerror="this.src='/images/com/web_v2/userIcon.png'" alt="${item.REQUEST_MEMBER_GRADE}"></span>
													<div class="timelineInfo">
														<p class="timelineUser">${item.REQUEST_MEMBER_NAME} ${item.REQUEST_MEMBER_GRADE} <span>제조2${item.REQUEST_MEMBER_DIV}</span></p>
														<p class="timelineTime">${item.DATE_REQUEST_HHMI} | ${item.DATE_REQUEST_YYMMDD}</p> 
													</div>
													<div class="timelineMsg">${item.REQUEST_DESCRIPTION}</div>
												</div>
											</div>
											<c:if test="${item.APPR_STATUS ne '01'&& item.APPR_STATUS ne '09'}">
											<div class="timeline">
												<div class="timelineBody">
													<span class="timelineImg"><img src="${item.RESPONSE_MEMBER_FILEURL}" onerror="this.src='/images/com/web_v2/userIcon.png'" alt="${item.RESPONSE_MEMBER_GRADE}"></span>
													<div class="timelineInfo">
														<p class="timelineUser">${item.RESPONSE_MEMBER_NAME} ${item.RESPONSE_MEMBER_GRADE} <span>${item.RESPONSE_MEMBER_DIV}</span></p>
														<p class="timelineTime">${item.DATE_RESPONSE_HHMI} | ${item.DATE_RESPONSE_YYMMDD}</p> 
													</div>
													<div class="timelineMsg answer">${item.RESPONSE_DESCRIPTION}</div>
												</div>
											</div>
											</c:if>
										</div>
									</div>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
			<div class="f-l wd-per-70">
				<iframe id="repairResultDtl" class="expandIfrm" name="repairResultDtl" marginwidth="0px" marginheight="0px" frameborder="no"  scrolling="no" align="center" style="width:100%;"
				 src=""></iframe>
			</div>
		</div>
	</div>
</body>
</html>