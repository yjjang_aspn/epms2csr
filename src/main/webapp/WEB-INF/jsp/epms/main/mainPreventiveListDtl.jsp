<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%--
	Class Name	: mainPreventiveListDtl.jsp
	Description : 대시보드 > 예방보전  현황 상세페이지
	author		: 김영환
	since		: 2018.06.21
 
	<< 개정이력(Modification Information) >>
	수정일 		 수정자		수정내용
	----------  ------	---------------------------
	2018.06.21	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
</head>
<body>

	<div class="expandPortletWrap bgWhite">
		<div class="expandSchWrap rel">
			<div class="wrap-inq">	
				<div class="inq-clmn">
					<h4 class="tit-inq"><spring:message code='epms.location' /></h4><!-- 공장 -->
					<div class="sel-wrap type02" style="width:120px">
						<select title="공장" id="LOCATION" name="LOCATION" >
							<c:choose>
								<c:when test="${fn:length(locationList) > 0}">
									<c:forEach var="item" items="${locationList}">
										<option value="${item.CODE}" role="${item.REMARKS}" <c:if test="${sessionScope.ssLocation eq item.CODE}"> selected="selected"</c:if>>${item.NAME}</option>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<option value="" selected="selected">권한필요</option>
								</c:otherwise>
							</c:choose>
						</select>
					</div>
				</div>
				<div class="inq-clmn">
					<h4 class="tit-inq">파트</h4>
					<div class="sel-wrap type02" style="width:80px">
						<tag:combo codeGrp="PART" name="PART" companyId="${ssCompanyId}" all="Y"/>
					</div>
				</div>
			</div>
			
			<div class="monthMoveWrap ab">
				<div class="monthMoveBtn prev"><div id="monthPrev"><</div></div>
				<div class="monthMoveInfo"><span id="monthMoveInfo"></span></div>
				<div class="monthMoveBtn next"><div id="monthNext">></div></div>
			</div>
		</div>
		
		<div class="expandPortletH02">
			<div class="exapndCountDtl">
				<ul class="statusDtlWrap" id="dashboardInfo">
					<li class="statusDtl v-center" id="PREVENTIVE_PLAN_LI">
						<div>
							<p class="statusTit">예방보전 계획<p>
							<p class="statusCont" name="PREVENTIVE_PLAN_CNT" id="PREVENTIVE_PLAN_CNT"><span>건</span></p>
						</div>
					</li>
					<li class="statusDtl v-center" id="PREVENTIVE_COMPLETE_LI">
						<div>
							<p class="statusTit">점검 완료<p>
							<p class="statusCont" name="PREVENTIVE_COMPLETE_CNT" id="PREVENTIVE_COMPLETE_CNT"><span>건</span></p>
							<p class="stutusWarning">정비필요 <span class="fontRed" name="PREVENTIVE_REPAIR_REQUEST_CNT"></span> 건</p>
						</div>
					</li>
					<li class="statusDtl v-center" id="PREVENTIVE_ONGOING_LI">
						<div>
							<p class="statusTit">진행중<p>
							<p class="statusCont" name="PREVENTIVE_ONGOING_CNT" id="PREVENTIVE_ONGOING_CNT"><span>건</span></p>
							<p class="stutusWarning">지연 <span class="fontRed" name="PREVENTIVE_EXCESS_CNT"></span> 건</p>
						</div>
					</li>
					<li class="statusDtl v-center" id="PREVENTIVE_UNCHECK_LI">
						<div>
							<p class="statusTit">미점검<p>
							<p class="statusCont" name="PREVENTIVE_UNCHECK_CNT" id="PREVENTIVE_UNCHECK_CNT"><span>건</span></p>
						</div>
					</li>
				</ul>
			</div>
			
			<div class="expandChartDtl">
				<div class="expandChartDtlIn">
					<div class="f-l wd-per-35">
						<div id="repairTypeChartDiv" class="expandChart" style="height:30vh;">
							<div class="expandChartTit">TBM</div>
							<canvas id="preventiveTBM"></canvas>
						</div>
						<div style="margin-top:2vh">
							<div id="repairTypeChartDiv" class="expandChart " style="height:30vh;">
								<div class="expandChartTit">CBM</div>
								<canvas id="preventiveCBM"></canvas>
							</div>
						</div>
					</div>
					<div class="f-l wd-per-65">
						<div class="mgn-l-10">
							<div>
								<div id="troubleTimeChartDiv" class="expandChart" style="height:30vh;">
									<div class="expandChartTit">계획일 초과현황</div>
									<canvas id="preventiveDayOverChart"></canvas>
								</div>
							</div>
							<div style="margin-top:2vh">
								<div class="wd-per-50 f-l">
									<div id="troubleType1ChartDiv" class="expandChart" style="height:30vh;">
										<div class="expandChartTit">CBM 판정</div>
								<canvas id="preventiveCBM_DECISION"></canvas>
									</div>
								</div>
								<div class="wd-per-50 f-l">
									<div id="troubleType2ChartDiv" class="expandChart mgn-l-10" style="height:30vh;">
										<div class="expandChartTit">예방정비 수행현황</div>
										<canvas id="preventivePerformChart"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>

<%-- 예방보전실적현황 --%>
<%@ include file="/WEB-INF/jsp/epms/main/popMainPreventivePlanList.jsp"%>

<%-- 예방보전실적조회 --%>
<div class="modal fade modalFocus" id="popPreventiveResultForm" data-backdrop="static" data-keyboard="false"></div>

<script type="text/javascript">
var year = 1;			<%-- [YYYY 연도] 전역변수 --%>
var month = 1;			<%-- [MM 월] 전역변수 --%>
var month_rare = 1;		<%-- [가공 전 월] 전역변수 --%>
var CHECK_TYPE = "";
var CHECK_DECISION = "";
var cbmData = [], tbmData = [], cbmDecisionDataID = [], performDataID = [];

var preventiveStatus = [];
var barOption = {
	maintainAspectRatio: false,
	responsive: true,
	legend: {
        display: false
    },
    tooltips: {
		enabled: true,
		intersect: false,
		titleFontSize: 11,
		bodyFontSize: 11,
		callbacks: {
			label: function(tooltipItem, data) {
				var allData = data.datasets[tooltipItem.datasetIndex].data;
				var tooltipLabel = data.labels[tooltipItem.index];
				var tooltipData = allData[tooltipItem.index];
				var total = 0;
				for (var i=0; i<allData.length; i++) {
					total += allData[i];
				}
				var tooltipPercentage = Math.round((tooltipData / total) * 100);
				
				return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
			}
		}
	},
    scales: {
		xAxes: [{
			barPercentage: 0.5,
			gridLines: {
				display: false
			}
		}],
		yAxes: [{
		    gridLines: {
		        drawBorder: false//축과 데이터의 경계선 표시 여부
		
			},
			ticks: {
				display: true, //축의 값 표시 여부
				min: 0,
				//stepSize: 0
			}
		}]
	},
	
	showValue : {
		fontStyle: 'Helvetica', //Default Arial
		fontSize: 20
	},
	'onClick' : function (event) {
		var param;
		var flag;
		if(this.canvas.id == 'preventiveTBM'){
			CHECK_TYPE = "02";
			var activePoints = mytbmChart.getElementsAtEvent(event);
	        var firstPoint = activePoints[0];
	        param = tbmData[firstPoint._index];
   	 	}else if(this.canvas.id == 'preventiveCBM'){
			CHECK_TYPE = "01";
			var activePoints = mycbmChart.getElementsAtEvent(event);
	        var firstPoint = activePoints[0];
	        label = mycbmChart.data.labels[firstPoint._index];
	        param = cbmData[firstPoint._index];
   	 	} else if(this.canvas.id == 'preventiveDayOverChart'){
			var activePoints = dayOverChart.getElementsAtEvent(event);
	        var firstPoint = activePoints[0];
	        label = dayOverChart.data.labels[firstPoint._index];
	        if(firstPoint._index == 5) {
	        	param = "09";
	        	flag = "99";
	        } else {
	        	param = "";
		        flag = firstPoint._index;
	        }
   	 	}
		$('#popMainPreventivePlanList_title').text("예방보전계획현황(" + $("#" + this.canvas.id).prev().text() + ")");
		param == "03" ? param = "(02, 03)" : param;
		popPreventive(param, flag);
    }
}

var ctx = document.getElementById("preventiveCBM");
var mycbmChart = new Chart(ctx, {
	    type: 'bar'
	    , data: {
			labels: ["진행중", "점검완료", "미점검"],
			datasets: [
				{
					label: "CBM",							
					backgroundColor: ["rgba(244, 208, 12, 1)", "rgba(0, 147, 209, 1)", "rgba(242, 99, 95, 1)"],
					hoverBackgroundColor: ["rgba(244, 208, 12, 0.5)", "rgba(0, 147, 209, 0.5)", "rgba(242, 99, 95, 0.5)"], 
					borderWidth:1,
					data: []
				}
			]
		}
	    , options : barOption
	});
	
var ctx = document.getElementById("preventiveTBM");
var mytbmChart = new Chart(ctx, {
	    type: 'bar'
	    , data: {
	    	labels: ["진행중", "점검완료", "미점검"],
			datasets: [
				{
					label: "TBM",							
					backgroundColor: ["rgba(244, 208, 12, 1)", "rgba(0, 147, 209, 1)", "rgba(242, 99, 95, 1)"],
					hoverBackgroundColor: ["rgba(244, 208, 12, 0.5)", "rgba(0, 147, 209, 0.5)", "rgba(242, 99, 95, 0.5)"], 
					borderWidth:1,
					data: []
				}
			]
		}
	    , options : barOption
	});

var ctx = document.getElementById("preventiveCBM_DECISION");
var pieChart = new Chart(ctx, {
	    type: 'pie'
	    , data: {
			labels: [],
			datasets: [
				{
					backgroundColor: ["rgba(0, 147, 209, 1)", "rgba(244, 208, 12, 1)", "rgba(242, 99, 95, 1)"],
					hoverBackgroundColor: ["rgba(0, 147, 209, 0.5)", "rgba(244, 208, 12, 0.5)", "rgba(242, 99, 95, 0.5)"], 
					borderWidth:1,
					data: []
				}
			]
		}
	    , options : {
	    	maintainAspectRatio: false,
			responsive: true,
			tooltips: {
				enabled: true,
				intersect: false,
				titleFontSize: 11,
				bodyFontSize: 11,
				callbacks: {
					label: function(tooltipItem, data) {
						var allData = data.datasets[tooltipItem.datasetIndex].data;
						var tooltipLabel = data.labels[tooltipItem.index];
						var tooltipData = allData[tooltipItem.index];
						var total = 0;
						for (var i=0; i<allData.length; i++) {
							total += allData[i];
						}
						var tooltipPercentage = Math.round((tooltipData / total) * 100);
						
						return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
					}
				}
			},
			legend: {
		        display: true,
		        position: 'right'
		        
		    },
			'onClick' : function (event) {
				var activePoints = pieChart.getElementsAtEvent(event);
		        var firstPoint = activePoints[0];
				CHECK_DECISION = cbmDecisionDataID[firstPoint._index];
				popPreventive();
		    }
		}
	});
	
var ctx = document.getElementById("preventivePerformChart");
var performChart = new Chart(ctx, {
	    type: 'doughnut'
	    , data: {
			labels: [],
			datasets: [
				{
					backgroundColor: ["rgba(0, 147, 209, 1)", "rgba(242, 99, 95, 1)"],
					hoverBackgroundColor: ["rgba(0, 147, 209, 0.5)", "rgba(242, 99, 95, 0.5)"], 
					borderWidth:1,
					data: []
				}
			]
		}
		, options : {
			maintainAspectRatio: false,
			responsive: true,
			legend: {
		        display: true,
		        position: 'right'
		        
		    },
		    tooltips: {
				enabled: true,
				intersect: false,
				titleFontSize: 11,
				bodyFontSize: 11,
				callbacks: {
					label: function(tooltipItem, data) {
						var allData = data.datasets[tooltipItem.datasetIndex].data;
						var tooltipLabel = data.labels[tooltipItem.index];
						var tooltipData = allData[tooltipItem.index];
						var total = 0;
						for (var i=0; i<allData.length; i++) {
							total += allData[i];
						}
						var tooltipPercentage = Math.round((tooltipData / total) * 100);
						
						return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
					}
				}
			},
			'onClick' : function (event) {
				var param = "";
				var activePoints = performChart.getElementsAtEvent(event);
		        var firstPoint = activePoints[0];
				CHECK_DECISION = "03";
				param = performDataID[firstPoint._index];
				popPreventive(param);
		    }
		}
	});
	
<%-- 계획일 초과현황 그래프 --%>
var ctx = document.getElementById("preventiveDayOverChart");
var	dayOverChart = new Chart(ctx, {
		type: 'bar'
		, data: {
			labels : ["정상점검", "1~3일", "4~6일", "1주", "2주 이상", "미점검"],
			datasets : [
			    {
			    	label: '건수',			
			    	data: [],
			    	backgroundColor: "rgba(0, 147, 209, 1)",
			    	hoverBackgroundColor: "rgba(0, 147, 209, 0.5)"
				}
			]
		}
		, options: barOption
	});
	

$(document).ready(function(){			
	$(".boxscroll").niceScroll({cursorwidth:"4px",cursorcolor:"#afbfd6",horizrailenabled:false});
	
	<%-- 현재 날짜 설정 --%>
	var date = new Date();
	year = date.getFullYear();
	month_rare = parseInt(date.getMonth()) + 1;
	
	<%-- 화면 데이터 입력--%>
// 	fnSetData();
	
	<%-- 이전 달로 이동 --%>
	$('#monthPrev').on('click', function(){
		<%-- 1월일 경우 년도 차감 --%>
		if(month_rare == 1){
			year = parseInt(year) - 1;
			month_rare = 12
		}else{
			month_rare = parseInt(month_rare) - 1;
		}
		
		<%-- 화면 데이터 입력--%>
		fnSetData();
	});
	
	<%-- 다음 달로 이동 --%>
	$('#monthNext').on('click', function(){
		<%-- 12월일 경우 년도 증가 --%>
		if(month_rare == 12){
			year = parseInt(year) + 1;
			month_rare = 1
		}else{
			month_rare = parseInt(month_rare) + 1;
		}
		
		<%-- 화면 데이터 입력--%>
		fnSetData();
	});
	$("#LOCATION,#PART").change(function(){
		fnSetData();
	});
	
	<%-- 예방보전 카운트 클릭 시 --%>
	$(".statusDtl").on('click', function(){
		if($(this).find('.statusCont').text() != 0){
			var param = $(this).attr("id").replace(/_LI/g, '');
			popPreventive(param)
	 		$('#popMainPreventivePlanList_title').text("예방보전계획현황(" + $(this).find('.statusTit').text() + ")");
		}
	})
});

function fnGetPreventiveInfo(date, location, part, remarks){
	
	$.ajax({
		type : 'POST',
		url : '/epms/main/mainPreventiveListDtlAjax.do',
		cache : false,
		dataType : 'json',
		data : { PARAM_DATE 		: date
				 , REMARKS 			: remarks
				 , PARAM_LOCATION 	: location
				 , PARAM_PART 		: part == "all" ? "" : part },
		async : false,
		error : function() {
			alert("처리중 오류가 발생 하였습니다.");
		},
		success : function(json) {
			var preventiveInfo = json.preventiveInfo;
			
			var _ul = $("#dashboardInfo");
			
			for( var name in preventiveInfo ) {
            	switch (name) {
	 				default:
	 					_ul.find("[name=" + name + "]").text(preventiveInfo[name]);
	 				break;
 				}
			}
			mycbmChart.data.datasets[0].data = [];
			mytbmChart.data.datasets[0].data = [];
			tbmData = [];
			cbmData = [];
			$.each(json.cbmChartInfo, function(i, obj) {
				tbmData.push(obj.CHECK_TYPE);
				mycbmChart.data.datasets[0].data.push(obj.CHECK_TYPE_CNT);
			});
			$.each(json.tbmChartInfo, function(i, obj) {
				cbmData.push(obj.CHECK_TYPE);
				mytbmChart.data.datasets[0].data.push(obj.CHECK_TYPE_CNT);
			});
			
			mycbmChart.update();
			mytbmChart.update();
			
			var cbmDecisionData = [], cbmDecisionLabel = [], cbmDecisionCnt = 0;
			cbmDecisionDataID = [];
			
			$.each(json.cbmDecisionChartInfo, function(i, obj) {
				cbmDecisionLabel.push(obj.CHECK_DECISION_NAME);
				cbmDecisionDataID.push(obj.CHECK_DECISION);
				cbmDecisionData.push(obj.DECISION_CNT);
				if(obj.DECISION_CNT == '0') cbmDecisionCnt = cbmDecisionCnt + 1;
			});
			if(cbmDecisionCnt != 3) {
				pieChart.data.labels = cbmDecisionLabel;
				pieChart.data.datasets[0].data = cbmDecisionData;
			} else {
				pieChart.data.labels = '';
				pieChart.data.datasets[0].data = '';
			}
			pieChart.update();
			
			var performData = [], performLabel = [], performCnt = 0;
			performDataID = [];
			
			$.each(json.performChartInfo, function(i, obj) {
				performDataID.push(obj.STATUS);
				performLabel.push(obj.STATUS_NM);
				performData.push(obj.STATUS_CNT);
				if(obj.STATUS_CNT == '0') performCnt = performCnt + 1;
			});
			if(performCnt != 2) {
				performChart.data.labels = performLabel;
				performChart.data.datasets[0].data = performData;
			} else {
				performChart.data.labels = '';
				performChart.data.datasets[0].data = '';
			}
			performChart.update();
			
			dayOverChart.data.datasets[0].data = [json.dayOverChartInfo.DAY1
												, json.dayOverChartInfo.DAY3
												, json.dayOverChartInfo.DAY6
												, json.dayOverChartInfo.DAY13
												, json.dayOverChartInfo.DAYOVER
												, json.dayOverChartInfo.PREVENTIVE_UNCHECK_CNT]
			dayOverChart.update();
		}		
	});
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- 화면 내 대시보드/차트 데이터 세팅 --%>
function fnSetData(){
	month_rare < 10 ? month = "0"+month_rare : month = month_rare;
	
	$("#monthMoveInfo").text(year + "년 " + month + "월");
	
	var yearMonth = year+""+month;
	var location = $("#LOCATION").val();
	var part = $("#PART").val();
	var remarks = $("select[name=LOCATION] option:selected").attr("role");

	fnGetPreventiveInfo(yearMonth, location, part, remarks);
}
<%-- 예방보전 현황 상세 리포트 내 예방보전 팝업 --%>
function popPreventive(param, flag){
	
	year = year.toString(); 
	month = month.toString();
	
	var startDt = year + month;
	var part = $("#PART").val() == "all" ? "" : $("#PART").val();
	var status = param == undefined ? "" : param;
	flag == undefined ? flag = "" : flag = flag;
	
	if(param=="PREVENTIVE_ONGOING" || status=="01"){
		Grids.PopMainPreventivePlanList.Source.Layout.Url = "/epms/main/popMainPreventivePlanListLayout.do?LOCATION=${sessionScope.ssLocation}";
	}else {
		Grids.PopMainPreventivePlanList.Source.Layout.Url = "/epms/main/popMainPreventiveResultListLayout.do?LOCATION=${sessionScope.ssLocation}";
	}
	
	Grids.PopMainPreventivePlanList.Source.Data.Url = "/epms/main/popMainPreventivePlanListData.do?LOCATION=" + $("#LOCATION").val() + "&startDt=" + startDt + "&PART=" + part 
																								+ "&CHECK_TYPE=" + CHECK_TYPE + "&CHECK_DECISION=" + CHECK_DECISION 
																								+ "&PREVENTIVE_STATUS=" + status + "&flag=" + flag;
	Grids.PopMainPreventivePlanList.Reload();
	fnModalToggle('popMainPreventivePlanList');
	CHECK_TYPE = "";
	CHECK_DECISION = "";
	flag = "";
}
</script>

</body>
</html>