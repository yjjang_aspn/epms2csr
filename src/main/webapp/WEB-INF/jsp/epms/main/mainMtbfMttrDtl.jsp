<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%--
	Class Name	: mainMtbfMttrDtl.jsp
	Description : 대시보드 > MTBF/MTTR
	author		: 김영환
	since		: 2018.06.21
 
	<< 개정이력(Modification Information) >>
	수정일 		 수정자		수정내용
	----------  ------	---------------------------
	2018.06.21	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
</head>
<body>

	<div class="expandPortletWrap">
		<div class="expandSchWrap rel">
			<div class="wrap-inq">	
				<div class="inq-clmn">
					<h4 class="tit-inq"><spring:message code='epms.location' /></h4><!-- 공장 -->
					<div class="sel-wrap type02" style="width:120px">
						<select title="공장" id="LOCATION" name="LOCATION" >
							<c:choose>
								<c:when test="${fn:length(locationList) > 0}">
									<c:forEach var="item" items="${locationList}">
										<option value="${item.CODE}" role="${item.REMARKS}" <c:if test="${sessionScope.ssLocation eq item.CODE}"> selected="selected"</c:if>>${item.NAME}</option>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<option value="" selected="selected">권한필요</option>
								</c:otherwise>
							</c:choose>
						</select>
					</div>
				</div>
			</div>
			
			<div class="monthMoveWrap ab">
				<div class="monthMoveBtn prev"><div id="yearPrev"><</div></div>
				<div class="monthMoveInfo"><span id="yearMoveInfo"></span></div>
				<div class="monthMoveBtn next"><div id="yearNext">></div></div>
			</div>
		</div>
		
		<div class="boxscroll expandPortletH02">	
			<div class="expandChartDtl">
				<div class="expandChartDtlIn">
					<div class="mgn-t-10">
						<div class="expandChart" style="height:34vh;">
							<div class="expandChartTit">MTBF (평균고장간격)</div>
							<canvas id="mtbfType"></canvas>
						</div>
					</div>
					<div class="mgn-t-10">
						<div class="expandChart" style="height:34vh;">
							<div class="expandChartTit">MTTR (평균수리시간)</div>
							<canvas id="mttrType"></canvas>
						</div>
					</div>
					<div class="mgn-t-10">
						<div class="expandChart" style="height:34vh;">
							<div class="expandChartTit">가용도</div>
							<canvas id="availability"></canvas>
						</div>
					</div>				
				</div>
			</div>
		</div>
	
	</div>
	
<script type="text/javascript">
var year = 1;			<%-- [YYYY 연도] 전역변수 --%>

var today = new Date();
var month = today.getMonth() + 1
var YYYYMM = today.getFullYear() + '-' + (month < 10 ? "0" + month : month);
var chartOption = {
	maintainAspectRatio: false,
	responsive: true,
	legend:{
		display:true,
		position : 'right',
		labels:{
			fontFamily: "'Open Sans Bold', sans-serif",
			fontSize:11
		}
	},
	scales: {
        yAxes: [{
            ticks: {
                min: 0,
                beginAtZero: true
            }
        }]
    }
}

var ctx = document.getElementById('mtbfType');

var mtbfTypeChart = new Chart(ctx, {
	type: 'line',
	data: {
	labels: ["1월","2월","3월","4월","5월","6월","7월","8월","9월","10월","11월","12월"],
	datasets: [{ 
        data: [],
        label: "",
        lineTension: 0,
        borderWidth: 1,
        borderColor: "#3e95cd",
        borderCapStyle : 'butt',
        ponintBackgroundColor : "#ccc",
        fill: false
      }, { 
        data: [],
        label: "",
        lineTension: 0,
        borderWidth: 1,
        borderColor: "#8e5ea2",
        fill: false
      }
    ]
	},
	options: chartOption
});

var ctx = document.getElementById('mttrType');

var mttrTypeChart = new Chart(ctx, {
	type: 'line',
	data: {
	labels: ["1월","2월","3월","4월","5월","6월","7월","8월","9월","10월","11월","12월"],
		datasets: [{ 
	        data: [],
	        label: "2018",
	        lineTension: 0,
	        borderWidth: 1,
	        borderColor: "#3e95cd",
	        borderCapStyle : 'butt',
	        ponintBackgroundColor : "#ccc",
	        fill: false
	      }, { 
	        data: [],
	        label: "2017",
	        lineTension: 0,
	        borderWidth: 1,
	        borderColor: "#8e5ea2",
	        fill: false
	      }
	    ]
	},
	options: chartOption
	
});

var ctx = document.getElementById('availability');

var availabilityChart = new Chart(ctx, {
	type: 'bar',
    data: {
      labels: ["1월","2월","3월","4월","5월","6월","7월","8월","9월","10월","11월","12월"],
      datasets: [
        { 
          label: "2018",
          backgroundColor: "#3e95cd",
          data: [],
        }, {
          label: "2017",
          backgroundColor: "#8e5ea2",
          data: [],
          fill: false
        }
      ]
    },
    options: {
    	maintainAspectRatio: false,
    	responsive: true,
    	scales: {
    		xAxes: [{
    			barPercentage: 0.7,
    			gridLines: {
    				display:false,
    					color: "#fff",
    					zeroLineColor: "#fff",
    					zeroLineWidth: 0
    			},
    			ticks: {
    				fontFamily: "'Open Sans Bold', sans-serif",
    				fontSize:11
    			}
    		}]
        }
    }
});

$(document).ready(function(){		
	$(".boxscroll").niceScroll({cursorwidth:"4px",cursorcolor:"#afbfd6",horizrailenabled:false});
	
	<%-- 현재 날짜 설정 --%>
	var date = new Date();
	year = date.getFullYear();
	
	<%-- 화면 데이터 입력--%>
	fnSetData();
	
	$('.monthMoveBtn.next').hide();
	
	<%-- 이전 달로 이동 --%>
	$('#yearPrev').on('click', function(){
		year = parseInt(year) - 1;
		<%-- 화면 데이터 입력--%>
		fnSetData();
		$('.monthMoveBtn.next').show();
	});
	
	<%-- 다음 달로 이동 --%>
	$('#yearNext').on('click', function(){
		year = parseInt(year) + 1;
		<%-- 화면 데이터 입력--%>
		fnSetData();
		<%-- 조회한 날짜가 현재일 경우 다음 날짜선택버튼 숨김 --%>
		if(year >= date.getFullYear()){
			$('.monthMoveBtn.next').hide();
		} else{
			$('.monthMoveBtn.next').show();
		}
	});
	
	
	
	<%-- 공장 값 변경 시 --%>
	$('#LOCATION').on('change', function(){
		<%-- 화면 데이터 입력--%>
		fnSetData();
	});
});

<%-- 화면 내 대시보드/차트 데이터 세팅 --%>
function fnSetData(){
	
	$("#yearMoveInfo").text(year + "년 ");
	
	var location = $("#LOCATION").val();
	
	$.ajax({
		type : 'POST',
		url : '/epms/main/mtbfMttrDtlInfo.do',
		cache : false,
		dataType : 'json',
		data : { PARAM_LOCATION 	: location
				,PARAM_YEAR 		: year},
		async : false,
		error : function() {
			alert("처리중 오류가 발생 하였습니다.");
		},
		success : function(json) {
			var thisYearArry = json.thisYearMtbfMttrInfo;
			var previousYearArry = json.previousYearMtbfMttrInfo;
			
			var thisYear = "", previousYear = "";
			
			var thisYearMtbf = [], previousYearMtbf = [], thisYearMttr = [], previousYearMttr = [], thisYearAilability = [], previousYearAVAilability = [];
			for(var i = 0 ; i < 12 ; i++){
				if(i == 0){
					thisYear = thisYearArry[i].YYYYMM.substring(0, 4);
					previousYear = previousYearArry[i].YYYYMM.substring(0, 4);
				}
				if(YYYYMM >= thisYearArry[i].YYYYMM){
					thisYearMtbf.push(thisYearArry[i].MTBF);
					thisYearMttr.push(thisYearArry[i].MTTR);
					thisYearAilability.push(thisYearArry[i].AVAILABILITY);
				}
				
				previousYearMtbf.push(previousYearArry[i].MTBF);
				previousYearMttr.push(previousYearArry[i].MTTR);
				previousYearAVAilability.push(previousYearArry[i].AVAILABILITY);
			}
			
			
			mtbfTypeChart.data.datasets[0].label = thisYear;
			mtbfTypeChart.data.datasets[1].label = previousYear;
			mtbfTypeChart.data.datasets[0].data	 = thisYearMtbf;
			mtbfTypeChart.data.datasets[1].data  = previousYearMtbf;
			mtbfTypeChart.update();
			
			mttrTypeChart.data.datasets[0].label = thisYear;
			mttrTypeChart.data.datasets[1].label = previousYear;
			mttrTypeChart.data.datasets[0].data	 = thisYearMttr;
			mttrTypeChart.data.datasets[1].data  = previousYearMttr;
			mttrTypeChart.update();
			
			availabilityChart.data.datasets[0].label = thisYear;
			availabilityChart.data.datasets[1].label = previousYear;
			availabilityChart.data.datasets[0].data	= thisYearAilability;
		 	availabilityChart.data.datasets[1].data = previousYearAVAilability;
			availabilityChart.update();
		}		
	});
	
	
}

</script>

</body>
</html>