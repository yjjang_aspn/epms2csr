<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld"                 %>
<%@ taglib prefix="c"	   uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: equipmentHistoryListLayout.jsp
	Description : 기기이력 그리드 레이아웃
    author		: 서정민
    since		: 2018.02.07
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.02.07	 서정민		최초 생성

--%>

<c:set var="gridId" value="EquipmentHistoryList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1" 			SuppressCfg="0"
			Editing="1"				Deleting="0"		Selecting="0"		Dragging="0"
			Filtering="1"			StandardFilter="3"	ClearSelected="2"
			Paging="1"			  	PageLength="15"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="3"			PasteFocused="3"	
	/>

	<Head>
		<Header	id="Header"	Align="center" CanExport="1" CanHide="0"
			EQUIPMENT_HISTORY		= "<spring:message code='epms.equipment.HISTORY.EQUIPMENT_HISTORY' />"   <% // 설비이력ID  %>
			EQUIPMENT				= "<spring:message code='epms.equipment.EQUIPMENT' />"                   <% // 설비ID     %>
			LOCATION				= "<spring:message code='epms.equipment.LOCATION' />"                    <% // 위치            %>
			LINE					= "<spring:message code='epms.equipment.LINE_ID' />"                     <% // 라인ID     %>
			LINE_UID				= "<spring:message code='epms.equipment.LINE_UID' />"                    <% // 기능위치       %>
			LINE_NAME				= "<spring:message code='epms.equipment.LINE' />"                        <% // 라인            %>
			EQUIPMENT_UID			= "<spring:message code='epms.equipment.uid' />"                         <% // 설비코드       %>
			EQUIPMENT_NAME			= "<spring:message code='epms.equipment.name' />"                        <% // 설비명         %>
			                          
			HISTORY_TYPE			= "<spring:message code='epms.type' />"                               <% // 이력유형       %>
			DATE_HISTORY			= "<spring:message code='epms.date.execute' />"                               <% // 수행일         %>
			HISTORY_DESCRIPTION		= "<spring:message code='epms.history' />"                            <% // 내역           %>
			DETAIL_ID				= "<spring:message code='epms.detail' />"                             <% // 상세정보      %>
		/>
		<Filter	id="Filter" CanExport="1" CanHide="0" CanFocus="1"/>
	</Head>
	
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4" CanHide="0"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	
	<Pager Visible="0"/>
	
	<Cols>
		<C Name="EQUIPMENT_HISTORY"			Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="EQUIPMENT"					Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="LOCATION"					Type="Enum" Align="left"   Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>	
		<C Name="LINE"						Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="LINE_UID"					Type="Text" Align="left"   Visible="0" Width="120" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="LINE_NAME"					Type="Text" Align="left"   Visible="0" Width="120" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT_UID"				Type="Text" Align="left"   Visible="0" Width="80"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT_NAME"			Type="Text" Align="left"   Visible="0" Width="180" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		
		<C Name="HISTORY_TYPE"				Type="Enum" Align="left"   Visible="1" Width="80"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="HISTORY_TYPE"/>/>
		<C Name="DATE_HISTORY"				Type="Date" Align="center" Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd"/>
		<C Name="HISTORY_DESCRIPTION"		Type="Text" Align="left"   Visible="1" Width="200" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="200"/>
		
		<C Name="DETAIL_ID"					Type="Icon" Align="center" Visible="1" Width="80"  CanEdit="0" CanExport="0" CanHide="1"/>
	</Cols>
	<RightCols>
	</RightCols>
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,항목,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyWidth="1" Empty="        " 
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
				엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>