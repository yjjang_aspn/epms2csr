<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
	Class Name	: equipmentHistoryListData.jsp
	Description : 기기이력 그리드 데이터
    author		: 서정민
    since		: 2018.02.07
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.02.07	 서정민		최초 생성

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I
			
			EQUIPMENT_HISTORY		= "${item.EQUIPMENT_HISTORY}"								<%-- 설비이력 ID --%>
			LOCATION				= "${item.LOCATION}"										<%-- 위치 --%>
			LINE					= "${item.LINE}"											<%-- 라인ID --%>
			LINE_UID				= "${fn:replace(item.LINE_UID, '"', '&quot;')}"				<%-- 기능위치 --%>
			LINE_NAME				= "${fn:replace(item.LINE_NAME, '"', '&quot;')}"			<%-- 라인명 --%>
			EQUIPMENT				= "${item.EQUIPMENT}"										<%-- 설비ID --%>
			EQUIPMENT_UID			= "${fn:replace(item.EQUIPMENT_UID, '"', '&quot;')}"		<%-- 설비코드 --%>
			EQUIPMENT_NAME			= "${fn:replace(item.EQUIPMENT_NAME, '"', '&quot;')}"		<%-- 설비명 --%>
			HISTORY_TYPE			= "${item.HISTORY_TYPE}"									<%-- 이력유형(01:설비등록,02:라인이동,11:일반정비,12:예방정비,13:돌발정비,21:CBM,22:TBM) --%>
			DATE_HISTORY			= "${item.DATE_HISTORY}"									<%-- 수행일 --%>
			HISTORY_DESCRIPTION		= "${fn:replace(item.HISTORY_DESCRIPTION, '"', '&quot;')}"	<%-- 내역 --%>
			DETAIL_ID				= "${item.DETAIL_ID}"										<%-- 상세ID --%>
			
			<%-- 상세정보(정비실적ID 및 점검실적ID) --%>
			<c:if test='${item.DETAIL_ID ne ""}'>
				<c:choose>
					<c:when test='${fn:contains("11,12,13", item.HISTORY_TYPE)}'>
						DETAIL_ID				= "${item.DETAIL_ID}"
			 			DETAIL_IDSwitch="1"  DETAIL_IDIcon="/images/com/web/commnet_up.gif" 
						DETAIL_IDOnClickSideIcon="fnOpenLayerWithParam('/epms/repair/result/popRepairResultForm.do?REPAIR_RESULT='+Row.DETAIL_ID,'popRepairResultForm');"
					</c:when>
					<c:when test='${fn:contains("21,22", item.HISTORY_TYPE)}'>
						DETAIL_ID				= "${item.DETAIL_ID}"
			 			DETAIL_IDSwitch="1"  DETAIL_IDIcon="/images/com/web/commnet_up.gif" 
						DETAIL_IDOnClickSideIcon="fnOpenLayerWithParam('/epms/preventive/result/popPreventiveResultForm.do?PREVENTIVE_RESULT='+Row.DETAIL_ID,'popPreventiveResultForm');"
					</c:when>
				</c:choose>
 			</c:if>
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>