<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld"                 %>
<%@ taglib prefix="c"	   uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: popEquipmentHistoryList.jsp
	Description : 기기이력 팝업 화면
    author		: 서정민
    since		: 2018.02.07
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.02.07	 서정민		최초 생성

--%>

<script type="text/javascript">	
$(document).ready(function(){
	
});
</script>

<input type="hidden" id="popEquipmentHistoryList_EQUIPMENT" name="EQUIPMENT"/>

<div class="modal fade modalFocus" id="popEquipmentHistoryList" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-55" style="min-width:970px"> <%-- 원하는 width 값 입력 --%>				
		 <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title"><spring:message code='epms.equipment.HISTORY.REFER' /></h4><!-- 기기이력 -->
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
					
					<div class="modal-section">
						 <div class="fl-box panel-wrap-modal wd-per-100">
							<div class="ab wrap-inq mgn-l-10">
								<ul class="wrap-inq">
									<li class="inq-clmn">
										<h4 class="tit-inq"><spring:message code='epms.location' /> : </h4><!-- 공장 -->
										<h4 class="tit-inq" id="popEquipmentHistoryList_LOCATION_NAME"></h4>
									</li>
									<li class="inq-clmn">
										<h4 class="tit-inq"><spring:message code='epms.category' /> : </h4><!-- 라인 -->
										<h4 class="tit-inq" id="popEquipmentHistoryList_LINE_NAME"></h4>
									</li>
									<li class="inq-clmn">
										<h4 class="tit-inq"><spring:message code='epms.system.uid' /> : </h4><!-- 설비코드 -->
										<h4 class="tit-inq" id="popEquipmentHistoryList_EQUIPMENT_UID"></h4>
									</li>
									<li class="inq-clmn">
										<h4 class="tit-inq"><spring:message code='epms.system.name' /> : </h4><!-- 설비명 -->
										<h4 class="tit-inq" id="popEquipmentHistoryList_EQUIPMENT_NAME"></h4>
									</li>
								</ul>
							</div>		
							<div class="panel-body" id="EquipmentHistoryList">	
								<bdo	Debug      = "Error"
										Data_Url   = ""
										Layout_Url = "/epms/equipment/history/equipmentHistoryListLayout.do"
										Export_Url = "/sys/comm/exportGridData.jsp?File=EquipmentHistoryList.xls&dataName=data"  Export_Data ="data" Export_Type="xls"
								></bdo>
							</div>
						</div>
					</div>
					
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>



