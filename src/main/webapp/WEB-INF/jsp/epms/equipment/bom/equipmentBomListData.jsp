<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%--
	Class Name	: equipmentMasterList.jsp
	Description : [설비BOM 조회],[설비BOM 관리] : 설비BOM 그리드 데이터
    author		: 서정민
    since		: 2018.02.09
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.02.09	 서정민		최초 생성

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I	
			<c:if test="${item.TYPE eq 'I'}">
				CanSelect = "0"
			</c:if>
			TYPE					= "${item.TYPE}"
			<c:if test="${item.CUR_LVL eq 1}">
				Background="#fffbe6"
				CanSelect = "0"
			</c:if>
			OBJECT					= "${item.OBJECT}"
			OBJTXT					= "${fn:replace(item.OBJTXT, '"', '&quot;')}"
		
			EQUIPMENT_BOM			= "${item.EQUIPMENT_BOM}"
			
			LOCATION				= "${item.LOCATION}"
			LINE					= "${item.LINE}"
			LINE_UID				= "${item.LINE_UID}"
			LINE_NAME				= "${fn:replace(item.LINE_NAME, '"', '&quot;')}"
			
			EQUIPMENT				= "${item.EQUIPMENT}"
			EQUIPMENT_UID			= "${item.EQUIPMENT_UID}"
			EQUIPMENT_NAME			= "${fn:replace(item.EQUIPMENT_NAME, '"', '&quot;')}"
			
			MATERIAL				= "${item.MATERIAL}"
			MATERIAL_TYPE			= "${item.MATERIAL_TYPE}"
			MATERIAL_TYPE_NAME		= "${fn:replace(item.MATERIAL_TYPE_NAME, '"', '&quot;')}"
			MATERIAL_UID			= "${item.MATERIAL_UID}"
			MATERIAL_NAME			= "${fn:replace(item.MATERIAL_NAME, '"', '&quot;')}"
			MODEL					= "${fn:replace(item.MODEL, '"', '&quot;')}"
			SPEC					= "${fn:replace(item.SPEC, '"', '&quot;')}"
			COUNTRY					= "${item.COUNTRY}"
			MANUFACTURER			= "${fn:replace(item.MANUFACTURER, '"', '&quot;')}"
			MEMO					= "${fn:replace(item.MEMO, '"', '&quot;')}"
			UNIT					= "${item.UNIT}"
			UNIT_NAME				= "${item.UNIT_NAME}"
			STOCK_OPTIMAL			= "${item.STOCK_OPTIMAL}"
			<c:choose>
				<c:when test="${item.STOCK_OPTIMAL > 0}">
					STOCK					= "${item.STOCK}"
					STOCK_CONDITION			= "${item.STOCK_CONDITION}"
					<c:if test="${item.STOCK_CONDITION == '1'}">
						STOCK_CONDITIONClass="gridStatus3"
					</c:if>
					<c:if test="${item.STOCK_CONDITION == '2'}">
						STOCKClass="gridStatus4"
						STOCK_CONDITIONClass="gridStatus4"
					</c:if>
				</c:when>
				<c:otherwise>
					STOCK					= ""
					STOCK_CONDITION			= ""
				</c:otherwise>
			</c:choose>
			
			HISTORY_INSwitch="1"  HISTORY_INIcon="/images/com/web/receiving_icon.gif" 
			HISTORY_INOnClickSideIcon="fnOpenLayerWithGrid('/epms/material/master/popMaterialInOutView.do?MATERIAL='+Row.MATERIAL,'popMaterialInputView');"
			HISTORY_OUTSwitch="1"  HISTORY_OUTIcon="/images/com/web/shipping_icon.gif" 
			HISTORY_OUTOnClickSideIcon="fnOpenLayerWithGrid('/epms/material/master/popMaterialInOutView.do?MATERIAL='+Row.MATERIAL,'popMaterialOutputView');"
			
			DETAILSwitch="1"  DETAILIcon="/images/com/web/commnet_up.gif" 
			DETAILOnClickSideIcon="fnOpenLayerWithParam('/epms/material/master/popMaterialMasterDtl.do?MATERIAL='+Row.MATERIAL,'popMaterialMasterDtl');"
			
			DEL_YN							= "${item.DEL_YN}"
			<c:if test='${item.DEL_YN eq "N"}'>
				DEL_YNClass="gridStatus3"
			</c:if>
			<c:if test='${item.DEL_YN eq "Y"}'>
				DEL_YNClass="gridStatus4"
			</c:if>
			DEL_YN2							= "${item.DEL_YN2}"
			<c:if test='${item.DEL_YN2 eq "N"}'>
				DEL_YN2Class="gridStatus3"
			</c:if>
			<c:if test='${item.DEL_YN2 eq "Y"}'>
				DEL_YN2Class="gridStatus4"
			</c:if>
			
		<c:choose>
			<c:when test="${item.CUR_LVL eq item.NEXT_LVL}">></I></c:when>
			<c:when test="${item.CUR_LVL < item.NEXT_LVL}">CanExpand='1' Expanded='1'></c:when>
			<c:when test="${item.CUR_LVL > item.NEXT_LVL}">></c:when>
			<c:otherwise>></c:otherwise>
		</c:choose>
		<c:set var="nodeCnt" value="${item.CUR_LVL - item.NEXT_LVL}" />
		<c:if test="${nodeCnt > 0}">
			<c:forEach var="item" begin="0" end="${nodeCnt}" step="1"></I></c:forEach>
		</c:if>
	</c:forEach> 
	</B>
</Body>
</Grid>