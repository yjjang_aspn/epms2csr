<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"	   uri="http://java.sun.com/jstl/fmt"           %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: equipmentMasterList.jsp
	Description : [설비BOM 조회] : 화면
    author		: 서정민
    since		: 2018.02.09
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.02.09	 서정민		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<script type="text/javascript">

<%-- 전역변수 --%>
var focusedRow = null;		<%-- Focus Row : [라인] --%>
var focusedRow2 = null;		<%-- Focus Row : [설비] --%>
var focusedRow3 = null;		<%-- Focus Row : [설비BOM] --%>
var callLoadingYn = true;	<%-- 로딩바 호출여부 --%>

$(document).ready(function(){
	<%-- 트리그리드 3개일 때 화면여닫기 실행  --%>
	treeslideLeft_0301();
});

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "CategoryList") {				<%-- 1. [라인] 클릭시 --%>
		
		if (focusedRow == null || focusedRow.CATEGORY != row.CATEGORY || focusedRow2 != null){
			
			<%-- 1-1. 포커스 --%>
			if(focusedRow != null){
				grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
			}
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			
			focusedRow = row;
			focusedRow2 = null;
			focusedRow3 = null;
			
			<%-- 1-2. 설비 목록 조회 --%>
			$('#CATEGORY_TYPE').val(row.CATEGORY_TYPE);	<%-- 카테고리 유형 : 'LINE' --%>
			$('#TREE').val(row.TREE);					<%-- 구조체 --%>
			$('#LOCATION').val(row.LOCATION);			<%-- 위치(공장)ID --%>
			getEquipmentList();
			
			<%-- 1-3. 설비BOM 목록 조회 --%>
			$('#EQUIPMENT').val("");					<%-- 설비 ID --%>
			<%-- $('#equipmentBomListTitle').html("설비BOM : " + "[ " + row.CATEGORY_NAME + " ]"); --%>
		}
	}
	else if(grid.id == "EquipmentList") {		<%-- 2. [설비] 클릭시 --%>
		
		if (focusedRow2 == null || focusedRow2.EQUIPMENT != row.EQUIPMENT){
			
			<%-- 2-1. 포커스 --%>
			if(focusedRow2 != null){
				grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
			}
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			
			focusedRow2 = row;
			focusedRow3 = null;
			
			<%-- 2-2. 설비 BOM 목록 조회 --%>
			$('#EQUIPMENT').val(row.EQUIPMENT);			<%-- 설비BOM  설비 ID --%>
			$('#equipmentBomListTitle').html("<spring:message code='epms.system.bom' /> : " + "[ " + row.LINE_NAME + " ] - </br> [ " + row.EQUIPMENT_UID + " / " + row.EQUIPMENT_NAME + " ]");	<%-- 설비BOM 타이틀 변경 --%>
			
			getEquipmentBomList();
		}
	}
	else if(grid.id == "EquipmentBomList") {	<%-- 3. [설비BOM] 클릭시 --%>
	
		<%-- 3-1. 포커스  --%>
		if(focusedRow3 != null){
			grid.SetAttribute(focusedRow3,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow3 = row;		
	}
}

<%-- 선택된 라인에 해당되는 설비 조회 --%>
function getEquipmentList(){
	Grids.EquipmentList.Source.Data.Url = "/epms/equipment/master/equipmentMasterListData.do?TREE=" + $('#TREE').val()
										 + "&CATEGORY_TYPE=" + $('#CATEGORY_TYPE').val();
	Grids.EquipmentList.ReloadBody();
}

<%-- 선택된 설비에 해당되는 설비BOM 조회 --%>
function getEquipmentBomList(){
	Grids.EquipmentBomList.Source.Data.Url = "/epms/equipment/bom/equipmentBomListData.do?TREE=" + $('#TREE').val()
											+ "&EQUIPMENT=" + $('#EQUIPMENT').val()
											+ "&LOCATION=" + $('#LOCATION').val()
											+ "&editYn=${editYn}";
	Grids.EquipmentBomList.ReloadBody();
}

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source, data, func){
	
	if(grid.id == "EquipmentList"){			<%-- [설비] 목록 조회시 --%>
		if(data != ""){
			callLoadingBar();
			callLoadingYn = false;
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "EquipmentList"){
					setTimeout($.unblockUI, 100);
					callLoadingYn = true;
					<%-- getEquipmentBomList(); --%>
				}
			}
		}
	}
	else if(grid.id == "EquipmentBomList"){	<%-- [설비BOM] 목록 조회시 --%>
		if(data != ""){
			if(callLoadingYn == true){
				callLoadingBar();
				callLoadingYn = false;
			}
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "EquipmentBomList"){
					setTimeout($.unblockUI, 100);
					callLoadingYn = true;
				}
			}
		}
	}
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- 그리드와 Param 값을 동시에 사용 --%>
function fnOpenLayerWithGrid(url, name){
	$.ajax({
		type: 'POST',
		url: url,
		dataType: 'json',
		processData: false, 
		contentType:false,
		success: function (json) {
			
			if(name == "PopEquipmentBomMng") {
				
			}
			else if(name == "popMaterialInputView") {
				
				$("#MATERIAL").val(json.MATERIAL);
				$("#"+name+"_startDt").val(isEmpty(json.startDt) == true ? '' : json.startDt);
				$("#"+name+"_endDt").val(  isEmpty(json.endDt)   == true ? '' : json.endDt);
				$("#"+name+"_MATERIAL_TYPE").text(isEmpty(json.item.MATERIAL_TYPE_NAME) == true ? '-' : "["+json.item.MATERIAL_TYPE_NAME+"]");
				$("#"+name+"_MATERIAL_GRP1").text(isEmpty(json.item.MATERIAL_GRP1_NAME) == true ? '-' : "["+json.item.MATERIAL_GRP1_NAME+"]");
				$("#"+name+"_MATERIAL_UID").text( isEmpty(json.item.MATERIAL_UID)       == true ? '-' : "["+json.item.MATERIAL_UID+"]");
				$("#"+name+"_MATERIAL_NAME").text(isEmpty(json.item.MATERIAL_NAME)      == true ? '-' : "["+json.item.MATERIAL_NAME+"]");
				
				Grids.MaterialInList.Source.Data.Url = "/epms/material/input/materialInputListData.do?MATERIAL=" + $('#MATERIAL').val()
				 + "&startDt=" + $('#popMaterialInputView_startDt').val()
				 + "&endDt=" + $('#popMaterialInputView_endDt').val();
				
				Grids.MaterialInList.ReloadBody();
			} else if(name == "popMaterialOutputView") {
				
				$("#MATERIAL").val(json.MATERIAL);
				$("#"+name+"_startDt").val(isEmpty(json.startDt) == true ? '' : json.startDt);
				$("#"+name+"_endDt").val(  isEmpty(json.endDt)   == true ? '' : json.endDt);
				$("#"+name+"_MATERIAL_TYPE").text(isEmpty(json.item.MATERIAL_TYPE_NAME) == true ? '-' : "["+json.item.MATERIAL_TYPE_NAME+"]");
				$("#"+name+"_MATERIAL_GRP1").text(isEmpty(json.item.MATERIAL_GRP1_NAME) == true ? '-' : "["+json.item.MATERIAL_GRP1_NAME+"]");
				$("#"+name+"_MATERIAL_UID").text( isEmpty(json.item.MATERIAL_UID)       == true ? '-' : "["+json.item.MATERIAL_UID+"]");
				$("#"+name+"_MATERIAL_NAME").text(isEmpty(json.item.MATERIAL_NAME)      == true ? '-' : "["+json.item.MATERIAL_NAME+"]");
				
				Grids.MaterialOutList.Source.Data.Url = "/epms/material/output/materialOutputListData.do?MATERIAL=" + $('#MATERIAL').val()
				 + "&startDt=" + $('#popMaterialOutputView_startDt').val()
				 + "&endDt=" + $('#popMaterialOutputView_endDt').val();
				 
				Grids.MaterialOutList.ReloadBody();
			}
			
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
			loadEnd();
		}
	});
	
	fnModalToggle(name);
	
}

</script>
</head>

<body>
<!-- 화면 UI Area Start  -->
<div id="contents">
	<input type="hidden" id="CATEGORY_TYPE" name="CATEGORY_TYPE"/>		<%-- 카테고리 유형 --%>
	<input type="hidden" id="TREE" name="TREE"/>						<%-- 구조체 --%>
	<input type="hidden" id="EQUIPMENT" name="EQUIPMENT"/>				<%-- 설비 ID --%>
	<input type="hidden" id="LOCATION" name="LOCATION"/>				<%-- 설비 ID --%>
	<input type="hidden" id="MATERIAL" name="MATERIAL"/>				<%-- 자재ID --%>
	
	
	<div class="fl-box panel-wrap03 slideWidth rel" style="width:20%; height:100%;">
		<div class="slideArea">
			<h5 class="panel-tit"><spring:message code='epms.category' /></h5><!-- 라인 -->
			<div class="panel-body">
				<div id="categoryList" class="slideTreeGrid">
					<bdo	Debug="Error"
							Data_Url="/sys/category/categoryData.do?CATEGORY_TYPE=LINE"
							Layout_Url="/sys/category/categoryNotEditLayout.do?CATEGORY_TYPE=LINE"
					>
					</bdo>
				</div>
			</div>
		</div>
		<a href="#none" class="slideBtn ir-pm" title="닫기" style="left:100px;">토글</a>
	</div>
	
	<div class="fl-box panel-wrap03 slideWidth rel" style="width:20%; height:100%;">
		<div class="slideArea">
			<h5 class="panel-tit mgn-l-10" id="equipmentListTitle"><spring:message code='epms.system' /></h5><!-- 설비 -->
			<div class="panel-body mgn-l-10">
				<div id="equipmentList" class="slideTreeGrid">
					<bdo	Debug="Error"
							Data_Url=""
							Layout_Url="/epms/equipment/bom/equipmentListLayout.do?editYn=${editYn}"
							Export_Data="data" Export_Type="xls"
							Export_Url="/sys/comm/exportGridData.jsp?File=EquipmentList.xls&dataName=data"
					>
					</bdo>
				</div>
			</div>
		</div>
		<a href="#none" class="slideBtn type02 ir-pm" title="닫기" style="left:100px;">토글</a>
	</div>
	 
	<div class="fl-box panel-wrap03" style="width:59.5%; height:100%;" id="slideWidthRight">
		<h5 class="panel-tit mgn-l-10" id="equipmentBomListTitle"><spring:message code='epms.system.bom' /></h5><!-- 설비BOM -->
		<div class="panel-body mgn-l-10">
			<div id="equipmentBomList">
				<bdo	Debug="Error"
		 				Data_Url=""
						Layout_Url="/epms/equipment/bom/equipmentBomListLayout.do"
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=EquipmentBomList.xls&dataName=data"
						>
				</bdo>
			</div>
		</div>
	</div>
</div>

<%-- 입고조회 --%>
<%@ include file="/WEB-INF/jsp/epms/material/master/popMaterialInputView.jsp"%>	

<%-- 출고조회 --%>
<%@ include file="/WEB-INF/jsp/epms/material/master/popMaterialOutputView.jsp"%>	

<%-- 상세내역 조회 --%>
<div class="modal fade modalFocus" id="popMaterialMasterDtl" data-backdrop="static" data-keyboard="false"></div>	

</body>
</html>

