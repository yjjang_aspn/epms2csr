<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld"                 %>
<%@ taglib prefix="c"	   uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: popEquipmentBomMng.jsp
	Description : [설비BOM 관리] : 설비BOM 등록 팝업
    author		: 서정민
    since		: 2018.02.09
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.02.09	 서정민		최초 생성

--%>

<html>
<head>

<script type="text/javascript">

$(document).ready(function() {
	
});

<%-- 선택된 라인에 해당되는 설비 조회 --%>
function fnGetMaterialList(){
	Grids.PopEquipmentBomMng.Source.Data.Url = "/epms/material/master/materialMasterListData.do?TREE=" + $('#POP_TREE').val()
											 + "&CATEGORY=" + $('#POP_CATEGORY').val()
											 + "&CATEGORY_TYPE=" + $('#POP_CATEGORY_TYPE').val()
											 + "&editYn=N";
	Grids.PopEquipmentBomMng.ReloadBody();	
}

</script>
</head>

<body>
<input type="hidden" id="POP_CATEGORY"      name="POP_CATEGORY"/>
<input type="hidden" id="POP_CATEGORY_TYPE" name="POP_CATEGORY_TYPE"/>
<input type="hidden" id="POP_TREE"          name="POP_TREE"/>

<div class="modal fade modalFocus" id="PopEquipmentBomMng" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-80">			
		 <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">
			   	    <spring:message code='epms.system.bom' /> <spring:message code='epms.regist' /> 
			   	</h4><!-- 설비BOM 등록 -->
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	           		
	           		<div class="modal-section">
						<div class="fl-box panel-wrap-modal wd-per-20">
							<h5 class="panel-tit"><spring:message code='epms.object.type' /></h5><!-- 자재유형 -->
							<div class="panel-body" id="popCategoryList">
								<bdo	Debug="Error"
										Data_Url="/sys/category/categoryData.do?CATEGORY_TYPE=MATERIAL"
										Layout_Url="/sys/category/categoryNotEditLayout.do?gridId=PopCategoryList&CATEGORY_TYPE=MATERIAL"
										Upload_Url="/sys/category/categoryEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"
								>
								</bdo>
							</div>
						</div>
						
						<div class="fl-box panel-wrap-modal wd-per-80">
							<h5 class="panel-tit mgn-l-10" id="materialMasterListTitle"><spring:message code='epms.object' /></h5>	<!-- 자재 -->		
							<div class="panel-body mgn-l-10" id="popEquipmentBomMng">
								<bdo	Debug="Error"
										Layout_Url="/epms/equipment/bom/popEquipmentBomMngLayout.do"
										Upload_Url="/epms/equipment/bom/equipmentBomEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"
									>
								</bdo>
							</div>
						</div>
					</div>
					
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>
</body>
</html>