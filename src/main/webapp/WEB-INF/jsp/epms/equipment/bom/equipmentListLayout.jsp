<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld"                 %>
<%@ taglib prefix="c"	   uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: equipmentListLayout.jsp
	Description : [설비BOM 조회],[설비BOM 관리] : 설비 그리드 레이아웃
    author		: 서정민
    since		: 2018.02.09
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.02.09	 서정민		최초 생성

--%>

<c:set var="gridId" value="EquipmentList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		Filtering="1"				Dragging="0"
			Editing="0"				Deleting="0"							SuppressCfg="0"				StandardFilter="3"
			CopyCols="0"			PasteFocused="3"	CustomScroll="1"
	/>
	<c:if test="${equipment_hierarchy eq 'true' }">
		<Cfg	MainCol="EQUIPMENT_NAME" />
	</c:if>

	<c:choose>
		<c:when test="${editYn eq 'Y'}">
			<Cfg Selecting="1"/>
		</c:when>
		<c:otherwise>
			<Cfg Selecting="1"/>
		</c:otherwise>
	</c:choose>
	<Head>
		<Header	id="Header"	Align="center" CanExport="1" CanHide="0"
			LOCATION				= "<spring:message code='epms.location'          />"   <%  //   위치          %>
			LINE					= "<spring:message code='epms.category.uid'      />"   <%  //   기능위치     %>
			LINE_NAME				= "<spring:message code='epms.category'          />"   <%  //   라인          %>
			EQUIPMENT				= "<spring:message code='epms.system.id'         />"   <%  //   설비ID    %>
			EQUIPMENT_UID			= "<spring:message code='epms.system.uid'        />"   <%  //   설비코드      %>
			EQUIPMENT_NAME			= "<spring:message code='epms.system.name'       />"   <%  //   설비명        %>
			GRADE					= "<spring:message code='epms.system.grade'      />"   <%  //   설비등급      %>
			MATERIAL_GRP			= "<spring:message code='epms.material.group.id' />"   <%  //   장치ID    %>
			MODEL					= "<spring:message code='epms.model'             />"   <%  //   모델명        %>
		/>
		<Filter	id="Filter" CanExport="1" CanHide="0" CanFocus="1"/>
	</Head>
	
	<Panel CanHide="0"/>
	
	<Cols>
		<C Name="LOCATION"					Type="Enum" Align="left" Visible="0" Width="100" CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>	
		<C Name="LINE"						Type="Text" Align="left" Visible="0" Width="100" CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="LINE_NAME"					Type="Text" Align="left" Visible="0" Width="100" CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="EQUIPMENT"					Type="Text" Align="left" Visible="0" Width="100" CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="EQUIPMENT_UID"				Type="Text" Align="left" Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT_NAME"			Type="Text" Align="left" Visible="1" Width="120" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="120"/>
		<C Name="GRADE"						Type="Text" Align="left" Visible="0" Width="70"  CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="MODEL"						Type="Text" Align="left" Visible="0" Width="100" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
	</Cols>
	
	<c:choose>
		<c:when test="${editYn eq 'Y'}">
			<c:set var="Cells" value="Empty,BomMapping,항목"/>
		</c:when>
		<c:otherwise>
			<c:set var="Cells" value="Empty,BomMapping,항목"/>
		</c:otherwise>
	</c:choose>
	<Toolbar	Space="0"	Styles="2"	Cells="${Cells}"
				EmptyType="Html"	EmptyWidth="1" Empty="        " 
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
<%-- 				접기Type="Html" 접기="&lt;a href='#none' title='Folding' id=&quot;Folding02&quot; class=&quot;icon folderClose&quot; --%>
<%-- 					onclick='fnExpandAll02(&quot;${gridId}&quot;)'>&lt;/a>" --%>
				BomMappingType="Html" BomMapping="&lt;a href='#none' title='BOM매핑' class=&quot;treeButton treeSelect&quot;
					onclick='fnMapping(&quot;${gridId}&quot;)'>BOM매핑&lt;/p>"
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton01 icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
	
	
</Grid>