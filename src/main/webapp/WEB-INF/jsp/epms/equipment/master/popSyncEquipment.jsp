<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">

var submitBool = true;

$(document).ready(function() {
	
	<%-- '확인' 버튼 --%>
	$('#okBtn').on('click', function(){
		if (submitBool == true) {
			submitBool = false;
			var sapSync     = $("input:radio[name=sapSync]:checked").val();
			fnSyncEquipment(sapSync);
		} else {
			return false;
		}
	});
				
});

</script>

<!-- Layer PopUp Start -->
<div class="modal-dialog root" style="width:450px;"> <!-- 원하는 width 값 입력 -->						
	 <div class="modal-content" style="height:220px;">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title">SAP 동기화 설정</h4>
		</div>
	 	<div class="modal-body">
           	<div class="modal-bodyIn">

				<div class="tb-wrap">
					<table class="tb-st">
						<caption class="screen-out">SAP 동기화 설정</caption>
						<colgroup>
							<col width="30%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row">동기화</th>
								<td>
									<div class="tb-row">
										<span class="rdo-st">
											<input type="radio" name="sapSync" id="sapSync_all" value="all">
											<label for="sapSync_all" class="label">전체 동기화</label>
										</span>
										<span class="rdo-st mgn-l-50">
											<input type="radio" name="sapSync" id="sapSync_OnlyUpdate" value="onlyUpdate" checked >
											<label for="sapSync_OnlyUpdate">수정건 동기화</label>
										</span>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>	
		<div class="modal-footer hasFooter">
			<a href="#none" id="cancelBtn" class="btn comm st02" data-dismiss="modal">취소</a>
			<a href="#none" id="okBtn" class="btn comm st01">확인</a>
		</div>
	</div>		
</div>
<!-- 좌측 : e -->
<!-- Layer PopUp End -->