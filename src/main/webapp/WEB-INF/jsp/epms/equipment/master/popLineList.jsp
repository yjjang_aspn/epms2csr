<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: popLineList.jsp
	Description : 라인변경 팝업 화면
    author		: 서정민
    since		: 2018.02.07
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.02.07	 서정민		최초 생성

--%>

<script type="text/javascript">

$( document ).ready(function() {
	
});

//클릭 이벤트
Grids.OnDblClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "popLineList") {
		fnSetLine(row.CATEGORY, row.CATEGORY_UID, row.CATEGORY_NAME);
		fnModalToggle("popLineList");
	}
}
</script>

<div class="modal fade modalFocus" id="popLineList" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-25"> <%-- 원하는 width 값 입력 --%>				
		 <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">라인변경</h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	           	
					 <div class="modal-section">

						 <div class="fl-box panel-wrap-modal wd-per-100">
							<div class="panel-body">
								<bdo	Debug="Error" 
										Data_Url="/sys/category/categoryData.do?CATEGORY_TYPE=LINE"
										Layout_Url="/sys/category/categoryNotEditLayout.do?CATEGORY_TYPE=LINE&gridId=popLineList"
										Export_Data= "data" Export_Type="xls"
										Export_Url = "/sys/comm/exportGridData.jsp?File=PopLineList.xls&dataName=data"
									>
								</bdo>
							</div>	
						</div>
						
					 </div>
					
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>



