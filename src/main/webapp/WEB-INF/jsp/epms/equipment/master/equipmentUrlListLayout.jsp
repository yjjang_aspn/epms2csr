<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld"                 %>
<%@ taglib prefix="c"	   uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: equipmentUrlListLayout.jsp
	Description : 설비 관련 URL 그리드 레이아웃
    author		: 서정민
    since		: 2018.04.13
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.02.07	 서정민		최초 생성

--%>

<c:set var="gridId" value="EquipmentUrlList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1" 			SuppressCfg="0"
			Editing="1"				Deleting="0"		Selecting="0"		Dragging="1"
			Filtering="1"			StandardFilter="3"	ClearSelected="2"
			Paging="1"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="3"			PasteFocused="3"	
	/>
	
	

	<Head>
		<Header	id="Header"	Align="center" CanExport="1" CanHide="0"
			EQUIPMENT				= "<spring:message code='epms.system.id'       />"  <%  // 설비ID    %>
			EQUIPMENT_URL			= "<spring:message code='epms.system.url.id'   />"  <%  // URL ID   %>
			URL_DESCRIPTION			= "<spring:message code='epms.url.description' />"  <%  // URL 설명   %>
			URL_ADDRESS				= "<spring:message code='epms.url.address'     />"  <%  // URL 주소   %>
			SEQ_DSP					= "<spring:message code='epms.display.order'   />"  <%  // 출력순서     %>
			DEL_YN					= "<spring:message code='epms.del.yn'          />"  <%  // 삭제여부     %>
		/>
		<Filter	id="Filter" CanExport="1" CanHide="0" CanFocus="1"/>
	</Head>
	
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4" CanHide="0"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	
	<Pager Visible="0" CanHide="0"/>
	<Panel CanHide="0"/>
	
	<Cols>
		<C Name="EQUIPMENT"					Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="EQUIPMENT_URL"				Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="SEQ_DSP"	 				Type="Text" Align="center" Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="0"/>
		<c:choose>
			<c:when test="${param.editYn eq 'Y' }">
				<C Name="URL_DESCRIPTION"			Type="Text" Align="left"   Visible="1" Width="200" CanEdit="1" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="200"/>
				<C Name="URL_ADDRESS"				Type="Text" Align="left"   Visible="1" Width="200" CanEdit="1" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="200"/>
				<C Name="DEL_YN"					Type="Enum" Align="left"   Visible="1" Width="70"  CanEdit="1" CanExport="1" CanHide="1" <tag:enum codeGrp="DEL_YN"/>/>
			</c:when>
			<c:otherwise>
				<C Name="URL_DESCRIPTION"			Type="Text" Align="left"   Visible="1" Width="200" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="200"/>
				<C Name="URL_ADDRESS"				Type="Text" Align="left"   Visible="1" Width="200" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="200"/>
				<C Name="DEL_YN"					Type="Enum" Align="left"   Visible="0" Width="70"  CanEdit="0" CanExport="0" CanHide="0" <tag:enum codeGrp="DEL_YN"/>/>
			</c:otherwise>
		</c:choose>
	</Cols>
	
	<c:choose>
		<c:when test="${param.editYn eq 'Y' }">
			<c:set var="Cells" value="Empty,Cnt,추가,저장,항목,새로고침,인쇄,엑셀"/>
		</c:when>
		<c:otherwise>
			<c:set var="Cells" value="Empty,Cnt,새로고침,인쇄,엑셀"/>
		</c:otherwise>
	</c:choose>
	<Toolbar	Space="0"	Styles="2"	Cells="${Cells}"
				EmptyType="Html"	EmptyWidth="1" Empty="        " 
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				추가Type="Html" 추가="&lt;a href='#none' title='추가' class=&quot;defaultButton icon add&quot;
					onclick='addRowGrid(&quot;${gridId}&quot;)'>추가&lt;/a>"
				저장Type="Html" 저장="&lt;a href='#none' title='저장' class=&quot;treeButton treeSave&quot;
					onclick='fnSave(&quot;${gridId}&quot;)'>저장&lt;/a>"
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
				엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>