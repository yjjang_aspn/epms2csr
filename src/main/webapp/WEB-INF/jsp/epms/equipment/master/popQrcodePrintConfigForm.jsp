<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">

var popQrcodePrintConfigForm_submitBool = true;

$(document).ready(function() {
	
	popQrcodePrintConfigForm_submitBool = true;
	
	$("#popTitle").html("QR코드 인쇄 설정");
	
	<%-- '인쇄 미리보기' 버튼 --%>
	$('#popQrcodePrintConfigForm_printBtn').on('click', function(){
		if (popQrcodePrintConfigForm_submitBool == true) {
			fnQrPreview();
		} else {
			return false;
		}
	});
});

<%-- 인쇄 미리보기 --%>
function fnQrPreview(){
	popQrcodePrintConfigForm_submitBool = false;
	
	fnModalToggle('popQrcodePrintConfigForm');
// 	$('#popQrcodePrintConfigForm').modal(toggle);
	fnQrPrint($("input:radio[name=size]:checked").val());
}
	
</script>

<!-- Layer PopUp Start -->
<div class="modal-dialog root" style="width:450px;"> <!-- 원하는 width 값 입력 -->						
	 <div class="modal-content" style="height:200px;">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title">QR코드 인쇄 설정</h4>
		</div>
	 	<div class="modal-body">
           	<div class="modal-bodyIn">

				<div class="tb-wrap">
					<table class="tb-st">
						<caption class="screen-out">QR코드 인쇄 설정</caption>
						<colgroup>
							<col width="30%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row">크기</th>
								<td>
									<div class="tb-row">
										<span class="rdo-st">
											<input type="radio" name="size" id="A4_2" value="A4_2" checked >
											<label for="A4_2" class="label">A4 : 1/2</label>
										</span>
										<span class="rdo-st mgn-l-10">
											<input type="radio" name="size" id="A4_4" value="A4_4">
											<label for="A4_4">A4 : 1/4</label>
										</span>
										<span class="rdo-st mgn-l-10">
											<input type="radio" name="size" id="A4_8" value="A4_8">
											<label for="A4_8">A4 : 1/8</label>
										</span>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>	
		<div class="modal-footer hasFooter">
			<a href="#none" id="cancelBtn" class="btn comm st02" data-dismiss="modal">취소</a>
			<a href="#none" id="popQrcodePrintConfigForm_printBtn" class="btn comm st01">인쇄 미리보기</a>
		</div>
	</div>		
</div>
<!-- 좌측 : e -->
<!-- Layer PopUp End -->