<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core"       %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"     %>
<%--
	Class Name	: popequipmentDtl.jsp
	Description : 설비마스터 상세 팝업 화면
    author		: 서정민
    since		: 2018.04.13
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.04.13	 서정민		최초 생성

--%>
<!-- 설비마스터 : 설비정보 관리 (설비등록 팝업) -->
<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">
var submitBool = true;
var maxFileCnt = 30;

$(document).ready(function() {
	
	//파일 다운로드
	$('.btnFileDwn').on('click', function(){
		var ATTACH_GRP_NO = $(this).data("attachgrpno");
		var ATTACH = $(this).data("attach");
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO='+ATTACH_GRP_NO+'&ATTACH='+ATTACH;
		$("#fileDownFrame").attr("src",src);
	});
	
	//링크
	$('.btnLink').on('click', function(){
		var URL_ADDRESS = $(this).data("address");
		if(URL_ADDRESS.toLowerCase().indexOf('http://')<0 && URL_ADDRESS.toLowerCase().indexOf('https://')<0){
			URL_ADDRESS = 'http://' + URL_ADDRESS;
		}
		window.open(URL_ADDRESS, "_blank");
	});

});

function fnValidate() {
	submitBool = false;

	if (!isNull_J($('#LOCATION'), "<spring:message code='epms.equipment.alert.LOCATION' />" )) {
		submitBool = true;
		return false;
	}
	if (!isNull_J($('#CATEGORY'), "<spring:message code='epms.equipment.alert.CATEGORY' />" )) {
		submitBool = true;
		return false;
	}
	if (!isNull_J($('#EQUIPMENT_UID'), "<spring:message code='epms.equipment.alert.EQUIPMENT_UID' />" )) {
		submitBool = true;
		return false;
	}
	if (!isNull_J($('#EQUIPMENT_NAME'), "<spring:message code='epms.equipment.alert.EQUIPMENT_NAME' />" )) {
		submitBool = true;
		return false;
	}
	
	//총중량 "," 삭제
	$('#WEIGHT').val($('#WEIGHT').val().replace(/,/gi, ""));
	//취득가액 "," 삭제
	$('#ACQUISITION_VALUE').val($('#ACQUISITION_VALUE').val().replace(/,/gi,""));
	
	var fileCnt = $("input[name=fileGb]").length;
	if (fileCnt > 0) {
		$('#attachExistYn').val("Y");
	} else {
		$('#attachExistYn').val("N");
	}
	
	//fnSubmit("equipmentAddForm", "/project/equipmentMaster/admin/popEquipmentAddInsert.do");
	
	var form = new FormData(document.getElementById('equipmentAddForm'));
	
	$.ajax({
		type : 'POST',
		url : '/project/equipmentMaster/admin/popEquipmentAddInsert.do',
		dataType : 'json',
		data : form,
		async : false,
		processData : false,
		contentType : false,
		success : function(json) {	
			
			window.close();
			opener.fnReload($('#TREE').val());

		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert( "<spring:message code='common.msg.error' />" + "\n [ErrorCode] : " + textStatus );
			loadEnd();
		}
	});
}
 
//총중량,취득가액 콤마처리
var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
var rgx2 = /(\d+)(\d{3})/; 

function getNumber(obj) {
	if(obj.value.charAt(obj.value.length-1) == "."){
		var parts=obj.value.toString().split(".");
		parts[0] = parts[0].replace(rgx1,"");
		obj.value = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ".";
	}
	else{
		var parts=obj.value.toString().split(".");
		parts[0] = parts[0].replace(rgx1,"");
		obj.value = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
	}
}
 
//총중량,취득가액 숫자 유효성 검사 
function check_isNum_onkeydown(){
	
	$(this).val( $(this).val().replace(/[^-0-9.,]/gi,"") );
	$(this).val(number_format($(this).val()));
}

<%-- 이미지 원본크기 보기 --%>
function originFileView(img){ 
	img1= new Image(); 
	img1.src=(img); 
	imgControll(img); 
} 
	  
function imgControll(img){ 
	if((img1.width!=0)&&(img1.height!=0)){ 
		viewImage(img); 
	}else{ 
		controller="imgControll('"+img+"')"; 
		intervalID=setTimeout(controller,20); 
	} 
}

function viewImage(img){ 
	W = img1.width  ; 
	H = img1.height ; 
	O = "width=" + W + ",height=" + H + ",scrollbars=yes" ;
	
	var click = "<spring:message code='epms.equipment.IMG_DETAILVIEW' />" ;
	
	imgWin = window.open("","",O); 
	imgWin.document.write("<html><head><title><spring:message code='epms.equipment.IMG_DETAILVIEW' /></title></head>");
	imgWin.document.write("<body topmargin=0 leftmargin=0>");
	imgWin.document.write("<img src=" + img + " onclick='self.close()' style='cursor:pointer;' title ='" + click + "'>");
	imgWin.document.close();
}
</script>

<div class="modal-dialog root wd-per-55">				
	<div class="modal-content">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title"><spring:message code='epms.detail' /></h4><!-- 설비마스터 상세 -->
		</div>
		<div class="modal-body" >
			<div class="modal-bodyIn">
			
			
				<form id="equipmentAddForm" name="equipmentAddForm"  method="post" enctype="multipart/form-data">
					<input type="hidden" id="attachExistYn" name="attachExistYn" /> 
					<input type="hidden" id="MODULE" name="MODULE" value="11" />
					<input type="hidden" id="TREE" name="TREE" value="${equipmentDtl.TREE}"/>
					
					<div class="tb-wrap">
						<table class="tb-st"><!-- 설비마스터 상세 -->
							<caption class="screen-out"><spring:message code='epms.detail' /></caption>
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="*" />
								</colgroup>
								<tbody>				
									<tr>
										<th><spring:message code='epms.location' /></th><!-- 위치 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="hidden" class="inp-comm" title="<spring:message code='epms.equipment.LOCATION' />" readonly="readonly" id="LOCATION" name="LOCATION" value="<c:out value="${equipmentDtl.LOCATION}" escapeXml="true"/>"/>
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.LOCATION_NAME' />" readonly="readonly" id="location_name" name="location_name" value="<c:out value="${equipmentDtl.LOCATION_NAME}" escapeXml="true"/>"/>
											</div>
										</td>
										<th><spring:message code='epms.category' /></th><!-- 라인 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="hidden" class="inp-comm" readonly="readonly" title="<spring:message code='epms.equipment.LINE' />" id="LINE" name="LINE" value="<c:out value="${equipmentDtl.LINE}" escapeXml="true"/>"/>
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.LINE_NAME' />" readonly="readonly" id="LINE_NAME" name="LINE_NAME" value="${equipmentDtl.LINE_NAME} [${equipmentDtl.LINE_UID}]"/>
											</div>
										</td>
									</tr>
									<tr>
										<th><spring:message code='epms.asset.no' /></th><!-- 자산번호 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.ASSET_NO' />" readonly="readonly" id="ASSET_NO" name="ASSET_NO" value="<c:out value="${equipmentDtl.ASSET_NO}" escapeXml="true"/>"/>
											</div>
										</td>
										<th><spring:message code='epms.costcenter' /></th><!-- 코스트센터 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.COST_CENTER' />" readonly="readonly" id="COST_CENTER" name="COST_CENTER" value="<c:out value="${equipmentDtl.COST_CENTER}" escapeXml="true"/>"/>
											</div>
										</td>
									</tr>
									<tr>
										<th><spring:message code='epms.system.code' /></th><!-- 설비코드 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.uid' />" readonly="readonly" id="EQUIPMENT_UID" name="EQUIPMENT_UID" value="<c:out value="${equipmentDtl.EQUIPMENT_UID}" escapeXml="true"/>"/>
											</div>
										</td>
										<th><spring:message code='epms.system.name' /></th><!-- 설비명 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.name' />" readonly="readonly" id="EQUIPMENT_NAME" name="EQUIPMENT_NAME" value="<c:out value="${equipmentDtl.EQUIPMENT_NAME}" escapeXml="true"/>"/>
											</div>
										</td>
									</tr>
									<tr>
										<th><spring:message code='epms.model' /></th><!-- 모델명 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.MODEL' />" readonly="readonly" id="MODEL" name="MODEL" value="<c:out value="${equipmentDtl.MODEL}" escapeXml="true"/>"/>
											</div>
										</td>
										<th><spring:message code='epms.sortfield' /></th><!-- 정렬필드 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.FIELD' />" readonly="readonly" id="FIELD" name="FIELD" value="<c:out value="${equipmentDtl.FIELD}" escapeXml="true"/>"/>
											</div>
										</td>
									</tr>
									<tr>
										<th><spring:message code='epms.qnty' /></th><!-- 총중량 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.WEIGHT' />" readonly="readonly" id="WEIGHT" name="WEIGHT" value="<c:out value="${equipmentDtl.WEIGHT}" escapeXml="true"/>" style="text-align: right;" onchange="getNumber(this);" onkeyup="getNumber(this);"		onkeydown="check_isNum_onkeydown();" autocomplete="off"/>
											</div>
										</td>
										<th><spring:message code='epms.unit' /></th><!-- 중량단위 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="hidden" class="inp-comm" title="<spring:message code='epms.equipment.WEIGHT_UNIT' />" readonly="readonly" id="WEIGHT_UNIT" name="WEIGHT_UNIT" value="${equipmentDtl.WEIGHT_UNIT}"/>
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.WEIGHT_UNIT_NAME' />" readonly="readonly" id="WEIGHT_UNIT_NAME" name="WEIGHT_UNIT_NAME" value="${equipmentDtl.WEIGHT_UNIT_NAME}"/>
											</div>
										</td>
									</tr>
									<tr>
										<th><spring:message code='epms.amount' /></th><!-- 취득가액 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.ACQUISITION_VALUE' />" readonly="readonly" id="ACQUISITION_VALUE" name="ACQUISITION_VALUE" value="<c:out value="${equipmentDtl.ACQUISITION_VALUE}" escapeXml="true"/>" style="text-align: right;" onchange="getNumber(this);" onkeyup="getNumber(this);"	onkeydown="check_isNum_onkeydown();" autocomplete="off"/>
											</div>
										</td>
										<th><spring:message code='epms.currency' /></th><!-- 통화 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="hidden" class="inp-comm" title="<spring:message code='epms.equipment.CURRENCY' />" readonly="readonly" id="CURRENCY" name="CURRENCY" value="${equipmentDtl.CURRENCY}"/>
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.CURRENCY_NAME' />" readonly="readonly" id="CURRENCY_NAME" name="CURRENCY_NAME" value="${equipmentDtl.CURRENCY_NAME}"/>
											</div>
										</td>
									</tr>
									<tr>
										<th><spring:message code='epms.date.acquire' /></th><!-- 취득일 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.DATE_ACQUISITION' />" readonly="readonly" id="DATE_ACQUISITION" name="DATE_ACQUISITION" value="${equipmentDtl.DATE_ACQUISITION}"/>
											</div>
										</td>
										<th><spring:message code='epms.spec' /></th><!-- 크기/치수 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.DIMENSION' />" readonly="readonly" id="DIMENSION" name="DIMENSION" value="<c:out value="${equipmentDtl.DIMENSION}" escapeXml="true"/>"/>
											</div>
										</td>
									</tr>
									<tr>
										<th><spring:message code='epms.date.install' /></th><!-- 설치일 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.DATE_INSTALL' />" readonly="readonly" id="DATE_INSTALL" name="DATE_INSTALL" value="${equipmentDtl.DATE_INSTALL}"/>
											</div>
										</td>
										<th><spring:message code='epms.country' /></th><!-- 제조국 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="hidden" class="inp-comm" title="<spring:message code='epms.equipment.COUNTRY' />" readonly="readonly" id="COUNTRY" name="COUNTRY" value="${equipmentDtl.COUNTRY}"/>
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.COUNTRY_NAME' />" readonly="readonly" id="COUNTRY_NAME" name="COUNTRY_NAME" value="${equipmentDtl.COUNTRY_NAME}"/>
											</div>
										</td>
									</tr>
									<tr>
										<th><spring:message code='epms.date.start' /></th><!-- 가동일 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.DATE_OPERATE' />" readonly="readonly" id="DATE_OPERATE" name="DATE_OPERATE" value="${equipmentDtl.DATE_OPERATE}"/>
											</div>
										</td>
										<th><spring:message code='epms.supplier' /></th><!-- 자산제조사 -->
										<td>
											<div class="inp-wrap readonly wd-per-100">
												<input type="text" class="inp-comm" title="<spring:message code='epms.equipment.MANUFACTURER' />" readonly="readonly" id="MANUFACTURER" name="MANUFACTURER" value="<c:out value="${equipmentDtl.MANUFACTURER}" escapeXml="true"/>"/>
											</div>
										</td>
									</tr>
									<tr>
										<th><spring:message code='epms.memo' /></th><!-- 메모 -->
										<td colspan="3">
											<div class="txt-wrap readonly">
												<textarea cols="" rows="3" title="<spring:message code='epms.equipment.MEMO' />" readonly="readonly" id="MEMO" name="MEMO"><c:out value="${equipmentDtl.MEMO}" escapeXml="true"/></textarea>
											</div>
										</td>
									</tr>
									<tr>
										<th scope="row"><spring:message code='epms.manual' /></th><!-- 매뉴얼 -->
										<td colspan="3">
											<div class="inq-area-top" style="width:100%">
												<div class="bxType01 fileWrap previewFileLst">
													<div>
														<div class="fileList type02">
															<div id="detailFileList0" class="fileDownLst">
																<c:forEach var="item" items="${manualList}" varStatus="idx">
																	<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																		<span class="MultiFile-export">${item.SEQ_DSP}</span> 
																		<span class="MultiFile-title" title="File selected: ${item.NAME}">
																		${item.FILE_NAME} (${item.FILE_SIZE_TXT})
																		</span>
																		<a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a>
																	</div>		
																</c:forEach>
															</div>
														</div>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<th scope="row"><spring:message code='epms.url' /></th><!-- URL -->
										<td colspan="3">
											<div class="inq-area-top" style="width:100%">
												<div class="bxType01 fileWrap previewFileLst">
													<div>
														<div class="fileList type02">
															<div id="detailFileList0" class="fileDownLst">
																<c:forEach var="item" items="${urlList}" varStatus="idx">
																	<div class="MultiFile-label" id="fileLstWrap_${item.EQUIPMENT_URL}">
																		<span class="MultiFile-export">${item.SEQ_DSP}</span> 
																		<span class="MultiFile-title" title="File selected: ${item.URL_DESCRIPTION}">
																		${item.URL_DESCRIPTION} <br>
																		</span>
																		<a href="#none" class="btnLink" data-address="${item.URL_ADDRESS}">${item.URL_ADDRESS}</a>
																	</div>		
																</c:forEach>
															</div>
														</div>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<th scope="row"><spring:message code='epms.image' /></th><!-- 이미지 -->
										<td colspan="3">
											<div id="previewList" class="wd-per-100">
												<div class="bxType01 fileWrap previewFileLst">
													<div>
														<div class="fileList type02">
															<div id="detailFileList0" class="fileDownLst">
																<c:forEach var="item" items="${attachList}" varStatus="idx">
																	<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																		<span class="MultiFile-export">${item.SEQ_DSP}</span> 
																		<span class="MultiFile-title" title="File selected: ${item.NAME}">
																		${item.FILE_NAME} (${item.FILE_SIZE_TXT})
																		</span>
																		<a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a>
																	</div>		
																</c:forEach>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											<div id="previewImg" class="wd-per-100">
												<div class="previewFile mgn-t-15">
													<h3 class="tit-cont preview" id="previewTitle"><spring:message code='epms.image.preview' /></h3><!-- 이미지 미리보기 -->
													<ul>
														<c:forEach var="item" items="${attachList}" varStatus="idx">
															<c:if test = "${item.CD_FILE_TYPE eq '01'}">	<%-- CD_FILE_TYPE : 01(이미지), 02(동영상), 03(음성), 00(기타) --%>
																<li><!-- 클릭하시면 원본크기로 보실 수 있습니다. -->
																	<img src="/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}" title="<spring:message code='epms.equipment.IMG_CLICK_ORG' />" onclick="originFileView('/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}')" />
																	<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																		<span class="MultiFile-title" title="File selected: ${item.NAME}">
																		<p>${item.FILE_NAME} (${item.FILE_SIZE_TXT}))</p>
																		</span>
																		<a href="#none" class="btnFileDwn icon download" data-attachgrpno="${item.ATTACH_GRP_NO}" data-attach="${item.ATTACH}">다운로드</a>
																	</div>
																</li>
															</c:if>
														</c:forEach>
													</ul>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
						</table>
					</div>
				</form>
				
				
			</div> <%-- modal-bodyIn : e --%>
		</div> <%-- modal-body : e --%>
	</div> <%-- modal-content : e --%>
</div> <%-- modal : e --%>

<%-- 파일 다운로드 iframe --%>
<iframe id="fileDownFrame" style="width:700px;height:300px;border:0px"></iframe>

