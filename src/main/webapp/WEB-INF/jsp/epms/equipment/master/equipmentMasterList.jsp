<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld"                 %>
<%@ taglib prefix="c"	   uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"	   uri="http://java.sun.com/jstl/fmt"           %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: equipmentMasterList.jsp
	Description : [설비마스터 조회] : 화면
    author		: 서정민
    since		: 2018.02.07
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.02.07	 서정민		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<script type="text/javascript">

<%-- 전역변수 --%>
var focusedRow = null;		<%-- Focus Row : [라인] --%>
var focusedRow2 = null;		<%-- Focus Row : [설비] --%>
var focusedRow3 = null;		<%-- Focus Row : [팝업 : 기기이력] --%>

$(document).ready(function(){
	
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function (e) {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>	
	});	
});

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "CategoryList") {	<%-- 1. [라인] 클릭시 --%>
		
		if (focusedRow == null || focusedRow.CATEGORY != row.CATEGORY || row.Added == 1){
			
			<%-- 1-1. 포커스 --%>
			if(focusedRow != null){
				grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
			}
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			
			focusedRow = row;
			focusedRow2 = null;
			
			<%-- 1-2. 설비 목록 조회 --%>
			if (row.TREE != null && row.TREE != "" && row.Added != 1) {
				
				$('#CATEGORY_TYPE').val(row.CATEGORY_TYPE);	<%-- 카테고리 유형 : 'LINE' --%>
				$('#CATEGORY_UID').val(row.CATEGORY_UID);	<%-- 기능위치 --%>
				$('#CATEGORY_NAME').val(row.CATEGORY_NAME);	<%-- 라인명 --%>
				$('#TREE').val(row.TREE);					<%-- 구조체 --%>
				$('#equipmentMasterListTitle').html(" <spring:message code='epms.system' /> : " + "[ " + row.CATEGORY_NAME + " ]");  <%-- 설비 --%>
				getEquipmentList();
			}
		}
	}
	else if(grid.id == "EquipmentMasterList") {	<%-- 2. [설비] 클릭시 --%>
	
		<%-- 2-1. 포커스  --%>
		if(focusedRow2 != null){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow2 = row;
		
	}
	else if(grid.id == "EquipmentHistoryList") {	<%-- 3. [팝업 : 기기이력] 클릭시 --%>
		
		<%-- 3-1. 포커스  --%>
		if(focusedRow3 != null){
			grid.SetAttribute(focusedRow3,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow3 = row;
		
	}
}
<%-- 선택된 라인에 해당되는 설비 조회 --%>
function getEquipmentList(){
	Grids.EquipmentMasterList.Source.Data.Url = "/epms/equipment/master/equipmentMasterListData.do?TREE=" + $('#TREE').val()
											  + "&CATEGORY_TYPE=" + $('#CATEGORY_TYPE').val()
											  + "&editYn=${editYn}";
	Grids.EquipmentMasterList.ReloadBody();	
}

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source,data, func){
	if(grid.id == "EquipmentMasterList"){
		if(data != ""){
			callLoadingBar();
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "EquipmentMasterList"){
					setTimeout($.unblockUI, 100);
				}
			}
		}
	}
}

<%-- QR코드 인쇄 : 1.인쇄 버튼 --%>
function fnQrPrint(Url, w, h){
	
	selRows = Grids.EquipmentList.GetSelRows();
	if(selRows.length == 0){
		alert("인쇄하실 설비를 선택해주세요.");
		return;
	}
	
	fn_openPop(Url, '', w, h);
}
<%-- QR코드 인쇄 : 2.설비명 --%>
function fnGetEquipmentNameArr(){
	var equipmentNameArr = new Array();
	for(var i=0; i <selRows.length;i++){
		equipmentNameArr[i] = Grids.EquipmentList.GetValue(selRows[i], "EQUIPMENT_NAME");
	}
	return equipmentNameArr;
}
<%-- QR코드 인쇄 : 3.설비코드 --%>
function fnGetEquipmentUidArr(){
	var equipmentUidArr = new Array();
	for(var i=0; i <selRows.length;i++){
		equipmentUidArr[i] = Grids.EquipmentList.GetValue(selRows[i], "EQUIPMENT_UID");
	}
	return equipmentUidArr;
}
<%-- QR코드 인쇄 : 4.모델명 --%>
function fnGetModelArr(){
	var modelArr = new Array();
	for(var i=0; i <selRows.length;i++){
		modelArr[i] = Grids.EquipmentList.GetValue(selRows[i], "MODEL");
	}
	return modelArr;
}
<%-- QR코드 인쇄 : 5.제조사 --%>
function fnGetManufacturerArr(){
	var manufacturerArr = new Array();
	for(var i=0; i <selRows.length;i++){
		manufacturerArr[i] = Grids.EquipmentList.GetValue(selRows[i], "MANUFACTURER");
	}
	return manufacturerArr;
}
<%-- QR코드 인쇄 : 6.자산번호 --%>
function fnGetAssetNoArr(){
	var assetNoArr = new Array();
	for(var i=0; i <selRows.length;i++){
		assetNoArr[i] = Grids.EquipmentList.GetValue(selRows[i], "ASSET_NO");
	}
	return assetNoArr;
}
<%-- QR코드 인쇄 : 7.QR이미지 --%>
function fnGetAttachGrpNoArr(){
	var attachGrpNoArr = new Array();
	for(var i=0; i <selRows.length;i++){
		attachGrpNoArr[i] = Grids.EquipmentList.GetValue(selRows[i], "ATTACH_GRP_NO2");
	}
	return attachGrpNoArr;
}


<%-- 모달 호출 (param 전달용) --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- 모달 호출 (Param 전달 및 그리드 호출) --%>
function fnOpenLayerWithGrid(url, name){
	$.ajax({
		type: 'POST',
		url: url,
		dataType: 'json',
		processData: false, 
		contentType:false,
		success: function (json) {
			
			if(name == 'popEquipmentUrlList'){				<%-- URL 팝업일 경우 --%>
				
				$("#"+name+"_EQUIPMENT").val(isEmpty(json.item.EQUIPMENT)==true ? '' :json.item.EQUIPMENT);
				$("#"+name+"_LOCATION_NAME").text(isEmpty(json.item.LOCATION_NAME)==true ? '' : "["+json.item.LOCATION_NAME+"]");
				$("#"+name+"_LINE_NAME").text(isEmpty(json.item.LINE_NAME)==true ? '' : "["+json.item.LINE_NAME+"]");
				$("#"+name+"_EQUIPMENT_NAME").text(isEmpty(json.item.EQUIPMENT_NAME)==true ? '' : "["+json.item.EQUIPMENT_NAME+"]");
				$("#"+name+"_EQUIPMENT_UID").text(isEmpty(json.item.EQUIPMENT_UID)==true ? '' : "["+json.item.EQUIPMENT_UID+"]");
				
				Grids.EquipmentUrlList.Source.Data.Url = "/epms/equipment/master/equipmentUrlListData.do?EQUIPMENT=" + $('#popEquipmentUrlList_EQUIPMENT').val();
				Grids.EquipmentUrlList.ReloadBody();
			}	
			else if(name == 'popEquipmentHistoryList'){		<%-- 기기이력 팝업일 경우 --%>
			
				$("#"+name+"_EQUIPMENT").val(isEmpty(json.item.EQUIPMENT)==true ? '' :json.item.EQUIPMENT);
				$("#"+name+"_LOCATION_NAME").text(isEmpty(json.item.LOCATION_NAME)==true ? '' : "["+json.item.LOCATION_NAME+"]");
				$("#"+name+"_LINE_NAME").text(isEmpty(json.item.LINE_NAME)==true ? '' : "["+json.item.LINE_NAME+"]");
				$("#"+name+"_EQUIPMENT_NAME").text(isEmpty(json.item.EQUIPMENT_NAME)==true ? '' : "["+json.item.EQUIPMENT_NAME+"]");
				$("#"+name+"_EQUIPMENT_UID").text(isEmpty(json.item.EQUIPMENT_UID)==true ? '' : "["+json.item.EQUIPMENT_UID+"]");
				
				Grids.EquipmentHistoryList.Source.Data.Url = "/epms/equipment/history/equipmentHistoryListData.do?EQUIPMENT=" + $('#popEquipmentHistoryList_EQUIPMENT').val();
				Grids.EquipmentHistoryList.ReloadBody();
			}
			
			fnModalToggle(name);
		
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
			loadEnd();
		}
	});
}

</script>
</head>

<body>
<div id="contents">
	<input type="hidden" id="CATEGORY_TYPE" name="CATEGORY_TYPE" />	<%-- 카테고리 유형 : 'LINE' --%>
	<input type="hidden" id="CATEGORY_UID"  name="CATEGORY_UID"  />	<%-- 카테고리 코드 --%>
	<input type="hidden" id="CATEGORY_NAME" name="CATEGORY_NAME" />	<%-- 카테고리 명 : 라인명 --%>
	<input type="hidden" id="TREE"          name="TREE"          />	<%-- 구조체 --%>
	
	<div class="fl-box panel-wrap03 leftPanelArea" style="width: 20%; min-width: 350px;">
		<h5 class="panel-tit">
		    <spring:message code='epms.category' />
		</h5><!-- 라인/회사/프로젝트 -->
		<div class="panel-body">
			<div id="categoryList">
				<bdo	Debug      = "Error"
						Data_Url   = "/sys/category/categoryData.do?CATEGORY_TYPE=LINE"
						Layout_Url = "/sys/category/categoryNotEditLayout.do?CATEGORY_TYPE=LINE"
				></bdo>
			</div>
		</div>
	</div>
	
	<div class="fl-box panel-wrap03 rightPanelArea">
		<h5 class="panel-tit mgn-l-10" id="equipmentMasterListTitle">
		    <spring:message code='epms.system' />
		</h5><!-- 설비/카테고리 -->
		<div class="panel-body mgn-l-10">
			<div id="equipmentMasterList">
				<bdo	Debug="Error"
						Layout_Url="/epms/equipment/master/equipmentMasterListLayout.do"	
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=equipmentMasterList.xls&dataName=data"
				>
				</bdo>
			</div>
		</div>
	</div>
</div>

<%-- 첨부파일 팝업(이미지 및 매뉴얼) --%>
<div class="modal fade modalFocus" id="popFileManageForm" data-backdrop="static" data-keyboard="false"></div>	<%-- popEquipmentImgView.jsp --%>

<%-- 기기이력 팝업 --%>
<%@ include file="/WEB-INF/jsp/epms/equipment/history/popEquipmentHistoryList.jsp"%>

<%-- URL 팝업 --%>
<%@ include file="/WEB-INF/jsp/epms/equipment/master/popEquipmentUrlList.jsp"%>

<%-- 설비마스터 상세 팝업 --%>
<div class="modal fade modalFocus" id="popEquipmentMasterDtl" data-backdrop="static" data-keyboard="false"></div>	<%-- popEquipmentMasterDtl.jsp --%>

<%-- 정비실적조회 --%>
<div class="modal fade modalFocus" id="popRepairResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 예방보전실적조회 --%>
<div class="modal fade modalFocus" id="popPreventiveResultForm" data-backdrop="static" data-keyboard="false"></div>

</body>
</html>
