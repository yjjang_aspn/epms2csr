<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: equipmentUrlListData.jsp
	Description : 설비 관련 URL 그리드 데이터
    author		: 서정민
    since		: 2018.04.13
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.04.13	 서정민		최초 생성

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I
			EQUIPMENT			= "${item.EQUIPMENT}"									<%-- 설비 ID --%>
			EQUIPMENT_URL		= "${item.EQUIPMENT_URL}"								<%-- URL ID --%>
			URL_DESCRIPTION		= "${fn:replace(item.URL_DESCRIPTION, '"', '&quot;')}"	<%-- URL 설명 --%>
			URL_ADDRESS			= "${fn:replace(item.URL_ADDRESS, '"', '&quot;')}"		<%-- URL 주소 --%>
			SEQ_DSP				= "${item.SEQ_DSP}"										<%-- 출력순서 --%>
			DEL_YN				= "${item.DEL_YN}"										<%-- 삭제여부 --%>
			
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>