<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld"                 %>
<%@ taglib prefix="c"	   uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: equipmentMasterListLayout.jsp
	Description : [설비마스터 관리] : 설비 그리드 레이아웃
    author		: 서정민
    since		: 2018.02.07
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.02.07	 서정민		최초 생성

--%>

<c:set var="gridId" value="EquipmentMasterList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1"			SuppressCfg="0"			
			Editing="1"				Deleting="0"		Selecting="1"		Dragging="1"					
			Filtering="1"			StandardFilter="3"	ClearSelected="2"
			Paging="2"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"				
			CopyCols="3"			PasteFocused="3"					
	/>
	<c:if test="${equipment_hierarchy eq 'true' }">
		<Cfg	MainCol="EQUIPMENT_NAME" />
	</c:if>
	
	<Head>
		<Header	id="Header"	Align="center" CanExport="1" CanHide="0"
			EQUIPMENT				= "<spring:message code='epms.system.id'                  />"  <%  // 설비ID     %>
			COMPANY_ID				= "<spring:message code='epms.company.code'               />"  <%  // 회사코드       %>
			LOCATION				= "<spring:message code='epms.location'                />"  <%  // 위치            %>
			LINE					= "<spring:message code='epms.category'                />"  <%  // 라인ID     %>
			LINE_UID				= "<spring:message code='epms.category.uid'            />"  <%  // 기능위치       %>
			LINE_NAME				= "<spring:message code='epms.category.name'           />"  <%  // 라인             %>
			ORG_LINE				= "<spring:message code='epms.exist.category.id'          />"  <%  // 기존라인ID   %>
			ORG_LINE_UID			= "<spring:message code='epms.exist.category.uid'         />"  <%  // 기존기능위치   %>
			ORG_LINE_NAME			= "<spring:message code='epms.exist.category.name'        />"  <%  // 기존라인        %>
			PARENT_EQUIPMENT		= "<spring:message code='epms.parent.system.id'           />"  <%  // 부모설비ID   %>
			TREE					= "<spring:message code='epms.equipment.TREE'             />"  <%  // 구조체       %>
			DEPTH					= "<spring:message code='epms.equipment.DEPTH'            />"  <%  // 레벨         %>
			SEQ_DSP					= "<spring:message code='epms.equipment.SEQ_DSP'          />"  <%  // 출력순서     %>
			EQUIPMENT_UID			= "<spring:message code='epms.system.uid'                 />"  <%  // 설비코드     %>
			EQUIPMENT_NAME			= "<spring:message code='epms.system.name'                />"  <%  // 설비명       %>
			
			GRADE					= "<spring:message code='epms.equipment.EQUIPMENT'        />"  <%  // 설비등급   %>
			ASSET_NO				= "<spring:message code='epms.asset.no'              />"  <%  // 자산번호   %>
			MODEL					= "<spring:message code='epms.model'                   />"  <%  // 모델명    %>
			DIMENSION				= "<spring:message code='epms.spec'                    />"  <%  // 크기/치수  %>
			WEIGHT					= "<spring:message code='epms.qnty'                    />"  <%  // 총중량    %>
			WEIGHT_UNIT				= "<spring:message code='epms.unit'                    />"  <%  // 중량단위   %>
			ACQUISITION_VALUE		= "<spring:message code='epms.amount'                  />"  <%  // 취득가액   %>
			CURRENCY				= "<spring:message code='epms.currency'                />"  <%  // 통화     %>
			COUNTRY					= "<spring:message code='epms.country'                 />"  <%  // 제조국    %>
			MANUFACTURER			= "<spring:message code='epms.supplier'                />"  <%  // 제조사    %>
			DATE_ACQUISITION		= "<spring:message code='epms.date.acquire'                   />"  <%  // 취득일    %>
			DATE_INSTALL			= "<spring:message code='epms.date.install'                   />"  <%  // 설치일    %>
			DATE_OPERATE			= "<spring:message code='epms.date.start'              />"  <%  // 가동일    %>
			DATE_VALID				= "<spring:message code='epms.date.end'                />"  <%  // 유효일    %>
			COST_CENTER				= "<spring:message code='epms.costcenter'              />"  <%  // 코스트센터  %>
			MEMO					= "<spring:message code='epms.memo'                    />"  <%  // 메모     %>
			
			ATTACH_GRP_NO			= "<spring:message code='epms.image'                   />"  <%  // 이미지    %>
			ATTACH_GRP_NO2			= "<spring:message code='epms.qrCode'                  />"  <%  // QR코드   %>
			ATTACH_GRP_NO3			= "<spring:message code='epms.manual'                  />"  <%  // 매뉴얼    %>
			URL						= "<spring:message code='epms.url'                     />"  <%  // URL    %>
			HISTORY					= "<spring:message code='epms.history'                    />"  <%  // 이력     %>
			DETAIL					= "<spring:message code='epms.detail'                     />"  <%  // 상세     %>
		/>
		<Filter	id="Filter" CanExport="1" CanHide="0" CanFocus="1"/>
	</Head>
	
	<Solid>
		<I	id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4" CanHide="0"
			NAVType="Pager" 
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
	
	<LeftCols>
		<C Name="EQUIPMENT"					Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="COMPANY_ID"				Type="Text" Align="center" Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="LOCATION"					Type="Enum" Align="left"   Visible="0" Width="60"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>	
		<C Name="LINE"						Type="Text" Align="center" Visible="0" Width="120" CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="LINE_UID"					Type="Text" Align="left"   Visible="0" Width="120" CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="LINE_NAME"					Type="Text" Align="left"   Visible="0" Width="120" CanEdit="0" CanExport="1" CanHide="1"/>
		<C Name="ORG_LINE"					Type="Text" Align="left"   Visible="0" Width="60"  CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="ORG_LINE_UID"				Type="Text" Align="left"   Visible="0" Width="120" CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="ORG_LINE_NAME"				Type="Text" Align="left"   Visible="0" Width="120" CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="PARENT_EQUIPMENT"			Type="Text" Align="center" Visible="0" Width="100" CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="TREE"	 					Type="Text" Align="left"   Visible="0" Width="100" CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="DEPTH"	 					Type="Text" Align="center" Visible="0" Width="100" CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="SEQ_DSP"	 				Type="Text" Align="center" Visible="0" Width="100" CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="EQUIPMENT_UID"				Type="Text" Align="left"   Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT_NAME"			Type="Text" Align="left"   Visible="1" Width="200" CanEdit="1" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
	</LeftCols>
	
	<Cols>
		<C Name="ASSET_NO"					Type="Text" Align="left"   Visible="1" Width="80"  CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="MODEL"						Type="Text" Align="left"   Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="DIMENSION"					Type="Text" Align="left"   Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="WEIGHT"					Type="Int"  Align="right"  Visible="1" Width="80"  CanEdit="0" CanExport="1" CanHide="1" Format="#,##0"  ExportStyle='mso-number-format:"#,##0";'/>
		<C Name="WEIGHT_UNIT"				Type="Enum" Align="left"   Visible="1" Width="60"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="WEIGHT_UNIT"/>/>
		<C Name="ACQUISITION_VALUE"			Type="Int"  Align="right"  Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" Format="#,##0"  ExportStyle='mso-number-format:"#,##0";'/>
		<C Name="CURRENCY"					Type="Enum" Align="left"   Visible="1" Width="60"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="CURRENCY"/>/>
		<C Name="COUNTRY"					Type="Enum" Align="left"   Visible="1" Width="80"  CanEdit="0" CanExport="1" CanHide="1" <tag:enum codeGrp="COUNTRY"/>/>
		<C Name="MANUFACTURER"				Type="Text" Align="left"   Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="DATE_ACQUISITION"			Type="Date" Align="center" Visible="1" Width="110" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd"/>
		<C Name="DATE_INSTALL"				Type="Date" Align="center" Visible="1" Width="110" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd"/>
		<C Name="DATE_OPERATE"				Type="Date" Align="center" Visible="1" Width="110" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd"/>
		<C Name="DATE_VALID"				Type="Date" Align="center" Visible="1" Width="110" CanEdit="0" CanExport="1" CanHide="1" Format="yyyy/MM/dd"/>
		<C Name="COST_CENTER"				Type="Text" Align="left"   Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="GRADE"						Type="Enum" Align="left"   Visible="0" Width="70"  CanEdit="1" CanExport="1" CanHide="1" <tag:enum codeGrp="GRADE"/>/>
		<C Name="MEMO"						Type="Text" Align="left"   Visible="1" Width="200" CanEdit="1" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" "/>
		<C Name="ATTACH_GRP_NO"				Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="0" CanHide="1"/>
		<C Name="ATTACH_GRP_NO2"			Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="0" CanHide="1"/>
		<C Name="ATTACH_GRP_NO3"			Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="0" CanHide="1"/>
		<C Name="URL"						Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="0" CanHide="1"/>
		<C Name="HISTORY"					Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="0" CanHide="1"/>
	</Cols>
	
	<RightCols>
		<C Name="DETAIL"					Type="Icon" Align="center" Visible="1" Width="70"  CanEdit="0" CanExport="0" CanHide="1"/>
	</RightCols>
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,추가,QR코드인쇄,저장,항목,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyWidth="1" Empty="        " 
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				동기화Type="Html" 동기화="&lt;a href='#none' title='동기화' class=&quot;treeButton treeSap&quot;
					onclick='fnPopSyncEquipment()'>동기화&lt;/a>"
				QR코드생성Type="Html" QR코드생성="&lt;a href='#none' title='QR코드생성' class=&quot;treeButton treeQrPrint&quot;
					onclick='fnQrCreate()'>QR코드생성&lt;/a>"
				QR코드인쇄Type="Html" QR코드인쇄="&lt;a href='#none' title='QR코드인쇄' class=&quot;treeButton treeQrPrint&quot;
					onclick='fnQrPrintConfig()'>QR코드인쇄&lt;/a>"
				라인변경Type="Html" 라인변경="&lt;a href='#none' title='라인변경' class=&quot;treeButton treeLineChange&quot;
					onclick='fnChangeLine()'>라인변경&lt;/a>"
<%-- 				접기Type="Html" 접기="&lt;a href='#none' title='접기' id=&quot;Folding02&quot; class=&quot;icon folderClose&quot; --%>
<%-- 					onclick='fnExpandAll02(&quot;${gridId}&quot;)'>&lt;/a>" --%>
				추가Type="Html" 추가="&lt;a href='#none' title='추가' class=&quot;defaultButton icon add&quot;
					onclick='addRowGrid(&quot;${gridId}&quot;)'>추가&lt;/a>"
				저장Type="Html" 저장="&lt;a href='#none' title='저장' class=&quot;treeButton treeSave&quot;
					onclick='fnSave(&quot;${gridId}&quot;)'>저장&lt;/a>"
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
				엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>