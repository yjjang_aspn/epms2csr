<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
	Class Name	: equipmentMasterListData.jsp
	Description : [설비마스터 조회],[설비마스터 관리] : 설비 그리드 데이터
    author		: 서정민
    since		: 2018.02.07
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.02.07	 서정민		최초 생성

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<c:set var="nodeCnt" value="0" />
		<I
			EQUIPMENT				= "${item.EQUIPMENT}"									<%-- 설비ID --%>
			COMPANY_ID				= "${item.COMPANY_ID}"									<%-- 회사코드 --%>
			LOCATION				= "${item.LOCATION}"									<%-- 위치 --%>
			LINE					= "${item.LINE}"										<%-- 라인ID --%>
			LINE_UID				= "${fn:replace(item.LINE_UID, '"', '&quot;')}"			<%-- 기능위치 --%>
			LINE_NAME				= "${fn:replace(item.LINE_NAME, '"', '&quot;')}"		<%-- 라인명 --%>
			ORG_LINE				= "${item.LINE}"										<%-- 기존 라인ID --%>
			ORG_LINE_UID			= "${fn:replace(item.LINE_UID, '"', '&quot;')}"			<%-- 기존 기능위치 --%>
			ORG_LINE_NAME			= "${fn:replace(item.LINE_NAME, '"', '&quot;')}"		<%-- 기존 라인명 --%>
			PARENT_EQUIPMENT		= "${item.PARENT_EQUIPMENT}"							<%-- 부모설비ID --%>
			TREE					= "${item.TREE}"										<%-- 구조체 --%>
			DEPTH					= "${item.DEPTH}"										<%-- 레벨 --%>
			SEQ_DSP					= "${item.SEQ_DSP}"										<%-- 출력순서 --%>
			EQUIPMENT_UID			= "${item.EQUIPMENT_UID}"								<%-- 설비코드 --%>
			EQUIPMENT_NAME			= "${fn:replace(item.EQUIPMENT_NAME, '"', '&quot;')}"	<%-- 설비명 --%>
			
			GRADE					= "${item.GRADE}"										<%-- 등급 --%>
			ASSET_NO				= "${fn:replace(item.ASSET_NO, '"', '&quot;')}"			<%-- 자산번호 --%>
			MODEL					= "${fn:replace(item.MODEL, '"', '&quot;')}"			<%-- 모델명 --%>
			DIMENSION				= "${fn:replace(item.DIMENSION, '"', '&quot;')}"		<%-- 크기/치수 --%>
			WEIGHT					= "${item.WEIGHT}"										<%-- 총중량 --%>
			WEIGHT_UNIT				= "${item.WEIGHT_UNIT}"									<%-- 중량단위 --%>
			ACQUISITION_VALUE		= "${item.ACQUISITION_VALUE}"							<%-- 취득가액 --%>
			CURRENCY				= "${item.CURRENCY}"									<%-- 통화 --%>
			COUNTRY					= "${item.COUNTRY}"										<%-- 제조국 --%>
			MANUFACTURER			= "${fn:replace(item.MANUFACTURER, '"', '&quot;')}"		<%-- 제조사 --%>
			DATE_ACQUISITION		= "${item.DATE_ACQUISITION}"							<%-- 취득일 --%>
			DATE_INSTALL			= "${item.DATE_INSTALL}"								<%-- 설치일 --%>
			DATE_OPERATE			= "${item.DATE_OPERATE}"								<%-- 가동일 --%>
			DATE_VALID				= "${item.DATE_VALID}"									<%-- 유효일 --%>
			COST_CENTER				= "${item.COST_CENTER}"									<%-- 코스트센터 --%>
			MEMO					= "${fn:replace(item.MEMO, '"', '&quot;')}"				<%-- 메모 --%>
			
			<%-- 이미지 --%>
			ATTACH_GRP_NO			= "${item.ATTACH_GRP_NO}"
			<c:choose>
				<c:when test = '${item.FILE_CNT > 0 && editYn eq "Y"}'>
					ATTACH_GRP_NOSwitch="1"  ATTACH_GRP_NOIcon="/images/com/web/modification_icon.gif" 
					ATTACH_GRP_NOOnClickSideIcon="fnOpenLayerWithParam('/attach/popFileManageForm.do?EQUIPMENT='+Row.EQUIPMENT+'&ATTACH_GRP_NO='+Row.ATTACH_GRP_NO+'&flag=equipment&editYn=${editYn}','popFileManageForm');"
				</c:when>
				<c:when test = '${item.FILE_CNT eq "0" && editYn eq "Y"}'>
					ATTACH_GRP_NOSwitch="1"  ATTACH_GRP_NOIcon="/images/com/web/upload_icon.gif" 
					ATTACH_GRP_NOOnClickSideIcon="fnOpenLayerWithParam('/attach/popFileManageForm.do?EQUIPMENT='+Row.EQUIPMENT+'&ATTACH_GRP_NO='+Row.ATTACH_GRP_NO+'&flag=equipment&editYn=${editYn}','popFileManageForm');"
				</c:when>
				<c:when test = '${item.FILE_CNT > 0 && editYn eq "N"}'>
					ATTACH_GRP_NOSwitch="1"  ATTACH_GRP_NOIcon="/images/com/web/commnet_up3.gif" 
					ATTACH_GRP_NOOnClickSideIcon="fnOpenLayerWithParam('/attach/popFileManageForm.do?EQUIPMENT='+Row.EQUIPMENT+'&ATTACH_GRP_NO='+Row.ATTACH_GRP_NO+'&flag=equipment&editYn=${editYn}','popFileManageForm');"
				</c:when>
			</c:choose>
			
			<%-- QR코드 --%>
			ATTACH_GRP_NO2			= "${item.ATTACH_GRP_NO2}"
			ATTACH_GRP_NO2Switch="1"  ATTACH_GRP_NO2Icon="/images/com/web/commnet_up3.gif" 
			ATTACH_GRP_NO2OnClickSideIcon="fnOpenLayerWithParam('/attach/popFileManageForm.do?EQUIPMENT='+Row.EQUIPMENT+'&ATTACH_GRP_NO='+Row.ATTACH_GRP_NO2+'&flag=qrcode&editYn=N','popFileManageForm');"
			
			<%-- 매뉴얼 --%>
			ATTACH_GRP_NO3			= "${item.ATTACH_GRP_NO3}"
			<c:choose>
				<c:when test = '${item.FILE_CNT3 > 0 && editYn eq "Y"}'>
					ATTACH_GRP_NO3Switch="1"  ATTACH_GRP_NO3Icon="/images/com/web/modification_icon.gif" 
					ATTACH_GRP_NO3OnClickSideIcon="fnOpenLayerWithParam('/attach/popFileManageForm.do?EQUIPMENT='+Row.EQUIPMENT+'&ATTACH_GRP_NO='+Row.ATTACH_GRP_NO3+'&flag=equipment_manual&editYn=${editYn}','popFileManageForm');"
				</c:when>
				<c:when test = '${item.FILE_CNT3 eq "0" && editYn eq "Y"}'>
					ATTACH_GRP_NO3Switch="1"  ATTACH_GRP_NO3Icon="/images/com/web/upload_icon.gif" 
					ATTACH_GRP_NO3OnClickSideIcon="fnOpenLayerWithParam('/attach/popFileManageForm.do?EQUIPMENT='+Row.EQUIPMENT+'&ATTACH_GRP_NO='+Row.ATTACH_GRP_NO3+'&flag=equipment_manual&editYn=${editYn}','popFileManageForm');"
				</c:when>
				<c:when test = '${item.FILE_CNT3 > 0 && editYn eq "N"}'>
					ATTACH_GRP_NO3Switch="1"  ATTACH_GRP_NO3Icon="/images/com/web/commnet_up3.gif" 
					ATTACH_GRP_NO3OnClickSideIcon="fnOpenLayerWithParam('/attach/popFileManageForm.do?EQUIPMENT='+Row.EQUIPMENT+'&ATTACH_GRP_NO='+Row.ATTACH_GRP_NO3+'&flag=equipment_manual&editYn=${editYn}','popFileManageForm');"
				</c:when>
			</c:choose>
			
			<%-- URL --%>
			<c:choose>
				<c:when test = '${item.URL_CNT > 0 && editYn eq "Y"}'>
					URL				= "${item.URL_CNT}"
					URLSwitch="1"  URLIcon="/images/com/web/modification_icon.gif" 
					URLOnClickSideIcon="fnOpenLayerWithGrid('/epms/equipment/master/popEquipmentUrlList.do?EQUIPMENT='+Row.EQUIPMENT+'&editYn=${editYn}','popEquipmentUrlList');"
				</c:when>
				<c:when test = '${item.URL_CNT eq "0" && editYn eq "Y"}'>
					URL				= "${item.URL_CNT}"
					URLSwitch="1"  URLIcon="/images/com/web/upload_icon.gif" 
					URLOnClickSideIcon="fnOpenLayerWithGrid('/epms/equipment/master/popEquipmentUrlList.do?EQUIPMENT='+Row.EQUIPMENT+'&editYn=${editYn}','popEquipmentUrlList');"
				</c:when>
				<c:when test = '${item.URL_CNT > 0 && editYn eq "N"}'>
					URLSwitch="1"  URLIcon="/images/com/web/commnet_up.gif" 
					URLOnClickSideIcon="fnOpenLayerWithGrid('/epms/equipment/master/popEquipmentUrlList.do?EQUIPMENT='+Row.EQUIPMENT+'&editYn=${editYn}','popEquipmentUrlList');"
				</c:when>
			</c:choose>
			
			<%-- 이력 --%>
 			<c:if test='${item.HISTORY_CNT>0}'>
				HISTORY			= "${item.HISTORY_CNT}"
	 			HISTORYSwitch="1"  HISTORYIcon="/images/com/web/commnet_up2.gif" 
	 			HISTORYOnClickSideIcon="fnOpenLayerWithGrid('/epms/equipment/history/popEquipmentHistoryList.do?EQUIPMENT='+Row.EQUIPMENT,'popEquipmentHistoryList');"
 			</c:if>
 			
 			<%-- 상세 --%>
			DETAILSwitch="1"  DETAILIcon="/images/com/web/commnet_up.gif" 
			DETAILOnClickSideIcon="fnOpenLayerWithParam('/epms/equipment/master/popEquipmentMasterDtl.do?EQUIPMENT='+Row.EQUIPMENT,'popEquipmentMasterDtl');"
			
			<%-- [정비요청] 버튼  --%>
			REGSwitch="1"  REGIcon="/images/com/web/gnb_ico_paper2.gif" 
			REGOnClickSideIcon="fnReg();"
		
			<c:choose>
				<c:when test="${item.CUR_LVL eq item.NEXT_LVL}">></I></c:when>
				<c:when test="${item.CUR_LVL < item.NEXT_LVL}">CanExpand='1' Expanded='1'></c:when>
				<c:when test="${item.CUR_LVL > item.NEXT_LVL}">></c:when>
				<c:otherwise>></c:otherwise>
			</c:choose>
			<c:set var="nodeCnt" value="${item.CUR_LVL - item.NEXT_LVL}" />
			<%-- <c:out value="${nodeCnt}"/> --%>
			<c:if test="${nodeCnt > 0}">
				<c:forEach var="item" begin="0" end="${nodeCnt}" step="1"></I></c:forEach>
			</c:if>
	</c:forEach> 
	</B>
</Body>
</Grid>