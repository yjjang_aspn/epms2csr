<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%--
	Class Name	: popQrcodePrintForm.jsp
	Description : QR코드 인쇄
	author		: 서정민
	since		: 2018.06.14
 
	<< 개정이력(Modification Information) >>
	수정일			수정자		수정내용
	----------  ------	---------------------------
	2018.06.14	 서정민		최초 생성

--%>
<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">
$( document ).ready(function() {
	$('#printPage').append('');
	fnDraw();
});

//=============================================================
//프린트 설정 Start
//=============================================================
// var initBody 
// function beforePrint() {
// 	initBody = document.body.innerHTML;
// 	document.body.innerHTML = idPrint.innerHTML;
// }

// function afterPrint() {
// 	document.body.innerHTML = initBody;
// }

// function printArea() {
// 	window.print(); 
	
//	const html = document.querySelector('html');
//	const printContents = document.querySelector('.printArea').innerHTML;
//	const printDiv = document.createElement("DIV");
//	printDiv.className = "print-div";
	 
//	html.appendChild(printDiv);
//	printDiv.innerHTML = printContents;
//	document.body.style.display = 'none';
//	window.print();
//	document.body.style.display = 'block';
//	printDiv.style.display = 'none';

// }

// window.onbeforeprint = beforePrint;
// window.onafterprint = afterPrint;

function printPage(){
	factory.printing.header = "";		//머릿말 설정
	factory.printing.footer = "";		//꼬릿말 설정
	factory.printing.portrait = true;	//출력방향 설정: true-가로, false-세로
	factory.printing.leftMargin = 1.0;	//왼쪽 여백 설정
	factory.printing.topMargin = 1.0;	//위쪽 여백 설정
	factory.printing.rightMargin = 1.0;	//오른쪽 여백 설정
	factory.printing.bottomMargin = 1.0;//아래쪽 여백 설정
	// factory.printing.printBackground = true; //배경이미지 출력 설정:라이센스 필요
	factory.printing.Print(false);		//출력하기
}
//=============================================================
//프린트 설정 End
//=============================================================


function fnDraw(){

	var equipmentNameArr = fnGetEquipmentNameArr();
	var equipmentUidArr = fnGetEquipmentUidArr();
	var modelArr = fnGetModelArr();
	var manufacturerArr = fnGetManufacturerArr();
	var assetNoArr = fnGetAssetNoArr();
	var attachGrpNoArr = fnGetAttachGrpNoArr();
	
	var cnt = equipmentNameArr.length;
	
	var i = 0;
	var j = 0;
	var html = "";
	
	// A4 : 1/2 사이즈
	if('${param.size}' == 'A4_2'){
		html = html + '<p class="printQrNoti"><span>!</span>2분할 사이즈는 세로 인쇄를 권장합니다.</p>';
		for(i=0; i<equipmentNameArr.length; i++){
			if(parseInt(i%2)==0){
				if(j!=0){
					html = html + '<div class="breakBefore">';
				}
				html = html + '<div class="printQrWrap halfSt">';
			}
			html = html + '	<div class="printQrLst">';
			html = html + '		<div class="printQrLstIn">';
			html = html + '			<div class="printQrTit">';
// 			html = html + '				<img src="/images/com/web/dunkin_logo.png">';
// 			html = html + '				설비관리';
			html = html + '			</div>';
			html = html + '			<div class="printQrCon">';
			html = html + '				<div class="printQr">';
			html = html + '					<img src="/attach/fileDownload.do?ATTACH_GRP_NO=' + attachGrpNoArr[i] + '&ATTACH=1" alt="QR코드">';
			html = html + '				</div>';
			html = html + '				<div class="printQrInfo">';
			html = html + '					<div class="printQrInfoLst">';
			html = html + '						<div>';
			html = html + '							<p>설비명</p>';
			html = html + '							' + equipmentNameArr[i];
			html = html + '						</div>';
			html = html + '					</div>';
			html = html + '					<div class="printQrInfoLst">';
			html = html + '						<div>';
			html = html + '							<p>설비코드</p>';
			html = html + '							' + equipmentUidArr[i];
			html = html + '						</div>';
			html = html + '					</div>';
			html = html + '				</div>';
			html = html + '			</div>';
			html = html + '			<div class="printQrLogo">';
// 			html = html + '				<h1><img src="/images/login/web/img_logo.png" alt="ASPN"></h1>';
			html = html + '				<h1><img src="/images/com/web/dunkin_logo.png" alt="dunkin"></h1>';
			html = html + '			</div>';
			html = html + '		</div>';
			html = html + '	</div>';
			
			
			if(cnt == i || parseInt(i%2) == 1){
				html = html + '	</div>';
				if(j!=0){
					html = html + '	</div>';
				}
				j++;
			}
		}
	}
	// A4 : 1/4 사이즈
	else if('${param.size}' == 'A4_4'){
		html = html + '<p class="printQrNoti"><span>!</span>4분할 사이즈는 가로 인쇄를 권장합니다.</p>';
		for(i=0; i<equipmentNameArr.length; i++){
			if(parseInt(i%4)==0){
				if(j!=0){
					html = html + '<div class="breakBefore">';
				}
				html = html + '<div class="printQrWrap quarterSt horizonSt">';
			}
			html = html + '	<div class="printQrLst">';
			html = html + '		<div class="printQrLstIn">';
			html = html + '			<div class="printQrTit">';
// 			html = html + '				<img src="/images/com/web/dunkin_logo.png">';
// 			html = html + '				설비관리';
			html = html + '			</div>';
			html = html + '			<div class="printQrCon">';
			html = html + '				<div class="printQr">';
			html = html + '					<img src="/attach/fileDownload.do?ATTACH_GRP_NO=' + attachGrpNoArr[i] + '&ATTACH=1" alt="QR코드">';
			html = html + '				</div>';
			html = html + '				<div class="printQrInfo">';
			html = html + '					<div class="printQrInfoLst">';
			html = html + '						<div>';
			html = html + '							<p>설비명</p>';
			html = html + '							' + equipmentNameArr[i];
			html = html + '						</div>';
			html = html + '					</div>';
			html = html + '					<div class="printQrInfoLst">';
			html = html + '						<div>';
			html = html + '							<p>설비코드</p>';
			html = html + '							' + equipmentUidArr[i];
			html = html + '						</div>';
			html = html + '					</div>';
			html = html + '				</div>';
			html = html + '			</div>';
			html = html + '			<div class="printQrLogo">';
// 			html = html + '				<h1><img src="/images/login/web/img_logo.png" alt="ASPN"></h1>';
			html = html + '				<h1><img src="/images/com/web/dunkin_logo.png" alt="dunkin"></h1>';
			html = html + '			</div>';
			html = html + '		</div>';
			html = html + '	</div>';
			
			
			if(cnt == i || parseInt(i%4) == 3){
				html = html + '	</div>';
				if(j!=0){
					html = html + '	</div>';
				}
				j++;
			}
		}
	}
	// A4 : 1/8 사이즈
	else if('${param.size}' == 'A4_8'){
		html = html + '<p class="printQrNoti"><span>!</span>8분할 사이즈는 세로 인쇄를 권장합니다.</p>';
		for(i=0; i<equipmentNameArr.length; i++){
			if(parseInt(i%8)==0){
				if(j!=0){
					html = html + '<div class="breakBefore">';
				}
				html = html + '<div class="printQrWrap">';
			}
			html = html + '	<div class="printQrLst">';
			html = html + '		<div class="printQrLstIn">';
			html = html + '			<div class="printQrTit">';
// 			html = html + '				<img src="/images/com/web/dunkin_logo.png">';
// 			html = html + '				설비관리';
			html = html + '			</div>';
			html = html + '			<div class="printQrCon">';
			html = html + '				<div class="printQr">';
			html = html + '					<img src="/attach/fileDownload.do?ATTACH_GRP_NO=' + attachGrpNoArr[i] + '&ATTACH=1" alt="QR코드">';
			html = html + '				</div>';
			html = html + '				<div class="printQrInfo">';
			html = html + '					<div class="printQrInfoLst">';
			html = html + '						<div>';
			html = html + '							<p>설비명</p>';
			html = html + '							' + equipmentNameArr[i];
			html = html + '						</div>';
			html = html + '					</div>';
			html = html + '					<div class="printQrInfoLst">';
			html = html + '						<div>';
			html = html + '							<p>설비코드</p>';
			html = html + '							' + equipmentUidArr[i];
			html = html + '						</div>';
			html = html + '					</div>';
			html = html + '				</div>';
			html = html + '			</div>';
			html = html + '			<div class="printQrLogo">';
// 			html = html + '				<h1><img src="/images/login/web/img_logo.png" alt="ASPN"></h1>';
			html = html + '				<h1><img src="/images/com/web/dunkin_logo.png" alt="dunkin"></h1>';
			html = html + '			</div>';
			html = html + '		</div>';
			html = html + '	</div>';
			
			
			if(cnt == i || parseInt(i%8) == 7){
				html = html + '	</div>';
				if(j!=0){
					html = html + '	</div>';
				}
				j++;
			}
		}
	}
	
	$('#printPage').append(html);
}

</script>

<div class="modal-dialog root" style="width:1200px;">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			<h4 class="modal-title">QR코드 인쇄</h4>
		</div>
		<div class="modal-body">
			<div class="modal-bodyIn" style="background:#f0f4f6;">
	           	
	           	<div class="printArea" id="idPrint">
					<div id="printPage" >
					
					</div>
				</div>
	           	
			</div>
		</div>
		<div class="modal-footer hasFooter" style="background:#fff;">
			<a href="#none" class="btn comm st01" id="regBtn" onclick="printDiv('idPrint'); return false;">인쇄하기</a> 
		</div>
	</div> <%-- modal-content : e --%>
</div> <%-- modal : e --%>

