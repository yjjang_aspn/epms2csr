<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld"                 %>
<%@ taglib prefix="c"	   uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"	   uri="http://java.sun.com/jstl/fmt"           %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: equipmentMasterMng.jsp
	Description : [설비마스터 관리] : 화면
    author		: 서정민
    since		: 2018.02.07
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2018.02.07	 	서정민		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<script type="text/javascript">

<%-- 전역변수 --%>
var focusedRow = null;		<%-- Focus Row : [라인] --%>
var focusedRow2 = null;		<%-- Focus Row : [설비] --%>
var focusedRow3 = null;		<%-- Focus Row : [팝업 : 기기이력] --%>
var focusedRow4 = null;		<%-- Focus Row : [팝업 : 관련 URL] --%>
var lastChildRow = null;	<%-- Last Child Row : [라인] --%>
var lastChildRow2 = null;	<%-- Last Child Row : [설비] --%>
var selRows = null;			<%-- 선택된 rows --%>

$(document).ready(function(){
	
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function (e) {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>	
	});	
});

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "CategoryList") {	<%-- 1. [라인] 클릭시 --%>

		if (focusedRow == null || focusedRow.CATEGORY != row.CATEGORY || row.Added == 1){
			
			<%-- 1-1. 포커스 --%>
			if(focusedRow != null){
				grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
			}
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			
			focusedRow = row;
			focusedRow2 = null;
			lastChildRow = row.lastChild;
			
			<%-- 1-2. 설비 목록 조회 --%>
			if (row.TREE != null && row.TREE != "" && row.Added != 1) {

				$('#COMPANY_ID').val(row.COMPANY_ID);		<%-- 회사코드 --%>
				$('#LOCATION').val(row.LOCATION);			<%-- 공장ID --%>
				$('#CATEGORY_TYPE').val(row.CATEGORY_TYPE);	<%-- 카테고리 유형 : 'LINE' --%>
				$('#CATEGORY').val(row.CATEGORY);			<%-- 라인ID --%>
				$('#CATEGORY_UID').val(row.CATEGORY_UID);	<%-- 기능위치 --%>
				$('#CATEGORY_NAME').val(row.CATEGORY_NAME);	<%-- 라인명 --%>
				$('#TREE').val(row.TREE);					<%-- 구조체 --%>
				$('#equipmentMasterListTitle').html("<spring:message code='epms.system' /> : " + "[ " + row.CATEGORY_NAME + " ]");  // 설비
				getEquipmentList();
			}
		}
	}
	else if(grid.id == "EquipmentMasterList") {	<%-- 2. [설비] 클릭시 --%>
	
		<%-- 2-1. 포커스  --%>
		if(focusedRow2 != null){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow2 = row;
		lastChildRow2 = row.lastChild;
		
	}
	else if(grid.id == "EquipmentHistoryList") {	<%-- 3. [팝업 : 기기이력] 클릭시 --%>
	
		<%-- 3-1. 포커스  --%>
		if(focusedRow3 != null){
			grid.SetAttribute(focusedRow3,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow3 = row;
		
	}
	else if(grid.id == "EquipmentUrlList") {	<%-- 4. [팝업 : 관련 URL] 클릭시 --%>
	
		<%-- 4-1. 포커스  --%>
		if(focusedRow4 != null){
			grid.SetAttribute(focusedRow4,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow4 = row;
		
	}
}

<%-- 선택된 라인에 해당되는 설비 조회 --%>
function getEquipmentList(){
	Grids.EquipmentMasterList.Source.Data.Url = "/epms/equipment/master/equipmentMasterListData.do?TREE=" + $('#TREE').val()
										+ "&CATEGORY_TYPE=" + $('#CATEGORY_TYPE').val()
										+ "&editYn=${editYn}";
	Grids.EquipmentMasterList.ReloadBody();	
}

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source, data, func){
	if(grid.id == "EquipmentMasterList"){
		if(data != ""){
			callLoadingBar();
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "EquipmentMasterList"){
					setTimeout($.unblockUI, 100);
				}
			}
		}
	}
}

<%-- QR코드 생성 --%>
function fnQrCreate(){
	
	if(confirm("QR코드를 일괄생성하시겠습니까?")){
		$.ajax({
			type : 'POST',
			url : '/epms/equipment/master/qrcodeCreateAjax.do',
			dataType : 'json',
			success : function(json) {	
				
				if(json.RES_CD == 'S'){
					alert("QR코드 일괄생성에 성공하였습니다.");
					Grids.EquipmentMasterList.Source.Data.Url = "/epms/equipment/master/equipmentMasterListData.do?TREE=" + $('#TREE').val()
														+ "&CATEGORY_TYPE=" + $('#CATEGORY_TYPE').val()
														+ "&editYn=${editYn}";
					Grids.EquipmentMasterList.ReloadBody();
				}
				else{
					alert("QR코드 일괄생성에 실패하였습니다.");
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
			}
		});
	}
}

<%-- QR코드 인쇄 : 1-1.인쇄 버튼(설정 팝업) --%>
function fnQrPrintConfig(){
	
	selRows = Grids.EquipmentMasterList.GetSelRows();
	if(selRows.length == 0){
		alert("인쇄하실 설비를 선택해주세요.");
		return;
	}
	fnOpenLayerWithParam('/epms/equipment/master/popQrcodePrintConfigForm.do','popQrcodePrintConfigForm');
}
<%-- QR코드 인쇄 : 1-2.인쇄 버튼(미리보기 팝업) --%>
function fnQrPrint(size){
	var url = '/epms/equipment/master/popQrcodePrintForm.do?size='+size;
  	fnOpenLayerWithParam(url,'popQrcodePrintForm');
}
<%-- QR코드 인쇄 : 2.설비명 --%>
function fnGetEquipmentNameArr(){
	var equipmentNameArr = new Array();
	for(var i=0; i <selRows.length;i++){
		equipmentNameArr[i] = Grids.EquipmentMasterList.GetValue(selRows[i], "EQUIPMENT_NAME");
	}
	return equipmentNameArr;
}
<%-- QR코드 인쇄 : 3.설비코드 --%>
function fnGetEquipmentUidArr(){
	var equipmentUidArr = new Array();
	for(var i=0; i <selRows.length;i++){
		equipmentUidArr[i] = Grids.EquipmentMasterList.GetValue(selRows[i], "EQUIPMENT_UID");
	}
	return equipmentUidArr;
}
<%-- QR코드 인쇄 : 4.모델명 --%>
function fnGetModelArr(){
	var modelArr = new Array();
	for(var i=0; i <selRows.length;i++){
		modelArr[i] = Grids.EquipmentMasterList.GetValue(selRows[i], "MODEL");
	}
	return modelArr;
}
<%-- QR코드 인쇄 : 5.제조사 --%>
function fnGetManufacturerArr(){
	var manufacturerArr = new Array();
	for(var i=0; i <selRows.length;i++){
		manufacturerArr[i] = Grids.EquipmentMasterList.GetValue(selRows[i], "MANUFACTURER");
	}
	return manufacturerArr;
}
<%-- QR코드 인쇄 : 6.자산번호 --%>
function fnGetAssetNoArr(){
	var assetNoArr = new Array();
	for(var i=0; i <selRows.length;i++){
		assetNoArr[i] = Grids.EquipmentMasterList.GetValue(selRows[i], "ASSET_NO");
	}
	return assetNoArr;
}
<%-- QR코드 인쇄 : 7.QR이미지 --%>
function fnGetAttachGrpNoArr(){
	var attachGrpNoArr = new Array();
	for(var i=0; i <selRows.length;i++){
		attachGrpNoArr[i] = Grids.EquipmentMasterList.GetValue(selRows[i], "ATTACH_GRP_NO2");
	}
	return attachGrpNoArr;
}

<%-- [라인변경] 버튼 --%>
function fnChangeLine(){
	
	selRows = Grids.EquipmentMasterList.GetSelRows();
	if(selRows.length > 0){
		Grids.popLineList.Source.Data.Url = "/sys/category/categoryData.do?CATEGORY_TYPE=LINE&LOCATION="+$('#LOCATION').val();
		Grids.popLineList.Reload();	
			
		fnModalToggle('popLineList');
	}
	else{
		alert("라인을 변경하실 설비를 선택해주세요.");
		return;
	}
}

<%-- 라인변경 (라인변경 팝업에서 선택이후) --%>
function fnSetLine(CATEGORY, CATEGORY_UID, CATEGORY_NAME){
	
	for(var i=0; i <selRows.length;i++){
		Grids["EquipmentMasterList"].SetValue(selRows[i], "LINE", CATEGORY, 1);
		Grids["EquipmentMasterList"].SetValue(selRows[i], "LINE_UID", CATEGORY_UID, 1);
		Grids["EquipmentMasterList"].SetValue(selRows[i], "LINE_NAME", CATEGORY_NAME, 1);
	}
}


<%-- 모달 호출 (param 전달용) --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- 모달 호출 (Param 전달 및 그리드 호출) --%>
function fnOpenLayerWithGrid(url, name){
	$.ajax({
		type: 'POST',
		url: url,
		dataType: 'json',
		processData: false, 
		contentType:false,
		success: function (json) {
			
			if(name == 'popEquipmentUrlList'){				<%-- URL 팝업일 경우 --%>
				
				$("#"+name+"_EQUIPMENT").val(isEmpty(json.item.EQUIPMENT)==true ? '' :json.item.EQUIPMENT);
				$("#"+name+"_LOCATION_NAME").text(isEmpty(json.item.LOCATION_NAME)==true ? '' : "["+json.item.LOCATION_NAME+"]");
				$("#"+name+"_LINE_NAME").text(isEmpty(json.item.LINE_NAME)==true ? '' : "["+json.item.LINE_NAME+"]");
				$("#"+name+"_EQUIPMENT_NAME").text(isEmpty(json.item.EQUIPMENT_NAME)==true ? '' : "["+json.item.EQUIPMENT_NAME+"]");
				$("#"+name+"_EQUIPMENT_UID").text(isEmpty(json.item.EQUIPMENT_UID)==true ? '' : "["+json.item.EQUIPMENT_UID+"]");
				
				Grids.EquipmentUrlList.Source.Data.Url = "/epms/equipment/master/equipmentUrlListData.do?EQUIPMENT=" + $('#popEquipmentUrlList_EQUIPMENT').val();
				Grids.EquipmentUrlList.ReloadBody();
			}	
			else if(name == 'popEquipmentHistoryList'){		<%-- 기기이력 팝업일 경우 --%>
			
				$("#"+name+"_EQUIPMENT").val(isEmpty(json.item.EQUIPMENT)==true ? '' :json.item.EQUIPMENT);
				$("#"+name+"_LOCATION_NAME").text(isEmpty(json.item.LOCATION_NAME)==true ? '' : "["+json.item.LOCATION_NAME+"]");
				$("#"+name+"_LINE_NAME").text(isEmpty(json.item.LINE_NAME)==true ? '' : "["+json.item.LINE_NAME+"]");
				$("#"+name+"_EQUIPMENT_NAME").text(isEmpty(json.item.EQUIPMENT_NAME)==true ? '' : "["+json.item.EQUIPMENT_NAME+"]");
				$("#"+name+"_EQUIPMENT_UID").text(isEmpty(json.item.EQUIPMENT_UID)==true ? '' : "["+json.item.EQUIPMENT_UID+"]");
				
				Grids.EquipmentHistoryList.Source.Data.Url = "/epms/equipment/history/equipmentHistoryListData.do?EQUIPMENT=" + $('#popEquipmentHistoryList_EQUIPMENT').val();
				Grids.EquipmentHistoryList.ReloadBody();
			}
			
			fnModalToggle(name);
		
		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
			loadEnd();
		}
	});
}

<%-- 첨부이미지 변경 이후 새로고침 --%>
function fnReload_attachEdit(){
	
	Grids.EquipmentMasterList.Source.Data.Url = "/epms/equipment/master/equipmentMasterListData.do?TREE=" + $('#TREE').val()
										+ "&CATEGORY_TYPE=" + $('#CATEGORY_TYPE').val()
										+ "&editYn=${editYn}";
	Grids.EquipmentMasterList.ReloadBody();	
	
	alert("정상적으로 등록 되었습니다.");
	
}

<%-- [추가] 버튼 --%>
function addRowGrid(gridNm) {

	if (gridNm == "CategoryList") {
		if (focusedRow == null || focusedRow == 'undefined') {	<%-- 선택된 라인이 없을 경우 --%>
			return false;
		}
		else {													<%-- 선택된 라인이 있을 경우, 하위 레벨 라인 추가 --%>
			
			Grids[gridNm].SetAttribute(focusedRow,null,"Expanded",1,1);
			
			var row = Grids[gridNm].AddRow(focusedRow, null, true);
			Grids[gridNm].SetValue(row, "CATEGORY_TYPE", focusedRow.CATEGORY_TYPE, 1);	<%-- 카테고리유형 : 'LINE'(라인) --%>
			Grids[gridNm].SetValue(row, "PARENT_CATEGORY", focusedRow.CATEGORY, 1);		<%-- 부모카테고리(라인) --%>
			Grids[gridNm].SetValue(row, "TREE", focusedRow.TREE, 1);					<%-- 구조체 --%>
			Grids[gridNm].SetValue(row, "DEPTH", focusedRow.DEPTH + 1, 1);				<%-- 레벨 --%>
			Grids[gridNm].SetValue(row, "COMPANY_ID", focusedRow.COMPANY_ID, 1);		<%-- 회사코드 --%>
			Grids[gridNm].SetValue(row, "LOCATION", focusedRow.LOCATION, 1);			<%-- 공장코드 --%>
			<%-- 출력순서 --%>
			if(lastChildRow == null || lastChildRow == 'undefined'){
				Grids[gridNm].SetValue(row, "SEQ_DSP", "1", 1);					
			}else{
				Grids[gridNm].SetValue(row, "SEQ_DSP", lastChildRow.SEQ_DSP+1, 1);
			}
			lastChildRow = row;
		}
	} else if (gridNm == "EquipmentMasterList") {
		
		if (focusedRow == null || focusedRow == 'undefined') {	<%-- 선택된 라인이 없을 경우 --%>
			return false;
		}
		else {
			if('${equipment_hierarchy}' == 'true'){	<%-- 설비마스터가 계층형 구조일 경우 --%>
			
				if (focusedRow2 == null || focusedRow2 == 'undefined') {	<%-- 선택된 설비가 없을 경우, 1레벨 설비 추가 --%>
					
					var row = Grids[gridNm].AddRow(null, Grids[gridNm].GetFirst(),true);
					Grids[gridNm].SetAttribute(row,"EQUIPMENT_UID","CanEdit","1",1);		<%-- 설비코드 수정 가능 --%>
					Grids[gridNm].SetValue(row, "COMPANY_ID", $('#COMPNAY_ID').val(), 1);	<%-- 회사코드 --%>
					Grids[gridNm].SetValue(row, "LOCATION", $('#LOCATION').val(), 1);		<%-- 위치 --%>
					Grids[gridNm].SetValue(row, "LINE", $('#CATEGORY').val(), 1);			<%-- 라인ID --%>
					Grids[gridNm].SetValue(row, "LINE_UID", $('#CATEGORY_UID').val(), 1);	<%-- 기능위치 --%>
					Grids[gridNm].SetValue(row, "LINE_NAME", $('#CATEGORY_NAME').val(), 1);	<%-- 라인명 --%>
					Grids[gridNm].SetValue(row, "DEPTH", "1", 1);
				} 
				else {														<%-- 선택된 설비가 있을 경우, 하위 레벨 설비 추가 --%>
					
					Grids[gridNm].SetAttribute(focusedRow2,null,"Expanded",1,1);
					
					var row = Grids[gridNm].AddRow(focusedRow2, null, true);
					Grids[gridNm].SetAttribute(row,"EQUIPMENT_UID","CanEdit","1",1);
					Grids[gridNm].SetValue(row, "COMPANY_ID", focusedRow2.COMPANY_ID, 1);		<%-- 회사코드 --%>
					Grids[gridNm].SetValue(row, "LOCATION", focusedRow2.LOCATION, 1);			<%-- 위치 --%>
					Grids[gridNm].SetValue(row, "LINE", focusedRow2.LINE, 1);					<%-- 라인ID --%>
					Grids[gridNm].SetValue(row, "LINE_UID", focusedRow2.LINE_UID, 1);			<%-- 기능위치 --%>
					Grids[gridNm].SetValue(row, "LINE_NAME", focusedRow2.LINE_NAME, 1);			<%-- 라인명 --%>
					Grids[gridNm].SetValue(row, "PARENT_EQUIPMENT", focusedRow2.EQUIPMENT, 1);	<%-- 부모설비 --%>
						Grids[gridNm].SetValue(row, "TREE", focusedRow2.TREE, 1);					<%-- 구조체 --%>
					Grids[gridNm].SetValue(row, "DEPTH", focusedRow2.DEPTH + 1, 1);				<%-- 레벨 --%>
					<%-- 출력순서 --%>
					if(lastChildRow2 == null || lastChildRow2 == 'undefined'){
						Grids[gridNm].SetValue(row, "SEQ_DSP", "1", 1);					
					}else{
						Grids[gridNm].SetValue(row, "SEQ_DSP", lastChildRow2.SEQ_DSP+1, 1);
					}
					lastChildRow2 = row;
				}
			}
			else{									<%-- 설비마스터가 비계층형 구조일 경우(단일레벨) --%>
				var row = Grids[gridNm].AddRow(null, Grids[gridNm].GetFirst(),true);
				Grids[gridNm].SetAttribute(row,"EQUIPMENT_UID","CanEdit","1",1);		<%-- 설비코드 수정 가능 --%>
				Grids[gridNm].SetValue(row, "COMPANY_ID", $('#COMPANY_ID').val(), 1);	<%-- 회사코드 --%>
				Grids[gridNm].SetValue(row, "LOCATION", $('#LOCATION').val(), 1);		<%-- 위치 --%>
				Grids[gridNm].SetValue(row, "LINE", $('#CATEGORY').val(), 1);			<%-- 라인ID --%>
				Grids[gridNm].SetValue(row, "LINE_UID", $('#CATEGORY_UID').val(), 1);	<%-- 기능위치 --%>
				Grids[gridNm].SetValue(row, "LINE_NAME", $('#CATEGORY_NAME').val(), 1);	<%-- 라인명 --%>
				Grids[gridNm].SetValue(row, "DEPTH", "1", 1);
			}
		}
		
	} else if (gridNm == "EquipmentUrlList") {
		var row = Grids[gridNm].AddRow(Grids[gridNm].GetLast(), null, true);
		Grids[gridNm].SetValue(row, "EQUIPMENT", $('#popEquipmentUrlList_EQUIPMENT').val(), 1);	<%-- 설비ID --%>
		Grids[gridNm].SetValue(row, "SEQ_DSP", Grids[gridNm].RowCount, 1);						<%-- 출력순서 --%>
		Grids[gridNm].SetValue(row, "DEL_YN", "N", 1);											<%-- 삭제여부 --%>
	}
}

<%-- 드래그시 출력순서 세팅 : 드래그한 row의  data 세팅 --%>
Grids.OnRowMove = function (grid, row, oldparent, oldnext){
	
	if(grid.id == "CategoryList"){	<%-- [라인]의 경우 --%>
		var location;
		var parent_category;
		var tree;
		var depth;
		
		if(row.parentNode.tagName != 'I'){	<%-- 1레벨로 이동할 경우 --%>
			location = "";
			parent_category = "";
			tree = row.CATEGORY;
			depth = 2;
		}
		else {								<%-- 2레벨 이하로 이동할 경우 --%>
			location = row.parentNode.LOCATION;
			parent_category = row.parentNode.CATEGORY;
			if(row.Added == true){
				tree = row.parentNode.TREE;
			}
			else{
				tree = row.parentNode.TREE+"$"+row.CATEGORY;
			}
			depth = row.parentNode.DEPTH+1;
		}
		
		<%-- 1. 공장코드 --%>
		grid.SetValue(row, "LOCATION", location, 1);	
		
		<%-- 2. 부모카테고리  ID --%>
		grid.SetValue(row, "PARENT_CATEGORY", parent_category, 1);
		
		<%-- 3. 구조체 --%>
		grid.SetValue(row, "TREE", tree, 1);
		
		<%-- 4. 레벨 --%>
		grid.SetValue(row, "DEPTH", depth, 1);
		
		var lineRow = grid.GetFirst(row);
		var endRow = grid.GetLast(row);
		
		if(!(lineRow == null || lineRow == 'undefined')){
			var gap = depth - grid.GetValue(lineRow, "DEPTH") + 1;
			
			for(lineRow; lineRow; lineRow = grid.GetNextVisible(lineRow)){
				
				grid.SetValue(lineRow, "DEPTH", grid.GetValue(lineRow, "DEPTH")+gap, 1);
				
				if(grid.GetValue(lineRow, "CATEGORY") == grid.GetValue(endRow, "CATEGORY")){
					break;
				}
			}
		}
		
		<%-- 5. 출력순서 --%>
		<%-- 5-1. 이동 이전 부모 이하 라인의 출력순서 변경 --%>
		var i =1;
		var lineRow2 = grid.GetFirst(oldparent);
		if(!(lineRow2 == null || lineRow2 == 'undefined')){
			do{
				
				grid.SetValue(lineRow2, "SEQ_DSP", i, 1);
				lineRow2 = lineRow2.nextSibling;
				i++;
				
			}while(!(lineRow2 == null || lineRow2 == 'undefined'));
		}
		<%-- 5-2. 이동 이후 부모 이하 라인의 출력순서 변경 --%>
		i=1;
		var lineRow3 = grid.GetFirst(row.parentNode);
		if(!(lineRow3 == null || lineRow3 == 'undefined')){
			do{
				
				grid.SetValue(lineRow3, "SEQ_DSP", i, 1);
				lineRow3 = lineRow3.nextSibling;
				i++;
				
			}while(!(lineRow3 == null || lineRow3 == 'undefined'));
		}			
	}
	else if(grid.id == "EquipmentMasterList"){	<%-- [설비]의 경우 --%>
	
		var location;
		var line;
		var line_name;
		var parent_equipment;
		var tree;
		var depth;
		
		if(row.parentNode.tagName != 'I'){	<%-- 1레벨로 이동할 경우 --%>
			location = $('#LOCATION').val();
			line = $('#CATEGORY').val();
			line_name = $('#CATEGORY_NAME').val();
			parent_equipment = "";
			tree = row.EQUIPMENT;
			depth = 1;
		}
		else {								<%-- 2레벨 이하로 이동할 경우 --%>
			location = row.parentNode.LOCATION;
			line = row.parentNode.LINE;
			line_name = row.parentNode.LINE_NAME;
			parent_equipment = row.parentNode.EQUIPMENT;
			if(row.Added == true){
				tree = row.parentNode.TREE;
			}
			else{
				tree = row.parentNode.TREE+"$"+row.EQUIPMENT;
			}
			depth = row.parentNode.DEPTH+1;
		}
		<%-- 1. 위치 --%>
		grid.SetValue(row, "LOCATION", location, 1);
		
		<%-- 2. 기능위치 --%>
		grid.SetValue(row, "LINE", line, 1);
		
		<%-- 3. 라인 --%>
		grid.SetValue(row, "LINE_NAME", line_name, 1);
		
		<%-- 4. 부모설비  ID --%>
		grid.SetValue(row, "PARENT_EQUIPMENT", parent_equipment, 1);
		
		<%-- 5. 구조체 --%>
		grid.SetValue(row, "TREE", tree, 1);
		
		<%-- 6. 레벨 --%>
		grid.SetValue(row, "DEPTH", depth, 1);	
		
		var lineRow = grid.GetFirst(row);
		var endRow = grid.GetLast(row);
		
		if(!(lineRow == null || lineRow == 'undefined')){
			var gap = depth - grid.GetValue(lineRow, "DEPTH") + 1;
			
			for(lineRow; lineRow; lineRow = grid.GetNextVisible(lineRow)){
				
				grid.SetValue(lineRow, "DEPTH", grid.GetValue(lineRow, "DEPTH")+gap, 1);
				
				if(lineRow.id == endRow.id){
					break;
				}
			}
		}
		
		<%-- 7. 출력순서 --%>
		<%-- 7-1. 이동 이전 부모 이하 설비의 출력순서 변경 --%>
		var i=1;
		var lineRow2 = grid.GetFirst(oldparent);
		if(!(lineRow2 == null || lineRow2 == 'undefined')){			
			if(lineRow2.DEPTH != "1"){
				do{
					grid.SetValue(lineRow2, "SEQ_DSP", i, 1);
					lineRow2 = lineRow2.nextSibling;
					i++;
					
				}while(!(lineRow2 == null || lineRow2 == 'undefined'));
			}
		}
		
		<%-- 7-2. 이동 이후 부모 이하 설비의 출력순서 변경 --%>
		i=1;
		var lineRow3 = grid.GetFirst(row.parentNode);
		if(!(lineRow3 == null || lineRow3 == 'undefined')){
			if(lineRow3.DEPTH != "1"){
				do{
					grid.SetValue(lineRow3, "SEQ_DSP", i, 1);
					lineRow3 = lineRow3.nextSibling;
					i++;
					
				}while(!(lineRow3 == null || lineRow3 == 'undefined'));
			}
		}			
	}
	else if(grid.id == "EquipmentUrlList"){	<%-- [설비 관련 URL(팝업)]의 경우 --%>
		var i=1;
		var lineRow = grid.GetFirst(row.parentNode);
		do{
			grid.SetValue(lineRow, "SEQ_DSP", i, 1);
			lineRow = lineRow.nextSibling;
			i++;
			
		}while(!(lineRow == null || lineRow == 'undefined'));
	}
};

<%-- Validation --%>
function dataValidation(gridNm){
	var cnt = 0;
	var chkCnt = 0;
	var grid = Grids[gridNm];
	
	if(gridNm == "CategoryList"){
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){  
			if(row.Changed || row.Added){
				
				cnt++;
				
				<%-- 라인명 --%>
				var CATEGORY_NAME = grid.GetValue(row, "CATEGORY_NAME");
				if(CATEGORY_NAME==''){
					chkCnt++;
					grid.SetAttribute(row,"CATEGORY_NAME","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"CATEGORY_NAME","Color","#FFFFAA",1);
				}
			}
		}
	}	
	else if(gridNm == "EquipmentMasterList"){
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow2);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){  
			if(row.Changed || row.Added){
				
				cnt++;
				
				<%-- 설비코드 --%>
				var EQUIPMENT_UID  = grid.GetValue(row, "EQUIPMENT_UID");
				if(EQUIPMENT_UID==''){
					chkCnt++;
					grid.SetAttribute(row,"EQUIPMENT_UID","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"EQUIPMENT_UID","Color","#FFFFAA",1);
				}

				<%-- 설비명 --%>
				var EQUIPMENT_NAME = grid.GetValue(row, "EQUIPMENT_NAME");
				if(EQUIPMENT_NAME==''){
					chkCnt++;
					grid.SetAttribute(row,"EQUIPMENT_NAME","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"EQUIPMENT_NAME","Color","#FFFFAA",1);
				}
			}
		}
	}
	else if(gridNm == "EquipmentUrlList"){
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow2);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){  
			if(row.Changed || row.Added){
				
				cnt++;
				
				<%-- URL 설명 --%>
				var URL_DESCRIPTION  = grid.GetValue(row, "URL_DESCRIPTION");
				if(URL_DESCRIPTION==''){
					chkCnt++;
					grid.SetAttribute(row,"URL_DESCRIPTION","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"URL_DESCRIPTION","Color","#FFFFAA",1);
				}

				<%-- URL 주소 --%>
				var URL_ADDRESS = grid.GetValue(row, "URL_ADDRESS");
				if(URL_ADDRESS==''){
					chkCnt++;
					grid.SetAttribute(row,"URL_ADDRESS","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"URL_ADDRESS","Color","#FFFFAA",1);
				}
			}
		}
	}
	
	<%-- 추가 및 변경된 데이터가 없을 경우 --%>
	if(cnt == 0){
		return false;
	}
	
	<%-- 필수값 체크 --%>
	if(chkCnt == 0){
		return true;
	}
	else{
		alert("필수값을 입력해주세요.");
		return false;
	}
}

<%-- [저장] 버튼 : 라인정보 및 설비정보 CRUD(관리자) --%>
function fnSave( gridNm ){
	
	console.log('# equipmentMasterMng.fnSave ( ' + gridNm + ' ) called ...');
	
	<%-- 유효성 체크 --%>
    if(!dataValidation(gridNm)){
    	return;
    }
	
	if ( Grids[gridNm].ActionValidate() ) {  // [YJJANG] Check for changes
		
		console.log('# ActionValidate >>> ( ' + Grids[gridNm].Source.Upload.Url + ' ) ');
		
		if(gridNm == "CategoryList"){
			if(confirm(" <spring:message code='epms.category' /> 정보를 저장하시겠습니까?")){    // 라인
	
				Grids[gridNm].ActionSave();
			}
		}
		else if(gridNm == "EquipmentMasterList"){
			if(confirm(" <spring:message code='epms.system' /> 정보를 저장하시겠습니까?")){    // 설비

				Grids[gridNm].ActionSave();
			}
		}
		else if(gridNm == "EquipmentUrlList"){
			if(confirm("<spring:message code='epms.system' /> 관련 URL 정보를 저장하시겠습니까?")){  // 설비

				Grids[gridNm].ActionSave();
			}
		}
		
	}
	// if ( Grids[gridNm].ActionValidate() )
	
}

<%-- 그리드 데이터 저장 후 --%> 
Grids.OnAfterSave  = function (grid){
	if(grid.id == "CategoryList"){
		
		grid.ReloadBody();
		
	} else if (grid.id == "EquipmentMasterList"){
		
		grid.ReloadBody();
		
	} else if (grid.id == "EquipmentUrlList"){
		
		fnModalToggle('popEquipmentUrlList');
		
	}
};

<%-- SAP 동기화 버튼(라인) --%>
function fnSyncLine(){
	if(confirm("라인 정보를 동기화하시겠습니까?")){
		$.ajax({
			type : 'POST',
			url : '/epms/equipment/master/updateFuncSiteInfo.do',
			dataType : 'json',
			contentType : false,
			success : function(json) {	
				
				if(json.E_RESULT == 'S'){
					alert("SAP동기화에 성공하였습니다.");
					Grids.CategoryList.ReloadBody();
				}
				else{
					alert("SAP동기화에 실패하였습니다.\n" + json.E_MESSAGE);
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
				setTimeout($.unblockUI, 100);
			}
		});
	}
}

<%-- SAP 동기화 팝업 호출(설비) --%>
function fnPopSyncEquipment(){
	
	fnOpenLayerWithParam('/epms/equipment/master/popSyncEquipment.do','popSyncEquipment');
}

<%-- SAP 동기화 버튼(설비) --%>
function fnSyncEquipment(sapSync){
	if(confirm("설비 정보를 동기화하시겠습니까?")){
		$.ajax({
			type : 'POST',
			url : '/epms/equipment/master/updateEquipInfoAjax.do?SAP_SYNC_FLAG='+sapSync,
			dataType : 'json',
			success : function(json) {	
				
				if(json.E_RESULT == 'S'){
					alert("SAP동기화에 성공하였습니다.");
					Grids.EquipmentMasterList.Source.Data.Url = "/epms/equipment/master/equipmentMasterListData.do?TREE=" + $('#TREE').val()
														+ "&CATEGORY_TYPE=" + $('#CATEGORY_TYPE').val()
														+ "&editYn=${editYn}";
					Grids.EquipmentMasterList.ReloadBody();
					fnModalToggle('popSyncEquipment');
				}
				else{
					alert("SAP동기화에 실패하였습니다.\n" + json.E_MESSAGE);
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
				setTimeout($.unblockUI, 100);
			}
		});
	}
}

</script>
</head>

<body>
<div id="contents">
	<input type="hidden" id="CATEGORY" name="CATEGORY"/>			<%-- 카테고리 ID --%>
	<input type="hidden" id="CATEGORY_TYPE" name="CATEGORY_TYPE"/>	<%-- 카테고리 유형 --%>
	<input type="hidden" id="CATEGORY_UID" name="CATEGORY_UID"/>	<%-- 카테고리 코드 --%>
	<input type="hidden" id="CATEGORY_NAME" name="CATEGORY_NAME"/>	<%-- 카테고리 명 --%>
	<input type="hidden" id="TREE" name="TREE"/>					<%-- 구조체 --%>
	<input type="hidden" id="COMPANY_ID" name="COMPANY_ID"/>		<%-- 회사코드 --%>
	<input type="hidden" id="LOCATION" name="LOCATION"/>			<%-- 위치(공장ID) --%>
	<input type="hidden" id="EQUIPMENT" name="EQUIPMENT"/>			<%-- 설비 ID --%>
	<input type="hidden" id="EQUIPMENT_NAME" name="EQUIPMENT_NAME"/><%-- 설비 명 --%>
	
	<div class="fl-box panel-wrap03 leftPanelArea" style="width: 20%; min-width: 350px;">
		<h5 class="panel-tit"><spring:message code='epms.category' /></h5><!-- 라인 -->
		<div class="panel-body">
			<div id="categoryList">
				<bdo	Debug="Error"
						Data_Url="/sys/category/categoryData.do?CATEGORY_TYPE=LINE"
						Layout_Url="/sys/category/categoryLayout.do?CATEGORY_TYPE=LINE"
						Upload_Url="/sys/category/categoryEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"
				>
				</bdo>
			</div>
		</div>
	</div>
	
	<div class="fl-box panel-wrap03 rightPanelArea">
		<h5 class="panel-tit mgn-l-10" id="equipmentMasterListTitle"><spring:message code='epms.system' /></h5><!-- 설비 -->
		<div class="panel-body mgn-l-10">
			<div id="equipmentMasterList">
				<bdo	Debug="Error"
						Layout_Url="/epms/equipment/master/equipmentMasterMngLayout.do"
						Upload_Url="/epms/equipment/master/equipmentMasterEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"	
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=equipmentMasterList.xls&dataName=data"
				>
				</bdo>
			</div>
		</div>
	</div>
</div>

<%-- SAP 설비 동기화 팝업 --%>
<div class="modal fade modalFocus" id="popSyncEquipment" data-backdrop="static" data-keyboard="false"></div>	<%-- fnSyncEquipmentPop.jsp --%>

<%-- QR코드인쇄 팝업(설정) --%>
<div class="modal fade modalFocus" id="popQrcodePrintConfigForm" data-backdrop="static" data-keyboard="false"></div>	<%-- popQrcodePrintConfigForm.jsp --%>

<%-- QR코드인쇄 팝업(미리보기) --%>
<div class="modal fade modalFocus" id="popQrcodePrintForm" data-backdrop="static" data-keyboard="false"></div>	<%-- popQrcodePrintForm.jsp --%>

<%-- 라인변경 팝업 --%>
<%@ include file="/WEB-INF/jsp/epms/equipment/master/popLineList.jsp"%>

<%-- 첨부파일 팝업(이미지 및 매뉴얼) --%>
<div class="modal fade modalFocus" id="popFileManageForm" data-backdrop="static" data-keyboard="false"></div>	<%-- popEquipmentImgView.jsp --%>

<%-- 기기이력 팝업 --%>
<%@ include file="/WEB-INF/jsp/epms/equipment/history/popEquipmentHistoryList.jsp"%>

<%-- URL 팝업 --%>
<%@ include file="/WEB-INF/jsp/epms/equipment/master/popEquipmentUrlList.jsp"%>

<%-- 설비마스터 상세 팝업 --%>
<div class="modal fade modalFocus" id="popEquipmentMasterDtl" data-backdrop="static" data-keyboard="false"></div>	<%-- popEquipmentMasterDtl.jsp --%>

<%-- 정비실적조회 --%>
<div class="modal fade modalFocus" id="popRepairResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 예방보전실적조회 --%>
<div class="modal fade modalFocus" id="popPreventiveResultForm" data-backdrop="static" data-keyboard="false"></div>

</body>
</html>