<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: repairResultListLayout.jsp
	Description : 메인 대시보드 - 정비실적 레이아웃
    author		: 김영환
    since		: 2018.06.19
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.06.19	 김영환		최초 생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="popReportPeriodLineRepairResultList"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		Filtering="1"				Dragging="0"
			Editing="1"				Deleting="0"		Selecting="0"		SuppressCfg="0"
			Paging="1"			  	PageLength="20"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="0"			PasteFocused="3"	CustomScroll="1"
	/>

	<Header	id="Header"	Align="center"
		REPAIR_STATUS			= "정비상태"
		REPAIR_TYPE				= "정비유형"
		REPAIR_RESULT			= "정비ID"
		LOCATION				= "위치"
		CATEGORY				= "기능위치"
		CATEGORY_NAME			= "라인"
		EQUIPMENT				= "설비ID"
		EQUIPMENT_UID			= "설비코드"
		EQUIPMENT_NAME			= "설비명"
		GRADE					= "설비등급"
		MODEL					= "모델명"
		MANUFACTURER			= "제조사"
		
		REPAIR_REQUEST			= "정비요청ID"
		PART					= "파트"
		REQUEST_TYPE			= "구분"
		REQUEST_DESCRIPTION		= "고장내용"
		ATTACH_GRP_NO1			= "첨부파일 코드"
		REQUEST_DIV				= "요청부서ID"
		REQUEST_DIV_NAME		= "요청부서"
		REQUEST_MEMBER			= "요청자ID"
		REQUEST_MEMBER_NAME		= "요청자"
		DATE_REQUEST			= "요청일"
		APPR_MEMBER_NAME		= "결재자"
		DATE_APPR				= "결재일"
		
		DATE_ASSIGN				= "배정일"
		DATE_PLAN				= "정비계획일"
		DATE_REPAIR				= "실적등록일"
		OUTSOURCING				= "외주"
		REPAIR_MEMBER			= "정비원"
		MATERIAL_USED			= "사용자재"
		MATERIAL_UNREG			= "사용자재(미등록)"
		TROUBLE_TYPE1			= "고장유형"
		TROUBLE_TYPE2			= "조치"
		REPAIR_DESCRIPTION		= "작업내용"
		MANAGER					= "담당자ID"
		MANAGER_NAME			= "담당자"
		TROUBLE_TIME1			= "고장시작시간"
		TROUBLE_TIME2			= "고장종료시간"
		trouble_time_hour		= "고장시간(h)"
		trouble_time_minute		= "고장시간(m)"
		ATTACH_GRP_NO2			= "첨부파일 코드"
		
		REPAIR_ANALYSIS			= "고장원인분석ID"
		SAVE_YN					= "분석여부"
		
		detail					= "실적상세"
		analysis				= "원인분석"
	/>
	
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
	
	<Cols>
		<c:choose>
			<c:when test="${param.flag eq 'repairResult'}">	
		<C Name="REPAIR_STATUS"				Type="Enum"		Align="left"		Visible="1"		Width="70"	CanEdit="0"	<tag:enum codeGrp="REPAIR_STATUS"/>/>
			</c:when>
			<c:when test="${param.flag eq 'maintenanceReview'}">
		<C Name="REPAIR_STATUS"				Type="Enum"		Align="left"		Visible="0"		Width="70"	CanEdit="0"	CanHide="0"	<tag:enum codeGrp="REPAIR_STATUS"/>/>
			</c:when>
			<c:otherwise>
			</c:otherwise>
		</c:choose>
		 
		
		<C Name="REPAIR_RESULT"				Type="Text"		Align="center"		Visible="0"		Width="60"	CanEdit="0"/>
		<C Name="LOCATION"					Type="Enum"		Align="left"		Visible="1"		Width="60"	CanEdit="0" <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>	
		<C Name="CATEGORY"					Type="Text"		Align="center"		Visible="0"		Width="60"	CanEdit="0"/>
		<C Name="CATEGORY_NAME"				Type="Text"		Align="left"		Visible="1"		Width="120"	CanEdit="0"/>
		<C Name="EQUIPMENT"					Type="Text"		Align="center"		Visible="0"		Width="60"	CanEdit="0"/>
		<C Name="EQUIPMENT_UID"				Type="Text"		Align="left"		Visible="1"		Width="80"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="EQUIPMENT_NAME"			Type="Text"		Align="left"		Visible="1"		Width="200"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		
		
		<c:choose>
			<c:when test="${param.flag eq 'repairResult'}">	
		<C Name="GRADE"						Type="Enum"		Align="left"		Visible="1"		Width="70"	CanEdit="0"	<tag:enum codeGrp="GRADE"/>/>
			</c:when>
			<c:when test="${param.flag eq 'maintenanceReview'}">
		<C Name="GRADE"						Type="Enum"		Align="left"		Visible="0"		Width="70"	CanEdit="0"	<tag:enum codeGrp="GRADE"/>/>
			</c:when>
			<c:otherwise>
			</c:otherwise>
		</c:choose>
		<C Name="MODEL"						Type="Text"		Align="left"		Visible="1"		Width="100"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="MANUFACTURER"				Type="Text"		Align="left"		Visible="1"		Width="100"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		
		<C Name="MATERIAL_USED"				Type="Text"		Align="left"		Visible="1"		Width="150"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="MATERIAL_UNREG"			Type="Text"		Align="left"		Visible="1"		Width="150"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="TROUBLE_TYPE1"				Type="Enum"		Align="left"		Visible="1"		Width="100"	CanEdit="0" <tag:enum codeGrp="TROUBLE1"/>/>
		<C Name="TROUBLE_TYPE2"				Type="Enum"		Align="left"		Visible="1"		Width="100"	CanEdit="0" <tag:enum codeGrp="TROUBLE2"/>/>
		<C Name="REPAIR_DESCRIPTION"		Type="Text"		Align="left"		Visible="1"		Width="200"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="TROUBLE_TIME1"				Type="Date"		Align="center"		Visible="1"		Width="130"	CanEdit="0"	Format="yyyy/MM/dd HH:mm"/>
		<C Name="TROUBLE_TIME2"				Type="Date"		Align="center"		Visible="1"		Width="130"	CanEdit="0"	Format="yyyy/MM/dd HH:mm"/>
		<C Name="trouble_time_hour"			Type="Int"		Align="center"		Visible="0"		Width="80"	CanEdit="0"/>
		<C Name="trouble_time_minute"		Type="Int"		Align="center"		Visible="1"		Width="80"	CanEdit="0"/>
		<C Name="ATTACH_GRP_NO2"			Type="Text"		Align="center"		Visible="0"		Width="80"	CanEdit="0"	CanHide="0"/>
		<C Name="REPAIR_TYPE"				Type="Enum"		Align="left"		Visible="1"		Width="70"	CanEdit="0"	<tag:enum codeGrp="REPAIR_TYPE" companyId="${ssCompanyId}"/>/>
		<C Name="REPAIR_MEMBER"				Type="Text"		Align="left"		Visible="1"		Width="120"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="OUTSOURCING"				Type="Text"		Align="left"		Visible="1"		Width="100"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>	
		
		
		<C Name="REPAIR_REQUEST"			Type="Text"		Align="center"		Visible="0"		Width="60"	CanEdit="0"/>
		<C Name="REQUEST_DESCRIPTION"		Type="Text"		Align="left"		Visible="1"		Width="200"	CanEdit="0" CaseSensitive="0" WhiteChars=" "/>
		<C Name="ATTACH_GRP_NO1"			Type="Text"		Align="center"		Visible="0"		Width="80"	CanEdit="0"	CanHide="0"/>
		<C Name="REQUEST_DIV"				Type="Text"		Align="left"		Visible="0"		Width="60"	CanEdit="0"	CanHide="0"/>
		<C Name="REQUEST_DIV_NAME"			Type="Text"		Align="left"		Visible="1"		Width="100"	CanEdit="0"/>
		<C Name="REQUEST_MEMBER"			Type="Text"		Align="left"		Visible="0"		Width="60"	CanEdit="0"/>
		<C Name="REQUEST_MEMBER_NAME"		Type="Text"		Align="left"		Visible="1"		Width="70"	CanEdit="0"/>
		<C Name="DATE_REQUEST"				Type="Date"		Align="center"		Visible="1"		Width="100"	CanEdit="0"	Format="yyyy/MM/dd"  />
		<C Name="APPR_MEMBER_NAME"			Type="Text"		Align="left"		Visible="1"		Width="70"	CanEdit="0"/>
		<C Name="DATE_APPR"					Type="Date"		Align="center"		Visible="1"		Width="100"	CanEdit="0"	Format="yyyy/MM/dd"  />
		
		
		
		
	</Cols>
	
	<RightCols>
		<C Name="REQUEST_TYPE"				Type="Enum"		Align="left"		Visible="1"		Width="70"	CanEdit="0"	<tag:enum codeGrp="REQUEST_TYPE"/>/> 
		<C Name="PART"						Type="Enum"		Align="left"		Visible="1"		Width="60"	CanEdit="0" <tag:enum codeGrp="PART" companyId="${ssCompanyId}"/>/>
		<C Name="DATE_ASSIGN"				Type="Date"		Align="center"		Visible="0"		Width="100"	CanEdit="0"	Format="yyyy/MM/dd"  />
		<c:choose>
			<c:when test="${param.flag eq 'repairResult'}">	
		<C Name="DATE_PLAN"					Type="Date"		Align="center"		Visible="1"		Width="100"	CanEdit="0"	Format="yyyy/MM/dd"  />
			</c:when>
			<c:when test="${param.flag eq 'maintenanceReview'}">
		<C Name="DATE_PLAN"					Type="Date"		Align="center"		Visible="0"		Width="100"	CanEdit="0"	Format="yyyy/MM/dd"  />
			</c:when>
			<c:otherwise>
			</c:otherwise>
		</c:choose>
		<C Name="DATE_REPAIR"				Type="Date"		Align="center"		Visible="1"		Width="100"	CanEdit="0"	Format="yyyy/MM/dd"  />
		
		 
<%-- 	<C Name="MANAGER"					Type="Enum"		Align="left"		Visible="1"		Width="70"	CanEdit="0" Enum='${repairMemberListOpt.MEMBER_NAME}'	EnumKeys='${repairMemberListOpt.MEMBER}'/> --%>	
		<C Name="MANAGER"					Type="Text"		Align="left"		Visible="0"		Width="70"	CanEdit="0" />
		<C Name="MANAGER_NAME"				Type="Text"		Align="left"		Visible="1"		Width="70"	CanEdit="0" />	
		<C Name="detail"					Type="Icon"		Align="center"		Visible="1"		Width="70"	CanEdit="0"	CanExport="1"/>
		
		<C Name="REPAIR_ANALYSIS"			Type="Text"		Align="center"		Visible="0"		Width="60"	CanEdit="0"/>
		<C Name="analysis"					Type="Icon"		Align="center"		Visible="0"		Width="70"	CanEdit="0"	CanExport="1"/>
		<C Name="SAVE_YN"					Type="Enum"		Align="center"		Visible="1"		Width="60"	CanEdit="0" Enum="|Y|N" EnumKeys="|Y|N"/>
	</RightCols>
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,항목,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        " 
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>