<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>

<%--
	Class Name	: reportRepairPeriodLineListLayout.jsp
	Description : 레포트 > 정비현황분석(라인별/상세) 레이아웃
    author		: 김영환
    since		: 2018.05.31
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.05.31	 김영환		최초 생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="ReportRepairPeriodLineList"/>
<Grid>
		<Cfg	id="${gridId}"
				IdChars="0123456789"    SortIcons="0"       Calculated="1"      CalculateSelected ="1"  NoFormatEscape="1"
	            NumberId="1"            DateStrings="2"     SelectingCells="0"  AcceptEnters="1"        Style="Office"
	            InEditMode="2"          SafeCSS='1'         NoVScroll="0"       NoHScroll="0"           EnterMode="0"
	            Filtering="1"           Dragging="0"        Deleting="0"        Adding="1"				ColMoving="0"
	            Selecting ="0"          Sorting = "1"       Sorted= "1"			SuppressCfg="0"
	            Paging="1"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"
				CopyCols="0"			PasteFocused="3"	CustomScroll="1"	StandardFilter="3"
				MainCol="CATEGORY_NAME"
		/>
		
	<Header	id="Header"	Align="center"
			
			LOCATION= "위치"
			CATEGORY_NAME="라인"
			EQUIPMENT="설비ID"
			CATEGORY="라인ID"
			EQUIPMENT_NAME="설비명"
			TOTAL="총 고장건"
			TREE="구조체"
			<c:forEach items="${partList}" var="list" varStatus="status">
				PART${list.PART} = "${list.PART_NAME}"
			</c:forEach>
			flag="선택여부"
	/>
		
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	<Pager Visible="0"/>
	
	
	<Cols>
		<C Name="LOCATION"				Type="Text" Align="left"   Visible="0" Width="210" CanEdit="0" CanExport="1" CanHide="1" RelWidth="210" Cursor="Hand" CaseSensitive="0" WhiteChars=" " />
		<C Name="CATEGORY_NAME"			Type="Text" Align="left"   Visible="1" Width="210" CanEdit="0" CanExport="1" CanHide="1" RelWidth="210" Cursor="Hand" CaseSensitive="0" WhiteChars=" " />
		<C Name="CATEGORY"				Type="Text" Align="left"   Visible="0" Width="90"  CanEdit="0" CanExport="1" CanHide="1" RelWidth="90"  Cursor="Hand" CaseSensitive="0" WhiteChars=" " />
		<C Name="EQUIPMENT"				Type="Text" Align="left"   Visible="0" Width="90"  CanEdit="0" CanExport="1" CanHide="1" RelWidth="90"  Cursor="Hand" CaseSensitive="0" WhiteChars=" " />
		<C Name="EQUIPMENT_NAME"		Type="Text" Align="left"   Visible="0" Width="200" CanEdit="0" CanExport="1" CanHide="1" RelWidth="200" Cursor="Hand" CaseSensitive="0" WhiteChars=" " />
		<C Name="TREE"					Type="Text" Align="left"   Visible="0" Width="100" CanEdit="0" CanExport="1" CanHide="1" RelWidth="100" Cursor="Hand" CaseSensitive="0" WhiteChars=" " />
		
		<C Name="TOTAL"					Type="Int"  Align="center" Visible="1" Width="83"  CanEdit="0" CanExport="1" CanHide="1" RelWidth="83" Cursor="Hand" CaseSensitive="0" WhiteChars=" " />
		<c:forEach items="${partList}" var="list" varStatus="status">
			<C Name="PART${list.PART}"	Type="Int"  Align="center" Visible="1" Width="83"  CanEdit="0" CanExport="1" CanHide="1" RelWidth="83" Cursor="Hand" CaseSensitive="0" WhiteChars=" " />
		</c:forEach>
		<C Name="flag"					Type="Text" Align="center" Visible="0" Width="70"  CanEdit="0" CanExport="1" CanHide="1" />
	</Cols>
	
	
	<Foot id="Foot">
		<I Calculated="1" Spanned="1" CATEGORY_NAME="합     계" CATEGORY_NAMESpan="2" CATEGORY_NAMEAlign="Center"  Class="GOHeaderRow GOHeaderText"
			
			TOTALFormula="sum()"		
			<c:forEach items="${partList}" var="list" varStatus="status">
				PART${list.PART}Formula="sum()"
			</c:forEach>
		/>
	</Foot>
	
	
	<Toolbar	Space="0"	Styles="1"	Cells="Empty,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1"	Empty="        "
				ColumnsType="Button"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
	
				
</Grid>