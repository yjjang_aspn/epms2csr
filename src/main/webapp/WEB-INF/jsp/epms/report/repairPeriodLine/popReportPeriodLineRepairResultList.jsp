<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: popMainRepairResultList.jsp
	Description : 대시보드-정비완료
    author		: 김영환
    since		: 2018.06.19
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2018.06.19	 	김영환		최초 생성

--%>

<!-- 고장관리 : 정비요청결재 -->
<script type="text/javascript">

var msg = "";
var focusedRow = null;

$(document).ready(function(){
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function (e) {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>	
	});	
	
});

<%-- row 값 계산 --%>
Grids.OnReady = function(grid){
	  
}

</script>

<div class="modal fade modalFocus" id="popReportPeriodLineRepairResultList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-80"> <!-- 원하는 width 값 입력 -->				
		 <div class="modal-content hgt-per-80" style="min-height:540px;">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="popReportPeriodLineRepairResultList_title">
			   	     <spring:message code='epms.work.perform' />
			   	</h4><!-- 정비 실적 -->
			</div>
			<div class="modal-body" >
	           	<div class="modal-bodyIn" style="height:calc(100% - 30px)">
						
					<!-- 트리그리드 : s -->
					<div id="PopMainRepairApprList" class="hgt-per-100">
						<bdo	Debug="Error"
									Layout_Url="/epms/report/repairPeriodLine/popReportPeriodLineRepairResultListLayout.do?flag=repairResult"
									Export_Data="data" Export_Type="xls"
									Export_Url="/sys/comm/exportGridData.jsp?File=정비현황 목록.xls&dataName=data"
								>
								</bdo>
					</div>
					<!-- 트리그리드 : e -->

				</div>
			</div>
		</div>
	</div>
</div>
						
				