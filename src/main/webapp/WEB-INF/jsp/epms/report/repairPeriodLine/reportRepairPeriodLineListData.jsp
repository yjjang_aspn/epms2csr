<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%--
	Class Name	: reportRepairPeriodLineListData.jsp
	Description : 레포트 > 정비현황분석(라인별/상세) 데이터
    author		: 김영환
    since		: 2018.05.31
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성

--%>

<Grid>
<Body>
	<B>
		<c:forEach var="item" items="${list}">
			<c:set var="nodeCnt" value="0" />
			<I  
				LOCATION			= "${item.LOCATION}"
				CATEGORY			= "${item.CATEGORY}" 			
				CATEGORY_NAME		= "${item.CATEGORY_NAME}" 
				TREE				= "${item.TREE}"
				TOTAL				= "${item.TOTAL}"
				<%-- 총 정비건 Link Class --%>	
				<c:if test="${item.TOTAL > 0}">
					TOTALClass		= "gridLink"
				</c:if>
				
				<c:forEach items="${partList}" var="list">
					<c:set var="key">PART${list.PART}</c:set>
					
					PART${list.PART} = "${item[key]}"
					
					<%-- PART01,02,03 정비건 Link Class --%>		
					<c:if test="${item[key] > 0}">
						PART${list.PART}Class				= "gridLink"
					</c:if>
				</c:forEach>

				flag				= "1"
				<c:choose>
					<c:when test="${item.CUR_LVL eq item.NEXT_LVL}">></I></c:when>
					<c:when test="${item.CUR_LVL < item.NEXT_LVL}">CanExpand='1' Expanded='1'></c:when>
					<c:when test="${item.CUR_LVL > item.NEXT_LVL}">></c:when>
					<c:otherwise>></c:otherwise>
				</c:choose>
				<c:set var="nodeCnt" value="${item.CUR_LVL - item.NEXT_LVL}" />
				
				<c:if test="${nodeCnt > 0}">
					<c:forEach var="item" begin="0" end="${nodeCnt}" step="1"></I></c:forEach>
				</c:if>
		</c:forEach>
	</B>
</Body>
</Grid>
