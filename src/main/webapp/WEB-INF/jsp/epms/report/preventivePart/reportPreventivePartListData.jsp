<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%--
	Class Name	: reportPreventivePartListData.jsp
	Description : 레포트 > 예방보전 현황(파트) 그리드 데이터
	author		: 김영환
	since		: 2018.05.30
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.05.30	 김영환		최초 생성

--%>


<Grid>
<Body>
	<B>	
	<c:forEach var="item" items="${list}">
		<%-- 첫 데이터일 경우 --%>
		<c:choose>
			<c:when test="${item.RNUM == 1}">
		 		
				<I  
					Background="#fffbe6" Spanned="1" Calculated="1"
					LOCATION					= "${item.LOCATION}"
					<c:if test="${item.CUR_LVL eq 0}">
						LOCATIONSpan = "2"
					</c:if>
					PART						= "${item.PART}"
					DATE_PLAN					= "${item.DATE_PLAN}"
					SUM_PLAN					= "${item.SUM_PLAN}"
					SUM_COMPLETE				= "${item.SUM_COMPLETE}"
					
					<%-- 합계 계획건 Link Class --%>	
					<c:if test="${item.SUM_PLAN > 0}">
						SUM_PLANClass				= "gridLink"
					</c:if>
					
					<%-- 합계 실시건 Link Class --%>	
					<c:if test="${item.SUM_COMPLETE > 0}">
						SUM_COMPLETEClass			= "gridLink"
					</c:if>
					
					<%-- 합계 PM률 --%>	
					<c:choose>
						<c:when test="${item.SUM_PLAN == 0}">
							SUM_PROGRESS				= "-"	SUM_PROGRESSType="Text" SUM_PROGRESSFormat=""
						</c:when>
						<c:otherwise>
							SUM_PROGRESSFormula			= "SUM_COMPLETE/SUM_PLAN"	
						</c:otherwise>  
					</c:choose>
					
					
					TBM_PLAN					= "${item.TBM_PLAN}"
					TBM_COMPLETE				= "${item.TBM_COMPLETE}"
					
					<%-- TBM 실시건 Link Class --%>	
					<c:if test="${item.TBM_PLAN > 0}">
						TBM_PLANClass				= "gridLink"
					</c:if>
					
					<%-- TBM 실시건 Link Class --%>	
					<c:if test="${item.TBM_COMPLETE > 0}">
						TBM_COMPLETEClass			= "gridLink"
					</c:if>
					
					<%-- TBM 실시율 --%>
					<c:choose>
						<c:when test="${item.TBM_PLAN == 0}">
							TBM_RATIO					= "-"	TBM_RATIOType="Text"	TBM_RATIOFormat=""
						</c:when>
						<c:otherwise>
							TBM_RATIOFormula			= "TBM_COMPLETE/TBM_PLAN"
						</c:otherwise>  
					</c:choose>
					
					CBM_PLAN					= "${item.CBM_PLAN}"
					CBM_COMPLETE				= "${item.CBM_COMPLETE}"			
					CBM_ERROR					= "${item.CBM_ERROR}"			
					CBM_MEASURE					= "${item.CBM_MEASURE}"
					
					<%-- CBM 계획건 Link Class --%>	
					<c:if test="${item.CBM_PLAN > 0}">
						CBM_PLANClass				= "gridLink"
					</c:if>	
					
					<%-- CBM 실시건 Link Class --%>	
					<c:if test="${item.CBM_COMPLETE > 0}">
						CBM_COMPLETEClass			= "gridLink"
					</c:if>	
					
					<%-- CBM 이상건 Link Class --%>	
					<c:if test="${item.CBM_ERROR > 0}">
						CBM_ERRORClass				= "gridLink"
					</c:if>	
					
					<%-- CBM 조치건 Link Class --%>	
					<c:if test="${item.CBM_MEASURE > 0}">
						CBM_MEASUREClass			= "gridLink"
					</c:if>	
					
					<%-- CBM 점검률 --%>
					<c:choose>
						<c:when test="${item.CBM_PLAN == 0}">
							CBM_PROGRESS				= "-"	CBM_PROGRESSType="Text"	CBM_PROGRESSFormat=""
						</c:when>
						<c:otherwise>
							CBM_PROGRESSFormula			= "CBM_COMPLETE/CBM_PLAN"
						</c:otherwise>  
					</c:choose>
					<%-- CBM 적중률 --%>
					<c:choose>
						<c:when test="${item.CBM_PLAN == 0 || item.CBM_ERROR == 0}">
							HIT_RATIO					= "-"	HIT_RATIOType="Text" HIT_RATIOFormat=""
						</c:when>
						<c:otherwise>
							HIT_RATIOFormula			= "CBM_ERROR/CBM_COMPLETE"
						</c:otherwise>  
					</c:choose>
					<%-- CBM 조치율 --%>
					<c:choose>
						<c:when test="${item.CBM_PLAN == 0 || item.CBM_ERROR == 0}">
							MEASURE_RATIO				= "-"	MEASURE_RATIOType="Text" MEASURE_RATIOFormat=""
						</c:when>
						<c:otherwise>
							MEASURE_RATIOFormula		= "CBM_MEASURE/CBM_ERROR"
						</c:otherwise>  
					</c:choose>
					<%-- CBM률 --%>
					<c:choose>
						<c:when test="${item.CBM_PLAN == 0}">
							CBM_RATIO					= "-"	CBM_RATIOType="Text"	CBM_RATIOFormat=""
						</c:when>
						<c:otherwise>
							CBM_RATIOFormula			= "(CBM_MEASURE/CBM_PLAN)"
						</c:otherwise>  
					</c:choose>	
				>
			</c:when>
			<c:otherwise>
				<I  
					<c:if test="${(item.CUR_LVL) ne 2}">
					Background="#fffbe6" Spanned="1"
					</c:if>
					Calculated="1"
					LOCATION					= "${item.LOCATION}"
					<c:if test="${item.CUR_LVL eq 0}">
						LOCATIONSpan = "2"
					</c:if>
					PART						= "${item.PART}"
					DATE_PLAN					= "${item.DATE_PLAN}"
					SUM_PLAN					= "${item.SUM_PLAN}"
					SUM_COMPLETE				= "${item.SUM_COMPLETE}"
					
					<%-- 합계 계획건 Link Class --%>	
					<c:if test="${item.SUM_PLAN > 0}">
						SUM_PLANClass				= "gridLink"
					</c:if>
					
					<%-- 합계 실시건 Link Class --%>	
					<c:if test="${item.SUM_COMPLETE > 0}">
						SUM_COMPLETEClass			= "gridLink"
					</c:if>
					
					<%-- 합계 PM률 --%>
					<c:choose>
						<c:when test="${item.SUM_PLAN == 0}">
							SUM_PROGRESS				= "-"	SUM_PROGRESSType="Text" SUM_PROGRESSFormat=""
						</c:when>
						<c:otherwise>
							SUM_PROGRESSFormula			= "SUM_COMPLETE/SUM_PLAN"	
						</c:otherwise>  
					</c:choose>
					
					
					TBM_PLAN					= "${item.TBM_PLAN}"
					TBM_COMPLETE				= "${item.TBM_COMPLETE}"
					<%-- TBM 실시건 Link Class --%>	
					<c:if test="${item.TBM_PLAN > 0}">
						TBM_PLANClass				= "gridLink"
					</c:if>
					
					<%-- TBM 실시건 Link Class --%>
					<c:if test="${item.TBM_COMPLETE > 0}">
						TBM_COMPLETEClass			= "gridLink"
					</c:if>
					<%-- TBM 실시율 --%>
					<c:choose>
						<c:when test="${item.TBM_PLAN == 0}">
							TBM_RATIO					= "-"	TBM_RATIOType="Text"	TBM_RATIOFormat=""
						</c:when>
						<c:otherwise>
							TBM_RATIOFormula			= "TBM_COMPLETE/TBM_PLAN"
						</c:otherwise>  
					</c:choose>
					
					CBM_PLAN					= "${item.CBM_PLAN}"
					CBM_COMPLETE				= "${item.CBM_COMPLETE}"			
					CBM_ERROR					= "${item.CBM_ERROR}"			
					CBM_MEASURE					= "${item.CBM_MEASURE}"
					<%-- CBM 계획건 Link Class --%>
					<c:if test="${item.CBM_PLAN > 0}">
						CBM_PLANClass				= "gridLink"
					</c:if>	
					
					<%-- CBM 실시건 Link Class --%>
					<c:if test="${item.CBM_COMPLETE > 0}">
						CBM_COMPLETEClass			= "gridLink"
					</c:if>	
					
					<%-- CBM 이상건 Link Class --%>
					<c:if test="${item.CBM_ERROR > 0}">
						CBM_ERRORClass				= "gridLink"
					</c:if>	
					
					<%-- CBM 조치건 Link Class --%>
					<c:if test="${item.CBM_MEASURE > 0}">
						CBM_MEASUREClass			= "gridLink"
					</c:if>	
					<%-- CBM 점검률 --%>
					<c:choose>
						<c:when test="${item.CBM_PLAN == 0}">
							CBM_PROGRESS				= "-"	CBM_PROGRESSType="Text"	CBM_PROGRESSFormat=""
						</c:when>
						<c:otherwise>
							CBM_PROGRESSFormula			= "CBM_COMPLETE/CBM_PLAN"
						</c:otherwise>  
					</c:choose>
					<%-- CBM 적중률 --%>
					<c:choose>
						<c:when test="${item.CBM_PLAN == 0 || item.CBM_ERROR == 0}">
							HIT_RATIO					= "-"	HIT_RATIOType="Text" HIT_RATIOFormat=""
						</c:when>
						<c:otherwise>
							HIT_RATIOFormula			= "CBM_ERROR/CBM_COMPLETE"
						</c:otherwise>  
					</c:choose>
					<%-- CBM 조치율 --%>
					<c:choose>
						<c:when test="${item.CBM_PLAN == 0 || item.CBM_ERROR == 0}">
							MEASURE_RATIO				= "-"	MEASURE_RATIOType="Text" MEASURE_RATIOFormat=""
						</c:when>
						<c:otherwise>
							MEASURE_RATIOFormula		= "CBM_MEASURE/CBM_ERROR"
						</c:otherwise>  
					</c:choose>
					<%-- CBM률 --%>	
					<c:choose>
						<c:when test="${item.CBM_PLAN == 0}">
							CBM_RATIO					= "-"	CBM_RATIOType="Text"	CBM_RATIOFormat=""
						</c:when>
						<c:otherwise>
							CBM_RATIOFormula			= "(CBM_MEASURE/CBM_PLAN)"
						</c:otherwise>  
					</c:choose>	
			</c:otherwise>
		</c:choose>
		
		<c:choose>
			<c:when test="${item.CUR_LVL eq item.NEXT_LVL}">></I></c:when>
			<c:when test="${item.CUR_LVL < item.NEXT_LVL}">CanExpand='1' Expanded='1'></c:when>
			<c:when test="${item.CUR_LVL > item.NEXT_LVL}">></c:when>
			<c:otherwise>></c:otherwise>
		</c:choose>
		<c:set var="nodeCnt" value="${item.CUR_LVL - item.NEXT_LVL}" />
		<c:if test="${nodeCnt > 0}">
			<c:forEach var="item" begin="0" end="${nodeCnt}" step="1"></I></c:forEach>
		</c:if>
	</c:forEach> 
	</B>
</Body>
</Grid>