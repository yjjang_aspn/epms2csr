<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%--
	Class Name	: preventiveResultListData.jsp
	Description : 예방보전관리 > 예방보전실적등록/조회 그리드 데이터
	author		: 김영환
	since		: 2018.05.09
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.05.09	 김영환		최초 생성

--%>



<c:set var="user_part" value="${sessionScope.ssPart}"/>
<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I	
			PREVENTIVE_RESULT				= "${item.PREVENTIVE_RESULT}"
			DATE_PLAN						= "${item.DATE_PLAN}"
			DATE_ASSIGN						= "${item.DATE_ASSIGN}"
			STATUS							= "${item.STATUS}"
			<c:if test="${item.STATUS == '03'}">
				STATUSClass="gridStatus3"
			</c:if>
			<c:if test="${item.STATUS == '02' || item.STATUS == '09'}">
				STATUSClass="gridStatus4"
			</c:if>
			MANAGER							= "${item.MANAGER}"
			MANAGER_NAME					= "${item.MANAGER_NAME}"
			
			PREVENTIVE_BOM					= "${item.PREVENTIVE_BOM}"
			WORK_TYPE						= "${item.WORK_TYPE}"
			WORK_TYPE_NAME					= "${fn:replace(item.WORK_TYPE_NAME, '"', '&quot;')}"
			PART							= "${item.PART}"
			PREVENTIVE_TYPE					= "${item.PREVENTIVE_TYPE}"
			CHECK_TYPE						= "${item.CHECK_TYPE}"
			CHECK_DETAIL          			= "${fn:replace(item.CHECK_DETAIL, '"', '&quot;')}"
			CHECK_STANDARD      			= "${fn:replace(item.CHECK_STANDARD, '"', '&quot;')}"
			CHECK_TOOL          			= "${fn:replace(item.CHECK_TOOL, '"', '&quot;')}"
			CHECK_CYCLE         			= "${item.CHECK_CYCLE}"
			
			EQUIPMENT           			= "${item.EQUIPMENT}"
			EQUIPMENT_UID       			= "${item.EQUIPMENT_UID}"
			EQUIPMENT_NAME          		= "${fn:replace(item.EQUIPMENT_NAME, '"', '&quot;')}"
			GRADE			       			= "${item.GRADE}"
			LOCATION						= "${item.LOCATION}"
			LINE	            			= "${item.LINE}"
			LINE_UID						= "${item.LINE_UID}"
			LINE_NAME          				= "${fn:replace(item.LINE_NAME, '"', '&quot;')}"
			
			LOCATIONPART					= "${item.LOCATIONPART}"
			
			ATTACH_GRP_NO					= "${item.ATTACH_GRP_NO}"
			CHECK_DECISION					= "${item.CHECK_DECISION}"
			<c:if test='${item.CHECK_DECISION eq "03"}'>
				CHECK_DECISIONClass="gridStatus4"
			</c:if>
			<c:if test='${item.CHECK_DECISION eq "01"}'>
				CHECK_DECISIONClass="gridStatus3"
			</c:if>
			<c:if test='${item.CHECK_DECISION eq "02"}'>
				CHECK_DECISIONClass="gridStatus3"
			</c:if>
			CHECK_VALUE						= "${item.CHECK_VALUE}"
			CHECK_DESCRIPTION				= "${item.CHECK_DESCRIPTION}"
			MATERIAL_USED					= "${fn:replace(item.MATERIAL_USED_NAME, '"', '&quot;')}"
			MATERIAL_UNREG					= "${fn:replace(item.MATERIAL_UNREG, '"', '&quot;')}"
			
			<c:if test='${item.STATUS eq "02" || item.STATUS eq "03" || item.STATUS eq "04"}'>
				detailSwitch="1"  detailIcon="/images/com/web/commnet_up.gif" 
				detailOnClickSideIcon="fnOpenLayerWithParam('/epms/preventive/result/popPreventiveResultForm.do?PREVENTIVE_RESULT='+Row.PREVENTIVE_RESULT,'popPreventiveResultForm');"
			</c:if>
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>