<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%--
	Class Name	: reportRepairMonthlyLineListData.jsp
	Description : 레포트 > 라인별 설비 고장분석(월별) 데이터
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성

--%>
<Grid>
<Body>
	<B>
		<c:forEach var="item" items="${list}">
			<I  
				CATEGORY_NAME		= "${item.CATEGORY_NAME}" 
				MATERIAL_UID		= "${item.MATERIAL_UID}"
				MATERIAL_NAME		= "${item.MATERIAL_NAME}"
				INPUT				= "${item.INPUT}"
				<c:if test="${item.INPUT > 0}">
					INPUTClass				= "gridLink"
				</c:if>
				<c:forEach var="item2" items="${list2}">
					
					<c:set var="tempName">INPUT_${fn:replace(item2, '-', '')}</c:set>
					
					${tempName}	= "${item[tempName]}"
					<c:if test="${item[tempName] > 0}">
						${tempName}Class				= "gridLink"
					</c:if>
				</c:forEach>
			/>
		</c:forEach>
	</B>
</Body>
</Grid>
