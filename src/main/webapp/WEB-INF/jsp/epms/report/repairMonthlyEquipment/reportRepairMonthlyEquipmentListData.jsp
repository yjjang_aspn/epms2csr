<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%--
	Class Name	: reportRepairMonthlyLineListData.jsp
	Description : 레포트 > 라인별 설비 고장분석(월별) 데이터
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성

--%>
<Grid>
<Body>
	<B>
	
	<c:forEach var="item" items="${list}">
		<c:set var="nodeCnt" value="0" />
		<I
			LOCATION				= "${item.LOCATION}"									<%-- 공장ID --%>
			LINE_NAME				= "${item.LINE_NAME}"									<%-- 라인명 --%>
			EQUIPMENT				= "${item.EQUIPMENT}"									<%-- 설비ID --%>
			EQUIPMENT_NAME			= "${fn:replace(item.EQUIPMENT_NAME, '"', '&quot;')}"	<%-- 설비명 --%>
			EQUIPMENT_UID			= "${item.EQUIPMENT_UID}"								<%-- 설비코드 --%>
			PARENT_EQUIPMENT		= "${item.PARENT_EQUIPMENT}"							<%-- 부모설비ID --%>
			TREE					= "${item.TREE}"										<%-- 구조체 --%>
			DEPTH					= "${item.DEPTH}"										<%-- 레벨 --%>
			SEQ_DSP					= "${item.SEQ_DSP}"										<%-- 출력순서 --%>
			TOTAL				= "${item.TOTAL}"
			MONTH_TOTAL         = "${item.MONTH_TOTAL}"
			YEAR_TOTAL          = "${item.YEAR_TOTAL}"
			
			<%-- 이달 총 정비건 Link Class --%>	
			<c:if test="${item.TOTAL > 0}">
				TOTALClass				= "gridLink"
			</c:if>
			
			<%-- 전달 총 정비건 Link Class --%>	
			<c:if test="${item.MONTH_TOTAL > 0}">
				MONTH_TOTALClass				= "gridLink"
			</c:if>
			
			<%-- 전년 총 정비건 Link Class --%>	
			<c:if test="${item.YEAR_TOTAL > 0}">
				YEAR_TOTALClass				= "gridLink"
			</c:if>
			
			<c:forEach items="${partList}" var="list">
				<c:set var="key">PART${list.PART}</c:set>
				<c:set var="MONTH_key">MONTH_PART${list.PART}</c:set>
				<c:set var="YEAR_key">YEAR_PART${list.PART}</c:set>
			
				PART${list.PART} = "${item[key]}"
				MONTH_PART${list.PART} = "${item[MONTH_key]}"
				YEAR_PART${list.PART} = "${item[YEAR_key]}"
				
				<%-- 이달 파트별 정비건 Link Class --%>	
				<c:if test="${item[key] > 0}">
					PART${list.PART}Class				= "gridLink"
				</c:if>
				
				<%-- 전달 파트별 정비건 Link Class --%>	
				<c:if test="${item[MONTH_key] > 0}">
					MONTH_PART${list.PART}Class				= "gridLink"
				</c:if>
				
				<%-- 전년 파트별 정비건 Link Class --%>	
				<c:if test="${item[YEAR_key] > 0}">
					YEAR_PART${list.PART}Class				= "gridLink"
				</c:if>
			</c:forEach>

			<c:choose>
				<c:when test="${item.CUR_LVL eq item.NEXT_LVL}">></I></c:when>
				<c:when test="${item.CUR_LVL < item.NEXT_LVL}">CanExpand='1' Expanded='1'></c:when>
				<c:when test="${item.CUR_LVL > item.NEXT_LVL}">></c:when>
				<c:otherwise>></c:otherwise>
			</c:choose>
			<c:set var="nodeCnt" value="${item.CUR_LVL - item.NEXT_LVL}" />
			<c:if test="${nodeCnt > 0}">
				<c:forEach var="item" begin="0" end="${nodeCnt}" step="1"></I></c:forEach>
			</c:if>
	</c:forEach> 
		
	</B>
</Body>
</Grid>
