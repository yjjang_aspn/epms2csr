<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: ReportRepairMonthlyEquipmentList.jsp
	Description : 레포트 > 정비현황 분석(월별) 메인화면
	author		: 김영환
	since		: 2018.04.04
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.04	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<style>
.inp-comm.monthPic {box-sizing:border-box;height:28px;padding:5px 25px 4px 6px;border:1px solid #cdd2da;
	background:#fff url('/images/com/web/ico_cal.png') no-repeat right 5px center;background-size: 17px auto;cursor:pointer;}
</style>

<script type="text/javascript">

<%-- 전역변수 --%>
var focusedRow = null;		<%-- Focus Row : [라인] --%>
var focusedRow2 = null;		<%-- Focus Row : [정비현확 분석(설비별/기준월)] --%>
var focusedRow3 = null;		<%-- Focus Row : [팝업 : 정비실적 현황] --%>
var originDt = '';			<%-- 검색 기준월 --%>

$(document).ready(function() {
	
	$('.monthPic').monthPicker();
	 
	//정비현황 분석 (월별) 검색
	$('#srcBtn').on('click', function(){
		
		if(focusedRow == null) {
			alert("<spring:message code='epms.category' />을 먼저 선택하세요");  // 라인
		}else{
			getRepairMonthlyEquipmentList();	
		}
	});
	
});

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "CategoryList"){		<%-- 1. [라인] 클릭시 --%>
		<%-- 1-1. 포커스 --%>
		if(focusedRow != null){
			grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow = row;
		
		<%-- 1-2. 설비 목록 조회 --%>
		if (row.TREE != null && row.TREE != "" && row.Added != 1) {
			
			$('#CATEGORY_TYPE').val(row.CATEGORY_TYPE);	<%-- 카테고리 유형 : 'LINE' --%>
			$('#TREE').val(row.TREE);					<%-- 구조체 --%>
			$('#RepairMonthlyEquipmentListTitle').html("정비현황 분석(설비별/기준월) : " + "[ " + row.CATEGORY_NAME + " ]");
			getRepairMonthlyEquipmentList();
		}
		
	}
	else if(grid.id == "ReportRepairMonthlyEquipmentList") { <%-- [정비현확 분석(설비별/기준월)] 클릭시 --%>
		<%-- 포커스 시작 --%>
		if(focusedRow2 != null){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		focusedRow2 = row;
		
	}
	else if(grid.id == "PopRepairDetailList") { <%-- [팝업 : 정비상세현황] 클릭시 --%>
		<%-- 포커스 시작 --%>
		if(focusedRow3 != null){
			grid.SetAttribute(focusedRow3,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		focusedRow3 = row;
		
	}
	
}

<%-- 선택된 라인에 해당되는 설비 조회 --%>
function getRepairMonthlyEquipmentList(){
	
	originDt = $("#searchDt").val();
	var srcDate = $("#searchDt").val().replace('-', '');
	
	Grids.ReportRepairMonthlyEquipmentList.Source.Layout.Url = "/epms/report/repairMonthlyEquipment/reportRepairMonthlyEquipmentListLayout.do?searchDt="+srcDate;
	Grids.ReportRepairMonthlyEquipmentList.Source.Data.Url = "/epms/report/repairMonthlyEquipment/reportRepairMonthlyEquipmentListData.do?searchDt="+srcDate
											  + "&TREE=" + $('#TREE').val()
											  + "&originDt=" + originDt
											  + "&flag=" + $('#view_type').val();
	Grids.ReportRepairMonthlyEquipmentList.Reload();	
}

<%-- 통계 컬럼 클릭 시 정비목록 이벤트 --%>
function fnToggleRepairDetail(row, col, part, date){
	
	if(row.id == "Header" || row.id == "Header2" || row.id == "Toolbar" || row.id == "Filter" || row.id == "Panel" || row.id == "NoData" ){ 
		return;
	}
	
	var count = Grids['ReportRepairMonthlyEquipmentList'].GetValue(row, col);

	if(count != 0){
		var tree = '';
		var equipment = '';
		
		if(row.id != "Foot"){
			equipment = row.EQUIPMENT;
		} else {
			if(focusedRow != null) tree = focusedRow.TREE;
			equipment = 'ALL';
		}
		
		Grids.PopRepairDetailList.Source.Data.Url = "/epms/report/repairMonthlyLine/popRepairDetailListData.do?TREE=" + tree
													+ "&EQUIPMENT=" + equipment 
													+ "&PART="+ part
													+ "&searchDt=" + date
													+ "&originDt=" + originDt;
		Grids.PopRepairDetailList.ReloadBody();
		fnModalToggle('popRepairDetailList');
	}
	
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}


</script>
</head>
<body>

<%-- 화면 UI Area Start --%>
<div id="contents">
	<input type="hidden" id="CATEGORY_TYPE" name="CATEGORY_TYPE"/>	<%-- 카테고리 유형 : 'LINE' --%>
	<input type="hidden" id="TREE" name="TREE"/>					<%-- 구조체 --%>
	
	<div class="fl-box panel-wrap03 leftPanelArea" style="width:20%;">
		<h5 class="panel-tit"><spring:message code='epms.category' /></h5><!-- 라인 -->
		<div class="panel-body">
			<div id="categoryList">
				<bdo	Debug="Error"
						Data_Url="/sys/category/categoryData.do?CATEGORY_TYPE=LINE"
						Layout_Url="/sys/category/categoryNotEditLayout.do?CATEGORY_TYPE=LINE"
				>
				</bdo>
			</div>
		</div>
	</div>
	
	<div class="fl-box panel-wrap04" style="width:80%">
		<h5 class="panel-tit mgn-l-10" id="RepairMonthlyEquipmentListTitle"><spring:message code='epms.work' />현황 분석(<spring:message code='epms.system' />별/기준월)</h5><!-- 정비현황 , 설비별 -->
		<div class="inq-area-inner ab">
			<ul class="wrap-inq">
				<li class="inq-clmn">
					<h4 class="tit-inq">기준월</h4>
					<div class="prd-inp-wrap" style="width:100px;">
						<input type="text" id="searchDt" class="inp-comm monthPic inpCal" readonly="readonly" title="검색일" value="${searchDt}" />
					</div>
				</li>
				<li class="inq-clmn">
					<h4 class="tit-inq">검색유형</h4>
					<div class="sel-wrap type02" style="width:150px;">
						<select title="구분" id="view_type" name="view_type">
							<option value="1" selected="selected">실적 있는 설비 조회</option><!-- 정비실적 -->
							<option value="2">전체 조회</option>
						</select>
					</div>
				</li>
			</ul>
			<a href="#none" id="srcBtn" class="btn comm st01">검색</a>
		</div>
		<div class="panel-body mgn-l-10 mgn-t-30">
			<div id="ReportRepairMonthlyEquipmentList">
				<bdo	Debug="Error"
						Data_Url=""
						Layout_Url="/epms/report/repairMonthlyEquipment/reportRepairMonthlyEquipmentListLayout.do?flag=1&searchDt=${searchDt}"
				>
				</bdo>
			</div>
		</div>
	</div>
	
</div>

<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<%-- 고장 상세조회 --%>
<%@ include file="/WEB-INF/jsp/epms/report/repairMonthlyLine/popRepairDetailList.jsp"%>

<%-- 정비실적조회 --%>
<div class="modal fade modalFocus" id="popRepairResultForm" data-backdrop="static" data-keyboard="false"></div>

</body>
</html>