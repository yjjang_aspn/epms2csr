<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%--
	Class Name	: reportRepairMemberListData.jsp
	Description : 레포트 > 정비원별 작업시간
    author		: 김영환
    since		: 2019.03.29
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2019.03.29	 	김영환		최초 생성

--%>

<Grid>
<Body>
	<B>	
	<c:forEach var="item" items="${list}">
		<%-- 첫 데이터일 경우 --%>
		<c:choose>
			<c:when test="${item.RNUM == 1}">
		 		
				<I  
					Background="#fffbe6" Spanned="1" Calculated="1"
					USER_ID						= "${item.USER_ID}"
					NAME						= "${item.NAME}"
					LOCATION					= "${item.LOCATION}"
					<c:choose>
						<c:when test="${item.CUR_LVL eq 0}">
						NAMESpan = "2"
						REPAIR_CNTFormula			= "sum()"
						WORK_TIMEFormula			= "sum()"
						</c:when>
						<c:otherwise>
						REPAIR_CNT					= "${item.REPAIR_CNT}"
						WORK_TIME					= "${item.WORK_TIME}"
						</c:otherwise>
					</c:choose>
					
					<%-- 합계 계획건 Link Class --%>	
					<c:if test="${item.REPAIR_CNT > 0}">
						REPAIR_CNTClass				= "gridLink"
					</c:if>
					
					<%-- 합계 실시건 Link Class --%>	
					<c:if test="${item.WORK_TIME > 0}">
						WORK_TIMEClass			= "gridLink"
					</c:if>
					
			</c:when>
			<c:otherwise>
				<I  
					<c:if test="${(item.CUR_LVL) ne 1}">
					Background="#fffbe6" Spanned="1"
					</c:if>
					Calculated="1"
					USER_ID						= "${item.USER_ID}"
					NAME						= "${item.NAME}"
					LOCATION					= "${item.LOCATION}"
					<c:choose>
						<c:when test="${item.CUR_LVL eq 0}">
						NAMESpan = "2"
						REPAIR_CNTFormula			= "sum()"
						WORK_TIMEFormula			= "sum()"
						</c:when>
						<c:otherwise>
						REPAIR_CNT					= "${item.REPAIR_CNT}"
						WORK_TIME					= "${item.WORK_TIME}"
						</c:otherwise>
					</c:choose>
					
					<%-- 합계 계획건 Link Class --%>	
					<c:if test="${item.REPAIR_CNT > 0}">
						REPAIR_CNTClass				= "gridLink"
					</c:if>
					
					<%-- 합계 실시건 Link Class --%>	
					<c:if test="${item.WORK_TIME > 0}">
						WORK_TIMEClass			= "gridLink"
					</c:if>
										
			</c:otherwise>
		</c:choose>
		
		<c:choose>
			<c:when test="${item.CUR_LVL eq item.NEXT_LVL}">></I></c:when>
			<c:when test="${item.CUR_LVL < item.NEXT_LVL}">CanExpand='1' Expanded='1'></c:when>
			<c:when test="${item.CUR_LVL > item.NEXT_LVL}">></c:when>
			<c:otherwise>></c:otherwise>
		</c:choose>
		<c:set var="nodeCnt" value="${item.CUR_LVL - item.NEXT_LVL}" />
		<c:if test="${nodeCnt > 0}">
			<c:forEach var="item" begin="0" end="${nodeCnt}" step="1"></I></c:forEach>
		</c:if>
	</c:forEach> 
	</B>
</Body>
</Grid>