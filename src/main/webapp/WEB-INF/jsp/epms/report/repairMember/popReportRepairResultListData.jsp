<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%--
	Class Name	: popReportRepairResultListData.jsp
	Description : 레포트 > 정비원별 작업시간 > 정비원별 정비실적 레이어 팝업
    author		: 김영환
    since		: 2019.04.01
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2019.04.01		김영환		최초생성

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I
			REPAIR_RESULT			= "${item.REPAIR_RESULT}"

			LOCATION				= "${item.LOCATION}"
			CATEGORY				= "${item.CATEGORY}"
			CATEGORY_NAME			= "${fn:replace(item.CATEGORY_NAME, '"', '&quot;')}"
			EQUIPMENT				= "${item.EQUIPMENT}"
			EQUIPMENT_UID			= "${item.EQUIPMENT_UID}"
			EQUIPMENT_NAME			= "${fn:replace(item.EQUIPMENT_NAME, '"', '&quot;')}"
			
			REPAIR_REQUEST			= "${item.REPAIR_REQUEST}"
			PART					= "${item.PART}"
			REQUEST_DESCRIPTION		= "${fn:replace(item.REQUEST_DESCRIPTION, '"', '&quot;')}"
			REQUEST_DIV				= "${item.REQUEST_DIV}"
			REQUEST_DIV_NAME		= "${item.REQUEST_DIV_NAME}"
			REQUEST_MEMBER			= "${item.REQUEST_MEMBER}"
			REQUEST_MEMBER_NAME		= "${item.REQUEST_MEMBER_NAME}"
			DATE_REQUEST			= "${item.DATE_REQUEST}"
			APPR_MEMBER				= "${item.APPR_MEMBER}"
			APPR_MEMBER_NAME		= "${item.APPR_MEMBER_NAME}"
			DATE_APPR				= "${item.DATE_APPR}"
			
			REPAIR_TYPE				= "${item.REPAIR_TYPE}"
			REPAIR_STATUS			= "${item.REPAIR_STATUS}"
			DATE_ASSIGN				= "${item.DATE_ASSIGN}"
			DATE_PLAN				= "${item.DATE_PLAN}"
			DATE_REPAIR				= "${item.DATE_REPAIR}"
			TROUBLE_TYPE1			= "${item.TROUBLE_TYPE1}"
			TROUBLE_TYPE2			= "${item.TROUBLE_TYPE2}"
			MANAGER					= "${item.MANAGER}"
			MANAGER_NAME			= "${item.MANAGER_NAME}"
			OUTSOURCING				= "${fn:replace(item.OUTSOURCING, '"', '&quot;')}"
			TROUBLE_TIME1			= "${item.TROUBLE_TIME1}"
			TROUBLE_TIME2			= "${item.TROUBLE_TIME2}"
			TROUBLE_TILE_HOUR		= "${item.TROUBLE_TIME_HOUR}"
			TROUBLE_TILE_MINUTE		= "${item.TROUBLE_TIME_MINUTE}"
			REPAIR_DESCRIPTION		= "${fn:replace(item.REPAIR_DESCRIPTION, '"', '&quot;')}"
			MATERIAL_USED			= "${fn:replace(item.MATERIAL_USED_NAME, '"', '&quot;')}"
			MODEL					= "${fn:replace(item.MODEL, '"', '&quot;')}"
			MANUFACTURER			= "${fn:replace(item.MANUFACTURER, '"', '&quot;')}"
			DETAILSwitch="1"  DETAILIcon="/images/com/web/commnet_up.gif" 
			DETAILOnClickSideIcon="fnOpenLayerWithParam('/epms/repair/result/popRepairResultForm.do?REPAIR_RESULT='+Row.REPAIR_RESULT+'&EQUIPMENT='+Row.EQUIPMENT,'popRepairResultForm');"
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>