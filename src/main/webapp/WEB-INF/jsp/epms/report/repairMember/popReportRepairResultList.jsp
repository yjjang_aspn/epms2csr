<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: popReportRepairResultList.jsp
	Description : 레포트 > 정비원별 작업시간 > 정비원별 정비실적 레이어 팝업
    author		: 김영환
    since		: 2019.04.01
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2019.04.01		김영환		최초생성

--%>

<div class="modal fade modalFocus" id="popReportRepairResultList" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-80">			
		 <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">정비실적 현황</h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
					<div class="" id="popReportRepairResultList" style="height:93%">	
						<bdo	Debug="Error"
								Data_Url=""
								Layout_Url="/epms/report/repairMember/popReportRepairResultListLayout.do"
								Export_Data="data" Export_Type="xls"
						>
						</bdo>
					</div>	
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>
