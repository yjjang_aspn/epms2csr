<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: reportRepairMemberListLayout.jsp
	Description : 레포트 > 정비원별 작업시간
    author		: 김영환
    since		: 2019.03.29
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2019.03.29	 	김영환		최초 생성

--%>

<c:set var="gridId" value="ReportRepairMemberList"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1"			SuppressCfg="0"	
			Editing="1"				Deleting="0"		Selecting="0"		Dragging="0"
			Filtering="1"			StandardFilter="3"	ClearSelected="2"
			CopyCols="3"			PasteFocused="3"	CustomScroll="1"
			MainCol="NAME"
	/>
	
	<Pager Visible="0"/>
	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>
	<Head>
		<Header	id="Header"	Align="center"
				Spanned					= '1'
				NAME					= "정비원"
				NAMERowSpan				= "2"
				LOCATION				= "공장"
				LOCATIONRowSpan			= "2"
	
				REPAIR_CNT				= "합   계"
				REPAIR_CNTSpan			= "2"
		/>
		
		<Header	Align="center"
				Spanned='1'
				NAME					= ""
				REPAIR_CNT				="정비건"
				WORK_TIME				="정비시간"
				
		/>
		
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<Cols> 
		<C Name="NAME"						Type="Text" Align="left"   Visible="1" Width="200" CanEdit="0" CanExport="1" CanHide="1" Spanned="1" />
		<C Name="LOCATION"					Type="Enum" Align="left"   Visible="1" Width="200" CanEdit="0" CanExport="1" CanHide="1" Spanned="1" <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>
		<C Name="USER_ID"					Type="Text" Align="center" Visible="0" Width="150" CanEdit="0" CanExport="1" CanHide="1" />
				
		<C Name="REPAIR_CNT"				Type="Int"  Align="center" Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" Cursor="Hand" CaseSensitive="0" WhiteChars=" " />
		<C Name="WORK_TIME"					Type="Int"  Align="center" Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" Cursor="Hand" CaseSensitive="0" WhiteChars=" " Format="0.00"/>
		
	</Cols>
	
	<Foot>
		<I id="Foot" Calculated="1" Spanned="1" NAME="합          계" NAMESpan="2" NAMEType="Text" NAMEAlign="Center"  Class="GOHeaderRow GOHeaderText"
			REPAIR_CNTFormula			= "sum()"
		    WORK_TIMEFormula			= "sum()"
		    
		/>
	</Foot>
	
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,접기,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        " 
				ColumnsType="Button"
				접기Type="Html" 접기="&lt;a href='#none' title='접기' id=&quot;Folding&quot; class=&quot;icon folderClose&quot;
					onclick='fnExpandAll(&quot;${gridId}&quot;)'>&lt;/a>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>