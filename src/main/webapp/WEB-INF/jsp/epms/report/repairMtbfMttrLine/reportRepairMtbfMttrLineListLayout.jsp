<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%--
	Class Name	: reportRepairMtbfMttrLineListLayout.jsp
	Description : 레포트 > MTBF/MTTR(라인별) 레이아웃
    author		: 김영환
    since		: 2018.06.07
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.06.07	 김영환		최초 생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="ReportRepairMtbfMttrLineList"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"    SortIcons="0"       Calculated="1"      CalculateSelected ="1"  NoFormatEscape="1"
            NumberId="1"            DateStrings="2"     SelectingCells="0"  AcceptEnters="1"        Style="Office"
            InEditMode="2"          SafeCSS='1'         NoVScroll="0"       NoHScroll="0"           EnterMode="0"
            Filtering="1"           Dragging="0"        Deleting="0"        Adding="1"				ColMoving="0"
            Selecting ="0"          Sorting = "1"       Sorted= "1"			SuppressCfg="0"
            Paging="1"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="0"			PasteFocused="3"	CustomScroll="1"    StandardFilter="3"
			MainCol="CATEGORY_NAME"
	/>
	<Head>
		<Header	id="Header"	Align="center" 	Spanned="1"
			CATEGORY_NAME        = "<spring:message code='epms.category' />"  <%  // 라인  %>
			CATEGORY_NAMERowSpan = "2"
			CNT_TOT = "합계"
			CNT_TOTSpan = "3"
			<c:forEach var="item" items="${list}" varStatus="idx">			    
				CNT_${fn:replace(item, '-', '')}      = "${item}"
				CNT_${fn:replace(item, '-', '')}Span  = "3"
			</c:forEach>
		/>
		<Header	Align="center" Spanned="1"
			CNT_TOT  = "<spring:message code='epms.work.urgency' />"
			MTBF_TOT = "MTBF(Hr)"
			MTTR_TOT = "MTTR(Min)"
			<c:forEach var="item" items="${list}" varStatus="idx">	
				CNT_${fn:replace(item, '-', '')}      	= "<spring:message code='epms.work.urgency' />"  <%  // 돌발정비건  %>
				MTBF_${fn:replace(item, '-', '')}      	= "MTBF(Hr)"		    
				MTTR_${fn:replace(item, '-', '')}      	= "MTTR(Min)"
			</c:forEach>
		/>
	</Head>
		
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	<Pager Visible="0"/>
	
	<LeftCols>
		<C Name="CATEGORY_NAME"			Type="Text" Align="left"   Visible="1" Width="325" CanEdit="0" CanExport="1" CanHide="0" RelWidth="325" MinWidth="325" Cursor="Hand" Spanned="1" CaseSensitive="0" WhiteChars=" " />
		<C Name="TREE"					Type="Text" Align="left"   Visible="0" Width="100" CanEdit="0" CanExport="1" CanHide="0" RelWidth="100" MinWidth="100" Cursor="Hand" Spanned="0" CaseSensitive="0" WhiteChars=" " />
	</LeftCols>
	
	<Cols>
		<C Name="CNT_TOT"				Type="Int"  Align="center" Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="0" RelWidth="100" MinWidth="100"/>
		<C Name="MTBF_TOT"				Type="Int"  Align="right"  Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="0" RelWidth="100" MinWidth="100"/>
		<C Name="MTTR_TOT"				Type="Int" Align="right"   Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="0" RelWidth="100" MinWidth="100"/>
		<c:forEach var="item" items="${list}" varStatus="idx">
			<C Name="CNT_${fn:replace(item, '-', '')}"	Type="Int" Align="center" Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="0" RelWidth="100" MinWidth="100"/>
			<C Name="MTBF_${fn:replace(item, '-', '')}"	Type="Int" Align="right"  Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="0" RelWidth="100" MinWidth="100"/>
			<C Name="MTTR_${fn:replace(item, '-', '')}"	Type="Int" Align="right"  Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="0" RelWidth="100" MinWidth="100"/>
		</c:forEach>
	</Cols>
	
	
	<Toolbar	Space="0"	Styles="1"	Cells="Empty,접기,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        "
				ColumnsType="Button"
				접기Type="Html" 접기="&lt;a href='#none' title='접기' id=&quot;Folding&quot; class=&quot;icon folderClose&quot;
					onclick='fnExpandAll(&quot;${gridId}&quot;)'>&lt;/a>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>