<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%--
	Class Name	: reportRepairMonthlyLineListData.jsp
	Description : 레포트 > 라인별 설비 고장분석(월별) 데이터
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성

--%>
<Grid>
<Body>
	<B>
		<c:forEach var="item" items="${list}">
			<I  
				CATEGORY_NAME		= "${item.CATEGORY_NAME}" 
				TREE				= "${item.TREE}"
				CNT_TOT				= "${item.CNT_TOT}"
				MTBF_TOT			= "${item.MTBF_TOT}"
				MTTR_TOT			= "${item.MTTR_TOT}"
				<c:choose>
					<c:when test="${item.MTBF_TOT.matches('[0-9]+')}">
						MTBF_TOTType="Int"
						MTTR_TOTType="Int"
					</c:when>
					<c:otherwise>
						MTBF_TOTType="Text"
						MTTR_TOTType="Text"
					</c:otherwise>
				</c:choose>
				
				<c:forEach var="item2" items="${list2}">
					<c:set var="YYYYMM">${fn:replace(item2, '-', '')}</c:set>
					<c:set var="CNT_YYYYMM">CNT_${fn:replace(item2, '-', '')}</c:set>
					<c:set var="MTBF_YYYYMM">MTBF_${fn:replace(item2, '-', '')}</c:set>
					<c:set var="MTTR_YYYYMM">MTTR_${fn:replace(item2, '-', '')}</c:set>
					
					CNT_${YYYYMM}  = "${item[CNT_YYYYMM]}"
					MTBF_${YYYYMM} = "${item[MTBF_YYYYMM]}"
					MTTR_${YYYYMM} = "${item[MTTR_YYYYMM]}"
					
					<c:choose>
						<c:when test="${item[MTBF_YYYYMM].matches('[0-9]+')}">
							MTBF_${YYYYMM}Type="Int"
							MTTR_${YYYYMM}Type="Int"
						</c:when>
						<c:otherwise>
							MTBF_${YYYYMM}Type="Text"
							MTTR_${YYYYMM}Type="Text"
						</c:otherwise>
					</c:choose>
				</c:forEach>
				
				<c:choose>
					<c:when test="${item.CUR_LVL eq item.NEXT_LVL}">></I></c:when>
					<c:when test="${item.CUR_LVL < item.NEXT_LVL}">CanExpand='1' Expanded='1'></c:when>
					<c:when test="${item.CUR_LVL > item.NEXT_LVL}">></c:when>
					<c:otherwise>></c:otherwise>
				</c:choose>
				<c:set var="nodeCnt" value="${item.CUR_LVL - item.NEXT_LVL}" />
				
				<c:if test="${nodeCnt > 0}">
					<c:forEach var="item" begin="0" end="${nodeCnt}" step="1"></I></c:forEach>
				</c:if>
		</c:forEach>
	</B>
</Body>
</Grid>