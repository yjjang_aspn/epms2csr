<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%--
	Class Name	: reportRepairMtbfMttrLineList.jsp
	Description : [레포트-MTBF/MTTR 분석(라인별)] 메인화면
	author		: 김영환
	since		: 2018.06.07
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.06.07	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<style>
.inp-comm.monthPic {box-sizing:border-box;height:28px;padding:5px 25px 4px 6px;border:1px solid #cdd2da;
	background:#fff url('/images/com/web/ico_cal.png') no-repeat right 5px center;background-size: 17px auto;cursor:pointer;}
</style>
<script type="text/javascript">

<%-- 전역변수 --%>
var focusedRow = null;		<%-- Focus Row : [MTBF/MTTR(라인별)] --%>

$(document).ready(function() {
	
	$('.monthPic').monthPicker();
	
	//설비고장 분석 (월별) 검색
	$('#srcBtn').on('click', function(){
		getRepairMtbfMttrLineList();	
	});
	
	<%-- monthPicker 선택시 --%>
	$("#monthList").click(function(){
		
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#endDt').val($('#startDt').val());
		}
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#startDt').val($('#endDt').val());
		}
		
	});
	
});

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "ReportRepairMtbfMttrLineList") { <%-- [MTBF/MTTR(라인별)] 클릭시 --%>
		<%-- 포커스 시작 --%>
		if(focusedRow != null){
			grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		focusedRow = row;
		
	}
}

<%-- 선택된 라인에 해당되는 설비 조회 --%>
function getRepairMtbfMttrLineList(){
	var startDt = $("#startDt").val();
	var endDt = $("#endDt").val()
	
	var startDt1 = new Date(startDt);
	var endDt1= new Date(endDt);

	var diff = endDt1 - startDt1;
	var currMonth = 30 * 24 * 60 * 60 * 1000 ;
	
	$("#diffMonth").val(parseInt(diff/currMonth));
	
	Grids.ReportRepairMtbfMttrLineList.Source.Layout.Url = "/epms/report/repairMtbfMttrLine/reportRepairMtbfMttrLineListLayout.do?startDt="+startDt
														 + "&endDt="+endDt
														 + "&diffMonth=" + $('#diffMonth').val();
	Grids.ReportRepairMtbfMttrLineList.Source.Data.Url = "/epms/report/repairMtbfMttrLine/reportRepairMtbfMttrLineListData.do?startDt="+startDt
													   + "&endDt="+endDt
													   + "&diffMonth=" + $('#diffMonth').val()
													   + "&flag=" + $('#view_type').val();
	Grids.ReportRepairMtbfMttrLineList.Reload();
}

</script>
</head>
<body>

<%-- 화면 UI Area Start --%>
<div id="contents">
	<input type="hidden" id="diffMonth" name="diffMonth"/>			<%-- 시작월 종료월 차이--%>
	
	<div class="fl-box panel-wrap04" style="width:100%">
		<h5 class="panel-tit" id="RepairMtbfMttrLineListTitle">MTBF/MTTR (<spring:message code='epms.category' />별)</h5><!-- 라인 -->
		<div class="inq-area-inner ab">
			<ul class="wrap-inq">
				<li class="inq-clmn">
					<h4 class="tit-inq">검색일</h4>
					<div class="prd-inp-wrap">
						<span class="prd-inp">
							<span class="inner">
								<input type="text" id="startDt" class="inp-comm monthPic" readonly="readonly" title="검색시작일" value="${searchDt}">
							</span>
						</span>	
						<span class="prd-inp">
							<span class="inner">
								<input type="text" id="endDt" class="inp-comm  monthPic" readonly="readonly" title="검색종료일" value="${searchDt}">
							</span>
						</span>	
					</div>
				</li>
				<li class="inq-clmn">
					<h4 class="tit-inq">검색유형</h4>
					<div class="sel-wrap type02" style="width:150px;">
						<select title="구분" id="view_type" name="view_type">
							<option value="1" selected="selected">실적 있는 <spring:message code='epms.category' /> 조회</option><!-- 라인 -->
							<option value="2">전체 조회</option>
						</select>
					</div>
				</li>
			</ul>
			<a href="#none" id="srcBtn" class="btn comm st01">검색</a>
		</div>
		<div class="panel-body mgn-t-30">
			<div id="ReportRepairMtbfMttrLineList">
				<bdo	Debug="Error"
						Data_Url=""
						Layout_Url="/epms/report/repairMtbfMttrLine/reportRepairMtbfMttrLineListLayout.do?startDt=${searchDt}"
				>
				</bdo>
			</div>
		</div>
	</div>
	
</div>
</body>
</html>