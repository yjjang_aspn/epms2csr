<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: reportRepairPeriodEquipmentList.jsp
	Description : 레포트 > 정비현황분석(설비별/상세)
	author		: 김영환
	since		: 2018.06.12
 
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2018.06.12		김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script src="/js/chartjs/Chart.min.js" type="text/javascript"></script>
<script type="text/javascript">

<%-- 전역변수 --%>
var focusedRow = null;		<%-- Focus Row : [라인] --%>
var focusedRow2 = null;		<%-- Focus Row : [설비별 정비현황] --%>
var focusedRow3 = null;		<%-- Focus Row : [정비실적 현황] --%>
var callLoadingYn = true;	<%-- 로딩바 호출여부 --%>

$(document).ready(function() {
	
	<%-- 전체 loading bar intercept 회피 --%>
	$.ajaxSetup({global: false});
	
	<%-- datePic 설정 --%>
	$('.datePic').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	});
	
	<%-- 검색 시작일이 종료일보다 이후인지 체크 (이후라면, 종료일을 시작일로 변경) --%>
	$('#startDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#endDt').val($('#startDt').val());
		}
	});
	
	<%-- 검색 종료일이 시작일보다 이전인지 체크 (이전이라면, 시작일을 종료일로 변경) --%>
	$('#endDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#startDt').val($('#endDt').val());
		}
	});
	
	<%-- 설비고장 분석 (월별) 검색 --%>
	$('#srcBtn').on('click', function(){
		
		if(focusedRow == null) {
			alert("<spring:message code='epms.category' />을 먼저 선택하세요");  // 라인
		}else{
			var search_flag="srcBtn";
			getRepairPeriodEquipmentList(search_flag);
		}
	});
	
	$("#search_startDt").val($('#startDt').val().replaceAll("-",""));
	$("#search_endDt").val($('#endDt').val().replaceAll("-",""));
	
	
		
});

<%-- 해당 라인,설비에 대한 고장상세 현황  조회 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "CategoryList"){		<%-- 1. [라인] 클릭시 --%>
		
		if (focusedRow == null || focusedRow.CATEGORY != row.CATEGORY || focusedRow2 != null){
			<%-- 1-1. 포커스 --%>
			if(focusedRow != null){
				grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
			}
			grid.SetAttribute(row,null,"Color","#FFFFAA",1);
			focusedRow = row;
			focusedRow2 = null;
			focusedRow3 = null;
			
			<%-- 1-2. 설비별 정비현황 목록 조회 --%>
			$('#EQUIPMENT').val('');					<%-- 라인 선택시 EQUIPMENT 초기화--%>
			$('#PART').val('');							<%-- 라인 선택시 PART 초기화 --%>
			$('#CATEGORY_TYPE').val(row.CATEGORY_TYPE);	<%-- 카테고리 유형 : 'LINE' --%>
			$('#TREE').val(row.TREE);					<%-- 구조체 --%>
			$('#LOCATION').val(row.LOCATION);			<%-- 위치 --%>
			$('#ReportRepairPeriodEquipmentListTitle').html("설비별 정비 현황 : " + "[ " + row.CATEGORY_NAME + " ]");
			$('#ReportRepairDetailListTitle').html("정비실적 현황");
			var search_flag="";
			getRepairPeriodEquipmentList(search_flag);
			
		}
	}
	else if(grid.id == "ReportRepairPeriodEquipmentList") {		<%-- 2. [설비별 정비 현황] 클릭시 --%>
		
		<%-- 2-1. 포커스 시작  --%>
		if(focusedRow2 != null){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		focusedRow2 = row;
		focusedRow3 = null;

		var count = Grids['ReportRepairPeriodEquipmentList'].GetValue(row, col);
		
		if(count != 0){
			<%-- 2-2. 정비실적 현황 조회 --%>
			if(row.id == "AR1"){		<%-- 합계영역 카운트 클릭시 --%>
				$('#EQUIPMENT').val('');
				$('#TREE').val(focusedRow.TREE);
				$('#ReportRepairDetailListTitle').html("정비실적 현황");
			}else{
				$('#EQUIPMENT').val(row.EQUIPMENT);
				$('#TREE').val(row.TREE);
				$('#ReportRepairDetailListTitle').html("정비실적 현황 : " + "[ " + row.EQUIPMENT_UID + " / " + row.EQUIPMENT_NAME+ " ]");	
			}
			
			var colId = col.substring(0, 4);
			var part = "";
			if(colId == "PART"){
				part = col.substring(4, col.length);
			}
			$('#PART').val(part);
			
			getReportRepairDetailList();	<%-- 정비실적 현황 조회 --%>
		}
			
 	}
	
	if(grid.id == "ReportRepairDetailList"){
		
		<%-- 1-1. 포커스 --%>
		if(focusedRow3 != null){
			grid.SetAttribute(focusedRow3,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		focusedRow3 = row;
		
	}
	
}

<%-- 선택된 라인에 해당되는 설비별 정비현황 조회 --%>
function getRepairPeriodEquipmentList(search_flag){
	var startDt=null;
	var endDt=null;
	
	if(search_flag=="srcBtn"){ <%-- 검색 버튼 클릭시 : 'srcBtn', 그리드 클릭시 : '' --%>
		startDt =  $("#startDt").val().replaceAll('-', '');
		endDt =  $("#endDt").val().replaceAll('-', '');
		$("#search_startDt").val(startDt);
		$("#search_endDt").val(endDt);
		$("#TREE").val(focusedRow.TREE);
		$("#EQUIPMENT").val('');
	}
	
	var view_type = $("#view_type").val();
	Grids.ReportRepairPeriodEquipmentList.Source.Layout.Url = "/epms/report/repairPeriodEquipment/reportRepairPeriodEquipmentListLayout.do?flag="+view_type;
	Grids.ReportRepairPeriodEquipmentList.Source.Data.Url =   "/epms/report/repairPeriodEquipment/reportRepairPeriodEquipmentListData.do?flag="+view_type
															  +	"&startDt=" + $("#search_startDt").val()
															  + "&endDt=" + $("#search_endDt").val()
															  + "&TREE=" + $('#TREE').val()
															  + "&EQUIPMENT=" + $('#EQUIPMENT').val();
	Grids.ReportRepairPeriodEquipmentList.ReloadBody();
}

<%-- 정비실적 현황 조회 --%>
function getReportRepairDetailList(){
	Grids.ReportRepairDetailList.Source.Data.Url = "/epms/report/repairPeriodEquipment/reportTroubleDetailEquipmentListData.do?"
												+ "TREE=" +  $('#TREE').val()
												+ "&startDt=" + $("#search_startDt").val()
												+ "&endDt=" + $("#search_endDt").val()
												+ "&PART=" + $('#PART').val()
												+ "&EQUIPMENT=" + $('#EQUIPMENT').val()
												+ "&LOCATION=" + $('#LOCATION').val();
	Grids.ReportRepairDetailList.ReloadBody();
	
	fnGetRepairResultInfo(); 		<%-- 그래프 호출  --%>
}

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source, data, func){
	
	if(grid.id == "ReportRepairPeriodEquipmentList"){			<%-- [설비별 정비현황] 목록 조회시 --%>
		if(data != ""){
			callLoadingBar();
			callLoadingYn = false;
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "ReportRepairPeriodEquipmentList"){
					getReportRepairDetailList();
				}
			}
		}
	}
	else if(grid.id == "ReportRepairDetailList"){				<%-- [정비실적 현황] 목록 조회시 --%>
		if(data != ""){
			if(callLoadingYn == true){
				callLoadingBar();
				callLoadingYn = false;
			}
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "ReportRepairDetailList"){
					setTimeout($.unblockUI, 100);
					callLoadingYn = true;
				}
			}
		}
	}
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

function fnReload_afterCancel(){
	alert("<spring:message code='epms.repair.complete' />가 취소되었습니다.");  // 정비완료
	fnModalToggle('popRepairResultForm');
	Grids.ReportRepairDetailList.ReloadBody();
}

</script>

</head>

<body>
<%-- 화면 UI Area Start --%>
<div id="contents">

<input type="hidden" id="chart_Tit" name="chart_Tit" value=""/>
<input type="hidden" id="target" name="target" value=""/>
<input type="hidden" id="PART" name="PART" />
<input type="hidden" id="curLine" name="curLine"/>
<input type="hidden" id="LOCATION" name="LOCATION" />
<input type="hidden" id="EQUIPMENT" name="EQUIPMENT" />
<input type="hidden" id="TREE" name="TREE"/>
<input type="hidden" id="search_startDt" name="search_startDt"/>
<input type="hidden" id="search_endDt"   name="search_endDt"/>

	<h5 class="panel-tit" style="top:25px;">정비현황 분석(설비별/상세)</h5>
	<div class="inq-area-top mgn-l-10 mgn-t-30">
		<ul class="wrap-inq">
			<li class="inq-clmn" >
				<h4 class="tit-inq">검색일</h4>
				<div class="prd-inp-wrap" style="width:300px;">
					<span class="prd-inp" >
						<span class="inner">
							<input type="text" id="startDt" value="${startDt}" class="inp-comm datePic inpCal" readonly="readonly" title="검색시작일">
						</span>
					</span>	
					<span class="prd-inp">
						<span class="inner">
							<input type="text" id="endDt" value="${endDt}" class="inp-comm datePic inpCal" readonly="readonly" title="검색종료일">
						</span>
					</span>	
				</div>
			</li>
			<li class="inq-clmn">
				<h4 class="tit-inq">검색유형</h4>
				<div class="sel-wrap type02" style="width:150px;">
					<select title="구분" id="view_type" name="view_type">
						<option value="1" selected="selected">정비실적 있는 설비 조회</option>
						<option value="2">전체 조회</option>
					</select>
				</div>
			</li>
		</ul>
		<a href="#none" id="srcBtn" class="btn comm st01">검색</a>
	</div>
	<div class="fl-box panel-wrap02 mgn-t-10" style="width:15%;">
		<h5 class="panel-tit"><spring:message code='epms.category' /></h5><!-- 라인 -->
		<div class="panel-body">
			<div id="categoryList">
				<bdo	Debug="Error"
						Data_Url="/sys/category/categoryData.do?CATEGORY_TYPE=LINE"
						Layout_Url="/sys/category/categoryNotEditLayout.do?CATEGORY_TYPE=LINE"
				>
				</bdo>
			</div>
		</div>
	</div>
	<div class="fl-box panel-wrap02 mgn-t-10" style="width:35%;">
		<h5 class="panel-tit mgn-l-10"><spring:message code='epms.system' />별 <spring:message code='epms.work' />현황</h5><!-- 설비별  , 정비현황 -->
		<div class="panel-body mgn-l-10">
			<div id="ReportRepairPeriodEquipmentList">
				<bdo	Debug="Error"
						Data_Url=""
						Layout_Url="/epms/report/repairPeriodEquipment/reportRepairPeriodEquipmentListLayout.do?flag=1"		
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=reportRepairPeriodList.xls&dataName=data"
				>
				</bdo>
			</div>
		</div>
	</div>
	<div class="fl-box panel-wrap02 mgn-t-10" style="width:50%;">
		<div class="fl-box panel-wrap02-top" style="width:100%;">
			<h5 class="panel-tit mgn-l-10" id="ReportRepairDetailListTitle"><spring:message code='epms.work' />실적 현황</h5><!-- 정비실적 -->
			<div class="panel-body mgn-l-10">
				<div id="ReportTroubleDetailLineList">
					<bdo	Debug="Error"
							Data_Url=""
							Layout_Url="/epms/report/repairPeriodEquipment/reportTroubleDetailEquipmentListLayout.do?flag=1"
							Export_Data="data" Export_Type="xls"
							Export_Url="/sys/comm/exportGridData.jsp?File=TroubleDetailList.xls&dataName=data"
					>
					</bdo>
				</div>
			</div>
		</div>
		<div class="fl-box panel-wrap02-btm" style="width:100%;">
			<div class="panel-body mgn-l-10 mgn-t-10 overflow">
				<div class="fl-box wd-per-50 rel" style="height:100%;">
					<div class="treeChartWrap">
						<h5 class="panel-tit mgn-l-15 mgn-t-10"><spring:message code='epms.occurr.type' /></h5><!-- 고장 유형 -->
						<div class="treeChart">
							<canvas id="troubleType1"></canvas>
						</div>
					</div>
				</div>
				<div class="fl-box wd-per-50 rel" style="height:100%;">
					<div class="treeChartWrap mgn-l-10">
						<h5 class="panel-tit mgn-l-15 mgn-t-10"><spring:message code='epms.active.type' /></h5><!-- 조치 유형 -->
						<div class="treeChart mgn-l-10">
							<canvas id="troubleType2"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<!-- 페이지 내용 : e -->
<%-- 정비실적조회 --%>
<div class="modal fade modalFocus" id="popRepairResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 정비실적등록 --%>
<div class="modal fade modalFocus" id="popRepairResultRegForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 정비실적 --%>
<%@ include file="/WEB-INF/jsp/epms/report/repairPeriodLine/popReportPeriodLineRepairResultList.jsp"%>

<%-- 정비원 검색 --%>
<div class="modal fade modalFocus" id="popWorkerList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-25">		
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">
                    <spring:message code='epms.worker' /> <spring:message code='epms.search' />
                </h4><!-- 정비원 검색 -->
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	           		<div class="modal-section">
						<div class="fl-box panel-wrap-modal wd-per-100">
							<div class="panel-body" id="snsList">
								<bdo Debug="Error"
									 Data_Url=""
									 Layout_Url="/epms/repair/result/popWorkerListLayout.do"
								 >
								</bdo>
							</div>
						</div>
					</div> <%-- modal-section : e --%>
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>

</body>

<script type="text/javascript">
<%-- 차트 관련 스크립트 --%>
var troubleType1 = document.getElementById("troubleType1");		<%-- [정비 조치유형 차트 데이터] 전역변수 --%>
var troubleType2 = document.getElementById("troubleType2"); 	<%-- [정비 대처유형 차트 데이터] 전역변수 --%>

<%-- pie형태 차트 옵션 --%>
var pieOption = {
	maintainAspectRatio: false,
	responsive: true,
	tooltips: {
		enabled: true,
		intersect: false,
		titleFontSize: 11,
		bodyFontSize: 11,
		callbacks: {
			label: function(tooltipItem, data) {
				var allData = data.datasets[tooltipItem.datasetIndex].data;
				var tooltipLabel = data.labels[tooltipItem.index];
				var tooltipData = allData[tooltipItem.index];
				var total = 0;
				for (var i=0; i<allData.length; i++) {
					total += allData[i];
				}
				var tooltipPercentage = Math.round((tooltipData / total) * 100);
				
				return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
			}
		}
	},
	hover :{
		animationDuration:0
	},
	legend:{
		display:true,
		position : 'right',
		labels:{
			boxWidth:12,
			fontFamily: "'Open Sans Bold', sans-serif",
			fontSize:11
		}
	},
    'onClick' : function (event) {
    	
    	if(this.canvas.id == 'troubleType1'){
    		fnOpenLayerWithGrid("Y", "troubleType1Chart", "");
    		
    	}else if(this.canvas.id == 'troubleType2'){
			fnOpenLayerWithGrid("Y", "troubleType2Chart", "");
    	}
    }
}


<%-- 조치유형 그래프 작성 --%>
var troubleType1Chart = new Chart(troubleType1, {
		type: 'pie'
		, data: {
			labels : [],
			datasets : [
			    {
			    	label: ['실적 건수'],			
			    	data: [],
			    	backgroundColor: [],
			    	hoverBackgroundColor: []
			    }
			]
		},
		options: pieOption
	});

<%-- 조치유형 그래프 작성 --%>
var troubleType2Chart = new Chart(troubleType2, {
		type: 'pie'
		, data: {
			labels : [],
			datasets : [
			    {
			    	label: ['실적 건수'],			
			    	data: [],
			    	backgroundColor: [],
			    	hoverBackgroundColor: []
			    }
			]
		},
		options: pieOption
	});

<%-- 고장유형 / 조치유형 그래프 데이터 --%>
function fnGetRepairResultInfo(){
	
	var location  = $("#LOCATION").val();
	var part      = $("#PART").val();
	var startDt = $("#search_startDt").val();
	var endDt   = $("#search_endDt").val();
	var equipment = $("#EQUIPMENT").val();
	var tree 	  = $("#TREE").val();
	
	$.ajax({
		type : 'POST',
		url : '/epms/report/repairPeriodEquipment/selectRepairResultInfo.do',
		cache : false,
		dataType : 'json',
		data : {   startDt   : startDt
				 , endDt     : endDt
				 , LOCATION  : location
				 , PART 	 : part 
				 , EQUIPMENT : equipment
				 , TREE      : tree },
		async : false,
		error : function() {
			alert("처리중 오류가 발생 하였습니다.");
		},
		success : function(json) {
			
			<%-- 차트 생성 --%>
			drawChart(json);
		}		
	});
}

<%-- 랜덤 색상 생성 --%>
var dynamicColors = function(){
	var r = Math.floor(Math.random()* (255-200+1))+200;
	var g = Math.floor(Math.random()* (255-200+1))+200;
	var b = Math.floor(Math.random()* (255-200+1))+200;

	return ["rgba("+r+","+g+","+b+", 1)", "rgba("+r+","+g+","+b+", 0.5)"];
}

<%-- 차트별 데이터 비교 전역변수 --%>
var troubleType1ID = [];
var troubleType2ID = [];

<%-- 차트 데이터 생성 --%>
function drawChart(json) {
	
	<%-- 배열 초기화 --%>
	troubleType1ID = [];
	troubleType2ID = [];
	
	<%-- 고장유헝 --%>
	var troubleType1Data = [], troubleType1Label = [], troubleType1Color = [], troubleType1Hover = [];
	
	$.each(json.TROUBLE_TYPE1, function(idx, val){
		troubleType1Label.push(val.TROUBLE_TYPE1_NAME);
		troubleType1Data.push(val.TROUBLE_TYPE1_COUNT);
		
		var tempColor = dynamicColors();
		troubleType1Color.push(tempColor[0]);
		troubleType1Hover.push(tempColor[1]);
		
		troubleType1ID.push(val.TROUBLE_TYPE1);
	});
	
	troubleType1Chart.data.labels = troubleType1Label;
	troubleType1Chart.data.datasets[0].data = troubleType1Data;
	troubleType1Chart.data.datasets[0].backgroundColor = troubleType1Color;
	troubleType1Chart.data.datasets[0].hoverBackgroundColor = troubleType1Hover;
	troubleType1Chart.update();
	
	<%-- 조치유헝 --%>
	var troubleType2Data = [], troubleType2Label = [], troubleType2Color = [], troubleType2Hover = [];
	
	$.each(json.TROUBLE_TYPE2, function(idx, val){
		troubleType2Label.push(val.TROUBLE_TYPE2_NAME);
		troubleType2Data.push(val.TROUBLE_TYPE2_COUNT);
		
		var tempColor = dynamicColors();
		troubleType2Color.push(tempColor[0]);
		troubleType2Hover.push(tempColor[1]);
		
		troubleType2ID.push(val.TROUBLE_TYPE2);
	});
	
	troubleType2Chart.data.labels = troubleType2Label;
	troubleType2Chart.data.datasets[0].data = troubleType2Data;
	troubleType2Chart.data.datasets[0].backgroundColor = troubleType2Color;
	troubleType2Chart.data.datasets[0].hoverBackgroundColor = troubleType2Hover;
	troubleType2Chart.update();
}

<%-- 대시보드/차트 클릭 시 상세 실적 리스트 --%>
function fnOpenLayerWithGrid(chartYn, chart, value) {
	
	var startDt  = $('#search_startDt').val();
	var endDt    = $('#search_endDt').val();
	var PART  = $("#PART").val();
	var tree  = $("#TREE").val();
	var equipment = $("#EQUIPMENT").val();
	var STATUS = "";
	var flag  = "";		<%-- Y일경우 차트데이터, N일경우 카운트 데이터 --%>
	var title = "";
	

	
	if(chartYn == "Y"){
		title = $("#" + chart).prev().text();
		if(chart == "troubleType1Chart"){
			var activePoints = troubleType1Chart.getElementsAtEvent(event);
		    var firstPoint = activePoints[0];
		    STATUS = troubleType1ID[firstPoint._index];
		    
		}else if(chart == "troubleType2Chart"){
			var activePoints = troubleType2Chart.getElementsAtEvent(event);
		    var firstPoint = activePoints[0];
		    STATUS = troubleType2ID[firstPoint._index];
		}
		
	} else {
		title = $("#" + chart).find('.statusTit').text();		
		STATUS = value;
	}
	
	$('#popReportPeriodLineRepairResultList_title').text("정비현황(" + title + ")");
	Grids.popReportPeriodLineRepairResultList.Source.Data.Url = "/epms/report/repairPeriodLine/popRepairResultListOptData.do?&STATUS="+STATUS
												  + "&startDt="+startDt
												  + "&endDt="+endDt
												  + "&PART="+PART
												  + "&LOCATION="+$("#LOCATION").val()
												  + "&flag="+chartYn
												  + "&CHART="+chart
	  											  + "&TREE="+tree
	  											  + "&EQUIPMENT="+equipment;
	Grids.popReportPeriodLineRepairResultList.Reload();
	fnModalToggle('popReportPeriodLineRepairResultList');
   
}

</script>

</html>