<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%--
	Class Name	: reportRepairMonthlyLineListData.jsp
	Description : 레포트 > 라인별 설비 고장분석(월별) 데이터
    author		: 김영환
    since		: 2018.03.20
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.03.20	 김영환		최초 생성

--%>
<Grid>
<Body>
	<B>
	
	<c:forEach var="item" items="${list}">
		<c:set var="nodeCnt" value="0" />
		<I
			LOCATION				= "${item.LOCATION}"									<%-- 위치ID --%>
			CATEGORY_NAME			= "${fn:replace(item.CATEGORY_NAME, '"', '&quot;')}"	<%-- 라인명 --%>
			EQUIPMENT				= "${item.EQUIPMENT}"									<%-- 설비ID --%>
			EQUIPMENT_NAME			= "${fn:replace(item.EQUIPMENT_NAME, '"', '&quot;')}"	<%-- 설비명 --%>
			EQUIPMENT_UID			= "${item.EQUIPMENT_UID}"								<%-- 설비코드 --%>
			PARENT_EQUIPMENT		= "${item.PARENT_EQUIPMENT}"							<%-- 부모설비ID --%>
			TREE					= "${item.TREE}"										<%-- 구조체 --%>
			DEPTH					= "${item.DEPTH}"										<%-- 레벨 --%>
			SEQ_DSP					= "${item.SEQ_DSP}"										<%-- 출력순서 --%>
			TOTAL				    = "${item.TOTAL}"
			<%-- 이달 총 정비건 Link Class --%>	
			<c:if test="${item.TOTAL > 0}">
				TOTALClass				= "gridLink"
			</c:if>
			
			<c:forEach items="${partList}" var="list">
				<c:set var="key">PART${list.PART}</c:set>
				
				PART${list.PART} = "${item[key]}"
				
				<%-- PART01,02,03 정비건 Link Class --%>		
				<c:if test="${item[key] > 0}">
					PART${list.PART}Class				= "gridLink"
				</c:if>
			</c:forEach>

			<c:choose>
				<c:when test="${item.CUR_LVL eq item.NEXT_LVL}">></I></c:when>
				<c:when test="${item.CUR_LVL < item.NEXT_LVL}">CanExpand='1' Expanded='1'></c:when>
				<c:when test="${item.CUR_LVL > item.NEXT_LVL}">></c:when>
				<c:otherwise>></c:otherwise>
			</c:choose>
			<c:set var="nodeCnt" value="${item.CUR_LVL - item.NEXT_LVL}" />
			<c:if test="${nodeCnt > 0}">
				<c:forEach var="item" begin="0" end="${nodeCnt}" step="1"></I></c:forEach>
			</c:if>
	</c:forEach> 
		
	</B>
</Body>
</Grid>
