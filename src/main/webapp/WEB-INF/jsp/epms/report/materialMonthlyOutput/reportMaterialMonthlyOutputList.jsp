<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: reportMaterialMonthlyOutputList.jsp
	Description : [레포트-자재출고현황 분석] 메인화면
	author		: 김영환
	since		: 2018.06.14
 
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2018.06.14	 	김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<style>
.inp-comm.monthPic {box-sizing:border-box;height:28px;padding:5px 25px 4px 6px;border:1px solid #cdd2da;
	background:#fff url('/images/com/web/ico_cal.png') no-repeat right 5px center;background-size: 17px auto;cursor:pointer;}
</style>

<script type="text/javascript">

<%-- 전역변수 --%>
var focusedRow = null;		<%-- Focus Row : [자재유형] --%>
var focusedRow2 = null;		<%-- Focus Row : [자재출고현황 분석] --%>

$(document).ready(function() {
	
	$('.monthPic').monthPicker();
	
	$("#startDt_hidden").val("${searchDt}");
	$("#endDt_hidden").val("${searchDt}");
	
	<%-- 자재출고현황 분석 검색 --%>
	$('#srcBtn').on('click', function(){
		
		if(focusedRow == null) {
			alert("자재유형을 먼저 선택하세요");
		}else{
			getMaterialMonthlyOutputList();	
		}
	});
	
	<%-- monthPicker 선택시 --%>
	$("#monthList").click(function(){
		
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		<%-- 검색 시작일이 종료일보다 이후인지 체크 (이후라면, 종료일을 시작일로 변경) --%>
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#endDt').val($('#startDt').val());
		}
		
		<%-- 검색 종료일이 시작일보다 이전인지 체크 (이전이라면, 시작일을 종료일로 변경) --%>
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#startDt').val($('#endDt').val());
		}
	});
	
});

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "CategoryList"){	<%-- 1. [자재유형] 클릭시 --%>
		<%-- 1-1. 포커스 --%>
		if(focusedRow != null){
			grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow = row;
		
		<%-- 1-2. 설비 목록 조회 --%>
		if (row.TREE != null && row.TREE != "" && row.Added != 1) {
			
			$('#TREE').val(row.TREE);					<%-- 구조체 --%>
			$('#MaterialMonthlyOutputListTitle').html("자재출고현황 분석 : " + "[ " + row.CATEGORY_NAME + " ]");
			getMaterialMonthlyOutputList();
		}
		
	}
	else if(grid.id == "ReportMaterialMonthlyOutputList") { <%-- 2. [자재출고현황 분석] 클릭시 --%>
	
		if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
	 		return;
	 	}
		
		<%-- 2-1. 포커스 시작 --%>
		if(focusedRow2 != null){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		focusedRow2 = row;
	}
}

<%-- 클릭한 카운트에 해당하는 자재출고목록 --%>
function fnToggleOutputDetail(row, col, date){
	
	var count = Grids['ReportMaterialMonthlyOutputList'].GetValue(row, col);
	
	if(count != 0){
		var material_uid = row.MATERIAL_UID
		var tree         = $('#TREE').val();
		var startDt = "";
		var endDt = "";
		
		if(date == "TOTAL"){
			startDt      = $('#startDt_hidden').val()+"-01";
			endDt        = $('#endDt_hidden').val()+"-31";  
		} else {
			startDt      = date + "-01";
			endDt        = date + "-31";  
		}
		
		Grids.MaterialOutList.Source.Data.Url = "/epms/material/output/materialOutputListData.do?"
									          + "startDt="        + startDt
									          + "&endDt="         + endDt
									          + "&MATERIAL_UID="  + material_uid
									          + "&TREE="          + tree
		Grids.MaterialOutList.ReloadBody();

		fnModalToggle("popReportMaterialMonthlyOutputView");
	}
	
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- 선택된 라인에 해당되는 설비 조회 --%>
function getMaterialMonthlyOutputList(){
	var startDt = $("#startDt").val();
	var endDt = $("#endDt").val();
	
	$("#startDt_hidden").val(startDt);
	$("#endDt_hidden").val(endDt);
	
	var startDt1 = new Date(startDt);
	var endDt1= new Date(endDt);

	var diff = endDt1 - startDt1;
	var currMonth = 30 * 24 * 60 * 60 * 1000 ;
	
	$("#diffMonth").val(parseInt(diff/currMonth));
	
	Grids.ReportMaterialMonthlyOutputList.Source.Layout.Url = "/epms/report/materialMonthlyOutput/reportMaterialMonthlyOutputListLayout.do?startDt="+startDt
							  				  + "&diffMonth=" + $('#diffMonth').val();
	Grids.ReportMaterialMonthlyOutputList.Source.Data.Url = "/epms/report/materialMonthlyOutput/reportMaterialMonthlyOutputListData.do?startDt="+startDt
											  + "&diffMonth=" + $('#diffMonth').val()
											  + "&TREE=" + $('#TREE').val()
											  + "&flag=" + $('#view_type').val();
	Grids.ReportMaterialMonthlyOutputList.Reload();	
}

</script>
</head>
<body>

<%-- 화면 UI Area Start --%>
<div id="contents">
	<input type="hidden" id="TREE" name="TREE"/>						<%-- 구조체 --%>
	<input type="hidden" id="diffMonth" name="diffMonth"/>				<%-- 시작월 종료월 차이--%>
	<input type="hidden" id="startDt_hidden" name="startDt_hidden"/>	<%-- 시작월 고정값 --%>
	<input type="hidden" id="endDt_hidden" name="endDt_hidden"/>		<%-- 종료월 고정값 --%>
	
	<div class="fl-box panel-wrap03 leftPanelArea" style="width:20%;">
		<h5 class="panel-tit">자재유형</h5>
		<div class="panel-body">
			<div id="categoryList">
				<bdo	Debug="Error"
						Data_Url="/sys/category/categoryData.do?CATEGORY_TYPE=MATERIAL"
						Layout_Url="/sys/category/categoryNotEditLayout.do?CATEGORY_TYPE=MATERIAL"
				>
				</bdo>
			</div>
		</div>
	</div>
	
	<div class="fl-box panel-wrap04" style="width:80%">
		<h5 class="panel-tit mgn-l-10" id="MaterialMonthlyOutputListTitle">자재출고현황 분석</h5>
		<div class="inq-area-inner type06 ab">
			<ul class="wrap-inq">
				<li class="inq-clmn">
					<h4 class="tit-inq">검색일</h4>
					<div class="prd-inp-wrap">
					<span class="prd-inp">
						<span class="inner">
							<input type="text" id="startDt" class="inp-comm monthPic" readonly="readonly" title="검색시작일" value="${searchDt}">
						</span>
					</span>	
					<span class="prd-inp">
						<span class="inner">
							<input type="text" id="endDt" class="inp-comm  monthPic" readonly="readonly" title="검색종료일" value="${searchDt}">
						</span>
					</span>	
				</div>
				</li>
				<li class="inq-clmn">
					<h4 class="tit-inq">검색유형</h4>
					<div class="sel-wrap type02" style="width:150px;">
						<select title="구분" id="view_type" name="view_type">
							<option value="1" selected="selected">전체</option>
							<option value="2">출고</option>
							<option value="3">임의출고</option>
						</select>
					</div>
				</li>
			</ul>
			<a href="#none" id="srcBtn" class="btn comm st01">검색</a>
		</div>
		<div class="panel-body mgn-l-10 mgn-t-30">
			<div id="ReportMaterialMonthlyOutputList">
				<bdo	Debug="Error"
						Data_Url=""
						Layout_Url="/epms/report/materialMonthlyOutput/reportMaterialMonthlyOutputListLayout.do?startDt=${searchDt}"
				>
				</bdo>
			</div>
		</div>
	</div>
	
</div>
<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<%-- 출고조회 팝업 --%> 
<%@ include file="/WEB-INF/jsp/epms/report/materialMonthlyOutput/popReportMaterialMonthlyOutputView.jsp"%>

<%-- 정비실적조회 --%>
<div class="modal fade" id="popRepairResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 예방보전실적조회 --%>
<div class="modal fade modalFocus" id="popPreventiveResultForm" data-backdrop="static" data-keyboard="false"></div>
</body>
</html>