<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
	Class Name	: reportMaterialMonthlyOutputListLayout.jsp
	Description : 자재출고현황 분석
    author		: 김영환
    since		: 2018.06.14
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.06.14	 김영환		최초 생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="ReportMaterialMonthlyOutputList"/>
<Grid>
	<Cfg	id="${gridId}"
		IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
		NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
		SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1"			SuppressCfg="0"	
		Editing="0"				Deleting="0"		Selecting="0"		Dragging="0"			
		Filtering="1"			StandardFilter="3"	ClearSelected="2"
		Paging="2"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"
		CopyCols="0"			PasteFocused="3"	
	/>
	<Head>
		<Header	id="Header"	Align="center" 	Spanned="1"
			CATEGORY_NAME="자재유형"
			MATERIAL_UID="자재코드"
			MATERIAL_NAME="자재명"
			OUTPUT = "합계"
			<c:forEach var="item" items="${list}" varStatus="idx">			    
				OUTPUT_${fn:replace(item, '-', '')}      = "${item}"
			</c:forEach>
		/>
	</Head>
		
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	<Pager Visible="0"/>
	
	<LeftCols>
		<C Name="CATEGORY_NAME"				Type="Text"   Align="left" Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" RelWidth="150" MinWidth="150" Cursor="Hand" Spanned="1" CaseSensitive="0" WhiteChars=" " />
		<C Name="MATERIAL_UID"				Type="Text"   Align="left" Visible="1" Width="120" CanEdit="0" CanExport="1" CanHide="1" RelWidth="120" MinWidth="120" Cursor="Hand" Spanned="1" CaseSensitive="0" WhiteChars=" " />
		<C Name="MATERIAL_NAME"				Type="Text"   Align="left" Visible="1" Width="200" CanEdit="0" CanExport="1" CanHide="1" RelWidth="200" MinWidth="200" Cursor="Hand" Spanned="1" CaseSensitive="0" WhiteChars=" " />
	</LeftCols>
	
	<Cols>
		<C Name="OUTPUT"					Type="Text"   Align="right" Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" RelWidth="100" MinWidth="100" OnClick="fnToggleOutputDetail(Row, Col,'TOTAL');"/>
		<c:forEach var="item" items="${list}" varStatus="idx">
			<C Name="OUTPUT_${fn:replace(item, '-', '')}"	Type="Text"   Align="right" Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" RelWidth="100" MinWidth="100" OnClick="fnToggleOutputDetail(Row, Col,'${item}');"/>
		</c:forEach>
	</Cols>
	
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyWidth="1" Empty="        " 
				ColumnsType="Button"
				QR코드인쇄Type="Html" QR코드인쇄="&lt;a href='#none' title='QR코드인쇄' class=&quot;btn evn-st01&quot;
					onclick='fnQrPrint(&quot;/project/equipmentMaster/admin/popQrcodePrintList.do&quot;, 940, 650)'>QR코드인쇄&lt;/a>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>	
</Grid>