<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: popReportMaterialMonthlyOutputView.jsp
	Description : 레포트 > 자재출고현황분석 > 팝업
    author		: 김영환
    since		: 2018.07.06
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자	수정내용
	----------  	------	---------------------------
	2018.07.06		김영환	최초생성

--%>

<script type="text/javascript">	
$(document).ready(function(){
	
});
</script>

<%-- 출고현황 Layer : s --%>
<div class="modal fade modalFocus" id="popReportMaterialMonthlyOutputView" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-70"> <!-- 원하는 width 값 입력 -->				
		 <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">출고현황</h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">

					<div class="modal-section">
						
						<div class="panel-body wd-per-100 mgn-t-25" id="MaterialOutList">	
							<bdo	Debug="Error"
									Data_Url=""
									Layout_Url="/epms/material/output/materialOutputListLayout.do"
									Export_Data="data" Export_Type="xls"
							>
							</bdo>
						</div>	
						
					</div><%-- 내용 : e --%>
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div> <%-- 출고현황 Layer : e --%>

