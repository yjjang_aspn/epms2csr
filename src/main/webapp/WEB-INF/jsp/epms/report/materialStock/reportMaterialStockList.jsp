<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: reportMaterialStockList.jsp
	Description : 유통기한 및 재고수량 현황
	author		: 김영환
	since		: 2018.04.02
 
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.04.02	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script type="text/javascript">

$(document).ready(function(){
});

</script>
</head>
<body>
<div id="contents">
	
	<div class="fl-box panel-wrap04" style="width:100%;">
		<h5 class="panel-tit" id="repairRequestApprListTitle">유통기한 및 재고수량 현황</h5>		
		<div class="panel-body mgn-t-20">
			<div id="MaterialStockList">
				<bdo	Debug="Error"
						Data_Url="/epms/report/materialStock/reportMaterialStockListData.do"
						Layout_Url="/epms/report/materialStock/reportMaterialStockListLayout.do"
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=MaterialList3.xls&dataName=data"
				>
				</bdo>
			</div>
		</div>
		
	</div>
</div> <%-- contents 끝 --%>