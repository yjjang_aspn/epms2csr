<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%--
	Class Name	: reportMaterialStockListData.jsp
	Description : 유통기한 및 재고수량 현황 - 그리드 데이터
    author		: 김영환
    since		: 2018.04.02
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.02	 김영환		최초 생성

--%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${list}">
		<I	
			BUDAT					= "${item.BUDAT}"
			MATERIAL_UID			= "${item.MATERIAL_UID}"
			MATERIAL_NAME			= "${fn:replace(item.MATERIAL_NAME, '"', '&quot;')}"
			SPEC					= "${fn:replace(item.SPEC, '"', '&quot;')}"
			UNIT					= "${item.UNIT}"
			STOCK					= "${item.STOCK}"
			LOCATION				= "${item.LOCATION}"
			WAREHOUSE				= "${item.WAREHOUSE}"
			ADATE					= "${item.ADATE}"
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>