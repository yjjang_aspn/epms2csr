<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: reportRepairMtbfMttrEquipmentListLayout.jsp
	Description : MTBF/MTTR (설비) 데이터
    author		: 김영환
    since		: 2018.06.05
	
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.06.05	 김영환		최초 생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="ReportRepairMtbfMttrEquipmentList"/>
<Grid>
	<Cfg	id="${gridId}"
		IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
		NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
		SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1"			SuppressCfg="0"	
		Editing="0"				Deleting="0"		Selecting="0"		Dragging="0"			
		Filtering="1"			StandardFilter="3"	ClearSelected="2"
		Paging="2"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"
		CopyCols="0"			PasteFocused="3"	
	/>
	<Head>
		<Header	id="Header"	Align="center" 	Spanned="1"
			LOCATION              ="<spring:message code='epms.location' />"    <%  // 위치    %>
			LOCATIONRowSpan       = "2"
			LINE_NAME             ="<spring:message code='epms.category' />"    <%  // 라인    %>
			LINE_NAMERowSpan      = "2"
		    EQUIPMENT             ="<spring:message code='epms.system.id'   />"    <%  // 설비ID  %>
			EQUIPMENTRowSpan      = "2"
			EQUIPMENT_UID         ="<spring:message code='epms.system.uid'  />"    <%  // 설비코드   %>
			EQUIPMENT_UIDRowSpan  = "2"
			EQUIPMENT_NAME        ="<spring:message code='epms.system.name' />"    <%  // 설비명    %>
			EQUIPMENT_NAMERowSpan = "2"
			CNT_TOT               = "합계"
			CNT_TOTSpan           = "3"
			<c:forEach var="item" items="${list}" varStatus="idx">			    
				CNT_${fn:replace(item, '-', '')}      = "${item}"
				CNT_${fn:replace(item, '-', '')}Span  = "3"
			</c:forEach>
		/>
		<Header	Align="center" Spanned="1"
			CNT_TOT  = "돌발정비건"
			MTBF_TOT = "MTBF(Hr)"
			MTTR_TOT = "MTTR(Min)"
			<c:forEach var="item" items="${list}" varStatus="idx">		
				CNT_${fn:replace(item, '-', '')}      	= "돌발정비건"	
				MTBF_${fn:replace(item, '-', '')}      	= "MTBF(Hr)"	    
				MTTR_${fn:replace(item, '-', '')}      	= "MTTR(Min)"
			</c:forEach>
		/>
	</Head>
		
	<Solid>	  	    
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
			NAVType="Pager"
			LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
			ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"/>
	</Solid>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	<Pager Visible="0"/>
	
	<LeftCols>
		<C Name="LOCATION"				Type="Enum" Align="left"  Visible="1" Width="60"  CanEdit="0" CanExport="1" CanHide="1" RelWidth="80"  MinWidth="80"  Cursor="Hand" Spanned="1" CaseSensitive="0" WhiteChars=" " <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>
		<C Name="LINE_NAME"        	 	Type="Text" Align="left"  Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" RelWidth="150" MinWidth="150" Cursor="Hand" Spanned="1" CaseSensitive="0" WhiteChars=" " />
		<C Name="EQUIPMENT"             Type="Text" Align="left"  Visible="0" Width="100" CanEdit="0" CanExport="1" CanHide="1" RelWidth="100" MinWidth="100" Cursor="Hand" Spanned="1" CaseSensitive="0" WhiteChars=" " />
		<C Name="EQUIPMENT_UID"         Type="Text" Align="left"  Visible="1" Width="120" CanEdit="0" CanExport="1" CanHide="1" RelWidth="120" MinWidth="120" Cursor="Hand" Spanned="1" CaseSensitive="0" WhiteChars=" " />
		<C Name="EQUIPMENT_NAME"        Type="Text" Align="left"  Visible="1" Width="200" CanEdit="0" CanExport="1" CanHide="1" RelWidth="200" MinWidth="200" Cursor="Hand" Spanned="1" CaseSensitive="0" WhiteChars=" " />
	</LeftCols>
	
	<Cols>
		<C Name="CNT_TOT"				Type="Int" Align="center" Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" RelWidth="100" MinWidth="100" CaseSensitive="0" WhiteChars=" " />
		<C Name="MTBF_TOT"				Type="Int" Align="right"  Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" RelWidth="100" MinWidth="100" CaseSensitive="0" WhiteChars=" " />
		<C Name="MTTR_TOT"				Type="Int" Align="right"  Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" RelWidth="100" MinWidth="100" CaseSensitive="0" WhiteChars=" " />
		<c:forEach var="item" items="${list}" varStatus="idx">
			<C Name="CNT_${fn:replace(item, '-', '')}"	Type="Int" Align="center" Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" RelWidth="100" MinWidth="100" CaseSensitive="0" WhiteChars=" " />
			<C Name="MTBF_${fn:replace(item, '-', '')}"	Type="Int" Align="right"  Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" RelWidth="100" MinWidth="100" CaseSensitive="0" WhiteChars=" " />
			<C Name="MTTR_${fn:replace(item, '-', '')}"	Type="Int" Align="right"  Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" RelWidth="100" MinWidth="100" CaseSensitive="0" WhiteChars=" " />
		</c:forEach>
	</Cols>
	
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyWidth="1" Empty="        " 
				ColumnsType="Button"
				QR코드인쇄Type="Html" QR코드인쇄="&lt;a href='#none' title='QR코드인쇄' class=&quot;btn evn-st01&quot;
					onclick='fnQrPrint(&quot;/project/equipmentMaster/admin/popQrcodePrintList.do&quot;, 940, 650)'>QR코드인쇄&lt;/a>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>	
</Grid>