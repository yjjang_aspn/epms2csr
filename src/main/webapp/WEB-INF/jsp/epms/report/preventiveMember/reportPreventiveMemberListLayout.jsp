<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>

<%--
	Class Name	: reportPreventiveMemberListLayout.jsp
	Description : 레포트 > 예방보전 현황(정비원) 그리드 레이아웃
	author		: 김영환
	since		: 2018.05.28
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.05.28	 김영환		최초 생성

--%>
<c:set var="gridId" value="ReportPreventiveMemberList"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1"			SuppressCfg="0"	
			Editing="1"				Deleting="0"		Selecting="0"		Dragging="0"
			Filtering="1"			StandardFilter="3"	ClearSelected="2"
			CopyCols="3"			PasteFocused="3"	CustomScroll="1"
			MainCol="LOCATION"
	/>
	
	<Pager Visible="0"/>
	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>
	<Head>
		<Header	id="Header"	Align="center"
				Spanned					= '1'
				LOCATION				= "공장"
				LOCATIONRowSpan			= "2"
				PART					= "파트"
				PARTRowSpan				= "2"
				NAME					= "정비원"
				NAMERowSpan				= "2"
	
				SUM_PLAN				= "합   계"
				SUM_PLANSpan			= "3"
				TBM_PLAN				= "TBM"
				TBM_PLANSpan			= "3"
				CBM_PLAN				= "CBM"
				CBM_PLANSpan			= "8"	
		/>
		
		<Header	Align="center"
				Spanned='1'
				PART_NAME				= ""
				NAME					= ""
				SUM_PLAN				="계획건"
				SUM_COMPLETE			="실시건"
				SUM_PROGRESS			="실시율(%)"
				
				TBM_PLAN				="계획건"
				TBM_COMPLETE			="실시건"
				TBM_RATIO				="실시율(%)"
				
				CBM_PLAN				="계획건"
				CBM_COMPLETE			="실시건"
				CBM_PROGRESS			="실시율(%)"
				CBM_ERROR				="이상건"
				HIT_RATIO				="적중률(%)"				
				CBM_MEASURE				="조치건"
				MEASURE_RATIO			="조치율(%)"
				CBM_RATIO				="CBM률(%)"
		/>
		
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<Cols> 
		<C Name="LOCATION"					Type="Enum" Align="left"   Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" RelWidth="150" Spanned="1" <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}" subAuthYn="Y"/>/>
		<C Name="PART"						Type="Enum" Align="left"   Visible="1" Width="80"  CanEdit="0" CanExport="1" CanHide="1" RelWidth="80"  Spanned="1" <tag:enum codeGrp="PART" companyId="${ssCompanyId}"/>/>
		<C Name="NAME"						Type="Text" Align="left"   Visible="1" Width="120" CanEdit="0" CanExport="1" CanHide="1" RelWidth="120" Spanned="1" />
		<C Name="USER_ID"					Type="Text" Align="center" Visible="0" Width="70"  CanEdit="0" CanExport="1" CanHide="1" RelWidth="70" />
				
		<C Name="SUM_PLAN"					Type="Int"   Align="center" Visible="1" Width="80" CanEdit="0" CanExport="1" CanHide="1" RelWidth="80" Cursor="Hand" CaseSensitive="0" WhiteChars=" " />
		<C Name="SUM_COMPLETE"				Type="Int"   Align="center" Visible="1" Width="80" CanEdit="0" CanExport="1" CanHide="1" RelWidth="80" Cursor="Hand" CaseSensitive="0" WhiteChars=" " />
		<C Name="SUM_PROGRESS"		   		Type="Float" Align="right"  Visible="1" Width="90" CanEdit="0" CanExport="1" CanHide="1" RelWidth="90" Cursor="Hand" CaseSensitive="0" WhiteChars=" " Format="0.00%"/>
		
		<C Name="TBM_PLAN"					Type="Int"   Align="center" Visible="1" Width="80" CanEdit="0" CanExport="1" CanHide="1" RelWidth="80" Cursor="Hand" CaseSensitive="0" WhiteChars=" " />
		<C Name="TBM_COMPLETE"				Type="Int"   Align="center" Visible="1" Width="80" CanEdit="0" CanExport="1" CanHide="1" RelWidth="80" Cursor="Hand" CaseSensitive="0" WhiteChars=" " />
		<C Name="TBM_RATIO"					Type="Float" Align="right"  Visible="1" Width="90" CanEdit="0" CanExport="1" CanHide="1" RelWidth="90" Cursor="Hand" CaseSensitive="0" WhiteChars=" " Format="0.00%"/>
		
		<C Name="CBM_PLAN"					Type="Int"   Align="center" Visible="1" Width="80" CanEdit="0" CanExport="1" CanHide="1" RelWidth="80" Cursor="Hand" CaseSensitive="0" WhiteChars=" " />
		<C Name="CBM_COMPLETE"				Type="Int"   Align="center" Visible="1" Width="80" CanEdit="0" CanExport="1" CanHide="1" RelWidth="80" Cursor="Hand" CaseSensitive="0" WhiteChars=" " />
		<C Name="CBM_PROGRESS"				Type="Float" Align="right"  Visible="1" Width="90" CanEdit="0" CanExport="1" CanHide="1" RelWidth="90" Cursor="Hand" CaseSensitive="0" WhiteChars=" " Format="0.00%"/>
		<C Name="CBM_ERROR"					Type="Int"   Align="center" Visible="1" Width="80" CanEdit="0" CanExport="1" CanHide="1" RelWidth="80" Cursor="Hand" CaseSensitive="0" WhiteChars=" " />
		<C Name="HIT_RATIO"					Type="Float" Align="right"  Visible="1" Width="90" CanEdit="0" CanExport="1" CanHide="1" RelWidth="90" Cursor="Hand" CaseSensitive="0" WhiteChars=" " Format="0.00%"/>
		<C Name="CBM_MEASURE"				Type="Int"   Align="center" Visible="1" Width="80" CanEdit="0" CanExport="1" CanHide="1" RelWidth="80" Cursor="Hand" CaseSensitive="0" WhiteChars=" " />
		<C Name="MEASURE_RATIO"				Type="Float" Align="right"  Visible="1" Width="90" CanEdit="0" CanExport="1" CanHide="1" RelWidth="90" Cursor="Hand" CaseSensitive="0" WhiteChars=" " Format="0.00%"/>
		<C Name="CBM_RATIO"					Type="Float" Align="right"  Visible="1" Width="90" CanEdit="0" CanExport="1" CanHide="1" RelWidth="90" Cursor="Hand" CaseSensitive="0" WhiteChars=" " Format="0.00%"/>
	</Cols>
	
	<Foot>
		<I id="Foot" Calculated="1" Spanned="1" LOCATION="합          계" LOCATIONSpan="3" LOCATIONType="Text" LOCATIONAlign="Center"  Class="GOHeaderRow GOHeaderText"
			
			SUM_PLANFormula					= "sum()" 	
		    SUM_COMPLETEFormula				= "sum()" 
		    SUM_PROGRESSFormula				= "SUM_COMPLETE/SUM_PLAN"
		    
		    TBM_PLANFormula					= "sum()"
		    TBM_COMPLETEFormula				= "sum()"
		    TBM_RATIOFormula				= "TBM_COMPLETE/TBM_PLAN"
		    
		    CBM_RATIOFormula				= "(CBM_MEASURE/CBM_PLAN)"				  
		    CBM_PLANFormula					= "sum()"
		    CBM_COMPLETEFormula				= "sum()"
		    CBM_PROGRESSFormula				= "CBM_COMPLETE/CBM_PLAN"
		    CBM_ERRORFormula				= "sum()"
		    HIT_RATIOFormula				= "CBM_ERROR/CBM_PLAN"
		    CBM_MEASUREFormula				= "sum()"
		    MEASURE_RATIOFormula			= "CBM_MEASURE/CBM_ERROR"			
		/>
	</Foot>
	
	
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,접기,새로고침,인쇄,엑셀"
				EmptyType="Html"	EmptyRelWidth="1" Empty="        " 
				ColumnsType="Button"
				접기Type="Html" 접기="&lt;a href='#none' title='접기' id=&quot;Folding&quot; class=&quot;icon folderClose&quot;
					onclick='fnExpandAll(&quot;${gridId}&quot;)'>&lt;/a>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>