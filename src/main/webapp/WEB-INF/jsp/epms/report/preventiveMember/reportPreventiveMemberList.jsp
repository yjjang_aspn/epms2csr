<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"	uri="http://java.sun.com/jstl/fmt" %>
<%--
	Class Name	: reportPreventiveMemberList.jsp
	Description : 레포트 > 예방보전 현황 (정비원) 화면
    author		: 김영환
    since		: 2018.05.28
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.28	 김영환		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<script type="text/javascript">
<%-- 전역변수 --%>
var focusedRow = null;	<%-- Focus Row : [예방보전현황 분석(정비원)] --%>
var focusedRow2 = null;	<%-- Focus Row : [팝업 : 예방보전실적 현황] --%>

$(document).ready(function() {
	<%-- datePic 설정 --%>
	$('.datePic').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	});
	
	<%-- 선택된 월의 마지막 일 세팅 --%>
	var dt = new Date();
	var year = dt.getFullYear();
	var month = dt.getMonth()+1;
	var LastDate = new Date(year,month,0).getDate();
	var txtMonth = "";
	if(month>9){
		txtMonth = String(month);
	}else{
		txtMonth = '0' + String(month);
	}
	$('#startDt').val(String(year) + "-" + txtMonth + "-" + "01");
	$('#endDt').val(String(year) + "-" + txtMonth + "-" + String(LastDate));

	<%-- 검색 시작일이 종료일보다 이후인지 체크 (이후라면, 종료일을 시작일로 변경) --%>
	$('#startDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#endDt').val($('#startDt').val());
		}
	});
	
	<%-- 검색 종료일이 시작일보다 이전인지 체크 (이전이라면, 시작일을 종료일로 변경) --%>
	$('#endDt').on('change', function(){
		var startDt = $('#startDt').val().replaceAll("-","");
		var endDt = $('#endDt').val().replaceAll("-","");
		
		if(parseInt(startDt ,10) > parseInt(endDt ,10)){
			$('#startDt').val($('#endDt').val());
		}
	});
	
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function (e) {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>
	});
	
	<%-- '검색'버튼 클릭 --%>
	$('#srcBtn').on('click', function(){
		
		var startDt = $("#startDt").val();
		var endDt = $("#endDt").val();
		$("#search_startDt").val(startDt);
		$("#search_endDt").val(endDt);
		
		Grids.ReportPreventiveMemberList.Source.Data.Url = "/epms/report/preventiveMember/reportPreventiveMemberListData.do?startDt=" + startDt
											   + "&endDt=" + endDt;
		Grids.ReportPreventiveMemberList.ReloadBody(function(){
			$("div.GOPageOne").eq(0).find("tbody:first").children().each(function(){
				$(this).children().each(function(){
					if($(this).text() == "NaN") {
						$(this).removeClass("GOFloat").addClass("GOText").text("-");
					}
				})
			})
		});
	});

});

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){
	
	var startDt = $("#search_startDt").val();
	var endDt = $("#search_endDt").val();
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData" ||/*  row.id == "Foot" || */ row.id == "NoData") {
		return;
	}
	if(grid.id == "ReportPreventiveMemberList") {	<%-- 1. [예방보전현황 분석(정비원)] 클릭시 --%>
		
		<%-- 1. 포커스  --%>
		if(focusedRow != null){
			grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow = row;
		
		<%-- 1-2. 예방보전실적 현황 조회 --%>
		switch(col) {
			case "SUM_PLAN" : 
			case "SUM_COMPLETE" :
			case "TBM_PLAN" :
			case "TBM_COMPLETE" :
			case "CBM_PLAN" :
			case "CBM_COMPLETE" :
			case "CBM_ERROR" :
			case "CBM_MEASURE" :
				if(grid.GetValue(row, col) !== 0) { 
					var userId;
					
					if(row.id == "Foot") { <%-- 합계 클릭 --%>
						Grids.PopPreventiveResultList.Source.Data.Url = "/epms/preventive/result/popPreventiveResultListData.do?userId=" + userId
															+ "&GRID_FLAG=" + col
															+ "&startDt=" + startDt
															+ "&endDt=" + endDt;
						Grids.PopPreventiveResultList.Reload();	
						fnModalToggle('PopPreventiveResultList');
					}
					else { <%-- 합계 외 클릭 --%>
						if(row.NAME.indexOf("소계") !== -1 && row.NAME.indexOf("공장") === -1){ <%-- 공장 소계가 아닌  파트 소계 클릭시 --%>
							userId = "part_sum"
						}
						else { <%-- 미배정 클릭시 --%>
							userId = row.USER_ID.indexOf('NULL_') !== -1 ?  "" : row.USER_ID;
						}
						
						var part = row.PART != undefined ? row.PART : "";
						Grids.PopPreventiveResultList.Source.Data.Url = "/epms/preventive/result/popPreventiveResultListData.do?userId=" + userId
															+ "&LOCATION=" + row.LOCATION
															+ "&requestReport=" + 'member'
															+ "&PART=" + part
															+ "&GRID_FLAG=" + col
															+ "&startDt=" + startDt
															+ "&endDt=" + endDt;
						Grids.PopPreventiveResultList.Reload();	
						
						fnModalToggle('PopPreventiveResultList');
						
					} 
						
				}
				break;
		}
	}
	else if(grid.id == "PopPreventiveResultList") { <%-- 2. [팝업 : 예방보전실적 현황] 클릭시 --%>
	
		<%-- 2-1.포커스 시작 --%>
		if(focusedRow2 != null){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		focusedRow2 = row;
		
	}
}

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source,data, func){
	if(grid.id == "ReportPreventiveMemberList"){
		if(data != ""){
			callLoadingBar();
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "ReportPreventiveMemberList"){
					setTimeout($.unblockUI, 100);
				}
			}
		}
	}
}

</script>
</head>

<body>
<!-- 화면 UI Area Start  -->
<div id="contents">
<input type="hidden" id="search_startDt" name="search_startDt"/>
<input type="hidden" id="search_endDt"   name="search_endDt"/>
	
	<!-- 페이지 레이아웃 시작 -->
	<div class="fl-box panel-wrap04" style="width:100%"><!-- 원하는 비율로 직접 지정하여 사용 -->
		<h5 class="panel-tit">예방보전현황 분석 (정비원)</h5>
		<div class="inq-area-inner ab">
			<ul class="wrap-inq">
				<li class="inq-clmn">
					<h4 class="tit-inq">점검계획일</h4>
					<div class="prd-inp-wrap">
						<span class="prd-inp">
							<span class="inner">
								<input type="text" id="startDt" class="inp-comm datePic inpCal" readonly="readonly" title="검색시작일">
							</span>
						</span>	
						<span class="prd-inp">
							<span class="inner">
								<input type="text" id="endDt" class="inp-comm datePic inpCal" readonly="readonly" title="검색종료일">
							</span>
						</span>	
					</div>
				</li>
			</ul>
			<a href="#none" id="srcBtn" class="btn comm st01" style="margin-left:-5px;">검색</a>
		</div>
		<!-- e:inq-area-inner -->
		<div class="panel-body mgn-t-30">
			<!-- 트리그리드 : s -->
<!-- 	 					Data_Url="/epms/report/preventiveMember/reportPreventiveMemberListData.do" -->
			<div id="reportPreventiveMemberList">
				<bdo	Debug="Error"
						Layout_Url="/epms/report/preventiveMember/reportPreventiveMemberListLayout.do"		
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=EquipmentBomList.xls&dataName=data"
				>
				</bdo>
			</div>
			<!-- 트리그리드 : e -->
		</div>
	</div>
</div>


<%-- 예방보전실적등록/조회  --%>
<%@ include file="/WEB-INF/jsp/epms/preventive/result/popPreventiveResultList.jsp"%>
</body>
</html>

