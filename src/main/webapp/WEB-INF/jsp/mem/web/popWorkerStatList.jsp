<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"         %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<style>
	th, td {text-align:center !important;}
	.pop-body > div {padding:0 !important;}
	.adcSwitchBtn2{position:relative;width:54px;-webkit-user-select:none;-moz-user-select:none;-ms-user-select: none;float: left;margin-top:0 !important;
							position:absolute; top:2px; left:50%; margin-left:-27px;}
	.adcSwitchBtn-checkbox2{display:none;}
	.adcSwitchBtn-label2{display:block; overflow:hidden; cursor:pointer; border-radius:20px;}
	.adcSwitchBtn-inner2{display:block; width:200%; margin-left:-100%; transition: margin 0.3s ease-in 0s;}
	.adcSwitchBtn-inner2:before, .adcSwitchBtn-inner2:after{display:block; float:left; width:50%; padding:0; height:23px; line-height:23px; font-size:12px; color:#ffffff; box-sizing:border-box; -moz-box-sizing:border-box;}
	.adcSwitchBtn-inner2:before{content:"ON"; padding-left:10px; background-color:#ee7d45; color:#ffffff; text-align:left;}
	.adcSwitchBtn-inner2:after{content:"OFF"; padding-right:10px; background-color:#999999; color:#ffffff; text-align:right;}
	.adcSwitchBtn-switch2{display:block; width:11px; margin:6px; background:#ffffff; position:absolute; top:0; bottom:0; right:30px; border-radius:20px; transition: all 0.3s ease-in 0s;}
	.adcSwitchBtn-checkbox2:checked + .adcSwitchBtn-label2 .adcSwitchBtn-inner2{margin-left:0;}
	.adcSwitchBtn-checkbox2:checked + .adcSwitchBtn-label2 .adcSwitchBtn-switch2{right:0px;}
</style>

<!-- Layer PopUp Start -->
<div class="modal-dialog root wd-per-60 hgt-per-80" style="min-width:900px; min-height:500px"> <!-- 원하는 width 값 입력 -->				
	 <div class="modal-content">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title">
		   	    <spring:message code='epms.worker' /> 현황
		   	</h4><!-- 정비원 현황 -->
		</div>
	 	<div class="modal-body" >
           	<div class="modal-bodyIn">
 				<input type="hidden" class="inp-comm" id="startDt2" name="startDt" value="${startDt}"/>
 				<input type="hidden" class="inp-comm" id="endDt2" name="endDt" value="${endDt}"/>
				 <div class="tb-wrap">
					<table class="tb-st" summary="출퇴근 상태 테이블" id="workerStatTable">
						<caption>출퇴근 상태 테이블</caption>
							<colgroup>																	
								<col width="14%"/>
								<col width="12%"/>
								<col width="12%"/>
								<col width="12%"/>
								<col width="12%"/>
								<col width="12%"/>
								<col width="13%"/>
								<col width="13%"/>
							</colgroup>
						<thead>
							<tr>																	
								<th>위치</th>
								<th>파트</th>
								<th>이름</th>
								<th>부서</th>
								<th>직책</th>
								<th>출/퇴근상태</th>
								<th>이력조회</th>
								<th>이력삭제</th>
							</tr>
						</thead>
						<tbody id="popWorkStatList">
							<tr role="template" style="display:none;">
								<td name="LOCATION_NAME"></td>
								<td name="PART_NAME"></td>
								<td name="NAME"></td>
								<td name="DIVISION_NAME"></td>
								<td name="JOB_GRADE_NAME"></td>
								<td role="general"></td>
								<td role="manager">
									<div class="rel" style="width:100%; height:100%;">
										<div class="adcSwitchBtn2" name="adcSwitchBtn2" id="adcSwitchBtn2">
											<input type="checkbox" name="adcSwitchBtn2" class="adcSwitchBtn-checkbox2" id="adcSwitchBtn2" checked>
											<label class="adcSwitchBtn-label2" for="adcSwitchBtn2">
												<span class="adcSwitchBtn-inner2"></span>
												<span class="adcSwitchBtn-switch2"></span>
												<input type="hidden" name="CHECK_INOUT">
											</label>
										</div>
									</div>
								</td>
								<td role="history">
 										<a href="#none" name="popCheckInOutListBtn" class="btn comm st01">조회</a>
								</td>
								<td role="history_del">
 										<a href="#none" name="popCheckInOutListDelBtn" class="btn comm st01">삭제</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
           	
			</div>
		</div>	
	</div>		
</div>

<!-- 페이지 내용 : e -->
<script type="text/javascript">
	$(document).ready(function() {
		<%-- 닫기 버튼 이벤트  --%>
		$('.closeBtn').on('click', function(){
			window.close();
		});
		
		<%-- 출/퇴근 버튼 --%>
		$("#popWorkStatList").on("click", "div[id=adcSwitchBtn2]", function(){
			var checkInOut = $(this).find("input[name=CHECK_INOUT]").val() == "01" ? "02" : "01";
			var user_id = $(this).closest("tr").data("user_id");
			workerStatCheckInOutAjax(checkInOut, user_id,  $(this));
		});
		
		<%-- 출퇴근 이력조회 버튼 --%>
		$("#popWorkStatList").on("click", "[name=popCheckInOutListBtn]", function(){
			var user_id = $(this).closest("tr").data("user_id");
			fnPopCheckInOutList(user_id);
		});
		
		<%-- 출퇴근 이력삭제 버튼 --%>
		$("#popWorkStatList").on("click", "[name=popCheckInOutListDelBtn]", function(){
			var user_id = $(this).closest("tr").data("user_id");
			checkInOutListDelAjax(user_id,  $(this));
		});
		
		workStatListAjax();
				
	});
	
	<%-- 정비원현황 출퇴근 '이력조회'버튼 클릭 --%>
	function fnPopCheckInOutList(user_id){
		var startDt = $('#startDt2').val();
		var endDt = $('#endDt2').val();
		fnOpenLayerWithParam('/mem/member/popCheckInOutList.do?USER_ID='+user_id+"&startDt="+startDt+"&endDt="+endDt, 'popCheckInOutList');
	}
	
	<%-- 출퇴근 상태값 변경 --%>
	function workerStatCheckInOutAjax(check_inout, user_id, _this){
		jQuery.ajax({
			type : 'POST',
			url : '/mem/member/checkInOutAjax.do',
			cache : false,
			dataType : 'json',
			data : {CHECK_INOUT : check_inout, USER_ID : user_id},
			async : false,
			error : function() {
				alert("처리중 오류가 발생 하였습니다.");
			},
			success : function(json) {
				if(json.RES_CD == "0000"){ <%-- T: 정상처리 --%>
					<%--  출퇴근 변경 후 상태값 변경   --%>
					_this.find("input[name=CHECK_INOUT]").val(check_inout);
					_this.find("input.adcSwitchBtn-checkbox2").prop("checked", check_inout == "02" ? false : true);
					
					if(user_id == '${sessionScope.ssUserId}') {
						$('#check_inout').val(check_inout);
						$("#adcSwitchBtn2").prop("checked", check_inout == "02" ? false : true);
						$("#adcSwitchBtn").prop("checked", check_inout == "02" ? false : true);
					}
				}else{
					alert(json.resultMsg);
				}
			}
		});
	}
	
	<%-- 출퇴근 이력 삭제 --%>
	function checkInOutListDelAjax(user_id, _this){
		var name = _this.closest("tr").find("[name=NAME]").text().trim();
		if(confirm("'"+name+"'님의 출퇴근 이력을 삭제하시겠습니까?")){
			jQuery.ajax({
				type : 'POST',
				url : '/mem/member/checkInOutListDelAjax.do',
				cache : false,
				dataType : 'json',
				data : {USER_ID : user_id},
				async : false,
				error : function() {
					alert("처리중 오류가 발생 하였습니다.");
				},
				success : function(json) {
					alert("출퇴근 이력이 삭제되었습니다.")
				}
			});
		}
	}
	
	function workStatListAjax(){
		<%-- 출퇴근 상태 팝업 데이터  --%>
		var location = '${sessionScope.SsLocation}';
		var view_auth = '${sessionScope.SsSubAuth}';
		var html = "";
		$.ajax({
			type : 'POST',
			url : '/mem/member/popWorkerStatListAjax.do',
			cache : false,
			dataType : 'json',
			data : {LOCATION : location, VIEW_AUTH : view_auth},
			async : false,
			error : function() {
				alert("처리중 오류가 발생 하였습니다.");
			},
			success : function(json) {
				var authArry = json.auth.split(",");
				var auth = ["AUTH02", "AUTH03"];
				
				var authCheck = false;
				
				authArry.some(function (item, index, array) {
					if(auth.indexOf(item) != -1) 
						authCheck = true;
						
					return authCheck;
				});
				
				if(json.list != "" && json.list != null) {
					
					_table = $("#workerStatTable");
				    _table.find("tbody").children().not("tr[role=template]").remove();
				    var _tr, _trType1 = _table.find("tbody tr[role=template]");
				    
				    $.each(json.list, function (i, o) {
			            _tr = _trType1.map(function () {
			                return "<tr role=\"row\">" + $(this).html() + "</tr>";
			            }).get().join("");
			            _tr = $(_tr);
			            
			            for (var name in o) {
			            	
			            	switch (name) {
				 				case "CHECK_INOUT":
				 					//퇴근 : 02
				 					if(o[name] == "02") {
				 						_tr.find("input:checkbox").removeAttr("checked");
					 					_tr.find("td[role=general]").text("퇴근");
				 					} 
				 					//출근 : 01
				 					else if(o[name] == "01") {
					 					_tr.find("td[role=general]").text("출근").css("color", "blue");
				 					}
			 						_tr.find("input[name=CHECK_INOUT]").val(o[name]);
			 						
			 						//권한 체크해서 출퇴근 변경 토글 보일지 안보일지 제어
			 						if(authCheck) {
			 							_tr.find("td[role=general]").hide();
			 						} else {
			 							_tr.find("td[role=manager]").hide();
			 						}
			 						
				 					break;
				 				case "USER_ID":
				 					_tr.attr("data-user_id", o[name]);
				 					break;
				 				default:
				 					_tr.find("td[name=" + name + "]").text(o[name]);
				 					break;
			 				}
			            }
			            _tr.appendTo(_table.find("tbody"));
			            _tr = null;
				    });
				}
			}		
		}); 
	}
</script>
