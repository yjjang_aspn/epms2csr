<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tag" uri="/WEB-INF/tlds/code.tld" %> 
<%--
	Class Name	: popProfile.jsp
	Description : 프로필 관리
	author		: 김영환
	since		: 2018.04.04
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.04.04	 김영환		최초 생성

--%>
<style type="text/css">
.thumbnail{height      : 100px;
	       line-height : 70px;
		   margin-top  : 10px;
	       text-align  : center;
	       width       : 120px;
} 

#fileList0 { width:0; height:0; }
</style>
<script>
var maxFileCnt = 1;
var ImgChg = '';

$(document).ready(function() {
	
	$("#popProfile").html();
	$('.pwChgArea').hide(); /* 비밀번호 변경 관련 테이블 */
	$('#infoSaveBtn').hide(); /* 저장 버튼 */
	
	/* 첨부파일 */
	for(var i=0; i<1; i++) {
		$('#attachFile'+i).append('<input type="file" multiple id="ex_file'+i+'" name="fileList" class="ksUpload with-preview">'); //class에 multi with-preview를 설정 해줘야 업로드전 이미지 미리보기 기능 활설화
		
		$('#ex_file'+i).MultiFile({
			accept : 'gif |jpg |png |jpeg'
           ,list   : '#detailFileList'+i
           ,max    : maxFileCnt
           ,STRING : {
				denied    : "$ext 는(은) 업로드 할수 없는 파일확장자입니다."
				,toomany   : "업로드할 수 있는 파일의 개수는 최대 $max개 입니다."
				,duplicate : "$file 은 이미 선택된 파일입니다."
				,remove    : '<img src="/images/com/web/btn_remove.png">'
                       		+ '<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'"/>'
	            ,text      : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" value="'+i+'"/>'
              },
		});
		
		$('.MultiFile-wrap').attr("id", "jquery"+i);
	}
	
	//첨부파일 삭제
	$('.icn_fileDel').on('click', function() {
		delModiFile($(this));
	});

	//이미지 파일 다운로드
	$('.btnFileDwn').on('click', function() {
		var ATTACH        = $(this).data("attach");
		var ATTACH_GRP_NO = $(this).data("attach_grp_no");
		location.href = '/attach/fileDownload.do?ATTACH_GRP_NO=' + ATTACH_GRP_NO + '&ATTACH=' + ATTACH;
	});
	
	/* 비밀번호 변경 버튼 */
	$('#passwordChgBtn').on('click', function(){
		var pwCtl = $('#passwordChgBtn').text();
		if(pwCtl == "비밀번호 변경"){
			$('#passwordChgBtn').text("비밀번호 변경 취소"); /* 버튼 변경 */
			$('.pwChgArea').show(); /* 비밀번호 변경 관련 테이블 */
			$("#infoSaveBtn").show(); /* 저장 버튼 */
			$("#infoCloseBtn").hide(); /* 닫기 버튼 */
		}else{
			$('#passwordChgBtn').text("비밀번호 변경");  /* 버튼 변경 */
			$('.pwChgArea').hide(); /* 비밀번호 변경 관련 테이블 */
			$('#newPw').val(""); /* 비밀번호 변경 관련 테이블 */
			$('#checkPw').val(""); /* 비밀번호 변경 관련 테이블 */
			
			// 이미지 변경을 했다면 저장버튼 계속 활성화
			if(ImgChg == 'Changed'){
				$("#infoSaveBtn").show(); /* 저장 버튼 */
				$("#infoCloseBtn").hide(); /* 닫기 버튼 */
			} else {
				$("#infoSaveBtn").hide(); /* 저장 버튼 */
				$('#infoCloseBtn').show(); /* 닫기 버튼 */
			}
			
		}
	});
	
	/* 취소 버튼 */
	$('#cancelBtn').on('click', function(){
		fnModalToggle('popProfile');
	});
	
	/* 닫기 버튼 */
	$('#closeBtn').on('click', function(){
		fnModalToggle('popProfile');
	});
	
	/* 저장 버튼 */
	$('#saveBtn').on('click', function(){
		var returnValue = "T"; /* Return 값 */
		
		if($('#passwordChgBtn').text() == "비밀번호 변경 취소"){
			var newPw = $('#newPw').val(); /* 신규 비밀번호 */
			var checkPw = $('#checkPw').val(); /* 비밀번호 확인 */
			
			if(newPw == "" || checkPw == "") {
				alert("비밀번호를 입력해주세요.");
				returnValue = "F";
				return;
			}
			
			if(newPw != checkPw){
				alert("비밀번호를 다시 확인해주세요.");
				returnValue = "F";
				return;
			}
			
			$('#PASSWORD').val(SHA256(hex_md5($('#newPw').val())));
		}
		
		if(returnValue == "T"){
			if(confirm("수정된 계정 정보를 저장하시겠습니까?")){
				
				var form = new FormData(document.getElementById('userInfoForm'));
				/* 계정 정보 수정 */
				$.ajax({
					type     : 'POST',
					url      : '/mem/member/updateProfileAjax.do',
					data     : form,
					dataType : 'json',
					processData : false,
					contentType : false,
					success  : function (data) {
						
						if(data.resultCd == 'T'){
							alert("계정 정보가 수정되었습니다.");
							fnModalToggle('popProfile');
						}
						
					},error: function(XMLHttpRequest, textStatus, errorThrown){
						alert('계정 정보 수정에 실패하였습니다. 관리자에게 문의해주세요.\n error : ' + textStatus );
						fnModalToggle('popProfile');
					}
				});
			}
		}
	});
	
	<%-- 프로필 이미지 변경 시 임시 썸네일  --%>
	$('#imgChgBtn').click(function(e){
		e.preventDefault();             
		$("input:file").click();               
		var ext = $("input:file").val().split(".").pop().toLowerCase();
		if(ext.length > 0){
			if($.inArray(ext, ["gif","png","jpg","jpeg"]) == -1) { 
				alert("이미지만 업로드 가능합니다.");
				return false;  
			}                  
		}
		$("input:file").val().toLowerCase();
	});
	
	$("#fileList0").on('change', function(){
		// 기존 이미지 삭제
		
		var sessionAttachNo = "${memberInfo.ATTACH_GRP_NO}";
		
		if(sessionAttachNo != "") $("#DEL_SEQ").val("1");
		else $("#attachExistYn").val("NY");
		
		ImgChg = "Changed";
		
		if (this.files && this.files[0]) {
			var reader = new FileReader();
			
			reader.onload = function (e) {
				$('#imgSrc').attr('src', e.target.result);
			}
			
			reader.readAsDataURL(this.files[0]);
			}
		
		$("#infoCloseBtn").hide(); /* 닫기 버튼 */
		$("#infoSaveBtn").show(); /* 저장 버튼 */
	});
	
	/* 이미지 버튼 */
// 	$('#imgChgBtn').on('click', function(){
// 		var attachGrpNo = "${memberInfo.ATTACH_GRP_NO}";
// 		var userId = "${memberInfo.USER_ID}";
// 		fn_openPop("/attach/fileUserUdateForm.do", "?MODULE=9&maxFileCnt=1&ATTACH=1&ATTACH_GRP_NO="+attachGrpNo+"&userId="+userId, 300, 300);
// 	});
	
});

/* 증빙 항목에서 제거 처리 */
function delModiFile(obj){
	var attach = obj.data("attach");
	var idx = obj.data('idx');
	
	$('#fileLstWrap_'+attach).remove();
	
	//fileNo set
	if(fileVal != '') {
		fileVal += ",";
	}
	
	fileVal += attach;
	$('#DEL_IMG_SEQ').val(fileVal);
}

</script>
<div class="modal-dialog root wd-per-35 hgt-per-90">
	<div class="modal-content hgt-per-60" style="min-height:478px">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title">계정 관리</h4>
		</div>
	 	<div class="modal-body" >
           	<div class="modal-bodyIn">
				<!-- 페이지 내용 : s -->
				<!-- 사용자 정보 -->
				
					<div class="profile-wrap">
						<form id="userInfoForm" method="post" action="" enctype="multipart/form-data">
							<div class="top-area">
								<div class="user-pic">
									<!-- 프로필 이미지 -->
										<input type="hidden" id="MODULE" name="MODULE" value="9">
										<input type="hidden" id="DEL_SEQ" name="DEL_SEQ" value="">
										<input type="hidden" id="attachExistYn" name="attachExistYn" value=""/>
										<a href="#none" id="imgChgBtn" onfocus="this.blur();">
											<c:choose>
												<c:when test="${memberInfo.ATTACH_GRP_NO eq null or memberInfo.ATTACH_GRP_NO eq ''}">
													<img id="imgSrc" src="/images/com/web/bg_prsn.png" alt="NO IMAGE"/>
													<input type="hidden" id="ATTACH_GRP_NO" name="ATTACH_GRP_NO" value=""/>
												</c:when>
												<c:otherwise>
													<img id="imgSrc" class="comtUsrBtn" src="/attach/fileDownload.do?ATTACH_GRP_NO=${memberInfo.ATTACH_GRP_NO}&ATTACH=1&timeStamp=${timeStamp}" title="이미지변경" />												<input type="hidden" id="ATTACH_GRP_NO" name="ATTACH_GRP_NO" value="${memberInfo.ATTACH_GRP_NO}"/>
												</c:otherwise>
											</c:choose>
										</a>
										<input type='file' id='fileList0' name='fileList' />
								</div>
								<div class="user-i">
									<span class="name">${memberInfo.NAME}</span> <span style="font-size:13px;">${memberInfo.JOB_GRADE}</span><br />
									<span class="group">${memberInfo.DIVISION_NM}</span>
								</div>
							</div>
							<input type="hidden" id="PASSWORD"   name="PASSWORD"/>
						</form>
						
						<!-- top-area -->
						<div class="bottom-area">
							<div class="tb-wrap type03">
								<table class="tb-st">
									<caption></caption>
									<colgroup>
										<col width="35%" />
										<col width="*" />
									</colgroup>
									<tbody>
										<tr>
											<th>사원번호(ID)</th>
											<td>
												<input type="text" id="userId" name="userId" value="${memberInfo.USER_ID}" placeholder="사원번호(ID)" readonly="readonly"/>
											</td>
										</tr>
										<%-- 
										<tr>
											<th>내선번호</th>
											<td>
												<input type="text" id="TELEPHONE" name="TELEPHONE" value="${memberInfo.TELEPHONE}" placeholder="내선번호" readonly="readonly"/>
											</td>
										</tr>
										 --%>
										<tr>
											<th>휴대폰번호</th>
											<td>
												<input type="text" id="PHONE" name="PHONE" value="${memberInfo.PHONE}" placeholder="휴대폰번호" readonly="readonly"/>
											</td>
										</tr>
										<tr class="pwChg">
											<th>비밀번호 변경</th>
											<td>
												<a href="#none" class="btn comm st01" id="passwordChgBtn">비밀번호 변경</a>
											</td>
										</tr>
										<tr class="pwChgArea" style="display: none">
											<th>신규 비밀번호</th>
											<td>
												<div class="inp-wrap">
													<input type="password" id="newPw" name="newPw"/>
												</div>
											</td>
										</tr>
										<tr class="pwChgArea" style="display: none">
											<th>비밀번호 확인</th>
											<td>
												<div class="inp-wrap">
													<input type="password" id="checkPw" name="checkPw"/>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<!-- e:bottom-area -->
						<div class="btn-area"  id="infoCloseBtn">
							<a href="#none" class="btn comm st02" id="closeBtn">닫기</a>
						</div>
						<div class="btn-area"  id="infoSaveBtn">
							<a href="#none" class="btn comm st01" id="saveBtn">저장</a>
							<a href="#none" class="btn comm st02" id="cancelBtn">취소</a>
						</div>
					</div>
					<!-- e:prifile-wrap -->
				<!-- 페이지 내용 : e -->
			</div> <%-- modal-bodyIn : e --%>
		</div> <%-- modal-body : e --%>
	</div> <%-- modal-content : e --%>
</div> <%-- modal : e --%>