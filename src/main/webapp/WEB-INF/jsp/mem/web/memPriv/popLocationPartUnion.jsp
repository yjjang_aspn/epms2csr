<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: popLocationPartUnion.jsp
	Description : [사용자권한] : 공장파트관리(파트구분,파트통합) 팝업
    author		: 김영환
    since		: 2018.04.25
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  	------	---------------------------
	2018.04.25	 	김영환		최초 생성

--%>

<script type="text/javascript">
var submitBool = true;
var maxFileCnt = 30;

$(document).ready(function(){
	
	$('#popTitle').html("공장별 파트통합 관리");
	
	//저장버튼
	$('#regBtn').on('click', function(){
		if(submitBool){
			fnValidate();	
		}else{
			alert("등록중입니다. 잠시만 기다려 주세요.");
			return false;
		}
	});
	
});

function fnValidate(){
	submitBool = false;
	
	var form = new FormData(document.getElementById('LocationPartForm'));
	
	$.ajax({
		type: 'POST',
		url: '/mem/member/updateLocationPartUnion.do',
		dataType: 'json',
		data: form,
		processData: false, 
		contentType:false,
		success: function (json) {
			
			if(json > 0){
				alert("정상적으로 저장되었습니다.");
				toggleModal($('#popLocationPartUnion'));
			}

		},error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus );
			loadEnd();
		}
	});
}
</script>

<div class="modal-dialog root wd-per-30"> <!-- 원하는 width 값 입력 -->				
	 <div class="modal-content" style="height:500px;">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title">파트통합 관리</h4><!-- 공장별 파트통합 관리 -->
		</div>
	 	<div class="modal-body" >
			<div class="modal-bodyIn" style="margin-bottom:43px;">
				<form id="LocationPartForm" name="LocationPartForm"  method="post" enctype="multipart/form-data">	
					<div class="tb-wrap">
						<table class="tb-st">
							<caption class="screen-out">파트통합 관리</caption>
							<colgroup>
								<col width="50%" />
								<col width="*" />
							</colgroup>
							<tbody>
								<c:forEach var="item" items="${list}" >
								<tr>
									<th>${item.COMCD_NM}</th>
									<input type="hidden" class="inp-comm" id="" name="comcdArr" value="${item.COMCD}"/>
									<td>
										<div class="sel-wrap">
											<select title="파트유형" id="" name="remarksArr">
												<option value="01" <c:if test="${item.REMARKS eq '01'}">selected</c:if>>파트구분</option>
												<option value="02" <c:if test="${item.REMARKS eq '02'}">selected</c:if>>파트통합</option>
											</select>
										</div>
									</td>
								</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</form>
           		
			</div>
		</div>
		<div class="modal-footer hasFooter">
			<a href="#none" class="btn comm st02" id="cancelBtn" data-dismiss="modal">취소</a>
			<a href="#none" class="btn comm st01" id="regBtn">저장</a> 
		</div>
	</div>		
</div>
<!-- Layer PopUp End -->
