<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="aspn.com.common.util.Utility" %>
<%@ page import="aspn.com.common.util.ObjUtil" %>
<%--
	Class Name	: memberPrivData.jsp
	Description : [사용자권한] : 사용자권한 그리드 데이터
    author		: 서정민
    since		: 2018.05.09
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.09	 서정민		최초 생성

--%>

<Grid>
	<Body>
		<B>
			<c:forEach var="item1" items="${memberList}">
				<I  
					USER_ID    					= "${item1.USER_ID}"
					NAME    					= "${item1.NAME}"
					LOCATION					= "${item1.LOCATION}"
					PART						= "${item1.PART}"
					AUTH						= "${item1.AUTH}"
					<c:forEach var="item2" items="${memberPrivList}">
						<c:if test="${item1.USER_ID==item2.USER_ID}">
							${item2.SUB_AUTH} = "${item2.USE_YN}" 
						</c:if>
					</c:forEach>
				/>
			</c:forEach>
		</B>
	</Body>
</Grid>
