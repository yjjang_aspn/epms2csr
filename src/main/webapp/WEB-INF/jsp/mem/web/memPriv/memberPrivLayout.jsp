<%@ page language="java"  contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: memberPrivLayout.jsp
	Description : [사용자권한] : 사용자권한 그리드 레이아웃
    author		: 서정민
    since		: 2018.05.09
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.09	 서정민		최초 생성

--%>

<c:set var="gridId" value="MemberPrivList"/> 
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"  	SortIcons="0"		Calculated="1"		CalculateSelected ="1"	NoFormatEscape="1"
			NumberId="1"          	DateStrings="2"		SelectingCells="0"	AcceptEnters="1"		Style="Office"
			InEditMode="2"        	SafeCSS='1'			NoVScroll="0"		NoHScroll="0"			EnterMode="0"
			Filtering="1"         	Dragging="0"		Deleting="0"		Adding="0"				ColMoving="0"
			Selecting ="0"          Sorting = "1"       Sorted= "1"			SuppressCfg="0"	
			SuppressCfg="0"			StandardFilter="3"
			CopyCols="0"			PasteFocused="3"	CustomScroll="1"
	/>
	
	<Head>
		<Header	id="Header"	Align="center" CanExport="1" CanHide="0"
				Spanned='1'
				USER_ID      	= "ID"
				USER_IDRowSpan 	= "2"
				NAME      	 	= "이름"
				NAMERowSpan  	= "2"
				LOCATION      	= "근무지"
				LOCATIONRowSpan = "2"
				PART      	 	= "<spring:message code='epms.work.part' />"    <%  // 정비파트  %>
				PARTRowSpan  	= "2"
				AUTH      	 	= "메뉴권한"
				AUTHRowSpan  	= "2"
				<c:forEach var="item" items='${subAuthList}' varStatus="idx">				
					<c:if test="${idx.index eq 0}">
						AUTH${item.COMCD} = "직책권한" 
						AUTH${item.COMCD}Span = "${fn:length(subAuthList)}"
					</c:if>
				</c:forEach>
				<c:forEach var="item" items='${locationList}' varStatus="idx">
					<c:if test="${idx.index eq 0}">
						FAC${item.COMCD} = "조회권한" 
						FAC${item.COMCD}Span = "${fn:length(locationList)}"
					</c:if>
				</c:forEach>
				<c:forEach var="item" items='${funcAuthList}' varStatus="idx">
					<c:if test="${idx.index eq 0}">
						FUNC${item.COMCD} = "기능권한" 
						FUNC${item.COMCD}Span = "${fn:length(funcAuthList)}"
					</c:if>
				</c:forEach>
		/>
		<Header	Align="center"
				Spanned='1'
				<c:forEach var="item" items='${subAuthList}'>
					AUTH${item.COMCD} = "${item.COMCD_NM}" 
				</c:forEach>
				<c:forEach var="item" items='${locationList}'>
					FAC${item.COMCD} = "${item.COMCD_NM}" 
				</c:forEach>
				<c:forEach var="item" items='${funcAuthList}'>
					FUNC${item.COMCD} = "${item.COMCD_NM}" 
				</c:forEach>
		/>
		<Filter	id="Filter" CanExport="1" CanHide="0" CanFocus="1"/>
	</Head>
	
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
	
	<LeftCols>
		<C Name="USER_ID"   		Type="Text" Align="left"   Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" Spanned="1" RelWidth="100" MinWidth="100"/>
		<C Name="NAME"     			Type="Text" Align="left"   Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" Spanned="1" RelWidth="100" MinWidth="100"/>
	</LeftCols>

	<Cols>
		<C Name="LOCATION"			Type="Enum" Align="left"   Visible="1" Width="100" CanEdit="1" CanExport="1" CanHide="1" Spanned="1" RelWidth="100" MinWidth="100" <tag:enum codeGrp="LOCATION" companyId="${ssCompanyId}"/>/>
		<C Name="PART"				Type="Enum" Align="left"   Visible="1" Width="80"  CanEdit="1" CanExport="1" CanHide="1" Spanned="1" RelWidth="80"  MinWidth="80"  <tag:enum codeGrp="PART" companyId="${ssCompanyId}" blankYn="Y" />/>
		<C Name="AUTH"				Type="Enum"	Align="left"   Visible="1" Width="100" CanEdit="1" CanExport="1" CanHide="1" Spanned="1" RelWidth="100" MinWidth="100" <tag:enum codeGrp="USER_PRIV" companyId="${ssCompanyId}"/>/>
		<c:forEach var="item" items="${subAuthList}">
			<C Name="AUTH${item.COMCD}"  Type="Bool" RelWidth="80" MinWidth="80"/>
		</c:forEach>
		<c:forEach var="item" items="${locationList}">
			<C Name="FAC${item.COMCD}"   Type="Bool" RelWidth="80" MinWidth="80"/>
		</c:forEach>
		<c:forEach var="item" items="${funcAuthList}">
			<C Name="FUNC${item.COMCD}"  Type="Bool" RelWidth="80" MinWidth="80"/>
		</c:forEach>
	</Cols>
	<!-- 공장파트 -->
	<Toolbar	Space="0"	Styles="2"	Cells="Empty,Cnt,Chg,지사관리,저장,새로고침,인쇄,엑셀"
			EmptyType="Html"	EmptyRelWidth="1" Empty="        "
			ColumnsType = "Button"
			CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
			지사관리Type	="Html" 지사관리	="&lt;a href='#none' title='지사관리' class=&quot;treeButton treeRegister&quot;
				onclick='layer_open_withParam(&quot;/mem/member/popLocationPartUnion.do&quot;,&quot;popLocationPartUnion&quot;)'>지사관리&lt;/a>"
			저장Type="Html" 저장="&lt;a href='#none' title='저장' class=&quot;treeButton treeSave&quot;
				onclick='fnSave(&quot;${gridId}&quot;)'>저장&lt;/a>"
			항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
				onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
			새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
				onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
			인쇄Type = "Html" 인쇄 = "&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
				onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
			엑셀Type = "Html" 엑셀 = "&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
				onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"	
	/>


</Grid>