<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: memberPrivList.jsp
	Description : [사용자권한] : 화면
    author		: 서정민
    since		: 2018.05.09
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.09	 서정민		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<script type="text/javascript">
<%-- 전역변수 --%>
var focusedRow = null;		<%-- Focus Row : [부서] --%>
var focusedRow2 = null;		<%-- Focus Row : [사용자권한] --%>

$(document).ready(function(){
	
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function (e) {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>	
	});	
});


Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "DivisionList") {	<%-- 1. [부서] 클릭시 --%>
		
		<%-- 1-1. 포커스 --%>
		if(focusedRow != null){
			grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow = row;
		focusedRow2 = null;
		
		<%-- 1-2. 사용자권한 조회 --%>
		if (row.DIVISION != null && row.DIVISION != "" && row.Added != 1) {
			
			getMemberPrivList(row.TREE);
		}
	}
	else if(grid.id == "MemberPrivList") {	<%-- 2. [사용자권한] 클릭시 --%>
		<%-- 2-1. 포커스  --%>
		if(focusedRow2 != null){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow2 = row;
	}
}

<%-- 사용자권한 조회 --%>
function getMemberPrivList(TREE){
	Grids.MemberPrivList.Source.Data.Url = "/mem/member/memberPrivData.do?TREE="+TREE;
	Grids.MemberPrivList.ReloadBody();	
}

<%-- Validation --%>
function dataValidation(gridNm){
	var cnt = 0;
	var grid = Grids[gridNm];
	
	if(gridNm == "MemberPrivList"){
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow2);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){  
			if(row.Changed || row.Added){
				
				cnt++;
			}
		}
	}
	
	<%-- 추가 및 변경된 데이터가 없을 경우 --%>
	if(cnt == 0){
		return false;
	}
	else{
		return true;
	}
}

<%-- [저장] 버튼 : 사용자권한 저장 --%>
function fnSave(gridNm){
	
	<%-- 유효성 체크 --%>
    if(!dataValidation(gridNm)){
    	return;
    }
	
	if(Grids[gridNm].ActionValidate()){
		if(gridNm == "MemberPrivList"){
			if(confirm("사용자권한 정보를 저장하시겠습니까?")){
	
				Grids[gridNm].ActionSave();
	
			}
		}
	}
}

<%-- 그리드 데이터 저장 후 --%> 
Grids.OnAfterSave  = function (grid){
	if(grid.id == "MemberPrivList"){
		
		grid.ReloadBody();
		
	}
};

<%-- param 전달용 모달 호출 --%>
function layer_open_withParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- 모달 토글 --%>
function toggleModal(obj){
	obj.modal('toggle');
}
	
</script>
</head>

<body>
<div id="contents">		
		
	<div class="fl-box panel-wrap03 leftPanelArea" style="width: 20%; min-width: 350px;">
		<h5 class="panel-tit">부서</h5>
		<div class="panel-body">
			<div id="memberDivList">
				<bdo	Debug="Error"
						Data_Url="/mem/division/divisionData.do"
						Layout_Url="/mem/division/divisionNotEditLayout.do"				
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=DivisionList.xls&dataName=data"
						>
				</bdo>
			</div>
		</div>
	</div>
	
	<div class="fl-box panel-wrap03 rightPanelArea">
		<h5 class="panel-tit mgn-l-10">사용자권한</h5>
		<div class="panel-body mgn-l-10" style="padding-top:-10px;">
			<div id="MemberPrivList">
				<bdo	Debug="Error"
						Layout_Url="/mem/member/memberPrivLayout.do"
						Upload_Url="/mem/member/memberPrivEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols"  Upload_Xml="2"				
						Export_Data="data" Export_Type="xls"
						Export_Url="/sys/comm/exportGridData.jsp?File=MemberPrivList.xls&dataName=data"
						>
				</bdo>
			</div>
		</div>
	</div>

</div>

<div class="modal fade modalFocus" id="popLocationPartUnion"   data-backdrop="static" data-keyboard="false"></div> 		<%-- 공장별 파트 통합관리 팝업 --%>
</body>
</html>							
		