<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%--
	Class Name	: divisionNotEditLayout.jsp
	Description : [사용자권한] : 부서 그리드 레이아웃(수정불가)
    author		: 서정민
    since		: 2018.05.08
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.08	 서정민		최초 생성

--%>

<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="DivisionList"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="0"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1"			SuppressCfg="0"			
			Editing="0"				Deleting="0"		Selecting="0"		Dragging="1"					
			Filtering="1"			StandardFilter="3"	ClearSelected="2"
			Paging="0"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"				
			CopyCols="3"			PasteFocused="3"	
			MainCol="NAME"
	/>
	
	<Head>
		<Header	id="Header"	Align="center" CanExport="1" CanHide="0"
				DIVISION		= "<spring:message code='division.divisionId'/>"
				DIVISION_UID	= "부서코드"
				NAME			= "<spring:message code='division.divisionNm'/>"
				PARENT_DIVISION	= "<spring:message code='division.parentDivision'/>"
				TREE			= "구조체"
				DEPTH			= "레벨"
				SEQ_DSP			= "출력순서"
				COMPANY_ID		= "회사코드"
		/>
		<Filter	id="Filter" CanExport="1" CanHide="0" CanFocus="1"/>
	</Head>
	
	<Cols>
		<C Name="DIVISION"			Type="Text" Align="left"   Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="DIVISION_UID"		Type="Text" Align="left"   Visible="0" Width="100"    CanEdit="0" CanExport="1" CanHide="1"/>
	    <C Name="NAME"				Type="Text" Align="left"   Visible="1" RelWidth="100" CanEdit="1" CanExport="1" CanHide="1" Cursor="hand"/>
		<C Name="PARENT_DIVISION"	Type="Text" Align="left"   Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="TREE"				Type="Text" Align="left"   Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="DEPTH"				Type="Text" Align="center" Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="SEQ_DSP"			Type="Text" Align="center" Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="COMPANY_ID"		Type="Text" Align="center" Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="CUR_LVL"					Type="Text"	Visible="0"/>
	</Cols>
	
	<Toolbar	Space="0"	Styles="1"	Cells="Empty,Chg"
			EmptyType = "Html"  EmptyWidth = "1" Empty="        "
			ColumnsType="Button"
			CntType="Html" CntFormula='"Rows:&lt;b>"+count(7)+"&lt;/b>"' CntWidth='50' CntAlign="top"
			ChgType="Html" ChgFormula='var cnt=count("Row.Changed==1",7);return cnt?"Changed:&lt;b>"+cnt+"&lt;/b>":""' ChgWidth='-1' ChgWrap='0'
	/>	
	
	
</Grid>