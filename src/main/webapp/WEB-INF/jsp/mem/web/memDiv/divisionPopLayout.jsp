<?xml version="1.0" encoding="UTF-8"?>
<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<Grid>
	<Cfg	id="DivisionList"
			IdChars="0123456789"  SortIcons="0"       Calculated="1"       CalculateSelected ="1"   NoFormatEscape="1"
			NumberId="1"          DateStrings="2"     SelectingCells="0"   AcceptEnters="1"        	Style="Office"
			InEditMode="2"        SafeCSS='1'         NoVScroll="0"        NoHScroll="0"            EnterMode="0"
			Filtering="1"         Dragging="0"        Selecting="0"        MainCol="NAME"
			Sorting = "0"		  Sorted= "0"
			CopySelected="0"	  CopyFocused="1"     CopyCols="0"			
			Deleting="0" 		  Editing ="0"
				
	/>
	
	<Header	id="Header"	Align="center"
			DIVISION="<spring:message code='division.divisionId' />"
			NAME="<spring:message code='division.divisionNm' />"
			PARENT_DIVISION="<spring:message code='division.parentDivision' />"
			COMPANY_ID = "<spring:message code='company.companyId' />"
	/>
	
	<Cols>
		<C Name="NAME"	 					Type="Text"	RelWidth="100"	Align="left"	 Visible="1"   Cursor="hand"/>
		<C Name="DIVISION"					Type="Text"	Visible="0"/>
		<C Name="PARENT_DIVISION"			Type="Text"	Visible="0"/>
		<C Name="CUR_LVL"					Type="Text"	Visible="0"/>
		<C Name="NEX_LVL"					Type="Text"	Visible="0"/>
		<C Name="COMPANY_ID"				Type="Text"	Visible="0"/>
	</Cols>
	
	<Toolbar Visible="0"/>
	

</Grid>