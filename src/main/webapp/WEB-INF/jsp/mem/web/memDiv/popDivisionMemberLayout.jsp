<?xml version="1.0" encoding="UTF-8"?>
<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<Grid>
	<Cfg	id="MemberNameList"
			IdChars="0123456789"	SortIcons="0"			Calculated="1"			CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"			SelectingCells="0"		AcceptEnters="1"			Style="Office"
			InEditMode="2"			SafeCSS='1'				NoVScroll="0"			NoHScroll="0"				
			Filtering="1"			Dragging="0"			Selecting="1" Deleting="0"
			
	/>
	

	<Header	id="Header"	Align="center"
					MEMBER	= "사용자Id"
					NAME	= "이름"
	/>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<Cols>
		<C Name="MEMBER"	Type="Text" RelWidth="100" Visible ="0" CanExport="1"/>
		<C Name="NAME"		Type="Text" RelWidth="100" CanExport="1"/>
	</Cols>
	

	<Toolbar	Space="0"	Styles="2"	
		Cells = "Empty,Cnt,Sel,선택,새로고침,Print,Export"
		EmptyType = "Html"	EmptyRelWidth = "1"
		ColumnsType = "Button"
		CntType = "Html" CntFormula    = '"총 : &lt;b>"+count(7)+"&lt;/b>행"' CntWidth = '-1' CntWrap = '0'
		RowsType ="Html" 	RowsFormula	= "'총 : '+ count(5) + '행'" RowsWidth='-1' RowsWrap='0'
		SelType = "Html" SelFormula = 'var cnt=count(15);return cnt?"선택된 행:&lt;b>"+cnt+"&lt;/b>":""' SelWidth='-1' SelWrap='0'
		선택Type = "Html" 선택="&lt;p class=&quot;btn comm st01&quot;
						 onclick='expnAddMember(&quot;MemberNameList&quot;)'>선택&lt;/p>"   선택Height="25"	
		새로고침Type = "Html" 새로고침 = "&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
			                onclick='reloadGrid(&quot;MemberNameList&quot;)'>새로고침&lt;/a>"
		PrintType = "Html" Print = "&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
		 				   onclick='printGrid(&quot;MemberNameList&quot;)'>인쇄&lt;/a>"
		ExportType = "Html" Export = "&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
		 				    onclick='exportGrid(&quot;MemberNameList&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>

</Grid>