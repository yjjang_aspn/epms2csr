<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
	Class Name	: popMemberCheck.jsp
	Description : [사용자관리] : ID중복확인 팝업
    author		: 서정민
    since		: 2018.05.08
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.08	 서정민		최초 생성

--%>


<script type="text/javascript">
	
	var memberCheckYn = false;		<%-- ID 중복체크 여부 --%>
	
	$(document).ready(function(){
		
		<%-- id 변경시 --%>
		$('#user_id').on('change', function() {
			memberCheckYn = false;
		});
		
		<%-- 중복체크 조회 --%>
		$('#productAjaxBtn').on('click', function() {
			fnAjaxProductList();
		});
		
	});

	<%-- 중복체크 조회 --%>
	function fnAjaxProductList(){
		
		memberCheckYn = false;
		
		var userId = $("#user_id").val();
		
		if(userId == ""){
			$("#returnMsg").val("ID를 입력하세요.");
			$("#returnMsg").css('color','red');
			$("#user_id").val("");
			return false;
		}
		
		$.ajax({
			url      : "/mem/member/fnAjaxMemberCheckAjax.do",
			datatype : "json",
			data     : {
						userId : userId,
						forMenu : '${param.forMenu}'
						},
			success  : function(data) {
						if(data == '0') {
							$("#returnMsg").val("사용할 수 있는 ID 입니다.");
							$("#returnMsg").css('color','blue');
							memberCheckYn = true;
							setPop();
						} else {
							$("#returnMsg").val("사용할 수 없는 ID 입니다.");
							$("#returnMsg").css('color','red');
							$("#user_id").val("");
							memberCheckYn = false;
						}
			}
		});
	}

	<%-- 아이디 세팅 --%>
	function setPop() {
		if(memberCheckYn == true){
			changeId($("#user_id").val());
			fnModalToggle('popMemberCheck');
		}
		else{
			fnAjaxProductList();
		}
	}

</script>   

<div class="modal fade modalFocus" id="popMemberCheck" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root" style="width:450px;"> <%-- 원하는 width 값 입력 --%>				
		 <div class="modal-content" style="height:200px;"> <%-- 원하는 height 값 입력 --%>
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">사용자 ID 중복체크</h4>
			</div>
		 	<div class="modal-body">
	           	<div class="modal-bodyIn">
	           		
<!-- 					<div class="inp-has-btn"> -->
					<div>
						<div class="inp-wrap wd-per-100">
							<input type="text" class="inpTxt" placeholder="ID를 입력하세요" title="ID입력" id="user_id"/>
						</div>
<!-- 						<a href="#none" class="btn comm st02 btnLinkAdd" id="productAjaxBtn">조회</a> -->
					</div>
					<div class="inp-msg">
						<input type="text" class="inpTxt"  id="returnMsg" readonly="readonly"/>
					</div>
					
				</div> <%-- modal-bodyIn : e --%>			
			</div> <%-- modal-body : e --%>
			
			<div class="modal-footer hasFooter">
				 <a href="#none" class="btn comm st01" onclick="setPop(); return false;"><span class="icon checked"></span>적용하기</a>
			</div>
		
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>  

