<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%--
	Class Name	: divisionLayout.jsp
	Description : [사용자관리 ] : 부서 그리드 레이아웃
    author		: 서정민
    since		: 2018.05.08
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.08	 서정민		최초 생성

--%>

<c:set var="gridId" value="DivisionList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="0"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1"			SuppressCfg="0"			
			Editing="1"				Deleting="1"		Selecting="0"		Dragging="1"					
			Filtering="1"			StandardFilter="3"	ClearSelected="2"
			Paging="0"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"				
			CopyCols="3"			PasteFocused="3"	
			MainCol="NAME"
	/>
	
	<Head>
		<Header	id="Header"	Align="center" CanExport="1" CanHide="0"
				DIVISION		= "<spring:message code='division.divisionId'/>"
				DIVISION_UID	= "부서코드"
				NAME			= "<spring:message code='division.divisionNm'/>"
				PARENT_DIVISION	= "<spring:message code='division.parentDivision'/>"
				TREE			= "구조체"
				DEPTH			= "레벨"
				SEQ_DSP			= "출력순서"
				COMPANY_ID		= "회사코드"
				LOCATION		= "공장코드"
		/>
		<Filter	id="Filter" CanExport="1" CanHide="0" CanFocus="1"/>
	</Head>
	
	<Cols>
		<C Name="DIVISION"			Type="Text" Align="left"   Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="DIVISION_UID"		Type="Text" Align="left"   Visible="0" Width="100"    CanEdit="0" CanExport="1" CanHide="1"/>
	    <C Name="NAME"				Type="Text" Align="left"   Visible="1" RelWidth="100" CanEdit="1" CanExport="1" CanHide="1" Cursor="hand"/>
		<C Name="PARENT_DIVISION"	Type="Text" Align="left"   Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="TREE"				Type="Text" Align="left"   Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="DEPTH"				Type="Text" Align="center" Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="SEQ_DSP"			Type="Text" Align="center" Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="COMPANY_ID"		Type="Text" Align="center" Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="LOCATION"			Type="Text" Align="center" Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
	</Cols>
	
	
	<Toolbar	Space="0"	Styles="1" Cells="Empty,Cnt,<spring:message code='button.add' />,<spring:message code='button.save' />,<spring:message code='button.refresh' />,<spring:message code='button.print' />,<spring:message code='button.excel' />"
			EmptyType = "Html"  EmptyWidth = "1" Empty="        "
			ColumnsType="Button"
			CntType = "Html" CntFormula = '"총 : &lt;b>"+count(7)+"&lt;/b>행"' CntWidth = '-1' CntWrap = '0'
			<spring:message code='button.add' />Type="Html" <spring:message code='button.add' />="&lt;a href='#none' title='<spring:message code='button.add' />' class=&quot;defaultButton icon add&quot;
				onclick='addRowGrid(&quot;${gridId}&quot;)'><spring:message code='button.add' />&lt;/a>"
			<spring:message code='button.save' />Type="Html" <spring:message code='button.save' />="&lt;a href='#none' title='<spring:message code='button.save' />' class=&quot;treeButton treeSave&quot;
				onclick='fnSave(&quot;${gridId}&quot;)'><spring:message code='button.save' />&lt;/a>"
			<spring:message code='button.refresh' />Type = "Html" <spring:message code='button.refresh' /> = "&lt;a href='#none' title='<spring:message code='button.refresh' />' class=&quot;defaultButton icon refresh&quot;
				onclick='reloadGrid(&quot;${gridId}&quot;)'><spring:message code='button.refresh' />&lt;/a>"
			<spring:message code='button.print' />Type = "Html" <spring:message code='button.print' /> = "&lt;a href='#none' title='<spring:message code='button.print' />' class=&quot;defaultButton icon printer&quot;
				onclick='printGrid(&quot;${gridId}&quot;)'><spring:message code='button.print' />&lt;/a>"
			<spring:message code='button.excel' />Type = "Html" <spring:message code='button.excel' /> = "&lt;a href='#none' title='<spring:message code='button.excel' />'class=&quot;defaultButton icon excel&quot;
				onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'><spring:message code='button.excel' />&lt;/a>"
		/>	

</Grid>