<%@ page language="java"  contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: memberLayout.jsp
	Description : [사용자관리] : 사용자 그리드 레이아웃
    author		: 서정민
    since		: 2018.05.08
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.08	 서정민		최초 생성

--%>
<c:set var="gridId" value="MemberList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="0"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1"			SuppressCfg="0"			
			Editing="1"				Deleting="0"		Selecting="0"		Dragging="0"					
			Filtering="1"			StandardFilter="3"	ClearSelected="2"
			Paging="1"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"				
			CopyCols="3"			PasteFocused="3"
	/>
	
	<Head>
		<Header	id="Header"	Align="center" CanExport="1" CanHide="0"
			USER_ID				= "ID"
			NAME 				= "이름"
			COMPANY_ID			= "회사코드"
			ORG_DIVISION		= "부서(수정전)"
			DIVISION	 		= "부서"
			LOCATION			= "근무지"
			PART				= "정비파트"
			AUTH				= "메뉴권한"
			
			MEMBER_ID			= "사번"
			JOB_GRADE			= "직위"
			JOB_POSITION		= "직책"
			PHONE 				= "휴대폰"
			MEMO 				= "출력순서"
			ATTACH_GRP_NO 		= "사진"
			PASSWORD			= "비밀번호"
			DATE_E				= "최종접속일"
			
			CD_MEMBER_TYPE 		= "회원유형"
			CD_MEMBER_STATUS	= "회원상태"
		/>
		<Filter	id="Filter" CanExport="1" CanHide="0" CanFocus="1"/>
	</Head>
	
	<Solid>
		<I  id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4" CanHide="0"
		 	NAVType="Pager"
		 	LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
		 	ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
		 	ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
		 	GROUPCanFocus="0"
		 />
	</Solid>
	
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
	
	<LeftCols>
		<C Name="USER_ID"			Type="Text" Align="left"   Visible="1" Width="100" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="100"/>
		<C Name="NAME"				Type="Text" Align="left"   Visible="1" Width="100" CanEdit="1" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="100"/>
	</LeftCols>
	
	<Cols>
		<C Name="COMPANY_ID"		Type="Text" Align="left"   Visible="0" Width="100" CanEdit="0" CanExport="0" CanHide="0" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="100"/>
		<C Name="ORG_DIVISION"		Type="Enum" Align="left"   Visible="0" Width="100" CanEdit="0" CanExport="0" CanHide="0" RelWidth="100" MinWidth="100" Enum='${divisionOpt.DIVISION_NM}' EnumKeys='${divisionOpt.DIVISION}'/>
		<C Name="DIVISION"			Type="Enum" Align="left"   Visible="1" Width="120" CanEdit="1" CanExport="1" CanHide="1" RelWidth="120" MinWidth="120" Enum='${divisionOpt.DIVISION_NM}' EnumKeys='${divisionOpt.DIVISION}'/>
		<C Name="MEMBER_ID"			Type="Text" Align="left"   Visible="1" Width="100" CanEdit="1" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="100"/>
		<C Name="JOB_GRADE"			Type="Enum" Align="left"   Visible="1" Width="100" CanEdit="1" CanExport="1" CanHide="1" RelWidth="100" MinWidth="100" <tag:enum codeGrp="JOB_GRADE"/>/>
		<C Name="JOB_POSITION"		Type="Enum" Align="left"   Visible="0" Width="100" CanEdit="1" CanExport="0" CanHide="0" RelWidth="100" MinWidth="100" <tag:enum codeGrp="JOB_POSITION"/>/>	
		<C Name="PHONE"				Type="Text" Align="left"   Visible="1" Width="120" CanEdit="1" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="120" MinWidth="120"/>
		<C Name="MEMO"				Type="Int"  Align="center" Visible="1" Width="80"  CanEdit="1" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="80" MinWidth="80"/>
		<C Name="ATTACH_GRP_NO"		Type="Icon" Align="center" Visible="1" Width="80"  CanEdit="0" CanExport="0" CanHide="0" RelWidth="80"  MinWidth="80"/>
		<C Name="PASSWORD"			Type="Icon" Align="center" Visible="1" Width="80"  CanEdit="0" CanExport="0" CanHide="0" RelWidth="80"  MinWidth="80"/>
		<C Name="DATE_E"			Type="Date" Align="center" Visible="0" Width="110" CanEdit="0" CanExport="0" CanHide="0" RelWidth="110" MinWidth="110" Format="yyyy/MM/dd"/>
		<C Name="CD_MEMBER_TYPE"	Type="Enum" Align="left"   Visible="1" Width="80"  CanEdit="1" CanExport="1" CanHide="1" RelWidth="80"  MinWidth="80" <tag:enum codeGrp="MEMBER_TYPE" />/>
		<C Name="CD_MEMBER_STATUS"	Type="Enum" Align="left"   Visible="1" Width="80"  CanEdit="1" CanExport="1" CanHide="1" RelWidth="80"  MinWidth="80" <tag:enum codeGrp="MEMBER_STATUS"/>/>
	</Cols>
	
	<Toolbar	Space="0"	Styles="2" Cells="Empty,Cnt,추가,저장,항목,새로고침,인쇄,엑셀"
			EmptyType="Html"	EmptyRelWidth="1" Empty="        "
			ColumnsType="Button"
			CntType="Html" CntFormula='"총 : &lt;b>"+count()+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
			추가Type="Html" 추가="&lt;a href='#none' title='추가' class=&quot;defaultButton icon add&quot;
				onclick='addRowGrid(&quot;${gridId}&quot;)'>추가&lt;/a>"
			저장Type="Html" 저장="&lt;a href='#none' title='저장' class=&quot;treeButton treeSave&quot;
				onclick='fnSave(&quot;${gridId}&quot;)'>저장&lt;/a>"
			항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
				onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
			새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
				onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
			인쇄Type = "Html" 인쇄 = "&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
				onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
			엑셀Type = "Html" 엑셀 = "&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
				onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"	
	/>
</Grid>