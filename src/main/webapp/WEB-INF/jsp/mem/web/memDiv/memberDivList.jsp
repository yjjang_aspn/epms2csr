<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"	uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: memberDivList.jsp
	Description : [사용자관리] : 화면
    author		: 서정민
    since		: 2018.05.08
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.08	 서정민		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<script type="text/javascript">

<%-- 전역변수 --%>
var focusedRow = null;		<%-- Focus Row : [부서] --%>
var focusedRow2 = null;		<%-- Focus Row : [사용자] --%>
var lastChildRow = null;	<%-- Last Child Row : [부서] --%>

$(document).ready(function(){
	
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function (e) {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>	
	});	
});

Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "DivisionList") {	<%-- 1. [부서] 클릭시 --%>
	
		<%-- 1-1. 포커스 --%>
		if(focusedRow != null){
			grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow = row;
		focusedRow2 = null;
		lastChildRow = row.lastChild;
		
		<%-- 1-2. 사용자 조회 --%>
		if (row.DIVISION != null && row.DIVISION != "" && row.Added != 1) {

			getMemberList(row.TREE);
		}
	}
	else if(grid.id == "MemberList") {	<%-- 2. [사용자] 클릭시 --%>
		<%-- 2-1. 포커스  --%>
		if(focusedRow2 != null){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow2 = row;
	}

}

<%-- 선택된 부서에 해당되는 사용자 조회 --%>
function getMemberList(TREE){
	Grids.MemberList.Source.Data.Url = "/mem/member/memberData.do?TREE="+TREE;
	Grids.MemberList.ReloadBody();	
}

Grids.OnDataReceive = function(grid,source){
	if(grid.id == "MemberList"){
		var attach_grp_no = "";
		
		for(var row=grid.GetFirst(null);row;row=grid.GetNext(row)){
			
			if(row.ATTACH_GRP_NO == null || row.ATTACH_GRP_NO == 'undefined'){
				attach_grp_no = "";
			}
			else{
				attach_grp_no = row.ATTACH_GRP_NO;
			}
			
			if(row.FILE_CNT > 0){
				grid.SetAttribute(row, "ATTACH_GRP_NO", "Switch","1",1);
				grid.SetAttribute(row, "ATTACH_GRP_NO", "Icon", "/images/com/web/modification_icon.gif",1);
				grid.SetAttribute(row, "ATTACH_GRP_NO", "OnClickSideIcon", "fnOpenLayerWithParam('/attach/popFileManageForm.do?USER_ID="+row.USER_ID+"&ATTACH_GRP_NO="+attach_grp_no+"&flag=profile&maxFileCnt=1&editYn=Y', 'popFileManageForm');", 1);
			}else{
				grid.SetAttribute(row, "ATTACH_GRP_NO", "Switch","1",1);
				grid.SetAttribute(row, "ATTACH_GRP_NO", "Icon", "/images/com/web/upload_icon.gif",1);
				grid.SetAttribute(row, "ATTACH_GRP_NO", "OnClickSideIcon", "fnOpenLayerWithParam('/attach/popFileManageForm.do?USER_ID="+row.USER_ID+"&ATTACH_GRP_NO="+attach_grp_no+"&flag=profile&maxFileCnt=1&editYn=Y', 'popFileManageForm');", 1);
			}
			grid.SetAttribute(row, "PASSWORD", "Icon", "/images/com/web/btn_s_reset.gif",1);
			grid.SetAttribute(row, "PASSWORD", "OnClickSideIcon", "changePassword('"+row.USER_ID+"', '"+row.NAME+"')", 1);
		}
		
	}
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- [추가] 버튼 --%>
function addRowGrid(gridNm) {
	
	if (gridNm == "DivisionList") {
		
		if (focusedRow == null || focusedRow == 'undefined') {	<%-- 선택된 부서가 없을 경우 --%>
			
			return false;
		}
		else {													<%-- 선택된 부서가 있을 경우, 하위 레벨 부서 추가 --%>
			
			Grids[gridNm].SetAttribute(focusedRow,null,"Expanded",1,1);
			
			row = Grids[gridNm].AddRow(focusedRow, null, true);
			Grids[gridNm].SetAttribute(row,"DIVISION_UID","CanEdit","1",1);				<%-- 부서코드 수정 가능 --%>
			Grids[gridNm].SetValue(row, "PARENT_DIVISION", focusedRow.DIVISION, 1);		<%-- 부모부서 ID --%>
			Grids[gridNm].SetValue(row, "TREE", focusedRow.TREE, 1);					<%-- 구조체 --%>
			Grids[gridNm].SetValue(row, "DEPTH", focusedRow.DEPTH + 1, 1);				<%-- 레벨 --%>
			Grids[gridNm].SetValue(row, "COMPANY_ID", focusedRow.COMPANY_ID, 1);		<%-- 회사코드 --%>
			Grids[gridNm].SetValue(row, "LOCATION", focusedRow.LOCATION, 1);			<%-- 공장코드 --%>
			<%-- 출력순서 --%>
			if(lastChildRow == null || lastChildRow == 'undefined'){
				Grids[gridNm].SetValue(row, "SEQ_DSP", "1", 1);					
			}else{
				Grids[gridNm].SetValue(row, "SEQ_DSP", lastChildRow.SEQ_DSP+1, 1);
			}
			lastChildRow = row;
		}
	}
	else if(gridNm == "MemberList"){
		
		if (focusedRow == null || focusedRow == 'undefined') {	<%-- 선택된 부서가 없을 경우 --%>
			
			return false;
		}
		else{
			
			var row = Grids[gridNm].AddRow(null, Grids[gridNm].GetFirst(), true);

			Grids[gridNm].SetValue(row, "USER_IDIcon","/images/com/web/b-search.png", 1);
			Grids[gridNm].SetValue(row, "USER_IDOnClickSideIcon","checkMemberId()", 1);
			Grids[gridNm].SetValue(row, "DIVISION", focusedRow.DIVISION, 1);
			Grids[gridNm].SetValue(row, "COMPANY_ID", focusedRow.COMPANY_ID, 1);
			Grids[gridNm].SetValue(row, "CD_MEMBER_TYPE", "01", 1);
			Grids[gridNm].SetValue(row, "CD_MEMBER_STATUS", "01", 1);
			Grids[gridNm].RefreshRow(row);
		}
	}
}

<%-- 드래그시 출력순서 세팅 : 드래그한 row의  data 세팅 --%>
Grids.OnRowMove = function (grid, row, oldparent, oldnext){
	
	if(grid.id == "DivisionList"){	<%-- [라인]의 경우 --%>
		
		var parent_division;
		var tree;
		var depth;
		
		if(row.parentNode.tagName != 'I'){	<%-- 1레벨로 이동할 경우 --%>
			
			parent_division = "";
			tree = row.CATEGORY;
			depth = 1;
		}
		else {								<%-- 2레벨 이하로 이동할 경우 --%>
			
			parent_division = row.parentNode.DIVISION;
			if(row.Added == true){
				tree = row.parentNode.TREE;
			}
			else{
				tree = row.parentNode.TREE+"$"+row.DIVISION;
			}
			depth = row.parentNode.DEPTH+1;
		}
				
		<%-- 1. 부모부서  ID --%>
		grid.SetValue(row, "PARENT_DIVISION", parent_division, 1);
		
		<%-- 2. 구조체 --%>
		grid.SetValue(row, "TREE", tree, 1);
		
		<%-- 3. 레벨 --%>
		grid.SetValue(row, "DEPTH", depth, 1);		
		
		<%-- 4. 출력순서 --%>
		<%-- 4-1. 이동 이전 부모 이하 부서의 출력순서 변경 --%>
		var i =1;
		var lineRow = grid.GetFirst(oldparent);
		if(!(lineRow == null || lineRow == 'undefined')){
			do{
				
				grid.SetValue(lineRow, "SEQ_DSP", i, 1);
				lineRow = lineRow.nextSibling;
				i++;
				
			}while(!(lineRow == null || lineRow == 'undefined'));
		}
		<%-- 4-2. 이동 이후 부모 이하 부서의 출력순서 변경 --%>
		i=1;
		var lineRow2 = grid.GetFirst(row.parentNode);
		if(!(lineRow2 == null || lineRow2 == 'undefined')){
			do{
				
				grid.SetValue(lineRow2, "SEQ_DSP", i, 1);
				lineRow2 = lineRow2.nextSibling;
				i++;
				
			}while(!(lineRow2 == null || lineRow2 == 'undefined'));
		}			
	}
};

<%-- ID 중복체크 팝업 호출 --%>
function checkMemberId(){
	
	$("#returnMsg").val("");
	$("#user_id").val("");
	
	fnModalToggle('popMemberCheck');
}

<%-- 사용자 ID 세팅 --%>
function changeId(user_id){
	Grids["MemberList"].SetValue(focusedRow2, "USER_ID", user_id, 1);
}

function changePassword(USER_ID, NAME) {	
	if(confirm("[ "+NAME+" ]님의 비밀번호를 초기화 하시겠습니까?")){
		$.ajax({
			type: 'POST',
			url: '/mem/member/changePwAjax.do?USER_ID='+USER_ID,
			dataType: 'json',
			success: function (json) {
				if(json.result > 0){
					alert("비밀번호가 사용자의 ID로 초기화 되었습니다.");
				}else{
					alert("비밀번호 초기화 실패.");
				}	
			},error: function(XMLHttpRequest, textStatus, errorThrown){
				alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' +'errorCode : ' + textStatus+" "+errorThrown );
			}
		});
	}
}

<%-- Validation --%>
function dataValidation(gridNm){
	var cnt = 0;
	var chkCnt = 0;
	var grid = Grids[gridNm];
	
	if(gridNm == "DivisionList"){
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){  
			if(row.Changed || row.Added){
				
				cnt++;
				
				<%-- 부서명 --%>
				var NAME = grid.GetValue(row, "NAME");
				if(NAME==''){
					chkCnt++;
					grid.SetAttribute(row,"NAME","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"NAME","Color","#FFFFAA",1);
				}
			}
			else if(row.Deleted){
				cnt++;
			}
		}
	}	
	else if(gridNm == "MemberList"){
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow2);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){  
			if(row.Changed || row.Added){
				
				cnt++;
				
				<%-- ID --%>
				var USER_ID  = grid.GetValue(row, "USER_ID");
				if(USER_ID==''){
					chkCnt++;
					grid.SetAttribute(row,"USER_ID","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"USER_ID","Color","#FFFFAA",1);
				}
				
				<%-- 이름 --%>
				var NAME = grid.GetValue(row, "NAME");
				if(NAME==''){
					grid.SetAttribute(row,"NAME","Color","#FF6969",1);	
					cnt++;
				}else{
					grid.SetAttribute(row,"NAME","Color","#FFFFAA",1);
				}

				<%-- 부서 --%>
				var DIVISION = grid.GetValue(row, "DIVISION");
				if(DIVISION==''){
					chkCnt++;
					grid.SetAttribute(row,"DIVISION","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"DIVISION","Color","#FFFFAA",1);
				}
			}
		}
	}
	
	<%-- 추가 및 변경된 데이터가 없을 경우 --%>
	if(cnt == 0){
		return false;
	}
	
	<%-- 필수값 체크 --%>
	if(chkCnt == 0){
		return true;
	}
	else{
		alert("필수값을 입력해주세요.");
		return false;
	}
}

<%-- [저장] 버튼 : 부서 및 사용자 저장 --%>
function fnSave(gridNm){
	
	<%-- 유효성 체크 --%>
    if(!dataValidation(gridNm)){
    	return;
    }
	
	if(Grids[gridNm].ActionValidate()){
		if(gridNm == "DivisionList"){
			if(confirm("부서정보를 저장하시겠습니까?")){
	
				Grids[gridNm].ActionSave();
	
			}
		}
		else if(gridNm == "MemberList"){
			if(confirm("사용자정보를 저장하시겠습니까?")){

				Grids[gridNm].ActionSave();

			}
		}
	}
}

<%-- 그리드 데이터 저장 후 --%> 
Grids.OnAfterSave  = function (grid){
	if(grid.id == "DivisionList"){
		
		grid.ReloadBody();
		Grids["MemberList"].Reload();
		
	} else if (grid.id == "MemberList"){
		
		grid.ReloadBody();
		
	}
};

<%-- 프로필 이미지 변경 이후 새로고침 --%>
function fnReload_attachEdit(){
	
	Grids.MemberList.ReloadBody();
	alert("정상적으로 등록 되었습니다.");
	
}

</script>
</head>

<body>
<div id="contents">

	<div class="fl-box panel-wrap03 leftPanelArea" style="width: 20%; min-width: 350px;">
		<h5 class="panel-tit">부서</h5>
		<div class="panel-body">
			<div id="memberPrivList">
				<bdo	Debug="Error"
						Data_Url="/mem/division/divisionData.do"
						Layout_Url="/mem/division/divisionLayout.do"
						Upload_Url="/mem/division/divisionEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"			
						Export_Data="data" Export_Type="xls"
						Export_Url="/com/exportGridData.jsp?File=DivisionList.xls&dataName=data"
						>
				</bdo>
			</div>
		</div>
	</div>

	<div class="fl-box panel-wrap03 rightPanelArea">
		<h5 class="panel-tit mgn-l-10">사용자</h5>
		<div class="panel-body mgn-l-10">
			<div id="userListDiv">
				<bdo	Debug="Error" 
						Layout_Url="/mem/member/memberLayout.do"
						Upload_Url="/mem/member/memberEdit.do" Upload_Data="uploadData" Upload_Xml="2" Upload_Format="Internal" Upload_Flags="AllCols"	
						Export_Data="data" Export_Type="xls"
						Export_Url="/com/exportGridData.jsp?File=MemberList.xls&dataName=data">
				</bdo>
			</div>
		</div>
	</div>
	
</div>

<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<%-- ID 중복체크 팝업 --%>
<%@ include file="/WEB-INF/jsp/mem/web/memDiv/popMemberCheck.jsp"%>

<%-- 첨부파일 조회/수정 --%>
<div class="modal fade modalFocus" id="popFileManageForm"    data-backdrop="static" data-keyboard="false"></div>	
</body>
</html>		
