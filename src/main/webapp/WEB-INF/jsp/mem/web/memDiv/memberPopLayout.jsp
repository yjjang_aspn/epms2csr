<%@ page language="java"  contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %> 
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="MemberList"
			IdChars="0123456789" 	SortIcons="0"       Calculated="1"    	CalculateSelected ="1"  	NoFormatEscape="1"
			NumberId="1"         	DateStrings="2"     SelectingCells="0"  AcceptEnters="1"        	Style="Office"
			InEditMode="2"       	SafeCSS='1'         NoVScroll="0"       NoHScroll="0"           	EnterMode="0"
			Filtering="1"         	Dragging="0"        Selecting="0" 		 
			Paging="1"			  	PageLength="10"		MaxPages="20"
			CopySelected="0"	    CopyFocused="1"     CopyCols="0"
			Deleting="0" 			Editing ="0"		Selecting ="1"
			
	/>
	
	<Solid>
		<I  id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
		 	NAVType="Pager"
		 	LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
		 	ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
		 	ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
		 	GROUPCanFocus="0"
		 />
	</Solid>
	
	<Pager Visible="0"/>
	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>
	
	<Header	id="Header"	Align="center"
			USER_ID				= "사용자Id"
			DIVISION_NM 		= "부서명"
			DIVISION	 		= "부서코드"
			NAME 				= "이름"
			JOB_GRADE			= "직급"
			JOB_POSITION		= "직책"
	/>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<LeftCols>
		<C Name="USER_ID"					Type="Text" Width="100" CanEdit="0"/>
		<C Name="NAME"						Type="Text" Width="100"/>
		
	</LeftCols>
	
	<Cols>
		<C Name="JOB_GRADE"					Type="Enum" 	Width="60" 	EmptyValue="선택" <tag:enum codeGrp="JOB_GRADE" />  />
		<C Name="JOB_POSITION"				Type="Enum" 	Width="60"	EmptyValue="선택" <tag:enum codeGrp="JOB_POSITION" /> />
		<C Name="DIVISION"					Type="Text"		Width="60" CanEdit="0"    Visible ="0"/>
		<C Name="DIVISION_NM"				Type="Text" 	Width="60" CanEdit="0"    Visible ="0"/>
	</Cols>
	
	<Toolbar	Visible ="0"/>
</Grid>