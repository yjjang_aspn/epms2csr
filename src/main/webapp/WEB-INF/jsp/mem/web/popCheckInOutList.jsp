<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"        %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<style>
	th, td {text-align:center !important;}
	.pop-body > div {padding:0 !important;}
	.adcSwitchBtn2{position:relative;width:54px;-webkit-user-select:none;-moz-user-select:none;-ms-user-select: none;float: left;margin-top:0 !important;
							position:absolute; top:2px; left:50%; margin-left:-27px;}
	.adcSwitchBtn-checkbox2{display:none;}
	.adcSwitchBtn-label2{display:block; overflow:hidden; cursor:pointer; border-radius:20px;}
	.adcSwitchBtn-inner2{display:block; width:200%; margin-left:-100%; transition: margin 0.3s ease-in 0s;}
	.adcSwitchBtn-inner2:before, .adcSwitchBtn-inner2:after{display:block; float:left; width:50%; padding:0; height:23px; line-height:23px; font-size:12px; color:#ffffff; box-sizing:border-box; -moz-box-sizing:border-box;}
	.adcSwitchBtn-inner2:before{content:"ON"; padding-left:10px; background-color:#ee7d45; color:#ffffff; text-align:left;}
	.adcSwitchBtn-inner2:after{content:"OFF"; padding-right:10px; background-color:#999999; color:#ffffff; text-align:right;}
	.adcSwitchBtn-switch2{display:block; width:11px; margin:6px; background:#ffffff; position:absolute; top:0; bottom:0; right:30px; border-radius:20px; transition: all 0.3s ease-in 0s;}
	.adcSwitchBtn-checkbox2:checked + .adcSwitchBtn-label2 .adcSwitchBtn-inner2{margin-left:0;}
	.adcSwitchBtn-checkbox2:checked + .adcSwitchBtn-label2 .adcSwitchBtn-switch2{right:0px;}
</style>

<!-- Layer PopUp Start -->
<div class="modal-dialog root wd-per-30 hgt-per-60" style="min-width:600px; min-height:500px"> <!-- 원하는 width 값 입력 -->				
	 <div class="modal-content">
		<div class="modal-header">
			
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title"><spring:message code='epms.worker' /> 현황</h4><!-- 정비원 현황 -->
		</div>
	 	<div class="modal-body" >
           	<div class="modal-bodyIn" style="margin-top: 25px; width: 550px;">
           		 <h5 class="panel-tit" style="margin-top: 19px;">출퇴근 이력조회</h5>
				 <div class="inq-area-inner type08" style="margin-bottom: 12px;margin-top: 12px;">
				 	<div class="wrap-inq">
				 		<div class="inq-clmn">
				 			<h4 class="tit-inq">구분</h4>
				 			<div class="sel-wrap type02">
				 				<select title="구분" id="srcType" name="srcType">
				 					<option value="all" selected="selected">출/퇴근</option>
				 					<option value="01">출근</option>
				 					<option value="02">퇴근</option>
				 				</select>
				 			</div>
				 		</div>
				 		<div class="inq-clmn">
				 			<h4 class="tit-inq">검색일</h4>
				 			<div class="prd-inp-wrap">
				 				<span class="prd-inp">
				 					<span class="inner">
				 						<input type="text" class="inp-comm datePic thisMonth inpCal gldp-el" readonly="readonly" id="startDt" title="검색시작일" value="${startDt}">
				 					</span>
				 				</span>
				 				<span class="prd-inp">
				 					<span class="inner">
				 						<input type="text" class="inp-comm datePic inpCal gldp-el" readonly="readonly" id="endDt" title="검색종료일" value="${endDt}">
				 					</span>
				 				</span>		
				 			</div>
				 		</div>
				 	</div>
				  	<a href="#none" id="srcBtn" class="btn comm st01">검색</a>
				 </div>
				
				 <div class="tb-wrap">
					<table class="tb-st" summary="출퇴근 이력조회 테이블" id="popCheckInOutTable">
					<input type="hidden" id="user_id2" value="${userId}"/>
						<caption>출퇴근 이력조회 테이블</caption>
							<colgroup>																	
								<col width="30%"/>
								<col width="70%"/>
							</colgroup>
						<thead>
							<tr>																	
								<th>유형</th>
								<th>등록일시</th>
							</tr>
						</thead>
						<tbody id="popCheckInOutListbody">
							<c:forEach var="item" items="${list}">
								<tr>
									<c:if test="${item.TYPE eq '01'}">
									<td name="TYPE" style="color: blue">${item.TYPE_NAME}</td>
									</c:if>
									<c:if test="${item.TYPE eq '02'}">
									<td name="TYPE">${item.TYPE_NAME}</td>
									</c:if>
									<td name="REG_DT">${item.REG_DT}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
           	
			</div>
		</div>	
	</div>		
</div>

<!-- 페이지 내용 : e -->
<script type="text/javascript">

	$(document).ready(function() {
		<%-- 닫기 버튼 이벤트  --%>
		$('.closeBtn').on('click', function(){
			window.close();
		});
			
		<%-- datePic 설정 --%>
		$('.datePic').glDatePicker({
			showAlways:false,
			cssName:'flatwhite',
			allowMonthSelect:true,
			allowYearSelect:true,
			onClick:function(target, cell, date, data){
				target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
			}
		});
		
		<%-- 검색 시작일이 종료일보다 이후인지 체크 (이후라면, 종료일을 시작일로 변경) --%>
		$('#startDt').on('change', function(){
			var startDt = $('#startDt').val().replaceAll("-","");
			var endDt = $('#endDt').val().replaceAll("-","");
			
			if(parseInt(startDt ,10) > parseInt(endDt ,10)){
				$('#endDt').val($('#startDt').val());
			}
		});
		
		<%-- 검색 종료일이 시작일보다 이전인지 체크 (이전이라면, 시작일을 종료일로 변경) --%>
		$('#endDt').on('change', function(){
			var startDt = $('#startDt').val().replaceAll("-","");
			var endDt = $('#endDt').val().replaceAll("-","");
			
			if(parseInt(startDt ,10) > parseInt(endDt ,10)){
				$('#startDt').val($('#endDt').val());
			}
		});
		
		<%-- 출퇴근이력 조회 검색 --%>
		$('#srcBtn').on('click', function(){
			var startDt = $('#startDt').val();
			var endDt = $('#endDt').val();
			var srcType = $('select[name=srcType]:visible').val();
			var user_id = $('#user_id2').val();
			$.ajax(
			        {
			            url: '/mem/member/popCheckInOutListAjax.do'
						, type : 'POST'
						, dataType : 'json'
						, data : {startDt : startDt,
							      endDt : endDt, 
							      TYPE: srcType, 
							      USER_ID : user_id}
			            , success: function (result) {
			            	alert("조회가 완료되었습니다.");
			            	successCallback(result.CheckInOutList);
			        	}
			            , error: function (XMLHttpRequest, textStatus, errorThrown) {
			            	alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
		        		}
		       });
			
			
		});
		
	});
	
	
	<%-- 고장/정비 시작/종료시간 기본값 세팅 --%>
	function successCallback(result){
		$('#popCheckInOutListbody').html('');
		
		$.each(result, function(i, obj) {
			
			var checkInOutListTemplate = $("#popCheckInOutListView").html();
			checkInOutListTemplate = $(checkInOutListTemplate);
			
			for (var name in obj) {
				switch (name) {
					case "TYPE_NAME" :
						if(obj[name] == "출근") {
							checkInOutListTemplate.find("[name=TYPE]").attr("role", "01");	
						}else{
							checkInOutListTemplate.find("[name=TYPE]").attr("role", "02");
						}
						checkInOutListTemplate.find("[name=TYPE]").text(obj[name]);
						break;
						
					case "REG_DT" : 
						checkInOutListTemplate.find("[name=REG_DT]").text(obj[name]);
						break;
				}
			}
			$('#popCheckInOutListbody').append(checkInOutListTemplate);
			$('#popCheckInOutListbody').find("td[role=01]").css("color", "blue");
			
		});				
	}
	
</script>

<!-- 페이지 내용 : e -->
<script type="text/template" id="popCheckInOutListView">
	<tr>
		<td name="TYPE"></td>
		<td name="REG_DT"></td>
	</tr>
</script>
