<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : approvalLineMngList.jsp
  Description : 결재라인 관리 목록
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.09.05    윤선철              최초 생성

    author   : 윤선철
    since    : 2017.09.05
--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<link rel="stylesheet" href="/css/eacc/modalPopup.css">
<script src="/js/jquery/jquery-1.11.2.min.js" type="text/javascript" ></script> <!-- 제이쿼리 구동 -->
<script src="/js/jquery/jquery.blockUI.js" type="text/javascript"></script>
<script src="/js/jquery/jquery.calendar.js" type="text/javascript"></script>
<script src="/js/eacc/eaccCommon.js" type="text/javascript" ></script>
<script src="/js/com/web/modalPopup.js"></script>
<script src="/js/jquery/jquery-filestyle.js" type="text/javascript" ></script>
<script src="/js/jquery/jquery.MultiFile.js" type="text/javascript"></script>
<script src="/js/jquery/jquery.ajaxsubmit.js" type="text/javascript"></script>
<script type="text/javascript">
var gubunDiv = '〔';
var apprLine_nm = "";
var apprLine_Ndx = "";
var company_id = "";
var user_id = "";
var layerGubun = "apprPerson"; // 레이어 팝업 구분자(결재라인 추가-apprPerson)
var apprGrid = "PopApprLineList"; 
$(document).ready(function() {
	
});

//그리드 row 클릭시 이빈트
Grids.OnClick = function(grid, row, col) {
	if(row.id == "Filter"     || row.id == "Header"  || row.id == "PAGER" ||
	   row.id == "spanHeader" || row.id == "Toolbar" || col    == "Panel") {
		return;
	}

	if(grid.id == "ApprovalLineMngList") {
		
		$('#approvalDtlNm').text(row.APPRLINE_NM);
		apprLine_nm = row.APPRLINE_NM;
		apprLine_Ndx = row.APPRLINE_NDX;
		company_id = row.COMPANY_ID;
		user_id = row.USER_ID;
		fn_apprLineDetailList(row.APPRLINE_NDX);
	}else if(grid.id == "DivisionList") {
		selDeptRow = row;
		getMemberList(row.DIVISION, row.CUR_LVL, row.COMPANY_ID);
	}
}

//그리드 드래그 시 결재순서 재정렬
Grids.OnRowMove = function(grid, row, oldparent, oldnext) {
	if(row.id == "Filter" || row.id == "Header" || row.id == "PAGER" || row.id == "Toolbar"){
		return;
	}
	
	if(grid.id == "PopApprLineList"){
		fn_countApprSeq();
	}
}

//그리드 삭제 버튼
Grids.OnRowDelete = function(grid, row, type){
  if(grid.id == "PopApprLineList"){
    if(row.Deleted){
      Grids.PopApprLineList.RemoveRow(row);
    }
    fn_countApprSeq();
  }
}

//그리드 변경 
Grids.OnAfterValueChanged = function(grid, row, col, type){
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return; 
	}
	
	if(grid.id == "PopMemberDivList"){
		if(col == "SELECT_YN" && row.SELECT_YN == "1"){
			if(row.USER_ID == "${sessionScope.ssUserId}"){
				alert("본인 선택은 불가합니다.");
				Grids.PopMemberDivList.SetValue(row, "SELECT_YN", "0", 1);
			}
		}
	}
}

/* 그리드 행 추가 */
function addRowGrid(gridNm) {
	var row = null;
	
	if(gridNm == "ApprovalLineMngList"){
		row = Grids[gridNm].AddRow(null, Grids[gridNm].GetFirst(), true);
		Grids[gridNm].RefreshRow(row);
	}
}

/* 결재라인 상세 조회*/
function fn_apprLineDetailList(param){
	
	Grids.ApprovalLineDtlLayout.Source.Data.Url = "/hello/eacc/approval/getApprovalLineDtlData.do?APPRLINE_NDX="+param;
	Grids.ApprovalLineDtlLayout.ReloadBody();	
}

/* 결재라인 상세 설정 팝업*/
function fn_popApprLineList(){
	 
	if(apprLine_Ndx == ''){
		alert("결재라인을 선택해주세요.");
		return;
	}
	
	layer_open('apprLineMngModal');
	var title = apprLine_nm + " 결재선 수정";
	$('#modal_title').text(title);
	fn_getApprLineMng(apprLine_Ndx, company_id, user_id);
}	

/* layerPopUp에 결재라인 설정 상세 화면 출력 */
function fn_getApprLineMng(apprLine_Ndx, company_id, user_id){
	//부서정보 조회
	var divisionUrl = "/mem/division/divisionData.do";
	Grids.DivisionList.Source.Data.Url = divisionUrl;
	Grids.DivisionList.ReloadBody();	
	
	//결재 라인 정보 조회
	var parameters = "?APPRLINE_NDX=" + apprLine_Ndx + "&companyId=" + company_id + "&userId=" + user_id;
	var apprLineUrl = "/hello/eacc/approval/popApprLineData.do" + parameters;
	Grids.PopApprLineList.Source.Data.Url = apprLineUrl;
	Grids.PopApprLineList.ReloadBody();	
}

/* 결재자 순서 재정렬 */
function fn_countApprSeq(){
	var i = 1;
	for(var r =Grids.PopApprLineList.GetFirstVisible(); r; r=Grids.PopApprLineList.GetNextVisible(r)){
		Grids.PopApprLineList.SetValue(r, "APPR_SEQ", i, 1);
		i++;
	}
} 

/* 결재자 수정/저장 */
function fn_saveApprLine(gridNm) {
	
	var companyId = "";
	var line_gb = "";
	var appr_seq = "";
	var appr_id = "";
	var apprLine = "";
	var msg = "";
	var isAble = false;
	if(apprLine_Ndx == null || apprLine_Ndx == ""){
		alert("변경할 결재선이 존재하지 않습니다.");
		return;
	}
	if(confirm(apprLine_nm + "의 결재선을 변경하시겠습니까?")){
		for(var r =Grids.PopApprLineList.GetFirstVisible(); r; r=Grids.PopApprLineList.GetNextVisible(r)){
			//결재 구분 선택 안되었을시 처리
			if(r.LINE_GB == null || r.LINE_GB == ""){
				if(msg == ""){
					msg = r.APPR_NM;
				}else{
					msg += "," + r.APPR_NM;
				}
				isAble = true;
			}
			//선택된 결재자 정보 세팅
			if(appr_id == ""){
				companyId = r.COMPANY_ID;
				line_gb = r.LINE_GB;
				appr_seq = r.APPR_SEQ;
				appr_id = r.APPR_ID;
			}else{
				companyId +=  gubunDiv + r.COMPANY_ID;
				line_gb +=  gubunDiv + r.LINE_GB;
				appr_seq +=  gubunDiv + r.APPR_SEQ;
				appr_id +=  gubunDiv + r.APPR_ID;
			} 
		}
		
		if(isAble){
			alert(msg + "님의 결재 구분을 선택해주시기 바랍니다.");
			return;
		}
		
		jQuery.ajax({
    		type : 'POST',
    		url : '/hello/eacc/approval/editApprLineAjax.do',
    		cache : false,
    		dataType : 'json',
    		data : {
    				APPRLINE_NDX : apprLine_Ndx,
    				COMPANY_ID : companyId,
    				LINE_GB : line_gb,
    				APPR_SEQ : appr_seq,
    				APPR_ID : appr_id
    				},
			async : false,
    		error : function() {
    			alert("처리중 오류가 발생 하였습니다.");
				Grids.PopApprLineList.ReloadBody();
				Grids.ApprovalLineDtlLayout.ReloadBody();
				layer_open('apprLineMngModal');
			},
    		success : function(json) {
    			if(json.resultCd == "T"){
    				alert(json.resultMsg);
    				Grids.PopApprLineList.ReloadBody();
    				Grids.ApprovalLineDtlLayout.ReloadBody();
    				layer_open('apprLineMngModal');
    			}else{
    				if(typeof json.errorMsg == 'undefined' || json.errorMsg == 'undefined'){
    					alert(json.resultMsg);
        				Grids.PopApprLineList.ReloadBody();
        				Grids.ApprovalLineDtlLayout.ReloadBody();
        				layer_open('apprLineMngModal');
    				}else{
    					alert(json.resultMsg + "\n" + json.errorMsg);
        				Grids.PopApprLineList.ReloadBody();
        				Grids.ApprovalLineDtlLayout.ReloadBody();
        				layer_open('apprLineMngModal');
    				}
    			}
			}
    	});
	}
}

</script>
</head>
<body>
<!-- 페이지 내용 : s -->

<!-- s:input -->
<form id="myApprFrm" name=myApprFrm method="post">
	<input type="hidden" id="MENU"       name="forMenu"    value="${menu}"/>
	<input type="hidden" id="MENU_GROUP" name="MENU_GROUP" value=""/>
	<input type="hidden" id="MENU_URL"   name="MENU_URL"   value=""/>
</form>
<!-- e:input -->

<div id="contents">
	<!-- 페이지 내용 : s -->
	<!-- Left Area Start  -->
	<!-- s:panel-wrap01 -->
	<div class="fl-box panel-wrap03" style="width:50%; height:100%;">
		<!-- Left Title Area Start  -->
		<h5 class="panel-tit">결재 라인 관리</h5>
		<!-- Left Title Area End    -->
		
		<!-- Left Grid Area Start   -->
		<div class="panel-body mgn-t-5">
			<!-- 트리그리드 : s -->
			<div id="ApprovalLineMngList" style="height:100%">
				<bdo Debug="Error"
					 Data_Url="/hello/eacc/approval/getApprovalLineMngData.do" Data_Format="String"
					 Layout_Url="/hello/eacc/approval/approvalLineMngLayout.do"
					 Upload_Url="/hello/eacc/approval/approvalLineMngEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols" Upload_Xml="2">
				</bdo>
			</div>
			<!-- 트리그리드 : e -->
		</div>
		<!-- Left Grid Area End     -->
	</div>
	<!-- Left Area End    -->
	
	<!-- Right Area Start -->
	<div class="fl-box panel-wrap03" style="width:50%; height:100%;">
		<!-- Right Title Area Start -->
		<h5 class="panel-tit mgn-l-10" id="approvalDtlNm"></h5>	
		<!-- Right Title Area End   -->
		
		<!-- Right Grid Area Start  -->
		<div class="panel-body mgn-l-10">
			<!-- 트리그리드 : s -->
			<div id="AccountDtlList" style="height:100%">
				<bdo	Debug="Error"
						Layout_Url="/hello/eacc/approval/approvalLineDtlLayout.do"
						Upload_Url="/hello/eacc/approva/approvalLineDtlEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols" Upload_Xml="2">
				</bdo>
			</div>	
			<!-- 트리그리드 : e -->
		</div>
		<!-- Right Grid Area End    -->
	</div>
	<!-- Right Area End   -->	
</div>	

<!-- 결재라인 관리 화면 모달 : s -->
<div class="modal fade" id="apprLineMngModal" data-backdrop="static" data-keyboard="false">
<!-- Layer PopUp Start -->	
	<div class="modal-dialog root wd-per-60"> <!-- 원하는 width 값 입력 -->				
		 <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title"></h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
					 
					 <div class="modal-section">
						<!-- START -->
						<div class="fl-box" style="width:30%;">
							<!-- Left Grid Area Start   -->
							<div id="memberPrivList">
								<bdo	Debug="Error"
										Data_Url=""
										Layout_Url="/mem/division/divisionLayout.do"
										Upload_Url="/mem/division/divisionEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols"				
										>
								</bdo>
							</div>
							<!-- Left Grid Area End     -->
						</div>
						<!-- Left Area End    -->
			
						<!-- Right Area Start -->
						<div class="fl-box" style="width:70%; height:50%;">
			
							<!-- Right Grid Area Start  -->
							<div id="PopMemberDivList" class="mgn-l-10">
								<bdo	Debug="Error"
										Layout_Url="/hello/eacc/common/popMemberDivLayout.do"
										>
								</bdo>
							</div>
							<!-- Right Grid Area End    -->
						</div>
						
						<div class="fl-box" style="width:70%; height:50%;">
			
							<!-- Right Grid Area Start  -->
							<div id="PopApprLineList" class="mgn-l-10">
								<bdo	Debug="Error"
										Layout_Url="/hello/eacc/approval/popApprLineLayout.do"
										>
								</bdo>
							</div>
							<!-- Right Grid Area End    -->
						</div>
						<!-- Right Area End   -->
					
					</div>
				</div>
			</div>	
		</div>		
	</div>
	<!-- 좌측 : e -->
<!-- Layer PopUp End -->		
</div>
<!-- 결재라인 관리 화면 모달 : e -->
</body>
</html>
