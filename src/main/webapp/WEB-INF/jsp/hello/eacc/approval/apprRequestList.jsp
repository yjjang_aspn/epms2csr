<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : apprRequestList.jsp
  Description : 결재 상신 요청 목록
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.09.05    정순주              최초 생성

    author   : 정순주
    since    : 2017.09.05
--%>
<script type="text/javascript">
var gubunDiv = '〔';
var layerGubun = "apprPerson"; // 레이어 팝업 구분자(결재라인 추가-apprPerson)
var apprGrid = "PopApprLineList";
$(document).ready(function(){
	
	// 첨부파일
	var maxFileCnt = 10;
	for(var i=0; i<1; i++){
		$('#attachFile'+i).append('<input type="file" multiple id="ex_file'+i+'" class="ksUpload" name="fileList">');	
		var fileType = 'ppt |pps |pptx |ppsx |pdf |hwp |xls |xlsx |xlsm | xlt |doc |dot |docx |docm |txt |gif |jpg |jpeg |png';
		
		$('#ex_file'+i).MultiFile({
				accept : fileType
			,	list   : '#detailFileList'+i
			,	max    : maxFileCnt
			,	STRING : {
					remove: '<img src="/images/com/web/btn_remove.png" />'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
//						text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" >',
					idx	  : i,
					denied:'$ext는(은) 업로드할 수 없는 파일 확장자입니다.'
				}
		});
		
		$('.MultiFile-wrap').attr("id", "jquery"+i);
	}
	
	// 첨부파일 삭제
	$('.icn_fileDel').on('click', function(){
		delModiFile($(this));
	});
	
	// 결재선 변경 이벤트
	$("#apprlineNdx").on("change", function(){
		if($(this).val() == ''){
			return false;
		}
		
		var companyId = "";
		var line_gb = "";
		var appr_seq = "";
		var appr_id = "";
		
		$.ajax({
			type: 'POST'
			, url: '/hello/eacc/approval/getApprLineItemAjax.do'
			, async : false 
			, dataType: 'json'
			, data : {APPRLINE_NDX : $('#apprlineNdx option:selected').val()}
			, success: function (itemList) {
				if(itemList != '' && itemList != null) {
					var sb = new StringBuilder();
					$('#lineTable').html("");
					
					sb  = '<table class="approval-tb-st type01" id="lineTable">';
					sb += '<tbody>';
					sb += '	<tr>';
					sb += '		<th rowspan="4">회계전표</th>';
					sb += '	</tr>';
					sb += '	<tr>';
					sb += '<td style="background:#dde9f5;">${member.JOB_GRADE_NM}</td>';
					$.each(itemList, function(index, item){
						sb += '<td style="background:#dde9f5;">'+item.JOB_GRADE+'</td>';
						// 기본 결재선 세팅
						if(appr_id == ""){
							companyId = item.COMPANY_ID;
							line_gb = item.LINE_GB;
							appr_seq = item.APPR_SEQ;
							appr_id = item.APPR_ID;
						}else{
							companyId += gubunDiv + item.COMPANY_ID;
							line_gb += gubunDiv + item.LINE_GB;
							appr_seq += gubunDiv + item.APPR_SEQ;
							appr_id += gubunDiv + item.APPR_ID;
						} 
					});
					sb += '	</tr>';
					sb += '	<tr class="stamp">';
					sb += '<td>/</td>';
					$.each(itemList, function(index, item){
						sb += '<td>/</td>';
					});
					sb += '	</tr>';
					sb += '	<tr>';
					sb += '<td>${member.USER_NM}</td>';
					$.each(itemList, function(index, item){
						sb += '<td>'+item.APPR_NM+'</td>';
					});
					sb += '	</tr>';
					sb += '</tbody>';
					sb += '</table>';
					$('#lineTable').html(sb);
				}
				
				// 결재선 파라미터 세팅
				$('#companyIds').val(companyId);
				$('#lineGbs').val(line_gb);
				$('#apprSeqs').val(appr_seq);
				$('#apprIds').val(appr_id);
			}
			, error: function(XMLHttpRequest, textStatus, errorThrown){
				alert("결재선을 가져오는중에 오류가 발생하였습니다.");
			}
		});
	});
	
	/* 증빙보기 버튼 이벤트 */
	$('.evidenceBtn').on('click', function(){
		var billGb = $(this).data('billgb');
		var billNdx = $(this).data('billndx');
		var url = '';
		
		// 전표구분에 따라 url분기
		if(billGb == '1'){
			url = '/hello/eacc/cardBill/getCardEvidenceView.do?cardBillNdx='+billNdx;
			$('#evidenceModal .modal-dialog').removeClass('wd-per-60').addClass('wd-per-20');
			$('#evidenceModal .modal-title').text('법인카드 증빙');
		}else if(billGb == '2'){
			url = '/hello/eacc/taxBill/getTaxEvidenceView.do?TAXBILL_NDX='+billNdx;
			$('#evidenceModal .modal-dialog').removeClass('wd-per-20').addClass('wd-per-60');
			$('#evidenceModal .modal-title').text('세금계산서 증빙');
		}else{
			
		}
		
		url += "&"+new Date().getTime();
		
		$('#evidenceModal .modal-bodyIn').load(url, function(){
			$('#evidenceModal').modal();
		});
		
		Grids.Active = null;
	});
	
	//그리드 row 클릭시 이빈트
	Grids.OnClick = function(grid, row, col) {
		if(row.id == "Filter"     || row.id == "Header"  || row.id == "PAGER" ||
		   row.id == "spanHeader" || row.id == "Toolbar" || col    == "Panel") {
			return;
		}

		if(grid.id == "DivisionList") {
			selDeptRow = row;
			getMemberList(row.DIVISION, row.CUR_LVL, row.COMPANY_ID);
		}
	}
	
	//그리드 드래그 시 결재순서 재정렬
	Grids.OnRowMove = function(grid, row, oldparent, oldnext) {
		if(row.id == "Filter" || row.id == "Header" || row.id == "PAGER" || row.id == "Toolbar"){
			return;
		}
		
		if(grid.id == "PopApprLineList"){
			fn_countApprSeq();
		}
	}

	//그리드 삭제 버튼
	Grids.OnRowDelete = function(grid, row, type){
	  if(grid.id == "PopApprLineList"){
	    if(row.Deleted){
	      Grids.PopApprLineList.RemoveRow(row);
	    }
	    fn_countApprSeq();
	  }
	}

	//그리드 변경 
	Grids.OnAfterValueChanged = function(grid, row, col, type){
		if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
			return; 
		}
		
		if(grid.id == "PopMemberDivList"){
			if(col == "SELECT_YN" && row.SELECT_YN == "1"){
				if(row.USER_ID == "${sessionScope.ssUserId}"){
					alert("본인 선택은 불가합니다.");
					Grids.PopMemberDivList.SetValue(row, "SELECT_YN", "0", 1);
				}
			}
		}
	}
});

/* 상신 */
function fn_apprSubmit(){
	
	if(!isNull_J($('#TITLE'), '문서제목을 입력해주세요.')) {
		return false;
	}
	if(!isNull_J($('#apprlineNdx'), '결재선을 선택해주세요.')) {
		return false;
	}
	
	if(confirm("상신 하시겠습니까?")){
		
		var fileCnt = $("input[name=fileGb]").length;
		$('#fileCnt').val(fileCnt);
		
		if(fileCnt > 0){
			$('#attachExistYn').val("Y");
		}else{
			$('#attachExistYn').val("N");
		}
		
		$('#apprSubmitForm').attr('action','/hello/eacc/approval/apprRequestAjax.do').ajaxSubmit(function(json){
			if(json.resultCd == "T"){
				alert(json.resultMsg);
				reLoadGrid();
				toggleModal($("#apprRequestModal"));
			}else{
				if(typeof json.errorMsg == 'undefined' || json.errorMsg == 'undefined'){
					alert(json.resultMsg);
				}else{
					alert(json.resultMsg + "\n" + json.errorMsg);
				}
				reLoadGrid();
				toggleModal($("#apprRequestModal"));
			}
		});
	}
}

/* 그리드 행 추가 */
function addRowGrid(gridNm) {
	var row = null;
	
	if(gridNm == "ApprovalLineMngList"){
		row = Grids[gridNm].AddRow(null, Grids[gridNm].GetFirst(), true);
		Grids[gridNm].RefreshRow(row);
	}
}

/* 결재라인 상세 설정 팝업*/
var apprLine_Ndx = '';
var apprLine_nm = '';
function fn_changeLine(){
	apprLine_Ndx = $('#apprlineNdx').val();
	apprLine_nm = $('#apprlineNdx option:selected').text();
	var company_id = '${sessionScope.ssCompanyId}';
	var user_id = '${sessionScope.ssUserId}';
	
	if(apprLine_Ndx == ''){
		alert("결재선을 선택해주세요.");
		return;
	}
	
	layer_open('apprLineMngModal');
	var title = apprLine_nm + " 결재선 수정";
	$('#modal_title').text(title);
	fn_getApprLineMng(apprLine_Ndx, company_id, user_id);
}	

/* layerPopUp에 결재라인 설정 상세 화면 출력 */
function fn_getApprLineMng(apprLine_Ndx, company_id, user_id){
	//부서정보 조회
	var divisionUrl = "/mem/division/divisionData.do";
	Grids.DivisionList.Source.Data.Url = divisionUrl;
	Grids.DivisionList.ReloadBody();	
	
	//결재 라인 정보 조회
	var parameters = "?APPRLINE_NDX=" + apprLine_Ndx + "&companyId=" + company_id + "&userId=" + user_id;
	var apprLineUrl = "/hello/eacc/approval/popApprLineData.do" + parameters;
	Grids.PopApprLineList.Source.Data.Url = apprLineUrl;
	Grids.PopApprLineList.ReloadBody();	
}

/* 결재자 순서 재정렬 */
function fn_countApprSeq(){
	var i = 1;
	for(var r =Grids.PopApprLineList.GetFirstVisible(); r; r=Grids.PopApprLineList.GetNextVisible(r)){
		Grids.PopApprLineList.SetValue(r, "APPR_SEQ", i, 1);
		i++;
	}
} 

/* 결재자 수정 */
function fn_saveApprLine(gridNm) {
	
	var companyId = "";
	var line_gb = "";
	var appr_seq = "";
	var appr_id = "";
	var apprLine = "";
	var msg = "";
	var isAble = false;
	if(apprLine_Ndx == null || apprLine_Ndx == ""){
		alert("변경할 결재선이 존재하지 않습니다.");
		return;
	}
	if(confirm(apprLine_nm + "의 결재선을 변경하시겠습니까?")){
		
		var sb = new StringBuilder();
		$('#lineTable').html("");
		
		sb  = '<table class="approval-tb-st type01" id="lineTable">';
		sb += '<tbody>';
		sb += '	<tr>';
		sb += '		<th rowspan="4">회계전표</th>';
		sb += '	</tr>';
		sb += '	<tr>';
		sb += '<td style="background:#dde9f5;">${member.JOB_GRADE_NM}</td>';
		for(var r =Grids.PopApprLineList.GetFirstVisible(); r; r=Grids.PopApprLineList.GetNextVisible(r)){
			//결재 구분 선택 안되었을시 처리
			if(r.LINE_GB == null || r.LINE_GB == ""){
				if(msg == ""){
					msg = r.APPR_NM;
				}else{
					msg += "," + r.APPR_NM;
				}
				isAble = true;
			}
			//선택된 결재자 정보 세팅
			if(appr_id == ""){
				companyId = r.COMPANY_ID;
				line_gb = r.LINE_GB;
				appr_seq = r.APPR_SEQ;
				appr_id = r.APPR_ID;
			}else{
				companyId +=  gubunDiv + r.COMPANY_ID;
				line_gb +=  gubunDiv + r.LINE_GB;
				appr_seq +=  gubunDiv + r.APPR_SEQ;
				appr_id +=  gubunDiv + r.APPR_ID;
			} 

			sb += '<td style="background:#dde9f5;">'+r.JOB_GRADE+'</td>';
		}
		
		sb += '	</tr>';
		sb += '	<tr class="stamp">';
		sb += '<td>/</td>';
		
		for(var r =Grids.PopApprLineList.GetFirstVisible(); r; r=Grids.PopApprLineList.GetNextVisible(r)){
			sb += '<td>/</td>';
		}
		
		sb += '	</tr>';
		sb += '	<tr>';
		sb += '<td>${member.USER_NM}</td>';
		
		for(var r =Grids.PopApprLineList.GetFirstVisible(); r; r=Grids.PopApprLineList.GetNextVisible(r)){
			sb += '<td>'+r.APPR_NM+'</td>';
		}
		
		sb += '	</tr>';
		sb += '</tbody>';
		sb += '</table>';
		$('#lineTable').html(sb);
		
		if(isAble){
			alert(msg + "님의 결재 구분을 선택해주시기 바랍니다.");
			return;
		}
		
		// 결재선 정보 세팅
		$('#companyIds').val(companyId);
		$('#lineGbs').val(line_gb);
		$('#apprSeqs').val(appr_seq);
		$('#apprIds').val(appr_id);
		
		layer_open('apprLineMngModal');
	}
}

</script>

	<div class="modal-dialog root wd-per-80"> <!-- 원하는 width 값 입력 -->
        <div class="modal-content">
			<div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
               	<h4 class="modal-title" style="display:inline-block">결재요청 목록</h4>
               	
               	<div style="position:absolute; top:13px; right:40px;">
               		<a href="#" class="btn comm st01 f-r" style="margin-right: 5px; margin-bottom: 5px;" onclick="fn_apprSubmit(); return false;">상신</a>
               	</div>
            </div>
            <div class="modal-body" >
				<div class="modal-bodyIn" >
				
					<form id="apprSubmitForm" action="" method="post" enctype="multipart/form-data">
						<!-- input : s -->
						<input type="hidden" name="billGbs" value="${approvalVO.billGbs }" />
						<input type="hidden" name="billNdxs" value="${approvalVO.billNdxs }" />
						<input type="hidden" name="erpbills" value="${approvalVO.erpbills }" />
						<input type="hidden" name="REG_ID" value="${sessionScope.ssUserId}" />
						
						<!-- 첨부파일 정보 -->
						<input type="hidden" title="파일등록여부" id="attachExistYn" name="attachExistYn" value=""/>
						<input type="hidden" title="삭제Seq" id="delSeq" name="delSeq" value=""/>
						<input type="hidden" title="파일 건수" id="fileCnt" name="fileCnt" value=""/>
						<input type="hidden" title="경비 모듈" id="MODULE" name="MODULE" value="8">
						
						<!-- 결재선 정보 -->
						<input type="hidden" id="companyIds" name="companyIds" value="" />
						<input type="hidden" id="lineGbs" name="lineGbs" value="" />
						<input type="hidden" id="apprSeqs" name="apprSeqs" value="" />
						<input type="hidden" id="apprIds" name="apprIds" value="" />
						<!-- input : e -->
						
						<!-- s:결재직인 -->
						<div class="approval-tb-wrap">
							<table class="approval-tb-st type01" id="lineTable">
								<tbody>
									<tr>
										<th rowspan="4">회계전표</th>
									</tr>
									<tr>
										<td style="background:#dde9f5;">${member.JOB_GRADE_NM}</td>
									</tr>
									<tr class="stamp">
										<td>/</td>
									</tr>
									<tr>
										<td>${member.USER_NM }</td>
									</tr>
								</tbody>
							</table>
						</div>
						
						<!-- s:문서정보 -->
						<div class="approval-tb-wrap mgn-t-10">
							<table class="approval-tb-st type02" >
								<tbody>
									<tr>
										<th scope="row">문서번호</th>
										<td></td>
										<th scope="row">기안부서</th>
										<td>${sessionScope.ssDivisionNm}</td>
										<th scope="row">기안자</th>
										<td>${sessionScope.ssUserNm}</td>
										<th scope="row">기안일자</th>
										<td>${fn:substring(toDate,0,4)}-${fn:substring(toDate,4,6)}-${fn:substring(toDate,6,8)}</td>
									</tr>
									<tr>
										<th scope="row">문서제목</th>
										<td colspan="7">
											<div class="inp-wrap" style="width:100%">
												<input type="text" class="inpTxt ar" title="문서제목" id="TITLE" name="TITLE" placeholder="문서제목" />
											</div>
										</td>
									</tr>
									<tr>
										<th scope="row">결재선</th>
										<td colspan="7">
											<div class="sel-wrap f-l" style="width:150px;"> 
												<select title="결재라인" id="apprlineNdx" name="apprlineNdx">
													<option value="">선택</option>
													<c:forEach var="lineHeader" items="${lineHeaderList}">
														<option value="${lineHeader.APPRLINE_NDX}">${lineHeader.APPRLINE_NM}</option>
													</c:forEach>
												</select>
											</div>
											<a href="#" class="btn evn-st01 type02 mgn-l-5"  onclick="fn_changeLine(); return false;">결재선 수정</a>
										</td>
									</tr>
									<tr>
										<th scope="row">첨부파일</th>
										<td colspan="7">
											<div class="bxType01 fileWrap">
												<div class="filebox" id="attachFile0"></div>
												<div class="fileList" style="padding:7px 0 5px;">
													<div id="detailFileList0" class="fileDownLst">
														
													</div>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						
						<!-- s:문서정보 -->
						<div class="approval-tb-wrap mgn-t-20">
							<h4>· 지출내역 (${headerList.size() }건)</h4>
							<table class="approval-tb-st type02" >
								<colgroup>
									<col width="7%">
									<col width="8%">
									<col width="7%">
									<col width="10%">
									<col width="15%">
									<col width="10%">
									<col width="24%">
									<col width="7%">
									<col width="7%">
								</colgroup>
								<tbody>
									<tr>
										<th class="t-c">전표번호</th>
										<th class="t-c">전기일</th>
										<th class="t-c">세금코드</th>
										<th class="t-c">추가직원</th>
										<th class="t-c">계정과목</th>
										<th class="t-c">코스트센터</th>
										<th class="t-c">적요</th>
										<th class="t-c">차변</th>
										<th class="t-c">대변</th>
									</tr>
									<c:set var="totTotAmt" value="0" />
									<c:set var="totItemAmt" value="0" /> 	
									<c:set var="totItemVat" value="0" /> 	
									<c:forEach var="billHeader" items="${headerList}" varStatus="status">
										<c:set var="totTotAmt" value="${totTotAmt + billHeader.TOT_AMOUNT }" />
										<c:forEach var="items" items="${itemList[status.index] }" varStatus="status2">
											<c:set var="rowspan" value="${itemList[status.index].size()+1 }"></c:set>
												<c:if test="${status2.index == 0 }">
													<c:forEach var="item" items="${itemList[status.index] }" varStatus="status2">
														<c:if test="${item.ITEM_VAT > 0 }">
															<c:set var="rowspan" value="${rowspan+1 }"></c:set>
														</c:if>
													</c:forEach>
													<tr>
														<td rowspan="${rowspan }" class="t-c">
															<c:choose>
																<c:when test="${billHeader.ATTACH_GRP_NO eq null || billHeader.ATTACH_GRP_NO == ''}">
																	${billHeader.ERPBILL }
																</c:when>
																<c:otherwise>
																	<a href="javascript:fn_openFileDownModal('${billHeader.ATTACH_GRP_NO }');" style="text-decoration: underline;">${billHeader.ERPBILL }</a>
																</c:otherwise>
															</c:choose>
															<c:if test="${billHeader.BILL_GB eq '1' or billHeader.BILL_GB eq '2' }">
																<br><a href="#none" class="numFileView evidenceBtn" data-billgb="${billHeader.BILL_GB }" data-billndx="${billHeader.BILL_NDX }">증빙</a>
															</c:if>
														</td>
														<td rowspan="${rowspan }" class="t-c">${billHeader.BOOK_DT }</td>
														<td rowspan="${rowspan }" class="t-c">${billHeader.TAX_NM }</td>
														<td rowspan="${rowspan }" class="t-l">${billHeader.ADD_PRSN_NM }</td>
														<td>[미지급]경비</td>
														<td>${billHeader.PURCHASEOFFI_NM }</td>
														<td></td>
														<td class="t-r"></td>
														<td class="t-r"><fmt:formatNumber type="number" value="${billHeader.TOT_AMOUNT }"/></td>
													</tr>
												</c:if>
											<c:set var="totItemAmt" value="${totItemAmt + items.ITEM_AMT }" />
											<tr>
												<td class="t-l">${items.GLNAME }</td>
												<td class="t-l">${items.KTEXT }</td>
												<td class="t-l">${items.SUMMARY }</td>
												<td class="t-r"><fmt:formatNumber type="number" value="${items.ITEM_AMT }"/>
													<c:set var="totItemVat" value="${totItemVat + items.ITEM_VAT }" />
												</td>
												<td class="t-r"></td>
											</tr>
											<c:if test="${items.ITEM_VAT > 0}">
												<tr>
													<td>부가세대급금</td>
													<td></td>
													<td>${items.SUMMARY }</td>
													<td class="t-r">
														<fmt:formatNumber type="number" value="${items.ITEM_VAT }"/>
													</td>
													<td class="t-r"></td>
												</tr>
											</c:if>
										</c:forEach>
									</c:forEach>
									
									<!-- 합계 -->
									<tr>
										<th colspan="7" class="t-c">합계</th>
										<td class="t-r"><fmt:formatNumber type="number" value="${totItemAmt+totItemVat }"/></td>
										<td class="t-r"><fmt:formatNumber type="number" value="${totTotAmt }"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</form>
				</div>
            </div>
        </div>
    </div>

