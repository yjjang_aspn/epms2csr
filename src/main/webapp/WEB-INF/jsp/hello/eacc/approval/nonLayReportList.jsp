<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : nonLayReportList.jsp
  Description : 미상신 전표 목록
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.08.30    정호윤              최초 생성

    author   : 정호윤
    since    : 2017.08.30
--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<link rel="stylesheet" href="/css/eacc/modalPopup.css">
<script src="/js/eacc/eaccCommon.js" type="text/javascript" ></script>
<script src="/js/com/web/modalPopup.js"></script>
<script src="/js/jquery/jquery-filestyle.js" type="text/javascript" ></script>
<script src="/js/jquery/jquery.MultiFile.js" type="text/javascript"></script>
<script src="/js/jquery/jquery.ajaxsubmit.js" type="text/javascript"></script>
<script type="text/javascript">
var gubunDiv = '〔';
$(document).ready(function(){
	
	/* 데이터 URL */
	var parameters = "startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val();
	var url = "/hello/eacc/approval/getNonLayReportListData.do?" + parameters;
	$("#NonLayReportList").find('bdo').attr("Data_Url", url);
	
	$('#searchVal').keydown(function(key){
		<%-- 전표번호 검색 Enter Key Down Event --%>
		if(key.keyCode == 13){
			fn_searchData();
		}
	});
	
	Grids.OnDataReceive = function(grid,source){
		if(grid.id == "NonLayReportList"){
			for(var row=grid.GetFirst(null);row;row=grid.GetNext(row)){
				if(row.ATTACH_GRP_NO){
					grid.SetAttribute(row, "ATTACH_GRP_NO", "Switch","1",1);
					grid.SetAttribute(row, "ATTACH_GRP_NO", "Icon", "/images/com/web/download.gif",1);
					grid.SetAttribute(row, "ATTACH_GRP_NO", "OnClickSideIcon", "fn_openFileDownModal('"+row.ATTACH_GRP_NO+"');", 1);
				}
			}
		}
	}
	
	Grids.OnDblClick = function(grid, row, col){
		if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
			return;
		}
		
		<%-- 그리드 row 더블클릭시 팝업 호출 --%>
		if(grid.id == "NonLayReportList"){
			var url = "";
			<%-- 전표 상세 팝업 호출 --%>
			if(row.BILL_GB == "1"){
				url = "/hello/eacc/cardBill/cardBillDtl.do?cardNdx=" + row.CARD_NDX + "&cardNum=" + row.CARD_NO + "&authNum=" + row.CARD_APPRNO + "&cardBillNdx=" + row.BILL_NDX + "&page=view";
			}else if(row.BILL_GB == "2"){
				url = "/hello/eacc/taxBill/taxBillDtl.do?TAXBILL_NDX=" + row.BILL_NDX + "&page=view";
			}else if(row.BILL_GB == "3"){
				url = "/hello/eacc/etcBill/etcBillDtl.do?etcBillNdx=" + row.BILL_NDX + "&page=view";
			}
			
			$('#billDtlModal').load(url, function(){
				$(this).modal({
				});
			});
			
			Grids.Active = null;
		}
	}
	
});

/* 날짜 검색 */
function fn_searchData(){
	var parameters = "startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val() + "&billGb=" + $("#searchKey").val() + "&searchVal=" + $("#searchVal").val();
	Grids.NonLayReportList.Source.Data.Url = "/hello/eacc/approval/getNonLayReportListData.do?" + parameters;
	Grids.NonLayReportList.ReloadBody();
}

function fn_apprRequest(grid){
	
	var billGbs = '';
	var billNdxs = '';
	var erpbills = '';
	var selRow = Grids.NonLayReportList.GetSelRows();
	
	if(selRow.length == 0){
		alert("상신할 전표를 선택하세요.");
		return;
	}else{
		
		if(selRow.length > 20){
			alert('최대 20건까지 묶음 상신 가능합니다.');
			return;
		}
		
		for(var i = 0 ; i < selRow.length ; i++) {
			if(billNdxs == ""){
				billGbs = selRow[i].BILL_GB;
				billNdxs = selRow[i].BILL_NDX;
				erpbills = selRow[i].ERPBILL;
			}else{
				billGbs += gubunDiv + selRow[i].BILL_GB;
				billNdxs += gubunDiv + selRow[i].BILL_NDX;
				erpbills += gubunDiv + selRow[i].ERPBILL;
			}
		}
		
		var url = "/hello/eacc/approval/apprRequestList.do?billGbs="+billGbs+"&billNdxs="+billNdxs+"&erpbills="+erpbills;
		url += "&"+new Date().getTime();
		
		// 전표 상신 모달 팝업 로드
		$("#apprRequestModal").load(url, function(){
			$(this).modal();
		});
		
		Grids.Active = null;
	}
}

//그리드 리로드
function reLoadGrid(){
	Grids.NonLayReportList.ReloadBody();	
}

// 모달 토글
function toggleModal(obj){
	obj.modal('toggle');
}

/* 결재라인 상세 설정 팝업*/
function fn_popApprLineList(){
	 
	if(apprLine_Ndx == ''){
		alert("결재라인을 선택해주세요.");
		return;
	}
	
	layer_open('apprLineMngModal');
	var title = apprLine_nm + " 결재선 수정";
	$('#modal_title').text(title);
	fn_getApprLineMng(apprLine_Ndx, company_id, user_id);
}	

/* layerPopUp에 결재라인 설정 상세 화면 출력 */
function fn_getApprLineMng(apprLine_Ndx, company_id, user_id){
	//부서정보 조회
	var divisionUrl = "/mem/division/divisionData.do";
	Grids.DivisionList.Source.Data.Url = divisionUrl;
	Grids.DivisionList.ReloadBody();	
	
	//결재 라인 정보 조회
	var parameters = "?APPRLINE_NDX=" + apprLine_Ndx + "&companyId=" + company_id + "&userId=" + user_id;
	var apprLineUrl = "/hello/eacc/approval/popApprLineData.do" + parameters;
	Grids.PopApprLineList.Source.Data.Url = apprLineUrl;
	Grids.PopApprLineList.ReloadBody();	
}

/* 결재자 순서 재정렬 */
function fn_countApprSeq(){
	var i = 1;
	for(var r =Grids.PopApprLineList.GetFirstVisible(); r; r=Grids.PopApprLineList.GetNextVisible(r)){
		Grids.PopApprLineList.SetValue(r, "APPR_SEQ", i, 1);
		i++;
	}
} 

/* 결재자 수정/저장 */
function fn_saveApprLine(gridNm) {
	
	var companyId = "";
	var line_gb = "";
	var appr_seq = "";
	var appr_id = "";
	var apprLine = "";
	var msg = "";
	var isAble = false;
	if(apprLine_Ndx == null || apprLine_Ndx == ""){
		alert("변경할 결재선이 존재하지 않습니다.");
		return;
	}
	if(confirm(apprLine_nm + "의 결재선을 변경하시겠습니까?")){
		for(var r =Grids.PopApprLineList.GetFirstVisible(); r; r=Grids.PopApprLineList.GetNextVisible(r)){
			//결재 구분 선택 안되었을시 처리
			if(r.LINE_GB == null || r.LINE_GB == ""){
				if(msg == ""){
					msg = r.APPR_NM;
				}else{
					msg += "," + r.APPR_NM;
				}
				isAble = true;
			}
			//선택된 결재자 정보 세팅
			if(appr_id == ""){
				companyId = r.COMPANY_ID;
				line_gb = r.LINE_GB;
				appr_seq = r.APPR_SEQ;
				appr_id = r.APPR_ID;
			}else{
				companyId +=  gubunDiv + r.COMPANY_ID;
				line_gb +=  gubunDiv + r.LINE_GB;
				appr_seq +=  gubunDiv + r.APPR_SEQ;
				appr_id +=  gubunDiv + r.APPR_ID;
			} 
		}
		
		if(isAble){
			alert(msg + "님의 결재 구분을 선택해주시기 바랍니다.");
			return;
		}
		
		jQuery.ajax({
    		type : 'POST',
    		url : '/hello/eacc/approval/editApprLineAjax.do',
    		cache : false,
    		dataType : 'json',
    		data : {
    				APPRLINE_NDX : apprLine_Ndx,
    				COMPANY_ID : companyId,
    				LINE_GB : line_gb,
    				APPR_SEQ : appr_seq,
    				APPR_ID : appr_id
    				},
			async : false,
    		error : function() {
    			alert("처리중 오류가 발생 하였습니다.");
				Grids.PopApprLineList.ReloadBody();
				Grids.ApprovalLineDtlLayout.ReloadBody();
				layer_open('apprLineMngModal');
			},
    		success : function(json) {
    			if(json.resultCd == "T"){
    				alert(json.resultMsg);
    				Grids.PopApprLineList.ReloadBody();
    				Grids.ApprovalLineDtlLayout.ReloadBody();
    				layer_open('apprLineMngModal');
    			}else{
    				if(typeof json.errorMsg == 'undefined' || json.errorMsg == 'undefined'){
    					alert(json.resultMsg);
        				Grids.PopApprLineList.ReloadBody();
        				Grids.ApprovalLineDtlLayout.ReloadBody();
        				layer_open('apprLineMngModal');
    				}else{
    					alert(json.resultMsg + "\n" + json.errorMsg);
        				Grids.PopApprLineList.ReloadBody();
        				Grids.ApprovalLineDtlLayout.ReloadBody();
        				layer_open('apprLineMngModal');
    				}
    			}
			}
    	});
	}
}

</script>
</head>
<body>


<!-- 페이지 내용 : s -->

<!-- s:input -->
<!-- e:input -->

<div id="contents">
	<!-- 페이지 내용 : s -->
	<!-- s:panel-wrap01 -->
	<div class="fl-box panel-wrap03" style="width:100%; height:100%;">
		<h4 class="panel-tit mgn-l-10">결재 상신</h4>
		
		<!-- inq-area-inner : s -->
		<div class="inq-area-inner type06 ab">
			<ul class="wrap-inq">
				<li class="inq-clmn">
					<h4 class="tit-inq">전기일자</h4>
					<div class="prd-inp-wrap">
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic openCal thisMonth" readonly="readonly" gldp-id="gldp-7636599364" id="startDt"/>
							</span>
						</span>
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic openCal" readonly="readonly" gldp-id="gldp-8530641166" id="endDt"/>
							</span>
						</span>
					</div>
				</li>
				<li class="inq-clmn">
					<h4 class="tit-inq">전표번호</h4>
					<div class="sel-wrap type02">
						<tag:combo codeGrp="BILL_GB" name="searchKey" choose="Y" />
					</div>
				</li>
				<li>
					<div class="prd-inp-wrap">
						<input type="text" class="inp-wrap type03" id="searchVal" name="searchVal" value=""/>
					</div>
				</li>
			</ul>
			<a href="#" class="btn comm st01" onclick="fn_searchData(); return false;">검색</a>
		</div>
		<!-- inq-area-inner : e -->
		
		<div class="panel-body mgn-t-25">
			<!-- 트리그리드 : s -->
			<div id="NonLayReportList" style="height:100%">
				<bdo Debug="Error"
					 Layout_Url="/hello/eacc/approval/nonLayReportLayout.do"
				>
				</bdo>
			</div>
			<!-- 트리그리드 : e -->
		</div>
	</div>
	<!-- e:panel-wrap01 -->
</div>

<!-- Layer PopUp Start -->
<!-- 전표 상신화면 모달 시작-->
<div class="modal fade" id="apprRequestModal" data-backdrop="static" data-keyboard="false"></div>
<!-- 전표 상신화면 모달 끝-->

<!-- 전표 상세 화면 모달 시작-->
<div class="modal fade" id="billDtlModal" data-backdrop="static" data-keyboard="false"></div>
<!-- 전표 상세 화면 모달 끝-->

<!-- 그리드 첨부파일 다운로드 모달 -->
<div class="modal fade" id="fileDownModal" data-backdrop="static" data-keyboard="false"></div>
<!-- 그리드 첨부파일 다운로드 모달 -->

<!-- 매입증빙 모달 -->
<div class="modal fade" id="evidenceModal" data-backdrop="static">
	<div class="modal-dialog root"> <!-- 원하는 width 값 입력 -->
		<div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
	            <h4 class="modal-title">증빙보기</h4>
			</div>
	        <div class="modal-body" >
				<div class="modal-bodyIn" >
				
				</div>
            </div>
        </div>
    </div>
</div>
<!-- 매입증빙 모달 -->
<!-- Layer PopUp End -->

<!-- 결재라인 관리 화면 모달 : s -->
<div class="modal fade" id="apprLineMngModal" data-backdrop="static" data-keyboard="false">
<!-- Layer PopUp Start -->	
	<div class="modal-dialog root wd-per-60"> <!-- 원하는 width 값 입력 -->				
		 <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title"></h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
					 
					 <div class="modal-section">
						<!-- START -->
						<div class="fl-box" style="width:30%;">
							<!-- Left Grid Area Start   -->
							<div id="memberPrivList">
								<bdo	Debug="Error"
										Data_Url=""
										Layout_Url="/mem/division/divisionLayout.do"
										Upload_Url="/mem/division/divisionEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols"				
										>
								</bdo>
							</div>
							<!-- Left Grid Area End     -->
						</div>
						<!-- Left Area End    -->
			
						<!-- Right Area Start -->
						<div class="fl-box" style="width:70%; height:50%;">
			
							<!-- Right Grid Area Start  -->
							<div id="PopMemberDivList" class="mgn-l-10">
								<bdo	Debug="Error"
										Layout_Url="/hello/eacc/common/popMemberDivLayout.do"
										>
								</bdo>
							</div>
							<!-- Right Grid Area End    -->
						</div>
						
						<div class="fl-box" style="width:70%; height:50%;">
			
							<!-- Right Grid Area Start  -->
							<div id="PopApprLineList" class="mgn-l-10">
								<bdo	Debug="Error"
										Layout_Url="/hello/eacc/approval/popApprLineLayout.do"
										>
								</bdo>
							</div>
							<!-- Right Grid Area End    -->
						</div>
						<!-- Right Area End   -->
					
					</div>
				</div>
			</div>	
		</div>		
	</div>
	<!-- 좌측 : e -->
<!-- Layer PopUp End -->		
</div>
<!-- 결재라인 관리 화면 모달 : e -->

<!-- 화면 UI Area End    -->
</body>
</html>