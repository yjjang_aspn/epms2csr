<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : waitApprovalListLayout.jsp
  Description : 내가 받은 결재 그리드 레이아웃 화면
  Modification Information
 
       수정일    	     수정자                   수정내용
   -------    --------    ---------------------------
   2017.09.11  정순주	              최초 생성

    author   : 정순주
    since    : 2017.09.11
--%>
<c:set var="gridId" value="waitApprovalList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			AcceptEnters   = "1" Calculated = "1"  CalculateSelected = "1"      DateStrings = "2" Deleting  = "0"
			Dragging       = "0" Editing    = "1"  EnterMode         = "4"      Filtering   = "1" IdChars   = "0123456789"
			InEditMode     = "2" MaxPages   = "20" NoFormatEscape    = "1"      NoHScroll   = "0" NoVScroll = "0"
			NumberId       = "1" SafeCSS    = '1'  Selecting		 = "1"
			SelectingCells = "0" SortIcons  = "0"  Style             = "Office" SuppressCfg = "1" 
			Paging		   = "2" AllPages	= "0"  PageLength		 = "24"		MaxPages	= "20"
			CopySelected   = "0" CopyFocused= "1"  CopyCols			 = "0"		ExportFormat="xls" ExportCols="0"
	/>
	
	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>
	
	<!-- Grid Header Setting Area Start  -->
	<Header	id="Header"	Align="center"
			COMPANY_ID		= "회사코드"
			APPR_NDX        = "전자결재 인덱스"
			APPR_KIND       = "결재종류"
			TITLE           = "제목"
			GW_DOC          = "문서번호"
			APPR_H_STS      = "문서결재상태"
			APPR_I_STS      = "결재상태"
			REG_ID          = "기안자ID"
			REG_USERNM      = "기안자"
			REG_DT          = "기안일시"
			LAST_USERID     = "최종결재자"
			UPT_USERID      = "최근결재자"
			UPT_DT          = "최근결재일시"
			DEL_YN          = "상신취소여부"
			DEL_USERID      = "삭제자"
			DEL_DT          = "삭제일시"
			MANAGER_YN      = "관리자 여부"
			CANCEL_MEMO     = "승인취소 메모"
     />
	<!-- Grid Header Setting Area End    -->
	
	<!-- Fileter Setting Area Start      -->
	<Head>
		<Filter	id = "Filter" CanFocus = "1"/>
	</Head>
	<!-- Fileter Setting Area End        -->
	
	<Cols>
		<C Name="COMPANY_ID"	Type="Text" RelWidth="100" Align="center" CanExport="0" CanEdit="0" Visible="0" />		
		<C Name="APPR_NDX"      Type="Int" 	RelWidth="100" Align="center" CanExport="0" CanEdit="0" Visible="0" />
		<C Name="APPR_KIND"     Type="Text" RelWidth="100" Align="center" CanExport="1" CanEdit="0" Visible="0" />
		<C Name="GW_DOC"        Type="Text" RelWidth="100" Align="center" CanExport="1" CanEdit="0" Visible="1" />
		<C Name="REG_ID"        Type="Text" RelWidth="100" Align="center" CanExport="1" CanEdit="0" Visible="0" />
		<C Name="REG_USERNM"    Type="Text" RelWidth="100" Align="center" CanExport="1" CanEdit="0" Visible="1" />
		<C Name="TITLE"         Type="Text" RelWidth="500" Align="left"   CanExport="1" CanEdit="0" Visible="1" />
		<C Name="APPR_H_STS"    Type="Text" RelWidth="100" Align="center" CanExport="1" CanEdit="0" Visible="0" />
		<C Name="APPR_I_STS"    Type="Enum" RelWidth="100" Align="center" CanExport="1" CanEdit="0" Visible="1"
								EmptyValue="선택" <tag:enum codeGrp="APPR_I_STS" /> />
		<C Name="REG_DT"        Type="Date" RelWidth="100" Align="center" CanExport="1" CanEdit="0" Visible="1" Format="yyyy-MM-dd HH:MM:ss" />
		<C Name="LAST_USERID"   Type="Text" Width="100" Align="center" CanExport="0" CanEdit="0" Visible="0" />
		<C Name="UPT_USERID"    Type="Text" Width="100" Align="center" CanExport="1" CanEdit="0" Visible="0" />
		<C Name="UPT_DT"        Type="Date" Width="100" Align="center" CanExport="1" CanEdit="0" Visible="0" Format="yyyy-MM-dd HH:MM:ss" />
		<C Name="DEL_YN"        Type="Text" Width="100" Align="center" CanExport="1" CanEdit="0" Visible="0" />
		<C Name="DEL_USERID"    Type="Text" Width="100" Align="center" CanExport="1" CanEdit="0" Visible="0" />
		<C Name="DEL_DT"        Type="Date" Width="100" Align="center" CanExport="1" CanEdit="0" Visible="0" Format="yyyy-MM-dd HH:MM:ss" />
		<C Name="MANAGER_YN"    Type="Text" Width="100" Align="center" CanExport="0" CanEdit="0" Visible="0" />
		<C Name="CANCEL_MEMO"   Type="Text" Width="100" Align="center" CanExport="1" CanEdit="0" Visible="0" />
	
	</Cols>
	
	<!-- Grid Paging Setting Area Start  -->
	<Pager Visible = "0"/>

	<Solid>
		<I id = "PAGER" Cells = "NAV,LIST,ONE,GROUP" Space = "4"
		   NAVType	     = "Pager"
		   LISTType	     = "Pages" LISTAlign   = "left" LISTLeft   = "10"                LISTRelWidth  = "1"
		   ONEType       = "Bool"  ONECanEdit  = "1"    ONEFormula = "Grid.AllPages?0:1" ONELabelRight = "페이지단위로보임"
		   ONEOnChange = "Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
		   GROUPCanFocus = "0"
		/>
	</Solid>
	<!-- Grid Paging Setting Area End    -->
	
	<Toolbar	Space="0"	Styles="1"	Cells="Empty,Cnt,Found,상신취소,항목선택,새로고침,인쇄,엑셀"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType = "Button"
				CntType = "Html" CntFormula = '"총 : &lt;b>"+count(7)+"&lt;/b>행"' CntWidth = '-1' CntWrap = '0'
				FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>행'" FoundWidth = '-1' FoundWrap = '0'
				상신취소Type ="Html" 상신취소="&lt;a href='#none' title='상신취소' class=&quot;treeReport ir-pm&quot;
			 						 onclick='fn_apprCancel(&quot;${gridId}&quot;)'>상신취소&lt;/a>"
				새로고침Type = "Html" 새로고침 = "&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
									  onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
 				인쇄Type = "Html" 인쇄 = "&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
									  onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
 				엑셀Type = "Html" 엑셀 = "&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
									  onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
				항목선택Type="Html" 항목선택="&lt;a href='#none' title='항목선택' class=&quot;treeSelect ir-pm	&quot;
			  						  onclick='showColumns(&quot;${gridId}&quot;,&quot;xls&quot;)'>항목선택&lt;/a>"
	/>

</Grid>

