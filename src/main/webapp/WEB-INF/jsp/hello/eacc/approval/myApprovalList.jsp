<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : myApprovalList.jsp
  Description : 내가 올린 결재 목록
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.09.06  정순주              최초 생성

    author   : 정순주
    since    : 2017.09.06
--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<link rel="stylesheet" href="/css/eacc/modalPopup.css">
<script src="/js/eacc/eaccCommon.js" type="text/javascript" ></script>
<script src="/js/com/web/modalPopup.js"></script>
<script src="/js/jquery/jquery-filestyle.js" type="text/javascript" ></script>
<script src="/js/jquery/jquery.MultiFile.js" type="text/javascript"></script>
<script src="/js/jquery/jquery.ajaxsubmit.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){
	
	Grids.OnDblClick = function(grid, row, col){
		if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
			return;
		}
		
		<%-- 그리드 row 더블클릭시 팝업 호출 --%>
		if(grid.id == "myApprovalList"){
			
			var url = "/hello/eacc/approval/myApprovalDtl.do?APPR_NDX="+row.APPR_NDX;
			url += "&"+new Date().getTime();
			
			$('#myApprovalDtlModal').load(url, function(responseTxt, statusTxt, xhr){
				$(this).modal({});
			});
			
			Grids.Active = null;
		}
	}
});

/* 날짜 검색 */
function fn_searchData(){
	var parameters = "startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val();
	Grids.myApprovalList.Source.Data.Url = "/hello/eacc/approval/myApprovalListData.do?" + parameters;
	Grids.myApprovalList.ReloadBody();
}

//그리드 리로드
function reLoadGrid(){
	Grids.myApprovalList.ReloadBody();	
}

//모달 토글
function toggleModal(obj){
	obj.modal('toggle');
}


</script>
</head>
<body>

<!-- 페이지 내용 : s -->

<!-- s:input -->
<!-- e:input -->

<div id="contents">
	<!-- 페이지 내용 : s -->
	<!-- s:panel-wrap01 -->
	<div class="fl-box panel-wrap03" style="width:100%; height:100%;">
		<h4 class="panel-tit mgn-l-10">결재 대기</h4>
		
		<!-- inq-area-inner : s -->
		<div class="inq-area-inner type06 ab">
			<ul class="wrap-inq">
				<li class="inq-clmn">
					<h4 class="tit-inq">기안일자</h4>
					<div class="prd-inp-wrap">
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic openCal thisMonth" readonly="readonly" gldp-id="gldp-7636599364" id="startDt"/>
							</span>
						</span>
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic openCal" readonly="readonly" gldp-id="gldp-8530641166" id="endDt" />
							</span>
						</span>
					</div>
				</li>
			</ul>
			<a href="#" class="btn comm st01" onclick="fn_searchData(); return false;">검색</a>
		</div>
		<!-- inq-area-inner : e -->
		
		<div class="panel-body mgn-t-30">
			<!-- 트리그리드 : s -->
			<div id="myApprovalList" style="height:100%">
				<bdo Debug="Error"
					 Data_Url="/hello/eacc/approval/myApprovalListData.do"
					 Layout_Url="/hello/eacc/approval/myApprovalListLayout.do"
				>
				</bdo>
			</div>
			<!-- 트리그리드 : e -->
		</div>
	</div>
	<!-- e:panel-wrap01 -->
</div>

<!-- 전표 상신화면 모달 시작-->
<div class="modal fade" id="myApprovalDtlModal" data-backdrop="static" data-keyboard="false"></div>
<!-- 전표 상신화면 모달 끝-->

<!-- 그리드 첨부파일 다운로드 모달 -->
<div class="modal fade" id="fileDownModal" data-backdrop="static" data-keyboard="false"></div>
<!-- 그리드 첨부파일 다운로드 모달 -->

<!-- 매입증빙 모달 -->
<div class="modal fade" id="evidenceModal" data-backdrop="static">
	<div class="modal-dialog root"> <!-- 원하는 width 값 입력 -->
		<div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
	            <h4 class="modal-title">증빙보기</h4>
			</div>
	        <div class="modal-body" >
				<div class="modal-bodyIn" >
				
				</div>
            </div>
        </div>
    </div>
</div>
<!-- 매입증빙 모달 -->
<!-- 화면 UI Area End    -->
</body>
</html>