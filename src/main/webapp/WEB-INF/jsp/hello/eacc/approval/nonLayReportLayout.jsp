<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : nonLayReportLayout.jsp
  Description : 미상신 전표 그리드 레이아웃 화면
  Modification Information
 
       수정일    	     수정자                   수정내용
   -------    --------    ---------------------------
   2017.08.30  정호윤	              최초 생성

    author   : 정호윤
    since    : 2017.08.30
--%>
<c:set var="gridId" value="NonLayReportList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			AcceptEnters   = "1" Calculated = "1"  CalculateSelected = "1"      DateStrings = "2" Deleting  = "0"
			Dragging       = "0" Editing    = "1"  EnterMode         = "4"      Filtering   = "1" IdChars   = "0123456789"
			InEditMode     = "2" MaxPages   = "20" NoFormatEscape    = "1"      NoHScroll   = "0" NoVScroll = "0"
			NumberId       = "1" SafeCSS    = '1'  Selecting		 = "1"
			SelectingCells = "0" SortIcons  = "0"  Style             = "Office" SuppressCfg = "1" 
			Paging		   = "2" AllPages	= "0"  PageLength		 = "24"		MaxPages	= "20"
			CopySelected   = "0" CopyFocused= "1"  CopyCols			 = "0"		ExportFormat="xls" ExportCols="0"
	/>
	
	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>
	
	<!-- Grid Header Setting Area Start  -->
	<Header	id="Header"	Align="center"
			COMPANY_ID 		= "회사 코드"
			USER_ID		 	= "작성자ID"
			NAME		 	= "작성자"
			BILL_GB			= "전표 구분"
			BILL_NDX		= "전표KEY"
			ERPBILLYEAR 	= "회계 연도"
			ERPBILL 		= "전표 번호"
			BILL_STATUS 	= "전표 상태"
			GW_STATUS	 	= "결재 상태"
			CARD_NO		 	= "카드 번호"
			CARD_APPRNO 	= "카드 승인 번호"
			CARD_APPRDT 	= "카드 승인 일시"
			CARD_AQUIDT 	= "카드 매입 일시"
			CORPORATION_NO 	= "가맹점사업자번호"
			CARD_NDX		= "법인카드 마스터 순번"
			TAXAPPR_NO		= "세금계산서 승인번호"
			CURRENCY_UNIT 	= "통화 코드"
			RATE		 	= "환율"
			TOT_AMOUNT	 	= "총 금액"
			AMT_AMOUNT	 	= "공급가액"
			VAT_AMOUNT	 	= "부가세액"
			SER_AMOUNT	 	= "봉사료"
			FRE_AMOUNT	 	= "면세금액"
			BOOK_DT		 	= "전기일"
			EVIDENCE_DT		= "증빙일"
			WORKPLACE_CD	= "부가세사업장 코드"
			WORKPLACE_NM	= "부가세사업장"
			ACCGUBUN		= "계정구분"
			TAX_CD		 	= "세금코드"
			PURCHASEOFFI_CD	= "구매처코드"
			PURCHASEOFFI_NM	= "구매처명"
			BANK_CD		 	= "은행코드"
			BANK_NM		 	= "은행명"
			PAYEE_CD        = "수취인코드"
			PAYEE_NM        = "수취인명"
			ACCOUNT_NO		= "계좌번호"
			ACCOUNT_HOLDER	= "예금주명"
			PAY_TERM        = "지급조건"
			PAYDUE_DT       = "지급예정일"
			ERR_MSG         = "오류메시지"
			REG_DT          = "작성일시"
			UPT_USERID      = "수정자"
			UPT_DT          = "수정일시"
			DEL_YN          = "삭제여부"
			DEL_USERID      = "삭제자"
			DEL_DT          = "삭제일시"
			ADD_PRSN        = "추가직원ID"
			ADD_PRSN_NM     = "추가직원명"
			ATTACH_GRP_NO	= "첨부파일"
			
     />
	<!-- Grid Header Setting Area End    -->
	
	<!-- Fileter Setting Area Start      -->
	<Head>
		<Filter	id = "Filter" CanFocus = "1"/>
	</Head>
	<!-- Fileter Setting Area End        -->
	
	<Cols>
		<C Name="COMPANY_ID"		Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="0" />
		<C Name="USER_ID"			Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="0" />
		<C Name="NAME"				Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="0" />
		<C Name="BILL_GB"			Type="Enum"	 Width="100"  Align="center" CanEdit="0"    EmptyValue="" <tag:enum codeGrp="BILL_GB" /> />
		<C Name="BILL_NDX"			Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="0" />
		<C Name="ERPBILLYEAR"		Type="Text"	 RelWidth="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="ERPBILL"			Type="Text"	 RelWidth="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="BILL_STATUS" 		Type="Enum"  RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1"
									EmptyValue="" <tag:enum codeGrp="BILL_STATUS" /> />
		<C Name="GW_STATUS" 		Type="Enum"  RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1"
								  	EmptyValue="" <tag:enum codeGrp="GW_STATUS" /> />
		<C Name="REG_DT" 			Type="Date"  RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		<C Name="BOOK_DT" 			Type="Date"  RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		<C Name="EVIDENCE_DT" 		Type="Date"  RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		<C Name="CARD_NO"			Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="0" />
		<C Name="CARD_APPRNO"		Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="0" />
		<C Name="CARD_APPRDT" 		Type="Date"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		<C Name="CARD_AQUIDT" 		Type="Date"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		<C Name="CORPORATION_NO"	Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="0" />
		<C Name="CARD_NDX"			Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="0" />
		<C Name="TAXAPPR_NO"		Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="0" />
		<C Name="CURRENCY_UNIT" 	Type="Text"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="RATE" 				Type="Text"  Width="100" CanEdit="0" Visible="0" Align="right" CanExport="0" />
		<C Name="TOT_AMOUNT" 		Type="Int"   RelWidth="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="AMT_AMOUNT" 		Type="Int"   RelWidth="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="VAT_AMOUNT" 		Type="Int"   RelWidth="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="SER_AMOUNT" 		Type="Int"   Width="100" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="FRE_AMOUNT" 		Type="Int"   Width="100" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="WORKPLACE_CD" 		Type="Text"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="WORKPLACE_NM" 		Type="Text"  RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="ACCGUBUN" 			Type="Enum"  RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1"
									EmptyValue="" <tag:enum codeGrp="ACCGUBUN" /> />
		<C Name="TAX_CD" 			Type="Enum"  RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1"
									EmptyValue="" <tag:taxEnum billGb="TAX" /> />
		<C Name="ADD_PRSN" 			Type="Text"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="ADD_PRSN_NM" 		Type="Text"  RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="ATTACH_GRP_NO" 	Type="Icon"  RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="PURCHASEOFFI_CD" 	Type="Text"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="PURCHASEOFFI_NM" 	Type="Text"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="BANK_CD" 			Type="Text"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="BANK_NM" 			Type="Text"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="PAYEE_CD" 			Type="Text"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="PAYEE_NM" 			Type="Text"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="ACCOUNT_NO" 		Type="Text"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="ACCOUNT_HOLDER" 	Type="Text"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="PAY_TERM" 			Type="Text"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="PAYDUE_DT" 		Type="Date"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" Format="yyyy-MM-dd" />
		<C Name="ERR_MSG" 			Type="Text"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="UPT_USERID" 		Type="Text"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="UPT_DT" 			Type="Date"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" Format="yyyy-MM-dd" />
		<C Name="DEL_YN" 			Type="Enum"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="DEL_USERID" 		Type="Text"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="DEL_DT" 			Type="Date"  Width="100" CanEdit="0" Visible="0" Align="center" CanExport="0" Format="yyyy-MM-dd" />
	</Cols>
	
	<!-- Grid Paging Setting Area Start  -->
	<Pager Visible = "0"/>

	<Solid>
		<I id = "PAGER" Cells = "NAV,LIST,ONE,GROUP" Space = "4"
		   NAVType	     = "Pager"
		   LISTType	     = "Pages" LISTAlign   = "left" LISTLeft   = "10"                LISTRelWidth  = "1"
		   ONEType       = "Bool"  ONECanEdit  = "1"    ONEFormula = "Grid.AllPages?0:1" ONELabelRight = "페이지단위로보임"
		   ONEOnChange = "Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
		   GROUPCanFocus = "0"
		/>
	</Solid>
	<!-- Grid Paging Setting Area End    -->
	
	<Toolbar	Space="0"	Styles="1"	Cells="Empty,Cnt,Found,상신,항목선택,새로고침,인쇄,엑셀"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType = "Button"
				CntType = "Html" CntFormula = '"총 : &lt;b>"+count(7)+"&lt;/b>행"' CntWidth = '-1' CntWrap = '0'
				FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>행'" FoundWidth = '-1' FoundWrap = '0'
				상신Type ="Html" 상신="&lt;a href='#none' title='상신' class=&quot;treeButton treeReport&quot;
			 						 onclick='fn_apprRequest(&quot;${gridId}&quot;)'>상신&lt;/a>"
				일괄상신Type ="Html" 일괄상신="&lt;a href='#none' title='일괄상신' class=&quot;treeButton treeAllreport &quot;
			 						 onclick=''>일괄상신&lt;/a>"
				새로고침Type = "Html" 새로고침 = "&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
									  onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
 				인쇄Type = "Html" 인쇄 = "&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
									  onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
 				엑셀Type = "Html" 엑셀 = "&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
									  onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
				항목선택Type="Html" 항목선택="&lt;a href='#none' title='항목선택' class=&quot;treeButton treeSelect	&quot;
			  						  onclick='showColumns(&quot;${gridId}&quot;,&quot;xls&quot;)'>항목선택&lt;/a>"
	/>

</Grid>