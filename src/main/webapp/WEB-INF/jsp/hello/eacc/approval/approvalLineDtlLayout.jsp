<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : approvalLineDtlLayout.jsp
  Description : 계정 관리 상세 그리드 레이아웃 화면
  Modification Information
 
     수정일    	     수정자                   수정내용
   -------    --------    ---------------------------
   2017.09.05  윤선철	              최초 생성

    author   : 윤선철
    since    : 2017.09.05
--%>
<c:set var="gridId" value="ApprovalLineDtlLayout"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			AcceptEnters   = "1" Calculated = "1"  CalculateSelected = "1"      DateStrings = "2" Deleting  = "0"
			Dragging       = "0" Editing    = "1"  EnterMode         = "4"      Filtering   = "1" IdChars   = "0123456789"
			InEditMode     = "2" MaxPages   = "20" NoFormatEscape    = "1"      NoHScroll   = "0" NoVScroll = "0"
			NumberId       = "1" SafeCSS    = '1'  Selecting		 = "0"
			SelectingCells = "0" SortIcons  = "0"  Style             = "Office" SuppressCfg = "1" 
			Paging		   = "2" AllPages	= "0"  PageLength		 = "20"		MaxPages	= "20"
			CopySelected   = "0" CopyFocused= "1"  CopyCols			 = "0"		ExportFormat="xls" ExportCols="0"
	/>
	
	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>
		
	<!-- Grid Header Setting Area Start  -->	
	<Header	id = "Header" Align = "center"
			APPRLINE_NDX	 	= "일련번호"
			APPRLINE_ITEM_NDX	= "라인일련번호"
			COMPANY_ID      	= "회사 코드"
			APPR_SEQ			= "결재순서"
			LINE_GB				= "결재 구분"
			APPR_DEPT_ID		= "결재자부서 ID"
			APPR_DEPT_NM		= "결재자부서명"
			JOB_GRADE			= "직급"
			APPR_ID				= "결재자자 ID"
			APPR_NM				= "결재자명"
			USER_ID				= "등록자 ID"
			REG_DT				= "등록일자"
	/>
	<!-- Grid Header Setting Area End    -->
	
	<!-- Fileter Setting Area Start      -->
	<Head>
		<Filter	id = "Filter" CanFocus = "1"/>
	</Head>
	<!-- Fileter Setting Area End        -->
	
	<!-- Grid Paging Setting Area Start  -->
	<Pager Visible = "0"/>

	<Solid>
		<I id = "PAGER" Cells = "NAV,LIST,ONE,GROUP" Space = "4"
		   NAVType	     = "Pager"
		   LISTType	     = "Pages" LISTAlign   = "left" LISTLeft   = "10"                LISTRelWidth  = "1"
		   ONEType       = "Bool"  ONECanEdit  = "1"    ONEFormula = "Grid.AllPages?0:1" ONELabelRight = "<spring:message code='button.showUnitPage' />"
		   ONEOnChange = "Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
		   GROUPCanFocus = "0"
		/>
	</Solid>
	<!-- Grid Paging Setting Area End    -->
	
	<Cols>
		<C Name="APPRLINE_NDX"  		Type="Text" Align="center" CanExport="0" RelWidth="30"  Visible="0"/>
		<C Name="APPRLINE_ITEM_NDX"  	Type="Text" Align="center" CanExport="0" RelWidth="30"  Visible="0"/>
		<C Name="COMPANY_ID"  			Type="Text" Align="center" CanExport="0" RelWidth="30"  Visible="0"/>
		<C Name="APPR_SEQ"    			Type="Text" Align="center" CanExport="0" RelWidth="60"  CanEdit="0" Visible="1"/>
		<C Name="LINE_GB"    			Type="Enum" RelWidth="100" CanEdit="0" Visible="1" Align="center" CanEdit="0"
									    EmptyValue="선택" <tag:enum codeGrp="APPR_LINE" /> />
		<C Name="APPR_DEPT_ID"    		Type="Text" Align="center" CanExport="0" RelWidth="100" Visible="0"/>
		<C Name="APPR_DEPT_NM"    		Type="Text" Align="center" CanExport="0" RelWidth="100" CanEdit="0" Visible="1"/>
		<C Name="JOB_GRADE"     		Type="Enum" RelWidth="100" CanEdit="0" Visible="1" Align="center" CanEdit="0"
							    		EmptyValue="선택" <tag:enum codeGrp="JOB_GRADE" /> /><!-- 직급 -->
		<C Name="APPR_ID"    			Type="Text" Align="center" CanExport="0" RelWidth="100" Visible="0"/>
		<C Name="APPR_NM"    			Type="Text" Align="center" CanExport="0" RelWidth="150" CanEdit="0"  Visible="1"/>
		<C Name="USER_ID"    			Type="Text" Align="center" CanExport="0" RelWidth="150" Visible="0"/>
		<C Name="REG_DT"        		Type="Date" Align="center" CanExport="0" RelWidth="100" CanEdit="0" Visible="1" Format="yyyy-MM-dd"/>
	</Cols>
	<Toolbar	Space="0"	Styles="1"	Cells="Empty,Cnt,설정,<spring:message code='button.refresh' />,<spring:message code='button.print' />,<spring:message code='button.excel' />"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType = "Button"
				CntType = "Html" CntFormula = '"Total : &lt;b>"+count(7)+"&lt;/b>"' CntWidth = '-1' CntWrap = '0'
				FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>'" FoundWidth = '-1' FoundWrap = '0'
				DelType = "Html" DelFormula = 'var cnt=count("Row.Deleted>0",7);return cnt?" Del :&lt;b>"+cnt+"&lt;/b>":""' DelWidth = '-1' DelWrap = '0'
				설정Type ="Html"  설정="&lt;a href='#none' title='설정' class=&quot;treeButton treeSetting&quot;
									onclick='fn_popApprLineList(&quot;${gridId}&quot;)'>설정&lt;/a>"
				<spring:message code='button.refresh' />Type = "Html" <spring:message code='button.refresh' /> = "&lt;a href='#none' title='<spring:message code='button.refresh' />' class=&quot;defaultButton icon refresh&quot;
									  onclick='reloadGrid(&quot;${gridId}&quot;)'><spring:message code='button.refresh' />&lt;/a>"
 				<spring:message code='button.print' />Type = "Html" <spring:message code='button.print' /> = "&lt;a href='#none' title='<spring:message code='button.print' />' class=&quot;defaultButton icon printer&quot;
									  onclick='printGrid(&quot;${gridId}&quot;)'><spring:message code='button.print' />&lt;/a>"
 				<spring:message code='button.excel' />Type = "Html" <spring:message code='button.excel' /> = "&lt;a href='#none' title='<spring:message code='button.excel' />'class=&quot;defaultButton icon excel&quot;
									  onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'><spring:message code='button.excel' />&lt;/a>"
	/>	
</Grid>