<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : myApprovalDtl.jsp
  Description : 결재대기 상세 화면
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.09.08    정순주              최초 생성

    author   : 정순주
    since    : 2017.09.08
--%>
<script type="text/javascript">
var gubunDiv = '〔';
$(document).ready(function(){
	
	// 첨부파일
	var maxFileCnt = 10;
	for(var i=0; i<1; i++){
		$('#attachFile'+i).append('<input type="file" multiple id="ex_file'+i+'" class="ksUpload" name="fileList">');	
		var fileType = 'ppt |pps |pptx |ppsx |pdf |hwp |xls |xlsx |xlsm | xlt |doc |dot |docx |docm |txt |gif |jpg |jpeg |png';
		
		$('#ex_file'+i).MultiFile({
				accept : fileType
			,	list   : '#detailFileList'+i
			,	max    : maxFileCnt
			,	STRING : {
					remove: '<img src="/images/com/web/btn_remove.png">'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
//						text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" >',
					idx	  : i,
					denied:'$ext는(은) 업로드할 수 없는 파일 확장자입니다.'
				}
		});
		
		$('.MultiFile-wrap').attr("id", "jquery"+i);
	}
	
	// 첨부파일 삭제
	$('.icn_fileDel').on('click', function(){
		delModiFile($(this));
	});
	
	/// 파일 다운로드
	$('.btnFileDwn').on('click', function(){
		var ATTACH = $(this).data("attach");
		var ATTACH_GRP_NO = $('#ATTACH_GRP_NO').val();
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO='+ ATTACH_GRP_NO + '&ATTACH='+ATTACH;
		$("#fileDownFrame").attr("src",src);
	});
	
	/* 증빙보기 버튼 이벤트 */
	$('.evidenceBtn').on('click', function(){
		var billGb = $(this).data('billgb');
		var billNdx = $(this).data('billndx');
		var url = '';
		
		// 전표구분에 따라 url분기
		if(billGb == '1'){
			url = '/hello/eacc/cardBill/getCardEvidenceView.do?cardBillNdx='+billNdx;
			$('#evidenceModal .modal-content').css('height', '100%');
			$('#evidenceModal .modal-dialog').removeClass('wd-per-60').addClass('wd-per-20');
		}else if(billGb == '2'){
			url = '/hello/eacc/taxBill/getTaxEvidenceView.do?TAXBILL_NDX='+billNdx;
			$('#evidenceModal .modal-content').css('height', '500');
			$('#evidenceModal .modal-dialog').removeClass('wd-per-20').addClass('wd-per-50');
		}else{
			
		}
		
		$('#evidenceModal .modal-bodyIn').load(url, function(){
			$('#evidenceModal').modal();
		});
	});
	
});

/* 상신 취소 */
function fn_apprCancel(){
	
	if(confirm("상신 취소 하시겠습니까?")){
		$('#apprSubmitForm').attr('action','/hello/eacc/approval/apprCancelAjax.do').ajaxSubmit(function(json){
			if(json.resultCd == "T"){
				alert(json.resultMsg);
				window.location.reload(true);
			}else{
				if(typeof json.errorMsg == 'undefined' || json.errorMsg == 'undefined'){
					alert(json.resultMsg);
					reLoadGrid();
					toggleModal($("#myApprovalDtlModal"));
				}else{
					alert(json.resultMsg + "\n" + json.errorMsg);
					reLoadGrid();
					toggleModal($("#myApprovalDtlModal"));
				}
			}
		});
	}
}
</script>

	<div class="modal-dialog root wd-per-85"> <!-- 원하는 width 값 입력 -->
        <div class="modal-content">
			<div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
               	<h4 class="modal-title">결재문서 상세</h4>
               	
               	<div style="position:absolute; top:13px; right:40px;">
	               	<c:if test="${apprHeader.APPR_H_STS eq '02' and apprHeader.REG_ID eq sessionScope.ssUserId}">
						<a href="#" class="btn comm st01 f-r" style="margin-right: 5px; margin-bottom: 5px;" onclick="fn_apprCancel(); return false;">상신취소</a>
					</c:if>
					<c:if test="${apprHeader.APPR_H_STS eq '04' and apprHeader.REG_ID eq sessionScope.ssUserId}">
						<a href="#" class="btn comm st01 f-r" style="margin-right: 5px; margin-bottom: 5px;" onclick="printDiv('print'); return false;">인쇄</a>
					</c:if>
				</div>
            </div>
            
            <div id="print">
	            <div class="modal-body" >
					<div class="modal-bodyIn" >
					
					<form id="apprSubmitForm" action="" method="post" enctype="multipart/form-data">
							<!-- input : s -->
							<input type="hidden" name="APPR_NDX" value="${apprHeader.APPR_NDX }" />
							
							<!-- 첨부파일 정보 -->
							<input type="hidden" title="파일등록여부" id="attachExistYn" name="attachExistYn" value=""/>
							<input type="hidden" title="삭제Seq" id="delSeq" name="delSeq" value=""/>
							<input type="hidden" title="파일 건수" id="fileCnt" name="fileCnt" value=""/>
							<input type="hidden" title="경비 모듈" id="MODULE" name="MODULE" value="8">
							<input type="hidden" id="ATTACH_GRP_NO" name="ATTACH_GRP_NO" value="${apprHeader.ATTACH_GRP_NO }"/>
							<!-- input : e -->
								
							<!-- s:tb-wrap -->
							<div class="approval-tb-wrap">
								<table class="approval-tb-st type01">
									<tbody>
										<tr>
											<th rowspan="4">회계전표</th>
										</tr>
										<tr>
											<c:forEach var="apprItem" items="${apprItems }">
												<td style="background:#dde9f5;">${apprItem.JOB_GRADE }</td>
											</c:forEach>
										</tr>
										<tr class="stamp">
											<c:forEach var="apprItem" items="${apprItems }">
												<td>
													<c:choose>
														<c:when test="${apprItem.APPR_I_STS eq '01'}">
															<c:choose>
																<c:when test="=${apprItem.SEAL_ATTACH_GRP_NO eq ''}">
																	${apprItem.NAME }
																</c:when>
																<c:otherwise>
																	<img src="/attach/thumbFileDownload.do?ATTACH_GRP_NO=${apprItem.SEAL_ATTACH_GRP_NO }&ATTACH=1" width="50" height="50" class="thumbnail" alt="thumbnail" />
																</c:otherwise>
															</c:choose>
														</c:when>
														<c:when test="${apprItem.APPR_I_STS eq '02'}">
															<img src="/images/eaccounting/reject_seal.gif" width="50" height="50" class="thumbnail" alt="반려직인" />
														</c:when>
														<c:when test="${apprItem.APPR_I_STS eq '03'}">
															<img src="/images/eaccounting/cancel_seal.gif" width="50" height="50" class="thumbnail" alt="승인취소직인" />
														</c:when>
														<c:otherwise>
														</c:otherwise>
													</c:choose>
												</td>
											</c:forEach>
										</tr>
										<tr>
											<c:forEach var="apprItem" items="${apprItems }">
												<td>${apprItem.NAME }<br/>${apprItem.APPR_DT }</td>
											</c:forEach>
										</tr>
									</tbody>
								</table>
							</div>
							
							<div class="approval-tb-wrap mgn-t-10">
								<table class="approval-tb-st type02" >
									<caption class="screen-out">결재요청 목록</caption>
									<tbody>
										<tr>
											<th scope="row">문서번호</th>
											<td>${apprHeader.GW_DOC }</td>
											<th scope="row">기안부서</th>
											<td>${apprHeader.REG_DEPTNM }</td>
											<th scope="row">기안자</th>
											<td>${apprHeader.REG_USERNM }</td>
											<th scope="row">기안일자</th>
											<td>${apprHeader.REG_DT }</td>
										</tr>
										<tr>
											<th scope="row">문서제목</th>
											<td colspan="7">
												${apprHeader.TITLE }
											</td>
										</tr>
										<tr>
											<th scope="row">첨부파일</th>
											<td colspan="7">
												<div class="bxType01 fileWrap">
													<div class="fileList">
														<div id="detailFileList" class="fileDownLst">
															<c:forEach var="item" items="${attachList}" varStatus="idx">
																<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																	<span class="MultiFile-export">
																		${item.SEQ_DSP}
																	</span> 
																	<span class="MultiFile-title" title="File selected: ${item.NAME}">
																		${item.NAME}(${item.FILE_SIZE}kb)
																	</span>
																	<a href="#none" class="btnFileDwn icon download" data-attach="${item.ATTACH}">다운로드</a>
																</div>		
															</c:forEach>	
														</div>
													</div>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- e:tb-wrap -->
							
							<!-- s:tb-wrap -->
							<div class="approval-tb-wrap mgn-t-20">
								<h4>지출내역(${headerList.size() }건)</h4>
								<table class="approval-tb-st type02" >
									<colgroup>
										<col width="4%">
										<col width="7%">
										<col width="8%">
										<col width="7%">
										<col width="7%">
										<col width="10%">
										<col width="15%">
										<col width="10%">
										<col width="20%">
										<col width="7%">
										<col width="7%">
									</colgroup>
									<tbody>
										<tr>
											<th class="t-c">반려</th>
											<th class="t-c">전표번호</th>
											<th class="t-c">전기일</th>
											<th class="t-c">구매처</th>
											<th class="t-c">세금코드</th>
											<th class="t-c">추가직원</th>
											<th class="t-c">계정과목</th>
											<th class="t-c">코스트센터</th>
											<th class="t-c">적요</th>
											<th class="t-c">차변</th>
											<th class="t-c">대변</th>
										</tr>
										
										<c:set var="totTotAmt" value="0" />
										<c:set var="totItemAmt" value="0" /> 	
										<c:set var="totItemVat" value="0" /> 	
										<c:forEach var="billHeader" items="${headerList}" varStatus="status">
											<c:set var="totTotAmt" value="${totTotAmt + billHeader.TOT_AMOUNT }" />
											<c:choose>
												<c:when test="${apprHeader.THIS_APPR_USERID ne sessionScope.ssUserId }">
													<c:set var="disabled" value="disabled='disabled'" />
												</c:when>
												<c:otherwise>
													<c:choose>	
														<c:when test="${billHeader.BILL_STS eq 'R' }">
															<c:set var="disabled" value="disabled='disabled'" />
														</c:when>
														<c:otherwise>
															<c:set var="disabled" value="" />
														</c:otherwise>
													</c:choose>
												</c:otherwise>
											</c:choose>
											<c:choose>
												<c:when test="${billHeader.BILL_STS eq 'R' }">
													<c:set var="checked" value="checked='checked'" />
												</c:when>
												<c:otherwise>
													<c:set var="checked" value="" />
												</c:otherwise>
											</c:choose>
											<c:forEach var="items" items="${itemList[status.index] }" varStatus="status2">
												<c:set var="rowspan" value="${itemList[status.index].size()+1 }"></c:set>
													<c:if test="${status2.index == 0 }">
														<c:forEach var="item" items="${itemList[status.index] }" varStatus="status2">
															<c:if test="${item.ITEM_VAT > 0 }">
																<c:set var="rowspan" value="${rowspan+1 }"></c:set>
															</c:if>
														</c:forEach>
														<tr>
															<td rowspan="${rowspan }" id="rowspan">
																<input type="checkbox" id="chkAppr${status.count }" name="chkAppr" ${checked } ${disabled } data-billGb="${billHeader.BILL_GB }" data-billNdx="${billHeader.BILL_NDX }" />
															</td>
															<td rowspan="${rowspan }" class="t-c">
																<c:choose>
																	<c:when test="${billHeader.ATTACH_GRP_NO eq null || billHeader.ATTACH_GRP_NO == ''}">
																		${billHeader.ERPBILL }
																	</c:when>
																	<c:otherwise>
																		<a href="javascript:fn_openFileDownModal('${billHeader.ATTACH_GRP_NO }');" style="text-decoration: underline;">${billHeader.ERPBILL }</a>
																	</c:otherwise>
																</c:choose>
																<c:if test="${billHeader.BILL_GB eq '1' or billHeader.BILL_GB eq '2' }">
																	<br><a href="#none" class="numFileView evidenceBtn" data-billgb="${billHeader.BILL_GB }" data-billndx="${billHeader.BILL_NDX }">증빙</a>
																</c:if>
															</td>
															<td rowspan="${rowspan }" class="t-c">${billHeader.BOOK_DT }</td>
															<td rowspan="${rowspan }" class="t-c">${billHeader.PURCHASEOFFI_NM }</td>
															<td rowspan="${rowspan }" class="t-c">${billHeader.TAX_NM }</td>
															<td rowspan="${rowspan }" class="t-l">${billHeader.ADD_PRSN_NM }</td>
															<td>[미지급]경비</td>
															<td></td>
															<td></td>
															<td class="t-r"></td>
															<td class="t-r"><fmt:formatNumber type="number" value="${billHeader.TOT_AMOUNT }"/></td>
														</tr>
													</c:if>
												<c:set var="totItemAmt" value="${totItemAmt + items.ITEM_AMT }" />
												<tr>
													<td class="t-l">${items.GLNAME }</td>
													<td class="t-l">${items.KTEXT }</td>
													<td class="t-l">${items.SUMMARY }</td>
													<td class="t-r"><fmt:formatNumber type="number" value="${items.ITEM_AMT }"/>
														<c:set var="totItemVat" value="${totItemVat + items.ITEM_VAT }" />
													</td>
													<td class="t-r"></td>
												</tr>
												<c:if test="${items.ITEM_VAT > 0}">
													<tr>
														<td>부가세대급금</td>
														<td></td>
														<td>${items.SUMMARY }</td>
														<td class="t-r">
															<fmt:formatNumber type="number" value="${items.ITEM_VAT }"/>
														</td>
														<td class="t-r"></td>
													</tr>
												</c:if>
											</c:forEach>
										</c:forEach>
										
										<!-- 합계 -->
										<tr>
											<th colspan="9" class="t-c">합계</th>
											<td class="t-r"><fmt:formatNumber type="number" value="${totItemAmt+totItemVat }"/></td>
											<td class="t-r"><fmt:formatNumber type="number" value="${totTotAmt }"/></td>
										</tr>
									</tbody> 
								</table>
							</div>
							
							<div class="approval-tb-wrap mgn-t-20">
								<h4>[결재의견]</h4>
								<table class="approval-tb-st type02" >
									<colgroup>
										<col width="20%">
										<col width="80%">
									</colgroup>
									<c:forEach var="apprItem" items="${apprItems }" varStatus="status">
										<c:if test="${apprItem.APPR_MEMO ne '' and apprItem.APPR_MEMO ne null }">
											<tr>
												<th class="t-l">
													[${apprItem.NAME } / ${apprItem.JOB_GRADE } / ${apprItem.APPR_DEPT_NM }] ${apprItem.APPR_DT }
												</th>
												<td>
													${apprItem.APPR_MEMO }
												</td>
											</tr>
										</c:if>
									</c:forEach>
								</table>
							</div>
							<c:if test="${apprHeader.CANCEL_MEMO != '' and apprHeader.CANCEL_MEMO != null }">
								<div class="approval-tb-wrap mgn-t-20">
									<h4>[승인취소 의견]</h4>
									<table class="approval-tb-st type02" >
										<colgroup>
											<col width="20%">
											<col width="80%">
										</colgroup>
										<tr>
											<th class="t-l">
												[${apprHeader.UPT_USERNM } / ${apprHeader.UPT_JOB_GRADE } / ${apprHeader.UPT_DEPTNM }] ${apprHeader.UPT_DT }
											</th>
											<td>
												${apprHeader.CANCEL_MEMO }
											</td>
										</tr>
									</table>
								</div>
							</c:if>
						</form>
						<iframe id="fileDownFrame" style="width:0px;height:0px;border:0px"></iframe>
					</div>
	            </div>
            </div>
        </div>
    </div>

