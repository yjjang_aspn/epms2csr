<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : popApprLineMng.jsp
  Description : 결재라인 관리 화면
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.09.06    윤선철              최초 생성

    author   : 윤선철
    since    : 2017.09.06
--%>
<script src="/js/jquery/jquery-1.11.2.min.js" type="text/javascript" ></script> <!-- 제이쿼리 구동 -->
<script src="/js/jquery/jquery.blockUI.js" type="text/javascript"></script>
<script src="/js/jquery/jquery.calendar.js" type="text/javascript"></script>
<script src="/js/eacc/eaccCommon.js" type="text/javascript" ></script>
<script src="/js/com/web/modalPopup.js"></script>
<script src="/js/jquery/jquery-filestyle.js" type="text/javascript" ></script>
<script src="/js/jquery/jquery.MultiFile.js" type="text/javascript"></script>
<script src="/js/jquery/jquery.ajaxsubmit.js" type="text/javascript"></script>
<script src="/js/treegrid/GridE.js" type="text/javascript" ></script>
<script type="text/javascript">
// 전역변수
var selDeptRow = null; // 선택된 부서
$(document).ready(function(){
// 	/* 데이터 URL */
	var divisionUrl = "/mem/division/divisionData.do";
	$("#memberPrivList").find('bdo').attr("Data_Url", divisionUrl);
});	

// 	/* 데이터 URL */
// 	var apprLineUrl = "/hello/eacc/approval/apprLineDtl.do";
// 	console.log("${apprLine.COMPANY_ID}");
// 	var parameters = "?A";
Grids.OnClick = function(grid, row, col){
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}

 	//부서의 포함된 사용자 조회 이벤트
	if(grid.id == "DivisionList") {
		selDeptRow = row;
		getMemberList(row.DIVISION, row.CUR_LVL, row.COMPANY_ID);
	}
}
// 	// 팝업 제목 세팅
// 	$("#header").html("<h3>결재선 수정</h3>");
	
// 	Grids.OnClick = function(grid, row, col){
// 		if(row.id == "Filter" || row.id == "Header" || row.id == "PAGER" ||
// 			row.id == "spanHeader" || row.id == "Toolbar" || col == "Panel"){
// 			return;
// 		}
		
// 		// 부서 그리드 클릭 시  이벤트
// 		if(grid.id == "DivisionList"){
// 			selDeptRow = row;
// 			// 부서에 속한 사원 조회
// 			fn_getMemberList(row.DIVISION, row.CUR_LVL, row.COMPANY_ID);
// 		}
// 	}
	
// 	// 그리드 삭제 버튼
// 	Grids.OnRowDelete = function(grid, row, type){
// 	  if(grid.id == "ApprLine"){
// 	    if(row.Deleted){
// 	      Grids.ApprLine.RemoveRow(row);
// 	    }
// 	    fn_countApprSeq();
// 	  }
// 	};
	
// 	// 그리드 드래그 시 결재순서 재정렬
// 	Grids.OnRowMove = function(grid, row, oldparent, oldnext) {
// 		if(row.id == "Filter" || row.id == "Header" || row.id == "PAGER" || row.id == "Toolbar"){
// 			return;
// 		}
		
// 		if(grid.id == "ApprLine"){
// 			fn_countApprSeq();
// 		}
// 	}
// });

// // 부서에 속한 사원 조회
// function fn_getMemberList(divNo, curLvl, COMPANY_ID) {	
// 	Grids.ApprLineList.Source.Data.Url = "/sys/member/admin/memberData.do?divNo=" + divNo + "&curLvl=" + curLvl + "&COMPANY_ID=" + COMPANY_ID;
// 	Grids.ApprLineList.ReloadBody();
// }

// // 사원 정보 그리드에서 '+' 버튼 클릭 시, 결재자 정보 그리드에 담당자 반영
// function addRowGrid(gridNm){
// 	var selRows = Grids.ApprLineList.GetSelRows();
// 	var isAddFlag = false;
	
// 	if(selRows == null || selRows == ""){
// 		alert("추가할 결재자를 선택하세요.");
// 		return;
// 	}
	
// 	var msg = "";
// 	for(var i=0; i<selRows.length; i++){
// 		for(var r =Grids.ApprLine.GetFirstVisible(); r; r=Grids.ApprLine.GetNextVisible(r)){
// 			if(selRows[i].user_id == r.APPR_USER_ID){
// 				if(msg == ''){
// 					msg += r.APPR_USER_ID;
// 				}else{
// 					msg += ", " + r.APPR_USER_ID;
// 				}
// 				isAddFlag = true;
// 			}
// 		}
// 	}
	
// 	if(isAddFlag){
// 		alert(msg + "는 이미 추가된 결재자 입니다.");
// 		return;
// 	}
		
// 	for(var i=0; i<selRows.length; i++){
// 		var row = Grids.ApprLine.AddRow(null, null, true);
// 		Grids.ApprLine.SetValue(row, "BUKRS", selRows[i].COMPANY_ID, 1);
// 		Grids.ApprLine.SetValue(row, "APPR_USER_ID", selRows[i].user_id, 1);
// 		Grids.ApprLine.SetValue(row, "APPR_USER_NM", selRows[i].NAME, 1);
// 		Grids.ApprLine.SetValue(row, "APPR_DEPT_ID", selRows[i].division_cd, 1);
// 		Grids.ApprLine.SetValue(row, "APPR_DEPT_NM", selRows[i].division_nm, 1);
// 	}
	
// 	fn_countApprSeq();
// }

// // 결재자 순서 재정렬
// function fn_countApprSeq(){
// 	var i = 1;
// 	for(var r =Grids.ApprLine.GetFirstVisible(); r; r=Grids.ApprLine.GetNextVisible(r)){
// 		Grids.ApprLine.SetValue(r, "APPR_SEQ", i, 1);
// 		i++;
// 	}
// }

// // 결재라인 저장
// function fn_saveApprLine(gridNm){
	
// 	var member_appr = ${member_appr};
// 	var appr_seqs = '';
// 	var appr_user_ids = '';
// 	var appr_dept_ids = '';
// 	var companys = '';
	
// 	for(var r =Grids.ApprLine.GetFirstVisible(); r; r=Grids.ApprLine.GetNextVisible(r)){
// 		var appr_seq = r.APPR_SEQ+"";
// 		if(appr_seq == ''){
// 			appr_seq = " ";
// 		}
		
// 		var appr_user_id = r.APPR_USER_ID;
// 		if(appr_user_id == ''){
// 			appr_user_id = " ";
// 		}
		
// 		var appr_dept_id = r.APPR_DEPT_ID;
// 		if(appr_dept_id == ''){
// 			appr_dept_id = " ";
// 		}
		
// 		var bukrs = r.BUKRS;
// 		if(bukrs == ''){
// 			bukrs = " ";
// 		}
		
// 		if(appr_seqs == ''){
// 			appr_seqs = appr_seq;
// 			appr_user_ids = appr_user_id; 
// 			appr_dept_ids = appr_dept_id;
// 			companys = bukrs;
// 		} else{
// 			appr_seqs += ']' + appr_seq;
// 			appr_user_ids += ']' + appr_user_id;
// 			appr_dept_ids += ']' + appr_dept_id;
// 			companys += ']' + bukrs;
// 		}
// 	}
	
// 	if(confirm("결재라인을 저장하시겠습니까?")){
// 		jQuery.ajax({
//     		type : 'POST',
//     		url : '/project/eaccounting/approval/web/addApprLineAjax.do',
//     		cache : false,
//     		dataType : 'json',
//     		data : {
//     				member_appr : member_appr,
//     				appr_seq : appr_seqs,
//     				appr_user_id : appr_user_ids,
//     				appr_dept_id : appr_dept_ids,
//     				company : companys
//     				},
// 			async : false,
//     		error : function() {
//     			alert("처리중 오류가 발생 하였습니다.");
// 			},
//     		success : function(json) {
//     			if(json.resultCd == "T"){
//     				alert(json.resultMsg);
//     				Grids.ApprLineList.ReloadBody();
//     				opener.location.reload();
//     				window.close();
//     			}else{
//     				if(typeof json.errorMsg == 'undefined' || json.errorMsg == 'undefined'){
//     					alert(json.resultMsg);
//     				}else{
//     					alert(json.resultMsg + "\n" + json.errorMsg);
//     				}
//     			}
// 			}
//     	});
// 	}
// }
</script>

<!-- Layer PopUp Start -->	

	<div class="modal-dialog root wd-per-60"> <!-- 원하는 width 값 입력 -->				
		 <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title">결재선 수정</h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
					 
					 <div class="modal-section">
						<!-- START -->
						<div class="fl-box" style="width:30%;">
							<!-- Left Grid Area Start   -->
							<div id="memberPrivList">
								<bdo	Debug="Error"
										Layout_Url="/mem/division/divisionLayout.do"
										Upload_Url="/mem/division/divisionEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols"				
										>
								</bdo>
							</div>
							<!-- Left Grid Area End     -->
						</div>
						<!-- Left Area End    -->
			
						<!-- Right Area Start -->
						<div class="fl-box" style="width:70%; height:50%;">
			
							<!-- Right Grid Area Start  -->
							<div id="PopMemberDivList" class="mgn-l-10">
								<bdo	Debug="Error"
										Layout_Url="/hello/eacc/common/popMemberDivLayout.do"
										>
								</bdo>
							</div>
							<!-- Right Grid Area End    -->
						</div>
						
						<div class="fl-box" style="width:70%; height:50%;">
			
							<!-- Right Grid Area Start  -->
							<div id="PopMemberDivList" class="mgn-l-10">
								<bdo	Debug="Error"
										Layout_Url="/hello/eacc/approval/popApprLineLayout.do"
										>
								</bdo>
							</div>
							<!-- Right Grid Area End    -->
						</div>
						<!-- Right Area End   -->
					
					</div>
				</div>
			</div>	
		</div>		
	</div>
	<!-- 좌측 : e -->
