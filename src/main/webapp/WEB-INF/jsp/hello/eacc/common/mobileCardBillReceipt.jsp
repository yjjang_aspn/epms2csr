<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : 
  Description : 
  Modification Information
  
  수정일			수정자			수정내용
  ------			------			--------
  2017-09-23		강승상			최초생성
  
  author	: 강승상
  since		: 2017-09-23
--%>
<html>
<head>
	<title>모바일 카드영수증</title>
	<link href="${pageContext.request.contextPath}/css/com/web/common.css" rel="stylesheet" />
	<link href="${pageContext.request.contextPath}/css/com/web/page.css" rel="stylesheet" />
</head>
<body>
	<div class="cardReceiptWrap" style="width: 350px">
		<div class="cardReceiptWrapIn">
			<div class="cardReceiptTit">
				${cardData.MERC_NAME}
			</div>
			<div class="cardReceiptCon">
				<table width="100%">
					<tr>
						<td style="padding-bottom:15px;'">
							<!-- ①사업자정보 start -->
							<table width="100%">
								<tr>
									<td colspan="2">[매장명] <c:out value="${cardData.MERC_NAME}" /></td>
								</tr>
								<tr>
									<td colspan="2">[사업자] <c:out value="${cardData.MERC_SAUP_NO}" /></td>
								</tr>
								<tr>
									<td colspan="2">[주   소] <c:out value="${cardData.MERC_ADDR}" /></td>
								</tr>
								<tr>
									<td class="t-l">[대표자] <c:out value="${cardData.MERC_REPR}" /></td>
									<td class="t-r">[TEL] <c:out value="${cardData.MERC_TEL}" /></td>
								</tr>
								<tr>
									<td colspan="2">[승인일] <c:out value="${cardData.AUTH_DATE}" />&nbsp;
										<c:out value="${fn:substring(cardData.AUTH_TIME, 0, 2)} : ${fn:substring(cardData.AUTH_TIME, 2, 4)} : ${fn:substring(cardData.AUTH_TIME, 4, 6)}" /></td>
								</tr>
							</table>
							<!-- ①사업자정보 end -->
						</td>
					</tr>
					<tr>
						<td style="border-top:3px dashed #808080; padding:15px 0;">
							<!-- ②금액정보 start -->
							<table width="100%">
								<tr>
									<td style="padding-bottom:15px;">
										<table width="100%">
											<tr>
												<td class="t-l" style="font-size:14px; font-weight:500;">공급가액</td>
												<c:set var="suprice"><fmt:formatNumber value="${cardData.AMT_AMOUNT}"></fmt:formatNumber></c:set>
												<c:set var="supriceLength" value="${fn:length(suprice)}" />
												<c:forEach var="i" begin="0" end="7" step="1">
													<c:set var="inverseIndex" value="${7-i}" />
													<td class="t-r" style="font-size:14px; font-weight:500; width:8px;">
														<c:choose>
															<c:when test="${inverseIndex<supriceLength}">
																<c:out value="${fn:substring(suprice,supriceLength-inverseIndex-1,supriceLength-inverseIndex)}" />
															</c:when>
															<c:otherwise></c:otherwise>
														</c:choose>
													</td>
												</c:forEach>
											</tr>
											<tr>
												<td class="t-l" style="font-size:14px; font-weight:500;">부가세액</td>
												<c:set var="vat"><fmt:formatNumber value="${cardData.VAT_AMOUNT}"></fmt:formatNumber></c:set>
												<c:set var="vatLength" value="${fn:length(vat)}" />
												<c:forEach var="i" begin="0" end="7" step="1">
													<c:set var="inverseIndex" value="${7-i}" />
													<td class="t-r" style="font-size:14px; font-weight:500; width:8px;">
														<c:choose>
															<c:when test="${inverseIndex<vatLength}">
																<c:out value="${fn:substring(vat,vatLength-inverseIndex-1,vatLength-inverseIndex)}" />
															</c:when>
															<c:otherwise></c:otherwise>
														</c:choose>
													</td>
												</c:forEach>
											</tr>
											<tr>
												<td class="t-l" style="font-size:14px; font-weight:500;">봉사료</td>
												<c:set var="tips"><fmt:formatNumber value="${cardData.SER_AMOUNT}"></fmt:formatNumber></c:set>
												<c:set var="tipsLength" value="${fn:length(tips)}" />
												<c:forEach var="i" begin="0" end="7" step="1">
													<c:set var="inverseIndex" value="${7-i}" />
													<td class="t-r" style="font-size:14px; font-weight:500; width:8px;">
														<c:choose>
															<c:when test="${inverseIndex<tipsLength}">
																<c:out
																		value="${fn:substring(tips,tipsLength-inverseIndex-1,tipsLength-inverseIndex)}" />
															</c:when>
															<c:otherwise></c:otherwise>
														</c:choose>
													</td>
												</c:forEach>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style="border-top:3px dashed #808080; padding-top:15px;">
										<table width="100%">
											<tr>
												<td width="" class="t-l" style="font-size:18px; font-weight:600;">총 금액</td>
												<c:set var="orgnapprtot"><fmt:formatNumber value="${cardData.TOT_AMOUNT}"></fmt:formatNumber></c:set>
												<c:set var="orgnapprtotLength" value="${fn:length(orgnapprtot)}" />
												<c:forEach var="i" begin="0" end="7" step="1">
													<c:set var="inverseIndex" value="${7-i}" />
													<td width=""  class="t-r" style="font-size:18px; font-weight:600; width:8px;">
														<c:choose>
															<c:when test="${inverseIndex<orgnapprtotLength}">
																<c:out value="${fn:substring(orgnapprtot,orgnapprtotLength-inverseIndex-1,orgnapprtotLength-inverseIndex)}" />
															</c:when>
															<c:otherwise></c:otherwise>
														</c:choose>
													</td>
												</c:forEach>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<!-- ②금액정보 end -->
						</td>
					</tr>
					<tr>
						<td style="border-top:3px dashed #808080; padding-top:15px;">
							<!-- ③카드정보 start -->
							<table width="100%">
								<tr>
									<td style="padding-bottom:15px;">
										<table width="100%">
											<tr>
												<td width="" class="t-l" style="font-size:14px; font-weight:500;">승인번호</td>
												<td width=""  class="t-r" style="font-size:14px; font-weight:500;"><c:out value="${cardData.AUTH_NUM}" /></td>
											</tr>
											<tr>
												<td width="" class="t-l" style="font-size:14px; font-weight:500;">하나카드</td>
												<td width=""  class="t-r" style="font-size:14px; font-weight:500;">
													<c:set var="str1" value="${cardData.CARD_NUM}"/>
													<c:set var="str2" value="${fn:substring(str1, 4 , 12)}" />
													<c:set var="str3" value="${fn:substring(str1, 14 , 16)}" />
													****<c:out value="${str2}" />**<c:out value="${str3}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<!-- ③카드정보 end -->
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</body>
</html>
