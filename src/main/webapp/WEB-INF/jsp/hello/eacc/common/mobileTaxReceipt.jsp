<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : taxReceipt.jsp
  Description : 세금계산서 증빙 영수증
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.10.18    강승상              최초 생성

    author   : 강승상
    since    : 2017.10.18
--%>
<html>
<head>
	<title>모바일 세금계산서 증빙</title>
	<link href="${pageContext.request.contextPath}/css/com/web/common.css" rel="stylesheet" />
	<link href="${pageContext.request.contextPath}/css/com/web/page.css" rel="stylesheet" />
</head>
<body>
	<!-- 세금계산서 증빙 테이블 -->
	<table width="920px" border="0" cellspacing="0" cellpadding="0" style="border: 1px solid #3b60ac; margin:0 auto">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="3" style="border: 1px solid #3b60ac; font-size: 11px; font-family: 맑은 고딕;">
					<tr>
						<td width="682" rowspan="2" align="center" height="32">
							<span style="color: #3b60ac; font-size: 20px;">
							 	<!-- 세금계산서 종류 -->
								<c:choose>
									<c:when test="${taxHeader.ZDTIG eq '02' || taxHeader.ZDTIG eq 'B02'}">
										전자계산서
									</c:when>
									<c:otherwise>
										전자세금계산서 
									</c:otherwise>
								</c:choose>
							</span>
						</td>
						<td width="162" rowspan="2" align="left" style="border-right: 1px solid #3b60ac; border-left: 1px solid #3b60ac; text-align: center; color: #3b60ac;">
								책&nbsp; 번&nbsp; 호<br /> 일련&nbsp; 번호
						</td>
						<td align="right" style="height:16px; border-right: 1px solid #3b60ac; border-bottom: 1px solid #3b60ac; padding-right:5px;">
							<c:out value="${taxHeader.ZVLID}" />권
						</td>
						<td align="right" style="height:16px; border-bottom: 1px solid #3b60ac; padding-right:5px;">
							<c:out value="${taxHeader.ZBKID}" />호
						</td>
					</tr>
					<tr>
						<td colspan="2" align="right">
							<c:out value="${taxHeader.ZSQNO}" /> <!-- 일련번호 -->
						</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" style="border: 1px solid #3b60ac; font-size: 12px; font-family: 맑은 고딕;">
					<tr>
						<td width="50" rowspan="5" class="" style="border-right: 1px solid #3b60ac; text-align: center; color: #3b60ac;" >
							<span>공<br /> <br /> 급<br /> <br /> 자 </span>
						</td>
						<td width="100"  height="30" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
							등록번호
						</td>
						<td width="160" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; padding-left:8px;">
							<c:out value="${taxHeader.SUPNO}" /> <!-- 공급사업자 사업자등록번호 -->
						</td>
						<td width="100" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
							종사업장 번호
						</td>
						<td align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; padding-left:8px;">
	<%-- 						<c:out value="${taxHeader.ZSPBP}" /> <!-- 종사업장 번호 --> --%>
						</td>
						<td width="50" rowspan="5" class="" style="border-right: 1px solid #3b60ac; text-align: center; color: #3b60ac;" >
							<span>공<br /> 급<br />받<br />는<br /> 자 </span>
						</td>
						<td width="100" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
							등록번호
						</td>
						<td width="160" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; padding-left:8px;">
							<c:out value="${taxHeader.BUYNO}" /> <!-- 등록 번호 -->
						</td>
						<td width="100"  align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
							종사업장 번호
						</td>
						<td align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; padding-left:8px;">
	<%-- 						<c:out value="${taxHeader.ZBKBP}" /> <!-- 종사업장 번호 --> --%>
						</td>
					</tr>
					<tr>
						<td height="30" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
							상&nbsp; &nbsp; 호
						</td>
						<td style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; padding-left:8px;">
							<c:out value="${taxHeader.ZSPCN}" /> <!-- 공급자상호 -->
						</td>
						<td align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
							성&nbsp; &nbsp; 명
						</td>
						<td style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; padding-left:8px;">
							<c:out value="${taxHeader.ZSPNM}" /> <!-- 공급자성명 -->
						</td>
						<td align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
							상&nbsp; &nbsp; 호
						</td>
						<td style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; padding-left:8px;">
							<c:out value="${taxHeader.ZBYCN}" /> <!-- 공급받는자상호 -->
						</td>
						<td align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
							성&nbsp; &nbsp; 명
						</td>
						<td style="border-bottom: 1px solid #3b60ac; padding-left:8px;">
							<c:out value="${taxHeader.ZBYNM}" /> <!-- 공급받는자성명 -->
						</td>
					</tr>
					<tr>
						<td height="30"  align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
								사업장 주소
						</td>
						<td colspan="3" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; padding-left:8px;">
							<c:out value="${taxHeader.ZSPAR}" /> <!-- 공급자 사업장 주소 -->
						</td>
						<td align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
								사업장 주소
						</td>
						<td colspan="3" style="border-bottom: 1px solid #3b60ac; padding-left:8px;">
							<c:out value="${taxHeader.ZBYAR}" /> <!-- 공급받는자 사업장 주소 -->
						</td>
					</tr>
					<tr>
						<td height="30"  align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
							업 &nbsp; &nbsp;종
						</td>
						<td width="141" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; padding-left:8px;">
							<c:out value="${taxHeader.ZSPBT}" /> <!-- 공급자 업종 -->
						</td>
						<td align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
							업 &nbsp; &nbsp;태
						</td>
						<td width="141" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; padding-left:8px;">
							<c:out value="${taxHeader.ZSPIT}" /> <!-- 공급자 업태 -->
						</td>
						<td align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
							업 &nbsp; &nbsp;종
						</td>
						<td width="141" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; padding-left:8px;">
							<c:out value="${taxHeader.ZBYBT}" /> <!-- 공급받는자 업종 -->
						</td>
						<td  align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
							업 &nbsp; &nbsp;태
						</td>
						<td width="141" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; padding-left:8px;">
							<c:out value="${taxHeader.ZBYIT}" /> <!-- 공급받는자 업태 -->
						</td>										
					</tr>
					<tr>
						<td height="30"  align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
								이 &nbsp;메 &nbsp;일
						</td>
						<td colspan="3" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; padding-left:8px;">
							<c:out value="${taxHeader.ZSPRE}" /> <!-- 공급자 이메일 -->
						</td>
						<td align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
								이 &nbsp;메 &nbsp;일
						</td>
						<td colspan="3" style="border-bottom: 1px solid #3b60ac; padding-left:8px;">
							<c:out value="${taxHeader.ZBYRE}" /> <!-- 공급받는자 이메일 -->
						</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="3" style="border: 1px solid #3b60ac; font-size: 11px; font-family: 맑은 고딕;">
					<tr>
						<td width="27"colspan="3" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
							작&nbsp; 성&nbsp; 일
						</td>
						<td width="190" colspan="12" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
							공&nbsp; 급 &nbsp; 가 &nbsp; 액
						</td>
						<td width="" colspan="10" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">
							세&nbsp; &nbsp; 액
						</td>
						<td width="204" align="center" style="border-bottom: 1px solid #3b60ac; color: #3b60ac;">
							비 &nbsp; &nbsp;고
						</td>
					</tr>
					<tr>
						<td width="27"align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">년</td>
						<td width="14"  align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">월</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">일</td>
						<td width="36" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">공란수</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">백</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">십</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">억</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">천</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">백</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">십</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">만</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">천</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">백</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">십</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">일</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">십</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">억</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">천</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">백</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">십</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">만</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">천</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">백</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">십</td>
						<td width="14" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">일</td>
						<td width="204" rowspan="2" style="text-align: center"><c:out value="${taxHeader.ZREMK}" /></td>
					</tr>
					<tr>
						<td align="center" style="border-right: 1px solid #3b60ac;">
							<c:out value="${fn:substring(taxHeader.ZMKDT,0,4)}" /> <!-- 작성일 년 -->
						</td>
						<td align="center" style="border-right: 1px solid #3b60ac;">
							<c:out value="${fn:substring(taxHeader.ZMKDT,5,7)}" /> <!-- 작성일 월 -->
						</td>
						<td align="center" style="border-right: 1px solid #3b60ac;">
							<c:out value="${fn:substring(taxHeader.ZMKDT,8,10)}" /> <!-- 작성일 일 -->
						</td>
						<td align="center" style="border-right: 1px solid #3b60ac;">
							<c:out value="${taxHeader.ZNCNT}" /><!-- 공란수 -->
						</td>
						<!-- 공급가액 출력 시작 -->
						<c:set var="hwbas" value="${fn:split(taxHeader.HWBAS,'.')[0] }" />
						<c:set var="hwbasLength" value="${fn:length(hwbas)}" />
						<c:forEach var="i" begin="0" end="10" step="1">
							<c:set var="inverseIndex" value="${10-i }" />
							<td align="center" style="border-right: 1px solid #3b60ac;">
								<c:choose>
									<c:when test="${inverseIndex < hwbasLength }">
										<c:out value="${fn:substring(hwbas, hwbasLength-inverseIndex-1, hwbasLength-inverseIndex)}" />
									</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>
							</td>
						</c:forEach>
						<!-- 공급가액 출력 끝 -->
						
						<!-- 세액 출력 시작 -->
						<c:set var="hwste" value="${fn:split(taxHeader.HWSTE,'.')[0] }" />
						<c:set var="hwsteLength" value="${fn:length(hwste) }" />
						<c:forEach var="i" begin="0" end="9" step="1">
							<c:set var="inverseIndex" value="${9-i }" />
							<td align="center" style="border-right: 1px solid #3b60ac;">
								<c:choose>
									<c:when test="${inverseIndex < hwsteLength }">
										<c:out value="${fn:substring(hwste, hwsteLength-inverseIndex-1, hwsteLength-inverseIndex) }"></c:out>
									</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>
							</td>
						</c:forEach>
						<!-- 세액 출력 끝 -->
					</tr>
				</table>
				<!-- 아이템 내역 시작 -->
				<table width="100%" border="0" cellspacing="0" cellpadding="4" style="border: 1px solid #3b60ac; font-size: 11px; font-family: 맑은 고딕;">
					<tr>
						<td width="23" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">월</td>
						<td width="25" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">일</td>
						<td width="257" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">품목</td>
						<td width="55" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">규격</td>
						<td width="54" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">수량</td>
						<td width="57" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">단가</td>
						<td width="86" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">공급가액</td>
						<td width="55" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">세액</td>
						<td width="56" align="center" style="border-bottom: 1px solid #3b60ac; color: #3b60ac;">비고</td>
					</tr>
						<c:forEach items="${taxItems }" var="taxItem">
						<tr>
							<td align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac;">&nbsp;
								<c:out value="${fn:substring(taxItem.ZRQDT,5,7) }" /> <!-- 월 -->
							</td>
							<td align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac;">&nbsp;
								<c:out value="${fn:substring(taxItem.ZRQDT,8,10) }" /> <!-- 일 -->
							</td>
							<td align="left" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; padding-left:5px;">&nbsp;
								<c:out value="${taxItem.ZITEM }" /> <!-- 품목 -->
							</td>
							<td align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac;">&nbsp;
								<c:out value="${taxItem.ZSIZE }" /> <!-- 규격 -->
							</td>
							<td align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac;">
								<c:set var="str1" value="${taxItem.ZREQT}" /> <!-- 수량 -->
								<c:set var="str2" value="${fn:substringBefore(str1 , '.')}" />&nbsp;${str2}
							</td>
							<td align="center" class="" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac;">&nbsp;
								<fmt:formatNumber type="number" value="${taxItem.ZNTAT}" /><!-- 단가 -->
							</td>
							<td align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac;">&nbsp;
								<fmt:formatNumber type="number" value="${taxItem.HWBAS}" /> <!-- 공급가액 -->
							</td>
							<td align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac;">&nbsp;
								<fmt:formatNumber type="number" value="${taxItem.HWSTE}" /> <!-- 세액 -->
							</td>
							<td align="center" style="border-bottom: 1px solid #3b60ac;"><c:out value="${taxItem.ZREMK }" /></td> <!-- 비고 -->
						</tr>
					</c:forEach>
				</table>
				<!-- 아이템 내역 끝 -->
				<table width="100%" border="0" cellspacing="0" cellpadding="4" style="border: 1px solid #3b60ac; font-size: 11px; font-family: 맑은 고딕;">
					<tr>
						<td width="92" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">총금액</td>
						<td width="79" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">현금</td>
						<td width="80" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">수표</td>
						<td width="76" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">어음</td>
						<td width="77" align="center" style="border-bottom: 1px solid #3b60ac; border-right: 1px solid #3b60ac; color: #3b60ac;">외상미수금</td>
						<td width="294" rowspan="2" align="center" style="color: #3b60ac;">
							위 금액을 &nbsp;
							<c:choose>
								<c:when test ="${taxHeader.ZRPTP eq '01'}">
									영수&nbsp;
								</c:when>
								<c:otherwise>
									청구&nbsp;
								</c:otherwise>
							</c:choose>			
							함
						</td>
					</tr>
					<tr>
						<td align="center" class="" style="border-right: 1px solid #3b60ac;">
							<fmt:formatNumber type="number" value="${taxHeader.ZTTAT }" /> <!-- 총금액 -->
						</td>
						<td align="right" style="border-right: 1px solid #3b60ac;">&nbsp;</td> <!-- 현금 -->
						<td align="right" style="border-right: 1px solid #3b60ac;">&nbsp;</td> <!-- 수표 -->
						<td align="right" style="border-right: 1px solid #3b60ac;">&nbsp;</td> <!-- 어음 -->
						<td align="right" style="border-right: 1px solid #3b60ac;">&nbsp;</td> <!-- 외상미수금 -->
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<!-- 세금계산서 테이블 -->
</body>
</html>