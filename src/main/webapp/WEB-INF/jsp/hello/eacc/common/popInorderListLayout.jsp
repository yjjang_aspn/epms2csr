<%@ page language="java"  contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="InorderList"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"	NoFormatEscape="1"
			NumberId="1"          	DateStrings="2"     SelectingCells="0"  AcceptEnters="1"        Style="Office"
			InEditMode="2"        	SafeCSS='1'         NoVScroll="0"       NoHScroll="0"           
			Filtering="1"        	Dragging="0"        Selecting="0" 		Deleting="0"			Editing="1"
			CopySelected="0" 		CopyFocused="1"  	CopyCols="0"		ExportFormat="xls" ExportCols="0"
			AllPages="0"
	/>
	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>
	<Cfg Paging="2" PageLength="10" PageMin="2" Pages="10" PagingFixed="0"/>
	<Header	id="Header"	Align="center"
			COMPANY_ID	= "회사코드"
			O_YEAR      = "회계년도"
			AUFNR       = "오더코드"
			AUART       = "오더유형"
			AUTXT       = "오더명"
			INDATE      = "생성일"
			INTIME      = "생성시간"
	/>
	
	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<Solid>
		<I id="PAGER" Cells="NAV,LIST,ONE,GROUP" Space="4"
		 NAVType="Pager"
		 LISTType="Pages" LISTRelWidth="1" LISTAlign="left" LISTLeft="10"
		 ONEType="Bool" ONEFormula="Grid.AllPages?0:1" ONECanEdit="1" ONELabelRight="페이지단위로보임"
		 ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
		 GROUPCanFocus="0"
		 />
	</Solid>
	<Pager Visible="0"/>
	
	<Cols>
		<C Name="COMPANY_ID"  	Type="Text"  	RelWidth="100"     Align="center"	CanEdit="0"		Visible="0" /><!-- 회사코드 -->	
		<C Name="O_YEAR"  		Type="Text"	    RelWidth="100"     Align="center"	CanEdit="0" 	Visible="1" /><!-- 오더년도 --> 
		<C Name="AUFNR"  		Type="Text"     RelWidth="100"     Align="center"	CanEdit="0" 	Visible="1" /><!-- 오더코드 -->  
		<C Name="AUART"	    	Type="Text"  	RelWidth="100"     Align="center"	CanEdit="0"		Visible="1" /><!-- 오더유형 --> 
		<C Name="AUTXT"	    	Type="Text"  	RelWidth="100"     Align="left"		CanEdit="0"		Visible="1" /><!-- 오더명 --> 
		<C Name="INDATE"	    Type="Date"  	RelWidth="100"     Align="center"	CanEdit="0"		Visible="0" /><!-- 생성일 --> 
		<C Name="INTIME"  		Type="Text"	    RelWidth="100"     Align="center"	CanEdit="0" 	Visible="0" /><!-- 생성시간 --> 
	</Cols>
	
	<Toolbar	Space="0"	Styles="1"	Cells="Empty,Cnt,Found,새로고침"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType = "Button"
				CntType = "Html" CntFormula = '"총 : &lt;b>"+count(7)+"&lt;/b>행"' CntWidth = '-1' CntWrap = '0'
				FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>행'" FoundWidth = '-1' FoundWrap = '0'
				새로고침Type = "Html" 새로고침 = "&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
									  onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
	/>
</Grid>