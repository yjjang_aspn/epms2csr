<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<Grid>
<Body>
	<B>
	<c:forEach var="item" items="${vendorList}">
		<I
			LIFNR		= "${item.LIFNR}"
			NAME1	    = "${item.NAME1}"
			BANKL 	    = "${item.BANKL}"
			BANKA     	= "${item.BANKA}"
			BANKN       = "${item.BANKN}"
			KOINH       = "${item.KOINH}"
			ZTERM       = "${item.ZTERM}"
		/>
	</c:forEach> 
	</B>
</Body>
</Grid>
