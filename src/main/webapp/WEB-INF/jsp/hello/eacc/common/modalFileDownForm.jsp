<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : modalFileDownForm.jsp
  Description : 첨부파일 다운로드 모달 폼
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.08.29    정순주              최초 생성

    author   : 정순주
    since    : 2017.08.29
--%>
<script src="/js/jquery/jquery-filestyle.js" type="text/javascript" ></script>
<script src="/js/jquery/jquery.MultiFile.js" type="text/javascript"></script>
<script src="/js/jquery/jquery.ajaxsubmit.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){
	
	//파일 다운로드
	$('.btnFileDwn').on('click', function(){
		var ATTACH = $(this).data("attach");
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO=${param.ATTACH_GRP_NO}&ATTACH='+ATTACH;
		$("#fileDownFrame2").attr("src",src);
	});
	
	var fileList = new Array();
	<c:forEach items="${fileList}" var="item" >
		var obj = new Object();
		
		obj.ATTACH_GRP_NO = "${item.ATTACH_GRP_NO}";
		obj.ATTACH = "${item.ATTACH}";
		obj.FORMAT = "${item.FORMAT}";
		
		fileList.push(obj);
	</c:forEach>
	var attachImgCnt = 0;
	for(var i=0; i<fileList.length; i++){
		if(fileList[i].FORMAT == "jpg" || fileList[i].FORMAT == "png" || fileList[i].FORMAT == "jpeg" || fileList[i].FORMAT == "JPG" || fileList[i].FORMAT == "PNG"){
			attachImgCnt ++;
		}
	}	
	
	if(attachImgCnt == 0){
		$('#previewImg').text('첨부된 이미지 파일이 존재하지 않습니다.');
	}
	
});

/* 이미지 원본크기 보기 */
function originFileView(img){ 
	img1= new Image(); 
	img1.src=(img); 
	imgControll(img); 
} 
	  
function imgControll(img){ 
	if((img1.width!=0)&&(img1.height!=0)){ 
		viewImage(img); 
	}else{ 
		controller="imgControll('"+img+"')"; 
		intervalID=setTimeout(controller,20); 
	} 
}

function viewImage(img){ 
	W=img1.width; 
	H=img1.height; 
	O="width="+W+",height="+H+",scrollbars=yes"; 
	imgWin=window.open("","",O); 
	imgWin.document.write("<html><head><title>이미지상세보기</title></head>");
	imgWin.document.write("<body topmargin=0 leftmargin=0>");
	imgWin.document.write("<img src="+img+" onclick='self.close()' style='cursor:pointer;' title ='클릭하시면 창이 닫힙니다.'>");
	imgWin.document.close();
}

</script>

	<div class="modal-dialog root wd-per-40"> <!-- 원하는 width 값 입력 -->
        <div class="modal-content" style="height: 500px;">
			<div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
               	<h4 class="modal-title">첨부파일 다운로드</h4>
            </div>
            <div class="modal-body" >
				<div class="modal-bodyIn" >

					<!-- s: form area -->
					<form id="fileFrm" name="fileFrm" method="post" enctype="multipart/form-data">
						<!-- s: input area -->
						<input type="hidden" id="MODULE" name="MODULE" value="${param.MODULE }"/>
						<input type="hidden" id="ATTACH_GRP_NO" name="ATTACH_GRP_NO" value=""/>
						<input type="hidden" id="DEL_SEQ" name="DEL_SEQ" value=""/>
						<!-- e: input area -->
							
						<div class="inq-area-top" style="width:100%">
							<div class="bxType01 fileWrap previewFileLst">
								<div>
									<div id="attachFile0" class="attachFile" style="height:0px;"></div>
									<div class="fileList type02">
										<div id="detailFileList0" class="fileDownLst">
											<c:forEach var="item" items="${fileList}" varStatus="idx">
												<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
<!-- 													<span class="MultiFile-remove"> -->
<%-- 														<a href="#none" class="icn_fileDel" data-attach="${item.ATTACH}" data-idx="${item.CD_FILE_TYPE }"> --%>
<!-- 															<img src="/images/com/web/btn_remove.png" /> -->
<!-- 														</a> -->
<!-- 													</span>  -->
													<span class="MultiFile-export">${item.SEQ_DSP}</span> 
													<span class="MultiFile-title" title="File selected: ${item.NAME}">
													${item.NAME}(${item.FILE_SIZE}kb)
													</span>
													<a href="#none" class="btnFileDwn icon download" data-attach="${item.ATTACH}">다운로드</a>
												</div>		
											</c:forEach>
										</div>
									</div>
								</div>
							</div>
						</div>
			
						<div class="fl-box panel-wrap04" style="width:100%;"><!-- 원하는 비율로 직접 지정하여 사용 -->
							<div class="panel-body">
								<div class="previewFile">
									<h3 class="tit-cont preview" id="previewImg">이미지 미리보기</h3>
									<ul>
										<c:forEach var="item" items="${fileList}" varStatus="idx">
											<c:if test = "${item.FORMAT eq 'jpg' || item.FORMAT eq 'png' || item.FORMAT eq 'jpeg' || item.FORMAT eq 'JPG' || item.FORMAT eq 'PNG'}">
												<li>
													<img src="/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}" title="클릭하시면 원본크기로 보실 수 있습니다." onclick="originFileView('/attach/fileDownload.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH=${item.ATTACH}')" />
													<p>${item.NAME}</p>
												</li>
											</c:if>
										</c:forEach>
									</ul>
								</div>
							</div>
						</div>
						
						<iframe id="fileDownFrame2" style="width:0px;height:0px;border:0px"></iframe>
					</form>
					
				</div>
            </div>
        </div>
    </div>
