<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>

<%--
  Class Name : popNeedBill.jsp
  Description : 레이어 팝업 모음 (코스트센터, GL계정, 구매처) Pop-Up 
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.08.25    정호윤              최초 생성

    author   : 정호윤
    since    : 2017.08.25
--%>

<!-- Layer PopUp Start -->
<!-- 구매처-->
<div class="modal fade" id="vendorModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog root wd-per-60"> <!-- 원하는 width 값 입력 -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
               	<h4 class="modal-title">구매처 조회</h4>
            </div>
            <div class="modal-body" >
            	<div class="modal-bodyIn">
            	
            		<div class="modal-section">
						<div class="inq-area-top">
							<!-- s:inq-area01 -->
							<ul class="wrap-inq">
								<li class="inq-clmn">
									<h4 class="tit-inq">조회</h4>
									<div class="sel-wrap type02" style="width: 120px;">
										<select title="조회선택" id="searchGubun" name="searchGubun">
											<option value="">조건 선택</option>
											<c:choose>
												<c:when test="${billGb eq 'card' }">
													<option value="card_num">카드 번호</option>
												</c:when>
												<c:when test="${billGb eq 'tax' }">
													<option value="corporation_no">사업자 번호</option>
												</c:when>						
												<c:otherwise>
													<option value="user_id">사원 번호</option>
												</c:otherwise>
											</c:choose>
											<option value="name">구매처 명</option>
										</select>
									</div>
								</li>
							</ul>
							<div class="inp-wrap">
								<input type="text" class="inp-comm wd-px-150" id="searchGubunVal" name="searchGubunVal" maxlength="30" onclick="Grids.Focused = null;"/>
							</div>
							<a href="#none" class="btn comm st01" id="search" onclick="javascript:fn_searchLifnr(); return false;">검색</a>
						</div>
						<!-- e:inq-area01 -->				
						 
						 <!-- START -->
						 <div class="fl-box wd-per-100 "><!-- 원하는 비율로 직접 지정하여 사용 -->
							<!-- 트리그리드 : s -->
							<div id="VendorList">
								<bdo	Debug="Error"
										Layout_Url="/hello/eacc/common/popVendorListLayout.do"
								>
								</bdo>
							</div>
							<!-- 트리그리드 : e -->
						</div>
						<!-- END -->
					 </div>
					 
            	</div>
            </div>
        </div>
    </div>
</div>
<!-- 구매처 ->
<!-- Layer PopUp End -->

<!-- Layer PopUp Start -->
<!-- 코스트센터 -->
<div class="modal fade" id="costcenterModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog root wd-per-40"> <!-- 원하는 width 값 입력 -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
               	<h4 class="modal-title">코스트센터 조회</h4>
            </div>
            <div class="modal-body" >
            	<div class="modal-bodyIn">
					 
					 <div class="modal-section">
						 <!-- START -->
						 <div class="fl-box wd-per-100" ><!-- 원하는 비율로 직접 지정하여 사용 -->
							<!-- 트리그리드 : s -->
							<div id="CostcenterList">
								<bdo	Debug="Error"
										Layout_Url="/hello/eacc/common/popCostcenterListLayout.do"
								>
								</bdo>
							</div>
							<!-- 트리그리드 : e -->
						</div>
						<!-- END -->
					 </div>
					 
            	</div>
            </div>
        </div>
    </div>
</div>
<!-- 코스트센터 -->
<!-- Layer PopUp End -->

<!-- Layer PopUp Start -->
<!-- GL 계정 -->
<div class="modal fade" id="glaccountModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog root wd-per-40"> <!-- 원하는 width 값 입력 -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
               	<h4 class="modal-title">계정 조회</h4>
            </div>
            <div class="modal-body" >
            	<div class="modal-bodyIn">
					 
					 <div class="modal-section">
						 <div class="inq-area-top">
							<!-- s:inq-area01 -->
							<ul class="wrap-inq">
								<li class="inq-clmn">
									<h4 class="tit-inq">조회</h4>
									<div class="sel-wrap type02" style="width: 120px;">
										<select title="계정 분류 선택" id="searchBunryu" name="searchBunryu">
											<option value="">계정 분류 선택</option>
										</select>
									</div>
								</li>
							</ul>
						</div>
					
						 <!-- START -->
						 <div class="fl-box wd-per-100"><!-- 원하는 비율로 직접 지정하여 사용 -->
							<!-- 트리그리드 : s -->
							<div id="GlAccountList">
								<bdo	Debug="Error"
										Layout_Url="/hello/eacc/common/popGlAccountListLayout.do"
								>
								</bdo>
							</div>
							<!-- 트리그리드 : e -->
						</div>
						<!-- END -->
						
					 </div>
            	</div>
            </div>
        </div>
    </div>
</div>
<!-- GL 계정 -->
<!-- Layer PopUp End -->

<!-- Layer PopUp Start -->
<!-- 내부오더 -->
<div class="modal fade" id="InorderModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog root wd-per-40"> <!-- 원하는 width 값 입력 -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
               	<h4 class="modal-title">오더 조회</h4>
            </div>
            <div class="modal-body" >
            	<div class="modal-bodyIn">
					 
					 <div class="modal-section">
						 <!-- START -->
						 <div class="fl-box wd-per-100"><!-- 원하는 비율로 직접 지정하여 사용 -->
							<!-- 트리그리드 : s -->
							<div id="InorderList">
								<bdo	Debug="Error"
										Layout_Url="/hello/eacc/common/popInorderListLayout.do"
								>
								</bdo>
							</div>
							<!-- 트리그리드 : e -->
						</div>
						<!-- END -->
						
					 </div>
            	</div>
            </div>
        </div>
    </div>
</div>
<!-- 내부오더 -->
<!-- Layer PopUp End -->