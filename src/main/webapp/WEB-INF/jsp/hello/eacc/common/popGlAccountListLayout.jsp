<%@ page language="java"  contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:set var="gridId" value="GlAccountList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			AcceptEnters   = "1" Calculated = "1"  CalculateSelected = "1"      DateStrings = "2" Deleting  = "0"
			Dragging       = "0" Editing    = "1"  EnterMode         = "4"      Filtering   = "1" IdChars   = "0123456789"
			InEditMode     = "2" MaxPages   = "20" NoFormatEscape    = "1"      NoHScroll   = "0" NoVScroll = "0"
			NumberId       = "1" SafeCSS    = '1'  Selecting		 = "0"
			SelectingCells = "0" SortIcons  = "0"  Style             = "Office" SuppressCfg = "1" 
			Paging		   = "2" AllPages	= "0"  PageLength		 = "20"		MaxPages	= "20"
			CopySelected   = "0" CopyFocused= "1"  CopyCols			 = "0"		ExportFormat="xls" ExportCols="0"
	/>
	
	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>
	
	<Header	id="Header"	Align="center"
			GLCODE			="계정코드"	
			GLGROUP	    	="계정그룹"
			GLNAME 	    	="계정명"
			GLANME_DETAIL 	="계정설명"
			COMPANY 	    ="회사코드"
			GLTYPE 	    	="계정타입"
	/>
	
	<!-- Fileter Setting Area Start      -->
	<Head>
		<Filter	id = "Filter" CanFocus = "1"/>
	</Head>
	<!-- Fileter Setting Area End        -->

	<Cols>
		<C Name="GLCODE"	    	Type="Text"  	RelWidth="100"     Align="center"	CanEdit="0"		Visible="1" /><!-- 계정코드 -->	
		<C Name="GLGROUP"	    	Type="Text"	    RelWidth="100"     Align="left"		CanEdit="0" 	Visible="0" /><!-- 계정그룹 --> 
		<C Name="GLNAME"	    	Type="Text"     RelWidth="200"     Align="left"		CanEdit="0" 	Visible="1" /><!-- 계정명 -->  
		<C Name="GLANME_DETAIL"	    Type="Text"  	RelWidth="100"     Align="center"	CanEdit="0"		Visible="0" /><!-- 계정설명 --> 
		<C Name="COMPANY"	    	Type="Text"	    RelWidth="100"     Align="center"	CanEdit="0" 	Visible="0" /><!-- 회사코드 --> 
		<C Name="GLTYPE"	    	Type="Text"     RelWidth="100"     Align="center"	CanEdit="0" 	Visible="0" /><!-- 계정타입 --> 
	</Cols>
	
	<!-- Grid Paging Setting Area Start  -->
	<Pager Visible = "0"/>

	<Solid>
		<I id = "PAGER" Cells = "NAV,LIST,ONE,GROUP" Space = "4"
		   NAVType	     = "Pager"
		   LISTType	     = "Pages" LISTAlign   = "left" LISTLeft   = "10"                LISTRelWidth  = "1"
		   ONEType       = "Bool"  ONECanEdit  = "1"    ONEFormula = "Grid.AllPages?0:1" ONELabelRight = "페이지단위로보임"
		   ONEOnChange = "Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
		   GROUPCanFocus = "0"
		/>
	</Solid>
	<!-- Grid Paging Setting Area End    -->
	
	<Toolbar	Space="0"	Styles="1"	Cells="Empty,Cnt,Found,새로고침"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType = "Button"
				CntType = "Html" CntFormula = '"총 : &lt;b>"+count(7)+"&lt;/b>행"' CntWidth = '-1' CntWrap = '0'
				FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>행'" FoundWidth = '-1' FoundWrap = '0'
				새로고침Type = "Html" 새로고침 = "&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
									  onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
	/>
</Grid>