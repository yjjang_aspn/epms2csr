<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : cardReceipt.jsp
  Description : 법인카드 증빙 영수증
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.08.09    정순주              최초 생성

    author   : 정순주
    since    : 2017.08.09
--%>
	
	<div class="cardReceiptWrap">
		<div class="cardReceiptWrapIn">
			<div class="cardReceiptTit">
					${cardData.MERC_NAME}
			</div>
			<div class="cardReceiptCon">
				<table width="100%">
					<tr>
						<td style="padding-bottom:15px;'">
							<!-- ①사업자정보 start -->
							<table width="100%">
								<tr>
									<td colspan="2">[매장명] <c:out value="${cardData.MERC_NAME}" /></td>
								</tr>
								<tr>
									<td colspan="2">[사업자] <c:out value="${cardData.MERC_SAUP_NO}" /></td>
								</tr>
								<tr>
									<td colspan="2">[주   소] <c:out value="${cardData.MERC_ADDR}" /></td>
								</tr>
								<tr>
									<td class="t-l">[대표자] <c:out value="${cardData.MERC_REPR}" /></td>
									<td class="t-r">[TEL] <c:out value="${cardData.MERC_TEL}" /></td>
								</tr>
								<tr>
									<td colspan="2">[승인일] <c:out value="${cardData.AUTH_DATE}" />&nbsp;
										<c:out value="${fn:substring(cardData.AUTH_TIME, 0, 2)} : ${fn:substring(cardData.AUTH_TIME, 2, 4)} : ${fn:substring(cardData.AUTH_TIME, 4, 6)}" /></td>
								</tr>
							</table>
							<!-- ①사업자정보 end -->
						</td>
					</tr>
					<tr>
						<td style="border-top:3px dashed #808080; padding:15px 0;">
							<!-- ②금액정보 start -->
							<table width="100%">
								<tr>
									<td style="padding-bottom:15px;">
										<table width="100%">
											<tr>
												<td class="t-l" style="font-size:14px; font-weight:500;">공급가액</td>
												<c:set var="suprice"><fmt:formatNumber value="${cardData.AMT_AMOUNT}"></fmt:formatNumber></c:set>
												<c:set var="supriceLength" value="${fn:length(suprice)}" />
												<c:forEach var="i" begin="0" end="7" step="1">
													<c:set var="inverseIndex" value="${7-i}" />
													<td class="t-r" style="font-size:14px; font-weight:500; width:8px;">
														<c:choose>
															<c:when test="${inverseIndex<supriceLength}">
																<c:out value="${fn:substring(suprice,supriceLength-inverseIndex-1,supriceLength-inverseIndex)}" />
															</c:when>
															<c:otherwise></c:otherwise>
														</c:choose>
													</td>
												</c:forEach>
											</tr>
											<tr>
												<td class="t-l" style="font-size:14px; font-weight:500;">부가세액</td>
												<c:set var="vat"><fmt:formatNumber value="${cardData.VAT_AMOUNT}"></fmt:formatNumber></c:set>
												<c:set var="vatLength" value="${fn:length(vat)}" />
												<c:forEach var="i" begin="0" end="7" step="1">
													<c:set var="inverseIndex" value="${7-i}" />
													<td class="t-r" style="font-size:14px; font-weight:500; width:8px;">
														<c:choose>
															<c:when test="${inverseIndex<vatLength}">
																<c:out value="${fn:substring(vat,vatLength-inverseIndex-1,vatLength-inverseIndex)}" />
															</c:when>
															<c:otherwise></c:otherwise>
														</c:choose>
													</td>
												</c:forEach>
											</tr>
											<tr>
												<td class="t-l" style="font-size:14px; font-weight:500;">봉사료</td>
												<c:set var="tips"><fmt:formatNumber value="${cardData.SER_AMOUNT}"></fmt:formatNumber></c:set>
												<c:set var="tipsLength" value="${fn:length(tips)}" />
												<c:forEach var="i" begin="0" end="7" step="1">
													<c:set var="inverseIndex" value="${7-i}" />
													<td class="t-r" style="font-size:14px; font-weight:500; width:8px;">
														<c:choose>
															<c:when test="${inverseIndex<tipsLength}">
																<c:out
																	value="${fn:substring(tips,tipsLength-inverseIndex-1,tipsLength-inverseIndex)}" />
															</c:when>
															<c:otherwise></c:otherwise>
														</c:choose>
													</td>
												</c:forEach>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style="border-top:3px dashed #808080; padding-top:15px;">
										<table width="100%">
											<tr>
												<td width="" class="t-l" style="font-size:18px; font-weight:600;">총 금액</td>
												<c:set var="orgnapprtot"><fmt:formatNumber value="${cardData.TOT_AMOUNT}"></fmt:formatNumber></c:set>
												<c:set var="orgnapprtotLength" value="${fn:length(orgnapprtot)}" />
												<c:forEach var="i" begin="0" end="7" step="1">
													<c:set var="inverseIndex" value="${7-i}" />
													<td width=""  class="t-r" style="font-size:18px; font-weight:600; width:8px;">
														<c:choose>
															<c:when test="${inverseIndex<orgnapprtotLength}">
																<c:out value="${fn:substring(orgnapprtot,orgnapprtotLength-inverseIndex-1,orgnapprtotLength-inverseIndex)}" />
															</c:when>
															<c:otherwise></c:otherwise>
														</c:choose>
													</td>
												</c:forEach>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<!-- ②금액정보 end -->
						</td>
					</tr>
					<tr>
						<td style="border-top:3px dashed #808080; padding-top:15px;">
							<!-- ③카드정보 start -->
							<table width="100%">
								<tr>
									<td style="padding-bottom:15px;">
										<table width="100%">
											<tr>
												<td width="" class="t-l" style="font-size:14px; font-weight:500;">승인번호</td>
												<td width=""  class="t-r" style="font-size:14px; font-weight:500;"><c:out value="${cardData.AUTH_NUM}" /></td>
											</tr>
											<tr>
												<td width="" class="t-l" style="font-size:14px; font-weight:500;">하나카드</td>
												<td width=""  class="t-r" style="font-size:14px; font-weight:500;">
													<c:set var="str1" value="${cardData.CARD_NUM}"/>
													<c:set var="str2" value="${fn:substring(str1, 4 , 12)}" />
													<c:set var="str3" value="${fn:substring(str1, 14 , 16)}" />
													****<c:out value="${str2}" />**<c:out value="${str3}" />				
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<!-- ③카드정보 end -->
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	
	
	
	
<%-- 	<table width="95%" border="0" cellspacing="0" cellpadding="0" style="background:url(/sys/images/web/cardbg2.png) repeat">
		<tr>
			<td align="center"><br />
				<table width="100%" border="0" cellspacing="0" cellpadding="3" style="border: 1px solid #949494; font-size: 11px; font-family: 맑은 고딕;">
					<tr>
						<td width="50%" bgcolor="#e3e3e3" style="border-bottom:1px dotted #949494; border-right:1px solid #949494;">카드종류</td>
						<td bgcolor="#e3e3e3" style="border-bottom:1px dotted #949494;">카드번호</td>
					</tr>
					<tr>
						<td align="center" style="border-right:1px solid #949494;">${cardData.CARD_KIND}</td>
						<td align="center">
							<c:set var="str1" value="${cardData.CARD_NUM}"/>
							<c:set var="str2" value="${fn:substring(str1, 4 , 12)}" />
							<c:set var="str3" value="${fn:substring(str1, 14 , 16)}" />
							****<c:out value="${str2}" />**<c:out value="${str3}" />				
						</td>
					</tr>
					<tr>
						<td bgcolor="#e3e3e3" style="border-bottom:1px dotted #949494; border-right:1px solid #949494;">유효기간</td>
						<td bgcolor="#e3e3e3" style="border-bottom:1px dotted #949494;">구매자명</td>
					</tr>
					<tr>
						<td align="center" style="border-right:1px solid #949494;"><c:out value="${cardData.AVAIL_TERM}" /></td>
						<td align="center"><c:out value="${cardData.USER_NAME}" /></td>
					</tr>
				</table>
				<!-- ①카드정보 end -->
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><!-- ②전표정보 start -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #949494; font-size:11px; font-family:맑은 고딕;">
								<tr>
									<td height="20" align="center" bgcolor="#65788e"><span style="width:50%; color:#fff; font-weight:bold;">가맹점 정보</span></td>
									<td rowspan="6" align="center" style="width:50%; border-left:1px solid #949494;">
										<table border="0" cellspacing="0" cellpadding="0" bgcolor="#efefef" class="borLeftNone">
											<tr>
												<td height="25" align="center" style="width:*; border-bottom:1px dotted #949494; border-right:1px solid #949494;">&nbsp;</td>
												<td align="center" style="width:10%; border-bottom:1px dotted #949494; border-right:1px solid #949494;">&nbsp;</td>
												<td align="center" style="width:10%; border-bottom:1px dotted #949494; border-right:1px solid #949494;">백</td>
												<td align="center" style="width:10%; border-bottom:1px dotted #949494; border-right:1px solid #949494;">&nbsp;</td>
												<td align="center" style="width:10%; border-bottom:1px dotted #949494; border-right:1px solid #949494;">&nbsp;</td>
												<td align="center" style="width:10%; border-bottom:1px dotted #949494; border-right:1px solid #949494;">천</td>
												<td align="center" style="width:10%; border-bottom:1px dotted #949494; border-right:1px solid #949494;">&nbsp;</td>
												<td align="center" style="width:10%; border-bottom:1px dotted #949494; border-right:1px solid #949494;">&nbsp;</td>
												<td align="center" style="width:10%; border-bottom:1px dotted #949494;">원</td>
											</tr>
											<tr>
												<td height="25" align="center" style="border-bottom:1px dotted #949494; border-right:1px solid #949494;">금액</td>
													<c:set var="suprice">${cardData.AMT_AMOUNT}</c:set>
													<c:set var="supriceLength" value="${fn:length(suprice)}" />
													<c:forEach var="i" begin="0" end="7" step="1">
													<c:set var="inverseIndex" value="${7-i}" />
												<td align="center" style="border-bottom:1px dotted #949494; border-left:1px solid #949494;">
													<c:choose>
														<c:when test="${inverseIndex<supriceLength}">
															<c:out value="${fn:substring(suprice,supriceLength-inverseIndex-1,supriceLength-inverseIndex)}" />
														</c:when>
														<c:otherwise></c:otherwise>
													</c:choose>
												</td>
													</c:forEach>
											</tr>
											<tr>
												<td height="25" align="center" style="border-bottom:1px dotted #949494; border-right:1px solid #949494;">부가세</td>
												<c:set var="vat">${cardData.VAT_AMOUNT}</c:set>
												<c:set var="vatLength" value="${fn:length(vat)}" />
												<c:forEach var="i" begin="0" end="7" step="1">
													<c:set var="inverseIndex" value="${7-i}" />
													<td align="center" style="border-bottom:1px dotted #949494; border-left:1px solid #949494;">
														<c:choose>
															<c:when test="${inverseIndex<vatLength}">
																<c:out value="${fn:substring(vat,vatLength-inverseIndex-1,vatLength-inverseIndex)}" />
															</c:when>
															<c:otherwise></c:otherwise>
														</c:choose>
													</td>
												</c:forEach>
											</tr>
											<tr>
												<td height="25" align="center" style="border-bottom:1px dotted #949494; border-right:1px solid #949494;">봉사료</td>
												<c:set var="tips">${cardData.SER_AMOUNT}</c:set>
												<c:set var="tipsLength" value="${fn:length(tips)}" />
												<c:forEach var="i" begin="0" end="7" step="1">
													<c:set var="inverseIndex" value="${7-i}" />
													<td align="center" style="border-bottom:1px dotted #949494; border-left:1px solid #949494;">
														<c:choose>
															<c:when test="${inverseIndex<tipsLength}">
																<c:out
																	value="${fn:substring(tips,tipsLength-inverseIndex-1,tipsLength-inverseIndex)}" />
															</c:when>
															<c:otherwise></c:otherwise>
														</c:choose>
													</td>
												</c:forEach>
											</tr>
											<tr>
												<td height="25" align="center" style="border-bottom:1px dotted #949494; border-right:1px solid #949494;">합계</td>
												<c:set var="orgnapprtot">${cardData.TOT_AMOUNT}</c:set>
												<c:set var="orgnapprtotLength" value="${fn:length(orgnapprtot)}" />
												<c:forEach var="i" begin="0" end="7" step="1">
													<c:set var="inverseIndex" value="${7-i}" />
													<td align="center" style="border-bottom:1px dotted #949494; border-left:1px solid #949494;">
														<c:choose>
															<c:when test="${inverseIndex<orgnapprtotLength}">
																<c:out value="${fn:substring(orgnapprtot,orgnapprtotLength-inverseIndex-1,orgnapprtotLength-inverseIndex)}" />
															</c:when>
															<c:otherwise></c:otherwise>
														</c:choose>
													</td>
												</c:forEach>
											</tr>
										</table>
									</td>
								</tr>
								<tr align="left">
									<td height="20" bgcolor="#b6c2ce" style="border-bottom:1px dotted #949494; border-top:1px solid #949494;">&nbsp;가맹점명</td>
								</tr>
								<tr align="center">
									<td height="20" bgcolor="#edf2f7"><c:out value="${cardData.MERC_NAME}" /></td>
								</tr>
								<tr align="left" bgcolor="#b6c2ce">
									<td height="20" style="border-bottom:1px dotted #949494; border-top:1px solid #949494;">&nbsp;가맹점 번호</td>
								</tr>
								<tr align="center" bgcolor="#edf2f7">
									<td height="20"><c:out value="${cardData.MERC_TEL}" /></td>
								</tr>
								<tr align="left" bgcolor="#b6c2ce">
									<td height="20" style="border-bottom:1px dotted #949494; border-top:1px solid #949494;">&nbsp;사업자등록번호</td>
								</tr>
								<tr align="center" bgcolor="#edf2f7">
									<td height="20"><c:out value="${cardData.MERC_SAUP_NO}" /></td>
									<td align="center" bgcolor="#7c7992" style="border-left:1px solid #949494;"><span style="color:#fff; font-weight:bold;">거래정보</span></td>
								</tr>
								<tr align="left" bgcolor="#b6c2ce">
									<td height="20" style="border-bottom:1px dotted #949494; border-top:1px solid #949494;">&nbsp;대표자명</td>
									<td bgcolor="#bebdc9" style="border-bottom:1px dotted #949494; border-top:1px solid #949494; border-left:1px solid #949494;">&nbsp;거래일시</td>
								</tr>
								<tr align="center" bgcolor="#edf2f7">
									<td height="20"><c:out value="${cardData.MERC_REPR}" /></td>
									<td bgcolor="#f5f4f6" style="border-left:1px solid #949494;"><c:out value="${cardData.AUTH_DATE}" />&nbsp;<c:out value="${cardData.AUTH_TIME}" /></td>
								</tr>
								<tr align="left" bgcolor="#b6c2ce">
									<td height="20" style="border-bottom:1px dotted #949494; border-top:1px solid #949494;">&nbsp;업태</td>
									<td bgcolor="#bebdc9" style="border-bottom:1px dotted #949494; border-top:1px solid #949494; border-left:1px solid #949494;">&nbsp;사용내역</td>
								</tr>
								<tr align="center" bgcolor="#edf2f7">
									<td height="20"></td>
									<td bgcolor="#f5f4f6" style="border-left:1px solid #949494;"></td>
								</tr>
								<tr align="left" bgcolor="#b6c2ce">
									<td height="20" style="border-bottom:1px dotted #949494; border-top:1px solid #949494;">&nbsp;종목(업종)</td>
									<td bgcolor="#bebdc9" style="border-bottom:1px dotted #949494; border-top:1px solid #949494; border-left:1px solid #949494;">&nbsp;승인번호</td>
								</tr>
								<tr align="center" bgcolor="#edf2f7">
									<td height="20"><c:out value="${cardData.MCC_NAME}" /></td>
									<td bgcolor="#f5f4f6" style="border-left:1px solid #949494;"><c:out value="${cardData.AUTH_NUM}" /></td>
								</tr>
								<tr align="left" bgcolor="#b6c2ce">
									<td height="20" style="border-bottom:1px dotted #949494; border-top:1px solid #949494;">&nbsp;가맹점 주소</td>
									<td bgcolor="#bebdc9" style="border-bottom:1px dotted #949494; border-top:1px solid #949494; border-left:1px solid #949494;">&nbsp;거래유형</td>
								</tr>
								<tr align="center" bgcolor="#edf2f7">
									<td height="20"><c:out value="${cardData.MERC_ADDR}" /></td>
									<td bgcolor="#f5f4f6" style="border-left:1px solid #949494;"></td>
								</tr>
								<tr align="left" bgcolor="#b6c2ce">
									<td height="20" style="border-bottom:1px dotted #949494; border-top:1px solid #949494;">&nbsp;승인관련문의</td>
									<td bgcolor="#bebdc9" style="border-bottom:1px dotted #949494; border-top:1px solid #949494; border-left:1px solid #949494;">&nbsp;거래상태</td>
								</tr>
								<tr align="center" bgcolor="#edf2f7">
									<td height="20"><c:out value="${cardData.MERC_TEL}" /></td>
									<td bgcolor="#f5f4f6" style="border-left:1px solid #949494;">${cardData.GEORAE_STAT}</td>
								</tr>
							</table>
							<!-- 전표정보 end -->
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table> --%>