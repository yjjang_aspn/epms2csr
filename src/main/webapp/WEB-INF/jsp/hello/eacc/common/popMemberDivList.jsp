<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%--
  Class Name : popMemberDivList.jsp
  Description : 사원 조회 Pop-Up 
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.08.23    정호윤              최초 생성

    author   : 정호윤
    since    : 2017.08.23
--%>
<script type="text/javascript">
var selDeptRow = null; // 선택된 부서

Grids.OnClick = function(grid, row, col){
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}

 	//부서의 포함된 사용자 조회 이벤트
	if(grid.id == "DivisionList") {
		selDeptRow = row;
		getMemberList(row.DIVISION, row.CUR_LVL, row.COMPANY_ID);
	}
}

</script>

<!-- Layer PopUp Start -->
<div class="modal fade" id="memberModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog root wd-per-50"> <!-- 원하는 width 값 입력 -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
               	<h4 class="modal-title"><spring:message code='title.selectUserUd' /></h4> <!-- 사원 선택 -->
            </div>
            <div class="modal-body" >
            	<div class="modal-bodyIn">
					 
					 <div class="modal-section">
						<!-- START -->
						<div class="fl-box" style="width:30%; height:100%;">
							<!-- Left Grid Area Start   -->
							<div id="memberPrivList">
								<bdo	Debug="Error"
										Data_Url="/mem/division/divisionData.do"
										Layout_Url="/mem/division/divisionLayout.do"
										Upload_Url="/mem/division/divisionEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols"				
										>
								</bdo>
							</div>
							<!-- Left Grid Area End     -->
						</div>
						<!-- Left Area End    -->
			
						<!-- Right Area Start -->
						<div class="fl-box" style="width:70%; height:100%;">
			
							<!-- Right Grid Area Start  -->
							<div id="PopMemberDivList" class="mgn-l-10">
								<bdo	Debug="Error"
										Layout_Url="/hello/eacc/common/popMemberDivLayout.do"
										>
								</bdo>
							</div>
							<!-- Right Grid Area End    -->
						</div>
						<!-- Right Area End   -->
					
					</div>
				</div>
            </div>
	    </div>
	</div>
</div>
<!-- Layer PopUp End -->
