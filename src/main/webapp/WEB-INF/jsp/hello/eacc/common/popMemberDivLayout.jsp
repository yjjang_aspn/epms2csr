<?xml version="1.0" encoding="UTF-8"?>
<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : popMemberDivLayout.jsp
  Description : 사원 조회 팝업 그리드 레이아웃 화면
  Modification Information
 
       수정일    	     수정자                   수정내용
   -------    --------    ---------------------------
   2017.08.23  정호윤	              최초 생성

    author   : 정호윤
    since    : 2017.08.22
--%>
<c:set var="gridId" value="PopMemberDivList"/>
<Grid>
	<Cfg	id="${gridId}"
			AcceptEnters   = "1" Calculated = "1"  CalculateSelected = "1"      DateStrings = "2" Deleting  = "0"
			Dragging       = "0" Editing    = "1"  EnterMode         = "4"      Filtering   = "1" IdChars   = "0123456789"
			InEditMode     = "2" MaxPages   = "20" NoFormatEscape    = "1"      NoHScroll   = "0" NoVScroll = "0"
			NumberId       = "1" SafeCSS    = '1'  Selecting		 = "0"
			SelectingCells = "0" SortIcons  = "0"  Style             = "Office" SuppressCfg = "1" 
			CopySelected   = "0" CopyFocused= "1"  CopyCols			 = "0"		ExportFormat="xls" ExportCols="0"
	/>
	
	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>
	
	<!-- Grid Header Setting Area Start  -->
	<Header  id="Header"  Align="center"
		COMPANY_ID	="<spring:message code='company.companyId' />"
		DIVISION	="<spring:message code='division.divisionId' />"
		DIVISION_NM	="<spring:message code='division.divisionNm' />"
		USER_ID		="<spring:message code='member.userId' />"
		NAME		="<spring:message code='member.name' />"
      	EMAIL		="<spring:message code='member.email' />"
      	JOB_GRADE	="<spring:message code='member.jobGrade' />"
      	KOSTL		="<spring:message code='member.kostl' />"
      	SELECT_YN	="<spring:message code='button.select' />"
      />
	<!-- Grid Header Setting Area End    -->
	
	<!-- Fileter Setting Area Start      -->
	<Head>
		<Filter	id = "Filter" CanFocus = "1" RECEIVE_EMAIL="" RECEIVE_EMAILTip = "<spring:message code='tip.batchapply.transfer' />" RECEIVE_EMAILCanEdit="0"/>/>
	</Head>
	<!-- Fileter Setting Area End        -->
	
	<Cols>
    	<C Name="COMPANY_ID"	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="0" /><!-- 회사코드 -->
		<C Name="DIVISION"     	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="0" /><!-- 부서 -->
		<C Name="DIVISION_NM"   Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="1" /><!-- 부서명 -->
	    <C Name="USER_ID"		Type="Text"    RelWidth="100"    Align="left"  	 CanEdit="0" Visible="0" /><!-- 사원번호(ID) -->
	    <C Name="JOB_GRADE"     Type="Enum" RelWidth="100" CanEdit="0" Visible="1" Align="center"
							    EmptyValue="선택" <tag:enum codeGrp="JOB_GRADE" /> /><!-- 직급 -->
	    <C Name="NAME"      	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="1" /><!-- 사원명 -->
	    <C Name="EMAIL"      	Type="Text"    RelWidth="200"    Align="center"  CanEdit="0" Visible="1" /><!-- 이메일 -->
	    <C Name="KOSTL"      	Type="Text"    RelWidth="200"    Align="center"  CanEdit="0" Visible="0" /><!-- 코스트센터 코드 -->
	    <C Name="SELECT_YN"		Type="Bool"	   RelWidth="50"								 Visible="1" />
	</Cols>
		
	<Toolbar	Space="0"	Styles="1"
				<c:choose>
					<c:when test="${param.page == 'none'}">
						Cells="Empty,Cnt,Found,<spring:message code='button.refresh' />,<spring:message code='button.print' />,<spring:message code='button.excel' />"
					</c:when>
					<c:otherwise>
						Cells="Empty,Cnt,Found,<spring:message code='button.add' />,<spring:message code='button.refresh' />,<spring:message code='button.print' />,<spring:message code='button.excel' />"
					</c:otherwise>
				</c:choose>
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType = "Button"
				CntType = "Html" CntFormula = '"Total : &lt;b>"+count(7)+"&lt;/b>"' CntWidth = '-1' CntWrap = '0'
				FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>'" FoundWidth = '-1' FoundWrap = '0'
				<spring:message code='button.add' />Type = "Html" <spring:message code='button.add' /> = "&lt;a href='#none' title='<spring:message code='button.add' />' class=&quot;defaultButton01 icon add&quot;
							       onclick='fn_addMember(&quot;${gridId}&quot;)'><spring:message code='button.add' />&lt;/a>"
				<spring:message code='button.refresh' />Type = "Html" <spring:message code='button.refresh' /> = "&lt;a href='#none' title='<spring:message code='button.refresh' />' class=&quot;defaultButton icon refresh&quot;
									  onclick='reloadGrid(&quot;${gridId}&quot;)'><spring:message code='button.refresh' />&lt;/a>"
 				<spring:message code='button.print' />Type = "Html" <spring:message code='button.print' /> = "&lt;a href='#none' title='<spring:message code='button.print' />' class=&quot;defaultButton icon printer&quot;
									  onclick='printGrid(&quot;${gridId}&quot;)'><spring:message code='button.print' />&lt;/a>"
 				<spring:message code='button.excel' />Type = "Html" <spring:message code='button.excel' /> = "&lt;a href='#none' title='<spring:message code='button.excel' />'class=&quot;defaultButton icon excel&quot;
									  onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'><spring:message code='button.excel' />&lt;/a>"
	/>

</Grid>