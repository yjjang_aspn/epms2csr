<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%--
  Class Name : etcBillWriteList.jsp
  Description : 일반전표 현황 목록
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.08.09    정순주              최초 생성

    author   : 정순주
    since    : 2017.08.09
--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<link rel="stylesheet" href="/css/eacc/modalPopup.css">
<script src="/js/eacc/eaccCommon.js" type="text/javascript" ></script>
<script src="/js/com/web/modalPopup.js"></script>
<script src="/js/jquery/jquery-filestyle.js" type="text/javascript" ></script>
<script src="/js/jquery/jquery.MultiFile.js" type="text/javascript"></script>
<script src="/js/jquery/jquery.ajaxsubmit.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	$('#searchVal').keydown(function(key){
		<%-- 사업장 검색 Enter Key Down Event --%>
		if(key.keyCode == 13){
			fn_searchData();
		}
	});
	
	/* 데이터 URL */
	var parameters = "startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val() + "&searchVal=" + $("#searchVal").val();
	var url = "/hello/eacc/etcBill/etcBillWriteListData.do?" + encodeURI(parameters);
	$("#etcBillWriteList").find('bdo').attr("Data_Url", url);
	
	Grids.OnDataReceive = function(grid,source){
		if(grid.id == "EtcBillWriteList"){
			for(var row=grid.GetFirst(null);row;row=grid.GetNext(row)){
				if(row.ATTACH_GRP_NO){
					grid.SetAttribute(row, "ATTACH_GRP_NO", "Switch","1",1);
					grid.SetAttribute(row, "ATTACH_GRP_NO", "Icon", "/images/com/web/download.gif",1);
					grid.SetAttribute(row, "ATTACH_GRP_NO", "OnClickSideIcon", "fn_openFileDownModal('"+row.ATTACH_GRP_NO+"');", 1);
				}
			}
		}
	}

	Grids.OnReady = function(grid){
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){
			if(row.GW_STATUS == "01" && row.BILL_STATUS == "R" && row.DEL_YN =="Y"){ //임시전표 생성전 사용자 삭제건
				grid.SetValue(row, "ABILITY", "1",1);
			}
			if(row.GW_STATUS == "05" && row.BILL_STATUS == "X" && row.REPARKING_YN =="Y"){ //반려된 전표에 대하여 재임시전표 생성건
				grid.SetValue(row, "ABILITY", "1",1);
			}
			if(row.GW_STATUS == "01" && row.BILL_STATUS == "X"){ //임시전표 생성후 사용자 임시전표 삭제 건
				grid.SetValue(row, "ABILITY", "1",1);
			}
			if(row.GW_STATUS == "06" && row.BILL_STATUS == "A"){ //실전표 처리후 승인취소 건
				grid.SetValue(row, "ABILITY", "1",1);
			}
		}
	}
	
	//그리드 row 더블클릭시 에빈트
	Grids.OnDblClick = function(grid, row, col){
		if(row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "Filter" || row.id == "PAGER" || row.id == "_ConstHeight") {
			return;
		}
		
		if(grid.id == "EtcBillWriteList") {
			if(row.id != "Filter"){
				var url = getEtcBillDetail(row, col);
				url += "&"+new Date().getTime();
				
				// 전표 수정 모달 팝업 로드
				$("#etcBillUptModal").load(url, function(){
					$(this).modal();
				});
				
				Grids.Active = null;
			}
		}
		
		//더블클릭 시 부모창에 코스트센터 정보 세팅
		if(grid.id == "CostcenterList") {
			fnSetCostcenter(row.KOSTL, row.KOKRS, row.KTEXT);
			layer_open('costcenterModal');
		}
		
		//더블클릭 시 부모창에 계정 정보 세팅
		if(grid.id == "GlAccountList") {
			fnSetGlAccount(row);
			layer_open('glaccountModal');
			$('#searchBunryu').val("");
		}
		
		//더블클릭 시 부모창에 구매처 정보 세팅
		if(grid.id == "VendorList") {
			fnSetVendor(row);
			layer_open('vendorModal');
			$("#searchVal").val("");
			$("#searchGubun").val("");
		}
		
		//더블클릭 시 부모창에 오더 정보 세팅
		if(grid.id == "InorderList") {
			fnSetInorder(row);
			layer_open('InorderModal');
		}
	}
	
	// 코스트센터 변경 팝업 호출
	$(document).on("click", ".ktext", function(){
		var idx = $(this).parents("tr").index() - 1;
		fn_popCostcenter(idx);
	});
	
	// G/L계정 변경 팝업 호출
	$(document).on("click", ".glName", function(){
		var idx = $(this).parents("tr").index() - 1;
		fn_popGlAccount(3, idx);
	});
	
	// 오더 변경 팝업 호출
	$(document).on("click", ".autxt", function(){
		var idx = $(this).parents("tr").index() - 1;
		fn_popInorder(idx);
	});
	
	// 모달 닫기 이벤트
// 	$('#etcBillUptModal').on('hidden.bs.modal', function () {
// 		Grids.EtcBillWriteList.ReloadBody();
// 	});
	
});

/* 날짜 검색 */
function fn_searchData(){
	var parameters = "startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val() + "&searchVal=" + $("#searchVal").val();
	Grids.EtcBillWriteList.Source.Data.Url = "/hello/eacc/etcBill/etcBillWriteListData.do?" + encodeURI(parameters);
	Grids.EtcBillWriteList.ReloadBody();
}

// iframe에 일반전표 상세 화면 출력
function getEtcBillDetail(row, col){
	var etcBillNdx = row.ETCBILL_NDX; 	// 전표ID
	var gwStatus = row.GW_STATUS; 		// 결재상태
	var billStatus = row.BILL_STATUS; 	// 전표상태
	var delYn = row.DEL_YN;				// 전표 삭제 여부
	var reparkingYn = row.REPARKING_YN; // 반려된 전표 재임시전표 생성 여부
	
	if(gwStatus == 01 && billStatus == "R" && delYn == "N") { // 임시전표 생성전 사용자 작성/수정  
		var srcUrl = "/hello/eacc/etcBill/etcBillUpt.do";
		var param = "?etcBillNdx=" + etcBillNdx;
	} else if((gwStatus == 05 && billStatus == "X" && reparkingYn == "")){ //임시전표 결재 반려
		var srcUrl = "/hello/eacc/etcBill/etcBillUpt.do";
		var param = "?etcBillNdx=" + etcBillNdx + "&apprYn=Y";
	}else{ //이외 건들
		var srcUrl = "/hello/eacc/etcBill/etcBillDtl.do";
		var param = "?etcBillNdx=" + etcBillNdx + "&apprYn=Y";
	}
	
	return srcUrl + param;
}


// 그리드 리로드
function reLoadGrid(){
	Grids.EtcBillWriteList.ReloadBody();	
}

// 모달 토글
function toggleModal(obj){
	obj.modal('toggle');
}

</script>
</head>
<body>


<!-- 페이지 내용 : s -->
<form id="etcBillWriteListForm" name="etcBillWriteListForm"  method="post" enctype="multipart/form-data">
</form>

<!-- s:input -->
<input type="hidden" title="오늘날짜" id="toDate" name="toDate" value="${toDate}"/>
<input type="hidden" title="종료날짜" id="endDate" name="endDate" value="${endDate}"/>
<input type="hidden" title="시작날짜" id="startDate" name="startDate" value="${startDate}"/>
<input type="hidden" title="경비작성마감일" id="submitDeadLine" name="submitDeadLine" value="${submitDeadLine}"/> <!-- 4일 -->
<input type="hidden" title="경비결재마감일" id="apprDeadLine" name="apprDeadLine" value="${apprDeadLine}"/> <!-- 6일 -->
<!-- e:input -->

<div id="contents">
	<!-- 페이지 내용 : s -->
	<!-- s:panel-wrap01 -->
	<div class="fl-box panel-wrap03" style="width:100%; height:100%">
		<h4 class="panel-tit mgn-l-10">일반전표 현황</h4>
		
		<!-- inq-area-inner : s -->
		<div class="inq-area-inner type06 ab">
			<ul class="wrap-inq">
				<li class="inq-clmn">
					<h4 class="tit-inq">전기일자</h4>
					<div class="prd-inp-wrap">
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic openCal thisMonth" readonly="readonly" gldp-id="gldp-7636599364" id="startDt"/>
							</span>
						</span>
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic openCal" readonly="readonly" gldp-id="gldp-8530641166" id="endDt"/>
							</span>
						</span>
					</div>
				</li>
				<li class="inq-clmn">
					<h4 class="tit-inq">전표번호</h4>
					<div class="prd-inp-wrap">
						<input type="text" class="inp-wrap type03" id="searchVal" name="searchVal" value=""/>
					</div>
				</li>
			</ul>
			<a href="#" class="btn comm st01" onclick="fn_searchData(); return false;">검색</a>
		</div>
		<!-- inq-area-inner : e -->
		
		<div class="panel-body mgn-t-25">
			<!-- 트리그리드 : s -->
			<div id="etcBillWriteList" style="height:100%">
				<bdo Debug="Error"
					 Layout_Url="/hello/eacc/etcBill/etcBillWriteListLayout.do"
				>
				</bdo>
			</div>
			<!-- 트리그리드 : e -->
		</div>
	</div>
	<!-- e:panel-wrap01 -->
</div>

<!-- 일반전표 수정화면 모달 시작-->
<div class="modal fade" id="etcBillUptModal" data-backdrop="static" data-keyboard="false"></div>
<!-- 일반전표 수정화면 모달 끝-->

<%@ include file="/WEB-INF/jsp/hello/eacc/common/popMemberDivList.jsp"%>
<%@ include file="/WEB-INF/jsp/hello/eacc/common/popNeedBill.jsp"%>

<!-- 그리드 첨부파일 다운로드 모달 -->
<div class="modal fade" id="fileDownModal" data-backdrop="static" data-keyboard="false"></div>
<!-- 그리드 첨부파일 다운로드 모달 -->

<!-- 화면 UI Area End    -->

<!-- 페이지 내용 : e -->
</body>
</html>