<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : etcBillUpt.jsp
  Description : 일반전표 수정화면
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.08.09    정순주              최초 생성

    author   : 정순주
    since    : 2017.08.09
--%>
<script type="text/javascript">
var maxFileCnt = 10;
var fileVal = "";
var layerGubun = "addPerson"; // 레이어 팝업 구분자(추가직원-addPerson, 이관-transPerson)
var bill_gb = "3";
$(document).ready(function(){
	
	//모달팝업 캘린더
	$('.moOpenCal').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	});
	
	// 첨부파일
	var maxFileCnt = 10;
	for(var i=0; i<1; i++){
		$('#attachFile'+i).append('<input type="file" multiple id="ex_file'+i+'" class="ksUpload" name="fileList">');	
		var fileType = 'ppt |pps |pptx |ppsx |pdf |hwp |xls |xlsx |xlsm | xlt |doc |dot |docx |docm |txt |gif |jpg |jpeg |png';
		
		$('#ex_file'+i).MultiFile({
				accept : fileType
			,	list   : '#detailFileList'+i
			,	max    : maxFileCnt
			,	STRING : {
					remove: '<img src="/images/com/web/btn_remove.png" />'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
//						text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" >',
					idx	  : i,
					denied:'$ext는(은) 업로드할 수 없는 파일 확장자입니다.'
				}
		});
		
		$('.MultiFile-wrap').attr("id", "jquery"+i);
	}
	
	// 첨부파일 삭제
	$('.icn_fileDel').on('click', function(){
		delModiFile($(this));
	});
	
	// 파일 다운로드
	$('.btnFileDwn').on('click', function(){
		var ATTACH = $(this).data("attach");
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO=${data.ATTACH_GRP_NO}&ATTACH='+ATTACH;
		location.href = src;
	});
	
	/* 초기값 설정 */
	$('#BOOK_DT').val('${data.BOOK_DT}');						// 전기일
	$('#EVIDENCE_DT').val('${data.EVIDENCE_DT}'); 				// 증빙일
	$("#TOT_AMOUNT").val(oneLimitCount("${data.TOT_AMOUNT}"));					// 총금액
	$("#AMT_AMOUNT").val(oneLimitCount("${data.AMT_AMOUNT}"));					// 총공급가액
	$("#VAT_AMOUNT").val(oneLimitCount("${data.VAT_AMOUNT}"));					// 총부가세액
	$('#WORKPLACE_CD').val('${data.WORKPLACE_CD}'); 			// 부가세사업장
	$('#ACCGUBUN').val('${data.ACCGUBUN}'); 					// 계정구분
	$('#TAX_CD').val('${data.TAX_CD}'); 						// 세금코드
	$("#PURCHASEOFFI_CD").val('${data.PURCHASEOFFI_CD}');		// 구매처코드 세팅
	$("#PURCHASEOFFI_NM").val('${data.PURCHASEOFFI_NM}');		// 구매처코드 세팅
	$("#BANK_CD").val('${data.BANK_CD}');						// 은행코드
	$("#BANK_NM").val('${data.BANK_NM}');						// 은행명
// 	$("#PAYEE_CD").val('${data.PAYEE_CD}');						// 수취인코드
// 	$("#PAYEE_NM").val('${data.PAYEE_NM}');						// 수취인명
	$("#ACCOUNT_NO").val('${data.ACCOUNT_NO}');					// 계좌번호
	$("#ACCOUNT_HOLDER").val('${data.ACCOUNT_HOLDER}');			// 예금주
// 	$('#PAY_TERM').val('${data.PAY_TERM}'); 					// 지급조건
// 	$("#PAYDUE_DT").val('${data.PAYDUE_DT}');					// 지급예정일
	
	// 초기화
	fn_setTable('${data.ACCGUBUN}');
	fn_setVatTd('${data.TAX_CD}');
	
	//공급가액 입력시 세액 계산하여 세팅
	$(document).on("keyup", "input[name=arrItemAmt]", (function(e) {
		// 세금코드가 부가세무관일때 세액이 없음
		var taxCd = $('#TAX_CD').val();
		if(taxCd == "V0"){
			tmpTax = "";
		}else{
			var amount = $(this).val().replace(/,/g, "") * 1;
			var tmpTax = "";
			if(amount != ""){
				tmpTax = jisuanTax(amount);  
			}else{
				tmpTax = "";
			}
		}
		$(this).parent().parent().next().find("input[name=arrItemVat]").val(tmpTax);
		
		fn_addAmt();
	}));
	
	$(document).on("keyup", "input[name=arrItemVat]", (function(e) {
		fn_addAmt();
	}));
	
	// 계정구분이 변경될 경우, 세금코드가 변경됨
	$('#ACCGUBUN').on('change', function(){
		fn_setTable($(this).val());
		fn_init();
		// 세금코드 첫번째 값으로 초기화
		$('#TAX_CD').val($('#TAX_CD option:eq(0)').val());
	});

	// 세금코드 변경에 따라 세액항목 hide/show
	$('#TAX_CD').on('change', function(){
		fn_init();
		fn_setVatTd($(this).val());
		fn_addAmt();
	});
});

/* 구매처 변경 팝업 호출 */
function fn_popVendor(billGb){
	layer_open('vendorModal');
	Grids.VendorList.Source.Data.Url = "/hello/eacc/common/popVendorListData.do?LIFNR=${sessionScope.ssUserId}&billGb="+billGb;
	Grids.VendorList.ReloadBody();
}

/* 증빙 항목에서 제거 처리 */
function delModiFile(obj){
	var attach = obj.data("attach");
	var idx = obj.data('idx');
	
	$('#fileLstWrap_'+attach).remove();
	
	//fileNo set
	if(fileVal != '') {
		fileVal += ",";
	}
	
	fileVal += attach;
	$('#delSeq').val(fileVal);
}

/* 세금코드에 따라 세액 항목 show/hide 처리 */
function fn_setVatTd(tax_cd){
	// 과세,불공제의 경우 세액항목 show
	if(tax_cd == 'VA' || tax_cd == 'VB'){
		$(".th_vat").show();
		$(".td_vat").show();
		
	// 특정세금코드의 경우 세액항목 hide
	}else{
		$(".th_vat").hide();
		$(".td_vat").hide();
	}
}

/* 계정구분값에 따라 th, td show/hide */
function fn_setTable(accgubun){
	if(accgubun == '3'){	// 선급비용
		$(".th_accstartdt").show();
		$(".th_accenddt").show();
		$(".td_accstartdt").show();
		$(".td_accenddt").show();
		
		$(".th_vat").show();
		$(".td_vat").show();
		
		$(".th_aufnr").hide();
		$(".td_aufnr").hide();
		
		$(".th_costcenter").show();
		$(".td_costcenter").show();
		
	}else{	//경비
		$(".th_accstartdt").hide();
		$(".th_accenddt").hide();
		$(".td_accstartdt").hide();
		$(".td_accenddt").hide();
		
		$(".th_vat").show();
		$(".td_vat").show();
		
		$(".th_costcenter").show();
		$(".td_costcenter").show();
		
		$(".th_aufnr").hide();
		$(".td_aufnr").hide();
		
		// 선급비용시작일 초기화
		$('[name=arrAccStartDt]').each(function(){
			$(this).val('');
		});
		// 선급비용종료일 초기화
		$('[name=arrAccEndDt]').each(function(){
			$(this).val('');
		});
	}
}

/* 계정구분, 세금코드 변경시 라인아이템 초기화 */
function fn_init(){
	$('#tb_items > tbody > tr:gt(1)').remove();
	itemSeq = 1;
	fn_addAmt();
	$('.glCode').val('');
	$('.glName').val('');
	$('.summary').val('');
	$('.summary').attr('placeholder', '');
	$('.itemAmt').val('');
	$('.itemVat').val('');
	$('.aufnr').val('');
	$('.autxt').val('');
	$('.accStartDt').val('');
	$('.accEndDt').val('');
}

/* validation Check */
function fn_Validation(){
	
	if(!isNull_J($('#BOOK_DT'), '전기일을 입력해주세요.')) {
		return false;
	}
	
	if(!isNull_J($('#EVIDENCE_DT'), '증빙일을 입력해주세요.')) {
		return false;
	}
	
	if(!isNull_J($('#WORKPLACE_CD'), '부가세사업장을 선택해주세요.')) {
		return false;
	}
  
  	if(!isNull_J($('#ACCGUBUN'), '계정구분을 선택해주세요.')) {
  		return false;
    }
   
  	if(!isNull_J($('#TAX_CD'), '세금코드를 선택해주세요.')) {
  		return false;
    }
  	
  	if(!isNull_J($('#lifnrNm'), '구매처를 선택해주세요.')) {
  		return false;
    }
  	
 	var isValid = true;
 	
	$('[name=arrGlCode]').each(function(){
		if(!isNull_J($(this), 'G/L 계정을 선택해주세요.')) {
			isValid = false;
			return false;
		}
	});

	if(isValid){
		$('[name=arrSummary]').each(function(){
			if(!isNull_J($(this), '적요를 입력해주세요.')) {
				isValid = false;
				return false;
			}
		});
	}
	
	// 공급가액
	var arrItemAmt = 0;
	$('[name=arrItemAmt]').each(function(){
		var itemAmt = $(this).val().replace(/,/g, "") * 1;
		arrItemAmt += itemAmt;
	});
	if(isValid){
		if(arrItemAmt == 0){
			alert("공급가액을 입력해주시기 바랍니다.");
			isValid = false;
			return false;
		}
	}
	
	if(isValid){
		// 계정구분이 경비(1)인 경우
		if($('#ACCGUBUN').val() == '1'){
			$('[name=arrAufnr]').each(function(index){
				// 오더번호가 존재하지 않으면 코스트센터가 반드시 존재해야 함
				if($(this).val() == ''){
					if(!isNull_J($('.kostl').eq(index), '오더번호를 입력하지 않는 경우, 코스트센터는 필수입력입니다.')) {
						$('.kostl').eq(index).focus();
						isValid = false;
						return false;
					}
				}
			});
		}else{	// 계정구분이 선급비용(3)인 경우
			// 선급비용시작일
			$('[name=arrAccStartDt]').each(function(){
				if(!isNull_J($(this), '선급비용시작일을 입력해주세요.')) {
					isValid = false;
					return false;
				}
			});
		
			// 선급비용종료일
			if(isValid){
				$('[name=arrAccEndDt]').each(function(){
					if(!isNull_J($(this), '선급비용종료일을 입력해주세요.')) {
						isValid = false;
						return false;
					}
				});
			}
		}
	}
	
	// 공급가액 콤마 제거
	$('[name=arrItemAmt]').each(function(){
		var itemAmt = $(this).val().replace(/,/g, "") * 1;
		$(this).val(itemAmt);
	});
	
	// 세액 콤마 제거
	$('[name=arrItemVat]').each(function(){
		var itemVat = $(this).val().replace(/,/g, "") * 1;
		$(this).val(itemVat);
	});
	
	if(!isValid){
		return false;
	}
	
	return isValid;
}

//세액 자동 계산
function jisuanTax(obj){
	var taxAmount = parseInt(obj)*0.1;
	taxAmount = Math.round(taxAmount);
	
	return number_format(taxAmount+''); 
}

var itemSeq = ${itemList.size()}; // 가져오는 아이템 수
/* 아이템 추가 */
function addItem(){
	var html = "<tr id='dynamicRow'>"+$("#tb_items > tbody > tr:last").html()+"</tr>"
	
	$("#tb_items > tbody > tr:last").after(html);
	// 복사된 항목 초기화
	$(".glCode").eq(itemSeq).val("");
	$(".glName").eq(itemSeq).val("");
	$(".summary").eq(itemSeq).val("");
	$(".summary").eq(itemSeq).attr("placeholder", "");
	$(".itemAmt").eq(itemSeq).val("");
	$(".itemVat").eq(itemSeq).val("");
	$(".aufnr").eq(itemSeq).val("");
	$(".autxt").eq(itemSeq).val("");
	$(".accStartDt").eq(itemSeq).val("");
	$(".accEndDt").eq(itemSeq).val("");
	itemSeq++;
}

/* 아이템 삭제 */
function deleteItem(obj){
	if(itemSeq == '1'){
		alert("첫번째 행은 삭제 할 수 없습니다.");
		return;
	}
	
// 	$('#tb_items > tbody > tr:last').remove();
	$(obj).parent().parent().parent().remove();
	
	itemSeq = itemSeq - 1;
	
	fn_addAmt();
}

/* 총 공급가액 */
function fn_addAmt(){
	var arrItemTot = 0;
	var arrItemAmt = 0;
	var arrItemVat = 0;
	$('input[name=arrItemAmt]').each(function(){
		var itemAmt = $(this).val().replace(/,/g, "") * 1;
		arrItemAmt += itemAmt;
	});
	$('input[name=arrItemVat]').each(function(){
		var itemVat = $(this).val().replace(/,/g, "") * 1;
		arrItemVat += itemVat;
	});
	arrItemTot = arrItemAmt + arrItemVat;
	
	$('#TOT_AMOUNT').val(oneLimitCount(arrItemTot));		// 총 금액
	$('#AMT_AMOUNT').val(oneLimitCount(arrItemAmt)); 		// 총 공급가액
	$('#VAT_AMOUNT').val(oneLimitCount(arrItemVat)); 		// 총 부가세액
}

// 삭제 처리
function fn_removeEtcBill(){
	if(confirm("삭제 하시겠습니까?" )){
		jQuery.ajax({
    		type : 'POST',
    		url : '/hello/eacc/etcBill/etcBillRemoveAjax.do',
    		cache : false,
    		dataType : 'json',
    		data : { etcBillNdx : '${data.ETCBILL_NDX}' },
			async : false,
    		error : function() {
    			alert("처리중 오류가 발생 하였습니다.");
			},
    		success : function(json) {
    			if(json.resultCd == "T"){
    				alert(json.resultMsg);
    				reLoadGrid();
    				toggleModal($("#etcBillUptModal"));
    			}else{
    				if(typeof json.errorMsg == 'undefined' || json.errorMsg == 'undefined'){
    					alert(json.resultMsg);
    				}else{
    					alert(json.resultMsg + "\n" + json.errorMsg);
    				}
    			}
			}
    	});
	}
}

/* 임시전표생성 */
function fn_parkingEtcBill(){
	if(confirm("임시전표를 생성 하시겠습니까?" )){
		jQuery.ajax({
			type : 'POST',
			url : '/hello/eacc/etcBill/etcBillPakingAjax.do',
			cache : false,
			dataType : 'json',
			data : { etcBillNdx : '${data.ETCBILL_NDX}' },
			async : false,
			error : function() {
				alert("처리중 오류가 발생 하였습니다.");
			},
			success : function(json) {
				if(json.resultCd == "T"){
					alert(json.resultMsg);
					reLoadGrid();
					toggleModal($("#etcBillUptModal"));
				}else{
					if(typeof json.errorMsg == 'undefined' || json.errorMsg == 'undefined'){
						alert(json.resultMsg);
					}else{
						alert(json.resultMsg + "\n" + json.errorMsg);
					}
				}
			}
		});
	}
}

// 수정 처리
function fn_uptEtcBill(){
	
	// validation check
	if(!fn_Validation()){
		return;
	}
	
	// 적요가 1개일 때, 적요에 들어가는 ',' 처리
	if($("input[name=arrSummary]").length == 1) {
		$("input[name=arrSummary]").val($("input[name=arrSummary]").val().replace(/,/g, "‚"));    
	}
	
	if(confirm("수정 하시겠습니까?")){
		
		var fileCnt = $(".MultiFile-label").length;
		$('#fileCnt').val(fileCnt);
		
		if(fileCnt > 0){
			$('#attachExistYn').val("Y");
		}else{
			$('#attachExistYn').val("N");
		}
		
		$('#etcBillUptForm').attr('action','/hello/eacc/etcBill/updateEtcBillAjax.do').ajaxSubmit(function(json){
			alert(json.resultMsg);
			if (json.resultCd == "T") {
				reLoadGrid();
				toggleModal($("#etcBillUptModal"));
			}
		});
	}
}

// (재)전표생성 처리
function fn_reParkingEtcBill() {
	
	// validation check
	if(!fn_Validation()){
		return;
	}
	
	// 적요가 1개일 때, 적요에 들어가는 ',' 처리
	if($("input[name=arrSummary]").length == 1) {
		$("input[name=arrSummary]").val($("input[name=arrSummary]").val().replace(/,/g, "‚"));    
	}
	
	if(confirm("(재)임시전표를 생성하시겠습니까?")){
		
		var fileCnt = $(".MultiFile-label").length;
		$('#fileCnt').val(fileCnt);

		if(fileCnt > 0){
			$('#attachExistYn').val("Y");
		}else{
			$('#attachExistYn').val("N");
		}
		
		$('#etcBillUptForm').attr('action', '/hello/eacc/etcBill/etcBillReParkingAjax.do').ajaxSubmit(function(json) {
			alert(json.resultMsg);
			if (json.resultCd == "T") {
				reLoadGrid();
				toggleModal($("#etcBillUptModal"));
			}
		});
	}
	
}

</script>

	<div class="modal-dialog root wd-per-80"> <!-- 원하는 width 값 입력 -->
        <div class="modal-content">
			<div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
               	<h4 class="modal-title">일반전표 상세</h4>
               	
               	<div style="position:absolute; top:13px; right:40px;">
					<c:if test="${data.GW_STATUS eq '01' && data.BILL_STATUS eq 'R'}">
						<a href="#" class="btn comm st02 f-r" style="margin-right: 5px; margin-bottom: 5px;" onclick="fn_removeEtcBill(); return false;">삭제</a>
						<a href="#" class="btn comm st01 f-r" style="margin-right: 5px; margin-bottom: 5px;" onclick="fn_parkingEtcBill(); return false;">임시전표생성</a>
						<a href="#" class="btn comm st02 f-r" style="margin-right: 5px; margin-bottom: 5px;" onclick="fn_uptEtcBill(); return false;">수정</a>
					</c:if>
					
					<c:if test="${data.GW_STATUS eq '05' and data.BILL_STATUS eq 'X' and data.REPARKING_YN eq ''}">
						<a href="#" class="btn comm st01 f-r" style="margin-bottom:5px;" onclick="fn_reParkingEtcBill(); return false;">(재)전표생성</a>
					</c:if>
				</div>
            </div>
            <div class="modal-body" >
				<div class="modal-bodyIn" >

					
					<!-- s:input -->
					<input type="hidden" title="오늘날짜" id="toDate" name="toDate" value="${toDate}"/>
					<input type="hidden" title="경비작성마감일" id="submitDeadLine" name="submitDeadLine" value="${submitDeadLine}"/> <!-- 4일 -->
					<input type="hidden" title="경비결재마감일" id="apprDeadLine" name="apprDeadLine" value="${apprDeadLine}"/> <!-- 6일 -->
					<!-- e:input -->

					<form id="etcBillUptForm" action="" method="post" enctype="multipart/form-data">
						
						<!-- input : s -->
						<input type="hidden" name="etcBillNdx" value="${data.ETCBILL_NDX }" />
						<!-- 첨부파일 정보 -->
						<input type="hidden" title="파일등록여부" id="attachExistYn" name="attachExistYn" value=""/>
						<input type="hidden" title="삭제Seq" id="delSeq" name="delSeq" value=""/>
						<input type="hidden" title="파일 건수" id="fileCnt" name="fileCnt" value=""/>
						<input type="hidden" title="경비 모듈" id="MODULE" name="MODULE" value="8">
						<!-- input : e -->
						
						<!-- s:inq-area-top -->
<!-- 						<div class="inq-area-top mgn-t-15"> -->
<%-- 							<c:if test="${data.GW_STATUS eq '01' && data.BILL_STATUS eq 'R'}"> --%>
<!-- 								<a href="#" class="btn comm st01 f-r" style="margin-right: 5px; margin-bottom: 5px;" onclick="fn_removeEtcBill(); return false;">삭제</a> -->
<!-- 								<a href="#" class="btn comm st03 f-r" style="margin-right: 5px; margin-bottom: 5px;" onclick="fn_parkingEtcBill(); return false;">임시전표생성</a> -->
<!-- 								<a href="#" class="btn comm st02 f-r" style="margin-right: 5px; margin-bottom: 5px;" onclick="fn_uptEtcBill(); return false;">수정</a> -->
<%-- 							</c:if> --%>
							
<%-- 							<c:if test="${data.GW_STATUS eq '05' and data.BILL_STATUS eq 'Z'}"> --%>
<!-- 								<a href="#" class="btn comm st03 f-r" style="margin-bottom:5px;" onclick="fn_reParkingEtcBill(); return false;">(재)전표생성</a> -->
<%-- 							</c:if> --%>
<!-- 						</div> -->
						<!-- e:inq-area-top -->
							
						<div id="">							
							<!-- s:tb-wrap -->
							<div class="tb-wrap mgn-t-5">
								<table class="tb-st">
									<caption class="screen-out">일반 전표 작성</caption>
									<colgroup>
										<col width="13%">
										<col width="20%">
										<col width="13%">
										<col width="20%">
										<col width="13%">
										<col width="20%">
									</colgroup>
									<tbody>
										<tr>
											<!-- s:전기일 -->	
											<th scope="row">전표 구분</th>
											<td>
												<div>일반</div>
											</td>
											<!-- e:전기일 -->
											
											<!-- s:전기일 -->	
											<th scope="row">전기일</th>
											<td>
												<div>
													<input type="text" class="inp-comm datePic moOpenCal" title="전기일" id="BOOK_DT" name="BOOK_DT" value="${data.BOOK_DT }" readonly="readonly">
												</div>
											</td>
											<!-- e:전기일 -->
											
											<!-- s:증빙일 -->
											<th scope="row">증빙일</th>
											<td>
												<div>
													<input type="text" class="inp-comm datePic moOpenCal" title="증빙일" id="EVIDENCE_DT" name="EVIDENCE_DT" value="${data.EVIDENCE_DT }" readonly="readonly">
												</div>
											</td>
											<!-- e:증빙일 -->
										</tr>
										<tr>
											<!-- s:총 금액 -->	
											<th scope="row">총 금액</th>
											<td>
												<div>
													<input type="text" class="infoTextInput" id="TOT_AMOUNT" name="TOT_AMOUNT" readonly="readonly" />
												</div>
											</td>
											<!-- e:총 금액 -->
											<!-- s:총 공급가액 -->	
											<th scope="row">총 공급가액</th>
											<td>
												<div>
													<input type="text" class="infoTextInput" id="AMT_AMOUNT" name="AMT_AMOUNT" readonly="readonly" />
												</div>
											</td>
											<!-- e:총 공급가액 -->
											<!-- s:총 부가세액 -->	
											<th scope="row">총 부가세액</th>
											<td>
												<div>
													<input type="text" class="infoTextInput" id="VAT_AMOUNT" name="VAT_AMOUNT" readonly="readonly" />
												</div>
											</td>
											<!-- e:총 부가세액 -->
										</tr>
										
									</tbody>
								</table>
							</div>
							<!-- e:tb-wrap -->
						</div>
						
						<!-- s:tb-wrap -->
						<div class="tb-wrap mgn-t-10">
							<table class="tb-st">
								<colgroup>
									<col width="13%">
									<col width="20%">
									<col width="13%">
									<col width="20%">
									<col width="13%">
									<col width="20%">
								</colgroup>
								<tbody>
									<tr>
										<!-- s:부가세사업장 -->	
										<th scope="row">부가세사업장</th>
										<td>
											<div class="sel-wrap">
												<select title="부가세사업장" id="WORKPLACE_CD" name="WORKPLACE_CD">
													<option value="">선택</option>
													<c:forEach var="item" items="${workplaceList}">
														<option value="${item.WORKPLACE_CD}">${item.WORKPLACE_NM}</option>
													</c:forEach>
												</select>
											</div>
										</td>
										<!-- e:부가세사업장 -->
										
										<!-- s:계정구분 -->	
										<th scope="row">계정구분</th>
										<td>
											<div class="sel-wrap">
												<tag:combo codeGrp="ACCGUBUN" name="ACCGUBUN" />
											</div>
										</td>
										<!-- e:계정구분 -->
										
										<!-- s:세금코드 -->	
										<th scope="row">세금코드</th>
										<td>
											<div class="sel-wrap">
												<tag:taxCombo billGb="ETC" name="TAX_CD" />
											</div>
										</td>
										<!-- e:세금코드 -->
									</tr>
									<tr>
										<!-- s:구매처 -->
										<th id="th_lifnr" scope="row">구매처</th>
										<td colspan="5">
											<div class="typeA">
												<div class="inp-wrap">
													<input type="text" class="inp-comm" title="구매처" id="PURCHASEOFFI_NM" name="PURCHASEOFFI_NM" value="${data.PURCHASEOFFI_NM }" readonly="readonly">
													<input type="hidden" id="PURCHASEOFFI_CD" name="PURCHASEOFFI_CD" value="${data.PURCHASEOFFI_CD }" /><!-- 구매처코드 -->
													<input type="hidden" id="BANK_CD" name="BANK_CD" value="${data.BANK_CD }" /><!-- 은행코드 -->
													<input type="hidden" id="BANK_NM" name="BANK_NM" value="${data.BANK_NM }" /><!-- 은행명 -->
	<%-- 												<input type="hidden" id="PAYEE_CD" name="PAYEE_CD" value="${data.PAYEE_CD }" /><!-- 수취인코드 --> --%>
	<%-- 												<input type="hidden" id="PAYEE_NM" name="PAYEE_NM" value="${data.PAYEE_NM }" /><!-- 수취인명 --> --%>
													<input type="hidden" id="ACCOUNT_NO" name="ACCOUNT_NO" value="${data.ACCOUNT_NO }" /><!-- 계좌번호 -->
													<input type="hidden" id="ACCOUNT_HOLDER" name="ACCOUNT_HOLDER" value="${data.ACCOUNT_HOLDER }" /><!-- 예금주명 -->
	<%-- 												<input type="hidden" id="PAY_TERM" name="PAY_TERM" value="${data.ZTERM }" /><!-- 지급조건 --> --%>
	<%-- 												<input type="hidden" id="PAYDUE_DT" name="PAYDUE_DT" value="${data.BANKL }" /><!-- 지급예정일 --> --%>
												</div>
												<a href="#none" class="btn evn-st01" style="margin-left:5px;" onclick="fn_popVendor(3); return false;">변경</a>
											</div>
										</td>
										<!-- e:구매처 -->
									</tr>
									<tr>
										<!-- s:은행명 -->
										<th scope="row">은행명</th>
										<td id="td_banka">${data.BANK_NM }</td>
										<!-- e:은행명 -->
										
										<!-- s:계좌번호 -->
										<th scope="row">계좌번호</th>
										<td id="td_bankn">${data.ACCOUNT_NO }</td>
										<!-- e:계좌번호 -->
										
										<!-- s:지급예정일 -->	
										<th scope="row">지급예정일</th>
										<td id="td_paydueDt">
											<c:if test="${data.PAYDUE_DT ne '' && data.PAYDUE_DT ne null}">
												${fn:substring(data.PAYDUE_DT,0,4)}-${fn:substring(data.PAYDUE_DT,5,7)}-${fn:substring(data.PAYDUE_DT,8,10)}
											</c:if>
										</td>
										<!-- e:지급예정일 -->
									</tr>								
									<tr>
										<!-- s:추가직원 -->	
										<th scope="row">추가직원</th>
										<td colspan="5" data-name="lstArea1">
											<a href="#" class="btn evn-st01" onclick="layer_open('memberModal'); return false;">선택</a>
											<div id="addPrsnName">
												<c:if test="${data.ADD_PRSN_NM ne '' and data.ADD_PRSN_NM ne null }">
													<div class="inp-wrap readonly" style="width:100%;height:auto;margin-top:5px;">
														<textarea cols="" rows="2" id="ADD_PRSN_NM" readonly="readonly" >${data.ADD_PRSN_NM}</textarea>
														<input type="hidden" value="${data.ADD_PRSN}"/>
													</div>
												</c:if>
											</div>
										</td>
										<!-- e:추가직원 -->
									</tr>
									<tr>
										<!-- s:증빙 -->	
										<th scope="row">증빙<br><span class="pd-l-10">(jpg, pdf, doc, xls 등)</span></th>
										<td colspan="5" style="padding:3px 15px 0;">
											<div class="bxType01 fileWrap">
												<div class="filebox" id="attachFile0"></div>
												<div class="fileList" style="padding:7px 0 5px;">
													<div id="detailFileList0" class="fileDownLst">
														<c:forEach var="item" items="${attachList}" varStatus="idx">
															<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
																<span class="MultiFile-remove">
																	<a href="#none" class="icn_fileDel" data-attach="${item.ATTACH}" data-idx="${item.CD_FILE_TYPE }">
																		<img src="/images/com/web/btn_remove.png" />
																	</a>
																</span> 
																<span class="MultiFile-export"> ${item.SEQ_DSP} </span>
																<span class="MultiFile-title" title="File selected: ${item.NAME}">
																${item.NAME}(${item.FILE_SIZE}kb)
																</span>
																<a href="#none" class="btnFileDwn icon download" data-attach="${item.ATTACH}">다운로드</a>
															</div>	
														</c:forEach>
													</div>
												</div>
											</div>
										</td>
										<!-- e:증빙 -->
									</tr>
								</tbody>
							</table>
						</div>
						<!-- e:tb-wrap -->
						
						<div class="popPayBtn" style="float:right; margin-bottom:5px; margin-top:5px;">
							<!-- s:추가버튼 -->
							<div id="addBTN" class="btn comm st01" onclick="javascript:addItem(); return false;">
								<a href="#"><span>추가</span></a>
							</div>
							<!-- e:추가버튼 -->
							<!-- s:삭제버튼 -->
<!-- 							<div id="delBTN" class="btn comm st02" onclick="javascript:deleteItem(); return false;"> -->
<!-- 								<a href="#"><span>삭제</span></a> -->
<!-- 							</div> -->
							<!-- e:삭제버튼 -->
						</div>
						
						<div class="tb-wrap" style="width:100%;">
							<table id="tb_items" class="tb-st">
								<tbody>
<!-- 								<thead> -->
									<tr>
										<th class="payDivTile th_costcenter" style="width:15%; text-align:center;">코스트센터</th>
										<th class="payDivTile" style="width:20%; text-align:center;">G/L 계정</th>
										<th class="payDivTile" style="width:40%; text-align:center;">적요</th>
										<th class="payDivTile" style="width:15%; text-align:center;">공급가액</th>
										<th class="payDivTile th_vat" style="width:15%; text-align:center;">부가세액</th>
										<th class="payDivTile th_aufnr" style="width:10%; text-align:center;">오더명</th>
										<th class="payDivTile th_accstartdt" style="width:16%; text-align: center;">선급비용-시작일</th>
										<th class="payDivTile th_accenddt" style="width:16%; text-align: center;">선급비용-종료일</th>
										<th class="payDivTile" style="width:6%; text-align:center;">삭제</th>
									</tr>
<!-- 								</thead> -->
									<c:forEach var="item" items="${itemList}" begin="0" end="${itemList.size()}" varStatus="status">
										<tr id="dynamicRow">
											<td class="td_costcenter"  style="width:100%">
												<div class="inp-wrap f-l" style="width:100%">
													<input type="text" class="inp-comm ar ktext" title="코스트센터" name="costCenter" value="${item.KTEXT }" readonly="readonly" placeholder="클릭하여 조회" />
													<input type="hidden" class="kostl" title="코스트센터코드" name="arrKostl" value="${item.KOSTL }" />
												</div>
											</td>
											<!-- s:G/L계정 -->	
											<td class="td_glaccount" style="width:100%">
												<div class="inp-wrap f-l" style="width: 100%;">
													<input type="text" class="inp-comm glName" title="GL계정" name="arrGlName" value="${item.GLNAME }" readonly="readonly" placeholder="클릭하여 조회" />
													<input type="hidden" class="glCode" name="arrGlCode" value="${item.GLCODE }" />
												</div>
											</td>
											<!-- e:G/L계정 -->
											<!-- s:적요 -->
											<td>
												<div class="inp-wrap" style="width:100%">
													<input type="text" class="inpTxt ar summary" title="적요" name="arrSummary" value="${fn:replace(item.SUMMARY, '"', '&quot;')}" maxlength="100" onclick="Grids.Focused = null;" />
												</div>
											</td>
											<!-- e:적요 -->
											<td>
												<div class="inp-wrap type02" style="width:90%;">
													<input type="text" class="inpTxt ar text_right onlyNumber itemAmt" style="text-align: right;" maxlength="12" title="공급가액" name="arrItemAmt" value="<fmt:formatNumber type="number" value="${item.ITEM_AMT}"/>" onclick="Grids.Focused = null;" />
													<span class="unit">원</span> 
												</div>
											</td>
											<td class="td_vat">
												<div class="inp-wrap type02" style="width:90%;">
													<input type="text" class="inpTxt ar text_right onlyNumber itemVat" style="text-align: right;" maxlength="12" title="세액" name="arrItemVat" value="<fmt:formatNumber type="number" value="${item.ITEM_VAT}"/>" onclick="Grids.Focused = null;" />
													<span class="unit">원</span> 
												</div>
											</td>
											<td class="td_aufnr">
												<div class="inp-wrap wd-per-100">
													<input type="hidden" class="aufnr" name="arrAufnr">
													<input type="text" class="inpTxt ar cur-po autxt" title="오더명" name="arrAutxt" placeholder="클릭하여 조회" readonly="readonly" />
													<input type="image" src="/images/com/web/textRemove.png" onclick="deleteText(${status.index}); return false;" class="textRemove">
												</div>
											</td>
											<td class="td_accstartdt">
												<div>
													<input type="text" class="inp-comm datePic moOpenCal accStartDt" title="선급비용-시작일" name="arrAccStartDt" value="${item.ACCSTARTDT }" readonly="readonly">
												</div>
											</td>
											<td class="td_accenddt">
												<div>
													<input type="text" class="inp-comm datePic moOpenCal accEndDt" title="선급비용-종료일" name="arrAccEndDt" value="${item.ACCSTARTDT }" readonly="readonly">
												</div>
											</td>
											<td class="td_del" style="text-align:center;">
												<div style="display:inline-block;">
													<input type="image" src="/images/com/web/textRemove.png" onclick="javascript:deleteItem(this); return false;" class="textRemove">
												</div>
											</td>
										</tr>
									</c:forEach>
								</tbody> 
							</table>
						</div>
					</form>

				</div>
            </div>
        </div>
    </div>
