<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : etcBillDtl.jsp
  Description : 일반전표 View화면
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.08.09    정순주              최초 생성

    author   : 정순주
    since    : 2017.08.09
--%>

<script type="text/javascript">
$(document).ready(function(){

	// 첨부파일
	var maxFileCnt = 10;
	for(var i=0; i<1; i++){
		$('#attachFile'+i).append('<input type="file" multiple id="ex_file'+i+'" class="ksUpload" name="fileList">');	
		var fileType = 'ppt |pps |pptx |ppsx |pdf |hwp |xls |xlsx |xlsm | xlt |doc |dot |docx |docm |txt |gif |jpg |jpeg |png';
		
		$('#ex_file'+i).MultiFile({
				accept : fileType
			,	list   : '#detailFileList'+i
			,	max    : maxFileCnt
			,	STRING : {
					remove: '<img src="/images/com/web/btn_remove.png" />'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
//						text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" >',
					idx	  : i,
					denied:'$ext는(은) 업로드할 수 없는 파일 확장자입니다.'
				}
		});
		
		$('.MultiFile-wrap').attr("id", "jquery"+i);
	}
	
	// 첨부파일 삭제
	$('.icn_fileDel').on('click', function(){
		delModiFile($(this));
	});
	
	// 파일 다운로드
	$('.btnFileDwn').on('click', function(){
		var ATTACH = $(this).data("attach");
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO=${data.ATTACH_GRP_NO}&ATTACH='+ATTACH;
		location.href = src;
	});
	
	// 초기화
	fn_setTable('${data.ACCGUBUN}');
	fn_setVatTd('${data.TAX_CD}');
	
});

/* 세금코드에 따라 세액 항목 show/hide 처리 */
function fn_setVatTd(tax_cd){
	// 과세,불공제의 경우 세액항목 show
	if(tax_cd == 'VA' || tax_cd == 'VB'){
		$(".th_vat").show();
		$(".td_vat").show();
		
	// 특정세금코드의 경우 세액항목 hide
	}else{
		$(".th_vat").hide();
		$(".td_vat").hide();
	}
}

/* 계정구분값에 따라 th, td show/hide */
function fn_setTable(accgubun){
	if(accgubun == '3'){	// 선급비용
		$(".th_accstartdt").show();
		$(".th_accenddt").show();
		$(".td_accstartdt").show();
		$(".td_accenddt").show();
		
		$(".th_vat").show();
		$(".td_vat").show();
		
		$(".th_aufnr").hide();
		$(".td_aufnr").hide();
		
		$(".th_costcenter").show();
		$(".td_costcenter").show();
		
	}else{	//경비
		$(".th_accstartdt").hide();
		$(".th_accenddt").hide();
		$(".td_accstartdt").hide();
		$(".td_accenddt").hide();
		
		$(".th_vat").show();
		$(".td_vat").show();
		
		$(".th_costcenter").show();
		$(".td_costcenter").show();
		
		$(".th_aufnr").hide();
		$(".td_aufnr").hide();
		
		// 선급비용시작일 초기화
		$('[name=arrAccStartDt]').each(function(){
			$(this).val('');
		});
		// 선급비용종료일 초기화
		$('[name=arrAccEndDt]').each(function(){
			$(this).val('');
		});
	}
}

/* 임시전표 삭제 처리 */
function deleteTmpEtcBill(){
	
	if(confirm("임시전표를 삭제하시겠습니까?")){
		
		$('#etcBillUptForm').attr("action","/hello/eacc/etcBill/deleteParkingEtcBill.do").ajaxSubmit(function(json){
			// 삭제 성공
			if(json.resultCd == "T"){
				alert(json.resultMsg);
				reLoadGrid();
				toggleModal($("#etcBillUptModal"));
			}else{
				alert(json.resultMsg);
			}	
		});
	}
}

/* 일반전표 증빙 수정 */
// function updateAttachFile(){
// 	if(confirm("증빙 파일을 수정하시겠습니까?")){
		
// 		var fileCnt = $("input[name=fileGb]").length;
// 		if(fileCnt > 0){
// 			$('#attachExistYn').val("Y");
// 		}else{
// 			$('#attachExistYn').val("N");
// 		}
// 		$('#fileCnt').val($(".MultiFile-label").length);
		
// 		$('#etcBillUptForm').attr('action', '/hello/eacc/etcBill/editFileProc.do').ajaxSubmit(function(json) {
// 			if(json.resultCd == "T"){
// 				alert(json.resultMsg);
// 				reLoadGrid();
// 				toggleModal($("#etcBillUptModal"));
// 			}else{
// 				if(typeof json.errorMsg == 'undefined' || json.errorMsg == 'undefined'){
// 					alert(json.resultMsg);
// 				}else{
// 					alert(json.resultMsg + "\n" + json.errorMsg);
// 				}
// 			}
// 		});
// 	}
// }

</script>

	<div class="modal-dialog root wd-per-80"> <!-- 원하는 width 값 입력 -->
        <div class="modal-content">
			<div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
               	<h4 class="modal-title">일반전표 상세</h4>
               	
               	<div style="position:absolute; top:13px; right:40px;">
					<c:if test="${page ne 'view'}">
						<!-- 결재상태가 저장이고 전표상태가 임시전표생성 상태인 경우 : 임시전표 삭제 버튼 -->
						<c:if test="${data.GW_STATUS eq '01' && data.BILL_STATUS eq 'V'}">
<!-- 								<a href="#" class="btn comm st03" style="margin-bottom: 5px;" onclick="updateAttachFile(); return false;">증빙수정</a> -->
							<a href="#" class="btn comm st01" style="margin-bottom: 5px; margin-right: 5px;" onclick="deleteTmpEtcBill(); return false;">임시전표 삭제</a>
						</c:if>
					</c:if>
				</div>
            </div>
            <div class="modal-body" >
				<div class="modal-bodyIn" >
					
					<form id="etcBillUptForm" action="" method="post" enctype="multipart/form-data">
						
						<!-- input : s -->
						<input type="hidden" name="etcBillNdx" value="${data.ETCBILL_NDX }" />
						<!-- 첨부파일 정보 -->
						<input type="hidden" title="파일등록여부" id="attachExistYn" name="attachExistYn" value=""/>
						<input type="hidden" title="삭제Seq" id="delSeq" name="delSeq" value=""/>
						<input type="hidden" title="파일 건수" id="fileCnt" name="fileCnt" value=""/>
						<input type="hidden" title="경비 모듈" id="MODULE" name="MODULE" value="8">
						<!-- input : e -->
							
						<div id="">							
							<!-- s:tb-wrap -->
							<div class="tb-wrap mgn-t-5">
								<table class="tb-st">
									<caption class="screen-out">일반 전표 VIEW</caption>
									<colgroup>
										<col width="13%">
										<col width="20%">
										<col width="13%">
										<col width="20%">
										<col width="13%">
										<col width="20%">
									</colgroup>
									<tbody>
										<tr>
											<!-- s:전기일 -->	
											<th scope="row">전표 구분</th>
											<td>
												<div>일반</div>
											</td>
											<!-- e:전기일 -->
											
											<!-- s:전기일 -->	
											<th scope="row">전기일</th>
											<td>
												<div>
													${fn:substring(data.BOOK_DT,0,4)}-${fn:substring(data.BOOK_DT,5,7)}-${fn:substring(data.BOOK_DT,8,10)}
												</div>
											</td>
											<!-- e:전기일 -->
											
											<!-- s:증빙일 -->
											<th scope="row">증빙일</th>
											<td>
												<div>
													${fn:substring(data.EVIDENCE_DT,0,4)}-${fn:substring(data.EVIDENCE_DT,5,7)}-${fn:substring(data.EVIDENCE_DT,8,10)}
												</div>
											</td>
											<!-- e:증빙일 -->
										</tr>
										<tr>
											<!-- s:총 금액 -->	
											<th scope="row">총 금액</th>
											<td>
												<div>
													<fmt:formatNumber type="number" value="${data.TOT_AMOUNT}"/>
													<span class="unit">원</span> 
												</div>
											</td>
											<!-- e:총 금액 -->
											<!-- s:총 공급가액 -->	
											<th scope="row">총 공급가액</th>
											<td>
												<div>
													<fmt:formatNumber type="number" value="${data.AMT_AMOUNT}"/>
													<span class="unit">원</span> 
												</div>
											</td>
											<!-- e:총 공급가액 -->
											<!-- s:총 부가세액 -->	
											<th scope="row">총 부가세액</th>
											<td>
												<div>
													<fmt:formatNumber type="number" value="${data.VAT_AMOUNT}"/>
													<span class="unit">원</span> 
												</div>
											</td>
											<!-- e:총 부가세액 -->
										</tr>
										
									</tbody>
								</table>
							</div>
							<!-- e:tb-wrap -->
						</div>
						
						<!-- s:tb-wrap -->
						<div class="tb-wrap mgn-t-10">
							<table class="tb-st">
								<colgroup>
									<col width="13%">
									<col width="20%">
									<col width="13%">
									<col width="20%">
									<col width="13%">
									<col width="20%">
								</colgroup>
								<tbody>
									<tr>
										<!-- s:부가세사업장 -->	
										<th scope="row">부가세사업장</th>
										<td>
											${data.WORKPLACE_NM }
										</td>
										<!-- e:부가세사업장 -->
										
										<!-- s:계정구분 -->	
										<th scope="row">계정구분</th>
										<td>
											${data.accgubunNm }
										</td>
										<!-- e:계정구분 -->
										
										<!-- s:세금코드 -->	
										<th scope="row">세금코드</th>
										<td>
											${data.taxCdNm }
										</td>
										<!-- e:세금코드 -->
									</tr>
									<tr>
										<!-- s:구매처 -->
										<th id="th_lifnr" scope="row">구매처</th>
										<td colspan="5">
											${data.PURCHASEOFFI_NM }
										</td>
										<!-- e:구매처 -->
									</tr>
									<tr>
										<!-- s:은행명 -->
										<th scope="row">은행명</th>
										<td id="td_banka">${data.BANK_NM }</td>
										<!-- e:은행명 -->
										
										<!-- s:계좌번호 -->
										<th scope="row">계좌번호</th>
										<td id="td_bankn">${data.ACCOUNT_NO }</td>
										<!-- e:계좌번호 -->
										
										<!-- s:지급예정일 -->	
										<th scope="row">지급예정일</th>
										<td id="td_paydueDt">
											<c:if test="${data.PAYDUE_DT ne '' && data.PAYDUE_DT ne null}">
												${fn:substring(data.PAYDUE_DT,0,4)}-${fn:substring(data.PAYDUE_DT,5,7)}-${fn:substring(data.PAYDUE_DT,8,10)}
											</c:if>
										</td>
										<!-- e:지급예정일 -->
									</tr>								
									<tr>
										<!-- s:추가직원 -->	
										<th scope="row">추가직원</th>
										<td colspan="5" data-name="lstArea1">
											<div id="addPrsnName">
												${data.ADD_PRSN_NM}
											</div>
										</td>
										<!-- e:추가직원 -->
									</tr>
									<tr>
										<!-- s:증빙 -->	
										<th scope="row">증빙</th>
										<td  colspan="5">
											<div class="bxType01 fileWrap">
<!-- 												<div id="attachFile" class="attachFile"></div> -->
												<div class="fileList">
													<div id="detailFileList" class="fileDownLst">
														<c:forEach var="item" items="${attachList}" varStatus="idx">
															<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
<!-- 																<span class="MultiFile-remove"> -->
<%-- 																	<a href="#none" class="icn_fileDel" data-attach="${item.ATTACH}" data-idx="${item.CD_FILE_TYPE }"> --%>
<!-- 																		<img src="/images/com/web/btn_remove.png" /> -->
<!-- 																	</a> -->
<!-- 																</span>  -->
																<span class="MultiFile-export">
																	${item.SEQ_DSP}
																</span> 
																<span class="MultiFile-title" title="File selected: ${item.NAME}">
																	${item.NAME}(${item.FILE_SIZE}kb)
																</span>
																<a href="#none" class="btnFileDwn icon download" data-attach="${item.ATTACH}">다운로드</a>
															</div>		
														</c:forEach>	
													</div>
												</div>
											</div>
										</td>
										<!-- e:증빙 -->
									</tr>
 									<c:if test="${apprItemList.size() > 0 }">
 										<tr>
 											<th>결재현황</th>
 											<td colspan="5">
 												<c:forEach var="item" items="${apprItemList}">
 													<ul class="expnSignLst type02" style="margin:5px 0 0 0;">
 														<li>
 															<p class="f-l">
																<a href="#none" class="seq">${item.APPR_SEQ}</a>
																<span>${item.APPR_USERNM} / ${item.APPR_DIVISIONNM}</span>
															</p>
															<p class="f-l mgn-l-5"><span style="text-align:center">${item.APPR_I_STSNM}</span></p>
															<c:if test="${!empty item.APPR_DT}">
																<p class="f-l mgn-l-5"><span>${item.APPR_DT}</span></p>
															</c:if>
															<c:if test="${!empty item.APPR_MEMO}">
																<p class="f-l mgn-l-5"><span>${item.APPR_MEMO}</span></p>
															</c:if>
 														</li>
 													</ul>
 												</c:forEach>
 											</td>
 										</tr>
  									</c:if>
								</tbody>
							</table>
						</div>
						<!-- e:tb-wrap -->
						
						<div class="tb-wrap mgn-t-10" style="width:100%;">
							<table id="tb_items" class="tb-st">
								<tbody>
									<tr>
										<th class="payDivTile th_costcenter" style="width:15%; text-align:center;">코스트센터</th>
										<th class="payDivTile" style="width:20%; text-align:center;">G/L 계정</th>
										<th class="payDivTile" style="width:40%; text-align:center;">적요</th>
										<th class="payDivTile" style="width:15%; text-align:center;">공급가액</th>
										<th class="payDivTile th_vat" style="width:15%; text-align:center;">부가세액</th>
										<th class="payDivTile th_aufnr" style="width:10%; text-align:center;">오더명</th>
										<th class="payDivTile th_accstartdt" style="width:16%; text-align: center;">선급비용-시작일</th>
										<th class="payDivTile th_accenddt" style="width:16%; text-align: center;">선급비용-종료일</th>
									</tr>
									<c:forEach var="item" items="${itemList}" begin="0" end="${itemList.size()}" varStatus="status">
										<tr id="dynamicRow">
											<td class="td_costcenter"  style="width:100%">
												${item.KTEXT}
											</td>
											<!-- s:G/L계정 -->	
											<td class="td_glaccount" style="width:100%">
												${item.GLNAME}
											</td>
											<!-- e:G/L계정 -->
											<!-- s:적요 -->
											<td>
												${fn:replace(item.SUMMARY, '"', '&quot;')}
											</td>
											<!-- e:적요 -->
											<td style="text-align: right;">
												<fmt:formatNumber type="number" value="${item.ITEM_AMT}"/>
												<span class="unit">원</span> 
											</td>
											<td class="td_vat" style="text-align: right;">
												<fmt:formatNumber type="number" value="${item.ITEM_VAT}"/>
												<span class="unit">원</span> 
											</td>
											<td class="td_aufnr">
												${item.AUTXT}
											</td>
											<td class="td_accstartdt" style="text-align: center;">
												${fn:substring(item.ACCSTARTDT,0,4)}-${fn:substring(item.ACCSTARTDT,5,7)}-${fn:substring(item.ACCSTARTDT,8,10)}
											</td>
											<td class="td_accenddt" style="text-align: center;">
												${fn:substring(item.ACCENDDT,0,4)}-${fn:substring(item.ACCENDDT,5,7)}-${fn:substring(item.ACCENDDT,8,10)}
											</td>
										</tr>
									</c:forEach>
								</tbody> 
							</table>
						</div>
					</form>

				</div>
            </div>
        </div>
    </div>