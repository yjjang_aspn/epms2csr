<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%--
  Class Name : noticeView.jsp
  Description : 공지사항 상세 화면
  Modification Information
 
      수정일                    수정자                   수정내용
   -------     --------    ---------------------------
   2017.08.16    윤선철              최초 생성

    author   : 윤선철
    since    : 2017.08.16
--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script src="/js/jquery/jquery-migrate-1.2.1.min.js" type="text/javascript" ></script>
<script type="text/javascript">
	var returnType = "";
	var chkBool = "";
	$(document).ready(function() {
		
		// 파일 다운로드
		$('.btnFileDwn').on('click', function(){
			var ATTACH = $(this).data("attach");
			var ATTACH_GRP_NO = $('#ATTACH_NDX').val();
			var src = '/attach/fileDownload.do?ATTACH_GRP_NO='+ ATTACH_GRP_NO + '&ATTACH='+ATTACH;
			$("#fileDownFrame").attr("src",src);
		});
		// Synap 첨부파일 뷰어
// 		$('.btnFileDwn').on('click', function(){
// 			var ATTACH = $(this).data("attach");
// 			var src = '/sys/attach/SynapDocView.do?ATTACH_GRP_NO=${item.ATTACH_GRP_NO}&ATTACH='+ATTACH;
// 			$("#fileDownFrame").attr("src",src);
// 		});
		
		$('#listBtn').on('click', function(){
			location.href = "/hello/eacc/notice/noticeList.do";
		});
		
		$('#updateBtn').on('click', function(){
			$('#NOTICE_NDX').val();
			fnSubmit("boardDetailForm", "/hello/eacc/notice/noticeEditForm.do");
		});
		
		$('#deleteBtn').on('click', function(){
			var crm = confirm("<spring:message code='noticeList.wantDelete' />"); // 게시글을 삭제 하시겠습니까?
			
			if(crm){
				$('#NOTICE_NDX').val();
				fnSubmit("boardDetailForm", "/hello/eacc/notice/deleteNotice.do");				
			}
		});
		
	});

</script>
</head>
<body>
<div class="scl"  style="margin-top:5px; height:99%">
	<div style="padding:15px;">
		<div class="fl-box panel-wrap top">
			<div class="panel-body">
				<div class="mgn-b-15">
				
					<form id="boardDetailForm" name="boardDetailForm" method="get">
						<input type="hidden" id="COMPANY_ID"  name="COMPANY_ID" value="${view.COMPANY_ID }"/>
						<input type="hidden" id="NOTICE_NDX"  name="NOTICE_NDX" value="${view.NOTICE_NDX }"/>
						<input type="hidden" id="ATTACH_NDX"  name="ATTACH_NDX" value="${view.ATTACH_NDX }"/>
						<input type="hidden" name="page" value="${view.page }"/>
					</form>
	
					<div class="overflow">
						<div class="f-l">
							<a href="#none" id="listBtn" class="btn comm st02"><spring:message code='button.list' /></a> <!-- 목록 -->
						</div>
						<c:if test="${auth eq 'Y'}">
							<div class="f-r">
								<a href="#none" id="updateBtn" class="btn comm st01"><spring:message code='button.modify' /></a> <!-- 수정 -->
								<a href="#none" id="deleteBtn" class="btn comm st02"><spring:message code='button.delete' /></a> <!-- 삭제 -->
							</div>
						</c:if>
					</div>
					
					<div class="viewBoardWrap mgn-t-10">
						<h3 class="viewBoardTit">${view.TITLE }</h3>
						<div class="viewBoardInfo">
							<c:choose>
								<c:when test="${view.TYPE eq '1'}">
									<p class="f-l" style="color:blue; font-weight: bold;"><spring:message code='notice.type.general' /></p> <!-- 일반공지 -->
								</c:when>
								<c:otherwise>
									<p class="f-l" style="color:red; font-weight: bold;"><spring:message code='notice.type.pop' /></p> <!-- 팝업공지 -->
								</c:otherwise>
							</c:choose>
							<p class="f-l"><span>${view.user_nm }</span><span>${view.REG_DT }</span></p>
						</div>
						<div class="viewBoardCon">
							<c:if test="${!empty attachList }">
								<div class="attachFileWrap">
									<div class="attachFileTit">
										<a href=#none" class="down ir-pm"><spring:message code='file.openFile' /></a><!-- 첨부파일 펼치기 -->
										<p><spring:message code='file.file' /><span>${attachSize}<spring:message code='file.fileCnt' /></span></p>
									</div>
									<ul class="attachFileLst" style="display:none;">
										<c:forEach var="item" items="${attachList}" varStatus="idx">
											<li>
												<a href="#none" class="attachDownBtn btnFileDwn" data-attach="${item.ATTACH}">
													<img src="/images/icon/web/attachDownload.gif">
													<span>${item.NAME}(${item.FILE_SIZE}kb)</span>
												</a>
											</li>
										</c:forEach>
									</ul>
								</div>
							</c:if>
							<div class="viewContent">
								<!-- 정규식 치환 -->
								${fn:replace(fn:replace(fn:replace(fn:replace(fn:replace(view.CONTENT,'<p>',''),'</p>','<br/>'),'&quot;','"'),'&lt;','<'),'&gt;','>')}
							</div>
						</div>
					</div>
	
					<iframe id="fileDownFrame" style="width:0px;height:0px;border:0px"></iframe>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>