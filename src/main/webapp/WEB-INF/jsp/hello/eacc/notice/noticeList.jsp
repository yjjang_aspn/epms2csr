<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%--
  Class Name : noticeList.jsp
  Description : 공지사항 목록 화면
  Modification Information
 
      수정일                    수정자                   수정내용
   -------     --------    ---------------------------
   2017.08.14    윤선철              최초 생성

    author   : 윤선철
    since    : 2017.08.14
--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script src="/js/jquery/jquery-filestyle.js" type="text/javascript" ></script>
<script src="/js/jquery/jquery.MultiFile.js" type="text/javascript"></script>
<script src="/js/jquery/jquery.ajaxsubmit.js" type="text/javascript"></script>

<!-- <link href="/css/eacc/eacc_common.css" rel="stylesheet" />	 -->
<script type="text/javascript">
	var returnType = "";
	var chkBool = "";
	$(document).ready(function() {
		$('.pagingWrap a[page]').click(function(){
			var page = $(this).attr('page');
			
			$('input[name=page]').val(page);

			fnSubmit("boradListForm", "/hello/eacc/notice/noticeList.do");
		});
		
		$('.tbTitle').click(function(){
			$('#NOTICE_NDX').val($(this).data("content"));
			location.href = "/hello/eacc/notice/noticeView.do?NOTICE_NDX=" + $('#NOTICE_NDX').val();
// 			fnSubmit("boradListForm", "/project/eaccounting/notice/web/noticeView.do");
		});
		
		$('#regBtn').click(function(){
			location.href = "/hello/eacc/notice/noticeWriteForm.do";
// 			fnSubmit("boradListForm", "/hello/eacc/notice/noticeWriteForm.do");
		});
		
		$('#srcBtn').click(function(){
			fnSrcList();
		});
		
		/* 달력 */
		$('.datePic').glDatePicker({
			showAlways:false,
			cssName:'flatwhite',
			allowMonthSelect:true,
			allowYearSelect:true,
			onClick:function(target, cell, date, data){
				target.val(date.getFullYear() + '' + getDate(parseInt(date.getMonth())+1) + '' + getDate(date.getDate()));
			}
		}).each(function(){
			if($(this).hasClass('thisMonth')){
				var date = new Date();
				if('${param.startDt}' == ''){
					$(this).val(date.getFullYear() + '' + getDate(parseInt(date.getMonth())+1) + '01');	
				}else{
					$(this).val('${param.startDt}');
				}
				
			}else if($(this).hasClass('noneType') == false){
				var date = new Date();
				if('${param.endDt}' == ''){
					$(this).val(date.getFullYear() + '' + getDate(parseInt(date.getMonth())+1) + '' + getDate(date.getDate()));	
				}else{
					$(this).val('${param.endDt}');
				}
			}
		});
	});
	
	function fnSrcList(){

// 		if($('#searchVal').val() == ""){
// 			alert("검색 할 단어를 입력하시기 바랍니다.");
// 			$('#searchVal').focus();
// 			return;
// 		}
// 		}

	    $('#page').val("1");
		fnSubmit("boradListForm", "/hello/eacc/notice/noticeList.do");
	}
</script>

<style>
	.tb-st th, .tb-st td {border-right:none;}
</style>
</head>
<body>
<div class="scl"  style="height:100%">
	<div style="padding:15px;">
		<div class="fl-box panel-wrap top">
			<h5 class="panel-tit"><spring:message code='title.notice' /></h5> <!-- 공지사항 -->
			<div class="panel-body mgn-t-20">
				<div>
					
					<form id="boradListForm" name="boradListForm" method="get">
						<input type="hidden" name="page" value="${page }"/>
						<input type="hidden" id="module" name="MODULE" value="8"/>
						<input type="hidden" id=NOTICE_NDX name="NOTICE_NDX" value=""/>
		
						<!-- s:inq-area01 -->
						<div class="inq-area-top type03 mgn-t-10">
							<ul class="wrap-inq type01">
	<!-- 							<li class="inq-clmn"> -->
	<!-- 								<h4 class="tit-inq">검색기간</h4> -->
	<!-- 								<div class="prd-inp-wrap"> -->
	<!-- 									<span class="prd-inp"> -->
	<!-- 										<span class="inner"> -->
	<!-- 											<input type="text" class="inp-comm datePic inpCal thisMonth" readonly="readonly" id="startDt" name="startDt" /> -->
	<!-- 										</span> -->
	<!-- 									</span> -->
	<!-- 									<span class="prd-inp"> -->
	<!-- 										<span class="inner"> -->
	<!-- 											<input type="text" class="inp-comm datePic inpCal" readonly="readonly" id="endDt" name="endDt" />  -->
	<!-- 										</span> -->
	<!-- 									</span> -->
	<!-- 								</div>									 -->
	<!-- 							</li> -->
								<li class="inq-clmn">
									<h4 class="tit-inq"><spring:message code='title.notice.type' /></h4> <!-- 공지 유형 -->
									<div class="sel-wrap type02">
										<select title="공지 유형" id="TYPE" name="TYPE">
											<c:if test='${search.TYPE eq ""}'>
												<option value="" selected="selected"><spring:message code='notice.type.total' /></option> <!-- 전체 -->
												<option value="1"><spring:message code='notice.type.general' /></option> <!-- 일반공지 -->
												<option value="2"><spring:message code='notice.type.pop' /></option> <!-- 팝업공지 -->
											</c:if>
											<c:if test='${search.TYPE eq "1" }'>
												<option value="" ><spring:message code='notice.type.total' /></option>
												<option value="1" selected="selected"><spring:message code='notice.type.general' /></option>
												<option value="2"><spring:message code='notice.type.pop' /></option>
											</c:if>
											<c:if test='${search.TYPE eq "2"}'>
												<option value=""><spring:message code='notice.type.total' /></option>
												<option value="1"><spring:message code='notice.type.general' /></option>
												<option value="2" selected="selected"><spring:message code='notice.type.pop' /></option>
											</c:if>
										</select>
									</div>
								</li>
								<li>
									<div class="sel-wrap type02">
										<select title="검색" id="searchKey" name="searchKey">
											<c:if test='${search.searchKey eq "" or search.searchKey eq "total"}'>
												<option value="total" selected="selected"><spring:message code='notice.search.total' /></option>
												<option value="user_id"><spring:message code='notice.search.userId' /></option>
											</c:if>
											<c:if test='${search.searchKey eq "user_id"}'>
												<option value="total" ><spring:message code='notice.search.total' /></option>
												<option value="user_id" selected="selected"><spring:message code='notice.search.userId' /></option>
											</c:if>
										</select>
									</div>
								</li>
								<li>
									<input type="text" class="inp-wrap type03" id="searchVal" name="searchVal" value="${search.searchVal }"/>
								</li>
							</ul>
							<a href="#" class="btn comm st01"  id="srcBtn"><spring:message code='button.search' /></a> <!-- 검색 -->
							<c:if test="${auth eq 'Y' }">
								<div class="f-r">
									<a href="#none" id="regBtn" class="btn comm st01"><spring:message code='button.create' /></a> <!-- 등록 -->
								</div>
							</c:if>
						</div>
						<!-- e:inq-area01 -->
						
						<div class="tb-wrap type03 mgn-t-10">
							<table id="boardMngList" class="tb-st type02">
								<caption>공지사항 관리 테이블입니다.</caption>
								<colgroup>
									<col width="7%"/>
									<col width="10%"/>
									<col width="*"/>
									<col width="10%"/>
									<col width="12%"/>
									<col width="7%"/>
								</colgroup>
								<thead>
									<tr>
										<th><spring:message code='notice.table.number' /></th> <!-- 번호 -->
										<th><spring:message code='notice.table.type' /></th> <!-- 유형 -->
										<th><spring:message code='notice.table.title' /></th> <!-- 제목 -->
										<th><spring:message code='notice.table.userId' /></th> <!-- 작성자 -->
										<th><spring:message code='notice.table.userDt' /></th> <!-- 작성일 -->
										<th><spring:message code='notice.table.file' /></th> <!-- 파일 -->
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${empty list }">
											<tr>
												<td colspan="6">
													<spring:message code='noticeList.noList' /> <!-- 등록된 리스트가 없습니다.  -->
												</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:forEach var="item" items="${list}">
												<tr id="boardId_13">
													<td>${(totalCnt - item.RN) + 1 }</td>
													<c:if test="${item.TYPE eq '1'}">
														<td><spring:message code='notice.type.general' /></td> <!-- 일반 공지 -->
													</c:if>
													<c:if test="${item.TYPE eq '2'}">
														<td><spring:message code='notice.type.pop' /></td> <!-- 팝업 공지 -->
													</c:if>
													<td class="tbTitle" style="cursor:pointer; text-align:left;" data-content="${item.NOTICE_NDX }">${item.TITLE }</td>
													<td>${item.user_nm }</td>
													<td>${item.REG_DT }</td>
													<td>
														<c:if test="${item.ATTACH > 0 }">
															<img src="/images/icon/web/fileBtn.png" alt="<spring:message code='button.file' />" class="fileBtn">
														</c:if>
													</td>
												</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
						</div>
					</form>
				${pageHTML}
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
<!-- 페이지 내용 : e -->
