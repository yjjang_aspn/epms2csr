<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%--
  Class Name : noticeWriteForm.jsp
  Description : 공지사항 작성 화면
  Modification Information
 
      수정일                    수정자                   수정내용
   -------     --------    ---------------------------
   2017.08.14    윤선철              최초 생성

    author   : 윤선철
    since    : 2017.08.14
--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/js/jquery/jquery.MultiFile.js"></script>
<script type="text/javascript" src="/js/jquery/jquery-filestyle.js"></script>
<script type="text/javascript">
	var maxFileCnt = 10;
	var fileVal = "";
	var attachGrpNo = "";
	$(document).ready(function() {
		//공지 유형 default 세팅
		$('#popUpType').hide();
		//ckEditor 세팅
		dockeditor();
				
		/* 달력 */
		$('.datePic').glDatePicker({
			showAlways:false,
			cssName:'flatwhite',
			allowMonthSelect:true,
			allowYearSelect:true,
			onClick:function(target, cell, date, data){
				target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
			}
		}).each(function(){
			if($(this).hasClass('thisMonth')){
				var date = new Date();
				$(this).val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-01');
			}else if($(this).hasClass('noneType') == false){
				var date = new Date();
				$(this).val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
			}
		});
		
		$('#saveBtn').on('click', function(){
 			fnValidate();
		});
		
		$('#listBtn').on('click', function(){
			location.href = "/hello/eacc/notice/noticeList.do";
			fnSubmit("boardForm", "/hello/eacc/notice/noticeList.do");	
		});
		
		$('.icn_fileDel').on('click', function(){
			delModiFile($(this));
		});

		//공지사항 유형에 따른 동적 작성 화면 
		$('#TYPE').on('change', function(){
			if($(this).val() == '1'){
				$('#popUpType').hide();
			}else{
				$('#popUpType').show();
			}
		});
		
		for(var i=0; i<1; i++){
			$('#attachFile'+i).append('<input type="file" multiple id="ex_file'+i+'" class="ksUpload" name="fileList">');	
			var fileType = 'ppt |pps |pptx |ppsx |pdf |hwp |xls |xlsx |xlsm | xlt |doc |dot |docx |docm |txt |gif |jpg |jpeg |png';
			
			$('#ex_file'+i).MultiFile({
					accept : fileType
				,	list   : '#detailFileList'+i
				,	max    : maxFileCnt
				,	STRING : {
						remove: '<img src="/images/com/web/btn_remove.png">'
							   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
//							text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" >',
						idx	  : i,
						denied:'$ext <spring:message code="errors.file" />'
					}
			});
			
			$('.MultiFile-wrap').attr("id", "jquery"+i);
		}
	});
	
	function fnValidate(){
		if( !isNull_J($('#TITLE'), "<spring:message code='noticeList.noTitle' />") ) { // 제목을 입력하셔야 합니다.
			return false;
		}
		
		var data = CKEDITOR.instances.CONTENT.getData();
		$("#CONTENT").val(data);
		if(data == ""){
			alert("<spring:message code='noticeList.noContent' />"); // 본문을 입력하셔야 합니다.
			return;
		}
		
		if(confirm("<spring:message code='conmonmsg.wantRegister' />")){ // 등록 하시겠습니까?
			
			var fileCnt = $("input[name=fileGb]").length;
			if(fileCnt > 0){
				$('#attachExistYn').val("Y");
			}else{
				$('#attachExistYn').val("N");
			}
			//공지 유형에 따른 세팅
			if($('#TYPE').val() == "1"){
				$('#POPSTART_DT').val('');
				$('#POPEND_DT').val('');
				$('#POPYN').val('N');
			}else{
				$('#POPYN').val('Y');
			}
			fnSubmit("boardForm","/hello/eacc/notice/insertNotice.do");
		}
		
	}
	
	function delModiFile(obj){
		var attach = obj.data("attach");
		var idx = obj.data('idx');
		
		$('#fileLstWrap_'+attach).remove();
		
		//fileNo set
		if(fileVal != '') {
			fileVal += ",";
		}
		
		fileVal += attach;
		$('#delSeq').val(fileVal);
	}
	
	function dockeditor() {
	    var ckeditor_config = {
	            resize_enabled: true,   
	            enterMode: CKEDITOR.ENTER_BR,
	            toolbarCanCollapse: true, 
	            toolbar: [
	                ['Source', 'NewPage', 'DocProps', 'Preview', 'Print', 'Templates', 'document'],
	                ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', 'Undo', 'Redo'],
	                ['Find', 'Replace', 'SelectAll', 'Scayt'],
	                ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'RemoveFormat'],
	                ['NumberedList', 'BulletedList', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'BidiLtr', 'BidiRtl'],
	                ['Styles', 'Format', 'Font', 'FontSize'],
	                ['TextColor', 'BGColor']
	            ],
	            title: false,
	            autoParagraph: false,
	            enableTabKeyTools: false,
	            useComputedState: false,
	            removePlugins: 'elementspath', 
	            fillEmptyBlocks: false
	            
	    }
	    CKEDITOR.replace('CONTENT', {//해당 이름으로 된 textarea에 에디터를 적용
									width:'100%',
									height:'510px',
									fillEmptyBlocks: false,
									extraPlugins: 'tableresize,table,tabletools,tableselection,colordialog,widget,widgetselection,lineutils,dialog,image2,justify,lineheight,colorbutton,panelbutton,font,pastebase64',
									filebrowserImageUploadUrl: '/attach/editor/imageUpload.do?folder=board&ATTACH_GRP_NO='+attachGrpNo
								 }
						);
	    CKEDITOR.on('dialogDefinition', function (ev) {
			var dialogName = ev.data.name;
			var dialog = ev.data.definition.dialog;
			var dialogDefinition = ev.data.definition;

				if (dialogName == 'image') {
					dialog.on('show', function (obj) {
					this.selectPage('Upload'); //업로드텝으로 시작
				});

				dialogDefinition.removeContents('advanced'); // 자세히탭 제거
				dialogDefinition.removeContents('Link'); // 링크탭 제거
			}
		});
	}
	
	function close(){
		//해당 레이어 팝업 (속성 값 처리) 닫기
		CKEDITOR.dialog.getCurrent().hide()
	}
		
</script>
</head>
<body>
<div class="scl"  style="margin-top:5px; height:99%">
	<div style="padding:15px;">
		<div class="overflow">
			<div class="f-l">
				<a href="#none" id="listBtn"  class="btn comm st02"><spring:message code='button.back' /></a> <!-- 이전 -->
			</div>
			<div class="f-r">
				<a href="#none" id="saveBtn" class="btn comm st01"><spring:message code='button.create' /></a> <!-- 등록 -->
			</div>
		</div>
			
		<div class="tb-wrap mgn-t-10">
			<form id="boardForm" name="boardForm" method="post" enctype="multipart/form-data">
				<input type="hidden" id="module" name="MODULE" value="80" /><!-- 공지사항 -->
				<input type="hidden" id="delSeq" name="delSeq" value=""/>
				<input type="hidden" id="attachExistYn" name="attachExistYn" value=""/>
				<input type="hidden" id="POPYN" name="POPYN" value=""/>
				
				<table class="tb-st type01">
					<caption>공지사항 작성</caption>
					<colgroup>
						<col width="15%"/>
						<col width="35%"/>
						<col width="15%"/>
						<col width="35%"/>
					</colgroup>
					<tbody>
						<tr>
							<th><spring:message code='notice.table.type' /></th> <!-- 유형 -->
							<td>
								<div class="sel-wrap">
									<select title="공지 유형" id="TYPE" name="TYPE">
										<option value="1" selected="selected"><spring:message code='notice.type.general' /></option> <!-- 일반공지 -->
										<option value="2"><spring:message code='notice.type.pop' /></option> <!-- 팝업공지 -->
									</select>
								</div>
							</td>
						</tr>
						<tr>
							<th><spring:message code='notice.table.title' /></th> <!-- 제목 -->
							<td colspan="3"><input type="text" class="inp-wrap wd-per-100" id="TITLE" name="TITLE" placeholder="<spring:message code='noticeList.ph.enterTitle' />"/></td> <!-- 제목을 입력해주세요 -->
						</tr>
						<tr id=popUpType>
							<th><spring:message code='notice.table.startPop' /></th> <!-- 공지시작일 -->
							<td>
								<span class="inner">
									<input type="text" class="inp-comm datePic openCal" readonly="readonly" id="POPSTART_DT" name="POPSTART_DT" placeholder="<spring:message code='noticeList.ph.startPop' />"/>
								</span>
							</td>
							<th><spring:message code='notice.table.endPop' /></th> <!-- 공지종료일 -->
							<td>
								<span class="inner">
									<input type="text" class="inp-comm datePic openCal" readonly="readonly" id="POPEND_DT" name="POPEND_DT" placeholder="<spring:message code='noticeList.ph.endPop' />"/> 
								</span>
							</td>
						</tr>
						<tr>
							<th><spring:message code='notice.table.content' /></th> <!-- 본문 -->
							<td colspan="3" style="height:510px;"> <!-- height값을 지정해서 사용해주세요 -->
								<textarea class="inpTxt" id="CONTENT" name="CONTENT" rows="10" cols="50"></textarea>
							</td>
						</tr>
						<tr>
							<th scope="row"><spring:message code='notice.table.file' /><br><span class="pd-l-10"><spring:message code='file.type' /></span></th> <!-- 파일첨부(jpg,pdf,doc,xls 등) -->
							<td colspan="3" style="padding:3px 15px 0;">
								<div class="bxType01 fileWrap">
									<div class="filebox" id="attachFile0"></div>
									<div class="fileList" style="padding:7px 0 5px;">
										<div id="detailFileList0" class="fileDownLst">
											
										</div>
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
	</div>
</div>	
</body>
</html>