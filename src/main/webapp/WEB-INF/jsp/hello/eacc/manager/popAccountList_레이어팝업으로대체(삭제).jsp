<%@ page language="java" contentType="text/html; charset=UTF-8;" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Class Name : popAccount.jsp
  Description : 계정분류 GL 계정 추가  Pop-Up 
  Modification Information
 
       수정일         		수정자                수정내용
   -------    --------    ---------------------------
   2017.08.16    정호윤              최초 생성

    author   : 정호윤
    since    : 2017.08.16
--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script type="text/javascript">	

/**
 * 그리드 load 이벤트 핸들러
 */
Grids.OnReady = function(grid){
	
};

Grids.OnClick = function(grid, row, col){
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar"){
		return;
	}
};

function addRowGrid(gridNm){
	var glCode = "";
	var accountNdx = "${accountNdx}";
	var gubunDiv = '〔';
	
	for(var row = Grids[gridNm].GetFirst(); row; row = Grids[gridNm].GetNext(row)){
		if(row.SELECT_YN == '1'){
			if(glCode == null || glCode == ''){
				glCode = row.GLCODE;
			}else{
				glCode += gubunDiv + row.GLCODE;
			}
		}
	}
	
	if(confirm("등록하시겠습니까?")){
		$.ajax({
			type: 'POST',
			url: '/hello/eacc/manager/insertGlCodeAjax.do',
			dataType: 'json',
			data : {glCode : glCode, accountNdx : accountNdx},
			success: function (json) {
				if(json.result == "T"){
					window.close();
					opener.fn_reloadGrid();
				}else{
					alert("GL 계정 등록 중 오류가 발생하였습니다.");
					window.close();
				}
			},error: function(XMLHttpRequest, textStatus, errorThrown){
				alert("GL 계정 등록 중 오류가 발생하였습니다.");
				window.close();
			}
		});
	}
}
</script>
</head>
<body>
	<div class="wrapper">
		<div id="header" class="pop-header">
			<h3>GL계정 추가</h3>	
		</div>
		<!-- e:pop-header -->
		<div id="main" class="pop-body">
			<section id="section">
				<div id="container">
					<div class="cont-inner">
						<!-- 페이지 내용 : s -->
						<div class="fl-box panel-wrap" style="height: 380px;"><!-- 원하는 비율로 직접 지정하여 사용 -->
							<div class="panel-body">
								<!-- 트리그리드 : s -->
								<bdo	Debug="Error"
										Data_Url="/hello/eacc/manager/getPopAccountData.do?accountNdx=${accountNdx}"
										Layout_Url="/hello/eacc/manager/popAccountLayout.do"
										Upload_Url="/hello/eacc/manager/popAccountEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols" Upload_Xml="2">
										>
								</bdo>
								<!-- 트리그리드 : e -->
							</div>
						</div>
						<!-- e:panel-wrap01 -->
						<!-- 페이지 내용 : e -->
					</div>
				</div>
			</section>
		</div>
		<!-- e:pop-body -->
	</div>
	<!-- e:wrapper -->
</body>
</html>
