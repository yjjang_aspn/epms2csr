<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%--
  Class Name : configure.jsp
  Description : 인터페이스, 마감일 관리 페이지
  Modification Information
  
  	수정일			수정자			수정내용
  ------			------			--------
  2017.08.24		강승상			최초생성
  2017.09.18		정호윤			경과일 관리 추가
  
  author	: 강승상
  since		: 2017.08.24
--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<link rel="stylesheet" href="/css/eacc/modalPopup.css">
<script src="/js/eacc/eaccCommon.js"></script>
<script src="/js/com/web/modalPopup.js"></script>
<title><spring:message code='title.configure' /></title>
<script type="text/javascript">
var layerGubun = "addPerson"; // 레이어 팝업 구분자(추가직원-addPerson, 이관-transPerson)
	$(document).ready(function() {
		$('.datePic').glDatePicker({
			showAlways:false,
			cssName:'flatwhite',
			allowMonthSelect:true,
			allowYearSelect:true,
			onClick:function(target, cell, date, data){
				target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
			}
		}).each(function(){
			if (!$(this).val()) {    /* 데이터가 없을 경우 현재 값 셋팅 */
				if($(this).hasClass('thisMonth')){
				var date = new Date();
				$(this).val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '01');
			} else {
				var date = new Date();
				$(this).val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
			}}
		});

		$(':radio').click(function() {
			var thisId = $(this).attr('id');
			var showTarget = $(this).data('target');
			if(thisId.match(/usen/i)){
				$('.' + showTarget).hide();
			} else {
				$('.' + showTarget).show();
			}
		});
	});

	function fn_saveDeadline(){
		var $form = $('#deadlineForm');
		if(isValidate($form)
			&& confirm("<spring:message code='conmonmsg.wantSave' />")) { // 저장하시겠습니까?
			$form.submit();
		}
	}

	/* 현재 미입력 상황이 발생하지 않으므로 임시 주석 처리 */
	function isValidate($form) {
		var errMsg = '';
		/* $form.find(':radio:checked').each(function () {
			var name = $(this).attr('name').replace('_USE', '');
			var $text = $(':text[name="^' + name + '"');
			if($(this).val() === '1') { /!* 사용 check *!/
				$text.each(function() {
					if ($(this).val() === '' || $(this).val == undefined) {
						errMsg =  name + "<spring:message code='configure.useValidation' />"; //사용으로 설정하실때는 날자입력이 필수입니다.
						return false;
					}
				});
			} else {                    /!* 미사용 check *!/

			}
			//console.log(name + ' / ' + $(this).val());
		}); */
		
		// 지급일 사용
		if($(':radio[name="PAY_USE"]:checked').val() === '1') {
			var payDT = $('#PAY_DT').val().trim();
			if(!payDT.match(/^([1-9]|[12][0-9]|3[01])$/)) {
				alert('[<spring:message code="title.configure.billPayDate" />] <spring:message code="configure.onlyRangeNumber" />'); 		//[경비지급일] 숫자(0~31)값을 입력해 주십시오.
				return false;
			}
		}
		
		// 경과일 사용
		if($(':radio[name="ELAPSE_USE"]:checked').val() === '1') {
			var payDT = $('#ELAPSE_DT').val().trim();
			if(!payDT.match(/^([1-9]|[12][0-9]|3[01])$/)) {
				alert('[<spring:message code="title.configure.elapseDate" />] <spring:message code="configure.onlyRangeNumber" />'); 		//[경과일] 숫자(0~31)값을 입력해 주십시오.
				return false;
			}
		}
		
		return true;
	}
	
	//사용자 조회
	function getMemberList(divNo, curLvl, company_id){
		Grids.PopMemberDivList.Source.Data.Url = "/hello/eacc/common/popMemberDivData.do?divNo="+divNo+"&curLvl="+curLvl+"&COMPANY_ID="+company_id;
		Grids.PopMemberDivList.ReloadBody();	
	}
	
	function fn_addMember(gridNm){
		<%--사원 추가 --%>
		var count = $("#addPrsnName > li:last").data('idx');;
		var userId = $('#WRITE_EXCEPT_ID').val();
		var html = "";

		for(var row = Grids[gridNm].GetFirst(); row; row = Grids[gridNm].GetNext(row)){
			if(row.SELECT_YN == '1'){
				if(userId.indexOf(row.USER_ID) == -1){
					count++;
					if(userId == "" || userId == null){
						userId = row.USER_ID;
					}else{
						userId += ',' + row.USER_ID;
					}
	
					html += '	<li id="writeExceptList_' + count +'" data-idx=' + count + '>'
						 +  '		<p><span>'+row.NAME+' / '+row.DIVISION_NM+'</span><a href="#none" class="del" onclick="fn_deleteExceptMember(' + count + '); return false;">X</a></p>'
						 +  '		<input type="hidden" id="writeUserId_' + count + '" name="writeUserId" value="' + row.USER_ID + '"/>'
						 +  '	</li>';
				}else{
					alert("<spring:message code='configure.alreadyInsertMember' /> [" + row.NAME + "]"); // 이미 등록된 사용자입니다.
					return;
				}
			}
		}
		
		$('#addPrsnName').append(html);
		$('#WRITE_EXCEPT_ID').val(userId);
		
		Grids.PopMemberDivList.ReloadBody();
		layer_open('memberModal');
	}
	
	function fn_deleteExceptMember(idx){
		$('#writeExceptList_' + idx).remove();
		
		var userId = "";
		$('[name=writeUserId]').each(function(){
			if(userId == "" || userId == null){
				userId = $(this).val();
			}else{
				userId += ',' + $(this).val();
			}
		});
		$('#WRITE_EXCEPT_ID').val(userId);
		
	}
</script>
<style>
	.floatWrap:after {content:''; clear:both;}
</style>
</head>
<body>
<div class="scl"  style="margin-top:5px; height:99%">
	<div class="floatWrap" style="padding:15px;">
		
		<form action="" method="post" id="deadlineForm">
			<!-- Left Area Start  -->
			<div class="f-l panel-wrap03 rel" style="width:50%;">
				<!-- Left Title Area Start  -->
				<h5 class="panel-tit"><spring:message code='title.configure' /></h5>
				<a href="#" class="btn comm st01" style="position:absolute; top:3px; right:10px;" onclick="fn_saveDeadline(); return false;" ><spring:message code='button.save' /></a>
				<!-- Left Title Area End    -->
		
				<!-- Left Grid Area Start   -->
				<div class="panel-body">
					<div class="tb-wrap mgn-t-40">
						<table class="tb-st mgn-r-5">
							<colgroup>
								<col width="20%" />
								<col width="80%" />
							</colgroup>
							<tbody>
								<tr>
									<th style="text-align: center;">
										<spring:message code='title.configure.billWriteDeadLine' /><br><spring:message code='title.configure.evidenceStandard' />
									</th>
									<td style="height:100px;">
										<div class="tb-row">
											<span class="rdo-st">
												<input type="radio" name="WRITE_USE" id="writeUseY" value="1" <c:if test="${deadline.WRITE_USE eq '1'}"> checked </c:if> data-target="mk-write">
												<label for="writeUseY" class="label"><spring:message code='button.use' /></label>
											</span>
											<span class="rdo-st mgn-l-10">
												<input type="radio" name="WRITE_USE" id="writeUseN" value="0" <c:if test="${deadline.WRITE_USE eq '0'}"> checked </c:if> data-target="mk-write">
												<label for="writeUseN"><spring:message code='button.notUse' /></label>
											</span>
										</div>
										<div class="wrap-inq mgn-t-5 mk-write" <c:if test="${deadline.WRITE_USE eq '0'}"> style="display: none;" </c:if>>
											<div class="prd-inp-wrap" style="width:400px">
												<span class="prd-inp">
													<span class="inner">
														<input type="text" class="inp-comm datePic openCal" title="전표작성 마감일 시작" id="WRITE_STARTDT" name="WRITE_STARTDT" value="${deadline.WRITE_STARTDT}" readonly="readonly">
													</span>
												</span>
												<span class="prd-inp">
													<span class="inner">
														<input type="text" class="inp-comm datePic openCal" title="전표작성 마감일 종료" id="WRITE_ENDDT" name="WRITE_ENDDT" value="${deadline.WRITE_ENDDT}" readonly="readonly">
													</span>
												</span>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th style="text-align: center;">
										<spring:message code='title.configure.billApprovalDeadLine' /><br><spring:message code='title.configure.postingStandard' />
									</th>
									<td style="height:80px;">
										<div class="tb-row">
											<span class="rdo-st">
												<input type="radio" name="APPR_USE" id="apprUseY" value="1" <c:if test="${deadline.APPR_USE eq '1'}"> checked </c:if> data-target="mk-appr">
												<label for="apprUseY" class="label"><spring:message code='button.use' /></label>
											</span>
											<span class="rdo-st mgn-l-10">
												<input type="radio" name="APPR_USE" id="apprUseN" value="0" <c:if test="${deadline.APPR_USE eq '0'}"> checked </c:if> data-target="mk-appr">
												<label for="apprUseN"><spring:message code='button.notUse' /></label>
											</span>
										</div>
										<div class="wrap-inq mgn-t-5 mk-appr" <c:if test="${deadline.APPR_USE eq '0'}"> style="display: none;" </c:if>>
											<div class="prd-inp-wrap" style="width:400px">
												<span class="prd-inp">
													<span class="inner">
														<input type="text" class="inp-comm datePic openCal" title="전표승인 마감일 시작" id="APPR_STARTDT" name="APPR_STARTDT" value="${deadline.APPR_STARTDT}" readonly="readonly">
													</span>
												</span>
												<span class="prd-inp">
													<span class="inner">
														<input type="text" class="inp-comm datePic openCal" title="전표승인 마감일 시작" id="APPR_ENDDT" name="APPR_ENDDT" value="${deadline.APPR_ENDDT}" readonly="readonly">
													</span>
												</span>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th style="text-align: center;">
										<spring:message code='title.configure.deadlineException' />
									</th>
									<td style="height:80px;">
										<div class="tb-row" style="width:100%;" <c:if test="${deadline.WRITE_USE eq '0'}"> style="display: none;" </c:if>>
											<a href="#" class="btn comm st02" style="border-radius:3px;" onclick="layer_open('memberModal'); return false;"><spring:message code='button.deadLineException' /></a>
											<input type="hidden" id="WRITE_EXCEPT_ID" name="WRITE_EXCEPT_ID" value="${deadline.WRITE_EXCEPT_ID }" />
											<ul class="expnSignLst type02" id="addPrsnName" style="margin:5px 0 0 0;">
												<c:forEach var="item" items="${writeExceptMemberList}" varStatus="idx">
													<li id="writeExceptList_${idx.count }" data-idx=${idx.count }>
														<p>
															<span>${item.NAME} / ${item.DIVISION_NM}</span>
															<a href="#none" class="del" onclick="fn_deleteExceptMember(${idx.count }); return false;">X</a>
														</p>
														<input type="hidden" id="writeUserId_${idx.count }" name="writeUserId" value="${item.USER_ID}"/>
													</li>
												</c:forEach>
											</ul>
										</div>
									</td>
								</tr>
								<tr>
									<th style="text-align: center;">
										<spring:message code='title.configure.billPayDate' />
									</th>
									<td style="height:80px;">
										<div class="tb-row">
											<span class="rdo-st">
												<input type="radio" name="PAY_USE" id="payUseY" value="1" <c:if test="${deadline.PAY_USE eq '1'}"> checked </c:if> data-target="mk-pay">
												<label for="payUseY" class="label"><spring:message code='button.use' /></label>
											</span>
											<span class="rdo-st mgn-l-10">
												<input type="radio" name="PAY_USE" id="payUseN" value="0" <c:if test="${deadline.PAY_USE eq '0'}"> checked </c:if> data-target="mk-pay">
												<label for="payUseN" class="label"><spring:message code='button.notUse' /></label>
											</span>
										</div>
										<div class="wrap-inq mgn-t-5 mk-pay" <c:if test="${deadline.PAY_USE eq '0'}"> style="display: none;" </c:if>>
											<span>매달</span>
											<div class="inp-wrap type02" style="width:115px">
												<input type="text" class="inpTxt ar text_right onlyNumber" style="text-align: right;" title="경비 지급일" id="PAY_DT" name="PAY_DT" value="${deadline.PAY_DT}" placeholder="<spring:message code="configure.insertDayNumber" />">		<%--// (1~31)일자 입력--%>
											</div>
											<span>일</span>
										</div>
									</td>
								</tr>
								<tr>
									<th style="text-align: center;">
										<spring:message code='title.configure.elapseDate' />
									</th>
									<td style="height:80px;">
										<div class="tb-row">
											<span class="rdo-st">
												<input type="radio" name="ELAPSE_USE" id="elapseUseY" value="1" <c:if test="${deadline.ELAPSE_USE eq '1'}"> checked </c:if> data-target="mk-elapse">
												<label for="elapseUseY" class="label"><spring:message code='button.use' /></label>
											</span>
											<span class="rdo-st mgn-l-10">
												<input type="radio" name="ELAPSE_USE" id="elapseUseN" value="0" <c:if test="${deadline.ELAPSE_USE eq '0'}"> checked </c:if> data-target="mk-elapse">
												<label for="elapseUseN" class="label"><spring:message code='button.notUse' /></label>
											</span>
										</div>
										<div class="wrap-inq mgn-t-5 mk-elapse" <c:if test="${deadline.ELAPSE_USE eq '0'}"> style="display: none;" </c:if>>
											<div class="inp-wrap type02" style="width:115px">
												<input type="text" class="inpTxt ar text_right onlyNumber" style="text-align: right;" title="경과일" id="ELAPSE_DT" name="ELAPSE_DT" value="${deadline.ELAPSE_DT}" placeholder="<spring:message code="configure.insertDayNumber" />">		<%-- (1~31)일자 입력--%>
											</div>
											<span>일</span>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- Left Grid Area End     -->
				</div>
				<!-- Left Area End    -->
			</div>
		</form>
		<div class="f-r panel-wrap03 rel" style="width:50%;">
			<!-- Right Title Area Start  -->
			<h5 class="panel-tit" style="left:10px">인터페이스 관리</h5>
			<!-- Right Title Area End    -->
			
			<!-- Right Grid Area Start   -->
			<div class="panel-body mgn-l-10">
				<div class="tb-wrap mgn-t-40 mgn-b-15">
					<table class="tb-st mgn-r-5">
						<colgroup>
							<col width="20%" />
							<col width="20%" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th style="text-align: center;">코스트센터 정보</th>
								<td colspan="2">
									<div class="tb-row">
										<a href="#" class="btn comm st02" style="border-radius:3px;" onclick="fn_getInformation('COSTCENTER'); return false;">가져오기</a>
									</div>
								</td>
							</tr>
							<tr>
								<th style="text-align: center;">GL계정코드 정보</th>
								<td colspan="2">
									<div class="tb-row">
										<a href="#" class="btn comm st02" style="border-radius:3px;" onclick="fn_getInformation('GLACCOUNT'); return false;">가져오기</a>
									</div>
								</td>
							</tr>
							<tr>
								<th style="text-align: center;">부가세사업장 정보</th>
								<td colspan="2">
									<div class="tb-row">
										<a href="#" class="btn comm st02" style="border-radius:3px;" onclick="fn_getInformation('WORKPLACE'); return false;">가져오기</a>
									</div>
								</td>
							</tr>
							<tr>
								<th style="text-align: center;">오더 정보</th>
								<td colspan="2">
									<div class="tb-row">
										<a href="#" class="btn comm st02" style="border-radius:3px;" onclick="fn_getInformation('ORDER'); return false;">가져오기</a>
									</div>
								</td>
							</tr>
							<tr>
								<th rowspan="4" style="text-align: center;">매입세금계산서 정보</th>
								<td colspan="2">
									<div class="tb-row">
										<a href="#" class="btn comm st02" style="border-radius:3px;" onclick="fn_getInformation('TAX'); return false;">가져오기</a>
									</div>
								</td>
							</tr>
							<tr>
								<td>● 공급자 사업자번호</td>
								<td>
									<div class="inp-wrap type02 wd-per-100">
										<input type="text" class="inpTxt ar onlyNumber" title="공급자 사업자번호" id="I_SUPNO" name="I_SUPNO" value="" placeholder="공급자 사업자번호를 입력해주세요">
									</div>
								</td>
							</tr>
							<tr>
								<td>● 레코드 생성일자</td>
								<td>
									<div class="prd-inp-wrap wd-per-100">
										<span class="prd-inp">
											<span class="inner">
												<input type="text" class="inp-comm datePic openCal none" title="레코드 생성일 시작" id="I_FZSGDT" name="I_FZSGDT" value="" readonly="readonly">
											</span>
										</span>
										<span class="prd-inp">
											<span class="inner">
												<input type="text" class="inp-comm datePic openCal none" title="레코드 생성일 종료" id="I_TZSGDT" name="I_TZSGDT" value="" readonly="readonly">
											</span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>● 공급자 이메일</td>
								<td>
									<div class="inp-wrap type02 wd-per-100">
										<input type="text" class="inpTxt ar" title="공급자 이메일" id="I_ZBYRE" name="I_ZBYRE" value="" placeholder="공급자 이메일주소를 입력해주세요">
									</div>
								</td>
							</tr>
							<tr>
								<th style="text-align: center;">법인카드 마스터 정보</th>
								<td colspan="2">
									<div class="tb-row">
										<a href="#" class="btn comm st02" style="border-radius:3px;" onclick="fn_getInformation('CARDINFO'); return false;">가져오기</a>
									</div>
								</td>
							</tr>
							<tr>
								<th style="text-align: center;">법인카드 사용내역 정보</th>
								<td colspan="2">
									<div class="tb-row">
										<a href="#" class="btn comm st02" style="border-radius:3px;" onclick="fn_getInformation('CARDMASTER'); return false;">가져오기</a>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
</div>

<!-- 마감 예외 선택 팝업 -->
<%@ include file="/WEB-INF/jsp/hello/eacc/common/popMemberDivList.jsp"%>
<!-- 마감 예외 선택 팝업 -->

</body>
</html>
