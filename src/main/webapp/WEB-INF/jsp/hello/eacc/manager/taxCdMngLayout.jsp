<?xml version="1.0" encoding="UTF-8"?>
<%@ page contentType="text/xml;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%--
  Class Name : taxCdMngLayout.jsp
  Description : 세금코드 관리 Layout
  Modification Information
  
  수정일			수정자			수정내용
  ------			------			--------
  2017-08-16		강승상			최초생성
  2017-08-24		강승상			1차 완성

  author	: 강승상
  since		: 2017-08-16
--%>
<c:set var="gridId" value="TaxCdMngGrid" />
<Grid>
	<Cfg	id="${gridId}"
			AcceptEnters   = "1" Calculated = "1"  CalculateSelected = "1"      DateStrings = "2" Deleting  = "1"
			Dragging       = "1" Editing    = "1"  EnterMode         = "4"      Filtering   = "1" IdChars   = "0123456789"
			InEditMode     = "2" MaxPages   = "20" NoFormatEscape    = "1"      NoHScroll   = "0" NoVScroll = "0"
			NumberId       = "1" Paging     = "2"  PageLength        = "10"     SafeCSS     = '1' Selecting = "0"
			SelectingCells = "0" SortIcons  = "0"  Style             = "Office" SuppressCfg="1" 
			CopySelected   = "0" CopyFocused= "1"  CopyCols			 = "0"		ExportFormat="xls"    ExportCols="0"		
			PrintCols="0"  		 MainCol="MENU_NM"   
	/>

	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>

	<!-- Grid Header Setting Area Start  -->
	<Header	id="Header"	Align="center"
			   TAXMANAGER_NDX	= "KEY"
			   COMPANY_ID 		= "회사 코드"
			   BILL_GB 			= "전표 구분"
			   TAX_CD			= "세금코드"
			   TAX_NM 			= "세금코드 이름"
			   TAX_DESC 		= "세금코드 상세"
			   SORT_ORD			= "출력순서"
			   DEL_YN 			= "사용 여부"
	/>
	<!-- Grid Header Setting Area End    -->

	<!-- Fileter Setting Area Start      -->
	<Head>
		<Filter	id = "Filter" CanFocus = "1"/>
	</Head>
	<!-- Fileter Setting Area End        -->

	<Cols>
		<C Name="TAXMANAGER_NDX"	Type="Text"	 RelWidth="70"  Align="center" CanEdit="0" Visible="0" />
		<C Name="COMPANY_ID"		Type="Text"	 RelWidth="70"  Align="center" CanEdit="0" Visible="0" />
		<C Name="BILL_GB"			Type="Enum"  RelWidth="50"  Align="center" Enum="|카드|세금계산서|일반" EnumKeys="|1|2|3" />
		<C Name="TAX_CD"			Type="Text"  RelWidth="50"  Align="center"/>
		<C Name="TAX_NM"			Type="Text"	 RelWidth="100" Aling="center"/>
		<C Name="TAX_DESC"			Type="Text"	 RelWidth="200"  />
		<C Name="SORT_ORD"			Type="Text"	 RelWidth="60"	Align="center"  CanEdit="0"	Visible="0"/>
		<C Name="DEL_YN"			Type="Enum"	 RelWidth="50"	Enum="|사용|미사용" EnumKeys="|N|Y" Visible="0" />
	</Cols>

	<!-- Grid Paging Setting Area Start  -->
	<Pager Visible = "0"/>

	<Solid>
		<I id = "PAGER" Cells = "NAV,LIST,ONE,GROUP" Space = "4"
		   NAVType	     = "Pager"
		   LISTType	     = "Pages" LISTAlign   = "left" LISTLeft   = "10"                LISTRelWidth  = "1"
		   ONEType       = "Bool"  ONECanEdit  = "1"    ONEFormula = "Grid.AllPages?0:1" ONELabelRight = "페이지단위로보임"
		   ONEOnChange = "Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
		   GROUPCanFocus = "0"
		/>
	</Solid>
	<!-- Grid Paging Setting Area End    -->

	<Toolbar	Space="0"	Styles="1"	Cells="Empty,Cnt,추가,저장,새로고침,인쇄,엑셀"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType = "Button"
				CntType = "Html" CntFormula = '"Total : &lt;b>"+count(7)+"&lt;/b>"' CntWidth = '-1' CntWrap = '0'
				FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>'" FoundWidth = '-1' FoundWrap = '0'
				DelType = "Html" DelFormula = 'var cnt=count("Row.Deleted>0",7);return cnt?"Del :&lt;b>"+cnt+"&lt;/b>":""' DelWidth = '-1' DelWrap = '0'
				추가Type = "Html" 추가 = "&lt;a href='#none' title='추가' class=&quot;defaultButton01 icon add&quot;
							       onclick='addRowGrid(&quot;${gridId}&quot;)'>추가&lt;/a>"
				저장Type = "Html" 저장 = "&lt;a href='#none' title='저장' class=&quot;defaultButton01 icon submit&quot;
								onclick='fn_saveGrid(&quot;${gridId}&quot;)'>저장&lt;/a>"
				새로고침Type = "Html" 새로고침 = "&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
									  onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type = "Html" 인쇄 = "&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
									  onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
				엑셀Type = "Html" 엑셀 = "&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
									  onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>

</Grid>
