<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Class Name :
  Description :
  Modification Information

  수정일			수정자			수정내용
  ------			------			--------
  2017-08-31		강승상			최초생성

  author	: 강승상
  since		: 2017-08-31
--%>
<html>
<head>
	<jsp:include page="/com/comHeader.jsp"/>
	<title>Admin ONLY SQL Query</title>
	<style>
		textarea {
			font-family: 'Courier New', monospace;
			font-size: 12px;
		}
		.fixed {
			table-layout: fixed;
			width: 100%;
			height: 100%;
		}
		.fixed td {
			overflow: scroll;
		}
	</style>
</head>
<body>
<form id="sqlForm" action="sqlQueryResult.do" target="sqlResultFrame" method="post">

	<div id="contents">

		<div class="fl-box panel-wrap03" style="width:20%; height:100%;">
			<h5 class="panel-tit">SQL 검색</h5>

			<div class="panel-body mgn-t-30">
				<div class="txt-wrap">
					<textarea name="query" id="sqlArea" cols="60" title="SQL AREA" placeholder="Please type the query"  style="height:90%"></textarea>
				</div>
				<div class="t-c mgn-t-5">
					<button class="btn comm st01">전송</button>
				</div>
			</div>
		</div>

		
		<div class="fl-box panel-wrap03" style="width:80%; height:100%;">
			<h5 class="panel-tit" style="margin-left:10px !important">조회결과</h5>	
			
			<div class="panel-body mgn-l-10 mgn-t-30">
				<div style="height:100%">					
					<iframe name="sqlResultFrame" scrolling="no"></iframe>
				</div>	
			</div>
		</div>
	</div>	


</form>

<%-- <table border="1" class="fixed">
	<tr>
		<td width="20%" valign="top">
			<div align="right">
				<button class="btn comm st01">전송</button>
			</div>
			<div>
				<textarea name="query" id="sqlArea" cols="60" rows="50" title="SQL AREA" placeholder="Please type the query"></textarea>
			</div>
		</td>
		<td width="80%">
			<table id="queryResult">
				<thead>
					<tr>
						<c:forEach var="resultHeader" items="${resultMapList.get(0).keySet()}">
							<th>
									${resultHeader}
							</th>
						</c:forEach>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${empty resultMapList.get(0).get('ERROR')}">	&lt;%&ndash; S: print row &ndash;%&gt;
							<c:forEach var="row" items="${resultMapList}">			&lt;%&ndash; S: List Loop (row) &ndash;%&gt;
								<tr>
								<c:forEach var="col" items="${row.keySet()}">		&lt;%&ndash; S: Map key (col) Loop &ndash;%&gt;
									<td>
										<c:out value="${row.get(col)}" />
									</td>
								</c:forEach>										&lt;%&ndash; E: Map key (col) Loop &ndash;%&gt;
								</tr>
							</c:forEach>											&lt;%&ndash; E: List Loop &ndash;%&gt;
						</c:when>													&lt;%&ndash; E: print row &ndash;%&gt;
						<c:otherwise>												&lt;%&ndash; S: print error &ndash;%&gt;
							<tr>
								<c:out value="${resultMapList.get(0).get('ERROR')}" />
							</tr>
						</c:otherwise>												&lt;%&ndash; S: print error &ndash;%&gt;
					</c:choose>
				</tbody>
			</table>
			<iframe name="sqlResultFrame" scrolling="no"></iframe>
		</td>
	</tr>
</table> --%>

</body>
</html>
