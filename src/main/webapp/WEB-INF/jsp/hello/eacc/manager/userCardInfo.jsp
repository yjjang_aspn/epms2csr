<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%--
  Class Name : userCardInfo.jsp
  Description : 법인카드 관리 페이지
  Modification Information
  
  수정일		수정자			수정내용
  ------		------			--------
  2017-08-28	강승상			최초생성
  
  author	: 강승상
  since		: 2017-08-28
--%>
<html>
<head>
	<%@ include file="/com/comHeader.jsp"%>
	<%-- 레이어 팝업 --%>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/eacc/modalPopup.css">
	<script src="${pageContext.request.contextPath}/js/com/web/modalPopup.js"></script>
	<script src="${pageContext.request.contextPath}/js/eacc/eaccCommon.js"></script>
	<title>법인카드 관리</title>
	<script type="text/javascript">
		$(document).ready(function() {
			Grids.OnReady = function (grid) {
				if(grid.id == 'PopMemberDivList') {
					grid.HideCol('SELECT_YN');
				}
			};
	
			Grids.OnValueChanged = function(grid, row, col, val, oldval, erros) {
				if (isSysRow(row, col)) return;
	
				if(grid.id == 'userCardInfoGrid' && col == 'TYPE' && val != oldval) {		/* 카드사용구분 변경 시 */
					
					/* 카드분류 : 개인 - 카드사용구분 : 개인용
					      카드분류 : 법인 - 카드사용구분 : 부서공용, 코스트센터공용, 회사공용 */
					if(grid.GetString (row,'CARD_KIND') == "개인"){
						if(val != "0" && val != "1"){
							alert("해당 카드는 개인용 법인카드입니다.");
							return false;
						}
					}else if(grid.GetString (row,'CARD_KIND') == "법인"){
						if(val == "1"){
							alert("해당 카드는 공용 법인카드입니다.");
							return false;
						}
					}
				
					grid.SetValue(row, 'USER_ID', '', 1);
					grid.SetValue(row, 'USER_NM', '', 1);
					grid.SetValue(row, 'DETAIL', '', 1);
	
					setGridMode(row, val);
					openChooseModal(grid, row, val);
					
				} else {
					return false;
				}
			};
			
			Grids.OnClick = function (grid, row, col) {    /* 실소유자 클릭 */
				if (isSysRow(row, col)) return;
	
				if (grid.id == 'userCardInfoGrid' && col == 'USER_NM') {
					var type = grid.GetValue(row, 'TYPE');
					setGridMode(row, type);
					grid.targetRow = row;
					openChooseModal(grid, row, type);
				}
	
				if(grid.id == 'DivisionList') {
					getMemberList(row.DIVISION, row.CUR_LVEL, row.COMPANY_ID);
				}
	
			};
	
			Grids.OnDblClick = function (grid, row, col) {    /* 모달 팝업 더블 클릭 */
				if (isSysRow(row, col)) return;
			
				if(grid.id == "userCardInfoGrid"){
					if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar"){
						return;
					}
					
					if(col == "CARD_NUM"){
						// 카드 번호가 선택된 경우, 해당 카드를 등록한 사용자 정보 출력 
						fn_registerMemberInfo(row.CARD_NUM);
					}
				}
	
				var targetGrid = Grids['userCardInfoGrid'];
				var targetRow = grid.tRow;
	
				if(grid.id == 'PopMemberDivList' && grid.chooseMode == 'division') {
					targetGrid.SetValue(targetRow, 'USER_ID', grid.GetValue(row, 'USER_ID'), 1);
					targetGrid.SetValue(targetRow, 'USER_NM', grid.GetValue(row, 'NAME'), 1);
					targetGrid.SetValue(targetRow, 'DETAIL', grid.GetValue(row, 'DIVISION'), 1);
					layer_open('memberModal');
				}
				if (grid.id == 'PopMemberDivList' && grid.chooseMode == 'member') {
					targetGrid.SetValue(targetRow, 'USER_ID', grid.GetValue(row, 'USER_ID'), 1);
					targetGrid.SetValue(targetRow, 'USER_NM', grid.GetValue(row, 'NAME'), 1);
					targetGrid.SetValue(targetRow, 'DETAIL', grid.GetValue(row, 'USER_ID'), 1);
					layer_open('memberModal');
				}
				if (grid.id == 'PopMemberDivList' && grid.chooseMode == 'costcenter') {
					//targetRow = grid.tRow;
					targetGrid.SetValue(targetRow, 'USER_ID', grid.GetValue(row, 'USER_ID'), 1);
					targetGrid.SetValue(targetRow, 'USER_NM', grid.GetValue(row, 'NAME'), 1);
					targetGrid.SetValue(targetRow, 'DETAIL', grid.GetValue(row, 'KOSTL'), 1);
					layer_open('memberModal');
				}
				if (grid.id == 'PopMemberDivList' && grid.chooseMode == 'company') {
					//targetRow = grid.tRow;
					targetGrid.SetValue(targetRow, 'USER_ID', grid.GetValue(row, 'USER_ID'), 1);
					targetGrid.SetValue(targetRow, 'USER_NM', grid.GetValue(row, 'NAME'), 1);
					targetGrid.SetValue(targetRow, 'DETAIL', grid.GetValue(row, 'COMPANY_ID'), 1);
					layer_open('memberModal');
				}
				delete grid.tRow;
			};
	
		});
		
		function openChooseModal(grid, row, type) {
			if (type == '1') {            /* 사용자 지정 */
				alert("개인 사용자를 선택해주세요.");
			} else if (type == '2') {    /* 부서 지정 */
				alert("부서 관리자를 선택해주세요.");
			} else if (type == '3') {    /* 코스트센터 지정 */
				alert("코스트센터 관리자를 선택해주세요.");
			} else if (type == '4') {    /* 회사 지정 */
				alert("재경팀 관리자를 선택해주세요.");
			}else{
				return false;
			}
			layer_open('memberModal');
		}

		function isSysRow(row, col) {
			return row.id == 'Filter' || row.id == 'Header' || row.id == 'spanHeader' || row.id == 'Toolbar' || col == 'Panel' || row.id == 'PAGER';
		}

		function setGridMode(row, type) {
			if (type == '1') {				/* 지정사용자 */
				Grids['PopMemberDivList'].chooseMode = 'member';
				Grids['PopMemberDivList'].tRow = row;
			} else if (type == '2') {		/* 지정 부서 */
				Grids['PopMemberDivList'].chooseMode = 'division';
				Grids['PopMemberDivList'].tRow = row;
			} else if (type == '3') {		/* 지정 코스트 센터 */
				Grids['PopMemberDivList'].chooseMode = 'costcenter';
				Grids['PopMemberDivList'].tRow = row;
			} else if (type == '4') {		/* 지정 회사 */
				Grids['PopMemberDivList'].chooseMode = 'company';
				Grids['PopMemberDivList'].tRow = row;
			}
		}

		function saveUserCardInfo(gridID) {
			var grid = Grids[gridID];

			$.ajax({
				url: 'userCardInfoAjax.do'
				, method: 'post'
				, data: {'uploadData': grid.GetXmlData()}
				, success: function(result) {
					alert('적용되었습니다.');		// 적용되었습니다.
				}
				, error: function(result) {
					alert('적용이 실패하였습니다.');		// 적용이 실패하였습니다.
				}
				, complete: function() {
					grid.ReloadBody();
				}
			});
		}
		
		/* 해당 카드를 등록한 사용자 정보 출력 */
		function fn_registerMemberInfo(CARD_NUM){
			layer_open('registerMemberModal');
			Grids.PopCardRegisterMemberList.Source.Data.Url = "/hello/eacc/manager/getPopCardRegisterMemberData.do?CARD_NUM="+CARD_NUM;
			Grids.PopCardRegisterMemberList.ReloadBody();
		}
		
	</script>
</head>
<body>
<%--${sessionScope}--%>
<div id="contents">
	<!-- S: body  -->
	<div class="fl-box panel-wrap03" style="width: 100%;height:100%;">
		<!-- S: title  -->
		<h5 class="panel-tit">법인카드 관리</h5>
		<!-- E: title    -->

		<!-- S: grid   -->
		<div class="panel-body">
			<div id="userCardInfoGrid">
				<bdo	Debug="Error"
						Data_Url="/hello/eacc/manager/userCardInfoData.do" Data_Format="String"
						Layout_Url="/hello/eacc/manager/userCardInfoLayout.do"
						Upload_Url="/hello/eacc/manager/userCardInfoAjax.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols" Upload_Xml="2">
				</bdo>
			</div>
		</div>
		<!-- E: grid     -->
	</div>
	<!-- E: body    -->
</div>

<!-- Layer PopUp Start -->
<%@ include file="/WEB-INF/jsp/hello/eacc/common/popMemberDivList.jsp"%>

<!-- 카드 등록한 사용자 정보 -->
<div class="modal fade" id="registerMemberModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog root wd-per-40"> <!-- 원하는 width 값 입력 -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
               	<h4 class="modal-title">카드 등록 사용자 정보</h4> <!-- 카드 등록 사용자 정보 -->
            </div>
            <div class="modal-body" >
            	<div class="modal-bodyIn">
					 
					 <!-- START -->
					 <div class="fl-box panel-wrap" style="height: 560px;"><!-- 원하는 비율로 직접 지정하여 사용 -->
						<div class="panel-body">
							<!-- 트리그리드 : s -->
							<div id="PopCardRegisterMemberList">
								<bdo	Debug="Error"
									Layout_Url="/hello/eacc/manager/popCardRegisterMemberLayout.do"
								>
								</bdo>
							</div>
							<!-- 트리그리드 : e -->
						</div>
					</div>
					<!-- END -->
					 
            	</div>
            </div>
        </div>
    </div>
</div>

<!-- Layer PopUp End -->

</body>
</html>
