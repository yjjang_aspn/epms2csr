<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : cardBillReportListLayout.jsp
  Description : 법인카드 리포트 현황 레이아웃 화면
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.08.28    정순주              최초 생성

    author   : 정순주
    since    : 2017.08.28
--%>
<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="CardBillReportList"/>
<Grid>
	<Cfg	id="${gridId}"
			AcceptEnters   = "1" Calculated = "1"  CalculateSelected = "1"      DateStrings = "2" Deleting  = "0"
			Dragging       = "0" Editing    = "1"  EnterMode         = "4"      Filtering   = "1" IdChars   = "0123456789"
			InEditMode     = "2" MaxPages   = "20" NoFormatEscape    = "1"      NoHScroll   = "0" NoVScroll = "0"
			NumberId       = "1" SafeCSS    = '1'  Selecting		 = "1"
			SelectingCells = "0" SortIcons  = "0"  Style             = "Office" SuppressCfg = "1" 
			Paging		   = "2" AllPages	= "0"  PageLength		 = "30"		MaxPages	= "20"
			CopySelected   = "0" CopyFocused= "1"  CopyCols			 = "0"		ExportFormat="xls" ExportCols="0"
	/>
	
	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>
	
	<Head>
		<Header	NoEscape="1" Spanned="1" Align="center"
			CARD_NDXSpan="16"			CARD_NDX="사용내역" 
            DEL_YNSpan="3"				DEL_YN="제외"
            SEND_YNSpan="4"				SEND_YN="이관"
            BILL_STATUSSpan="11"		BILL_STATUS="전표"
            GW_STATUSSpan="3"			GW_STATUS="결재"
		 />
		<Header	id="Header"	Align="center" Spanned="1"
			CARD_NDX		= "저장 순번"         
			CARD_CODE       = "카드사 코드"
			OWNER_REG_NO    = "기업사업자번호"
			CARD_NUM        = "카드번호"
			AUTH_NUM        = "승인번호"
			AUTH_DT         = "승인일시"
			AUTH_DATE       = "승인일자"
			AUTH_TIME       = "승인시간"
			GEORAE_STAT     = "거래상태"
			GEORAE_CAND     = "승인/매입취소일자"
			AQUI_DATE       = "매입일자"
			AQUI_TIME       = "매입시간"
			SETT_DATE       = "결제예정일자"
			A_TOT_AMOUNT    = "승인총금액"
			A_AMT_AMOUNT    = "승인공급가액"
			A_VAT_AMOUNT    = "승인부가세"
			A_SER_AMOUNT    = "승인봉사료"
			A_FRE_AMOUNT    = "승인면세금액"
			P_TOT_AMOUNT    = "매입총금액"
			P_AMT_AMOUNT    = "매입공급가액"
			P_VAT_AMOUNT    = "매입부가세"
			P_SER_AMOUNT    = "매입봉사료"
			P_FRE_AMOUNT    = "매입면세금액"
			B_TOT_AMOUNT    = "청구금액"
			CURRENCY_UNIT   = "통화코드"
			RATE            = "환율"
			MERC_NAME       = "가맹점명"
			MERC_SAUP_NO    = "가맹점사업자번호"
			MERC_ADDR       = "가맹점주소"
			MERC_REPR       = "가맹점대표자명"
			MERC_TEL        = "가맹점전화번호"
			MERC_ZIP        = "가맹점우편번호"
			MCC_NAME        = "가맹점업종명"
			MCC_CODE        = "가맹점업종코드"
			MCC_STAT        = "가맹점업종구분"
			VAT_STAT        = "가맹점과세유형"
			GONGJE_GUBUN    = "매입세액공제불가구분"
			INDATE          = "생성년월일"
			INTIME          = "생성시분초"
			
			DEL_YN          = "제외여부"
			DEL_NM          = "제외한사람"
			DEL_DT          = "제외날짜"
			
			SEND_YN         = "이관여부"
			SEND_NM         = "이관한사람"
			RECEIVE_NM      = "이관대상자"
			SEND_DT         = "이관날짜"
			
			BILL_STATUS		= "전표상태"
			ACCGUBUN       	= "계정구분"
			BOOK_DT			= "전기일자"
			EVIDENCE_DT		= "증빙일자"
			TAX_CD			= "세금코드"
			ERPBILL			= "전표번호"
			ERPBILLYEAR     = "회계년도"
			USER_NM			= "작성자"
			REG_DT			= "작성일자"
			BILL_DEL_NM		= "전표삭제자"
			BILL_DEL_DT		= "전표삭제일자"
			
			GW_STATUS		= "결재상태"
			APPROVAL_STATUS = "작성 상태"
			RJCTBILL_NDX    = "반려/승인취소 전표번호"
			
		/>
		
		<Filter	id="Filter"	CanFocus="1" RECEIVE_EMAIL="" RECEIVE_EMAILTip = "이관 일괄적용" RECEIVE_EMAILCanEdit="0"/>/>
		
	</Head>
	
	<Solid>
		<I  id="PAGER"	Cells="NAV,LIST,ONE,GROUP"	Space="4"
			NAVType="Pager"
			LISTType="Pages"	LISTRelWidth="1"	LISTAlign="left"	LISTLeft="10"
			ONEType="Bool"		ONEFormula="Grid.AllPages?0:1" 			ONECanEdit="1"	ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"
		/>
	</Solid>
	
	<Pager Visible="0" CanHide="0"/>
	
	<Cols>
		<C Name="CARD_NDX" 			Type="Int" Width="50" CanEdit="0" Visible="1" Align="center" CanExport="1" CanHide="0" />
		<C Name="AUTH_DT" 			Type="Date" Width="150" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd HH:MM:ss" />
		<C Name="CARD_NUM" 			Type="Text" Width="150" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="AUTH_NUM" 			Type="Text" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="A_TOT_AMOUNT" 		Type="Int" 	Width="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="A_AMT_AMOUNT" 		Type="Int" 	Width="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="A_VAT_AMOUNT" 		Type="Int" 	Width="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="P_TOT_AMOUNT" 		Type="Int" 	Width="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="P_AMT_AMOUNT" 		Type="Int" 	Width="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="P_VAT_AMOUNT" 		Type="Int" 	Width="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="B_TOT_AMOUNT" 		Type="Int" 	Width="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="GEORAE_STAT" 		Type="Text" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="MERC_NAME" 		Type="Text" Width="150" CanEdit="0" Visible="1" Align="left" CanExport="1" />
		<C Name="MERC_ADDR" 		Type="Text" Width="200" CanEdit="0" Visible="1" Align="left" CanExport="1" />
		
		
		<C Name="AUTH_DATE" 		Type="Date" Width="60" CanEdit="0" Visible="0" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		<C Name="AUTH_TIME" 		Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="A_SER_AMOUNT" 		Type="Int" 	Width="50" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="P_SER_AMOUNT" 		Type="Int" 	Width="50" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="MCC_NAME" 			Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="CARD_CODE" 		Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" CanHide="0" />
		<C Name="OWNER_REG_NO" 		Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" CanHide="0" />
		<C Name="GEORAE_CAND" 		Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="AQUI_DATE" 		Type="Date" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		<C Name="AQUI_TIME" 		Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="GEORAE_COLL" 		Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="SETT_DATE" 		Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="A_FRE_AMOUNT" 		Type="Int" 	Width="100" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="P_FRE_AMOUNT" 		Type="Int" 	Width="100" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="CURRENCY_UNIT" 	Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="RATE" 				Type="Int" 	Width="100" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="MERC_SAUP_NO" 		Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="MERC_REPR" 		Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="MERC_TEL" 			Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="MERC_ZIP" 			Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="MCC_CODE" 			Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="MCC_STAT" 			Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="VAT_STAT" 			Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="GONGJE_GUBUN" 		Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="INDATE" 			Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="INTIME" 			Type="Text" Width="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		
		<C Name="DEL_YN" 			Type="Text" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
	    <C Name="DEL_NM" 			Type="Text" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
	    <C Name="DEL_DT" 			Type="Date" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		
		<C Name="SEND_YN" 			Type="Text" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="SEND_NM" 			Type="Text" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="RECEIVE_NM" 		Type="Text" Width="80"  CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="SEND_DT" 			Type="Date" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		
		<C Name="BILL_STATUS" 		Type="Enum" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1"
									EmptyValue="" <tag:enum codeGrp="BILL_STATUS" /> /><!-- 전표상태 -->
		<C Name="ACCGUBUN" 			Type="Enum" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1"
									EmptyValue="" <tag:enum codeGrp="ACCGUBUN" /> /><!-- 계정일자 -->							
		<C Name="BOOK_DT" 			Type="Date" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" /><!-- 전기일자 -->
		<C Name="EVIDENCE_DT" 		Type="Date" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" /><!-- 증빙일자 -->							
		<C Name="TAX_CD" 			Type="Enum" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1"
									EmptyValue="" <tag:taxEnum billGb="TAX" /> /><!-- 세금코드 -->
		<C Name="ERPBILL" 			Type="Text" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" /><!-- 전표번호 -->
		<C Name="ERPBILLYEAR" 		Type="Text" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" /><!-- 회계연도 -->
		<C Name="USER_NM" 			Type="Text" Width="120" CanEdit="0" Visible="1" Align="center" CanExport="0" /><!-- 작성자 -->
		<C Name="REG_DT" 			Type="Date" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" /><!-- 작성일자 -->
		<C Name="BILL_DEL_NM" 		Type="Text" Width="120" CanEdit="0" Visible="1" Align="center" CanExport="0" /><!--	삭제자 -->
		<C Name="BILL_DEL_DT" 		Type="Date" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" /><!-- 삭제일자 -->
		
		<C Name="GW_STATUS" 		Type="Enum" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1"
								  	EmptyValue="" <tag:enum codeGrp="GW_STATUS" /> /><!--	결재상태 -->
	    <C Name="APPROVAL_STATUS" 	Type="Text" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
	   <C Name="RJCTBILL_NDX" 		Type="Text" Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" /><!-- 반려/승인취소 전표번호 -->
	</Cols>
	
	<Toolbar	Space="0"	Styles="2" Cells="Empty,Cnt,Found,제외해제,이관,항목선택,새로고침,인쇄,엑셀"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType="Button"
				CntType = "Html" CntFormula = '"총 : &lt;b>"+count(7)+"&lt;/b>행"' CntWidth = '-1' CntWrap = '0'
				ChgType = "Html" ChgFormula = 'var cnt=count("Row.Changed==1",7);return cnt?"변경된 행 :&lt;b>"+cnt+"&lt;/b>":""' ChgWidth = '-1' ChgWrap = '0'
				FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>행'" FoundWidth = '-1' FoundWrap = '0'
				제외해제Type ="Html"  제외해제="&lt;a href='#none' title='제외해제' class=&quot;treeButton treeClear&quot;
									onclick='fn_cancelDel(&quot;${gridId}&quot;)'>제외해제&lt;/a>"
				이관Type ="Html"  이관="&lt;a href='#none' title='이관' class=&quot;treeButton treeMove&quot;
									onclick='fn_passCard(&quot;${gridId}&quot;)'>이관&lt;/a>"
				새로고침Type = "Html" 새로고침 = "&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
									  onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
 				인쇄Type = "Html" 인쇄 = "&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
									  onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
 				엑셀Type = "Html" 엑셀 = "&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
									  onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
			 	항목선택Type="Html" 항목선택="&lt;a href='#none' title='항목선택' class=&quot;treeButton treeSelect&quot;
			  						  onclick='showColumns(&quot;${gridId}&quot;,&quot;xls&quot;)'>항목선택&lt;/a>"
	/>
</Grid>