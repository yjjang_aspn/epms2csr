<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="aspn.com.common.util.Utility" %>
<%--
  Class Name : cardBillReportList.jsp
  Description : 법인카드 리포트 목록 화면
  Modification Information
 
          수정일       		수정자                   수정내용
    -------    --------    ---------------------------
    2017.08.28  정순주              최초 생성

    author   : 정순주
    since    : 2017.08.28
--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<link rel="stylesheet" href="/css/eacc/modalPopup.css">
<script src="/js/eacc/eaccCommon.js" type="text/javascript" ></script>
<script src="/js/com/web/modalPopup.js"></script>
<script src="/js/jquery/jquery-filestyle.js" type="text/javascript" ></script>
<script src="/js/jquery/jquery.MultiFile.js" type="text/javascript"></script>
<script src="/js/jquery/jquery.ajaxsubmit.js" type="text/javascript"></script>
<script type="text/javascript" >
var gubunDiv = '〔';
var layerGubun = "transCardPerson"; // 레이어 팝업 구분자(추가직원-addPerson, 이관-transPerson)
var transGrid = ""; // 이관대상자 그리드
var transCol = ""; // 이관대상자 컬럼
var transRow = []; // 이관대상자 row
$(document).ready(function(){
		
	$('#searchVal').keydown(function(key){
			<%-- 사업장 검색 Enter Key Down Event --%>
			if(key.keyCode == 13){
				fn_searchData();
			}
	});
	/* 데이터 URL */
	var parameters = "startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val();
	var url = "/hello/eacc/manager/getCardReportData.do?" + parameters;
	$("#cardBillReportList").find('bdo').attr("Data_Url", url);
	
	//그리드 row 더블클릭시 에빈트
	Grids.OnDblClick = function(grid, row, col){
		if(row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "_ConstHeight") {
			return;
		}
		
		if (grid.id == "CardBillReportList") {
			
			// 이관 모달 팝업
			if(col == "RECEIVE_NM"){
				if(row.id == "Filter"){
					var selRow = Grids.CardBillReportList.GetSelRows();
					if(selRow.length == 0){
						alert("이관할 사용내역을 선택하세요.");
						return;
					}else{
						transGrid = grid.id; // 이관대상자 그리드
						transCol = col; // 이관대상자 컬럼
						for(var i = 0 ; i < selRow.length ; i++) {
							transRow.push(selRow[i]);
						}
						layer_open('memberModal');
					}
				}else{
					transGrid = grid.id; // 이관대상자 그리드
					transCol = col; // 이관대상자 컬럼
					transRow.length = 0;
					transRow.push(row);
					layer_open('memberModal');
				}
			}else{
				var cardNum = row.CARD_NUM;
				var authNum = row.AUTH_NUM;
				var cardNdx = row.CARD_NDX;
				var authDate = DateToString(row.AUTH_DATE, 'yyyyMMdd');

				var url = "/hello/eacc/cardBill/cardBillWrite.do";
				var param = "?cardNum="+cardNum+"&authNum=" + authNum+"&cardNdx="+cardNdx+"&authDate="+authDate+"&"+new Date().getTime();
				// 전표 작성 모달 팝업 로드
				$("#cardBillWriteModal").load(url+param, function(){
					$(this).modal({
						backdrop : 'static'
					});
				});
			}
		}
	}	
	//그리드 저장 후 이벤트
	Grids.OnAfterSave = function(grid, result, autoupdate) {
		grid.ReloadBody();
	}
	
	Grids.OnChange = function(grid, row, col){
		if(col == "RECEIVE_EMAIL"){
			Grids["TaxBillReportList"].SetValue(row, "SEND_NM", "", 1);
			Grids["TaxBillReportList"].SetValue(row, "RECEIVE_NM", "", 1);
			Grids["TaxBillReportList"].SetValue(row, "SEND_DT", "", 1);
			Grids["TaxBillReportList"].SetAttribute(row, "SEND_YN", "Color", "#FFFFAA",1);
			Grids["TaxBillReportList"].SetAttribute(row, "SEND_NM", "Color", "#FFFFAA",1);
			Grids["TaxBillReportList"].SetAttribute(row, "RECEIVE_NM", "Color", "#FFFFAA",1);
			Grids["TaxBillReportList"].SetAttribute(row, "RECEIVE_EMAIL", "Color", "#FFFFAA",1);
			Grids["TaxBillReportList"].SetAttribute(row, "SEND_DT", "Color", "#FFFFAA",1);
		}
	}
});

/* 날짜 검색 */
function fn_searchData() {
	var parameters = "startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val() + "&searchVal=" + $("#searchVal").val();
	Grids.CardBillReportList.Source.Data.Url = "/hello/eacc/manager/getCardReportData.do?" + parameters;
	Grids.CardBillReportList.ReloadBody();
}

//그리드 리로드
function fn_reloadGrid(){
	Grids.CardBillReportList.ReloadBody();
}

//모달 토글
function toggleModal(obj){
	obj.modal('toggle');
}

//제외 해제
function fn_cancelDel(gridNm){
	<%-- 제외 해제 --%>
	var cardNum = "";
	var authNum = "";
	var cardNdx = "";
	
	if(gridNm == "CardBillReportList"){
		var selRow = Grids.CardBillReportList.GetSelRows();
		if(selRow.length == 0){
			alert("제외 해제할 사용내역을 선택하세요.");
			return;
		}else{
			for(var i = 0 ; i < selRow.length ; i++) {
				if(cardNdx == ""){
					cardNum = selRow[i].CARD_NUM;
					authNum = selRow[i].AUTH_NUM;
					cardNdx = selRow[i].CARD_NDX;
				}else{
					cardNum += gubunDiv + selRow[i].CARD_NUM;
					authNum += gubunDiv + selRow[i].AUTH_NUM;
					cardNdx += gubunDiv + selRow[i].CARD_NDX;
				}
			}
			
			if(confirm("선택하신 법인카드 사용내역을 제외 해제 하시겠습니까?")){
				$.ajax({
					type: 'POST',
					url: '/hello/eacc/cardBill/cardMasterCancelExceptAjax.do',
					async : false, 
					dataType: 'json',
					data : {CARD_NUM : cardNum, AUTH_NUM : authNum, CARD_NDX : cardNdx },
					success: function (json) {
						if(json.result == "T"){
							alert(json.resultMsg);
							fn_reloadGrid();
						}else{
							alert(json.resultMsg);
							fn_reloadGrid();
						}
					},error: function(XMLHttpRequest, textStatus, errorThrown){
						alert("[시스템오류]제외 해제 처리 중 오류가 발생하였습니다.");
						fn_reloadGrid();
					}
				});
			}
		}
	}
}

//이관
function fn_passCard(gridNm){
	var receiveId = "";
	var cardNum = "";
	var authNum = "";
	var cardNdx = "";
	var authDate = "";
	<%-- 이관 --%>
	if(gridNm == "CardBillReportList"){
		var selRow = Grids.CardBillReportList.GetSelRows();
		if(selRow.length == 0){
			alert("이관할 법인카드 내역을 선택하세요.");
			return;
		}else{
			for(var i = 0 ; i < selRow.length ; i++) {
				if(selRow[i].RECEIVE_NM == "" || selRow[i].RECEIVE_NM == null || selRow[i].RECEIVE_NM == "undefined"){
					alert("이관할 대상자를 확인해 주세요.");
					return;
				}else{
					if(receiveId == ""){
						receiveId = selRow[i].RECEIVE_NM;
						cardNum = selRow[i].CARD_NUM;
						authNum = selRow[i].AUTH_NUM;
						cardNdx = selRow[i].CARD_NDX;
						authDate = selRow[i].AUTH_DATE;
					}else{
						receiveId += gubunDiv + selRow[i].RECEIVE_NM;
						cardNum += gubunDiv + selRow[i].CARD_NUM;
						authNum += gubunDiv + selRow[i].AUTH_NUM;
						cardNdx += gubunDiv + selRow[i].CARD_NDX;
						authDate += gubunDiv + selRow[i].AUTH_DATE;
					}
				}
			}
			
			if(confirm("선택하신 법인카드 내역을 이관하시겠습니까?")){
				$.ajax({
					type: 'POST',
					url: '/hello/eacc/cardBill/updatePassCardAjax.do',
					async : false, 
					dataType: 'json',
					data : {RECEIVE_ID : receiveId, CARD_NUM : cardNum, AUTH_NUM : authNum, CARD_NDX : cardNdx, AUTH_DATE : authDate},
					success: function (json) {
						if(json.result == "T"){
							alert("이관처리 되었습니다");
							fn_reloadGrid();
						}else{
							alert("법인카드 내역 이관 중 오류가 발생하였습니다.");
						}
					},error: function(XMLHttpRequest, textStatus, errorThrown){
						alert("법인카드 내역 이관 중 오류가 발생하였습니다.");
					}
				});
			}
		}
	}
}	
</script>
</head>
<body>

<!-- 화면 UI Area Start  -->
<div id="contents">

<!-- Hidden Area Start -->
<!-- Hidden Area End   -->

	<div class="fl-box panel-wrap03" style="width:100%; height:100%;">
	<!-- <div class="panel-wrap st01"> -->
			<h5 class="panel-tit mgn-l-10">법인카드 리포트</h5>
			
			<!-- inq-area-inner : s -->
			<div class="inq-area-inner type06 ab">
				<ul class="wrap-inq">
					<li class="inq-clmn">
						<h4 class="tit-inq">승인일자</h4>
						<div class="prd-inp-wrap">
							<span class="prd-inp">
								<span class="inner">
									<input type="text" class="inp-comm datePic openCal thisMonth" readonly="readonly" gldp-id="gldp-7636599364" id="startDt"/>
								</span>
							</span>
							<span class="prd-inp">
								<span class="inner">
									<input type="text" class="inp-comm datePic openCal" readonly="readonly" gldp-id="gldp-8530641166" id="endDt" />
								</span>
							</span>
						</div>
					</li>
					<li class="inq-clmn">
						<h4 class="tit-inq">승인번호</h4>
						<div class="prd-inp-wrap">
							<input type="text" class="inp-wrap type03" id="searchVal" name="searchVal" value=""/>
						</div>
					</li>
				</ul>
				<a href="#" class="btn comm st01" onclick="fn_searchData(); return false;">검색</a>
				
<!-- 				<a href="#" class="btn comm st02" onclick="fn_popDeleteCardBill(); return false;">일괄제외</a> -->
			</div>
			<!-- inq-area-inner : e -->
			<div class="panel-body mgn-t-25">
				<div id="cardBillReportList" style="height:100%;">
					<bdo	Debug="Error"
							Layout_Url="/hello/eacc/manager/cardBillReportListLayout.do"
						>
					</bdo>
				</div>
			</div>
			<!-- e:grid-wrap -->
	</div>
</div>

<%@ include file="/WEB-INF/jsp/hello/eacc/common/popMemberDivList.jsp"%>
<%@ include file="/WEB-INF/jsp/hello/eacc/common/popNeedBill.jsp"%>
<!-- Layer PopUp End -->

<!-- 화면 UI Area End    -->
</body>
</html>
