<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="aspn.com.common.util.Utility" %>
<%--
  Class Name : accountMngList.jsp
  Description : 계정관리화면
  Modification Information
 
          수정일       		수정자                   수정내용
    -------    --------    ---------------------------
    2017.08.14  정호윤              최초 생성

    author   : 정호윤
    since    : 2017.08.14
--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<%-- 레이어 팝업 --%>
<link rel="stylesheet" href="/css/eacc/modalPopup.css">
<script src="/js/com/web/modalPopup.js"></script>
<script type="text/javascript" >
var selCompanyId = null;
var selAccountNdx = null;
var selBillGb = null;

<%-- 그리드 load 이벤트 핸들러 --%>
Grids.OnReady = function(grid){
};

Grids.OnClick = function(grid, row, col){
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar"){
		return;
	}

	if(grid.id == "AccountList"){
		if((row.ACCOUNT_NDX != null && row.ACCOUNT_NDX != "undefined" && row.ACCOUNT_NDX != "") && row.Added != 1 && row.Deleted != 1) {
			$('#accountDtlNm').text(grid.GetString (row,'BILL_GB') + " - " + grid.GetString (row,'ACCGUBUN') + " [" + row.ACCOUNT_NM + "] 상세");
			selCompanyId = row.COMPANY_ID;
			selAccountNdx =  row.ACCOUNT_NDX;
			selBillGb = row.BILL_GB;
			var params = "companyId=" + selCompanyId + "&billGb=" +selBillGb + "&accountNdx=" + selAccountNdx;

			getAccountDtlList(params);
			
		}
	}
};

Grids.OnAfterValueChanged = function(grid, row, col, type){
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return; 
	}
	
	if(grid.id == "AccountDtlList"){
		if(col == "TAX_CMD" && row.TAX_CMD == "N"){
			alert("<spring:message code='accountMngList.taxValidation' />"); // 세금코드 변경이 불가능한 경우, 기본 세금코드 입력이 필수입니다.
		}
	}
}

Grids.OnDataReceive = function(grid, source){
	
	if(grid.id == "AccountDtlList"){
		<%-- GRID에 SETTING --%>
		if(selBillGb == "1"){
			for(row=grid.GetFirst(null);row;row=grid.GetNext(row)){
				grid.SetAttribute(row, "TAX_CD", "Enum", "${cardEnum}", 1);
				grid.SetAttribute(row, "TAX_CD", "EnumKeys", "${cardEnumKeys}", 1);
			}
		}else if(selBillGb == "2"){
			for(row=grid.GetFirst(null);row;row=grid.GetNext(row)){
				grid.SetAttribute(row, "TAX_CD", "Enum", "${taxEnum}", 1);
				grid.SetAttribute(row, "TAX_CD", "EnumKeys", "${taxEnumKeys}", 1);
			}
		}else if(selBillGb == "3"){
			for(row=grid.GetFirst(null);row;row=grid.GetNext(row)){
				grid.SetAttribute(row, "TAX_CD", "Enum", "${etcEnum}", 1);
				grid.SetAttribute(row, "TAX_CD", "EnumKeys", "${etcEnumKeys}", 1);
			}
		}
	}
}

function getAccountDtlList(params){
	Grids.AccountDtlList.Source.Data.Url = "/hello/eacc/manager/getAccountDtlData.do?"+params;
	Grids.AccountDtlList.ReloadBody();
}

function addRowGrid(gridNm){
	var row = null;
	var i = 0;
	
	var glCode = "";
	var gubunDiv = '〔';
	
	if(gridNm == "AccountDtlList") {
		if(selAccountNdx == null || selAccountNdx == "undefined" || selAccountNdx == ""){
			alert("<spring:message code='accountMngList.noAccount' />"); // 선택된 계정 분류가 없습니다.
			return;
		}else{
			layer_open('modal1');
			Grids.PopAccountList.Source.Data.Url = "/hello/eacc/manager/getPopAccountData.do?accountNdx="+selAccountNdx;
			Grids.PopAccountList.ReloadBody();
		}
	}else if(gridNm == "AccountList") {

		row = Grids[gridNm].AddRow( null, Grids[gridNm].GetFirst(), true);

		Grids[gridNm].SetValue(row, "COMPANY_ID", "", 1);
		Grids[gridNm].SetValue(row, "ACCOUNT_NDX", "", 1);
		Grids[gridNm].SetValue(row, "ACCOUNT_NM", "", 1);
		Grids[gridNm].SetValue(row, "BILL_GB", "", 1);
		Grids[gridNm].SetValue(row, "ACCGUBUN", "", 1);
		Grids[gridNm].SetValue(row, "DEL_YN", "N", 1);
		
	}else if(gridNm == "PopAccountList"){
		
		for(var row = Grids[gridNm].GetFirst(); row; row = Grids[gridNm].GetNext(row)){
			if(row.SELECT_YN == '1'){
				if(glCode == null || glCode == ''){
					glCode = row.GLCODE;
				}else{
					glCode += gubunDiv + row.GLCODE;
				}
			}
		}
		
		if(confirm("<spring:message code='conmonmsg.wantRegister' />")){ // 등록하시겠습니까?
			$.ajax({
				type: 'POST',
				url: '/hello/eacc/manager/insertGlCodeAjax.do',
				async : false, 
				dataType: 'json',
				data : {glCode : glCode, accountNdx : selAccountNdx},
				success: function (json) {
					if(json.result == "T"){
						layer_open('modal1');
						fn_reloadGrid();
					}else{
						alert("<spring:message code='accountMngList.errRegisterGlAccount' />"); // GL 계정 등록 중 오류가 발생하였습니다.
						layer_open('modal1');
					}
				},error: function(XMLHttpRequest, textStatus, errorThrown){
					alert("<spring:message code='accountMngList.errRegisterGlAccount' />");
					layer_open('modal1');
				}
			});
		}
	}
}

function fn_reloadGrid(){
	<%-- gl 코드 insert 후 그리드 새로고침 --%>
	Grids.AccountDtlList.ReloadBody();
}
</script>
</head>
<body>

<!-- 화면 UI Area Start  -->
<div id="contents">

<!-- Hidden Area Start -->
<!-- Hidden Area End   -->

<!-- Left Area Start  -->
<div class="fl-box panel-wrap03" style="width:35%; height:100%;">
	<!-- Left Title Area Start  -->
	<h5 class="panel-tit"><spring:message code='title.accountMng' /></h5> <!-- 계정 분류 정보 -->
	<!-- Left Title Area End    -->

	<!-- Left Grid Area Start   -->
	<div class="panel-body">
		<div id="AccountList">
			<bdo	Debug="Error"
					Data_Url="/hello/eacc/manager/getAccountMngData.do" Data_Format="String"
					Layout_Url="/hello/eacc/manager/accountMngLayout.do"
					Upload_Url="/hello/eacc/manager/accountMngEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols" Upload_Xml="2">
			</bdo>
		</div>
	</div>
	<!-- Left Grid Area End     -->
</div>
<!-- Left Area End    -->

<!-- Right Area Start -->
<div class="fl-box panel-wrap03" style="width:65%; height:100%;">
	<!-- Right Title Area Start -->
<%-- 	<h5 class="panel-tit mgn-l-10"><spring:message code='title.accountDtl' /></h5> <!-- 계정 분류 정보 상세 --> --%>
	<h5 class="panel-tit mgn-l-10" id="accountDtlNm"></h5> <!-- 계정 분류 정보 상세 -->
	<!-- Right Title Area End   -->

	<!-- Right Grid Area Start  -->
	<div class="panel-body mgn-l-10">
		<div id="AccountDtlList">
			<bdo	Debug="Error"
					Layout_Url="/hello/eacc/manager/accountDtlLayout.do"
					Upload_Url="/hello/eacc/manager/accountDtlEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols" Upload_Xml="2"
					>
			</bdo>
		</div>
	</div>
	<!-- Right Grid Area End    -->
</div>
<!-- Right Area End   -->

</div>

<!-- Layer PopUp Start -->
<div class="modal fade" id="modal1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog root wd-per-50"> <!-- 원하는 width 값 입력 -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
               	<h4 class="modal-title"><spring:message code='title.addGlAccount' /></h4> <!-- GL계정 추가 -->
            </div>
            <div class="modal-body" >
            	<div class="modal-bodyIn">
					 
					 <!-- START -->
					 <div class="fl-box panel-wrap" style="height: 560px;"><!-- 원하는 비율로 직접 지정하여 사용 -->
						<div class="panel-body">
							<!-- 트리그리드 : s -->
							<div id="PopAccountList">
								<bdo	Debug="Error"
									Layout_Url="/hello/eacc/manager/popAccountLayout.do"
									Upload_Url="/hello/eacc/manager/popAccountEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols" Upload_Xml="2">
									>
								</bdo>
							</div>
							<!-- 트리그리드 : e -->
						</div>
					</div>
					<!-- END -->
					 
            	</div>
            </div>
        </div>
    </div>
</div>

<!-- Layer PopUp End -->

<!-- 화면 UI Area End    -->

</body>
</html>
