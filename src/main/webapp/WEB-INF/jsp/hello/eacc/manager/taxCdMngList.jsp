<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%--
  Class Name : taxCdMngList.jsp
  Description : 세금코드 관리 페이지
  Modification Information
  
  수정일			수정자			수정내용
  ------			------			--------
  2017-08-16		강승상			최초생성
  2017-08-24		강승상			1차 완성

  author	: 강승상
  since		: 2017-08-16
--%>
<html>
<head>
	<%@ include file="/com/comHeader.jsp"%>
	<title>세금코드 관리</title>
	<script type="text/javascript">
		var selTab = "";
	
		$(document).ready(function() {
			<%-- 현재 선택된 탭 아이디 조회 --%>
			tab_select_id();
			
			<%-- Tab Click Event --%>
			$('.btn-tab').on('click', function(){
				selTab = $(this).attr("id");
				
				if(selTab == "card"){
					getData(1);
				}else if(selTab == "tax"){
					getData(2);
				}else if(selTab == "etc"){
					getData(3);
				}
			});
		});
		
		<%-- 드래그시 출력순서 세팅 : 드래그한 row의 sibling의 출력순서 세팅 --%>
		Grids.OnRowMove = function (grid, row, oldparent, oldnext){
			
			if(grid.id == "TaxCdMngGrid"){
				var i =1;
				var lineRow = grid.GetFirst(oldparent);
				if(!(lineRow == null || lineRow == 'undefined')){
					do{
						grid.SetValue(lineRow, "SORT_ORD", i, 1);
						lineRow = lineRow.nextSibling;
						i++;
					}while(!(lineRow == null || lineRow == 'undefined'));
				}
			}
			
		};
		
		function tab_select_id(){
			<%-- 현재 선택된 탭 아이디 조회 --%>
			$('.tab-depth01 li').each(function(e){
				if($(this).hasClass('on')){
					selTab = $(this).attr("id");
				}
			});
			return selTab;
		}
		
		function addRowGrid(gridNm) {
			var row = null;
			<%-- Tab --%>
			var billGb = "";
			if(selTab == "card"){
				billGb = "1";
			}else if(selTab == "tax"){
				billGb = "2";
			}else if(selTab == "etc"){
				billGb = "3";
			}
			
			if(gridNm == 'TaxCdMngGrid') {
				row = Grids[gridNm].AddRow(null, Grids[gridNm].GetFirst(), true);
				
				Grids[gridNm].SetValue(row, 'TAXMANGE_NDX', '', 1);
				Grids[gridNm].SetValue(row, 'COMPANY_ID', '${sessionScope.ssCompanyId}', 1);	// by s2ng session에서 값 받음
				Grids[gridNm].SetValue(row, 'BILL_GB', '', 1);
				Grids[gridNm].SetValue(row, 'TAX_CD', '', 1);
				Grids[gridNm].SetValue(row, 'TAX_NM', '', 1);
				Grids[gridNm].SetValue(row, 'TAX_DESC', '', 1);
				Grids[gridNm].SetValue(row, "SORT_ORD", Grids[gridNm].GetLast().SORT_ORD+1, 1);					
				Grids[gridNm].SetValue(row, 'DEL_YN', 'N', 1);
			}
			
			Grids[gridNm].RefreshRow(row);
		}

		function fn_saveGrid(gridNm) {
			if (gridNm == 'TaxCdMngGrid') {
				for (var row = Grids[gridNm].GetFirst(); row; row = Grids[gridNm].GetNext(row)) {
					<%-- 비어있는 입력 값 검사 --%>
					if (row.BILL_GB == null || row.BILL_GB == 'undefined' || row.BILL_GB == '') {
						alert('전표 구분 값을 선택해 주세요.');
						return;
					}
					if (row.TAX_CD == null || row.TAX_CD == 'undefined' || row.TAX_CD == '') {
						alert('세금코드 값을 입력해 주세요.');
						return;
					}
					if (row.TAX_NM == null || row.TAX_NM == 'undefined' || row.TAX_NM == '') {
						alert('세금코드 이름 값을 입력해 주세요.');
						return;
					}
					
					<%-- 추가/수정 시, 세금코드 길이 검사 : 2Byte --%>
					if("Changed" in row || "Added" in row){
						if(row.TAX_CD.length > 2){
							alert('세금코드는 2Byte를 넘길 수 없습니다. ' + row.TAX_NM + '의 세금코드를 확인해 주세요.');
							return;
						}
					}
				}
				saveGrid(gridNm);
			}
		}

		function getData(target) {
			Grids.TaxCdMngGrid.Source.Data.Url = 'taxCdMngData.do' + '?BILL_GB='+target;
			Grids.TaxCdMngGrid.ReloadBody();
		}
	</script>
</head>
<body>
<div id="contents">

	<div class="fl-box panel-wrap05_01 " style="width:100%; height:100%;">
	<h5 class="panel-tit">세금 코드 관리</h5>
	
		<!-- s:tab area -->
		<div class="mgn-t-30" style="height:100%">
			<!-- 원하는 비율로 직접 지정하여 사용 -->
			<div class="panel-body">
				<div class="tab-wrap">
					<ul class="tab-depth03">
						<li id="card" style="color:#4b97bd;" class="on"><a href="#none" class="btn-tab" id="card">법인 카드</a></li>
						<li id="tax" style="color:#276baa;"><a href="#none" class="btn-tab" id="tax">세금계산서</a></li>
						<li id="etc" style="color:#276baa;"><a href="#none" class="btn-tab" id="etc">일반</a></li>
					</ul>
		
					<div class="tab-contents mgn-t-n30" style="z-index:-1">	
						<!-- Right Grid Area Start  -->
						<div class="panel-body" id="TaxCdMngList">
							<bdo	Debug="Error"
									Data_Url="/hello/eacc/manager/taxCdMngData.do?BILL_GB=1" Data_Format="String"
									Layout_Url="/hello/eacc/manager/taxCdMngLayout.do"
									Upload_Url="/hello/eacc/manager/taxCdMngEditAjax.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols" Upload_Xml="2">
							</bdo>
						</div>
						<!-- Right Grid Area End    -->
					</div>
				</div>
			</div>
		</div>
		<!-- e:tab area -->
	
	</div>

</div>

</body>
</html>
