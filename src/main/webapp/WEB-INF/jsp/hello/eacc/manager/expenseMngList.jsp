<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="aspn.com.common.util.Utility" %>
<%--
  Class Name : expenseMngList.jsp
  Description : 경비현황화면
  Modification Information
 
          수정일       		수정자                   수정내용
    -------    --------    ---------------------------
    2017.08.28  정호윤              최초 생성

    author   : 정호윤
    since    : 2017.08.28
--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script src="/js/eacc/eaccCommon.js" type="text/javascript" ></script>
<%-- 레이어 팝업 --%>
<link rel="stylesheet" href="/css/eacc/modalPopup.css">
<script src="/js/com/web/modalPopup.js"></script>
<script src="/js/jquery/jquery-filestyle.js" type="text/javascript" ></script>
<script src="/js/jquery/jquery.MultiFile.js" type="text/javascript"></script>
<script type="text/javascript" >
var selDivision = ""; // 선택 부서
var selUserId = ""; // 선택 직원
var selTab = ""; // 선택 Tab

$(document).ready(function() {
	
	<%-- 현재 선택된 탭 아이디 조회 --%>
	tab_select_id();
	
	<%-- Tab Click Event --%>
	$(".btn-tab").on('click', function(){
		selTab = $(this).attr("id");
		
		<%-- TAB 변경에 따른 컬럼 재배열 --%>
		fn_gridReplace(selTab);
		
		if(selDivision == "" || selDivision == null || selDivision == "undefined"){
			alert("<spring:message code='expenseMngList.selectDivision' />"); // 부서를 선택하세요.
		}else{
			var billGb = "";
			if(selTab == "card"){
				billGb = "1";
			}else if(selTab == "tax"){
				billGb = "2";
			}else if(selTab == "etc"){
				billGb = "3";
			}
			
			Grids.ExpenseMngList.Source.Data.Url = "/hello/eacc/manager/getExpenseMngData.do?billGb=" + billGb + "&division=" + selDivision + "&userId=" + selUserId + "&startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val();
			Grids.ExpenseMngList.ReloadBody();
		}
	});
	
	<%-- 그리드 load 이벤트 핸들러 --%>
	Grids.OnReady = function(grid){
		if(grid.id == "PopMemberDivList"){
			<%-- GRID COULUMN SHOW/HIDE --%>
			grid.HideCol("EMAIL");
			grid.HideCol("SELECT_YN");
		}
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){
			if(row.GW_STATUS == "01" && row.BILL_STATUS == "R" && row.DEL_YN =="Y"){ //임시전표 생성전 사용자 삭제건
				grid.SetValue(row, "ABILITY", "1",1);
			}
			if(row.GW_STATUS == "05" && row.BILL_STATUS == "X" && row.REPARKING_YN =="Y"){ //반려된 전표에 대하여 재임시전표 생성건
				grid.SetValue(row, "ABILITY", "1",1);
			}
			if(row.GW_STATUS == "01" && row.BILL_STATUS == "X"){ //임시전표 생성후 사용자 임시전표 삭제 건
				grid.SetValue(row, "ABILITY", "1",1);
			}
			if(row.GW_STATUS == "06" && row.BILL_STATUS == "A"){ //실전표 처리후 승인취소 건
				grid.SetValue(row, "ABILITY", "1",1);
			}
			
			// 문서의 최종 결재 상태가 '승인'인 경우 (=관리자 승인취소가 가능한 경우) 만 selecting활성화
			if(row.CANCELAVAILABLE == 'O'){
				grid.SetValue(row, "CanSelect", "1", 1);
			}else{
				grid.SetValue(row, "CanSelect", "0", 1);
			}
		}
	};
	
	<%-- 첨부파일 --%>
	Grids.OnDataReceive = function(grid,source){
		if(grid.id == "ExpenseMngList"){
			for(var row=grid.GetFirst(null);row;row=grid.GetNext(row)){
				if(row.ATTACH_GRP_NO){
					grid.SetAttribute(row, "ATTACH_GRP_NO", "Switch","1",1);
					grid.SetAttribute(row, "ATTACH_GRP_NO", "Icon", "/images/com/web/download.gif",1);
					grid.SetAttribute(row, "ATTACH_GRP_NO", "OnClickSideIcon", "fn_openFileDownModal('"+row.ATTACH_GRP_NO+"');", 1);
				}
			}
		}
	}
	
	Grids.OnClick = function(grid, row, col){
		if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar"){
			return;
		}
		
		if(grid.id == "DivisionList"){
			selDivision = row.DIVISION;
			getMemberList(row.DIVISION, row.CUR_LVL, row.COMPANY_ID);
			
			selUserId = "";
			
			if(selTab == "" || selTab == null || selTab == "undefined"){
				alert("<spring:message code='expenseMngList.selectBillGb' />"); // 전표 유형을 선택하세요.
			}else{
				var billGb = "";
				if(selTab == "card"){
					billGb = "1";
				}else if(selTab == "tax"){
					billGb = "2";
				}else if(selTab == "etc"){
					billGb = "3";
				}
				
				Grids.ExpenseMngList.Source.Data.Url = "/hello/eacc/manager/getExpenseMngData.do?billGb=" + billGb + "&division=" + selDivision + "&userId=&startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val();
				Grids.ExpenseMngList.ReloadBody();
			}
		}
		
		if(grid.id == "PopMemberDivList"){
			if(selDivision == "" || selDivision == null || selDivision == "undefined"){
				alert("<spring:message code='expenseMngList.selectDivision' />"); // 부서를 선택하세요.
			}else{
				selUserId = row.USER_ID;

				if(selTab == "" || selTab == null || selTab == "undefined"){
					alert("<spring:message code='expenseMngList.selectBillGb' />"); // 전표 유형을 선택하세요.
				}else{
					var billGb = "";
					if(selTab == "card"){
						billGb = "1";
					}else if(selTab == "tax"){
						billGb = "2";
					}else if(selTab == "etc"){
						billGb = "3";
					}
					
					Grids.ExpenseMngList.Source.Data.Url = "/hello/eacc/manager/getExpenseMngData.do?billGb=" + billGb + "&division=" + selDivision + "&userId=" + selUserId + "&startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val();
					Grids.ExpenseMngList.ReloadBody();
				}
			}
		}
	}
	
	Grids.OnDblClick = function(grid, row, col){
		if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
			return;
		}
		
		<%-- 그리드 row 더블클릭시 팝업 호출 --%>
		if(grid.id == "ExpenseMngList"){
			var url = "";
			<%-- 그룹웨어 상태가 저장, 상신, 진행중, 완료, 반려, 승인취소 일 경우 전표 상세 팝업 호출 --%>
			if(row.GW_STATUS == "01" || row.GW_STATUS == "02" || row.GW_STATUS == "03" || row.GW_STATUS == "04" || row.GW_STATUS == "05" || row.GW_STATUS == "06"){
				if(selTab == "card"){
					url = "/hello/eacc/cardBill/cardBillDtl.do?cardNdx=" + row.CARD_NDX + "&cardNum=" + row.CARD_NO + "&authNum=" + row.CARD_APPRNO + "&cardBillNdx=" + row.BILL_NDX + "&page=view";
				}else if(selTab == "tax"){
					url = "/hello/eacc/taxBill/taxBillDtl.do?TAXBILL_NDX=" + row.BILL_NDX + "&page=view";
				}else if(selTab == "etc"){
					url = "/hello/eacc/etcBill/etcBillDtl.do?etcBillNdx=" + row.BILL_NDX + "&page=view";
				}
				
				url += "&"+new Date().getTime();
				
				$('#billDtlModal').load(url, function(){
					$(this).modal({
					});
				});
				
				Grids.Active = null;
			}
		}
	}
	
});

function fn_gridReplace(selTab){
	if(selTab == "card"){
		<%-- GRID COULUMN SHOW/HIDE --%>
		Grids.ExpenseMngList.ShowCol("CARD_NO");
		Grids.ExpenseMngList.ShowCol("CARD_APPRNO");
		Grids.ExpenseMngList.ShowCol("CARD_APPRDT");
		Grids.ExpenseMngList.ShowCol("CARD_AQUIDT");
		Grids.ExpenseMngList.ShowCol("CORPORATION_NO");
		Grids.ExpenseMngList.ShowCol("CARD_NDX");

		Grids.ExpenseMngList.HideCol("TAXAPPR_NO");
	}else if(selTab == "tax"){
		<%-- GRID COULUMN SHOW/HIDE --%>
		Grids.ExpenseMngList.ShowCol("TAXAPPR_NO");
		
		Grids.ExpenseMngList.HideCol("CARD_NO");
		Grids.ExpenseMngList.HideCol("CARD_APPRNO");
		Grids.ExpenseMngList.HideCol("CARD_APPRDT");
		Grids.ExpenseMngList.HideCol("CARD_AQUIDT");
		Grids.ExpenseMngList.HideCol("CORPORATION_NO");
		Grids.ExpenseMngList.HideCol("CARD_NDX");
	}else if(selTab == "etc"){
		<%-- GRID COULUMN SHOW/HIDE --%>
		Grids.ExpenseMngList.HideCol("TAXAPPR_NO");
		Grids.ExpenseMngList.HideCol("CARD_NO");
		Grids.ExpenseMngList.HideCol("CARD_APPRNO");
		Grids.ExpenseMngList.HideCol("CARD_APPRDT");
		Grids.ExpenseMngList.HideCol("CARD_AQUIDT");
		Grids.ExpenseMngList.HideCol("CORPORATION_NO");
		Grids.ExpenseMngList.HideCol("CARD_NDX");
	}
}

function tab_select_id(){
	<%-- 현재 선택된 탭 아이디 조회 --%>
	$('.tab-depth01 li').each(function(e){
		if($(this).hasClass('on')){
			selTab = $(this).attr("id");
		}
	});
	return selTab;
}

function fn_searchData(){
	if(selDivision == "" || selDivision == null || selDivision == "undefined"){
		alert("<spring:message code='expenseMngList.selectDivision' />"); // 부서를 선택하세요.
	}else{
		if(selTab == "" || selTab == null || selTab == "undefined"){
			alert("<spring:message code='expenseMngList.selectBillGb' />"); // 전표 유형을 선택하세요.
		}else{
			var billGb = "";
			if(selTab == "card"){
				billGb = "1";
			}else if(selTab == "tax"){
				billGb = "2";
			}else if(selTab == "etc"){
				billGb = "3";
			}
			
			Grids.ExpenseMngList.Source.Data.Url = "/hello/eacc/manager/getExpenseMngData.do?billGb=" + billGb + "&division=" + selDivision + "&userId=" + selUserId + "&startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val();
			Grids.ExpenseMngList.ReloadBody();
		}
	}
}
</script>
</head>
<body>

<!-- 화면 UI Area Start  -->
<div id="contents">

	<!-- Hidden Area Start -->
	<!-- Hidden Area End   -->

	<!-- Left Area Start  -->
	<div class="fl-box panel-wrap03" style="width:20%; height:100%;">
		<!-- Left Title Area Start  -->
		<h5 class="panel-tit"><spring:message code='title.division' /></h5> <!-- 부서 정보 -->
		<!-- Left Title Area End    -->
	
		<!-- Left Grid Area Start   -->
		<div class="panel-body">
			<div id="memberPrivList">
				<bdo	Debug="Error"
						Data_Url="/mem/division/divisionData.do"
						Layout_Url="/mem/division/divisionLayout.do"
						Upload_Url="/mem/division/divisionEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols"				
						>
				</bdo>
			</div>
		</div>
		<!-- Left Grid Area End     -->
	</div>
	<!-- Left Area End    -->

	<!-- Middle Area Start -->
	<div class="fl-box panel-wrap03" style="width:15%; height:100%;">
		<!-- Middle Title Area Start -->
		<h5 class="panel-tit mgn-l-10"><spring:message code='title.member' /></h5> <!-- 직원 정보 -->
		<!-- Middle Title Area End   -->
	
		<!-- Middle Grid Area Start  -->
		<div class="panel-body mgn-l-10">
			<div id="PopMemberDivList">
				<bdo	Debug="Error"
						Layout_Url="/hello/eacc/common/popMemberDivLayout.do?page=none"
						>
				</bdo>	
			</div>
		</div>
		<!-- Middle Grid Area End    -->
	</div>
	<!-- Middle Area End   -->

<!-- Right Area Start -->
<div class="fl-box panel-wrap05 " style="width:64.5%; height:100%;">
	<!-- Right Title Area Start -->
	<h5 class="panel-tit mgn-l-10" style="top:19px;"><spring:message code='title.expense' /></h5> <!-- 경비 현황 -->
	<!-- Right Title Area End   -->
	
		<!-- s:tab area -->
		<div class="fl-box panel-wrap top mgn-l-10 mgn-t-30" style="height:100%">
			<!-- 원하는 비율로 직접 지정하여 사용 -->
			<div class="panel-body">
				<div class="tab-wrap">
					<ul class="tab-depth01">
						<li id="card" style="color:4b97bd;" class="on"><a href="#none" class="btn-tab" id="card"><spring:message code='bill.card' /></a></li>
						<li id="tax" style="color:#276baa;"><a href="#none" class="btn-tab" id="tax"><spring:message code='bill.tax' /></a></li>
						<li id="etc" style="color:#276baa;"><a href="#none" class="btn-tab" id="etc"><spring:message code='bill.etc' /></a></li>
					</ul>
	
					<div class="tab-contents">
						<!-- top -->
						<div class="inq-area-top ab" style="padding-top:7px; width:50%;">
							<ul class="wrap-inq mgn-l-10">
								<li class="inq-clmn">
									<h4 class="tit-inq"><spring:message code='expense.bookDt' /></h4> <!-- 전기일자 -->
									<div class="prd-inp-wrap">
										<span class="prd-inp">
											<span class="inner">
												<input type="text" class="inp-comm datePic openCal thisMonth" readonly="readonly" gldp-id="gldp-7636599364" id="startDt"/>
											</span>
										</span>
										<span class="prd-inp">
											<span class="inner">
												<input type="text" class="inp-comm datePic openCal" readonly="readonly" gldp-id="gldp-8530641166" id="endDt" />
											</span>
										</span>
									</div>
								</li>
							</ul>
							<a href="#" class="btn comm st01" onclick="fn_searchData(); return false;"><spring:message code='button.search' /></a> <!-- 검색 -->
						</div>
						<!-- top -->
	
						<!-- Right Grid Area Start  -->
						<div class="panel-body" id="ExpenseMngList">
							<bdo	Debug="Error"
									Layout_Url="/hello/eacc/manager/expenseMngLayout.do"
									>
							</bdo>
						</div>
						<!-- Right Grid Area End    -->
					</div>
				</div>
			</div>
		</div>
		<!-- e:tab area -->

</div>
<!-- Right Area End   -->

<!-- 전표 상세 화면 모달 시작-->
<div class="modal fade" id="billDtlModal"></div>
<!-- 전표 상세 화면 모달 끝-->

<!-- 그리드 첨부파일 다운로드 모달 -->
<div class="modal fade" id="fileDownModal" data-backdrop="static" data-keyboard="false"></div>
<!-- 그리드 첨부파일 다운로드 모달 -->

</div>
<!-- 화면 UI Area End    -->

</body>
</html>
