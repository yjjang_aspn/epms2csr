<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : expenseMngLayout.jsp
  Description : 경비 현황  그리드 레이아웃 화면
  Modification Information
 
       수정일    	     수정자                   수정내용
   -------    --------    ---------------------------
   2017.08.28  정호윤	              최초 생성

    author   : 정호윤
    since    : 2017.08.28
--%>
<c:set var="gridId" value="ExpenseMngList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			AcceptEnters   = "1" Calculated = "1"  CalculateSelected = "1"      DateStrings = "2" Deleting  = "0"
			Dragging       = "0" Editing    = "1"  EnterMode         = "4"      Filtering   = "1" IdChars   = "0123456789"
			InEditMode     = "2" MaxPages   = "20" NoFormatEscape    = "1"      NoHScroll   = "0" NoVScroll = "0"
			NumberId       = "1" SafeCSS    = '1'  Selecting		 = "1"
			SelectingCells = "0" SortIcons  = "0"  Style             = "Office" SuppressCfg = "1" 
			Paging		   = "2" AllPages	= "0"  PageLength		 = "24"		MaxPages	= "20"
			CopySelected   = "0" CopyFocused= "1"  CopyCols			 = "0"		ExportFormat="xls" ExportCols="0"
	/>
	
	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>
	
	<!-- Grid Header Setting Area Start  -->
	<Header	id="Header"	Align="center"
			ABILITY			= ""
			COMPANY_ID 		= "<spring:message code='company.companyId' />"
			USER_ID		 	= "<spring:message code='member.userId' />"
			NAME		 	= "<spring:message code='member.name' />"
			BILL_NDX		= "<spring:message code='expense.billNdx' />"
			ERPBILLYEAR 	= "<spring:message code='expense.erpBillYer' />"
			ERPBILL 		= "<spring:message code='expense.erpBill' />"
			BILL_STATUS 	= "<spring:message code='expense.billStatus' />"
			GW_STATUS	 	= "<spring:message code='expense.gwStatus' />"
			CANCELAVAILABLE	= "<spring:message code='expense.apprHSts' />"
			CARD_NO		 	= "<spring:message code='card.cardNo' />"
			CARD_APPRNO 	= "<spring:message code='card.cardApprNo' />"
			CARD_APPRDT 	= "<spring:message code='card.cardApprDt' />"
			CARD_AQUIDT 	= "<spring:message code='card.cardAquiDt' />"
			CORPORATION_NO 	= "<spring:message code='card.corporationNo' />"
			CARD_NDX		= "<spring:message code='card.cardNdx' />"
			TAXAPPR_NO		= "<spring:message code='tax.taxApprNo' />"
			CURRENCY_UNIT 	= "<spring:message code='expense.currencyUnit' />"
			RATE		 	= "<spring:message code='expense.rate' />"
			TOT_AMOUNT	 	= "<spring:message code='expense.totAmount' />"
			AMT_AMOUNT	 	= "<spring:message code='expense.amtAmount' />"
			VAT_AMOUNT	 	= "<spring:message code='expense.vatAmount' />"
			SER_AMOUNT	 	= "<spring:message code='expense.serAmount' />"
			FRE_AMOUNT	 	= "<spring:message code='expense.freAmount' />"
			BOOK_DT		 	= "<spring:message code='expense.bookDt' />"
			EVIDENCE_DT		= "<spring:message code='expense.evidenceDt' />"
			WORKPLACE_CD	= "<spring:message code='expense.workplaceCd' />"
			WORKPLACE_NM	= "<spring:message code='expense.workplaceNm' />"
			ACCGUBUN		= "<spring:message code='expense.accgubun' />"
			TAX_CD		 	= "<spring:message code='expense.taxCd' />"
			PURCHASEOFFI_CD	= "<spring:message code='expense.purchaseOffiCd' />"
			PURCHASEOFFI_NM	= "<spring:message code='expense.purchaseOffiNm' />"
			BANK_CD		 	= "<spring:message code='expense.bankCd' />"
			BANK_NM		 	= "<spring:message code='expense.bankNm' />"
			PAYEE_CD        = "<spring:message code='expense.payeeCd' />"
			PAYEE_NM        = "<spring:message code='expense.payeeNm' />"
			ACCOUNT_NO		= "<spring:message code='expense.accountNo' />"
			ACCOUNT_HOLDER	= "<spring:message code='expense.accountHolder' />"
			PAY_TERM        = "<spring:message code='expense.payTerm' />"
			PAYDUE_DT       = "<spring:message code='expense.paydueDt' />"
			ERR_MSG         = "<spring:message code='expense.errMsg' />"
			REG_DT          = "<spring:message code='expense.regDt' />"
			UPT_USERID      = "<spring:message code='expense.uptUserId' />"
			UPT_DT          = "<spring:message code='expense.uptDt' />"
			DEL_YN          = "<spring:message code='expense.delYn' />"
			DEL_USERID      = "<spring:message code='expense.delUserId' />"
			DEL_DT          = "<spring:message code='expense.delDt' />"
			ADD_PRSN        = "<spring:message code='expense.addPrsn' />"
			ADD_PRSN_NM     = "<spring:message code='expense.addPrsnNm' />"
			ATTACH_GRP_NO	= "<spring:message code='expense.attachGrpNo' />"
			
     />
	<!-- Grid Header Setting Area End    -->
	
	<!-- Fileter Setting Area Start      -->
	<Head>
		<Filter	id = "Filter" CanFocus = "1"/>
	</Head>
	<!-- Fileter Setting Area End        -->
	
	<Cols>
		<C Name="ABILITY"			Type="Enum"	 Width="40"  				 CanEdit="0"
									EmptyValue="" Enum="||&lt;div style='background:10px 0px url(/images/icon/web/dltIcon.png) no-repeat; background-size: 15px; height:15px;'/&gt;&lt;/div&gt;" EnumKeys="||1"/>
		<C Name="COMPANY_ID"		Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="0" />
		<C Name="USER_ID"			Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="0" />
		<C Name="NAME"				Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="BILL_NDX"			Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="0" />
		<C Name="ERPBILLYEAR"		Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="ERPBILL"			Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="BILL_STATUS" 		Type="Enum"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1"
									EmptyValue="" <tag:enum codeGrp="BILL_STATUS" /> />
		<C Name="GW_STATUS" 		Type="Enum"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1"
								  	EmptyValue="" <tag:enum codeGrp="GW_STATUS" /> />
		<C Name="CANCELAVAILABLE"	Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="0" />
		<C Name="BOOK_DT" 			Type="Date"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		<C Name="EVIDENCE_DT" 		Type="Date"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		<C Name="CARD_NO"			Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="CARD_APPRNO"		Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="CARD_APPRDT" 		Type="Date"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		<C Name="CARD_AQUIDT" 		Type="Date"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		<C Name="CORPORATION_NO"	Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="CARD_NDX"			Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="TAXAPPR_NO"		Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="CURRENCY_UNIT" 	Type="Text"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" />
		<C Name="RATE" 				Type="Text"  Width="100" CanEdit="0" Visible="1" Align="right" CanExport="0" />
		<C Name="TOT_AMOUNT" 		Type="Int"   Width="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="AMT_AMOUNT" 		Type="Int"   Width="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="VAT_AMOUNT" 		Type="Int"   Width="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="SER_AMOUNT" 		Type="Int"   Width="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="FRE_AMOUNT" 		Type="Int"   Width="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="WORKPLACE_CD" 		Type="Text"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" />
		<C Name="WORKPLACE_NM" 		Type="Text"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="ACCGUBUN" 			Type="Enum"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1"
									EmptyValue="" <tag:enum codeGrp="ACCGUBUN" /> />
		<C Name="TAX_CD" 			Type="Enum"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1"
									EmptyValue="" <tag:taxEnum billGb="TAX" /> />
		<C Name="PURCHASEOFFI_CD" 	Type="Text"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" />
		<C Name="PURCHASEOFFI_NM" 	Type="Text"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" />
		<C Name="BANK_CD" 			Type="Text"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" />
		<C Name="BANK_NM" 			Type="Text"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" />
		<C Name="PAYEE_CD" 			Type="Text"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" />
		<C Name="PAYEE_NM" 			Type="Text"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" />
		<C Name="ACCOUNT_NO" 		Type="Text"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" />
		<C Name="ACCOUNT_HOLDER" 	Type="Text"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" />
		<C Name="PAY_TERM" 			Type="Text"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" />
		<C Name="PAYDUE_DT" 		Type="Date"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" Format="yyyy-MM-dd" />
		<C Name="ADD_PRSN" 			Type="Text"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" />
		<C Name="ADD_PRSN_NM" 		Type="Text"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="ATTACH_GRP_NO" 	Type="Icon"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="ERR_MSG" 			Type="Text"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="REG_DT" 			Type="Date"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		<C Name="UPT_USERID" 		Type="Text"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" />
		<C Name="UPT_DT" 			Type="Date"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" Format="yyyy-MM-dd" />
		<C Name="DEL_YN" 			Type="Enum"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" />
		<C Name="DEL_USERID" 		Type="Text"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" />
		<C Name="DEL_DT" 			Type="Date"  Width="100" CanEdit="0" Visible="1" Align="center" CanExport="0" Format="yyyy-MM-dd" />
	</Cols>
	
	<Foot>
		<I Calculated = "1" type = "Int" Spanned = "1" Align = "right" CanExport = "1" Format = "#,##0"
			NAME = "<spring:message code='foot.total' />" NAMEColor = "#37aad9" NAMEClass = "gridStatus5" NAMESpan = "17" NAMEAlign = "center"
 			TOT_AMOUNTFormula='sum()' AMT_AMOUNTFormula='sum()' VAT_AMOUNTFormula='sum()' SER_AMOUNTFormula='sum()' FRE_AMOUNTFormula='sum()'
 		/>
	</Foot>
		
	<!-- Grid Paging Setting Area Start  -->
	<Pager Visible = "0"/>

	<Solid>
		<I id = "PAGER" Cells = "NAV,LIST,ONE,GROUP" Space = "4"
		   NAVType	     = "Pager"
		   LISTType	     = "Pages" LISTAlign   = "left" LISTLeft   = "10"                LISTRelWidth  = "1"
		   ONEType       = "Bool"  ONECanEdit  = "1"    ONEFormula = "Grid.AllPages?0:1" ONELabelRight = "<spring:message code='button.showUnitPage' />"
		   ONEOnChange = "Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
		   GROUPCanFocus = "0"
		/>
	</Solid>
	<!-- Grid Paging Setting Area End    -->
	
	<Toolbar	Space="0"	Styles="1"	Cells="Empty,Cnt,Found,<spring:message code='button.selectItem' />,<spring:message code='button.refresh' />,<spring:message code='button.print' />,<spring:message code='button.excel' />"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType = "Button"
				CntType = "Html" CntFormula = '"Total : &lt;b>"+count(7)+"&lt;/b>"' CntWidth = '-1' CntWrap = '0'
				FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>'" FoundWidth = '-1' FoundWrap = '0'
				<spring:message code='button.refresh' />Type = "Html" <spring:message code='button.refresh' /> = "&lt;a href='#none' title='<spring:message code='button.refresh' />' class=&quot;defaultButton icon refresh&quot;
									  onclick='reloadGrid(&quot;${gridId}&quot;)'><spring:message code='button.refresh' />&lt;/a>"
 				<spring:message code='button.print' />Type = "Html" <spring:message code='button.print' /> = "&lt;a href='#none' title='<spring:message code='button.print' />' class=&quot;defaultButton icon printer&quot;
									  onclick='printGrid(&quot;${gridId}&quot;)'><spring:message code='button.print' />&lt;/a>"
 				<spring:message code='button.excel' />Type = "Html" <spring:message code='button.excel' /> = "&lt;a href='#none' title='<spring:message code='button.excel' />'class=&quot;defaultButton icon excel&quot;
									  onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'><spring:message code='button.excel' />&lt;/a>"
				<spring:message code='button.selectItem' />Type="Html" <spring:message code='button.selectItem' />="&lt;a href='#none' title='<spring:message code='button.selectItem' />' class=&quot;treeButton treeSelect&quot;
			  						  onclick='showColumns(&quot;${gridId}&quot;,&quot;xls&quot;)'><spring:message code='button.selectItem' />&lt;/a>"
	/>

</Grid>

