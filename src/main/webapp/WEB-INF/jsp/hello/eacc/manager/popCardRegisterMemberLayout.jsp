<?xml version="1.0" encoding="UTF-8"?>
<%@ page contentType="text/xml;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : userCardInfoLayout.jsp
  Description : 법인카드 관리 페이지 레이아웃
  Modification Information
  
  수정일			수정자			수정내용
  ------			------			--------
  2017-08-28		강승상			최초생성
  
  author	: 강승상
  since		: 2017-08-28
--%>
<c:set var="gridId" value="PopCardRegisterMemberList" />
<Grid>
	<Cfg	id="${gridId}"
			AcceptEnters   = "1" Calculated = "1"  CalculateSelected = "1"      DateStrings = "2"	Deleting = "0"
			Dragging       = "0" Editing    = "1"  EnterMode         = "4"      Filtering   = "1" IdChars   = "0123456789"
			InEditMode     = "2" MaxPages   = "20" NoFormatEscape    = "1"      NoHScroll   = "0" NoVScroll = "0"
			NumberId       = "1" SafeCSS    = '1'  Selecting		 = "1"
			SelectingCells = "0" SortIcons  = "0"  Style             = "Office" SuppressCfg = "1"
			Paging		   = "2" AllPages	= "0"  PageLength		 = "20"		Panel = "0"
			CopySelected   = "0" CopyFocused= "1"  CopyCols			 = "0"		ExportFormat="xls" ExportCols="0"
	/>

	<%-- Panel hide --%>
	<Panel Visible="0" />

	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>

	<!-- Grid Header Setting Area Start  -->
	<Header	id="Header"	Align="center"
		    USER_CARD_NDX		= "카드등록 KEY"
		    COMPANY_ID			= "회사코드"
		    USER_ID				= "사번"
		    NAME				= "이름"
		    CARD_NUM			= "카드번호"
	/>
	<!-- Grid Header Setting Area End    -->

	<!-- Fileter Setting Area Start      -->
	<Head>
		<Filter	id = "Filter" CanFocus = "1"/>
	</Head>
	<!-- Fileter Setting Area End        -->

	<Cols>
		<C Name="USER_CARD_NDX"		Type="Text"	 RelWidth="50"    Align="center" CanEdit="0" Visible="0" />
		<C Name="CARD_NUM"			Type="Text"	 RelWidth="150"   Align="center" CanEdit="0" Visible="1" />
		<C Name="COMPANY_ID"		Type="Text"	 RelWidth="50"    Align="center" CanEdit="0" Visible="1" />
		<C Name="USER_ID"			Type="Text"	 RelWidth="100"   Align="center" CanEdit="0" Visible="1" />
		<C Name="NAME"				Type="Text"	 RelWidth="100"   Align="center" CanEdit="0" Visible="1" />
	</Cols>

	<!-- Grid Paging Setting Area Start  -->
	<Pager Visible = "0"/>

	<Solid>
		<I id = "PAGER" Cells = "NAV,LIST,ONE,GROUP" Space = "4"
		   NAVType	     = "Pager"
		   LISTType	     = "Pages" LISTAlign   = "left" LISTLeft   = "10"                LISTRelWidth  = "1"
		   ONEType       = "Bool"  ONECanEdit  = "1"    ONEFormula = "Grid.AllPages?0:1" ONELabelRight = "페이지단위로보임"
		   ONEOnChange = "Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
		   GROUPCanFocus = "0"
		/>
	</Solid>
	<!-- Grid Paging Setting Area End    -->

	<Toolbar	Space="0"	Styles="1"	Cells="Empty,Cnt,Found,새로고침,인쇄,엑셀"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType = "Button"
				CntType = "Html" CntFormula = '"Total : &lt;b>"+count(7)+"&lt;/b>"' CntWidth = '-1' CntWrap = '0'
				FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>'" FoundWidth = '-1' FoundWrap = '0'
				새로고침Type = "Html" 새로고침 = "&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
									  onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type = "Html" 인쇄 = "&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
									  onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
				엑셀Type = "Html" 엑셀 = "&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
									  onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>

</Grid>
