<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="aspn.com.common.util.Utility" %>
<%--
  Class Name : taxBillReportList.jsp
  Description : 세금계산서 리포트 목록 화면
  Modification Information
 
          수정일       		수정자                   수정내용
    -------    --------    ---------------------------
    2017.08.28  윤선철              최초 생성

    author   : 윤선철
    since    : 2017.08.28
--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<link rel="stylesheet" href="/css/eacc/modalPopup.css">
<script src="/js/eacc/eaccCommon.js" type="text/javascript" ></script>
<script src="/js/com/web/modalPopup.js"></script>
<script src="/js/jquery/jquery-filestyle.js" type="text/javascript" ></script>
<script src="/js/jquery/jquery.MultiFile.js" type="text/javascript"></script>
<script src="/js/jquery/jquery.ajaxsubmit.js" type="text/javascript"></script>
<script type="text/javascript" >
var gubunDiv = '〔';
var layerGubun = "transTaxPerson"; // 레이어 팝업 구분자(추가직원-addPerson, 이관-transPerson)
var transGrid = ""; // 이관대상자 그리드
var transCol = ""; // 이관대상자 컬럼
var transRow = []; // 이관대상자 row
$(document).ready(function(){
		
	$('#searchVal').keydown(function(key){
			<%-- 사업장 검색 Enter Key Down Event --%>
			if(key.keyCode == 13){
				fn_searchData();
			}
	});
	/* 데이터 URL */
	var parameters = "startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val();
	var url = "/hello/eacc/manager/getTaxReportData.do?" + parameters;
	$("#taxBillReportList").find('bdo').attr("Data_Url", url);
	
	//그리드 row 더블클릭시 에빈트
	Grids.OnDblClick = function(grid, row, col){
		if(row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "_ConstHeight") {
			return;
		}
		
		if(col == "RECEIVE_EMAIL"){
			if(row.id == "Filter"){
				var selRow = Grids.TaxBillReportList.GetSelRows();
				if(selRow.length == 0){
					alert("<spring:message code='taxBillReportList.selectTransferTax' />"); // 이관할 세금계산서를 선택하세요.
					return;
				}else{
					transGrid = grid.id; // 이관대상자 그리드
					transCol = col; // 이관대상자 컬럼
					for(var i = 0 ; i < selRow.length ; i++) {
						transRow.push(selRow[i]);
					}
					layer_open('memberModal');
				}
			}else{
				transGrid = grid.id; // 이관대상자 그리드
				transCol = col; // 이관대상자 컬럼
				transRow.length = 0;
				transRow.push(row);
				layer_open('memberModal');
			}
		}
				
	}	
	//그리드 저장 후 이벤트
	Grids.OnAfterSave = function(grid, result, autoupdate) {
		grid.ReloadBody();
	}
	
	Grids.OnChange = function(grid, row, col){
		if(col == "RECEIVE_EMAIL"){
			Grids["TaxBillReportList"].SetValue(row, "SEND_NM", "", 1);
			Grids["TaxBillReportList"].SetValue(row, "RECEIVE_NM", "", 1);
			Grids["TaxBillReportList"].SetValue(row, "SEND_DT", "", 1);
			Grids["TaxBillReportList"].SetAttribute(row, "SEND_YN", "Color", "#FFFFAA",1);
			Grids["TaxBillReportList"].SetAttribute(row, "SEND_NM", "Color", "#FFFFAA",1);
			Grids["TaxBillReportList"].SetAttribute(row, "RECEIVE_NM", "Color", "#FFFFAA",1);
			Grids["TaxBillReportList"].SetAttribute(row, "RECEIVE_EMAIL", "Color", "#FFFFAA",1);
			Grids["TaxBillReportList"].SetAttribute(row, "SEND_DT", "Color", "#FFFFAA",1);
		}
	}
});

/* 날짜 검색 */
function fn_searchData() {
	var parameters = "startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val() + "&searchVal=" + $("#searchVal").val();
	Grids.TaxBillReportList.Source.Data.Url = "/hello/eacc/manager/getTaxReportData.do?" + parameters;
	Grids.TaxBillReportList.ReloadBody();
}

//그리드 리로드
function fn_reloadGrid(){
	Grids.TaxBillReportList.ReloadBody();
}

//모달 토글
function toggleModal(obj){
	obj.modal('toggle');
}

//제외 해제
function fn_cancelDel(gridNm){
	<%-- 제외 해제 --%>
	var zissid = "";
	if(gridNm == "TaxBillReportList"){
		var selRow = Grids.TaxBillReportList.GetSelRows();
		if(selRow.length == 0){
			alert("<spring:message code='taxBillReportList.selectReleaseExceptTax' />"); // 제외 해제할 세금계산서를 선택하세요.
			return;
		}else{
			for(var i = 0 ; i < selRow.length ; i++) {
				if(selRow[i].DEL_YN == 'X'){
					alert("<spring:message code='taxBillReportList.selectExceptTax' />"); // 제외된 세금계산서만 선택해주세요.
					return;
				}else{
					if(zissid == ""){
						zissid = selRow[i].ZISSID;
					}else{
						zissid += gubunDiv + selRow[i].ZISSID;
					}					
				}
			}
			
			if(confirm("<spring:message code='taxBillReportList.wantReleaseExceptTax' />")){ // 선택하신 세금계산서를 제외 해제 하시겠습니까?
				$.ajax({
					type: 'POST',
					url: '/hello/eacc/taxBill/taxMasterCancelExceptAjax.do',
					async : false, 
					dataType: 'json',
					data : {ZISSID : zissid},
					success: function (json) {
						if(json.result == "T"){
							alert(json.resultMsg);
							fn_reloadGrid();
						}else{
							alert(json.resultMsg);
							fn_reloadGrid();
						}
					},error: function(XMLHttpRequest, textStatus, errorThrown){
						alert("<spring:message code='taxBillReportList.errReleaseExceptTax' />"); // [시스템오류]제외 해제 처리 중 오류가 발생하였습니다.
						fn_reloadGrid();
					}
				});
			}
		}
	}
}

//이관
function fn_passTax(gridNm){
	var receiveEmail = "";
	var zissid = "";
	<%-- 이관 --%>
	if(gridNm == "TaxBillReportList"){
		var selRow = Grids.TaxBillReportList.GetSelRows();
		if(selRow.length == 0){
			alert("<spring:message code='taxBillReportList.selectTransferTax' />"); // 이관할 세금계산서를 선택하세요.
			return;
		}else{
			for(var i = 0 ; i < selRow.length ; i++) {
				if(selRow[i].RECEIVE_EMAIL == "" || selRow[i].RECEIVE_EMAIL == null || selRow[i].RECEIVE_EMAIL == "undefined"){
					alert("<spring:message code='taxBillReportList.checkTransferEmail' />"); // 이관할 이메일 주소를 확인해 주세요.
					return;
				}else{
					if(receiveEmail == ""){
						receiveEmail = selRow[i].RECEIVE_EMAIL;
						zissid = selRow[i].ZISSID;
					}else{
						receiveEmail += gubunDiv + selRow[i].RECEIVE_EMAIL;
						zissid += gubunDiv + selRow[i].ZISSID;
					}
				}
			}
			
			if(confirm("<spring:message code='taxBillReportList.wantTransferTax' />")){ // 선택하신 세금계산서를 이관하시겠습니까?
				$.ajax({
					type: 'POST',
					url: '/hello/eacc/taxBill/updatePassTaxAjax.do',
					async : false, 
					dataType: 'json',
					data : {RECEIVE_EMAIL : receiveEmail, ZISSID : zissid},
					success: function (json) {
						if(json.result == "T"){
							alert(json.resultMsg);
							fn_reloadGrid();
						}else{
							alert(json.resultMsg);
							fn_reloadGrid();
						}
					},error: function(XMLHttpRequest, textStatus, errorThrown){
						alert("<spring:message code='taxBillReportList.errTransferTax' />"); // 세금계산서 이관 중 오류가 발생하였습니다.
						fn_reloadGrid();
					}
				});
			}
		}
	}
}
</script>
</head>
<body>

<!-- 화면 UI Area Start  -->
<div id="contents">

<!-- Hidden Area Start -->
<!-- Hidden Area End   -->

	<div class="fl-box panel-wrap03" style="width:100%; height:100%;">
	<!-- <div class="panel-wrap st01"> -->
			<h5 class="panel-tit mgn-l-10"><spring:message code='title.taxbillReport' /></h5> <!-- 세금계산서 리포트 -->
			
			<!-- inq-area-inner : s -->
			<div class="inq-area-inner type06 ab">
				<ul class="wrap-inq">
					<li class="inq-clmn">
						<h4 class="tit-inq"><spring:message code='tax.zmkdt' /></h4>
						<div class="prd-inp-wrap">
							<span class="prd-inp">
								<span class="inner">
									<input type="text" class="inp-comm datePic openCal thisMonth" readonly="readonly" gldp-id="gldp-7636599364" id="startDt"/>
								</span>
							</span>
							<span class="prd-inp">
								<span class="inner">
									<input type="text" class="inp-comm datePic openCal" readonly="readonly" gldp-id="gldp-8530641166" id="endDt" />
								</span>
							</span>
						</div>
					</li>
					<li class="inq-clmn">
						<h4 class="tit-inq"><spring:message code='tax.buynoNm' /></h4> <!-- 사업장 -->
						<div class="prd-inp-wrap">
							<input type="text" class="inp-wrap type03" id="searchVal" name="searchVal" value=""/>
						</div>
					</li>
				</ul>
				<a href="#" class="btn comm st01" onclick="fn_searchData(); return false;"><spring:message code='button.search' /></a> <!-- 검색 -->
				
<!-- 				<a href="#" class="btn comm st02" onclick="fn_popDeleteTaxBill(); return false;">일괄제외</a> -->
			</div>
			<!-- inq-area-inner : e -->
			<div class="panel-body mgn-t-25">
				<div id="taxBillReportList" style="height:100%;">
					<bdo	Debug="Error"
							Layout_Url="/hello/eacc/manager/taxBillReportListLayout.do"
						>
					</bdo>
				</div>
			</div>
			<!-- e:grid-wrap -->
	</div>
</div>

<%@ include file="/WEB-INF/jsp/hello/eacc/common/popMemberDivList.jsp"%>
<%@ include file="/WEB-INF/jsp/hello/eacc/common/popNeedBill.jsp"%>
<!-- Layer PopUp End -->

<!-- 화면 UI Area End    -->
</body>
</html>
