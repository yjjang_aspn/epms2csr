<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%--
  Class Name : popAccountLayout.jsp
  Description : 계정 관리 그리드 레이아웃 화면
  Modification Information
 
       수정일    	     수정자                   수정내용
   -------    --------    ---------------------------
   2017.08.16   정호윤	              최초 생성

    author   : 정호윤
    since    : 2017.08.16
--%>
<c:set var="gridId" value="PopAccountList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			AcceptEnters   = "1" Calculated = "1"  CalculateSelected = "1"      DateStrings = "2" Deleting  = "0"
			Dragging       = "0" Editing    = "1"  EnterMode         = "4"      Filtering   = "1" IdChars   = "0123456789"
			InEditMode     = "2" MaxPages   = "20" NoFormatEscape    = "1"      NoHScroll   = "0" NoVScroll = "0"
			NumberId       = "1" SafeCSS    = '1'  Selecting		 = "0"
			SelectingCells = "0" SortIcons  = "0"  Style             = "Office" SuppressCfg = "1" 
			Paging		   = "2" AllPages	= "0"  PageLength		 = "22"		MaxPages	= "22"
			CopySelected   = "0" CopyFocused= "1"  CopyCols			 = "0"		ExportFormat="xls" ExportCols="0"
	/>
	
	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>
	
	<!-- Grid Header Setting Area Start  -->
	<Header	id="Header"	Align="center"
			COMPANY_ID		= "<spring:message code='company.companyId' />"
			GLGROUP			= "<spring:message code='account.glGroup' />"	
			GLCODE	    	= "<spring:message code='account.glCode' />"
			GLNAME 	    	= "<spring:message code='account.glName' />"
			GLTYPE		 	= "<spring:message code='account.glType' />"
			GLDETAIL		= "<spring:message code='account.glDetail' />"
			SELECT_YN		= "<spring:message code='expense.selectYn' />"
	/>
	<!-- Grid Header Setting Area End    -->
	
	<!-- Fileter Setting Area Start      -->
	<Head>
		<Filter	id = "Filter" CanFocus = "1"/>
	</Head>
	<!-- Fileter Setting Area End        -->
	
	<Cols>
		<C Name="COMPANY_ID"	    Type="Text"	    RelWidth="100"     Align="center"	CanEdit="0" 	Visible="0" /><!-- 회사코드 --> 
		<C Name="GLGROUP"	    	Type="Text"	    RelWidth="100"     Align="left"		CanEdit="0" 	Visible="0" /><!-- 계정그룹 --> 
		<C Name="GLCODE"	    	Type="Text"  	RelWidth="100"     Align="center"	CanEdit="0"		Visible="1" /><!-- 계정코드 -->	
		<C Name="GLNAME"	    	Type="Text"     RelWidth="200"     Align="left"		CanEdit="0" 	Visible="1" /><!-- 계정명 -->  
		<C Name="GLTYPE"	    	Type="Text"     RelWidth="100"     Align="center"	CanEdit="0" 	Visible="0" /><!-- 계정타입 --> 
		<C Name="GLDETAIL"	   		Type="Text"  	RelWidth="100"     Align="center"	CanEdit="0"		Visible="0" /><!-- 계정설명 -->
		<C Name="SELECT_YN"	  		Type="Bool" 	RelWidth="70" 	   Align="center"	CanEdit="1"		Visibile="1"/><!-- 선택여부 -->
	</Cols>
	
	<!-- Grid Paging Setting Area Start  -->
	<Pager Visible = "0"/>

	<Solid>
		<I id = "PAGER" Cells = "NAV,LIST,ONE,GROUP" Space = "4"
		   NAVType	     = "Pager"
		   LISTType	     = "Pages" LISTAlign   = "left" LISTLeft   = "10"                LISTRelWidth  = "1"
		   ONEType       = "Bool"  ONECanEdit  = "1"    ONEFormula = "Grid.AllPages?0:1" ONELabelRight = "<spring:message code='button.showUnitPage' />"
		   ONEOnChange = "Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
		   GROUPCanFocus = "0"
		/>
	</Solid>
	<!-- Grid Paging Setting Area End    -->
	
	<Toolbar	Space="0"	Styles="1"	Cells="Empty,Cnt,Found,<spring:message code='button.add' />,새로고침"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType = "Button"
				CntType = "Html" CntFormula = '"Total : &lt;b>"+count(7)+"&lt;/b>"' CntWidth = '-1' CntWrap = '0'
				FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>'" FoundWidth = '-1' FoundWrap = '0'
				<spring:message code='button.add' />Type = "Html" <spring:message code='button.add' /> = "&lt;a href='#none' title='<spring:message code='button.add' />' class=&quot;defaultButton01 icon add&quot;
							       onclick='addRowGrid(&quot;${gridId}&quot;)'><spring:message code='button.add' />&lt;/a>"
				<spring:message code='button.refresh' />Type = "Html" <spring:message code='button.refresh' /> = "&lt;a href='#none' title='<spring:message code='button.refresh' />' class=&quot;defaultButton icon refresh&quot;
									  onclick='reloadGrid(&quot;${gridId}&quot;)'><spring:message code='button.refresh' />&lt;/a>"
	/>
</Grid>