<?xml version="1.0" encoding="UTF-8"?>
<%@ page contentType="text/xml;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : sqlQueryCustom.jsp
  Description : 쿼리 결과 그리드 레이아웃
  Modification Information
  
  수정일			수정자			수정내용
  ------			------			--------
  2017-09-01		강승상			최초생성
  
  author	: 강승상
  since		: 2017-09-01
--%>

<c:set var="gridId" value="sqlQueryGrid" />
<Grid>
	<Cfg	id="${gridId}"
			AcceptEnters   = "1" Calculated = "1"  CalculateSelected = "1"      DateStrings = "2" Deleting  = "1"
			Dragging       = "0" Editing    = "1"  EnterMode         = "4"      Filtering   = "1" IdChars   = "0123456789"
			InEditMode     = "2" MaxPages   = "20" NoFormatEscape    = "1"      NoHScroll   = "0" NoVScroll = "0"
			NumberId       = "1" SafeCSS    = '1'  Selecting		 = "0"
			SelectingCells = "0" SortIcons  = "0"  Style             = "Office" SuppressCfg = "1"
			Paging		   = "2" AllPages	= "0"  PageLength		 = "20"
	/>

	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>

	<!-- Grid Header Setting Area Start  -->
	<Header	id="Header"	Align="center"
<%--			HEADER			= "HEADER_NAME"	--%>
	<c:forEach var="resultHeader" items="${resultMapList.get(0).keySet()}">
				${resultHeader} = "${resultHeader}"
	</c:forEach>
	/>
	<!-- Grid Header Setting Area End    -->

	<!-- Fileter Setting Area Start      -->
	<Head>
		<Filter	id = "Filter" CanFocus = "1"/>
	</Head>
	<!-- Fileter Setting Area End        -->

	<Cols>
		<%--
		<C Name="colName"		Type="Text"	 RelWidth="number"  Align="left|right|center" CanEdit="0|1" Visible="0|1" />
		<C Name="colName"		Type="Enum"   RelWidth="number" Align="left|right|center" Enum="사용|미사용" EnumKeys="N|Y" />
		--%>
		<c:choose>
			<c:when test="${empty resultMapList.get(0).get('ERROR')}">	<%-- S: print row --%>
				<c:forEach var="resultHeader" items="${resultMapList.get(0).keySet()}">			<%-- S: List Loop (row) --%>
					<C Name="${resultHeader}" Type="Text"	 RelWidth="100"	MinWidth="100" Align="center" CanEdit="0" Visible="1"/>
				</c:forEach>											<%-- E: List Loop --%>
			</c:when>													<%-- E: print row --%>
			<c:otherwise>												<%-- S: print error --%>
				<C Name="ERROR" Type="Text"	 RelWidth="100"  Align="center" CanEdit="1" Visible="1" />
			</c:otherwise>												<%-- S: print error --%>
		</c:choose>
	</Cols>

	<!-- Grid Paging Setting Area Start  -->
	<Pager Visible = "0"/>

	<Solid>
		<I id = "PAGER" Cells = "NAV,LIST,ONE,GROUP" Space = "4"
		   NAVType	     = "Pager"
		   LISTType	     = "Pages" LISTAlign   = "left" LISTLeft   = "10"                LISTRelWidth  = "1"
		   ONEType       = "Bool"  ONECanEdit  = "1"    ONEFormula = "Grid.AllPages?0:1" ONELabelRight = "페이지단위로보임"
		   ONEOnChange = "Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
		   GROUPCanFocus = "0"
		/>
	</Solid>
	<!-- Grid Paging Setting Area End    -->

	<Toolbar	Space="0"	Styles="1"	Cells="Empty"
				EmptyType = "Html"	EmptyRelWidth = "1"
	/>

	<Body>
		<B>
		<c:choose>
			<c:when test="${empty resultMapList.get(0).get('ERROR')}">	<%-- S: print row --%>
				<c:forEach var="row" items="${resultMapList}">			<%-- S: List Loop (row) --%>
					<I
					<c:forEach var="col" items="${row.keySet()}">		<%-- S: Map key (col) Loop --%>
						${col}="${row.get(col)}"
					</c:forEach>										<%-- E: Map key (col) Loop --%>
					/>
				</c:forEach>											<%-- E: List Loop --%>
			</c:when>													<%-- E: print row --%>
			<c:otherwise>												<%-- S: print error --%>
				<I ERROR="${resultMapList.get(0).get('ERROR')}" />
			</c:otherwise>												<%-- S: print error --%>
		</c:choose>
		</B>
	</Body>
</Grid>
