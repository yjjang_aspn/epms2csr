<?xml version="1.0" encoding="UTF-8"?>
<%@ page contentType="text/xml;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : userCardInfoLayout.jsp
  Description : 법인카드 관리 페이지 레이아웃
  Modification Information
  
  수정일			수정자			수정내용
  ------			------			--------
  2017-08-28		강승상			최초생성
  
  author	: 강승상
  since		: 2017-08-28
--%>
<c:set var="gridId" value="userCardInfoGrid" />
<Grid>
	<Cfg	id="${gridId}"
			AcceptEnters   = "1" Calculated = "1"  CalculateSelected = "1"      DateStrings = "2"	Deleting = "0"
			Dragging       = "0" Editing    = "1"  EnterMode         = "4"      Filtering   = "1" IdChars   = "0123456789"
			InEditMode     = "2" MaxPages   = "20" NoFormatEscape    = "1"      NoHScroll   = "0" NoVScroll = "0"
			NumberId       = "1" SafeCSS    = '1'  Selecting		 = "1"
			SelectingCells = "0" SortIcons  = "0"  Style             = "Office" SuppressCfg = "1"
			Paging		   = "2" AllPages	= "0"  PageLength		 = "20"		Panel = "0"
			CopySelected   = "0" CopyFocused= "1"  CopyCols			 = "0"		ExportFormat="xls" ExportCols="0"
	/>

	<%-- Panel hide --%>
	<Panel Visible="0" />

	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>

	<!-- Grid Header Setting Area Start  -->
	<Header	id="Header"	Align="center"
<%--			HEADER			= "HEADER_NAME"	--%>
				OWNER_REG_NO	= "기업사업자번호"
				CARD_NUM		= "카드번호"
				BANK_CODE		= "카드사코드"
				BANK_NAME		= "카드사명"
				CARD_KIND		= "카드분류"
			    CARD_NAME		= "발급자명"
			    AVAIL_TERM		= "유효기간"
			    USE_STATUS		= "카드상태"
			    FIRST_DATE		= "발급일자"
			    CANCEL_DATE		= "탈퇴일자"
			    SETT_DATE		= "결제일"
			    SETT_BANKCODE	= "결제은행코드"
			    SETT_ACCO		= "결제계좌"
			    USE_ONELMT		= "국내총한도금액"
			    ONE_LIMIT		= "국내잔여한도금액"
			    FORE_LMT		= "해외총한도금액"
			    FORE_USELMT		= "해외잔여한도금액"
			    STAND_DATE		= "카드한도갱신기준일자"
			    STAND_TIME		= "카드한도갱신기준시간"
			    BEFCARDNO		= "재발급 이전 카드번호"
			    INDATETIME		= "생성일시"
			    TYPE			= "카드사용구분"
			    USER_ID			= "실소유자ID"
			    USER_NM			= "실소유자"
			    ISSUE_STAT		= "불출가능"
			    DETAIL			= "상세"
			    DETAIL_NM		= "상세"
	/>
	<!-- Grid Header Setting Area End    -->

	<!-- Fileter Setting Area Start      -->
	<Head>
		<Filter	id = "Filter" CanFocus = "1"/>
	</Head>
	<!-- Fileter Setting Area End        -->

	<LeftCols>
		<C Name="CARD_NUM"			Type="Text"	 Width="150"  Align="center" CanEdit="0" Visible="1" />
	</LeftCols>

	<Cols>
		<C Name="OWNER_REG_NO"		Type="Text"	 Width="120"  Align="center" CanEdit="0" Visible="1" />
		<C Name="BANK_CODE"			Type="Text"	 Width="80"   Align="center" CanEdit="0" Visible="1" />
		<C Name="BANK_NAME"			Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="CARD_KIND"			Type="Enum"	 Width="100"  Align="center" <tag:enum codeGrp="CARD_KIND" /> CanEdit="0" Visible="1" /> />
		<C Name="CARD_NAME"			Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="AVAIL_TERM"		Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="USE_STATUS"		Type="Enum"	 Width="100"  Align="center" Enum="|가능|불가" EnumKeys="|가능|불가" CanEdit="0"/>
		<C Name="FIRST_DATE"		Type="Date"	 Width="100"  Align="center" CanEdit="0" Visible="1" Format="yyyy-MM-dd" />
		<C Name="CANCEL_DATE"		Type="Date"	 Width="100"  Align="center" CanEdit="0" Visible="1" Format="yyyy-MM-dd" />
		<C Name="SETT_DATE"			Type="Text"	 Width="80"   Align="center" CanEdit="0" Visible="1" />
		<C Name="SETT_BANKCODE"		Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="SETT_ACCO"			Type="Text"	 Width="120"  Align="center" CanEdit="0" Visible="1" />
		<C Name="USE_ONELMT"		Type="Int"	 Width="120"  Align="right"  CanEdit="0" Visible="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";'/>
		<C Name="ONE_LIMIT"			Type="Int"	 Width="120"  Align="right"  CanEdit="0" Visible="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";'/>
		<C Name="FORE_LMT"			Type="Int"	 Width="120"  Align="right"  CanEdit="0" Visible="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";'/>
		<C Name="FORE_USELMT"		Type="Int"	 Width="120"  Align="right"  CanEdit="0" Visible="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";'/>
		<C Name="STAND_DATE"		Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="STAND_TIME"		Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="INDATETIME"		Type="Text"	 Width="100"  Align="center" CanEdit="0" Visible="0" />
	</Cols>

	<RightCols>
		<C Name="TYPE"				Type="Enum"	 Width="120" EmptyValue="선택" <tag:enum codeGrp="USERCARD_TYPE" /> CanEdit="1" Visible="1" />
		<C Name="USER_ID"			Type="Text"	 Width="80"  Align="center" CanEdit="0" Visible="0" />
		<C Name="USER_NM"			Type="Text"	 Width="80"  Align="center" CanEdit="0" Visible="1" />
		<C Name="ISSUE_STAT"		Type="Enum"	 Width="100" EmptyValue="선택" Align="center" <tag:enum codeGrp="ISSUE_STAT" /> CanEdit="1" Visible="0" />
		<C Name="DETAIL"			Type="Text"	 Width="200" Align="left" CanEdit="0" Visible="0" />
		<C Name="DETAIL_NM"			Type="Text"	 Width="200" Align="left" CanEdit="0" Visible="1" />
	</RightCols>

	<!-- Grid Paging Setting Area Start  -->
	<Pager Visible = "0"/>

	<Solid>
		<I id = "PAGER" Cells = "NAV,LIST,ONE,GROUP" Space = "4"
		   NAVType	     = "Pager"
		   LISTType	     = "Pages" LISTAlign   = "left" LISTLeft   = "10"                LISTRelWidth  = "1"
		   ONEType       = "Bool"  ONECanEdit  = "1"    ONEFormula = "Grid.AllPages?0:1" ONELabelRight = "페이지단위로보임"
		   ONEOnChange = "Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
		   GROUPCanFocus = "0"
		/>
	</Solid>
	<!-- Grid Paging Setting Area End    -->

	<Toolbar	Space="0"	Styles="1"	Cells="Empty,Cnt,Del,Found,항목선택,저장,새로고침,인쇄,엑셀"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType = "Button"
				CntType = "Html" CntFormula = '"Total : &lt;b>"+count(7)+"&lt;/b>"' CntWidth = '-1' CntWrap = '0'
				FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>'" FoundWidth = '-1' FoundWrap = '0'
				DelType = "Html" DelFormula = 'var cnt=count("Row.Deleted>0",7);return cnt?"삭제된 행 :&lt;b>"+cnt+"&lt;/b>":""' DelWidth = '-1' DelWrap = '0'
				항목선택Type="Html" 항목선택="&lt;a href='#none' title='항목선택' class=&quot;treeButton treeSelect&quot; onclick='showColumns(&quot;${gridId}&quot;,&quot;xls&quot;)'>항목선택&lt;/a>"

				저장Type = "Html" 저장 = "&lt;a href='#none' title='저장' class=&quot;defaultButton01 icon submit&quot;
								onclick='saveUserCardInfo(&quot;${gridId}&quot;)'>저장&lt;/a>"
				새로고침Type = "Html" 새로고침 = "&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
									  onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type = "Html" 인쇄 = "&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
									  onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
				엑셀Type = "Html" 엑셀 = "&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
									  onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>

</Grid>
