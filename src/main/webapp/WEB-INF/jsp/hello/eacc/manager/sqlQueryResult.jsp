<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Class Name : 
  Description : 
  Modification Information
  
  수정일			수정자			수정내용
  ------			------			--------
  2017-08-31		강승상			최초생성
  
  author	: 강승상
  since		: 2017-08-31
--%>
<html>
<head>
	<%@ include file="/com/comHeader.jsp" %>
	<title>Query Result</title>
</head>
<div class="scl"  style="height:94%; width:100%; overflow-x:auto;">
	<%--<!-- S: body  -->
	<div class="fl-box panel-wrap01" style="width: 100%;">
		<!-- S: title  -->
		<h5 class="panel-tit">조회 결과</h5>
		<!-- E: title    -->

		<!-- S: grid   -->
		<div class="panel-body">
			<div id="GRID_IDGrid">
				<bdo	Debug="Error"
						&lt;%&ndash;Data_Url="/hello/eacc/manager/sqlQueryData.do" Data_Format="String"&ndash;%&gt;
						Layout_Url="/hello/eacc/manager/sqlQueryCustom.do?query=${query}"
				>
				</bdo>
			</div>
		</div>
		<!-- E: grid     -->
	</div>
	<!-- E: body    -->--%>
	<c:if test="${resultMapList != null or resultMapList.size() > 0}">
		<table id="queryResult" width="100%" border="1"  class="tb-st type05">
			<thead class="">
				<tr>
					<c:forEach var="resultHeader" items="${resultMapList.get(0).keySet()}">
						<th>
								${resultHeader}
						</th>
					</c:forEach>
				</tr>
			</thead>
			<tbody>
			<c:choose>
				<c:when test="${empty resultMapList.get(0).get('ERROR')}">	<%-- S: print row --%>
					<c:forEach var="row" items="${resultMapList}">			<%-- S: List Loop (row) --%>
						<tr>
							<c:forEach var="col" items="${row.keySet()}">		<%-- S: Map key (col) Loop --%>
								<td>
									<c:out value="${row.get(col)}" />
								</td>
							</c:forEach>										<%-- E: Map key (col) Loop --%>
						</tr>
					</c:forEach>											<%-- E: List Loop --%>
				</c:when>													<%-- E: print row --%>
				<c:otherwise>												<%-- S: print error --%>
					<tr>
						<td>
							<c:out value="${resultMapList.get(0).get('ERROR')}" />
						</td>
					</tr>
				</c:otherwise>												<%-- S: print error --%>
			</c:choose>
			</tbody>
		</table>

	</c:if>
</div>
</body>
</html>
