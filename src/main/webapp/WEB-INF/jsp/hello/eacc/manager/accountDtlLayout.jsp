<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%--
  Class Name : accountDtlLayout.jsp
  Description : 계정 관리 상세 그리드 레이아웃 화면
  Modification Information
 
       수정일    	     수정자                   수정내용
   -------    --------    ---------------------------
   2017.08.14  정호윤	              최초 생성

    author   : 정호윤
    since    : 2017.08.14
--%>
<c:set var="gridId" value="AccountDtlList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			AcceptEnters   = "1" Calculated = "1"  CalculateSelected = "1"      DateStrings = "2" Deleting  = "1"
			Dragging       = "0" Editing    = "1"  EnterMode         = "4"      Filtering   = "1" IdChars   = "0123456789"
			InEditMode     = "2" MaxPages   = "20" NoFormatEscape    = "1"      NoHScroll   = "0" NoVScroll = "0"
			NumberId       = "1" SafeCSS    = '1'  Selecting		 = "0"
			SelectingCells = "0" SortIcons  = "0"  Style             = "Office" SuppressCfg = "1" 
			Paging		   = "2" AllPages	= "0"  PageLength		 = "20"		MaxPages	= "20"
			CopySelected   = "0" CopyFocused= "1"  CopyCols			 = "0"		ExportFormat="xls" ExportCols="0"
	/>
	
	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>
	
	<!-- Grid Header Setting Area Start  -->
	<Header	id="Header"	Align="center"
			ACCOUNT_NDX 	= "<spring:message code='account.accountNdx' />"
			GLCODE			= "<spring:message code='account.glCode' />"
			GLNAME			= "<spring:message code='account.glName' />"
			TAX_CD	 		= "<spring:message code='account.taxCd' />"
			TAX_CMD 		= "<spring:message code='account.taxCmd' />"
			TAX_CMT 		= "<spring:message code='account.taxCmt' />"
     />
	<!-- Grid Header Setting Area End    -->
	
	<!-- Fileter Setting Area Start      -->
	<Head>
		<Filter	id = "Filter" CanFocus = "1"/>
	</Head>
	<!-- Fileter Setting Area End        -->
	
	<Cols>
	    <C Name="ACCOUNT_NDX"		Type="Int"   RelWidth="100" Align="center" CanEdit="0" Visible="0" />
	    <C Name="GLCODE"			Type="Text"  RelWidth="100" /> 
	    <C Name="GLNAME"			Type="Text"  RelWidth="100" /> 
	    <C Name="TAX_CD"			Type="Enum"	 RelWidth='100' Enum="" 		 EnumKeys="" />
	    <C Name="TAX_CMD"			Type="Enum"	 RelWidth="170" Enum="|<spring:message code='button.possible' />|<spring:message code='button.impossible' />" EnumKeys="|Y|N" />
	    <C Name="TAX_CMT"			Type="Text"	 RelWidth="200" />
	</Cols>
		
	<!-- Grid Paging Setting Area Start  -->
	<Pager Visible = "0"/>

	<Solid>
		<I id = "PAGER" Cells = "NAV,LIST,ONE,GROUP" Space = "4"
		   NAVType	     = "Pager"
		   LISTType	     = "Pages" LISTAlign   = "left" LISTLeft   = "10"                LISTRelWidth  = "1"
		   ONEType       = "Bool"  ONECanEdit  = "1"    ONEFormula = "Grid.AllPages?0:1" ONELabelRight = "<spring:message code='button.showUnitPage' />"
		   ONEOnChange = "Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
		   GROUPCanFocus = "0"
		/>
	</Solid>
	<!-- Grid Paging Setting Area End    -->
	
	<Toolbar	Space="0"	Styles="1"	Cells="Empty,Cnt,<spring:message code='button.add' />,<spring:message code='button.save' />,<spring:message code='button.refresh' />,<spring:message code='button.print' />,<spring:message code='button.excel' />"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType = "Button"
				CntType = "Html" CntFormula = '"Total : &lt;b>"+count(7)+"&lt;/b>"' CntWidth = '-1' CntWrap = '0'
				FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>'" FoundWidth = '-1' FoundWrap = '0'
				DelType = "Html" DelFormula = 'var cnt=count("Row.Deleted>0",7);return cnt?" Del :&lt;b>"+cnt+"&lt;/b>":""' DelWidth = '-1' DelWrap = '0'
				<spring:message code='button.add' />Type = "Html" <spring:message code='button.add' /> = "&lt;a href='#none' title='<spring:message code='button.add' />' class=&quot;defaultButton01 icon add&quot;
							       onclick='addRowGrid(&quot;${gridId}&quot;)'><spring:message code='button.add' />&lt;/a>"
				<spring:message code='button.save' />Type = "Html" <spring:message code='button.save' /> = "&lt;a href='#none' title='<spring:message code='button.save' />' class=&quot;defaultButton01 icon submit&quot;
								onclick='saveGrid(&quot;${gridId}&quot;)'><spring:message code='button.save' />&lt;/a>" 
				<spring:message code='button.refresh' />Type = "Html" <spring:message code='button.refresh' /> = "&lt;a href='#none' title='<spring:message code='button.refresh' />' class=&quot;defaultButton icon refresh&quot;
									  onclick='reloadGrid(&quot;${gridId}&quot;)'><spring:message code='button.refresh' />&lt;/a>"
 				<spring:message code='button.print' />Type = "Html" <spring:message code='button.print' /> = "&lt;a href='#none' title='<spring:message code='button.print' />' class=&quot;defaultButton icon printer&quot;
									  onclick='printGrid(&quot;${gridId}&quot;)'><spring:message code='button.print' />&lt;/a>"
 				<spring:message code='button.excel' />Type = "Html" <spring:message code='button.excel' /> = "&lt;a href='#none' title='<spring:message code='button.excel' />'class=&quot;defaultButton icon excel&quot;
									  onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'><spring:message code='button.excel' />&lt;/a>"
	/>

</Grid>

