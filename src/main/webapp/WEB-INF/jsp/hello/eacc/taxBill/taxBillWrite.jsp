<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : taxBillWrite.jsp
  Description : 세금계산서전표 작성화면
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.08.22    윤선철              최초 생성

    author   : 윤선철
    since    : 2017.08.22
--%>
<script type="text/javascript">

var fileVal = "";
var attachGrpNo = "";
var layerGubun = "addPerson"; // 레이어 팝업 구분자(추가직원-addPerson, 이관-transPerson)
$(document).ready(function(){
	
	//모달팝업 캘린더
	$('.moOpenCal').glDatePicker({
		showAlways:false,
		cssName:'flatwhite',
		allowMonthSelect:true,
		allowYearSelect:true,
		onClick:function(target, cell, date, data){
			target.val(date.getFullYear() + '-' + getDate(parseInt(date.getMonth())+1) + '-' + getDate(date.getDate()));
		}
	});
	
	//첨부파일
	var maxFileCnt = 10;
	for(var i=0; i<1; i++){
		$('#attachFile'+i).append('<input type="file" multiple id="ex_file'+i+'" class="ksUpload" name="fileList">');	
		var fileType = 'ppt |pps |pptx |ppsx |pdf |hwp |xls |xlsx |xlsm | xlt |doc |dot |docx |docm |txt |gif |jpg |jpeg |png';
		
		$('#ex_file'+i).MultiFile({
				accept : fileType
			,	list   : '#detailFileList'+i
			,	max    : maxFileCnt
			,	STRING : {
					remove: '<img src="/images/com/web/btn_remove.png" />'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
//						text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" >',
					idx	  : i,
					denied:'$ext는(은) 업로드할 수 없는 파일 확장자입니다.'
				}
		});
		
		$('.MultiFile-wrap').attr("id", "jquery"+i);
	}
	
	//첨부파일 삭제
	$('.icn_fileDel').on('click', function(){
		delModiFile($(this));
	});
	
	// 초기화
	fn_setTable($('#ACCGUBUN').val());
	
	//공급가액 입력시 세액 계산하여 세팅
	$(document).on("keyup", "input[name=arrItemAmt]", (function(e) {
		// 세금코드가 부가세무관일때 세액이 없음
		var taxCd = $('#TAX_CD').val();
		if(taxCd == "V0"){
			tmpTax = "";
		}else{
			var amount = $(this).val().replace(/,/g, "") * 1;
			var tmpTax = "";
			if(amount != ""){
				tmpTax = jisuanTax(amount);  
			}else{
				tmpTax = "";
			}
		}
		$(this).parent().parent().next().find("input[name=arrItemVat]").val(tmpTax);
		
		fn_addAmt();
	}));
	
	$(document).on("keyup", "input[name=arrItemVat]", (function(e) {
		fn_addAmt();
	}));
	
	// 계정구분이 변경될 경우, 세금코드가 변경됨
	$('#ACCGUBUN').on('change', function(){
		fn_setTable($(this).val());
		fn_init();
		// 세금코드 첫번째 값으로 초기화
		$('#TAX_CD').val($('#TAX_CD option:eq(0)').val());
	});

	// 세금코드 변경에 따라 세액항목 hide/show
	$('#TAX_CD').on('change', function(){
		fn_init();
		fn_setVatTd($(this).val());
		fn_addAmt();
	});
	
});

/* 구매처 변경 팝업 호출 */
function fn_popVendor(billGb){
	
	layer_open('vendorModal');
	Grids.VendorList.Source.Data.Url = "/hello/eacc/common/popVendorListData.do?STCD2=${taxHeader.SUPNO}&billGb="+billGb;
	Grids.VendorList.ReloadBody();
}

/* 첨부파일 항목에서 제거 처리 */
function delModiFile(obj){
	var attach = obj.data("attach");
	var idx = obj.data('idx');
	
	$('#fileLstWrap_'+attach).remove();
	
	//fileNo set
	if(fileVal != '') {
		fileVal += ",";
	}
	
	fileVal += attach;
	$('#delSeq').val(fileVal);
}

/* 세금코드에 따라 세액 항목 show/hide 처리 */
function fn_setVatTd(tax_cd){
	// 과세,불공제의 경우 세액항목 show
	if(tax_cd == 'VA' || tax_cd == 'VB'){
		$(".th_vat").show();
		$(".td_vat").show();
		
	// 특정세금코드의 경우 세액항목 hide
	}else{
		$(".th_vat").hide();
		$(".td_vat").hide();
	}
}

/* 계정구분값에 따라 th, td show/hide */
function fn_setTable(accgubun){
	if(accgubun == '3'){	// 선급비용
		$(".th_accstartdt").show();
		$(".th_accenddt").show();
		$(".td_accstartdt").show();
		$(".td_accenddt").show();
		
		$(".th_vat").show();
		$(".td_vat").show();
		
		$(".th_aufnr").hide();
		$(".td_aufnr").hide();
		
		$(".th_costcenter").show();
		$(".td_costcenter").show();
		
	}else{	//경비
		$(".th_accstartdt").hide();
		$(".th_accenddt").hide();
		$(".td_accstartdt").hide();
		$(".td_accenddt").hide();
		
		$(".th_vat").show();
		$(".td_vat").show();
		
		$(".th_costcenter").show();
		$(".td_costcenter").show();
		
		$(".th_aufnr").hide();
		$(".td_aufnr").hide();
		
		// 선급비용시작일 초기화
		$('[name=arrAccStartDt]').each(function(){
			$(this).val('');
		});
		// 선급비용종료일 초기화
		$('[name=arrAccEndDt]').each(function(){
			$(this).val('');
		});
	}
}

/* 계정구분, 세금코드 변경시 라인아이템 초기화 */
function fn_init(){
	$('#tb_items > tbody > tr:gt(1)').remove();
	itemSeq = 1;
	fn_addAmt();
	$('.glCode').val('');
	$('.glName').val('');
	$('.summary').val('');
	$('.summary').attr('placeholder', '');
	$('.itemAmt').val('');
	$('.itemVat').val('');
	$('.aufnr').val('');
	$('.autxt').val('');
	$('.accStartDt').val('');
	$('.accEndDt').val('');
}

/* 세금계산서전표 저장 */
function fn_saveTaxBill(){
	
	// validation 체크
	if(!fn_Validation()){
		return;
	}
	
	// 적요가 1개일 때, 적요에 들어가는 ',' 처리
	if($("input[name=arrSummary]").length == 1) {
		$("input[name=arrSummary]").val($("input[name=arrSummary]").val().replace(/,/g, "‚"));    
	}
	
	if(confirm("저장 하시겠습니까?")){
		
		var fileCnt = $("input[name=fileGb]").length;
		if(fileCnt > 0){
			$('#taxWriteForm #attachExistYn').val("Y");
		}else{
			$('#taxWriteForm #attachExistYn').val("N");
		}
		
		$('#taxWriteForm').attr('action','/hello/eacc/taxBill/taxBillSaveAjax.do').ajaxSubmit(function(json){
			if(json.resultCd == "T"){
				if(confirm(json.resultMsg + " 전표를 생성하시겠습니까?")){
					fn_parkingTaxBill(json.taxBillNdx);
				} else{
					toggleModal($("#taxBillWriteModal"));
					fn_reloadGrid();
				}
			} else {
				alert(json.resultMsg);
			}
		});
	}
}

/* 세금계산서 제외 */
function fn_taxMasterExcept(){
	if(confirm("제외 하시겠습니까?")){
		$('#DEL_YN').val("Y");
		$('#taxWriteForm').attr('action','/hello/eacc/taxBill/taxMasterExceptAjax.do').ajaxSubmit(function(json){
			if(json.resultCd == "T"){
				alert(json.resultMsg);
				toggleModal($("#taxBillWriteModal"));
				fn_reloadGrid();
			} else {
				alert(json.resultMsg);
				toggleModal($("#taxBillWriteModal"));
				fn_reloadGrid();
			}
		});
	}
	
}

/* validation Check */
function fn_Validation(){
	
	if(!isNull_J($('#BOOK_DT'), '전기일자을 입력해주세요.')) {
		return false;
	}
	
	if(!isNull_J($('#EVIDENCE_DT'), '증빙일자을 입력해주세요.')) {
		return false;
	}
	
	if(!isNull_J($('#WORKPLACE_CD'), '부가세사업장을 선택해주세요.')) {
		return false;
	}
  
  	if(!isNull_J($('#ACCGUBUN'), '계정구분을 선택해주세요.')) {
  		return false;
    }
   
  	if(!isNull_J($('#TAX_CD'), '세금코드를 선택해주세요.')) {
  		return false;
    }
  	
  	if(!isNull_J($('#lifnrNm'), '구매처를 선택해주세요.')) {
  		return false;
    }
  	
 	var isValid = true;
 	
	$('[name=arrGlCode]').each(function(){
		if(!isNull_J($(this), 'G/L 계정을 선택해주세요.')) {
			isValid = false;
			return false;
		}
	});

	if(isValid){
		$('[name=arrSummary]').each(function(){
			if(!isNull_J($(this), '적요를 입력해주세요.')) {
				isValid = false;
				return false;
			}
		});
	}
	
	if(!isValid){
		return false;
	}
	
	// 공급가액
	var arrItemAmt = 0;
	$('[name=arrItemAmt]').each(function(){
		var itemAmt = $(this).val().replace(/,/g, "") * 1;
		arrItemAmt += itemAmt;
	});
	if(isValid){
		if(arrItemAmt == 0){
			alert("공급가액을 입력해주시기 바랍니다.");
			isValid = false;
			return false;
		}
	}
	
	// 총 금액 = Line Item의 공급가액 + 입력된 부가가치세 금액 
	var totamount = '${taxHeader.ZTTAT }' * 1;
	var arrItemVat = 0;						// 부가세액
	var sum = 0;
	
	$("[name=arrItemVat]").each(function(){
		var itemVat = $(this).val().replace(/,/g, "") * 1;
		arrItemVat += itemVat;
	});
	
	sum = arrItemAmt + arrItemVat;
	if( totamount != sum){
		alert("총 금액과 입력된공급가액, 부가세액의 합이 일치하지 않습니다.");
		isValid = false
	}
	if(!isValid){
		return false;
	}
	
	if(isValid){
		// 계정구분이 경비(1)인 경우
		if($('#ACCGUBUN').val() == '1'){
			$('[name=arrAufnr]').each(function(index){
				// 오더번호가 존재하지 않으면 코스트센터가 반드시 존재해야 함
				if($(this).val() == ''){
					if(!isNull_J($('#kostl_'+index), '오더번호를 입력하지 않는 경우, 코스트센터는 필수입력입니다.')) {
						$('#kostl_'+index).focus();
						isValid = false;
						return false;
					}
				}
			});
		}else{	// 계정구분이 선급비용(3)인 경우
			// 선급비용시작일
			$('[name=arrAccStartDt]').each(function(){
				if(!isNull_J($(this), '선급비용시작일을 입력해주세요.')) {
					isValid = false;
					return false;
				}
			});
		
			// 선급비용종료일
			if(isValid){
				$('[name=arrAccEndDt]').each(function(){
					if(!isNull_J($(this), '선급비용종료일을 입력해주세요.')) {
						isValid = false;
						return false;
					}
				});
			}
		}
	}
	
	// 공급가액 콤마 제거
	$('[name=arrItemAmt]').each(function(){
		var itemAmt = $(this).val().replace(/,/g, "") * 1;
		$(this).val(itemAmt);
	});
	
	// 세액 콤마 제거
	$('[name=arrItemVat]').each(function(){
		var itemVat = $(this).val().replace(/,/g, "") * 1;
		$(this).val(itemVat);
	});
	
	if(!isValid){
		return false;
	}
	
	return isValid;
}

//세액 자동 계산
function jisuanTax(obj){
	var taxAmount = parseInt(obj)*0.1;
	taxAmount = Math.round(taxAmount);
	
	return number_format(taxAmount+''); 
}

/* 임시전표생성 */
function fn_parkingTaxBill(taxBillNdx){
	jQuery.ajax({
		type : 'POST',
		url : '/hello/eacc/taxBill/taxBillPakingAjax.do',
		cache : false,
		dataType : 'json',
		data : { TAXBILL_NDX : taxBillNdx },
		async : false,
		error : function() {
			alert("처리중 오류가 발생 하였습니다.");
		},
		success : function(json) {
			if(json.resultCd == "T"){
				alert(json.resultMsg);
				toggleModal($("#taxBillWriteModal"));
				fn_reloadGrid();
			}else{
				if(typeof json.errorMsg == 'undefined' || json.errorMsg == 'undefined'){
					alert(json.resultMsg);
				}else{
					alert(json.resultMsg + "\n" + json.errorMsg);
				}
			}
		}
	});
}

var itemSeq = 1;
/* 아이템 추가 */
function addItem(){
// 	var idx =($('#tb_items > tbody > tr').length)-2;
// 	var html = "<tr id='dynamicRow_"+(idx+1)+"'>"+$("#tb_items > tbody > tr:last").html()+"</tr>";
	var html = "<tr id='dynamicRow'>"+$("#tb_items > tbody > tr:last").html()+"</tr>"
	
	$("#tb_items > tbody > tr:last").after(html);
	$(".glCode").eq(itemSeq).val("");
	$(".glName").eq(itemSeq).val("");
	$(".summary").eq(itemSeq).val("");
	$(".summary").eq(itemSeq).attr("placeholder", "");
	$(".itemAmt").eq(itemSeq).val("");
	$(".itemVat").eq(itemSeq).val("");
	$(".aufnr").eq(itemSeq).val("");
	$(".autxt").eq(itemSeq).val("");
	$(".accStartDt").eq(itemSeq).val("");
	$(".accEndDt").eq(itemSeq).val("");
	itemSeq++;
}

/* 아이템 삭제 */
function deleteItem(obj){
	if(itemSeq == '1'){
		alert("첫번째 행은 삭제 할 수 없습니다.");
		return;
	}
	
	$(obj).parent().parent().parent().remove();
	
	itemSeq = itemSeq - 1;
	
	fn_addAmt();
}

/* 총 공급가액 */
function fn_addAmt(){

	var arrItemTot = 0;
	var arrItemAmt = 0;
	var arrItemVat = 0;
	$('input[name=arrItemAmt]').each(function(){
		var itemAmt = $(this).val().replace(/,/g, "") * 1;
		arrItemAmt += itemAmt;
	});
	$('input[name=arrItemVat]').each(function(){
		var itemVat = $(this).val().replace(/,/g, "") * 1;
		arrItemVat += itemVat;
	});
	arrItemTot = arrItemAmt + arrItemVat;
	
	$('#TOT_AMOUNT').val(oneLimitCount(arrItemTot));		// 총 금액
	$('#AMT_AMOUNT').val(oneLimitCount(arrItemAmt)); 		// 총 공급가액
	$('#VAT_AMOUNT').val(oneLimitCount(arrItemVat)); 		// 총 부가세액
	
	// 미입력금액 계산
	$('#minusAmt').val(oneLimitCount("${taxHeader.ZTTAT}"*1 - arrItemAmt*1 - arrItemVat*1));
}


</script>
	<div class="modal-dialog root wd-per-80"> <!-- 원하는 width 값 입력 -->
        <div class="modal-content">
			<div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
               	<h4 class="modal-title">세금계산서전표 상세</h4>
               	
               	<div style="position:absolute; top:13px; right:40px;">
					<a href="#" class="btn comm st01" style="margin-bottom: 5px;" onclick="fn_saveTaxBill(); return false;" >저장</a>
					<a href="#" class="btn comm st02" style="margin-bottom: 5px; margin-right: 5px;" onclick="fn_taxMasterExcept(); return false;" >제외</a>
				</div>
            </div>
            <div class="modal-body" >
				<div class="modal-bodyIn" >
					<!-- s:input -->
					<input type="hidden" title="오늘날짜" id="toDate" name="toDate" value="${toDate}"/>
					<input type="hidden" title="경비작성마감일" id="submitDeadLine" name="submitDeadLine" value="${submitDeadLine}"/> <!-- 4일 -->
					<input type="hidden" title="경비결재마감일" id="apprDeadLine" name="apprDeadLine" value="${apprDeadLine}"/> <!-- 6일 -->
					<!-- e:input -->

					<form id="taxWriteForm" action="" method="post" enctype="multipart/form-data">
						
						<!-- s:input -->
						<input type="hidden" name="taxBillNdx" value="" />
						<!-- 첨부파일 정보 -->
						<input type="hidden" title="파일등록여부" id="attachExistYn" name="attachExistYn" value=""/>
						<input type="hidden" title="삭제Seq" id="delSeq" name="delSeq" value=""/>
						<input type="hidden" title="파일 건수" id="fileCnt" name="fileCnt" value=""/>
						<input type="hidden" title="경비 모듈" id="MODULE" name="MODULE" value="8">
						<input type="hidden" title="제외여부" id="DEL_YN" name="DEL_YN" value="">
						<!-- e: input -->											
						
<!-- 						<div class="popPayBtn" style="float:right; margin-bottom:5px; margin-top:5px;"> -->
<!-- 							<a href="#" class="btn comm st02" style="margin-bottom: 5px;" onclick="fn_saveTaxBill(); return false;" >저장</a> -->
<!-- 							<a href="#" class="btn comm st04" style="margin-bottom: 5px; margin-right: 5px;" onclick="fn_taxMasterExcept(); return false;" >제외</a> -->
<!-- 						</div> -->
						
						<div style="clear:both">
							<!-- 세금계산서 테이블 -->
							<%@ include file="/WEB-INF/jsp/hello/eacc/common/taxReceipt.jsp"%>
							<!-- 세금계산서 테이블 -->
						</div>
						
						<!-- s:tb-wrap -->
						<div class="tb-wrap mgn-t-15">
							<table class="tb-st">
								<caption class="screen-out">세금계산서 정보</caption>
								<colgroup>
									<col width="13%">
									<col width="20%">
									<col width="13%">
									<col width="20%">
									<col width="13%">
									<col width="20%">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">전표 구분</th>
										<td>
											<c:choose>
												<c:when test="${taxHeader.ZDTIG eq '02' || taxHeader.ZDTIG eq 'B02'}">
													<div>전자계산서</div>
												</c:when>
												<c:otherwise>
													<div>전자세금계산서</div> 
												</c:otherwise>
											</c:choose>
										</td>
										<!-- s:전기일 -->	
										<th scope="row">전기일자</th>
										<td>
											<div>
												<input type="text" class="inp-comm datePic moOpenCal" title="전기일" id="BOOK_DT" name="BOOK_DT" value="${taxHeader.ZMKDT}" readonly="readonly">
											</div>
										</td>
										<!-- e:전기일 -->
										
										<!-- s:증빙일 -->
										<th scope="row">증빙일자</th>
										<td>
											<div>
												<input type="hidden" class="infoTextInput" title="증빙일" id="EVIDENCE_DT" name="EVIDENCE_DT" value="${taxHeader.ZMKDT}" readonly="readonly">
												${taxHeader.ZMKDT}
											</div>
										</td>
										<!-- e:증빙일 -->
									</tr>
									<tr>
										<!-- s:승인번호 -->	
										<th scope="row">승인번호</th>
										<td>
											<div>
												<c:out value="${taxHeader.ZISSID }" />
												<input type="hidden" class="infoTextInput" id="ZISSID" name="ZISSID" readonly="readonly"  value="${taxHeader.ZISSID }"/>
											</div>
										</td>	
										<!-- e:승인번호 -->
										<!-- s:참조번호 -->	
										<th scope="row">참조번호</th>
										<td colspan="3">
											<div>
												<c:out value="${taxHeader.ZETNO }" />
												<input type="hidden" class="infoTextInput" id="ZETNO" name="ZETNO" readonly="readonly"  value="${taxHeader.ZETNO }"/>
											</div>
										</td>	
										<!-- e:참조번호 -->
									</tr>
									<tr>
										<!-- s:총 금액 -->	
										<th scope="row">총 금액</th>
										<td>
											<div>
												<fmt:formatNumber type="number" value="${taxHeader.ZTTAT }" /> 원
												<input type="hidden" class="infoTextInput" id="TOT_AMOUNT" name="TOT_AMOUNT" readonly="readonly"  value="${taxHeader.ZTTAT }"/>
											</div>
										</td>
										<!-- e:총 금액 -->
										<!-- s:총 공급가액 -->	
										<th scope="row">총 공급가액</th>
										<td>
											<div>
												<fmt:formatNumber type="number" value="${taxHeader.HWBAS }" /> 원
												<input type="hidden" class="infoTextInput" id="AMT_AMOUNT" name="AMT_AMOUNT" readonly="readonly" value="${taxHeader.HWBAS }"/>
											</div>
										</td>
										<!-- e:총 공급가액 -->
										<!-- s:총 부가세액 -->	
										<th scope="row">총 부가세액</th>
										<td>
											<div>
												<fmt:formatNumber type="number" value="${taxHeader.HWSTE }" /> 원
												<input type="hidden" class="infoTextInput" id="VAT_AMOUNT" name="VAT_AMOUNT" readonly="readonly" value="${taxHeader.HWSTE }"/>
											</div>
										</td>
										<!-- e:총 부가세액 -->
									</tr>
								</tbody>
							</table>
						
							<!-- s:tb-wrap -->
							<div class="tb-wrap mgn-t-10">
								<table class="tb-st">
									<caption class="screen-out">전표작성항목</caption>
									<colgroup>
										<col width="13%">
										<col width="20%">
										<col width="13%">
										<col width="20%">
										<col width="13%">
										<col width="20%">
									</colgroup>
									<tbody>
										<tr>
											<!-- s:부가세사업장 -->	
											<th scope="row">부가세사업장</th>
											<td>
												<div class="sel-wrap">
													<select title="부가세사업장" id="WORKPLACE_CD" name="WORKPLACE_CD">
														<option value="">선택</option>
														<c:forEach var="item" items="${workplaceList}">
															<option value="${item.WORKPLACE_CD}">${item.WORKPLACE_NM}</option>
														</c:forEach>
													</select>
												</div>
											</td>
											<!-- e:부가세사업장 -->
										
											<!-- s:계정구분 -->	
											<th scope="row">계정구분</th>
											<td>
												<div class="sel-wrap">
													<tag:combo codeGrp="ACCGUBUN" name="ACCGUBUN" />
												</div>
											</td>
											<!-- e:계정구분 -->
										
											<!-- s:세금코드 -->	
											<th scope="row">세금코드</th>
											<td>
												<div class="sel-wrap">
													<tag:taxCombo billGb="TAX" name="TAX_CD" />
												</div>
											</td>
											<!-- e:세금코드 -->
										</tr>
									
										<tr>
											<!-- s:구매처 -->
											<th id="th_lifnr" scope="row">구매처</th>
											<td colspan="5">
												<div class="typeA">
													<div class="inp-wrap">
														<input type="text" class="inp-comm" title="구매처" id="PURCHASEOFFI_NM" name="PURCHASEOFFI_NM" value="${vaList[0].NAME1}" readonly="readonly">
														<input type="hidden" id="PURCHASEOFFI_CD" name="PURCHASEOFFI_CD" value="${vaList[0].LIFNR}" /><!-- 구매처코드 -->
														<input type="hidden" id="BANK_CD" name="BANK_CD" value="${vaList[0].BANKL}" /><!-- 은행코드 -->
														<input type="hidden" id="BANK_NM" name="BANK_NM" value="${vaList[0].BANKA}" /><!-- 은행명 -->
		<%-- 												<input type="hidden" id="PAYEE_CD" name="PAYEE_CD" value="${data.PAYEE_CD }" /><!-- 수취인코드 --> --%>
		<%-- 												<input type="hidden" id="PAYEE_NM" name="PAYEE_NM" value="${data.PAYEE_NM }" /><!-- 수취인명 --> --%>
														<input type="hidden" id="ACCOUNT_NO" name="ACCOUNT_NO" value="${vaList[0].BANKN}" /><!-- 계좌번호 -->
														<input type="hidden" id="ACCOUNT_HOLDER" name="ACCOUNT_HOLDER" value="${vaList[0].KOINH}" /><!-- 예금주명 -->
														<input type="hidden" id="PAY_TERM" name="PAY_TERM" value="${vaList[0].ZTERM}" /><!-- 지급조건 -->
		<%-- 												<input type="hidden" id="PAYDUE_DT" name="PAYDUE_DT" value="${data.BANKL }" /><!-- 지급예정일 --> --%>
													</div>
													<a href="#none" class="btn evn-st01" onclick="fn_popVendor(2); return false;">변경</a>
												</div>
											</td>
											<!-- e:구매처 -->
											</tr>
										<tr>
											<!-- s:은행명 -->
											<th scope="row">은행명</th>
											<td id="td_banka">${vaList[0].BANKA }</td>
											<!-- e:은행명 -->
											
											<!-- s:계좌번호 -->
											<th scope="row">계좌번호</th>
											<td id="td_bankn">${vaList[0].BANKN }</td>
											<!-- e:계좌번호 -->
											
											<!-- s:지급예정일 -->	
											<th scope="row">지급예정일</th>
											<td id="td_paydueDt"></td>
											<!-- e:지급예정일 -->
										</tr>	
																	
										<tr>
											<!-- s:추가직원 -->	
											<th scope="row">추가직원</th>
											<td colspan="5" data-name="lstArea1">
												<a href="#" class="btn evn-st01" onclick="layer_open('memberModal'); return false;">선택</a>
												<div id="addPrsnName">
												</div>
											</td>
											<!-- e:추가직원 -->
										</tr>
										
										<tr>
											<!-- s:증빙 -->	
											<th scope="row">증빙 (jpg, pdf, doc, xls 등)</th>
											<td  colspan="5">
												<div class="bxType01 fileWrap">
													<div class="filebox" id="attachFile0"></div>
													<div class="fileList" style="padding:7px 0 5px;">
														<div id="detailFileList0" class="fileDownLst">
															
														</div>
													</div>
												</div>
											</td>
											<!-- e:증빙 -->
										</tr>
									</tbody>
								</table>
							</div>
							<!-- e:tb-wrap -->
							
							<div class="popPayBtn" style="text-align:right; margin:5px 0;">
								<!-- s:추가버튼 -->
								<div id="addBTN" class="btn comm st01" onclick="javascript:addItem(); return false;">
									<a href="#"><span>추가</span></a>
								</div>
								<!-- e:추가버튼 -->
								<!-- s:삭제버튼 -->
<!-- 								<div id="delBTN" class="btn comm st02" onclick="javascript:deleteItem(); return false;"> -->
<!-- 									<a href="#"><span>삭제</span></a> -->
<!-- 								</div> -->
								<!-- e:삭제버튼 -->
							</div>
							
							<table class="tb-st wd-per-100" id="tb_items">
								<tbody>
<!-- 								<thead> -->
									<tr>
										<th class="payDivTile th_costcenter" style="width:15%; text-align:center;">코스트센터</th>
										<th class="payDivTile" style="width:20%; text-align:center;">G/L 계정</th>
										<th class="payDivTile" style="width:50%; text-align:center;">적요</th>
										<th class="payDivTile" style="width:15%; text-align:center;">공급가액</th>
										<th class="payDivTile th_vat" style="width:15%; text-align:center;">부가세액</th>
										<th class="payDivTile th_aufnr" style="width:10%; text-align:center;">오더명</th>
										<th class="payDivTile th_accstartdt" style="width:16%; text-align: center;">선급비용-시작일</th>
										<th class="payDivTile th_accenddt" style="width:16%; text-align: center;">선급비용-종료일</th>
										<th class="payDivTile" style="width:6%; text-align:center;">삭제</th>
									</tr>
<!-- 								</thead> -->
<!-- 								<tbody> -->
									<tr id="dynamicRow">
										<td class="td_costcenter"  style="width:100%">
											<div class="inp-wrap f-l" style="width:100%">
												<input type="text" class="inp-comm ar ktext" title="코스트센터" name="costCenter" value="${costcenter.KTEXT }" readonly="readonly" placeholder="클릭하여 조회" />
												<input type="hidden" class="kostl" title="코스트센터코드" id="kostl_0" name="arrKostl" value="${costcenter.KOSTL }"/>
											</div>
										</td>
										<!-- s:G/L계정 -->	
										<td class="td_glaccount" style="width:100%">
											<div class="inp-wrap f-l" style="width: 100%;">
												<input type="text" class="inp-comm glName" title="GL계정" name="arrGlName" readonly="readonly" placeholder="클릭하여 조회" />
												<input type="hidden" class="glCode" name="arrGlCode">
											</div>
										</td>
										<!-- e:G/L계정 -->
										<!-- s:적요 -->
										<td>
											<div class="inp-wrap" style="width:100%">
												<input type="text" class="inpTxt ar summary" title="적요" name="arrSummary" maxlength="100" onclick="Grids.Focused = null;" />
											</div>
										</td>
										<!-- e:적요 -->
										<td>
											<div class="inp-wrap type02" style="width:90%;">
												<input type="text" class="inpTxt ar text_right onlyNumber itemAmt" style="text-align: right;" maxlength="12" title="공급가액" name="arrItemAmt" onclick="Grids.Focused = null;" />
												<span class="unit">원</span> 
											</div>
										</td>
										<td class="td_vat">
											<div class="inp-wrap type02" style="width:90%;">
												<input type="text" class="inpTxt ar text_right onlyNumber itemVat" style="text-align: right;" maxlength="12" title="세액" name="arrItemVat" onclick="Grids.Focused = null;" />
												<span class="unit">원</span> 
											</div>
										</td>
										<td class="td_aufnr">
											<div class="inp-wrap wd-per-100">
												<input type="hidden" class="aufnr" name="arrAufnr">
												<input type="text" class="inpTxt ar cur-po autxt" title="오더명" name="arrAutxt" placeholder="클릭하여 조회" readonly="readonly" />
												<input type="image" src="/images/com/web/textRemove.png" onclick="deleteText('0'); return false;" class="textRemove">
											</div>
										</td>
										<td class="td_accstartdt">
											<div>
												<input type="text" class="inp-comm datePic moOpenCal accStartDt" title="선급비용-시작일" name="arrAccStartDt" readonly="readonly">
											</div>
										</td>
										<td class="td_accenddt">
											<div>
												<input type="text" class="inp-comm datePic moOpenCal accEndDt" title="선급비용-종료일" name="arrAccEndDt" readonly="readonly">
											</div>
										</td>
										<td class="td_del" style="text-align:center;">
											<div style="display:inline-block;">
												<input type="image" src="/images/com/web/textRemove.png" onclick="javascript:deleteItem(this); return false;" class="textRemove">
											</div>
										</td>
									</tr>
								</tbody> 
							</table>
							<div class="t-r mgn-t-5" style=" color:#276baa;">
								<p>
									<b>미입력금액 :</b>
									<span style="display:inline-block; width:130px; vertical-align:top;">
										<input type="text" class="infoTextInput t-r" id="minusAmt" readonly/>
									</span>
									<b>원</b>
								</p>
							</div>
						</div>		
					</form>
					
				</div>
            </div>
        </div>
    </div>
