<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="aspn.com.common.util.Utility" %>
<%--
  Class Name : unregisteredTaxList.jsp
  Description : 미등록 세금계산서 화면
  Modification Information
 
          수정일       		수정자                   수정내용
    -------    --------    ---------------------------
    2017.08.22  정호윤              최초 생성

    author   : 정호윤
    since    : 2017.08.22
--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<%-- 레이어 팝업 --%>
<link rel="stylesheet" href="/css/eacc/modalPopup.css">
<script src="/js/com/web/modalPopup.js"></script>
<script src="/js/eacc/eaccCommon.js"></script>
<script type="text/javascript">
var gubunDiv = '〔';
var layerGubun = "transTaxPerson"; // 레이어 팝업 구분자(추가직원-addPerson, 이관-transPerson)
var transGrid = ""; // 이관대상자 그리드
var transCol = ""; // 이관대상자 컬럼
var transRow = []; // 이관대상자 row

$(document).ready(function(){
	
	$('#searchVal').keydown(function(key){
		<%-- 사업장 검색 Enter Key Down Event --%>
		if(key.keyCode == 13){
			fn_searchData();
		}
	});
	
	<%-- 데이터 URL --%>
	var parameters = "startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val() + "&searchVal=" + $("#searchVal").val();
	var url = "/hello/eacc/taxBill/getUnregisteredTaxData.do?" + parameters;
	$("#UnregisteredTaxList").find('bdo').attr("Data_Url", url);

	<%-- 그리드 load 이벤트 핸들러 --%>
	Grids.OnReady = function(grid){
	};
	
	<%-- 그리드 row 더블클릭시 팝업 호출 --%>
	Grids.OnDblClick = function(grid, row, col){
		if(row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER") {
			return;
		}
		
		<%-- 일괄 이관 적용 --%>
		
		if(grid.id == "UnregisteredTaxList") {
			if(col == "RECEIVE_EMAIL"){
				if(row.id == "Filter"){
					var selRow = Grids.UnregisteredTaxList.GetSelRows();
					if(selRow.length == 0){
						alert("<spring:message code='taxBillReportList.selectTransferTax' />"); // 이관할 세금계산서를 선택하세요.
						return;
					}else{
						transGrid = grid.id; // 이관대상자 그리드
						transCol = col; // 이관대상자 컬럼
						for(var i = 0 ; i < selRow.length ; i++) {
							transRow.push(selRow[i]);
						}
						layer_open('memberModal');
					}
				}else{
					transGrid = grid.id; // 이관대상자 그리드
					transCol = col; // 이관대상자 컬럼
					transRow.length = 0;
					transRow.push(row);
					layer_open('memberModal');
				}
			}
		}
		//부서의 포함된 사용자 조회 이벤트
		if(grid.id == "DivisionList") {
			selDeptRow = row;
			getMemberList(row.DIVISION, row.CUR_LVL, row.COMPANY_ID);
		}
	}
	
});

function fn_reloadGrid(){
	<%-- 그리드 새로고침 --%>
	Grids.UnregisteredTaxList.ReloadBody();
}

function fn_searchData() {
	<%-- 날짜 검색 --%>
	var parameters = "startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val() + "&searchVal=" + $("#searchVal").val();
	Grids.UnregisteredTaxList.Source.Data.Url = "/hello/eacc/taxBill/getUnregisteredTaxData.do?" + parameters;
	Grids.UnregisteredTaxList.ReloadBody();
}

function fn_passTax(gridNm){
	var receiveEmail = "";
	var zissid = "";
	<%-- 이관 --%>
	if(gridNm == "UnregisteredTaxList"){
		var selRow = Grids.UnregisteredTaxList.GetSelRows();
		if(selRow.length == 0){
			alert("<spring:message code='taxBillReportList.selectTransferTax' />"); // 이관할 세금계산서를 선택하세요.
			return;
		}else{
			for(var i = 0 ; i < selRow.length ; i++) {
				if(selRow[i].RECEIVE_EMAIL == "" || selRow[i].RECEIVE_EMAIL == null || selRow[i].RECEIVE_EMAIL == "undefined"){
					alert("<spring:message code='taxBillReportList.checkTransferEmail' />"); // 이관할 이메일 주소를 확인해 주세요.
					return;
				}else{
					if(receiveEmail == ""){
						receiveEmail = selRow[i].RECEIVE_EMAIL;
						zissid = selRow[i].ZISSID;
					}else{
						receiveEmail += gubunDiv + selRow[i].RECEIVE_EMAIL;
						zissid += gubunDiv + selRow[i].ZISSID;
					}
				}
			}
			
			if(confirm("<spring:message code='taxBillReportList.wantTransferTax' />")){ // 선택하신 세금계산서를 이관하시겠습니까?
				$.ajax({
					type: 'POST',
					url: '/hello/eacc/taxBill/updatePassTaxAjax.do',
					async : false, 
					dataType: 'json',
					data : {RECEIVE_EMAIL : receiveEmail, ZISSID : zissid},
					success: function (json) {
						if(json.result == "T"){
							alert(json.resultMsg);
							fn_reloadGrid();
						}else{
							alert(json.resultMsg);
							fn_reloadGrid();
						}
					},error: function(XMLHttpRequest, textStatus, errorThrown){
						alert("<spring:message code='taxBillReportList.errTransferTax' />"); // 세금계산서 이관 중 오류가 발생하였습니다.
						fn_reloadGrid();
					}
				});
			}
		}
	}
}
</script>
</head>
<body>
<!-- 화면 UI Area Start  -->
<div id="contents">

	<!-- Hidden Area Start -->
	<!-- Hidden Area End   -->
	
	<!-- Area Start  -->
	<div class="fl-box panel-wrap03" style="width:100%; height:100%;">
		<!-- Title Area Start  -->
		<h5 class="panel-tit mgn-l-10"><spring:message code='title.unregisteredTax' /></h5> <!-- 미등록 세금계산서 -->
		<!-- Title Area End    -->
		
		<!-- inq-area-inner : s  -->
		<div class="inq-area-inner type06 ab">
			<ul class="wrap-inq">
				<li class="inq-clmn">
					<h4 class="tit-inq"><spring:message code='tax.zmkdt' /></h4> <!-- 발행일자 -->
					<div class="prd-inp-wrap">
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic openCal thisMonth" readonly="readonly" gldp-id="gldp-7636599364" id="startDt"/>
							</span>
						</span>
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic openCal" readonly="readonly" gldp-id="gldp-8530641166" id="endDt" />
							</span>
						</span>
					</div>
				</li>
				<li class="inq-clmn">
					<h4 class="tit-inq"><spring:message code='tax.buynoNm' /></h4> <!-- 사업장명 -->
					<div class="prd-inp-wrap">
						<input type="text" class="inp-wrap type03" id="searchVal" name="searchVal" value=""/>
					</div>
				</li>
			</ul>
			<a href="#" class="btn comm st01" onclick="fn_searchData(); return false;"><spring:message code='button.search' /></a> <!-- 검색 -->
		</div>
	
		<!-- Grid Area Start   -->
		<div class="grid-wrap mgn-t-25">
			<div id="UnregisteredTaxList" style="height:100%;">
				<bdo	Debug="Error"
						Layout_Url="/hello/eacc/taxBill/unregisteredTaxLayout.do"
						Upload_Url="/hello/eacc/taxBill/unregisteredTaxEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols" Upload_Xml="2">
				</bdo>
			</div>
		</div>
		<!-- Grid Area End     -->
	</div>
	<!-- Area End    -->
</div>
<!-- 화면 UI Area End    -->

<%@ include file="/WEB-INF/jsp/hello/eacc/common/popMemberDivList.jsp"%>
</body>
</html>
