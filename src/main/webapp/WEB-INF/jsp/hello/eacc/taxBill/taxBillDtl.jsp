<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : taxBillDtl.jsp
  Description : 세금계산서전표 View화면
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.08.25    윤선철              최초 생성

    author   : 윤선철
    since    : 2017.08.25
--%>

<script type="text/javascript">
$(document).ready(function(){
	
	// 초기화
	fn_setTable('${data.ACCGUBUN}');
	
	// 첨부파일
	var maxFileCnt = 10;
	for(var i=0; i<1; i++){
		$('#attachFile'+i).append('<input type="file" multiple id="ex_file'+i+'" class="ksUpload" name="fileList">');	
		var fileType = 'ppt |pps |pptx |ppsx |pdf |hwp |xls |xlsx |xlsm | xlt |doc |dot |docx |docm |txt |gif |jpg |jpeg |png';
		
		$('#ex_file'+i).MultiFile({
				accept : fileType
			,	list   : '#detailFileList'+i
			,	max    : maxFileCnt
			,	STRING : {
					remove: '<img src="/images/com/web/btn_remove.png">'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
//						text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" >',
					idx	  : i,
					denied:'$ext는(은) 업로드할 수 없는 파일 확장자입니다.'
				}
		});
		
		$('.MultiFile-wrap').attr("id", "jquery"+i);
	}
	
	// 첨부파일 삭제
	$('.icn_fileDel').on('click', function(){
		delModiFile($(this));
	});
	
	// 파일 다운로드
	$('.btnFileDwn').on('click', function(){
		var ATTACH = $(this).data("attach");
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO=${data.ATTACH_GRP_NO}&ATTACH='+ATTACH;
		location.href = src;
	});
	
});

/* 계정구분값에 따라 th, td show/hide */
function fn_setTable(accgubun){
	if(accgubun == '3'){	// 선급비용
		$(".th_accstartdt").show();
		$(".th_accenddt").show();
		$(".td_accstartdt").show();
		$(".td_accenddt").show();
		
		$(".th_vat").show();
		$(".td_vat").show();
		
		$(".th_aufnr").hide();
		$(".td_aufnr").hide();
		
		$(".th_costcenter").show();
		$(".td_costcenter").show();
		
	}else{	//경비
		$(".th_accstartdt").hide();
		$(".th_accenddt").hide();
		$(".td_accstartdt").hide();
		$(".td_accenddt").hide();
		
		$(".th_vat").show();
		$(".td_vat").show();
		
		$(".th_costcenter").show();
		$(".td_costcenter").show();
		
		$(".th_aufnr").hide();
		$(".td_aufnr").hide();
		
		// 선급비용시작일 초기화
		$('[name=arrAccStartDt]').each(function(){
			$(this).val('');
		});
		// 선급비용종료일 초기화
		$('[name=arrAccEndDt]').each(function(){
			$(this).val('');
		});
	}
}

/* 임시전표 삭제 처리 */
function deleteTmpTaxBill(){
	
	if(confirm("임시전표를 삭제하시겠습니까?")){
		
		$('#taxBillUptForm').attr("action","/hello/eacc/taxBill/deleteParkingTaxBill.do").ajaxSubmit(function(json){
			// 삭제 성공
			if(json.resultCd == "T"){
				alert(json.resultMsg);
				reLoadGrid();
				toggleModal($("#taxBillUptModal"));
			}else{
				alert(json.resultMsg);
			}	
		});
	}
}

</script>

	<div class="modal-dialog root wd-per-80"> <!-- 원하는 width 값 입력 -->
        <div class="modal-content">
			<div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
               	<h4 class="modal-title">세금계산서전표 상세</h4>
               	
               	<div style="position:absolute; top:13px; right:40px;">
					<c:if test="${page ne 'view'}">
						<!-- 결재상태가 저장이고 전표상태가 임시전표생성 상태인 경우 : 임시전표 삭제 버튼 -->
						<c:if test="${data.GW_STATUS eq '01' && data.BILL_STATUS eq 'V'}">
							<a href="#" class="btn comm st01 f-r" style="margin-bottom: 5px; margin-right: 5px;" onclick="deleteTmpTaxBill(); return false;">임시전표 삭제</a>
						</c:if>
					</c:if>
				</div>
            </div>
            <div class="modal-body" >
				<div class="modal-bodyIn" >

					
					<!-- s:input -->
					<input type="hidden" title="오늘날짜" id="toDate" name="toDate" value="${toDate}"/>
					<input type="hidden" title="경비작성마감일" id="submitDeadLine" name="submitDeadLine" value="${submitDeadLine}"/> <!-- 4일 -->
					<input type="hidden" title="경비결재마감일" id="apprDeadLine" name="apprDeadLine" value="${apprDeadLine}"/> <!-- 6일 -->
					<!-- e:input -->

					<form id="taxBillUptForm" action="" method="post" enctype="multipart/form-data">
						
						<!-- input : s -->
						<input type="hidden" name="TAXBILL_NDX" value="${data.TAXBILL_NDX }" />
						<input type="hidden" name="TAXAPPR_NO" value="${data.TAXAPPR_NO }" />
						<!-- 첨부파일 정보 -->
						<input type="hidden" title="파일등록여부" id="attachExistYn" name="attachExistYn" value=""/>
						<input type="hidden" title="삭제Seq" id=delSeq name="delSeq" value=""/>
						<input type="hidden" title="파일 건수" id="fileCnt" name="fileCnt" value=""/>
						<input type="hidden" title="경비 모듈" id="MODULE" name="MODULE" value="8">
						<input type="hidden" id="ATTACH_GRP_NO" name="ATTACH_GRP_NO" value="${data.ATTACH_GRP_NO }"/>
						<!-- input : e -->
						
						<div>
							<!-- s: 세금계산서 영수증 -->
							<%@ include file="/WEB-INF/jsp/hello/eacc/common/taxReceipt.jsp"%>
							<!-- e: 세금계산서 영수증 -->
						</div>
												
						<!-- s:inq-area-top -->
<!-- 						<div class="inq-area-top mgn-t-15"> -->
<%-- 							<c:if test="${page ne 'view'}"> --%>
<!-- 								결재상태가 저장이고 전표상태가 임시전표생성 상태인 경우 : 임시전표 삭제 버튼 -->
<%-- 								<c:if test="${data.GW_STATUS eq '01' && data.BILL_STATUS eq 'V'}"> --%>
<!-- 									<a href="#" class="btn comm st01 f-r" style="margin-bottom: 5px; margin-right: 5px;" onclick="deleteTmpTaxBill(); return false;">임시전표 삭제</a> -->
<%-- 								</c:if> --%>
<%-- 							</c:if> --%>
<!-- 						</div> -->
						<!-- e:inq-area-top -->
							
						<div id="">							
							<!-- s:tb-wrap -->
							<div class="tb-wrap mgn-t-15">
								<table class="tb-st">
									<caption class="screen-out">세금계산서 전표 VIEW</caption>
									<colgroup>
										<col width="13%">
										<col width="20%">
										<col width="13%">
										<col width="20%">
										<col width="13%">
										<col width="20%">
									</colgroup>
									<tbody>
										<tr>
											<!-- s:전기일 -->	
											<th scope="row">전표 구분</th>
											<td>
												<c:choose>
													<c:when test="${taxHeader.ZDTIG eq '02' || taxHeader.ZDTIG eq 'B02'}">
														<div>전자계산서</div>
													</c:when>
													<c:otherwise>
														<div>전자세금계산서</div> 
													</c:otherwise>
												</c:choose>
											</td>
											<!-- e:전기일 -->
											
											<!-- s:전기일 -->	
											<th scope="row">전기일자</th>
											<td>
												<div>
													${data.BOOK_DT }
												</div>
											</td>
											<!-- e:전기일 -->
											
											<!-- s:증빙일 -->
											<th scope="row">증빙일자</th>
											<td>
												<div>
													${data.EVIDENCE_DT }
												</div>
											</td>
											<!-- e:증빙일 -->
										</tr>
										<tr>
											<!-- s:승인번호 -->	
											<th scope="row">승인번호</th>
											<td>
												<div>
													<c:out value="${taxHeader.ZISSID }" />
												</div>
											</td>	
											<!-- e:승인번호 -->
											<!-- s:참조번호 -->	
											<th scope="row">참조번호</th>
											<td colspan="3">
												<div>
													<c:out value="${taxHeader.ZETNO }" />
												</div>
											</td>	
											<!-- e:참조번호 -->
										</tr>
										<tr>
											<!-- s:총 금액 -->	
											<th scope="row">총 금액</th>
											<td>
												<div>
													<fmt:formatNumber type="number" value="${data.TOT_AMOUNT}"/>
													<span class="unit">원</span> 
												</div>
											</td>
											<!-- e:총 금액 -->
											<!-- s:총 공급가액 -->	
											<th scope="row">총 공급가액</th>
											<td>
												<div>
													<fmt:formatNumber type="number" value="${data.AMT_AMOUNT}"/>
													<span class="unit">원</span> 
												</div>
											</td>
											<!-- e:총 공급가액 -->
											<!-- s:총 부가세액 -->	
											<th scope="row">총 부가세액</th>
											<td>
												<div>
													<fmt:formatNumber type="number" value="${data.VAT_AMOUNT}"/>
													<span class="unit">원</span> 
												</div>
											</td>
											<!-- e:총 부가세액 -->
										</tr>
										
									</tbody>
								</table>
							</div>
							<!-- e:tb-wrap -->
						</div>
						
						<!-- s:tb-wrap -->
						<div class="tb-wrap mgn-t-10">
							<table class="tb-st">
								<colgroup>
									<col width="13%">
									<col width="20%">
									<col width="13%">
									<col width="20%">
									<col width="13%">
									<col width="20%">
								</colgroup>
								<tbody>
									<tr>
										<!-- s:부가세사업장 -->	
										<th scope="row">부가세사업장</th>
										<td>
											${data.WORKPLACE_NM }
										</td>
										<!-- e:부가세사업장 -->
										
										<!-- s:계정구분 -->	
										<th scope="row">계정구분</th>
										<td>
											${data.accgubunNm }
										</td>
										<!-- e:계정구분 -->
										
										<!-- s:세금코드 -->	
										<th scope="row">세금코드</th>
										<td>
											${data.taxCdNm }
										</td>
										<!-- e:세금코드 -->
									</tr>
									<tr>
										<!-- s:구매처 -->
										<th id="th_lifnr" scope="row">구매처</th>
										<td colspan="5">
											${data.PURCHASEOFFI_NM }
										</td>
										<!-- e:구매처 -->
									</tr>
									<tr>
										<!-- s:은행명 -->
										<th scope="row">은행명</th>
										<td id="td_banka">${data.BANK_NM }</td>
										<!-- e:은행명 -->
										
										<!-- s:계좌번호 -->
										<th scope="row">계좌번호</th>
										<td id="td_bankn">${data.ACCOUNT_NO }</td>
										<!-- e:계좌번호 -->
										
										<!-- s:지급예정일 -->	
										<th scope="row">지급예정일</th>
										<td id="td_paydueDt">
											<c:if test="${data.PAYDUE_DT ne '' && data.PAYDUE_DT ne null}">
												${fn:substring(data.PAYDUE_DT,0,4)}-${fn:substring(data.PAYDUE_DT,5,7)}-${fn:substring(data.PAYDUE_DT,8,10)}
											</c:if>
										</td>
										<!-- e:지급예정일 -->
									</tr>								
									<tr>
										<!-- s:추가직원 -->	
										<th scope="row">추가직원</th>
										<td colspan="5" data-name="lstArea1">
											<div id="addPrsnName">
												${data.ADD_PRSN_NM}
<%-- 												<c:if test="${data.ADD_PRSN_NM ne '' and data.ADD_PRSN_NM ne null }"> --%>
<!-- 													<div class="inp-wrap readonly" style="width:100%;height:auto;margin-top:5px;"> -->
<%-- 														<textarea cols="" rows="2" id="ADD_PRSN_NM" name="ADD_PRSN_NM" readonly="readonly" >${data.ADD_PRSN_NM}</textarea> --%>
<%-- 														<input type="hidden" name="ADD_PRSN" value="${data.ADD_PRSN}"/> --%>
<!-- 													</div> -->
<%-- 												</c:if> --%>
											</div>
										</td>
										<!-- e:추가직원 -->
									</tr>
									<tr>
										<!-- s:증빙 -->	
										<th scope="row">증빙</th>
										<td  colspan="5">
											<div class="bxType01 fileWrap">
<!-- 												<div id="attachFile" class="attachFile"></div> -->
												<div class="fileList">
													<div id="detailFileList" class="fileDownLst">
														<c:forEach var="item" items="${attachList}" varStatus="idx">
															<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
<!-- 																<span class="MultiFile-remove"> -->
<%-- 																	<a href="#none" class="icn_fileDel" data-attach="${item.ATTACH}" data-idx="${item.CD_FILE_TYPE }"> --%>
<!-- 																		<img src="/images/com/web/btn_remove.png" /> -->
<!-- 																	</a> -->
<!-- 																</span>  -->
																<span class="MultiFile-export">
																	${item.SEQ_DSP}
																</span> 
																<span class="MultiFile-title" title="File selected: ${item.NAME}">
																	${item.NAME}(${item.FILE_SIZE}kb)
																</span>
																<a href="#none" class="btnFileDwn icon download" data-attach="${item.ATTACH}">다운로드</a>
															</div>		
														</c:forEach>	
													</div>
												</div>
											</div>
										</td>
										<!-- e:증빙 -->
									</tr>
								</tbody>
							</table>
						</div>
						<!-- e:tb-wrap -->
						
						<div class="tb-wrap mgn-t-10" style="width:100%;">
							<table id="tb_items" class="tb-st">
								<tbody>
<!-- 								<thead> -->
									<tr>
										<th class="payDivTile th_costcenter" style="width:15%; text-align:center;">코스트센터</th>
										<th class="payDivTile" style="width:20%; text-align:center;">G/L 계정</th>
										<th class="payDivTile" style="width:40%; text-align:center;">적요</th>
										<th class="payDivTile" style="width:15%; text-align:center;">공급가액</th>
										<th class="payDivTile th_vat" style="width:15%; text-align:center;">부가세액</th>
										<th class="payDivTile th_aufnr" style="width:10%; text-align:center;">오더명</th>
										<th class="payDivTile th_accstartdt" style="width:16%; text-align: center;">선급비용-시작일</th>
										<th class="payDivTile th_accenddt" style="width:16%; text-align: center;">선급비용-종료일</th>
									</tr>
<!-- 								</thead> -->
									<c:forEach var="item" items="${itemList}" begin="0" end="${itemList.size()}" varStatus="status">
										<tr id="dynamicRow0">
											<td class="td_costcenter"  style="width:100%">
												${item.KTEXT}
											</td>
											<!-- s:G/L계정 -->	
											<td class="td_glaccount" style="width:100%">
												${item.GLNAME}
											</td>
											<!-- e:G/L계정 -->
											<!-- s:적요 -->
											<td>
												${fn:replace(item.SUMMARY, '"', '&quot;')}
											</td>
											<!-- e:적요 -->
											<td style="text-align: right;">
												<fmt:formatNumber type="number" value="${item.ITEM_AMT}"/>
												<span class="unit">원</span> 
											</td>
											<td class="td_vat" style="text-align: right;">
												<fmt:formatNumber type="number" value="${item.ITEM_VAT}"/>
												<span class="unit">원</span> 
											</td>
											<td class="td_aufnr">
												${item.AUTXT}
											</td>
											<td class="td_accstartdt" style="text-align: center;">
												${fn:substring(item.ACCSTARTDT,0,4)}-${fn:substring(item.ACCSTARTDT,5,7)}-${fn:substring(item.ACCSTARTDT,8,10)}
											</td>
											<td class="td_accenddt" style="text-align: center;">
												${fn:substring(item.ACCENDDT,0,4)}-${fn:substring(item.ACCENDDT,5,7)}-${fn:substring(item.ACCENDDT,8,10)}
											</td>
										</tr>
									</c:forEach>
								</tbody> 
							</table>
						</div>
					</form>
					<iframe id="fileDownFrame" style="width:0px;height:0px;border:0px"></iframe>
				</div>
            </div>
        </div>
    </div>