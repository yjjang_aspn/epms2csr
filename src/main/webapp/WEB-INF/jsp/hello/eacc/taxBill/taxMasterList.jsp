<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="aspn.com.common.util.Utility" %>
<%--
  Class Name : taxBillList.jsp
  Description : 세금계산서 작성 목록 화면
  Modification Information
 
          수정일       		수정자                   수정내용
    -------    --------    ---------------------------
    2017.08.22  윤선철              최초 생성
    2017.10.19  위경욱              IE에서 Parameter encoding Error 수정
                                                           각 컴퓨터의 server.xml과 web.xml에 URI Encoding 세팅을 한 경우에도 IE에서 Parameter Encoding이 제대로 되지 않는 경우가 있어 아래와 같이 처리
                          1. GET방식으로 넘길 경우
                          Grids.TaxMasterList.Source.Data.Url = "/hello/eacc/taxBill/getTaxMasterData.do?" + encodeURI(parameters);
                          2. POST방식으로 넘길 경우
                          Grids.TaxMasterList.Source.Data.Url = "/hello/eacc/taxBill/getTaxMasterData.do";
                          Grids.TaxMasterList.Source.Data.Params = parameters;
                          POST방식은 bdo 태그도 아래와 같이 Data_Method="POST" 를 추가
                          <bdo	Debug="Error"
   	                      		Layout_Url="/hello/eacc/taxBill/taxMasterListLayout.do"
   	                      		Data_Method="POST"
   	                      >
                          </bdo>

    author   : 윤선철
    since    : 2017.08.22
--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<link rel="stylesheet" href="/css/eacc/modalPopup.css">
<script src="/js/eacc/eaccCommon.js" type="text/javascript" ></script>
<script src="/js/com/web/modalPopup.js"></script>
<script src="/js/jquery/jquery-filestyle.js" type="text/javascript" ></script>
<script src="/js/jquery/jquery.MultiFile.js" type="text/javascript"></script>
<script src="/js/jquery/jquery.ajaxsubmit.js" type="text/javascript"></script>
<script type="text/javascript" >
var gubunDiv = '〔';
var layerGubun = "transTaxPerson"; // 레이어 팝업 구분자(추가직원-addPerson, 이관-transPerson)
var transGrid = ""; // 이관대상자 그리드
var transCol = ""; // 이관대상자 컬럼
var transRow = []; // 이관대상자 row
var bill_gb = "2";
$(document).ready(function(){
		
	$('#searchVal').keydown(function(key){
			<%-- 사업장 검색 Enter Key Down Event --%>
			if(key.keyCode == 13){
				fn_searchData();
			}
	});
	/* 데이터 URL */
	var parameters = "startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val() + "&searchVal=" + $("#searchVal").val();
	var url = "/hello/eacc/taxBill/getTaxMasterData.do?" + encodeURI(parameters);
	$("#taxMasterList").find('bdo').attr("Data_Url", url);
	
	//그리드 row 더블클릭시 이빈트
	Grids.OnDblClick = function(grid, row, col){
		if(row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "_ConstHeight") {
			return;
		}
		
		if(grid.id == "TaxMasterList") {
			if(col == "RECEIVE_EMAIL"){
				if(row.id == "Filter"){
					var selRow = Grids.TaxMasterList.GetSelRows();
					if(selRow.length == 0){
						alert("이관할 세금계산서를 선택하세요.");
						return;
					}else{
						transGrid = grid.id; // 이관대상자 그리드
						transCol = col; // 이관대상자 컬럼
						for(var i = 0 ; i < selRow.length ; i++) {
							transRow.push(selRow[i]);
						}
						layer_open('memberModal');
					}
				}else{
					transGrid = grid.id; // 이관대상자 그리드
					transCol = col; // 이관대상자 컬럼
					transRow.length = 0;
					transRow.push(row);
					layer_open('memberModal');
				}
			}else{
				if(row.id != "Filter"){
					var url = fn_getTaxBillDetail(row, col);	
					url += "&"+new Date().getTime();
					
					// 전표 수정 모달 팝업 로드
					$("#taxBillWriteModal").load(url, function(){
						$(this).modal({
							//backdrop : 'static'
						});
					});
					
					Grids.Active = null;
				}
			}
		}
		
		//더블클릭 시 부모창에 코스트센터 정보 세팅
		if(grid.id == "CostcenterList") {
			fnSetCostcenter(row.KOSTL, row.KOKRS, row.KTEXT);
			layer_open('costcenterModal');
		}
		
		//더블클릭 시 부모창에 계정 정보 세팅
		if(grid.id == "GlAccountList") {
			fnSetGlAccount(row);
			layer_open('glaccountModal');
			$('#searchBunryu').val("");
		}
		
		//더블클릭 시 부모창에 구매처 정보 세팅
		if(grid.id == "VendorList") {
			fnSetVendor(row);
			layer_open('vendorModal');
			$("#searchVal").val("");
			$("#searchGubun").val("");
		}
		
	}	
	//그리드 저장 후 이벤트
	Grids.OnAfterSave = function(grid, result, autoupdate) {
		grid.ReloadBody();
	}
	// 코스트센터 변경 팝업 호출
	$(document).on("click", ".ktext", function(){
		var idx = $(this).parents("tr").index() - 1;
		fn_popCostcenter(idx);
	});
	
	// G/L계정 변경 팝업 호출
	$(document).on("click", ".glName", function(){
		var idx = $(this).parents("tr").index() - 1;
		fn_popGlAccount(bill_gb, idx);
	});
	
	// 오더 변경 팝업 호출
	$(document).on("click", ".autxt", function(){
		var idx = $(this).parents("tr").index() - 1;
		fn_popInorder(idx);
	});
});

/* 날짜 검색 */
function fn_searchData() {
	var parameters = "startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val() + "&searchVal=" + $("#searchVal").val();
	Grids.TaxMasterList.Source.Data.Url = "/hello/eacc/taxBill/getTaxMasterData.do";
	Grids.TaxMasterList.Source.Data.Params = parameters;
	Grids.TaxMasterList.ReloadBody();
}

/* layerPopUp에 세금계산서 상세 화면 출력 */
function fn_getTaxBillDetail(row, col){
	var ZISSID = row.ZISSID;
	var srcUrl = "/hello/eacc/taxBill/taxBillWrite.do";
	var param = "?ZISSID=" + ZISSID;
	
	return srcUrl + param;
}

/* 그리드 리로드 */
function fn_reloadGrid(){
	Grids.TaxMasterList.ReloadBody();
}

/* 모달 토글 */
function toggleModal(obj){
	obj.modal('toggle');
}

/* 이관처리 */
function fn_passTax(gridNm){
	var receiveEmail = "";
	var zissid = "";
	<%-- 이관 --%>
	if(gridNm == "TaxMasterList"){
		var selRow = Grids.TaxMasterList.GetSelRows();
		if(selRow.length == 0){
			alert("이관할 세금계산서를 선택하세요.");
			return;
		}else{
			for(var i = 0 ; i < selRow.length ; i++) {
				if(selRow[i].RECEIVE_EMAIL == "" || selRow[i].RECEIVE_EMAIL == null || selRow[i].RECEIVE_EMAIL == "undefined"){
					alert("이관할 이메일 주소를 확인해 주세요.");
					return;
				}else{
					if(receiveEmail == ""){
						receiveEmail = selRow[i].RECEIVE_EMAIL;
						zissid = selRow[i].ZISSID;
					}else{
						receiveEmail += gubunDiv + selRow[i].RECEIVE_EMAIL;
						zissid += gubunDiv + selRow[i].ZISSID;
					}
				}
			}
			
			if(confirm("선택하신 세금계산서를 이관하시겠습니까?")){
				$.ajax({
					type: 'POST',
					url: '/hello/eacc/taxBill/updatePassTaxAjax.do',
					async : false, 
					dataType: 'json',
					data : {RECEIVE_EMAIL : receiveEmail, ZISSID : zissid},
					success: function (json) {
						if(json.result == "T"){
							alert(json.resultMsg);
							fn_reloadGrid();
						}else{
							alert(json.resultMsg);
							fn_reloadGrid();
						}
					},error: function(XMLHttpRequest, textStatus, errorThrown){
						alert("<spring:message code='taxBillReportList.errTransferTax' />"); // 세금계산서 이관 중 오류가 발생하였습니다.
						fn_reloadGrid();
					}
				});
			}
		}
	}
}

/* 세금계산서 xml 업로드 */
function fn_taxUpload(){
	//세금계산서 xml 업로드 module은 80으로 설정 한다
	var url = '/hello/eacc/taxBill/popTaxUploadForm.do';	
	url += "&"+new Date().getTime();
	
	// 전표 수정 모달 팝업 로드
	$("#taxMasterUploadModal").load(url, function(){
		$(this).modal({
		});
	});
	
	Grids.Active = null;
}

/* 세금계산서 xml 삭제 */
function fn_taxDelete(){
		
	var zissid = "";
	var zetno = "";
	var attach_grp_no = "";
	var attach = "";
	var delSelRow = Grids.TaxMasterList.GetSelRows();
	if(delSelRow.length == 0){
		alert("삭제할 세금계산서를 선택하세요.");
		return;
	}else{
		for(var i = 0 ; i < delSelRow.length ; i++) {
			var row = delSelRow[i];
			if(zissid == ""){
				zissid = row.ZISSID;
				zetno = row.ZETNO;
				attach_grp_no = row.ATTACH_GRP_NO;
				attach = row.ATTACH;
			}else{
				zissid += gubunDiv + row.zissid;
				zetno += gubunDiv + row.zetno;
				attach_grp_no += gubunDiv + row.ATTACH_GRP_NO;
				attach += gubunDiv + row.ATTACH;
			}
		}
	}
	$('#taxMasterForm #ZISSID').val(zissid);
	$('#taxMasterForm #ZETNO').val(zetno);
	$('#taxMasterForm #ATTACH_GRP_NO').val(attach_grp_no);
	$('#taxMasterForm #delSeq').val(attach);
	$('#taxMasterForm #attachExistYn').val('Y');
	if(confirm("세금계산서를 삭제 하시겠습니까?")){
		
		$('#taxMasterForm').attr('action','/hello/eacc/taxBill/taxDeleteAjax.do').ajaxSubmit(function(json){
			if(json.resultCd == "T"){
				alert(json.resultMsg);
				Grids.TaxMasterList.ReloadBody();
			} else {
				alert(json.resultMsg);
				Grids.TaxMasterList.ReloadBody();
			}
		});
	}	
}

</script>
</head>
<body>

<!-- 화면 UI Area Start  -->
<div id="contents">

<!-- Hidden Area Start -->
<!-- Hidden Area End   -->

	<div class="fl-box panel-wrap03" style="width:100%; height:100%;">
	<!-- <div class="panel-wrap st01"> -->
			<h5 class="panel-tit">세금계산서 전표작성</h5>
			<form id="taxMasterForm" action="" method="post" enctype="multipart/form-data">
				<input type="hidden" title="승인번호" id="ZISSID" name="ZISSID" value=""/>
				<input type="hidden" title="참조번호" id="ZETNO" name="ZETNO" value=""/>
				<input type="hidden" title="파일등록여부" id="attachExistYn" name="attachExistYn" value=""/>
				<input type="hidden" title="첨부파일그룹번호" id="ATTACH_GRP_NO" name="ATTACH_GRP_NO" value=""/>
				<input type="hidden" title="삭제Seq" id="delSeq" name="delSeq" value=""/>
			</form>
			<!-- inq-area-inner : s -->
			<div class="inq-area-inner type06 ab">
				<ul class="wrap-inq">
					<li class="inq-clmn">
						<h4 class="tit-inq">발행일자</h4>
						<div class="prd-inp-wrap">
							<span class="prd-inp">
								<span class="inner">
									<input type="text" class="inp-comm datePic openCal thisMonth" readonly="readonly" gldp-id="gldp-7636599364" id="startDt"/>
								</span>
							</span>
							<span class="prd-inp">
								<span class="inner">
									<input type="text" class="inp-comm datePic openCal" readonly="readonly" gldp-id="gldp-8530641166" id="endDt" />
								</span>
							</span>
						</div>
					</li>
					<li class="inq-clmn">
						<h4 class="tit-inq">사업장명</h4>
						<div class="prd-inp-wrap">
							<input type="text" class="inp-wrap type03" id="searchVal" name="searchVal" value=""/>
						</div>
					</li>
				</ul>
				<a href="#" class="btn comm st01" onclick="fn_searchData(); return false;">검색</a>
				<a href="#" class="btn comm st02" onclick="fn_taxUpload(); return false;">세금계산서 xml 업로드</a>
				<a href="#" class="btn comm st02" onclick="fn_taxDelete(); return false;">세금계산서 삭제 </a>
				
<!-- 				<a href="#" class="btn comm st02" onclick="fn_popDeleteTaxBill(); return false;">일괄제외</a> -->
			</div>
			<!-- inq-area-inner : e -->
			<div class="grid-wrap mgn-t-25">
				<div id="taxMasterList" style="height:100%;">
					<bdo	Debug="Error"
							Layout_Url="/hello/eacc/taxBill/taxMasterListLayout.do"
							Data_Method="POST"
						>
					</bdo>
				</div>
			</div>
			<!-- e:grid-wrap -->
	</div>
</div>
<!-- 세금계산서 전표 작성화면 모달 시작-->
<div class="modal fade" id="taxBillWriteModal" data-backdrop="static" data-keyboard="false"></div>
<div class="modal fade" id="taxMasterUploadModal" data-backdrop="static" data-keyboard="false"></div>
<!-- 세금계산서 전표 작성화면 모달 끝-->

<%@ include file="/WEB-INF/jsp/hello/eacc/common/popMemberDivList.jsp"%>
<%@ include file="/WEB-INF/jsp/hello/eacc/common/popNeedBill.jsp"%>
<!-- Layer PopUp End -->

<!-- 화면 UI Area End    -->
</body>
</html>
