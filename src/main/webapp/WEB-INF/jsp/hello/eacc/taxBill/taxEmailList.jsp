<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="aspn.com.common.util.Utility" %>
<%--
  Class Name : registerTaxEmailList.jsp
  Description : 세금계산서 이메일 등록 화면
  Modification Information
 
          수정일       		수정자                   수정내용
    -------    --------    ---------------------------
    2017.08.21  정호윤              최초 생성

    author   : 정호윤
    since    : 2017.08.21
--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script src="/js/eacc/eaccCommon.js"></script>
<script type="text/javascript" >
var selEmail = null;

<%-- 그리드 load 이벤트 핸들러 --%>
Grids.OnReady = function(grid){
};

Grids.OnClick = function(grid, row, col){
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar"){
		return;
	}

};

Grids.OnAfterValueChanged = function(grid, row, col, type){
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return; 
	}
	
	if(grid.id == "TaxEmailList"){
		if(col == "EMAIL"){
			selEmail = row.EMAIL;
			fn_checkEmail(selEmail);
		}
	}
}

function addRowGrid(gridNm){
	var row = null;
	var i = 0;
	
	if(gridNm == "TaxEmailList") {

		row = Grids[gridNm].AddRow( null, Grids[gridNm].GetFirst(), true);

		Grids[gridNm].SetValue(row, "TAXEMAIL_NDX", "", 1);
		Grids[gridNm].SetAttribute(row, "COMPANY_ID", "", 1);
		Grids[gridNm].SetValue(row, "USER_ID", "", 1);
		Grids[gridNm].SetValue(row, "NAME", "", 1);
		Grids[gridNm].SetValue(row, "EMAIL", "", 1);
	}
}

function fn_saveGrid(gridNm){
	<%-- 비어있는 입력 값 검사 --%>
	if(gridNm == "TaxEmailList") {
		for(var row = Grids[gridNm].GetFirst(); row; row = Grids[gridNm].GetNext(row)){
			if(row.EMAIL == null || row.EMAIL == "undefined" || row.EMAIL == ""){
				alert("<spring:message code='taxEmailList.enterEmail' />"); // 저장할 이메일 주소를 입력해 주세요.
				return;
			}
		}
		saveGrid(gridNm);
	}
}

function fn_checkEmail(email){
	<%-- 이메일 형식 검사 --%>
	 var regEmail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	
	if(!regEmail.test(email)){
		alert("<spring:message code='taxEmailList.errEmailAddress' />"); // 이메일 주소가 유효하지 않습니다.
		return false;
	}
}

</script>
</head>
<body>
<!-- 화면 UI Area Start  -->
<div id="contents">

	<!-- Hidden Area Start -->
	<!-- Hidden Area End   -->
	
	<!-- Area Start  -->
	<div class="fl-box panel-wrap03" style="width:100%; height:100%;">
		<!-- Title Area Start  -->
		<h5 class="panel-tit"><spring:message code='title.taxEmail' /></h5> <!-- 이메일 등록 -->
		<!-- Title Area End    -->
	
		<!-- Grid Area Start   -->
		<div class="panel-body">
			<div id="TaxEmailList">
				<bdo	Debug="Error"
						Data_Url="/hello/eacc/taxBill/getTaxEmailData.do" Data_Format="String"
						Layout_Url="/hello/eacc/taxBill/taxEmailLayout.do"
						Upload_Url="/hello/eacc/taxBill/taxEmailEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols" Upload_Xml="2">
				</bdo>
			</div>
		</div>
		<!-- Grid Area End     -->
	</div>
	<!-- Area End    -->
</div>
<!-- 화면 UI Area End    -->

</body>
</html>
