<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%--
  Class Name : unregisteredTaxLayout.jsp
  Description : 미등록 세금계산서 그리드 레이아웃 화면
  Modification Information
 
       수정일    	     수정자                   수정내용
   -------    --------    ---------------------------
   2017.08.22  정호윤	              최초 생성

    author   : 정호윤
    since    : 2017.08.22
--%>
<c:set var="gridId" value="UnregisteredTaxList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			AcceptEnters   = "1" Calculated = "1"  CalculateSelected = "1"      DateStrings = "2" Deleting  = "0"
			Dragging       = "0" Editing    = "1"  EnterMode         = "4"      Filtering   = "1" IdChars   = "0123456789"
			InEditMode     = "2" MaxPages   = "20" NoFormatEscape    = "1"      NoHScroll   = "0" NoVScroll = "0"
			NumberId       = "1" SafeCSS    = '1'  Selecting		 = "1"
			SelectingCells = "0" SortIcons  = "0"  Style             = "Office" SuppressCfg = "1" 
			Paging		   = "2" AllPages	= "0"  PageLength		 = "20"		MaxPages	= "20"
			CopySelected   = "0" CopyFocused= "1"  CopyCols			 = "0"		ExportFormat="xls" ExportCols="0"
	/>
	
	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>
	
	<!-- Grid Header Setting Area Start  -->
	<Head>
	<Header  Spanned="1" Align="center" 
		ZETNOSpan="10"    ZETNO ="<spring:message code='tax.header.zetno' />" 
		ZBYCNSpan="10"    ZBYCN ="<spring:message code='tax.header.zbycn' />" 
		ZMKDTRowSpan="2"  ZMKDT ="<spring:message code='tax.zmkdt' />"   		ZMKDTType  ="Text"
		ZTTATRowSpan="2"  ZTTAT ="<spring:message code='tax.header.zttat' />"   ZTTATType  ="Text"
		HWBASRowSpan="2"  HWBAS ="<spring:message code='tax.header.hwbas' />"   HWBASType  ="Text"
		HWSTERowSpan="2"  HWSTE ="<spring:message code='tax.header.hwste' />"   HWSTEType  ="Text"
		ZISSIDRowSpan="2" ZISSID ="<spring:message code='tax.header.zissid' />" ZISSIDType  ="Text"
		RECEIVE_EMAILRowSpan="2"	RECEIVE_EMAIL= "<spring:message code='tax.header.receiveEmail' />"		RECEIVE_EMAILType  ="Text"
    />
	<Header  id="Header"  Align="center"
		ZETNO      ="<spring:message code='tax.zetno' />"
		COMPANY_ID ="<spring:message code='company.companyId' />"
      	ZSPCN      ="<spring:message code='tax.zspcn' />"
      	SUPNO      ="<spring:message code='tax.supno' />"
      	ZSPNM      ="<spring:message code='tax.zspnm' />"
		ZSPRN      ="<spring:message code='tax.zsprn' />"
		ZSPRT      ="<spring:message code='tax.zsprt' />"
		ZSPBT      ="<spring:message code='tax.zspbt' />"
		ZSPIT      ="<spring:message code='tax.zspit' />"
		ZSPAR      ="<spring:message code='tax.zspar' />"
		ZBYCN      ="<spring:message code='tax.zbycn' />"
		BUYNO      ="<spring:message code='tax.buyno' />"
		ZBYNM      ="<spring:message code='tax.zbynm' />"
		ZBYRP      ="<spring:message code='tax.zbyrp' />"
		ZBYRN      ="<spring:message code='tax.zbyrn' />"
		ZBYRT      ="<spring:message code='tax.zbyrt' />"
		ZBYRE      ="<spring:message code='tax.zbyre' />"
		ZBYBT      ="<spring:message code='tax.zbybt' />"
		ZBYIT      ="<spring:message code='tax.zbyit' />"
		ZBYAR      ="<spring:message code='tax.zbyar' />"
      />
	<!-- Grid Header Setting Area End    -->
	
	<!-- Fileter Setting Area Start      -->
		<Filter	id = "Filter" CanFocus = "1" RECEIVE_EMAIL="" RECEIVE_EMAILTip = "<spring:message code='tip.batchapply.transfer' />" RECEIVE_EMAILCanEdit="0"/>/>
	</Head>
	<!-- Fileter Setting Area End        -->
	
	<Cols>
		<C Name="ZETNO"     	Type="Text"    RelWidth="1"    	 Align="center"  CanEdit="0" Visible="0" CanHide="0" /><!-- 세금계산서 기타참조번호 -->
    	<C Name="COMPANY_ID"	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="0" CanHide="0" /><!-- 회사코드 -->
	    <C Name="ZSPCN"			Type="Text"    RelWidth="150"    Align="left"  	 CanEdit="0" Visible="1" CanHide="0" /><!-- 공급업체 -->
	    <C Name="SUPNO"      	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="1" CanHide="0" /><!-- 사업자번호  -->
	    <C Name="ZSPNM"      	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="1" /><!-- 대표자 -->
	    <C Name="ZSPBT"      	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="0" /><!-- 공급자 업종  -->
	    <C Name="ZSPIT"      	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="0" /><!-- 공급자 업태 -->
	    <C Name="ZSPAR"      	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="0" /><!-- 공급자 주소 -->
	    <C Name="ZSPRN"      	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="1" CanHide="0" /><!-- 공급담당자  -->
	    <C Name="ZSPRT"     	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="1" /><!-- 공급담당자연락처  -->
	    <C Name="ZBYCN"      	Type="Text"    RelWidth="150"    Align="left"    CanEdit="0" Visible="1" CanHide="0" /><!-- 공급받는자 상호  --> 
	    <C Name="BUYNO"      	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="0" CanHide="0" /><!-- 공급받는자 사업자번호  --> 
	    <C Name="ZBYNM"      	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="1" /><!-- 공급받는자 대표자  --> 
	    <C Name="ZBYBT"      	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="0" /><!-- 공급받는자 업종  -->
	    <C Name="ZBYIT"      	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="0" /><!-- 공급받는자 업태  -->
	    <C Name="ZBYAR"      	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="0" /><!-- 공급받는자 주소  -->
	    <C Name="ZBYRP"      	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="0" /><!-- 공급받는자 담당부서명  --> 
	    <C Name="ZBYRN"      	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="1" /><!-- 공급받는자 담당자명  -->
	    <C Name="ZBYRT"      	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="1" /><!-- 공급받는자 연락처  -->
	    <C Name="ZBYRE"      	Type="Text"    RelWidth="150"    Align="center"  CanEdit="0" Visible="1" /><!-- 공급받는자 이메일  --> 
	    <C Name="ZMKDT"      	Type="Date"    RelWidth="100"    Align="center"  CanEdit="0" Visible="1" Format="yyyy-MM-dd" EditFormat="yyyy-MM-dd" 			  Spanned="1" CanHide="0" /><!-- 발행일  --> 
	    <C Name="ZTTAT"         Type="Int"     RelWidth="100"    Align="right"   CanEdit="0" Visible="1" Format="#,##0"  ExportStyle='mso-number-format:"#,##0";' Spanned="1" CanHide="0" /><!-- 총금액  --> 
	    <C Name="HWBAS"         Type="Int"     RelWidth="100"    Align="right"   CanEdit="0" Visible="1" Format="#,##0"  ExportStyle='mso-number-format:"#,##0";' Spanned="1" CanHide="0" /><!-- 공급가  --> 
	    <C Name="HWSTE"         Type="Int"     RelWidth="100"    Align="right"   CanEdit="0" Visible="1" Format="#,##0"  ExportStyle='mso-number-format:"#,##0";' Spanned="1" CanHide="0" /><!-- 세액  --> 
	    <C Name="ZISSID"    	Type="Text"    RelWidth="220"    Align="center"  CanEdit="0" Visible="1" Spanned="1" CanHide="0" /><!-- 승인번호  -->
	    <C Name="RECEIVE_EMAIL"	Type="Text"    RelWidth="100"    Align="center"  CanEdit="0" Visible="1" Spanned="1" CanHide="0" /><!-- 이관대상자ID --> 
	</Cols>
		
	<!-- Grid Paging Setting Area Start  -->
	<Pager Visible = "0"/>

	<Solid>
		<I id = "PAGER" Cells = "NAV,LIST,ONE,GROUP" Space = "4"
		   NAVType	     = "Pager"
		   LISTType	     = "Pages" LISTAlign   = "left" LISTLeft   = "10"                LISTRelWidth  = "1"
		   ONEType       = "Bool"  ONECanEdit  = "1"    ONEFormula = "Grid.AllPages?0:1" ONELabelRight = "<spring:message code='button.showUnitPage' />"
		   ONEOnChange = "Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
		   GROUPCanFocus = "0"
		/>
	</Solid>
	<!-- Grid Paging Setting Area End    -->
	
	<Toolbar	Space="0"	Styles="1"	Cells="Empty,Cnt,Found,<spring:message code='button.trans' />,<spring:message code='button.selectItem' />,<spring:message code='button.refresh' />,<spring:message code='button.print' />,<spring:message code='button.excel' />"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType = "Button"
				CntType = "Html" CntFormula = '"Total : &lt;b>"+count(7)+"&lt;/b>"' CntWidth = '-1' CntWrap = '0'
				FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>'" FoundWidth = '-1' FoundWrap = '0'
				<spring:message code='button.trans' />Type ="Html"  <spring:message code='button.trans' />="&lt;a href='#none' title='<spring:message code='button.trans' />' class=&quot;treeButton treeMove &quot;
									onclick='fn_passTax(&quot;${gridId}&quot;)'><spring:message code='button.trans' />&lt;/a>"
				<spring:message code='button.refresh' />Type = "Html" <spring:message code='button.refresh' /> = "&lt;a href='#none' title='<spring:message code='button.refresh' />' class=&quot;defaultButton icon refresh&quot;
									  onclick='reloadGrid(&quot;${gridId}&quot;)'><spring:message code='button.refresh' />&lt;/a>"
 				<spring:message code='button.print' />Type = "Html" <spring:message code='button.print' /> = "&lt;a href='#none' title='<spring:message code='button.print' />' class=&quot;defaultButton icon printer&quot;
									  onclick='printGrid(&quot;${gridId}&quot;)'><spring:message code='button.print' />&lt;/a>"
 				<spring:message code='button.excel' />Type = "Html" <spring:message code='button.excel' /> = "&lt;a href='#none' title='<spring:message code='button.excel' />'class=&quot;defaultButton icon excel&quot;
									  onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'><spring:message code='button.excel' />&lt;/a>"
				<spring:message code='button.selectItem' />Type="Html" <spring:message code='button.selectItem' />="&lt;a href='#none' title='<spring:message code='button.selectItem' />' class=&quot;treeButton treeSelect&quot;
			  						  onclick='showColumns(&quot;${gridId}&quot;,&quot;xls&quot;)'><spring:message code='button.selectItem' />&lt;/a>"
	/>

</Grid>

