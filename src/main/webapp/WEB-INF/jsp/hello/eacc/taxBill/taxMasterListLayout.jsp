<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%--
  Class Name : taxMasterLayout.jsp
  Description : 세금계산서 작성 목록 그리드 레이아웃 화면
  Modification Information
 
       수정일    	     수정자                   수정내용
   -------    --------    ---------------------------
   2017.08.22  윤선철	              최초 생성

    author   : 윤선철
    since    : 2017.08.22
--%>
<c:set var="gridId" value="TaxMasterList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"  SortIcons="0"       Calculated="1"      CalculateSelected ="1"  NoFormatEscape="1"
			NumberId="1"          DateStrings="2"     SelectingCells="0"  AcceptEnters="1"        Style="Office"
			InEditMode="0"        SafeCSS='1'         NoVScroll="0"       NoHScroll="0"           EnterMode="0"
			Filtering="1"         Dragging="0"        Selecting="1" 	  Deleting="0" 			  Editing ="1"
			CopySelected="0"	  CopyFocused="1"     CopyCols="0"		  ExportFormat="xls" 	  ExportCols="0"
	/>
	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>
	<Cfg Paging="2" PageLength="30" PageMin="2" MaxPages="10" SuppressCfg="1"/>
	
	<Head>
		<Header	NoEscape="1" Spanned="1" Align="center" 
            ZETNOSpan="10"  			ZETNO="공급자" 
            ZBYCNSpan="10"  			ZBYCN="공급받는자" 
            ZMKDTRowSpan="2" 			ZMKDT ='발행일'      				ZMKDTType  ="Text" 
            ZTTATRowSpan="2" 			ZTTAT ='총금액'       			ZTTATType  ="Text"
            HWBASRowSpan="2" 			HWBAS ='공급가액'      			HWBASType  ="Text"
            HWSTERowSpan="2" 			HWSTE ='부가세액'      			HWSTEType  ="Text"
            ZISSIDRowSpan="2" 			ZISSID ='승인번호'     			ZISSIDType  ="Text"
            RECEIVE_EMAILRowSpan="2"	RECEIVE_EMAIL= "이관(Email)"		RECEIVE_EMAILType  ="Text"
            ATTACH_GRP_NORowSpan="2" 	ATTACH_GRP_NO="첨부파일그룹번호"		ATTACH_GRP_NOType  ="Text"
            ATTACHRowSpan="2" 			ATTACH="첨부파일번호"				ATTACHType  ="Text"
		 />
		<Header	id="Header"	Align="center" Spanned="1"
			ZETNO	     	="기타참조번호"
			COMPANY_ID		="회사 코드"
			ZSPCN        	="상호"
			SUPNO       	="사업자번호"
			ZSPNM			="대표자"
			ZSPRN	       	="담당자"
			ZSPRT	       	="연락처"
			ZSPBT			="업종"
			ZSPIT			="업태"
			ZSPAR			="주소"
			ZBYCN			="상호"
			BUYNO			="사업자번호"
			ZBYNM			="대표자"
			ZBYRP			="담당부서명"
			ZBYRN			="담당자"
			ZBYRT			="연락처"
			ZBYRE	       	="이메일"
			ZBYBT			="업종"
			ZBYIT			="업태"
			ZBYAR			="주소"
			
		/>
		
		<Filter	id="Filter"	CanFocus="1" RECEIVE_EMAIL="" RECEIVE_EMAILTip = "이관 일괄적용" RECEIVE_EMAILCanEdit="0"/>/>
		
	</Head>

	<Solid>
		<I  id="PAGER"	Cells="NAV,LIST,ONE,GROUP"	Space="4"
			NAVType="Pager"
			LISTType="Pages"	LISTRelWidth="1"	LISTAlign="left"	LISTLeft="10"
			ONEType="Bool"		ONEFormula="Grid.AllPages?0:1" 			ONECanEdit="1"	ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"
		/>
	</Solid>
	
	<Pager Visible="0" CanHide="0"/>
	
	<Cols>
		<C Name="ZETNO" 		Type="Text"		RelWidth="1"		Align="center"	CanEdit="0"	Visible="0" CanHide="0"/><!-- 세금계산서 기타참조번호 -->
		<C Name="COMPANY_ID" 	Type="Text"		RelWidth="100"		Align="center"	CanEdit="0"	Visible="0" CanHide="0"/><!-- 회사코드 -->
		<C Name="ZSPCN" 		Type="Text"		RelWidth="150"		Align="center"	CanEdit="0" Visible="1"	CanHide="0"/><!-- 공급업체 -->
		<C Name="SUPNO"  		Type="Text"		RelWidth="100"		Align="center"	CanEdit="0" Visible="1"	CanHide="0"/><!-- 사업자번호 -->
		<C Name="ZSPNM"  		Type="Text"		RelWidth="100"		Align="center"	CanEdit="0" Visible="1"/><!-- 대표자 -->
		<C Name="ZSPBT"  		Type="Text"		RelWidth="100"		Align="center"	CanEdit="0" Visible="0"/><!-- 공급자 업종 -->
		<C Name="ZSPIT"  		Type="Text"		RelWidth="100"		Align="center"	CanEdit="0" Visible="0"/><!-- 공급자 업태 -->
		<C Name="ZSPAR"  		Type="Text"		RelWidth="100"		Align="center"	CanEdit="0" Visible="0"/><!-- 공급자 주소 -->
		<C Name="ZSPRN"			Type="Text"	    RelWidth="100"		Align="center"	CanEdit="0"	Visible="1" CanHide="0"/><!-- 공급담당자 -->
		<C Name="ZSPRT"			Type="Text"	    RelWidth="100"		Align="center"	CanEdit="0"	Visible="1"/><!-- 공급담당자연락처 -->
		<C Name="ZBYCN"			Type="Text"	    RelWidth="150"		Align="center"	CanEdit="0"	Visible="1" CanHide="0"/><!-- 공급받는자 상호 -->
		<C Name="BUYNO"			Type="Text"	    RelWidth="100"		Align="center"	CanEdit="0"	Visible="0" CanHide="0"/><!-- 공급받는자 사업자번호 -->
		<C Name="ZBYNM"			Type="Text"	    RelWidth="100"		Align="center"	CanEdit="0"	Visible="1"/><!-- 공급받는자 대표자 -->
		<C Name="ZBYBT"			Type="Text"	    RelWidth="100"		Align="center"	CanEdit="0"	Visible="0"/><!-- 공급받는자 업종-->
		<C Name="ZBYIT"			Type="Text"	    RelWidth="100"		Align="center"	CanEdit="0"	Visible="0"/><!-- 공급받는자 업태-->
		<C Name="ZBYAR"			Type="Text"	    RelWidth="100"		Align="center"	CanEdit="0"	Visible="0"/><!-- 공급받는자 주소-->
		<C Name="ZBYRP"			Type="Text"	    RelWidth="100"		Align="center"	CanEdit="0"	Visible="0"/><!-- 공급받는자 담당부서명-->
		<C Name="ZBYRN"			Type="Text"	    RelWidth="100"		Align="center"	CanEdit="0"	Visible="1"/><!-- 공급받는자 담당자명 -->
		<C Name="ZBYRT"			Type="Text"	    RelWidth="100"		Align="center"	CanEdit="0"	Visible="1"/><!-- 공급받는자 연락처 -->
		<C Name="ZBYRE"			Type="Text"	    RelWidth="150"		Align="center"	CanEdit="0"	Visible="1"/><!-- 공급받는자 이메일 -->
		<C Name="ZMKDT"			Type="Date"     RelWidth="100"		Align="center"  CanEdit="0" Visible="1" Format="yyyy-MM-dd" EditFormat="yyyy-MM-dd" Spanned="1" CanHide="0"/><!-- 발행일 -->
		<C Name="ZTTAT"	    	Type="Int"      Width="100"     	Align="right"  	CanEdit="0" Visible="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' Spanned="1" CanHide="0"/><!-- 총금액 -->
		<C Name="HWBAS"	    	Type="Int"  	Width="100"     	Align="right"  	CanEdit="0" Visible="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' Spanned="1" CanHide="0"/><!-- 공급가 -->
		<C Name="HWSTE"	    	Type="Int"	    Width="100"     	Align="right"  	CanEdit="0" Visible="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' Spanned="1" CanHide="0"/><!-- 세액 -->
		<C Name="ZISSID"		Type="Text"     Width="220"     	Align="center"  CanEdit="0" Visible="1" Spanned="1" CanHide="0"/><!-- 승인번호 -->
		<C Name="RECEIVE_EMAIL"	Type="Text"     Width="100"     	Align="center"  CanEdit="0" Visible="1" Spanned="1" CanHide="0"/><!-- 이관대상자ID -->
		<C Name="ATTACH_GRP_NO" Type="Text"		RelWidth="1"		Align="center"	CanEdit="0"	Visible="0" CanHide="0"/><!-- 세금계산서 첨부파일 그룹번호 -->
		<C Name="ATTACH" 		Type="Text"		RelWidth="1"		Align="center"	CanEdit="0"	Visible="0" CanHide="0"/><!-- 세금계산서 첨부파일 번호 -->
	</Cols>
	
	<Toolbar	Space="0"	Styles="2" Cells="Empty,Cnt,Found,Chg,이관,항목선택,새로고침,인쇄,엑셀"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType="Button"
				CntType="Html"	CntFormula='"총 :&lt;b>"+count(7)+"&lt;/b>행"'	CntWidth='-1'	CntWrap='0'
             	FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>행'" FoundWidth = '-1' FoundWrap = '0'
				ChgType = "Html" ChgFormula = 'var cnt=count("Row.Changed==1",7);return cnt?"변경된 행 :&lt;b>"+cnt+"&lt;/b>":""' ChgWidth = '-1' ChgWrap = '0'
				이관Type ="Html"  이관="&lt;a href='#none' title='이관' class=&quot;treeButton treeMove&quot;
									onclick='fn_passTax(&quot;${gridId}&quot;)'>이관&lt;/a>"
				새로고침Type = "Html" 새로고침 = "&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
									  onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
 				인쇄Type = "Html" 인쇄 = "&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
									  onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
 				엑셀Type = "Html" 엑셀 = "&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
									  onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
			 	항목선택Type="Html" 항목선택="&lt;a href='#none' title='항목선택' class=&quot;treeButton treeSelect&quot;
			  						  onclick='showColumns(&quot;${gridId}&quot;,&quot;xls&quot;)'>항목선택&lt;/a>"
	/>
</Grid>