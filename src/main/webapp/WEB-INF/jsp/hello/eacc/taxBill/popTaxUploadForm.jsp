<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : popTaxUploadForm.jsp
  Description : 세금계산서 xml 업로드 화면
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.08.31    윤선철              최초 생성

    author   : 윤선철
    since    : 2017.08.31
--%>
<script type="text/javascript">
var maxFileCnt = 5;
	
	//첨부파일
	for(var i=0; i<1; i++){
		$('#attachFile'+i).append('<input type="file" multiple id="ex_file'+i+'" class="ksUpload" name="fileList">');	
		var fileType = 'xml';
		
		$('#ex_file'+i).MultiFile({
				accept : fileType
			,	list   : '#detailFileList'+i
			,	max    : maxFileCnt
			,	STRING : {
					remove: '<img src="/images/com/web/btn_remove.png">'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
//						text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" >',
					idx	  : i,
					denied:'$ext는(은) 업로드할 수 없는 파일 확장자입니다.'
				}
		});
		
		$('.MultiFile-wrap').attr("id", "jquery"+i);
	}
	
	//첨부파일 삭제
	$('.icn_fileDel').on('click', function(){
		delModiFile($(this));
	});
	
	//xml 등록
	$('#btnTaxAdd').on('click', function(){
		var fileCnt = $(".MultiFile-label").length;
		
		if(fileCnt < 1){
			alert("업로드할 세금계산서 파일을 선택해주세요.");
			return;
		}
		$('#fileFrm').attr("action","/hello/eacc/taxBill/taxUploadAjax.do").ajaxSubmit(function(json){
			
			if(json.resultCd == "T"){
				alert(json.resultMsg);
				toggleModal($("#taxMasterUploadModal"));
				Grids.TaxMasterList.ReloadBody();
			}else{
				alert(json.resultMsg);
				toggleModal($("#taxMasterUploadModal"));
				Grids.TaxMasterList.ReloadBody();
			}	
		});
	});

</script>

	<div class="modal-dialog root wd-per-30"> <!-- 원하는 width 값 입력 -->
        <div class="modal-content" style="height: 300px;">
			<div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
               	<h4 class="modal-title">첨부파일 다운로드</h4>
            </div>
            <div class="modal-body" >
				<div class="modal-bodyIn" >

					<form id="fileFrm" name="fileFrm" method="post" enctype="multipart/form-data">
						<input type="hidden" id="MODULE" name="MODULE" value="81"> <!-- 세금계산서 xml파일은 81으로 세팅 -->
						<input type="hidden" id="ATTACH_GRP_NO" name="ATTACH_GRP_NO" value="">
						<input type="hidden" id="DEL_SEQ" name="DEL_SEQ" value="">
						<div class="bxType01 fileWrap">
							<div class="filebox" id="attachFile0"></div>
							<div class="fileList" style="padding:7px 0 5px;">
								<div id="detailFileList0" class="fileDownLst">
									
								</div>
							</div>
						</div>
					</form>
					<div class="btn-area">
						<a href="#none" class="btn comm st04" id="btnTaxAdd">등록</a>
					</div>
				</div>
            </div>
        </div>
    </div>
