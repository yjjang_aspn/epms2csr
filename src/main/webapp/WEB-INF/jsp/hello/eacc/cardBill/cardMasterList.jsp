<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%--
  Class Name : cardMasterList.jsp
  Description : 법인카드 전표 작성 목록 
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.08.09    정순주              최초 생성

    author   : 정순주
    since    : 2017.08.09
--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<link rel="stylesheet" href="/css/eacc/modalPopup.css">
<script src="/js/eacc/eaccCommon.js" type="text/javascript" ></script>
<script src="/js/com/web/modalPopup.js"></script>
<script src="/js/jquery/jquery-filestyle.js" type="text/javascript" ></script>
<script src="/js/jquery/jquery.MultiFile.js" type="text/javascript"></script>
<script src="/js/jquery/jquery.ajaxsubmit.js" type="text/javascript"></script>
<script type="text/javascript">
	var gubunDiv = '〔';
	var layerGubun = "transCardPerson"; // 레이어 팝업 구분자(추가직원-addPerson, 이관-transPerson)
	var transGrid = ""; // 이관대상자 그리드
	var transCol = ""; // 이관대상자 컬럼
	var transRow = []; // 이관대상자 row
	var selectedRow = "";
	$(document).ready(function() {
		
		$('#searchVal').keydown(function(key){
			<%-- 사업장 검색 Enter Key Down Event --%>
			if(key.keyCode == 13){
				fn_searchData();
			}
		});
		
		/* 데이터 URL */
		var parameters = "startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val() + "&searchVal=" + $("#searchVal").val();
		var url = "/hello/eacc/cardBill/cardMasterListData.do?" + encodeURI(parameters);
		$("#cardMasterList").find('bdo').attr("Data_Url", url);

		//그리드 row 더블클릭시 에빈트
		Grids.OnDblClick = function(grid, row, col){
			if(row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel" || row.id == "PAGER" || row.id == "_ConstHeight") {
				return;
			}
			
			if (grid.id == "CardMasterList") {
				
				// 이관 모달 팝업
				if(col == "RECEIVE_ID"){
					if(row.id == "Filter"){
						var selRow = Grids.CardMasterList.GetSelRows();
						if(selRow.length == 0){
							alert("이관할 사용내역을 선택하세요.");
							return;
						}else{
							transGrid = grid.id; // 이관대상자 그리드
							transCol = col; // 이관대상자 컬럼
							for(var i = 0 ; i < selRow.length ; i++) {
								transRow.push(selRow[i]);
							}
							layer_open('memberModal');
						}
					}else{
						transGrid = grid.id; // 이관대상자 그리드
						transCol = col; // 이관대상자 컬럼
						transRow.length = 0;
						transRow.push(row);
						layer_open('memberModal');
					}
				}else{
					if(row.id != "Filter"){
						var cardNum = row.CARD_NUM;
						var authNum = row.AUTH_NUM;
						var cardNdx = row.CARD_NDX;
						var authDate = DateToString(row.AUTH_DATE, 'yyyyMMdd');
	
						var url = "/hello/eacc/cardBill/cardBillWrite.do";
						var param = "?cardNum="+cardNum+"&authNum=" + authNum+"&cardNdx="+cardNdx+"&authDate="+authDate+"&"+new Date().getTime();
						// 전표 작성 모달 팝업 로드
						$("#cardBillWriteModal").load(url+param, function(){
							$(this).modal({
								backdrop : 'static'
							});
						});
						
						Grids.Active = null;
					}
				}
			}
			
			//더블클릭 시 부모창에 코스트센터 정보 세팅
			if(grid.id == "CostcenterList") {
				fnSetCostcenter(row.KOSTL, row.KOKRS, row.KTEXT);
				layer_open('costcenterModal');
			}
			
			//더블클릭 시 부모창에 계정 정보 세팅
			if(grid.id == "GlAccountList") {
				fnSetGlAccount(row);
				layer_open('glaccountModal');
				$('#searchBunryu').val("");
			}
			
			//더블클릭 시 부모창에 구매처 정보 세팅
			if(grid.id == "VendorList") {
				fnSetVendor(row);
				layer_open('vendorModal');
				$("#searchVal").val("");
				$("#searchGubun").val("");
			}
		}
		
		//그리드 저장 후 이벤트
		Grids.OnAfterSave = function(grid, result, autoupdate) {
			grid.ReloadBody();
		}
		
		// 코스트센터 변경 팝업 호출
		$(document).on("click", ".ktext", function(){
			var idx = $(this).parents("tr").index() - 1;
			fn_popCostcenter(idx);
		});
		
		// G/L계정 변경 팝업 호출
		$(document).on("click", ".glName", function(){
			var idx = $(this).parents("tr").index() - 1;
			fn_popGlAccount(1, idx);
		});
		
		// 오더 변경 팝업 호출
		$(document).on("click", ".autxt", function(){
			var idx = $(this).parents("tr").index() - 1;
			fn_popInorder(idx);
		});
	});

	/* 날짜 검색 */
	function fn_searchData() {
		var parameters = "startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val() + "&searchVal=" + $("#searchVal").val();
		Grids.CardMasterList.Source.Data.Url = "/hello/eacc/cardBill/cardMasterListData.do?" + encodeURI(parameters);
		Grids.CardMasterList.ReloadBody();
	}

	//그리드 리로드
	function fn_reloadGrid(){
		Grids.CardMasterList.ReloadBody();
	}
	
	//모달 토글
	function toggleModal(obj){
		obj.modal('toggle');
	}
	
	/* 법인카드 사용내역 이관 */
	function fn_passCard(gridNm){
		var receiveId = "";
		var cardNum = "";
		var authNum = "";
		var cardNdx = "";
		var authDate = "";
		<%-- 이관 --%>
		if(gridNm == "CardMasterList"){
			var selRow = Grids.CardMasterList.GetSelRows();
			if(selRow.length == 0){
				alert("이관할 법인카드 내역을 선택하세요.");
				return;
			}else{
				for(var i = 0 ; i < selRow.length ; i++) {
					if(selRow[i].RECEIVE_ID == "" || selRow[i].RECEIVE_ID == null || selRow[i].RECEIVE_ID == "undefined"){
						alert("이관할 대상자를 확인해 주세요.");
						return;
					}else{
						if(receiveId == ""){
							receiveId = selRow[i].RECEIVE_ID;
							cardNum = selRow[i].CARD_NUM;
							authNum = selRow[i].AUTH_NUM;
							cardNdx = selRow[i].CARD_NDX;
							authDate = DateToString(selRow[i].AUTH_DATE, 'yyyyMMdd')
						}else{
							receiveId += gubunDiv + selRow[i].RECEIVE_ID;
							cardNum += gubunDiv + selRow[i].CARD_NUM;
							authNum += gubunDiv + selRow[i].AUTH_NUM;
							cardNdx += gubunDiv + selRow[i].CARD_NDX;
							authDate += gubunDiv + DateToString(selRow[i].AUTH_DATE, 'yyyyMMdd');
						}
					}
				}
				
				if(confirm("선택하신 법인카드 내역을 이관하시겠습니까?")){
					$.ajax({
						type: 'POST',
						url: '/hello/eacc/cardBill/updatePassCardAjax.do',
						async : false, 
						dataType: 'json',
						data : {RECEIVE_ID : receiveId, CARD_NUM : cardNum, AUTH_NUM : authNum, CARD_NDX : cardNdx, AUTH_DATE : authDate},
						success: function (json) {
							if(json.result == "T"){
								alert(json.resultMsg);
								fn_reloadGrid();
							}else{
								alert(json.resultMsg);
								fn_reloadGrid();
							}
						},error: function(XMLHttpRequest, textStatus, errorThrown){
							alert("법인카드 내역 이관 중 오류가 발생하였습니다.");
							fn_reloadGrid();
						}
					});
				}
			}
		}
	}	
	
</script>
</head>
<body>

<!-- 화면 UI Area Start  -->
<div id="contents">
	
	<!-- input : s -->
	<input type="hidden" title="종료날짜" id="endDate" name="endDate" value="${endDate}"/>
	<input type="hidden" title="시작날짜" id="startDate" name="startDate" value="${startDate}"/>
	<input type="hidden" title="경비작성마감일" id="submitDeadLine" name="submitDeadLine" value="${submitDeadLine}" /> <!-- 4일 -->
	<input type="hidden" title="경비결재마감일" id="apprDeadLine" name="apprDeadLine" value="${apprDeadLine}" /> <!-- 6일 -->
	<!-- input : e -->
	
	<!-- form : s -->
	<form id="cardDataForm" name="cardDataForm" method="post"></form>
	<!-- form : e -->
	
	<!-- 페이지 내용 : s -->
	<!-- s:panel-wrap01 -->
	<div class="fl-box panel-wrap03" style="width:100%; height:100%;">
		<h4 class="panel-tit mgn-l-10">법인카드 전표작성</h4>
	
		<!-- inq-area-inner : s -->
		<div class="inq-area-inner type06 ab">
			<ul class="wrap-inq">
				<li class="inq-clmn">
					<h4 class="tit-inq">전기일</h4>
					<div class="prd-inp-wrap">
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic openCal thisMonth" readonly="readonly" gldp-id="gldp-7636599364" id="startDt"/>
							</span>
						</span>
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic openCal" readonly="readonly" gldp-id="gldp-8530641166" id="endDt" />
							</span>
						</span>
					</div>
				</li>
				<li class="inq-clmn">
					<h4 class="tit-inq">승인번호</h4>
					<div class="prd-inp-wrap">
						<input type="text" class="inp-wrap type03" id="searchVal" name="searchVal" value=""/>
					</div>
				</li>
			</ul>
			<a href="#" class="btn comm st01" onclick="fn_searchData(); return false;">검색</a>
		</div>
		<!-- inq-area-inner : e -->
	
		<div class="panel-body mgn-t-25">
			<!-- 트리그리드 : s -->
			<div id="cardMasterList" style="height:100%">
				<bdo Debug="Error"
					Layout_Url="/hello/eacc/cardBill/cardMasterListLayout.do"
					Upload_Url="/hello/eacc/cardBill/transferUserEdit.do" Upload_Data="uploadData" Upload_Flags="AllCols" Upload_Format="Internal"
				>
				</bdo>
			</div>
			<!-- 트리그리드 : e -->
		</div>
	</div>
	<!-- e:panel-wrap01 -->
	<!-- 페이지 내용 : e -->

<!-- 전표 작성 모달 시작-->
<div class="modal fade" id="cardBillWriteModal"  data-backdrop="static" data-keyboard="false"></div>
<!-- 전표 작성 모달 끝-->

</div>

<%@ include file="/WEB-INF/jsp/hello/eacc/common/popNeedBill.jsp"%>
<%@ include file="/WEB-INF/jsp/hello/eacc/common/popMemberDivList.jsp"%>
</body>
</html>