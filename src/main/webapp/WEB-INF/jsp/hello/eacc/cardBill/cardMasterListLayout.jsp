<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%--
  Class Name : cardMasterLayout.jsp
  Description : 법인카드 전표 작성 목록 그리드 레이아웃 화면
  Modification Information
 
       수정일    	     수정자                   수정내용
   -------    --------    ---------------------------
   2017.08.22  정순주	              최초 생성

    author   : 정순주
    since    : 2017.08.22
--%>
<c:set var="gridId" value="CardMasterList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"  SortIcons="0"       Calculated="1"      CalculateSelected ="1"  NoFormatEscape="1"
			NumberId="1"          DateStrings="2"     SelectingCells="0"  AcceptEnters="1"        Style="Office"
			InEditMode="0"        SafeCSS='1'         NoVScroll="0"       NoHScroll="0"           EnterMode="0"
			Filtering="1"         Dragging="0"        Selecting="1" 	  Deleting="0" 			  Editing ="1"
			CopySelected="0"	  CopyFocused="1"     CopyCols="0"		  ExportFormat="xls"	  ExportCols="0"
	/>
	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>
	<Cfg Paging="2" PageLength="30" PageMin="2" MaxPages="10" SuppressCfg="1"/>
	
	<!-- Grid Header Setting Area Start  -->
	<Head>
	<Header	id="Header"	Align="center" Spanned="1"
		CARD_NDX		= "저장 순번"         
		OWNER_REG_NO    = "기업사업자번호"
		CARD_CODE		= "카드코드"
		CARD_NUM        = "카드번호"
		AUTH_NUM        = "승인번호"
		AUTH_DT         = "승인일시"
		AUTH_DATE       = "승인일자"
		AUTH_TIME       = "승인시간"
		GEORAE_STAT     = "거래상태"
		GEORAE_CAND     = "승인/매입취소일자"
		AQUI_DATE       = "매입일자"
		AQUI_TIME       = "매입시간"
		SETT_DATE       = "결제예정일자"
		TOT_AMOUNT		= "총금액"
		AMT_AMOUNT		= "공급가액"
		VAT_AMOUNT		= "부가세"
		SER_AMOUNT		= "봉사료"
		FRE_AMOUNT		= "면세금액"
		A_TOT_AMOUNT    = "승인총금액"
		A_AMT_AMOUNT    = "승인공급가액"
		A_VAT_AMOUNT    = "승인부가세"
		A_SER_AMOUNT    = "승인봉사료"
		P_TOT_AMOUNT    = "매입총금액"
		P_AMT_AMOUNT    = "매입공급가액"
		P_VAT_AMOUNT    = "매입부가세"
		P_SER_AMOUNT    = "매입봉사료"
		B_TOT_AMOUNT    = "청구총금액"
		CURRENCY_UNIT   = "통화코드"
		RATE            = "환율"
		MERC_NAME       = "가맹점명"
		MERC_SAUP_NO    = "가맹점사업자번호"
		MERC_ADDR       = "가맹점주소"
		MERC_REPR       = "가맹점대표자명"
		MERC_TEL        = "가맹점전화번호"
		MERC_ZIP        = "가맹점우편번호"
		MCC_NAME        = "가맹점업종명"
		MCC_CODE        = "가맹점업종코드"
		MCC_STAT        = "가맹점업종구분"
		VAT_STAT        = "가맹점과세유형"
		GONGJE_GUBUN    = "공제불가구분"
		INDATE          = "생성년월일"
		INTIME          = "생성시분초"
		SEND_YN         = "이관여부"
		SEND_ID         = "이관한사람"
		RECEIVE_ID      = "이관대상자"
		SEND_DT         = "이관날짜"
		APPROVAL_STATUS = "작성 상태(1-미작성, 2-작성, 3-반려/결재승인취소)"
		RJCTBILL_NDX    = "반려/승인취소 전표 NDX (구분자)"
		DEL_YN          = "제외여부"
		DEL_ID          = "제외한사람"
		DEL_DT          = "제외날짜"
	/>
	<!-- Grid Header Setting Area End    -->
	
	<!-- Fileter Setting Area Start      -->
		<Filter	id = "Filter" CanFocus = "1" RECEIVE_ID="" RECEIVE_IDTip = "이관 일괄적용" RECEIVE_IDCanEdit="0"/>/>
	</Head>
	<!-- Fileter Setting Area End        -->
	
	<Cols>
		<C Name="AUTH_DT" 			Type="Date" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" Format="yyyy-MM-dd HH:MM:ss" />
		<C Name="AUTH_DATE" 		Type="Date" RelWidth="60" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		<C Name="AUTH_TIME" 		Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="CARD_NUM" 			Type="Text" RelWidth="80" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="AUTH_NUM" 			Type="Text" RelWidth="60" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="TOT_AMOUNT" 		Type="Int" 	RelWidth="50" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="AMT_AMOUNT" 		Type="Int" 	RelWidth="50" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="VAT_AMOUNT" 		Type="Int" 	RelWidth="50" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="SER_AMOUNT" 		Type="Int" 	RelWidth="50" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="A_TOT_AMOUNT" 		Type="Int" 	RelWidth="50" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="A_AMT_AMOUNT" 		Type="Int" 	RelWidth="50" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="A_VAT_AMOUNT" 		Type="Int" 	RelWidth="50" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="A_SER_AMOUNT" 		Type="Int" 	RelWidth="50" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="P_TOT_AMOUNT" 		Type="Int" 	RelWidth="50" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="P_AMT_AMOUNT" 		Type="Int" 	RelWidth="50" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="P_VAT_AMOUNT" 		Type="Int" 	RelWidth="50" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="P_SER_AMOUNT" 		Type="Int" 	RelWidth="50" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="B_TOT_AMOUNT" 		Type="Int" 	RelWidth="50" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="GEORAE_STAT" 		Type="Text" RelWidth="50" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="MERC_NAME" 		Type="Text" RelWidth="80" CanEdit="0" Visible="1" Align="left" CanExport="1" />
		<C Name="MCC_NAME" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="MERC_ADDR" 		Type="Text" RelWidth="180" CanEdit="0" Visible="1" Align="left" CanExport="1" />
		<C Name="RECEIVE_ID" 		Type="Text" RelWidth="80" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		
		<C Name="CARD_NDX" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" CanHide="0" />
		<C Name="CARD_CODE" 		Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" CanHide="0" />
		<C Name="OWNER_REG_NO" 		Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" CanHide="0" />
		<C Name="GEORAE_CAND" 		Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="AQUI_DATE" 		Type="Date" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		<C Name="AQUI_TIME" 		Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="SETT_DATE" 		Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="FRE_AMOUNT" 		Type="Int" 	RelWidth="100" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="CURRENCY_UNIT" 	Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="RATE" 				Type="Int" 	RelWidth="100" CanEdit="0" Visible="0" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="MERC_SAUP_NO" 		Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="MERC_REPR" 		Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="MERC_TEL" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="MERC_ZIP" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="MCC_CODE" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="MCC_STAT" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="VAT_STAT" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="GONGJE_GUBUN" 		Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="INDATE" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="INTIME" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="SEND_YN" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="SEND_ID" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="SEND_DT" 			Type="Date" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" Format="yyyy-MM-dd" />
	    <C Name="APPROVAL_STATUS" 	Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
	    <C Name="RJCTBILL_NDX" 		Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
	    <C Name="DEL_YN" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
	    <C Name="DEL_ID" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
	    <C Name="DEL_DT" 			Type="Date" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" Format="yyyy-MM-dd" />
	</Cols>

	<!-- Grid Paging Setting Area Start  -->	
	<Pager Visible="0" CanHide="0"/>
	
	<Solid>
		<I  id="PAGER"	Cells="NAV,LIST,ONE,GROUP"	Space="4"
			NAVType="Pager"
			LISTType="Pages"	LISTRelWidth="1"	LISTAlign="left"	LISTLeft="10"
			ONEType="Bool"		ONEFormula="Grid.AllPages?0:1" 			ONECanEdit="1"	ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"
		/>
	</Solid>
	<!-- Grid Paging Setting Area End    -->
	
	<Toolbar Space="0"	Styles="2"	Cells="Empty,Cnt,Found,이관,항목선택,새로고침,인쇄,엑셀" 
		ColumnsType="Button"
		EmptyType = "Html"  EmptyWidth = "1" Empty="        "
	    CntType="Html" CntFormula='"총 :&lt;b>"+count(7)+"&lt;/b>행"'	CntWidth='-1'	CntWrap='0'
	    FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>행'" FoundWidth = '-1' FoundWrap = '0'
		새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
		 onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
		이관Type="Html" 이관="&lt;a href='#none' title='이관' class=&quot;treeButton treeMove&quot;
			  onclick='fn_passCard(&quot;${gridId}&quot;)'>이관&lt;/a>"
		인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
		 onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
		엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
		 onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
		항목선택Type="Html" 항목선택="&lt;a href='#none' title='항목선택' class=&quot;treeButton treeSelect&quot;
		 onclick='showColumns(&quot;${gridId}&quot;,&quot;xls&quot;)'>항목선택&lt;/a>"
	/>
</Grid>