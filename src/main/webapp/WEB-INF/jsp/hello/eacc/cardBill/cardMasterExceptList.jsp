<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" %>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="aspn.com.common.util.Utility" %>
<%--
  Class Name : cardMasterExceptList.jsp
  Description : 법인카드 사용내역 제외 현황 목록
  Modification Information
 
          수정일       		수정자                   수정내용
    -------    --------    ---------------------------
    2017.08.23  정순주              최초 생성

    author   : 정순주
    since    : 2017.08.23
--%>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script src="/js/eacc/eaccCommon.js"></script>
<script type="text/javascript" >
var gubunDiv = '〔';
$(document).ready(function(){
	
	$('#searchVal').keydown(function(key){
		<%-- 승인ㅂ전호 검색 Enter Key Down Event --%>
		if(key.keyCode == 13){
			fn_searchData();
		}
	});
	
	<%-- 데이터 URL --%>
	var parameters = "startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val() + "&searchVal=" + $("#searchVal").val();
	var url = "/hello/eacc/cardBill/getCardMasterExceptData.do?" + parameters;
	$("#CardMasterExceptList").find('bdo').attr("Data_Url", url);
	
	<%-- 그리드 load 이벤트 핸들러 --%>
	Grids.OnReady = function(grid){
	};
	
	Grids.OnClick = function(grid, row, col){
		if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar"){
			return;
		}
	
	};	
});

function fn_reloadGrid(){
	<%-- 그리드 새로고침 --%>
	Grids.CardMasterExceptList.ReloadBody();
}

function fn_searchData() {
	<%-- 날짜 검색 --%>
	var parameters = "startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val() + "&searchVal=" + $("#searchVal").val();
	Grids.CardMasterExceptList.Source.Data.Url = "/hello/eacc/cardBill/getCardMasterExceptData.do?" + parameters;
	Grids.CardMasterExceptList.ReloadBody();
}

function fn_cancelDel(gridNm){
	<%-- 제외 해제 --%>
	var cardNum = "";
	var authNum = "";
	var cardNdx = "";
	
	if(gridNm == "CardMasterExceptList"){
		var selRow = Grids.CardMasterExceptList.GetSelRows();
		if(selRow.length == 0){
			alert("제외 해제할 사용내역을 선택하세요.");
			return;
		}else{
			for(var i = 0 ; i < selRow.length ; i++) {
				if(cardNdx == ""){
					cardNum = selRow[i].CARD_NUM;
					authNum = selRow[i].AUTH_NUM;
					cardNdx = selRow[i].CARD_NDX;
				}else{
					cardNum += gubunDiv + selRow[i].CARD_NUM;
					authNum += gubunDiv + selRow[i].AUTH_NUM;
					cardNdx += gubunDiv + selRow[i].CARD_NDX;
				}
			}
			
			if(confirm("선택하신 법인카드 사용내역을 제외 해제 하시겠습니까?")){
				$.ajax({
					type: 'POST',
					url: '/hello/eacc/cardBill/cardMasterCancelExceptAjax.do',
					async : false, 
					dataType: 'json',
					data : {CARD_NUM : cardNum, AUTH_NUM : authNum, CARD_NDX : cardNdx },
					success: function (json) {
						if(json.result == "T"){
							alert(json.resultMsg);
							fn_reloadGrid();
						}else{
							alert(json.resultMsg);
							fn_reloadGrid();
						}
					},error: function(XMLHttpRequest, textStatus, errorThrown){
						alert("[시스템오류]제외 해제 처리 중 오류가 발생하였습니다.");
						fn_reloadGrid();
					}
				});
			}
		}
	}
}

</script>
</head>
<body>
<!-- 화면 UI Area Start  -->
<div id="contents">

	<!-- Hidden Area Start -->
	<!-- Hidden Area End   -->
	
	<!-- Area Start  -->
	<div class="fl-box panel-wrap03" style="width:100%; height:100%;">
		<!-- Title Area Start  -->
		<h5 class="panel-tit mgn-l-10">법인카드 사용내역 제외현황</h5>
		<!-- Title Area End    -->
		
		<!-- inq-area-inner : s  -->
		<div class="inq-area-inner type06 ab">
			<ul class="wrap-inq">
				<li class="inq-clmn">
					<h4 class="tit-inq">승인일자</h4>
					<div class="prd-inp-wrap">
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic openCal thisMonth" readonly="readonly" gldp-id="gldp-7636599364" id="startDt"/>
							</span>
						</span>
						<span class="prd-inp">
							<span class="inner">
								<input type="text" class="inp-comm datePic openCal" readonly="readonly" gldp-id="gldp-8530641166" id="endDt" />
							</span>
						</span>
					</div>
				</li>
				<li class="inq-clmn">
					<h4 class="tit-inq">승인번호</h4>
					<div class="prd-inp-wrap">
						<input type="text" class="inp-wrap type03" id="searchVal" name="searchVal" value=""/>
					</div>
				</li>
			</ul>
			<a href="#" class="btn comm st01" onclick="fn_searchData(); return false;">검색</a>
		</div>
	
		<!-- Grid Area Start   -->
		<div class="panel-body mgn-t-25">
			<div id="CardMasterExceptList" style="height:100%;">
				<bdo	Debug="Error"
						Layout_Url="/hello/eacc/cardBill/cardMasterExceptLayout.do"
				>
				</bdo>
			</div>
		</div>
		<!-- Grid Area End     -->
	</div>
	<!-- Area End    -->
</div>
<!-- 화면 UI Area End    -->

</body>
</html>
