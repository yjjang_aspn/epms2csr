<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%--
  Class Name : cardList.jsp
  Description : 법인카드 등록 페이지
  Modification Information
  
  수정일			수정자			수정내용
  ------			------			--------
  2017-08-24		강승상			최초생성
  2017-09-06		강승상			카드등록 기능 추가
  
  author	: 강승상
  since		: 2017-08-24
--%>
<html>
<head>
	<%@ include file="/com/comHeader.jsp"%>
	<title>법인카드 등록</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/eacc/modalPopup.css">
	<script src="${pageContext.request.contextPath}/js/com/web/modalPopup.js"></script>
	<script src="${pageContext.request.contextPath}/js/eacc/eaccCommon.js"></script>
	<script type="text/javascript">
		<%-- 그리드 load 이벤트 핸들러 --%>
		$(document).ready(function() {
			Grids.OnReady = function(grid){
			};

			Grids.OnAfterSave = function (grid, result) {
				grid.ReloadBody();
			};
		});

		function addRowGrid(gridNm) {
			/*var row = null;

			row = Grids[gridNm].AddRow(null, Grids[gridNm].GetFirst(), true);
			if (gridNm == 'cardListGrid') {
				Grids[gridNm].SetValue(row, 'COMPANY_ID', '', 1);
				Grids[gridNm].SetValue(row, 'USER_CARD_NDX', '', 1);
				Grids[gridNm].SetValue(row, 'USER_ID', '', 1);
				Grids[gridNm].SetValue(row, 'DEPTNAME', '${sessionScope.ssDivisionNm}', 1);
				Grids[gridNm].SetValue(row, 'USERNAME', '${sessionScope.ssUserNm}', 1);
				Grids[gridNm].SetValue(row, 'CARD_TYPE', '', 1);
				Grids[gridNm].SetValue(row, 'CARD_NUM', '', 1);
			}*/
			popAvailCardList();
		}

		function popAvailCardList() {
			layer_open('availCardListModal');
			Grids['availCardGrid'].Source.Data.Url = "popAvailCardData.do" + getGridDataParam();
			Grids['availCardGrid'].ReloadBody();
		}

		function getGridDataParam() {
			var grid = Grids['cardListGrid']
				, tCnt = grid.LoadedCount
				, param = '?CARDNUM_ARRAY='
				, rCnt = 0;

			for(var row=grid.GetFirst(null);row;row=grid.GetNext(row)){
				rCnt++;
				param += grid.GetValue(row, 'CARD_NUM');
				if(tCnt != rCnt) {
					param += ',';
				}
			}
			return param;
		}

		function fn_addAvailCard(gridNm) {
			var grid = Grids[gridNm];
			var cardArray = [];
			var cardNum = '';
			var selrowArray = grid.GetSelRows();

			for(var i = 0; i < selrowArray.length; i++) {
				var tmp = selrowArray[i];
				console.log('CARDNUM ::: ' + tmp['CARD_NUM']);
				cardArray.push(tmp['CARD_NUM']);
				cardNum += tmp['CARD_NUM'];
				if(i < selrowArray.length-1) cardNum += ',';
			}

			$.ajax({
				url: 'cardListAjax.do'
				, method: 'post'
				, data: {'CARDNUM_ARRAY': cardNum}
				, success: function (result) {
					if(result.result == 'T') {
						alert('입력되었습니다.');
					}
				}
				, error: function (result) {
					if(result.result == 'F') {
						alert('입력이 실패하였습니다.\n' + result.error);
					}
				}
				, complete: function (result) {
					Grids['cardListGrid'].ReloadBody();
					layer_open('availCardListModal');
				}
			});
		}
	</script>
</head>
<body>
<div id="contents">
	<!-- S: body  -->
	<div class="fl-box panel-wrap03" style="width:100%; height:100%;">
		<!-- S: title  -->
		<h5 class="panel-tit mgn-l-10">법인카드 등록</h5>
		<!-- E: title    -->

		<!-- S: grid   -->
		<div class="panel-body">
			<div id="cardListGrid">
				<bdo	Debug="Error"
						Data_Url="/hello/eacc/cardBill/cardListData.do" Data_Format="String"
						Layout_Url="/hello/eacc/cardBill/cardListLayout.do"
						Upload_Url="/hello/eacc/cardBill/cardListAjax.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols" Upload_Xml="2">
				</bdo>
			</div>
		</div>
		<!-- E: grid     -->
	</div>
	<!-- E: body    -->
</div>

<!-- Layer PopUp Start -->
<!-- 등록가능 카드 -->
<div class="modal fade" id="availCardListModal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-40"> <!-- 원하는 width 값 입력 -->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">등록가능 법인카드 조회</h4>
			</div>
			<div class="modal-body" >
				<div class="modal-bodyIn">

					<div class="modal-section">
						<!-- START -->
						<div class="fl-box wd-per-100" ><!-- 원하는 비율로 직접 지정하여 사용 -->
							<!-- 트리그리드 : s -->
							<div id="availCardGrid">
								<bdo	Debug="Error"
										Layout_Url="popAvailCardLayout.do"
								>
								</bdo>
							</div>
							<!-- 트리그리드 : e -->
						</div>
						<!-- END -->
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- 코스트센터 -->
<!-- Layer PopUp End -->
</body>
</html>
