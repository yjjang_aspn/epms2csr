<?xml version="1.0" encoding="UTF-8"?>
<%@ page contentType="text/xml;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : popAvailCardLayout.jsp
  Description : 법인카드 등록 - 사용가능 카드 팝업 페이지
  Modification Information
  
  수정일			수정자			수정내용
  ------			------			--------
  2017-09-05		강승상			최초생성
  
  author	: 강승상
  since		: 2017-09-05
--%>

<c:set var="gridId" value="availCardGrid" />
<Grid>
	<Cfg	id="${gridId}"
			AcceptEnters   = "1" Calculated = "1"  CalculateSelected = "1"      DateStrings = "2" Deleting  = "0"
			Dragging       = "0" Editing    = "1"  EnterMode         = "4"      Filtering   = "1" IdChars   = "0123456789"
			InEditMode     = "2" MaxPages   = "20" NoFormatEscape    = "1"      NoHScroll   = "0" NoVScroll = "0"
			NumberId       = "1" SafeCSS    = '1'  Selecting		 = "1"
			SelectingCells = "0" SortIcons  = "0"  Style             = "Office" SuppressCfg = "1"
			Paging		   = "2" AllPages	= "0"  PageLength		 = "20"
			CopySelected   = "0" CopyFocused= "1"  CopyCols="0"		  			ExportFormat="xls" ExportCols="0"
	/>

	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>

	<!-- Grid Header Setting Area Start  -->
	<Header	id="Header"	Align="center"
<%--			HEADER			= "HEADER_NAME"	--%>
				CARD_NUM		= "카드번호"
			   	BANK_NAME		= "카드사명"
			   	CARD_NAME		= "발급자명"
			   	USE_STATUS		= "카드상태"
			   	TYPE			= "카드사용구분"
			   	USER_ID			= "소유자ID"
			   	USER_NM			= "실소유자"
	/>
	<!-- Grid Header Setting Area End    -->

	<!-- Fileter Setting Area Start      -->
	<Head>
		<Filter	id = "Filter" CanFocus = "1"/>
	</Head>
	<!-- Fileter Setting Area End        -->

	<Cols>
		<%--
		<C Name="colName"		Type="Text"	 RelWidth="number"  Align="left|right|center" CanEdit="0|1" Visible="0|1" />
		<C Name="colName"		Type="Enum"   RelWidth="number" Align="left|right|center" Enum="사용|미사용" EnumKeys="N|Y" />
		--%>
		<C Name="CARD_NUM"		Type="Text"	 RelWidth="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="BANK_NAME"		Type="Text"	 RelWidth="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="CARD_NAME"		Type="Text"	 RelWidth="100"  Align="center" CanEdit="0" Visible="1" />
		<C Name="USE_STATUS"	Type="Enum"	 RelWidth="100"	 Align="center" Enum="|가능|불가" EnumKey="|가능|불가" CanEdit="0" Visible="1"/>
		<C Name="TYPE"			Type="Enum"	 RelWidth="100"	 Align="center" <tag:enum codeGrp="USERCARD_TYPE" /> CanEdit="0" Visible="1"/>
		<C Name="USER_ID"		Type="Text"	 RelWidth="100"  Align="center" CanEdit="0" Visible="0" />
		<C Name="USER_NM"		Type="Text"	 RelWidth="100"  Align="center" CanEdit="0" Visible="1" />
	</Cols>

	<!-- Grid Paging Setting Area Start  -->
	<Pager Visible = "0"/>

	<Solid>
		<I id = "PAGER" Cells = "NAV,LIST,ONE,GROUP" Space = "4"
		   NAVType	     = "Pager"
		   LISTType	     = "Pages" LISTAlign   = "left" LISTLeft   = "10"                LISTRelWidth  = "1"
		   ONEType       = "Bool"  ONECanEdit  = "1"    ONEFormula = "Grid.AllPages?0:1" ONELabelRight = "페이지단위로보임"
		   ONEOnChange = "Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
		   GROUPCanFocus = "0"
		/>
	</Solid>
	<!-- Grid Paging Setting Area End    -->

	<Toolbar	Space="0"	Styles="1"	Cells="Empty,Cnt,추가,인쇄,엑셀"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType = "Button"
				CntType = "Html" CntFormula = '"총 : &lt;b>"+count(7)+"&lt;/b>행"' CntWidth = '-1' CntWrap = '0'
				추가Type = "Html" 추가 = "&lt;a href='#none' title='추가' class=&quot;defaultButton01 icon add&quot;
							       onclick='fn_addAvailCard(&quot;${gridId}&quot;)'>추가&lt;/a>"
				새로고침Type = "Html" 새로고침 = "&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
									  onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type = "Html" 인쇄 = "&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
									  onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
				엑셀Type = "Html" 엑셀 = "&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
									  onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>

</Grid>