<?xml version="1.0" encoding="UTF-8"?>
<%@ page contentType="text/xml;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : cardListLayout.jsp
  Description : 법인카드 등록 페이지 레이아웃
  Modification Information

  수정일			수정자			수정내용
  ------			------			--------
  2017-08-24		강승상			최초생성
  2017-09-06		강승상			카드 등록 추가 관련 레이어 수정

  author	: 강승상
  since		: 2017-08-24
--%>
<c:set var="gridId" value="cardListGrid" />
<Grid>
	<Cfg	id="${gridId}"
			AcceptEnters   = "1" Calculated = "1"  CalculateSelected = "1"      DateStrings = "2" Deleting  = "1"
			Dragging       = "0" Editing    = "1"  EnterMode         = "4"      Filtering   = "1" IdChars   = "0123456789"
			InEditMode     = "2" MaxPages   = "20" NoFormatEscape    = "1"      NoHScroll   = "0" NoVScroll = "0"
			NumberId       = "1" SafeCSS    = '1'  Selecting		 = "0"
			SelectingCells = "0" SortIcons  = "0"  Style             = "Office" SuppressCfg = "1"
			Paging		   = "2" AllPages	= "0"  PageLength		 = "20"
			CopySelected   = "0" CopyFocused= "1"  CopyCols			 = "0"		ExportFormat="xls" ExportCols="0"
	/>

	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>

	<!-- Grid Header Setting Area Start  -->
	<Header	id="Header"	Align="center"
			<%--HEADER			= "HEADER_NAME"--%>
				COMPANY_ID 		= "COMPANY_ID"
				USER_CARD_NDX 	= "USER_CARD_NDX"
				USER_ID 		= "사번"
				USERNAME 		= "이름"
				DEPTNAME 		= "부서명"
				TYPE 			= "카드세부구분"
				CARD_NUM 		= "법인카드번호"
	/>
	<!-- Grid Header Setting Area End    -->

	<!-- Fileter Setting Area Start      -->
	<Head>
		<Filter	id = "Filter" CanFocus = "1"/>
	</Head>
	<!-- Fileter Setting Area End        -->

	<Cols>
		<!--
		<C Name="colName"		Type="Text"	 RelWidth="number"  Align="left|right|center" CanEdit="0|1" Visible="0|1" />
		<C Name="colName"		Type="Enum"   RelWidth="number" Align="left|right|center" Enum="사용|미사용" EnumKeys="N|Y" />
		-->
		<C Name="COMPANY_ID"		Type="Text"	 RelWidth="0"  Align="center" CanEdit="0" Visible="0" />
		<C Name="USER_CARD_NDX"		Type="Text"	 RelWidth="0"  Align="center" CanEdit="0" Visible="0" />
		<C Name="USER_ID"			Type="Text"	 RelWidth="0"  Align="center" CanEdit="0" Visible="0" />
		<C Name="DEPTNAME"			Type="Text"	 RelWidth="30"  Align="center" CanEdit="0" Visible="1" />
		<C Name="USERNAME"			Type="Text"	 RelWidth="30"  Align="center" CanEdit="0" Visible="1" />
		<%--<C Name="TYPE"		Type="Enum"   RelWidth="30" Align="CENTER" Enum="|개인|부서공용|회사공용" EnumKeys="|1|2|3" CanEdit="0"/>--%>
		<C Name="TYPE"		Type="Enum"   RelWidth="30" Align="CENTER" <tag:enum codeGrp="USERCARD_TYPE" /> CanEdit="0"/>
		<C Name="CARD_NUM"			Type="Text"	 RelWidth="70"  Align="center" CanEdit="0" Visible="1" />

	</Cols>

	<!-- Grid Paging Setting Area Start  -->
	<Pager Visible = "0"/>

	<Solid>
		<I id = "PAGER" Cells = "NAV,LIST,ONE,GROUP" Space = "4"
		   NAVType	     = "Pager"
		   LISTType	     = "Pages" LISTAlign   = "left" LISTLeft   = "10"                LISTRelWidth  = "1"
		   ONEType       = "Bool"  ONECanEdit  = "1"    ONEFormula = "Grid.AllPages?0:1" ONELabelRight = "페이지단위로보임"
		   ONEOnChange = "Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
		   GROUPCanFocus = "0"
		/>
	</Solid>
	<!-- Grid Paging Setting Area End    -->

	<Toolbar	Space="0"	Styles="1"	Cells="Empty,Cnt,추가,저장,새로고침,인쇄,엑셀"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType = "Button"
				CntType = "Html" CntFormula = '"총 : &lt;b>"+count(7)+"&lt;/b>행"' CntWidth = '-1' CntWrap = '0'
				FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>행'" FoundWidth = '-1' FoundWrap = '0'
				DelType = "Html" DelFormula = 'var cnt=count("Row.Deleted>0",7);return cnt?"삭제된 행 :&lt;b>"+cnt+"&lt;/b>":""' DelWidth = '-1' DelWrap = '0'
				추가Type = "Html" 추가 = "&lt;a href='#none' title='추가' class=&quot;defaultButton01 icon add&quot;
							       onclick='addRowGrid(&quot;${gridId}&quot;)'>추가&lt;/a>"
				저장Type = "Html" 저장 = "&lt;a href='#none' title='저장' class=&quot;defaultButton01 icon submit&quot;
								onclick='saveGrid(&quot;${gridId}&quot;)'>저장&lt;/a>"
				새로고침Type = "Html" 새로고침 = "&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
									  onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type = "Html" 인쇄 = "&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
									  onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
				엑셀Type = "Html" 엑셀 = "&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
									  onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>

</Grid>