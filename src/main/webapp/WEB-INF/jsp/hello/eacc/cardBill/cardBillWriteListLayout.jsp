<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %>
<%--
  Class Name : cardBillWriteListLayout.jsp
  Description : 법인카드 전표 현황 목록 그리드 레이아웃
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.08.09    정순주              최초 생성

    author   : 정순주
    since    : 2017.08.09
--%>

<?xml version="1.0" encoding="UTF-8"?>
<c:set var="gridId" value="CardBillWriteList"/>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"  SortIcons="0"       Calculated="1"      CalculateSelected ="1"  NoFormatEscape="1"
			NumberId="1"          DateStrings="2"     SelectingCells="0"  AcceptEnters="1"        Style="Office"
			InEditMode="2"        SafeCSS='1'         NoVScroll="0"       NoHScroll="0"           EnterMode="4"
			Filtering="1"         Dragging="0"        Selecting="0" 	  Deleting="0" 			  Editing ="1"
			CopySelected="0"	  CopyFocused="1"     CopyCols="0"		  TabStop="1"  			  PasteFocused="3"
			ExportFormat="xls"	  ExportCols="0"
	/>
	<Cfg Validate="All" ValidateMessage="There are errors in grid!&lt;br>Data cannot be saved"/>
	<Cfg Paging="2" PageLength="30" PageMin="2" MaxPages="10" SuppressCfg="1"/>
	
	<Header	id="Header"	Align="center"	NoEscape="1"	Spanned="1"
			COMPANY_ID			= "회사코드"
			USER_ID             = "작성자"
			ABILITY				= ""
			CARDBILL_NDX        = "법인카드 전표 KEY"
			ERPBILLYEAR         = "회계년도"
			ERPBILL             = "전표번호"
			BILL_STATUS         = "전표상태"
			GW_STATUS           = "결재상태"
			CARD_NDX			= "법인카드 마스터 KEY"
		  	CARD_NO				= "카드번호"
		  	CARD_APPRNO			= "승인번호"
		  	CARD_APPRDT			= "승인일시"
		  	CARD_AQUIDT			= "매입일시"
		  	CORPORATION_NO 		= "가맹점 사업자번호"
			CURRENCY_UNIT       = "통화코드"
			RATE                = "환율"
			TOT_AMOUNT			= "총금액"			 
			AMT_AMOUNT          = "공급가액"
			VAT_AMOUNT          = "부가세"
			SER_AMOUNT			= "봉사료"
			FRE_AMOUNT			= "국내건면세금액"
			BOOK_DT             = "전기일"
			EVIDENCE_DT         = "증빙일"
			WORKPLACE_CD        = "부가세사업장"
			WORKPLACE_NM        = "부가세사업장"
			ACCGUBUN       		= "계정구분"
			TAX_CD              = "세금코드"
			PURCHASEOFFI_CD     = "구매처코드"
			PURCHASEOFFI_NM     = "구매처명"
			BANK_CD             = "은행코드"
			BANK_NM             = "은행명"
			PAYEE_CD            = "수취인코드"
			PAYEE_NM            = "수취인명"
			ACCOUNT_NO     		= "계좌번호"
			ACCOUNT_HOLDER      = "예금주명"
			PAY_TERM            = "지급조건"
			PAYDUE_DT           = "지급예정일"
			ERR_MSG             = "오류메시지"
			REG_DT              = "작성일시"
			UPT_USERID          = "수정자"
			UPT_DT              = "수정일시"
			DEL_YN              = "삭제여부"
			DEL_USERID          = "삭제자"
			DEL_DT              = "삭제일시"
			RJCTBILL_NDX        = "반려/승인취소 전표 NDX (구분자)"
			ADD_PRSN            = "추가직원ID"
			ADD_PRSN_NM         = "추가직원명"
			ATTACH_GRP_NO  		= "첨부파일"
			REPARKING_YN		= "재임시전표 생성여부"
	/>

	<Head>
		<Filter	id="Filter"			CanFocus="1"/>
	</Head>
	
	<I  Kind='Filter'	noticeTrgtRange='1'	noticeTypeRange='1'
		subjectResultMask=""	subjectResultText=""
		contentsResultMask=""	contentsResultText=""
	/>

	<Solid>
		<I  id="PAGER"	Cells="NAV,LIST,ONE,GROUP"	Space="4"
			NAVType="Pager"
			LISTType="Pages"	LISTRelWidth="1"	LISTAlign="left"	LISTLeft="10"
			ONEType="Bool"		ONEFormula="Grid.AllPages?0:1" 			ONECanEdit="1"	ONELabelRight="페이지단위로보임"
			ONEOnChange="Grid.AllPages = !Value; Grid.OnePage = Value?7:0; Grid.RenderBody();"
			GROUPCanFocus="0"
		/>
	</Solid>
	
	<Pager Visible="0" CanHide="0"/>
	
	<Cols>
		<C Name="COMPANY_ID" 		Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="USER_ID" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="ABILITY"			Type="Enum" RelWidth="50"  CanEdit="0" 
									EmptyValue="" Enum="||&lt;div style='background:10px 0px url(/images/icon/web/dltIcon.png) no-repeat; background-size: 15px; height:15px;'/&gt;&lt;/div&gt;" EnumKeys="||1"/>
		<C Name="CARDBILL_NDX" 		Type="Text" RelWidth="50" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="ERPBILLYEAR" 		Type="Text" RelWidth="80" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="ERPBILL" 			Type="Text" RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="BILL_STATUS" 		Type="Enum" RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1"
									EmptyValue="선택" <tag:enum codeGrp="BILL_STATUS" /> />
		<C Name="GW_STATUS" 		Type="Enum" RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1"
								  	EmptyValue="선택" <tag:enum codeGrp="GW_STATUS" /> />
		<C Name="CARD_NDX"			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
	  	<C Name="CARD_NO"			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
	  	<C Name="CARD_APPRNO"		Type="Text" RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
	  	<C Name="CARD_APPRDT"		Type="Date" RelWidth="150" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd HH:MM:ss" />
	  	<C Name="CARD_AQUIDT"		Type="Date" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" Format="yyyy-MM-dd HH:MM:ss" />
	  	<C Name="CORPORATION_NO"	Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="1" />
		<C Name="REG_DT" 			Type="Date" RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		<C Name="BOOK_DT" 			Type="Date" RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		<C Name="EVIDENCE_DT" 		Type="Date" RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1" Format="yyyy-MM-dd" />
		<C Name="CURRENCY_UNIT" 	Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="RATE" 				Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="right" CanExport="0" />
		<C Name="TOT_AMOUNT" 		Type="Int" RelWidth="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="AMT_AMOUNT" 		Type="Int" RelWidth="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="VAT_AMOUNT" 		Type="Int" RelWidth="100" CanEdit="0" Visible="1" Align="right" CanExport="1" Format="#,##0"	ExportStyle='mso-number-format:"#,##0";' />
		<C Name="WORKPLACE_CD" 		Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="WORKPLACE_NM" 		Type="Text" RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="ACCGUBUN" 			Type="Enum" RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1"
									EmptyValue="선택" <tag:enum codeGrp="ACCGUBUN" /> />
		<C Name="TAX_CD" 			Type="Enum" RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1"
									EmptyValue="선택" <tag:taxEnum billGb="CARD" /> />
		<C Name="ADD_PRSN" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="ADD_PRSN_NM" 		Type="Text" RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="ATTACH_GRP_NO" 	Type="Icon" RelWidth="80" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<!-- 구매처 -->
		<C Name="PURCHASEOFFI_CD" 	Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="PURCHASEOFFI_NM" 	Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="BANK_CD" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="BANK_NM" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="PAYEE_CD" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="PAYEE_NM" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="ACCOUNT_NO" 		Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="ACCOUNT_HOLDER" 	Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="PAY_TERM" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="PAYDUE_DT" 		Type="Date" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" Format="yyyy-MM-dd" />
		<!-- 구매처 -->
		<C Name="ERR_MSG" 			Type="Text" RelWidth="100" CanEdit="0" Visible="1" Align="center" CanExport="1" />
		<C Name="UPT_USERID" 		Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="UPT_DT" 			Type="Date" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" Format="yyyy-MM-dd" />
		<C Name="DEL_YN" 			Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="DEL_USERID" 		Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="DEL_DT" 			Type="Date" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" Format="yyyy-MM-dd" />
		<C Name="RJCTBILL_NDX" 		Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
		<C Name="REPARKING_YN" 		Type="Text" RelWidth="100" CanEdit="0" Visible="0" Align="center" CanExport="0" />
	</Cols>
	
	<Toolbar Space="0"	Styles="2"	Cells="Empty,Cnt,Found,항목선택,새로고침,인쇄,엑셀"
			 ColumnsType="Button"
			 EmptyType = "Html"  EmptyWidth = "1" Empty="        "
             CntType="Html"	CntFormula='"총 :&lt;b>"+count(7)+"&lt;/b>행"'	CntWidth='-1'	CntWrap='0'
             FoundType = "Html" FoundFormula = "Grid.FilterCount==null ? '' : 'Found : &lt;b>'+Grid.FilterCount+'&lt;/b>행'" FoundWidth = '-1' FoundWrap = '0'
		     상신Type ="Html" 상신="&lt;a href='#none' title='상신' class=&quot;treeButton treeReport&quot;
			  onclick='apprEtcBill(); return false;'>상신&lt;/a>"
		     새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
			  onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
			 인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
			  onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
			 엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
			  onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
			 항목선택Type="Html" 항목선택="&lt;a href='#none' title='항목선택' class=&quot;treeButton treeSelect&quot;
			  onclick='showColumns(&quot;${gridId}&quot;,&quot;xls&quot;)'>항목선택&lt;/a>"
	/>
</Grid>