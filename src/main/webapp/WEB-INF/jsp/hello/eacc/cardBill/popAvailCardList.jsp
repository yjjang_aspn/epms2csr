<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%--
  Class Name : popAvailCardList.jsp
  Description : 법인카드 등록 - 등록 가능 법인카드 리스트 페이지
  Modification Information
  
  수정일			수정자			수정내용
  ------			------			--------
  2017-09-05		강승상			최초생성
  
  author	: 강승상
  since		: 2017-09-05
--%>
<html>
<head>
	<title>등록 가능 법인카드 리스트</title>
</head>
<body>
<div id="contents">
	<!-- S: body  -->
	<div class="fl-box panel-wrap01" style="width: 100%;">
		<!-- S: title  -->
		<h5 class="panel-tit">등록 가능 법인카드</h5>
		<!-- E: title    -->

		<!-- S: grid   -->
		<div class="panel-body">
			<div id="availCardGrid">
				<bdo	Debug="Error"
						Data_Url="/hello/eacc/manager/availCardData.do" Data_Format="String"
						Layout_Url="/hello/eacc/manager/availCardLayout.do"
						Upload_Url="/hello/eacc/manager/availCardAjax.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols" Upload_Xml="2">
				</bdo>
			</div>
		</div>
		<!-- E: grid     -->
	</div>
	<!-- E: body    -->
</div>
</body>
</html>
