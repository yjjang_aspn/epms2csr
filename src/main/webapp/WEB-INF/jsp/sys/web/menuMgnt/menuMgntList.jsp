<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%--
	Class Name	: menuMgntList.jsp
	Description : [메뉴관리] : 화면
    author		: 서정민
    since		: 2018.05.09
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.09	 서정민		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<script type="text/javascript">

<%-- 전역변수 --%>
var focusedRow = null;		<%-- Focus Row : [메뉴] --%>
var lastChildRow = null;	<%-- Last Child Row : [메뉴] --%>

Grids.OnDataReceive = function(grid,source){
	if(grid.id == "MenuMgntList"){
		for(var row=grid.GetFirst(null);row;row=grid.GetNext(row)){
			if(row.DEL_YN == "Y"){
				grid.SetAttribute(row, "DEL_YN", "Class", "gridStatus4");
			}
			else if(row.DEL_YN == "N"){
				grid.SetAttribute(row, "DEL_YN", "Class", "gridStatus3");
			}
		}	
	}
}

Grids.OnClick = function(grid, row, col) {
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "MenuMgntList") {	<%-- 1. [메뉴] 클릭시 --%>
	
		<%-- 1-1. 포커스 --%>
		if(focusedRow != null){
			grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow = row;
		lastChildRow = row.lastChild;
		
	}
};

function fnChangeMenuCd(grid, row, col){
	var i=1;
	var lineRow = grid.GetFirst(row);
	if(!(lineRow == null || lineRow == 'undefined')){
		do{
			
			grid.SetValue(lineRow, "UPPR_MENU_CD", row.MENU_CD, 1);
			lineRow = lineRow.nextSibling;
			i++;
			
		}while(!(lineRow == null || lineRow == 'undefined'));
	}	
}

<%-- 활성여부 변경시 --%>
function fnChangeSts(grid,row,col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return 
	}
	
	if(row.DEL_YN == "Y"){
		grid.SetAttribute(row, "DEL_YN", "Class", "gridStatus4");
	}
	else if(row.DEL_YN == "N"){
		grid.SetAttribute(row, "DEL_YN", "Class", "gridStatus3");
	}
}

<%-- [추가] --%>
function addRowGrid(gridNm) {
	
	var row = null;
	var i = 0;
	var lastParent = 1;
	if(gridNm == "MenuMgntList") {
		if(focusedRow == null || focusedRow == 'undefined'){
			row = Grids[gridNm].AddRow(null, null, true);
			
			Grids[gridNm].SetValue(row, "DEL_YN", "N", 1);								//삭제여부
			Grids[gridNm].SetValue(row, "MENU_LVL", "1", 1);							//메뉴레벨
			for(var r =Grids[gridNm].GetFirst(); r; r=Grids[gridNm].GetNextSibling(r)){
				i++;
			}
			Grids[gridNm].SetValue(row, "SORT_ORD", i, 1);
		}else{
			if(focusedRow.MENU_LVL < 3){
				row = Grids[gridNm].AddRow(focusedRow, null, true);
				
				Grids[gridNm].SetValue(row, "WORK_GRP", focusedRow.WORK_GRP, 1);		//그룹코드
				Grids[gridNm].SetValue(row, "WORK_CD", focusedRow.WORK_CD, 1);			//하위그룹코드
				Grids[gridNm].SetValue(row, "UPPR_MENU_CD", focusedRow.MENU_CD, 1);		//상위메뉴코드
				Grids[gridNm].SetValue(row, "DEL_YN", "N", 1);							//삭제여부
				Grids[gridNm].SetValue(row, "MENU_LVL", focusedRow.MENU_LVL+1, 1);		//메뉴레벨
				if(lastChildRow == null || lastChildRow == 'undefined'){				//출력순서
					Grids[gridNm].SetValue(row, "SORT_ORD", "1", 1);					
				}else{
					Grids[gridNm].SetValue(row, "SORT_ORD", lastChildRow.SORT_ORD+1, 1);
				}
				lastChildRow = row;
			}
		}
	}
}

<%-- 그리드 드래그 --%>
Grids.OnRowMove = function (grid, row, oldparent, oldnext){
	
	if(grid.id == "MenuMgntList"){
		var uppr_menu_cd;
		var menu_lvl;
		
		if(row.parentNode.tagName != 'I'){	<%-- 1레벨로 이동할 경우 --%>
			uppr_menu_cd = "";
			menu_lvl = 1;
		}
		else {								<%-- 2레벨 이하로 이동할 경우 --%>
			uppr_menu_cd = row.parentNode.MENU_CD;
			menu_lvl = row.parentNode.MENU_LVL+1;
		}
		
		<%-- 1. 부모카테고리  ID --%>
		grid.SetValue(row, "UPPR_MENU_CD", uppr_menu_cd, 1);
		
		<%-- 2. 레벨 --%>
		grid.SetValue(row, "MENU_LVL", menu_lvl, 1);
		
		var lineRow = grid.GetFirst(row);
		var endRow = grid.GetLast(row);
		
		if(!(lineRow == null || lineRow == 'undefined')){
			var gap = menu_lvl - grid.GetValue(lineRow, "MENU_LVL") + 1;
			
			for(lineRow; lineRow; lineRow = grid.GetNextVisible(lineRow)){
				
				grid.SetValue(lineRow, "MENU_LVL", grid.GetValue(lineRow, "MENU_LVL")+gap, 1);
				
				if(grid.GetValue(lineRow, "MENU_CD") == grid.GetValue(endRow, "MENU_CD")){
					break;
				}
			}
		}
		
		<%-- 3. 출력순서 --%>
		<%-- 3-1. 이동 이전 부모 이하 라인의 출력순서 변경 --%>
		var i =1;
		var lineRow = grid.GetFirst(oldparent);
		if(!(lineRow == null || lineRow == 'undefined')){
			do{
				grid.SetValue(lineRow, "SORT_ORD", i, 1);
				lineRow = lineRow.nextSibling;
				i++;
				
			}while(!(lineRow == null || lineRow == 'undefined'));
		}
		<%-- 3-2. 이동 이후 부모 이하 라인의 출력순서 변경 --%>
		i=1;
		var lineRow2 = grid.GetFirst(row.parentNode);
		if(!(lineRow2 == null || lineRow2 == 'undefined')){
			do{
				
				grid.SetValue(lineRow2, "SORT_ORD", i, 1);
				lineRow2 = lineRow2.nextSibling;
				i++;
				
			}while(!(lineRow2 == null || lineRow2 == 'undefined'));
		}	
	}
};

<%-- Validation --%>
function dataValidation(gridNm){
	var cnt = 0;
	var chkCnt = 0;
	var grid = Grids[gridNm];
	
	if(gridNm == "MenuMgntList"){
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){  
			if(row.Changed || row.Added){
				
				cnt++;
				
				<%-- 메뉴명 --%>
				var MENU_NM  = grid.GetValue(row, "MENU_NM");
				if(MENU_NM==''){
					chkCnt++;
					grid.SetAttribute(row,"MENU_NM","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"MENU_NM","Color","#FFFFAA",1);
				}
				
				<%-- 그룹코드 --%>
				var WORK_GRP  = grid.GetValue(row, "WORK_GRP");
				if(WORK_GRP==''){
					chkCnt++;
					grid.SetAttribute(row,"WORK_GRP","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"WORK_GRP","Color","#FFFFAA",1);
				}
				
				<%-- 하위그룹코드 --%>
				var WORK_CD  = grid.GetValue(row, "WORK_CD");
				if(WORK_CD==''){
					chkCnt++;
					grid.SetAttribute(row,"WORK_CD","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"WORK_CD","Color","#FFFFAA",1);
				}
				
				<%-- 메뉴코드 --%>
				var MENU_CD  = grid.GetValue(row, "MENU_CD");
				if(MENU_CD==''){
					chkCnt++;
					grid.SetAttribute(row,"MENU_CD","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"MENU_CD","Color","#FFFFAA",1);
				}
			}
		}
	}
	
	<%-- 추가 및 변경된 데이터가 없을 경우 --%>
	if(cnt == 0){
		return false;
	}
	
	<%-- 필수값 체크 --%>
	if(chkCnt == 0){
		return true;
	}
	else{
		alert("필수값을 입력해주세요.");
		return false;
	}
}

<%-- [저장] 버튼 : 메뉴 저장 --%>
function fnSave(gridNm){
	
	<%-- 유효성 체크 --%>
    if(!dataValidation(gridNm)){
    	return;
    }
	
	if(Grids[gridNm].ActionValidate()){
		if(gridNm == "MenuMgntList"){
			if(confirm("메뉴 정보를 저장하시겠습니까?")){
	
				Grids[gridNm].ActionSave();
	
			}
		}
	}
}

<%-- 그리드 데이터 저장 후 --%> 
Grids.OnAfterSave  = function (grid){
	if(grid.id == "MenuMgntList"){
		
		grid.ReloadBody();
		
	}
};

</script>
</head>
<body>

<div id="contents">

	<div class="fl-box panel-wrap03" style="width:100%; height:100%;">
		<h4 class="panel-tit">메뉴</h4>
		<div class="panel-body">
			<div>
				<bdo	Debug="Error"
						Data_Url="/sys/menu/getMenuMgntData.do"
						Layout_Url="/sys/menu/getMenuMgntLayout.do"
						Upload_Url="/sys/menu/menuMgntEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols" Upload_Xml="2"
						Export_Data= "data" Export_Type="xls"
						Export_Url = "/com/exportGridData.jsp?File=ttt.xls&dataName=data"
						>
				</bdo>
			</div>
		</div>
	</div>	

</div>

</body>
</html>