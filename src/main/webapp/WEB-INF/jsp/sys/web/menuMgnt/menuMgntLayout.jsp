<%@ page language="java"  contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ page import="java.util.*" %>
<%@ page import="aspn.com.common.util.Utility" %>
<%--
	Class Name	: memberPrivLayout.jsp
	Description : [메뉴관리] : 메뉴 그리드 레이아웃
    author		: 서정민
    since		: 2018.05.09
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.09	 서정민		최초 생성

--%>
<c:set var="gridId" value="MenuMgntList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1"			SuppressCfg="0"	
			Editing="1"				Deleting="0"		Selecting="0"		Dragging="1"			
			Filtering="1"			StandardFilter="3"	ClearSelected="2"
			Paging="0"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="0"			PasteFocused="3"	
			MainCol="MENU_NM"    
	/>
	
	<Head>
		<Header	id="Header"	Align="center" CanExport="1" CanHide="0"
				WORK_GRP		= "그룹코드"
				WORK_CD			= "하위그룹코드"
				ORG_MENU_CD		= "기존메뉴코드"
				MENU_CD			= "메뉴코드"
				UPPR_MENU_CD	= "상위메뉴코드"
				MENU_NM			= "메뉴명"
				MENU_URL		= "URL"
				POPUP_YN		= "팝업여부"
				SORT_ORD		= "출력순서"
				DEL_YN			= "삭제여부"
				MENU_LVL		= "메뉴레벨"
	     />
		<Filter	id="Filter" CanExport="1" CanHide="0" CanFocus="1"/>
	</Head>
	
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
	
	<Cols>
		<c:choose>
			<c:when test="${userId eq 'admin' or userId eq 'samlip' or userId eq 'shany'}">
				<C Name="MENU_NM"			Type="Text"	 	Width="300"		Align="left"		CanEdit="1"/>
				<C Name="MENU_LVL"			Type="Text"	 	Width="60"		Align="center"		CanEdit="0"	Visible="0"/>
				<C Name="SORT_ORD"			Type="Text"	 	Width="60"		Align="center"		CanEdit="0"	Visible="0"/>
				<C Name="WORK_GRP"			Type="Text"	 	Width="120"		Align="center"		CanEdit="1"/>
				<C Name="WORK_CD"			Type="Text"	 	Width="120"		Align="center"		CanEdit="1"/>
				<C Name="ORG_MENU_CD"		Type="Text"	 	Width="120"		Align="center"		CanEdit="0" Visible="0"/>
				<C Name="MENU_CD"			Type="Text"		Width="120"		Align="center"		CanEdit="1" OnChange="fnChangeMenuCd(Grid,Row,Col);"/>
				<C Name="UPPR_MENU_CD"		Type="Text"		Width="120"		Align="center"		CanEdit="0"	Visible="0"/>
				<C Name="MENU_URL"			Type="Text"	 	RelWidth="220"	Align="left"		CanEdit="1"/>
				<C Name="POPUP_YN"			Type="Text"	 	Width="60"		Align="center"		CanEdit="1" Visible="0"/>
				<C Name="DEL_YN"			Type="Enum"	 	Width="60"	 	Align="left"		CanEdit="1" <tag:enum codeGrp="DEL_YN"/>  OnChange="fnChangeSts(Grid,Row,Col);"/>
			</c:when>
			<c:otherwise>
				<C Name="MENU_NM"			Type="Text"	 	Width="300"		Align="left"		CanEdit="1"/>
				<C Name="MENU_LVL"			Type="Text"	 	Width="60"		Align="center"		CanEdit="0"	Visible="0"/>
				<C Name="SORT_ORD"			Type="Text"	 	Width="60"		Align="center"		CanEdit="0"	Visible="0"/>
				<C Name="WORK_GRP"			Type="Text"	 	Width="120"		Align="center"		CanEdit="0"/>
				<C Name="WORK_CD"			Type="Text"	 	Width="120"		Align="center"		CanEdit="0"/>
				<C Name="ORG_MENU_CD"		Type="Text"	 	Width="120"		Align="center"		CanEdit="0" Visible="0"/>
				<C Name="MENU_CD"			Type="Text"		Width="120"		Align="center"		CanEdit="0"/>
				<C Name="UPPR_MENU_CD"		Type="Text"		Width="120"		Align="center"		CanEdit="0"	Visible="0"/>
				<C Name="MENU_URL"			Type="Text"	 	RelWidth="220"	Align="left"		CanEdit="0"/>
				<C Name="POPUP_YN"			Type="Text"	 	Width="60"		Align="center"		CanEdit="1" Visible="0"/>
				<C Name="DEL_YN"			Type="Enum"	 	Width="60"	 	Align="left"		CanEdit="1" <tag:enum codeGrp="DEL_YN"/>  OnChange="fnChangeSts(Grid,Row,Col);"/>
			</c:otherwise>
		</c:choose>
	</Cols>
	
	<c:choose>
		<c:when test="${userId eq 'admin' or userId eq 'samlip' or userId eq 'shany'}">
			<c:set var="Cells" value="Empty,Cnt,Del,추가,저장,새로고침,인쇄,엑셀"/>
		</c:when>
		<c:otherwise>
			<c:set var="Cells" value="Empty,Cnt,저장,새로고침,인쇄,엑셀"/>
		</c:otherwise>
	</c:choose>
	<Toolbar	Space="0"	Styles="1"	Cells="${Cells}"
				EmptyType = "Html"  EmptyWidth = "1" Empty="        "
				ColumnsType = "Button"
				CntType = "Html" CntFormula = '"총 : &lt;b>"+count(7)+"&lt;/b>행"' CntWidth = '-1' CntWrap = '0'
				DelType = "Html" DelFormula = 'var cnt=count("Row.Deleted>0",7);return cnt?"/ 삭제 : &lt;b>"+cnt+"&lt;/b>행":""' DelWidth = '-1' DelWrap = '0'
				추가Type="Html" 추가="&lt;a href='#none' title='추가' class=&quot;defaultButton icon add&quot;
					onclick='addRowGrid(&quot;${gridId}&quot;)'>추가&lt;/a>"
				저장Type="Html" 저장="&lt;a href='#none' title='저장' class=&quot;treeButton treeSave&quot;
					onclick='fnSave(&quot;${gridId}&quot;)'>저장&lt;/a>"
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type = "Html" 인쇄 = "&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
				엑셀Type = "Html" 엑셀 = "&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"	
	/>
	

</Grid>