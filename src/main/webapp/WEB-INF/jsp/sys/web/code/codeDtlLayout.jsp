<%@ page language="java"  contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"	uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: codeDtlLayout.jsp
	Description : [공통코드관리] : 공통코드 상세 그리드 레이아웃
    author		: 서정민
    since		: 2018.05.09
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.09	 서정민		최초 생성

--%>
<c:set var="gridId" value="CodeDtlList"/>
<?xml version="1.0" encoding="UTF-8"?>
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"		NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="1"	AcceptEnters="1"			Style="Office"
			SafeCSS='1'				NoVScroll="0"		NoHScroll="0"		CustomScroll="1"			SuppressCfg="0"	
			Editing="1"				Deleting="0"		Selecting="0"		Dragging="1"			
			Filtering="1"			StandardFilter="3"	ClearSelected="2"
			Paging="0"			  	PageLength="30"	  	MaxPages="10"  		AllPages = "0"
			CopyCols="0"			PasteFocused="3"		
	/>
	
	<Head>
		<Header	id="Header"	Align="center" CanExport="1" CanHide="0"
				COMCD_GRP  = "그룹코드"
				COMCD_ORD  = "순서"
				COMCD      = "공통코드"
				COMCD_NM   = "공통코드명"
				COMCD_DESC = "설명"
				COMPANY_ID = "회사코드"
				LOCATION   = "<spring:message code='epms.location.code' />"    <%  // 공장코드  %>
				DEL_YN     = "사용여부"
	     />
		<Filter	id="Filter" CanExport="1" CanHide="0" CanFocus="1"/>
	</Head>
	
	<Cols>
	<c:choose>
		<c:when test="${ssUserId eq 'admin'}">
			<C Name="COMCD_ORD"		Type="Text" Align="center" Visible="1" Width="60"  CanEdit="0" CanExport="1" CanHide="1"/>
			<C Name="COMCD_GRP"		Type="Text" Align="left"   Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="150"/>
		    <C Name="COMCD"	        Type="Text" Align="left"   Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="150"/>
		    <C Name="COMCD_NM"      Type="Text" Align="left"   Visible="1" Width="150" CanEdit="1" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="150"/>
			<C Name="COMCD_DESC"	Type="Text" Align="left"   Visible="1" Width="150" CanEdit="1" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="150"/>
			<C Name="COMPANY_ID"	Type="Text" Align="left"   Visible="1" Width="150" CanEdit="1" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="70"  MinWidth="100"/>
			<C Name="LOCATION"		Type="Text" Align="left"   Visible="1" Width="150" CanEdit="1" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="70"  MinWidth="100"/>		
		</c:when>
		<c:otherwise>
			<C Name="COMCD_ORD"		Type="Text" Align="center" Visible="1" Width="60"  CanEdit="0" CanExport="1" CanHide="1"/>
			<C Name="COMCD_GRP"		Type="Text" Align="left"   Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="150"/>
		    <C Name="COMCD"	        Type="Text" Align="left"   Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="150"/>
		    <C Name="COMCD_NM"      Type="Text" Align="left"   Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="150"/>
			<C Name="COMCD_DESC"	Type="Text" Align="left"   Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="100" MinWidth="150"/>
			<C Name="COMPANY_ID"	Type="Text" Align="left"   Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="70"  MinWidth="100"/>
			<C Name="LOCATION"		Type="Text" Align="left"   Visible="1" Width="150" CanEdit="0" CanExport="1" CanHide="1" CaseSensitive="0" WhiteChars=" " RelWidth="70"  MinWidth="100"/>		
		</c:otherwise>
	</c:choose>
	</Cols>
	
	<RightCols>
		<C Name="DEL_YN"		Type="Enum" Align="left"   Visible="1" Width="60"  CanEdit="1" CanExport="1" CanHide="1" <tag:enum codeGrp="DEL_YN"/> OnChange="fnChangeSts(Grid,Row,Col);"/>
	</RightCols>
	
	<Pager Visible="0"	CanHide="0"/>
	<Panel CanHide="0"/>
	
	<c:choose>
		<c:when test="${ssUserId eq 'admin'}">
			<c:set var="Cells" value="Empty,Cnt,Del,추가,저장,새로고침,인쇄,엑셀"/>
		</c:when>
		<c:otherwise>
			<c:set var="Cells" value="Empty,Cnt,Del,저장,새로고침,인쇄,엑셀"/>
		</c:otherwise>
	</c:choose>
	
	<Toolbar	Space="0"	Styles="1"	Cells="${Cells}"
				EmptyType = "Html"	EmptyWidth = "1" Empty="        " 
				ColumnsType = "Button"
				CntType = "Html" CntFormula = '"총 : &lt;b>"+count(7)+"&lt;/b>행"' CntWidth = '-1' CntWrap = '0'
				추가Type="Html" 추가="&lt;a href='#none' title='추가' class=&quot;defaultButton icon add&quot;
					onclick='addRowGrid(&quot;${gridId}&quot;)'>추가&lt;/a>"
				저장Type="Html" 저장="&lt;a href='#none' title='저장' class=&quot;treeButton treeSave&quot;
					onclick='fnSave(&quot;${gridId}&quot;)'>저장&lt;/a>"
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton icon refresh&quot;
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type = "Html" 인쇄 = "&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
				엑셀Type = "Html" 엑셀 = "&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"	
	/>
	
</Grid>
