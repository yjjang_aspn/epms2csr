<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tag" uri="/WEB-INF/tlds/code.tld" %> 
<%--
	Class Name	: popSyncCode.jsp
	Description : 프로필 관리
	author		: 김영환
	since		: 2018.09.18
 
	<< 개정이력(Modification Information) >>
	수정일 		수정자		수정내용
	----------  ------	---------------------------
	2018.09.18	 김영환		최초 생성

--%>

<script>
$(document).ready(function() {
	
	/* 닫기 버튼 */
	$('#closeBtn').on('click', function(){
		fnModalToggle('popSyncCode');
	});
	
	<%-- '플랜트별오더유형 SAP 동기화'버튼 클릭 --%>
	$('#orderTypeInfoBtn').on('click', function(){
		var params = "?comcdGrp=ORDER_TYPE" + "&editYn=Y";
		fnSyncOrderType(params);
	});
	
	<%-- '오브젝트유형 등록 SAP 동기화'버튼 클릭 --%>
	$('#objTypeBtn').on('click', function(){
		var params = "?comcdGrp=OBJ_TYPE" + "&editYn=Y";
		fnSyncObjType(params);
	});
	
	<%-- '오브젝트유형 등록 SAP 동기화'버튼 클릭 --%>
	$('#categoryItemBtn').on('click', function(){
		var params = "?comcdGrp=CATALOG_2" + "&editYn=Y";
		fnSyncCategoryItem(params);
	});
	
});

<%-- SAP 동기화 버튼(플랜트별오더유형 조회) --%>
function fnSyncOrderType(params){
	if(confirm("플랜트별오더유형 정보를 동기화하시겠습니까?")){
		$.ajax({
			type : 'POST',
			url : '/sys/code/updateOrderTypeInfoAjax.do',
			dataType : 'json',
			success : function(json) {
				
				if(json.E_RESULT == 'S'){
					alert("SAP동기화에 성공하였습니다.");
					Grids.CodeDtlList.Source.Data.Url = "/sys/code/getCodeDtlData.do"+params;
					Grids.CodeDtlList.ReloadBody();
				}
				else{
					alert("SAP동기화에 실패하였습니다.");
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
				setTimeout($.unblockUI, 100);
			}
		});
	}
}

<%-- SAP 동기화 버튼(오브젝트유형 조회) --%>
function fnSyncObjType(params){
	if(confirm("오브젝트유형 정보를 동기화하시겠습니까?")){
		$.ajax({
			type : 'POST',
			url : '/sys/code/updateObjTypeInfoAjax.do',
			dataType : 'json',
			success : function(json) {
				
				if(json.E_RESULT == 'S'){
					alert("SAP동기화에 성공하였습니다.");
					Grids.CodeDtlList.Source.Data.Url = "/sys/code/getCodeDtlData.do" + params;
					Grids.CodeDtlList.ReloadBody();
				}
				else{
					alert("SAP동기화에 실패하였습니다.");
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
				setTimeout($.unblockUI, 100);
			}
		});
	}
}

<%-- SAP 동기화 버튼(카테고리ITEM 조회) --%>
function fnSyncCategoryItem(params){
	if(confirm("카테고리ITEM 정보를 동기화하시겠습니까?")){
		$.ajax({
			type : 'POST',
			url : '/sys/code/updateCategoryItemAjax.do',
			dataType : 'json',
			success : function(json) {
				
				if(json.E_RESULT == 'S'){
					alert("SAP동기화에 성공하였습니다.");
					Grids.CodeDtlList.Source.Data.Url = "/sys/code/getCodeDtlData.do"+ params;
					Grids.CodeDtlList.ReloadBody();
				}
				else{
					alert("SAP동기화에 실패하였습니다.");
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert('오류가 발생 하였습니다. 잠시 후 다시 시도 해주십시오.\n' + 'errorCode : ' + textStatus);
				setTimeout($.unblockUI, 100);
			}
		});
	}
}

</script>


<div class="modal-dialog root wd-per-25 hgt-per-90">
	<div class="modal-content hgt-per-45" style="min-height:150px">
		<div class="modal-header">
		    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
		   	<h4 class="modal-title" id="modal_title">SAP 동기화</h4>
		</div>
	 	<div class="modal-body" >
           	<div class="modal-bodyIn">
				<!-- 페이지 내용 : s -->
				<!-- 사용자 정보 -->
				
					<div class="profile-wrap">
						
						<!-- top-area -->
						<div class="bottom-area">
							<div class="tb-wrap type03">
								<table class="tb-st">
									<caption></caption>
									<colgroup>
										<col width="*" />
										<col width="110px" />
									</colgroup>
									<tbody>
										<tr>
											<th>플랜트별오더유형 등록</th>
											<td>
												<a href="#none" class="btn comm st01" id="orderTypeInfoBtn">SAP 동기화</a>
											</td>
										</tr>
										<tr>
											<th>오브젝트유형 등록</th>
											<td>
												<a href="#none" class="btn comm st01" id="objTypeBtn">SAP 동기화</a>
											</td>
										</tr>
										<tr class="pwChg">
											<th>카테고리ITEM 등록</th>
											<td>
												<a href="#none" class="btn comm st01" id="categoryItemBtn">SAP 동기화</a>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<!-- e:bottom-area -->
						<div class="btn-area"  id="infoCloseBtn">
							<a href="#none" class="btn comm st02" id="closeBtn">닫기</a>
						</div>
					</div>
					<!-- e:prifile-wrap -->
				<!-- 페이지 내용 : e -->
			</div> <%-- modal-bodyIn : e --%>
		</div> <%-- modal-body : e --%>
	</div> <%-- modal-content : e --%>
</div> <%-- modal : e --%>
