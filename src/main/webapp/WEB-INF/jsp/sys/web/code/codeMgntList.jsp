<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="aspn.com.common.util.Utility" %>
<%--
	Class Name	: codeMgntList.jsp
	Description : [공통코드관리] : 화면
    author		: 서정민
    since		: 2018.05.09
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.09	 서정민		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<script type="text/javascript" >

<%-- 전역변수 --%>
var focusedRow = null;		<%-- Focus Row : [부서] --%>
var focusedRow2 = null;		<%-- Focus Row : [사용자] --%>

var doc = document;
var gridCodeInit = false;
var gridCodeDtlInit = false;
var selComcdGrp = null;

$(document).ready(function(){
	
});

Grids.OnDataReceive = function(grid,source){
	if(grid.id == "CodeList"){
		for(var row=grid.GetFirst(null);row;row=grid.GetNext(row)){
			if(row.DEL_YN == "Y"){
				grid.SetAttribute(row, "DEL_YN", "Class", "gridStatus4");
			}
			else if(row.DEL_YN == "N"){
				grid.SetAttribute(row, "DEL_YN", "Class", "gridStatus3");
			}
		}	
	}
	else if(grid.id == "CodeDtlList"){
		for(var row=grid.GetFirst(null);row;row=grid.GetNext(row)){
			if(row.DEL_YN == "Y"){
				grid.SetAttribute(row, "DEL_YN", "Class", "gridStatus4");
			}
			else if(row.DEL_YN == "N"){
				grid.SetAttribute(row, "DEL_YN", "Class", "gridStatus3");
			}
		}	
	}
}

Grids.OnClick = function(grid, row, col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}

	if(grid.id == "CodeList"){		<%-- 1. [코드 정보] 클릭시 --%>
	
		<%-- 1-1. 포커스 --%>
		if(focusedRow != null){
			grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow = row;
		focusedRow2 = null;
	
		<%-- 1.2. 코드 상세 조회 --%>
		if((row.COMCD_GRP!=null && selComcdGrp != "undefined" && selComcdGrp != "") &&
			row.Added != 1 && row.Deleted != 1) {

			selComcdGrp =  row.COMCD_GRP;
			var params = "&comcdGrp=" + row.COMCD_GRP
					   + "&editYn=Y";	<%-- 비활성도 조회 --%>

			getCodeDtlList(params);
		}
	}
	else if(grid.id == "CodeDtlList"){	<%-- 2. [코드 상세 정보] 클릭시 --%>
		<%-- 2-1. 포커스  --%>
		if(focusedRow2 != null){
			grid.SetAttribute(focusedRow2,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow2 = row;
	}
};

<%-- 드래그시 출력순서 세팅 : 드래그한 row의  sibling 의 출력순서 세팅 --%>
Grids.OnRowMove = function (grid, row, oldparent, oldnext){
	
	if(grid.id == "CodeDtlList"){
		var i =1;
		var lineRow = grid.GetFirst(oldparent);
		if(!(lineRow == null || lineRow == 'undefined')){
			do{
				grid.SetValue(lineRow, "COMCD_ORD", i, 1);
				lineRow = lineRow.nextSibling;
				i++;
				
			}while(!(lineRow == null || lineRow == 'undefined'));
		}
		
		
		i=1;
		var lineRow2 = grid.GetFirst(row.parentNode);
		if(!(lineRow2 == null || lineRow2 == 'undefined')){
			do{
				
				grid.SetValue(lineRow2, "COMCD_ORD", i, 1);
				lineRow2 = lineRow2.nextSibling;
				i++;
				
			}while(!(lineRow2 == null || lineRow2 == 'undefined'));
		}	
	}
};

function getCodeDtlList(params){

	Grids.CodeDtlList.Source.Data.Url = "/sys/code/getCodeDtlData.do?"+params;
	Grids.CodeDtlList.ReloadBody();
}

function addRowGrid(gridNm){
	var row = null;
	var i = 0;
	if(gridNm == "CodeDtlList") {
		if(selComcdGrp == null || selComcdGrp == "undefined" || selComcdGrp==""){
			alert("선택된 그룹코드가 없습니다.");
			return;
		}else{
			row = Grids[gridNm].AddRow( null, null, true);
			
			Grids[gridNm].SetValue(row, "COMCD_GRP", selComcdGrp, 1);
			Grids[gridNm].SetValue(row, "COMCD", "", 1);
			Grids[gridNm].SetValue(row, "COMCD_NM", "", 1);
			Grids[gridNm].SetValue(row, "COMCD_DESC", "", 1);
			Grids[gridNm].SetValue(row, "DEL_YN", 'N', 1);
			Grids[gridNm].SetAttribute(row,"COMCD","CanEdit","1",1);  
			
			for(var r =Grids[gridNm].GetFirst(); r; r=Grids[gridNm].GetNextSibling(r)){
				i++;
			}
			Grids[gridNm].SetValue(row, "COMCD_ORD", i, 1);
			
			Grids[gridNm].RefreshRow (row);
		}
	}else if(gridNm == "CodeList") {

		row = Grids[gridNm].AddRow( null, Grids[gridNm].GetFirst(), true);

		Grids[gridNm].SetValue(row, "COMCD_GRP", "", 1);
		Grids[gridNm].SetValue(row, "COMCD_GRP_NM", "", 1);
		Grids[gridNm].SetValue(row, "COMCD_GRP_DESC", "", 1);
		Grids[gridNm].SetValue(row, "DEL_YN", "N", 1);
		Grids[gridNm].SetAttribute(row, "COMCD_GRP", "CanEdit", 1);
		Grids[gridNm].SetAttribute(row, "COMCD_GRP_NM", "CanEdit", 1);
		Grids[gridNm].SetAttribute(row, "COMCD_GRP_DESC", "CanEdit", 1);
	}
}

<%-- 활성여부 변경시 --%>
function fnChangeSts(grid,row,col){
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return 
	}
	
	if(row.DEL_YN == "Y"){
		grid.SetAttribute(row, "DEL_YN", "Class", "gridStatus4");
	}
	else if(row.DEL_YN == "N"){
		grid.SetAttribute(row, "DEL_YN", "Class", "gridStatus3");
	}
}

<%-- Validation --%>
function dataValidation(gridNm){
	var cnt = 0;
	var chkCnt = 0;
	var grid = Grids[gridNm];
	
	if(gridNm == "CodeList"){
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){  
			if(row.Changed || row.Added){
				
				cnt++;
				
				<%-- 그룹명 --%>
				var COMCD_GRP_NM = grid.GetValue(row, "COMCD_GRP_NM");
				if(COMCD_GRP_NM==''){
					chkCnt++;
					grid.SetAttribute(row,"COMCD_GRP_NM","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"COMCD_GRP_NM","Color","#FFFFAA",1);
				}
			}
		}
	}	
	else if(gridNm == "CodeDtlList"){
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow2);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){  
			if(row.Changed || row.Added){
				
				cnt++;
				
				<%-- 그룹코드 --%>
				var COMCD_GRP  = grid.GetValue(row, "COMCD_GRP");
				if(COMCD_GRP==''){
					chkCnt++;
					grid.SetAttribute(row,"COMCD_GRP","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"COMCD_GRP","Color","#FFFFAA",1);
				}
				
				<%-- 공통코드 --%>
				var COMCD  = grid.GetValue(row, "COMCD");
				if(COMCD==''){
					chkCnt++;
					grid.SetAttribute(row,"COMCD","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"COMCD","Color","#FFFFAA",1);
				}

				<%-- 공통코드명 --%>
				var COMCD_NM = grid.GetValue(row, "COMCD_NM");
				if(COMCD_NM==''){
					chkCnt++;
					grid.SetAttribute(row,"COMCD_NM","Color","#FF6969",1);
				}else{
					grid.SetAttribute(row,"COMCD_NM","Color","#FFFFAA",1);
				}
			}
		}
	}
	
	<%-- 추가 및 변경된 데이터가 없을 경우 --%>
	if(cnt == 0){
		return false;
	}
	
	<%-- 필수값 체크 --%>
	if(chkCnt == 0){
		return true;
	}
	else{
		alert("필수값을 입력해주세요.");
		return false;
	}
}

<%-- [저장] 버튼 : 라인정보 및 설비정보 CRUD(관리자) --%>
function fnSave(gridNm){
	
	<%-- 유효성 체크 --%>
    if(!dataValidation(gridNm)){
    	return;
    }
	
	if(Grids[gridNm].ActionValidate()){
		if(gridNm == "CodeList"){
			if(confirm("공통코드 정보를 저장하시겠습니까?")){
	
				Grids[gridNm].ActionSave();
	
			}
		}
		else if(gridNm == "CodeDtlList"){
			if(confirm("공통코드상세 정보를 저장하시겠습니까?")){

				Grids[gridNm].ActionSave();

			}
		}
	}
}

<%-- 그리드 데이터 저장 후 --%> 
Grids.OnAfterSave  = function (grid){
	if(grid.id == "CodeList"){
		
		grid.ReloadBody();
		
	} else if (grid.id == "CodeDtlList"){
		
		grid.ReloadBody();
		
	}
};

<%-- param 전달용 모달 호출 --%>
function layer_open_withParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

<%-- 모달 토글 --%>
function toggleModal(obj){
	obj.modal('toggle');
}

</script>
</head>

<body>

<div id="contents">

	<div class="fl-box panel-wrap03 leftPanelArea" style="width: 50%;">
		<h5 class="panel-tit">공통코드</h5>
		<div class="panel-body">
			<div id="CodeList">
				<bdo	Debug="Error"
						Data_Url="/sys/code/getCodeData.do" Data_Format="String"
						Layout_Url="/sys/code/getCodeLayout.do"
						Upload_Url="/sys/code/codeEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols" Upload_Xml="2">
				</bdo>
			</div>
		</div>
	</div>
	
	<div class="fl-box panel-wrap03 rightPanelArea">
		<h5 class="panel-tit mgn-l-10">공통코드 상세</h5>
		<div class="panel-body mgn-l-10">
			<div id="CodeDtlList">
				<bdo	Debug="Error"
						Layout_Url="/sys/code/getCodeDtlLayout.do"
						Upload_Url="/sys/code/codeDtlEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols" Upload_Xml="2"
						>
				</bdo>
			</div>
		</div>
	</div>

</div>

<div class="modal fade modalFocus" id="popSyncCode"   data-backdrop="static" data-keyboard="false"></div> 		<%-- 공장별 파트 통합관리 팝업 --%>
</body>
</html>
