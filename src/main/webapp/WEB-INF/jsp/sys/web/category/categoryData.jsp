<%@ page contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- <%@ page import="java.util.*" %>
<%@ page import="aspn.sys.common.util.ObjUtil" %>
<%@ page import="aspn.sys.category.model.CategoryModel" %> --%>

<Grid>
<Body>
	<B>
		<c:forEach var="item" items="${list}">
			<c:set var="nodeCnt" value="0" />
			<I 
				CATEGORY			= "${item.CATEGORY}" 			<%-- 카테고리 ID --%>
				CATEGORY_UID		= "${item.CATEGORY_UID}" 		<%-- 카테고리 명 --%>
				CATEGORY_NAME		= "${item.CATEGORY_NAME}" 		<%-- 카테고리 명 --%>
				CATEGORY_TYPE		= "${item.CATEGORY_TYPE}"		<%-- 카테고리 유형 --%>
				PARENT_CATEGORY		= "${item.PARENT_CATEGORY}"		<%-- 부모카테고리 ID --%>
				TREE				= "${item.TREE}"				<%-- 구조체 --%> 
				DEPTH 				= "${item.DEPTH}"				<%-- 레벨 --%>
				SEQ_DSP				= "${item.SEQ_DSP}"				<%-- 출력순서 --%>
				COMPANY_ID			= "${item.COMPANY_ID}"			<%-- 회사코드 --%>
				LOCATION			= "${item.LOCATION}"			<%-- 공장코드 --%>
				SUB_ROOT			= "${item.SUB_ROOT}"			<%-- 공장구분 --%>
				<c:choose>
					<c:when test="${item.CUR_LVL eq item.NEXT_LVL}">></I></c:when>
					<c:when test="${item.CUR_LVL < item.NEXT_LVL}">CanExpand='1' Expanded='1'></c:when>
					<c:when test="${item.CUR_LVL > item.NEXT_LVL}">></c:when>
					<c:otherwise>></c:otherwise>
				</c:choose>
				<c:set var="nodeCnt" value="${item.CUR_LVL - item.NEXT_LVL}" />
				<%-- <c:out value="${nodeCnt}"/> --%>
				<c:if test="${nodeCnt > 0}">
					<c:forEach var="item" begin="0" end="${nodeCnt}" step="1"></I></c:forEach>
				</c:if>
		</c:forEach>
	</B>
</Body>
</Grid>
