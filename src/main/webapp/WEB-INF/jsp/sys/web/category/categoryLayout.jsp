<%@ page language="java" contentType="text/xml" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag"	   uri="/WEB-INF/tlds/code.tld"                 %>
<%@ taglib prefix="c"	   uri="http://java.sun.com/jsp/jstl/core"      %>
<%@ taglib prefix="fn"	   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
	Class Name	: categoryLayout.jsp
	Description : 카테고리 그리드 레이아웃(수정가능)
    author		: 서정민
    since		: 2018.02.08
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.02.08	 서정민		최초 생성

--%>

<c:set var="gridId" value="CategoryList"/>
<?xml version="1.0" encoding="UTF-8"?>
<!-- 카테고리 -->
<Grid>
	<Cfg	id="${gridId}"
			IdChars="0123456789"	SortIcons="0"		Calculated="1"		CalculateSelected ="1"	NoFormatEscape="1"
			NumberId="1"			DateStrings="2"		SelectingCells="0"	AcceptEnters="1"		Style="Office"
			InEditMode="2"			SafeCSS="1"			NoVScroll="0"		NoHScroll="0"			EnterMode="0"
			Filtering="1"			Dragging="1"		Selecting="0"		Deleting="0"			Editing ="1"
			SuppressCfg="0"			StandardFilter="3"	CopyCols="0"		PasteFocused="3"		CustomScroll="1"
			MainCol="CATEGORY_NAME"
			
	/>
	
	<c:choose>
		<c:when test="${param.CATEGORY_TYPE eq 'LINE' }">
			<c:set var="CATEGORY_UID" value="기능위치"/>
			<c:set var="CATEGORY_NAME"><spring:message code='epms.category' /></c:set><!-- 라인 -->
		</c:when>
		<c:when test="${param.CATEGORY_TYPE eq 'MATERIAL' }">
			<c:set var="CATEGORY_UID" value="자재유형 코드"/>
			<c:set var="CATEGORY_NAME"><spring:message code='epms.object.type' /></c:set><!-- 자재유형 -->
		</c:when>
		<c:when test="${param.CATEGORY_TYPE eq 'WORK' }">
			<c:set var="CATEGORY_UID" value="오더유형 코드"/>
			<c:set var="CATEGORY_NAME" value="오더유형"/>
		</c:when>
		<c:otherwise>
			<c:set var="CATEGORY_UID" value="카테고리 코드"/>
			<c:set var="CATEGORY_NAME" value="카테고리명"/>
		</c:otherwise>
	</c:choose>
	<Head>
		<Header	id="Header"	Align="center" CanExport="1" CanHide="0"
				CATEGORY           = "카테고리 ID"
				CATEGORY_UID       = "${CATEGORY_UID}"
				CATEGORY_NAME      = "${CATEGORY_NAME}"
				CATEGORY_TYPE      = "카테고리 유형"
				PARENT_CATEGORY    = "부모카테고리 ID"
				TREE               = "구조체"
				DEPTH              = "레벨"
				SEQ_DSP            = "출력순서"
				COMPANY_ID         = "회사코드"
				LOCATION           = "공장코드"
				SUB_ROOT           = "공장구분"
		/>	
		<Filter	id="Filter" CanExport="1" CanHide="0" CanFocus="1"/>
	</Head>
	
	<Cols>
		<C Name="CATEGORY"			Type="Text" Align="left"   Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="CATEGORY_UID"		Type="Text" Align="left"   Visible="0" Width="100"    CanEdit="0" CanExport="1" CanHide="1"/>
	    <C Name="CATEGORY_NAME"		Type="Text" Align="left"   Visible="1" RelWidth="100" CanEdit="1" CanExport="1" CanHide="1" Cursor="hand"/>
		<C Name="CATEGORY_TYPE"		Type="Text" Align="center" Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="PARENT_CATEGORY"	Type="Text" Align="left"   Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="TREE"				Type="Text" Align="left"   Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="DEPTH"				Type="Text" Align="center" Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="SEQ_DSP"			Type="Text" Align="center" Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="COMPANY_ID"		Type="Text" Align="center" Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="LOCATION"			Type="Text" Align="center" Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
		<C Name="SUB_ROOT"			Type="Text" Align="center" Visible="0" Width="100"    CanEdit="0" CanExport="0" CanHide="0"/>
	</Cols>
	
	<c:choose>
		<c:when test="${param.CATEGORY_TYPE eq 'LINE' }">
			<c:set var="Cells" value="Empty,추가,저장,접기,항목"/>  <!-- 동기화 -->
		</c:when>
		<c:when test="${param.CATEGORY_TYPE eq 'MATERIAL' }">
			<c:set var="Cells" value="Empty,접기,추가,저장"/>  <!-- 동기화 -->
		</c:when>
		<c:when test="${param.CATEGORY_TYPE eq 'WORK' }">
			<c:set var="Cells" value="Empty,접기,추가,저장"/>
		</c:when>
		<c:otherwise>
			<c:set var="Cells" value="Empty,접기,추가,저장"/>
		</c:otherwise>
	</c:choose>
	<Toolbar	Space="0"	Styles="2"	Cells="${Cells}"
				EmptyType="Html"	EmptyWidth="1" Empty="        " 
				ColumnsType="Button"
				CntType="Html" CntFormula='"총 : &lt;b>"+count(7)+"&lt;/b>행"' CntWidth='-1' CntWrap='0'
				<c:choose>
					<c:when test="${param.CATEGORY_TYPE eq 'LINE' }">
					동기화Type="Html" 동기화="&lt;a href='#none' title='동기화' class=&quot;treeButton treeSap&quot;
						onclick='fnSyncLine()'>동기화&lt;/a>"
					</c:when>	
					<c:when test="${param.CATEGORY_TYPE eq 'MATERIAL' }">
					동기화Type="Html" 동기화="&lt;a href='#none' title='동기화' class=&quot;treeButton treeSap&quot;
						onclick='fnSyncMaterialGroup()'>동기화&lt;/a>"
					</c:when>
				</c:choose>
				추가Type="Html" 추가="&lt;a href='#none' title='추가' class=&quot;defaultButton icon add&quot;
					onclick='addRowGrid(&quot;${gridId}&quot;)'>추가&lt;/a>"
 				저장Type="Html" 저장="&lt;a href='#none' title='저장' class=&quot;treeButton treeSave&quot;
 					onclick='fnSave(&quot;${gridId}&quot;)'>저장&lt;/a>"
				항목Type="Html" 항목="&lt;a href='#none' title='항목' class=&quot;treeButton treeSelect&quot;
					onclick='showColumns(&quot;${gridId}&quot;)'>항목&lt;/p>"
				접기Type="Html" 접기="&lt;a href='#none' title='접기' id=&quot;Folding&quot; class=&quot;icon folderClose&quot;
					onclick='fnExpandAll(&quot;${gridId}&quot;)'>&lt;/a>"
				새로고침Type="Html" 새로고침="&lt;a href='#none' title='새로고침' class=&quot;defaultButton01 icon refresh&quot; 
					onclick='reloadGrid(&quot;${gridId}&quot;)'>새로고침&lt;/a>"
				인쇄Type="Html" 인쇄="&lt;a href='#none' title='인쇄' class=&quot;defaultButton icon printer&quot;
					onclick='printGrid(&quot;${gridId}&quot;)'>인쇄&lt;/a>"
	 			엑셀Type="Html" 엑셀="&lt;a href='#none' title='엑셀'class=&quot;defaultButton icon excel&quot;
					onclick='exportGrid(&quot;${gridId}&quot;,&quot;xls&quot;)'>엑셀&lt;/a>"
	/>
</Grid>
