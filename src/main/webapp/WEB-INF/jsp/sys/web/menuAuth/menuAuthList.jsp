<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag"	uri="/WEB-INF/tlds/code.tld" %>
<%--
	Class Name	: menuAuthList.jsp
	Description : [권한별 메뉴관리] : 화면
    author		: 서정민
    since		: 2018.05.09
	
	<< 개정이력(Modification Information) >>
	수정일 			수정자		수정내용
	----------  ------	---------------------------
	2018.05.09	 서정민		최초 생성

--%>

<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<script type="text/javascript">

<%-- 전역변수 --%>
var focusedRow = null;		<%-- Focus Row : [메뉴] --%>


Grids.OnRenderFinish = function(grid) {
	
	if(grid.id == "MenuList") {
		menuMgntReload($('#userPriv').val());
	}
};

Grids.OnClick = function(grid, row, col) {
	
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "MenuList") {	<%-- 1. [메뉴] 클릭시 --%>
	
		<%-- 1-1. 포커스 --%>
		if(focusedRow != null){
			grid.SetAttribute(focusedRow,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		focusedRow = row;
		
	}
};

<%-- 데이터 조회시 로딩바 출력 --%>
Grids.OnDataSend  = function(grid, source, data, func){
	if(grid.id == "MenuList"){
		if(data != ""){
			callLoadingBar();
			Grids.OnDataReceive  = function(grid, source){
				if(grid.id == "MenuList"){
					setTimeout($.unblockUI, 100);
				}
			}
		}
	}
}

Grids.OnAfterValueChanged = function(grid, row, col, type){
	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return; 
	}

	if(!(grid.GetFirst(row) == null || grid.GetFirst(row) == "undefined")){
		for(var r=grid.GetFirst(row); r; r=grid.GetNext(r)){
			if(!(r.MENU_CD == null || r.MENU_CD == "undefined" || r.MENU_CD == "")){
			 	grid.SetValue(r, "USE_YN", row.USE_YN, 1);
			}
			if(r.MENU_CD == grid.GetLast(row).MENU_CD){
				break;
			}
		 }
	}

	if(row.USE_YN=="1"){
		if(!(row.parentNode == null || row.parentNode == "undefined")){
			for(var r=row.parentNode; r; r=r.parentNode){
					if(!(r.MENU_CD == null || r.MENU_CD == "undefined" || r.MENU_CD == "")){
					grid.SetValue(r, "USE_YN", row.USE_YN, 1);
					}
			}
		}
	}
}

<%-- Validation --%>
function dataValidation(gridNm){
	var cnt = 0;
	var grid = Grids[gridNm];
	
	if(gridNm == "MenuList"){
		
		<%-- 데이터 수정 중 등록할 경우 마지막 수정값으로 저장 --%>
		Grids[gridNm].EndEdit(focusedRow);
		
		for(var row = grid.GetFirstVisible(); row; row = grid.GetNextVisible(row)){  
			if(row.Changed || row.Added){
				
				cnt++;
				
			}
		}
	}
	
	<%-- 추가 및 변경된 데이터가 없을 경우 --%>
	if(cnt == 0){
		return false;
	}
	else{
		return true;
	}
}

<%-- [저장] 버튼 : 메뉴 저장 --%>
function fnSave(gridNm){
	
	<%-- 유효성 체크 --%>
    if(!dataValidation(gridNm)){
    	return;
    }
    
	if(Grids[gridNm].ActionValidate()){
		if(gridNm == "MenuList"){
			if(confirm("권한별 메뉴 정보를 저장하시겠습니까?")){
	
				Grids[gridNm].ActionSave();
	
			}
		}
	}
}

<%-- 그리드 데이터 저장 후 --%> 
Grids.OnAfterSave  = function (grid){
	if(grid.id == "MenuList"){
		
		grid.ReloadBody();
		
	}
};

function menuMgntReload(value){
	Grids.MenuList.Source.Data.Url = "/sys/menu/getMenuData.do?userPriv=" + value;
	Grids.MenuList.ReloadBody();

}



</script>
</head>

<body>

<div id="contents">
	
	<div class="fl-box panel-wrap03" style="width:100%; height:100%;">
		<h4 class="panel-tit type03">
			권한별 메뉴
			<span class="menuSelect">
				<tag:combo codeGrp="USER_PRIV" name="userPriv" companyId="${ssCompanyId}" onChange="menuMgntReload(value);"/>
			</span>
		</h4>
		<div class="panel-body">
			<div>
				<bdo	id="MenuList" Debug="Error"
						Layout_Url="/sys/menu/getMenuLayout.do"
						Upload_Url="/sys/menu/menuEdit.do" Upload_Data="uploadData" Upload_Format="Internal" Upload_Flags="AllCols" Upload_Xml="2"
				>
				</bdo>
			</div>
		</div>
	</div>

</div>

</body>
</html>
