<%@ page language="java" pageEncoding="UTF-8"%>
<%--
  Class Name : fileUploadResult.jsp
  Description : 파일업로드결과
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.08.16    이영탁              최초 생성

    author   : 이영탁
    since    : 2017.08.16
--%>
<!DOCTYPE html>
<html>
<head>
<script>
alert("파일 등록/수정을 정상적으로 처리하였습니다.");
window.opener.uploadFileCallback('${attachModel.ATTACH_GRP_NO}');
self.close();
</script>
</head>