<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Class Name : fileUserUdateForm.jsp
  Description : 파일사용자수정폼
  Modification Information
 
     수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.08.16    이영탁              최초 생성

    author   : 이영탁
    since    : 2017.08.16
--%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script type="text/javascript" src="/js/jquery/jquery.MultiFile.js"></script>
<script type="text/javascript" src="/js/jquery/jquery-filestyle.js"></script>

<script type="text/javascript">
	
var maxFileCnt;
var fileVal = "";

$(document).ready(function(){
	var attachGrpNo = "${attachGrpNo}";
	
	if('${param.flag}' == 'edit'){
		opener.fn_reloadPage();
 		self.close();
	}
	
	if('${param.ATTACH_GRP_NO}' == ""){
		$('#ATTACH_GRP_NO').val(attachGrpNo);
	}else{
		$('#ATTACH_GRP_NO').val("${param.ATTACH_GRP_NO}");
	}
	
	
	for(var i=0; i<1; i++){				//파일 영역 추가를 고려하여 개발
		if(i == 0){
			maxFileCnt = 1;
		}
		
		$('#attachFile'+i).append('<input type="file" id="fileList'+i+'" name="fileList">');	

		var fileType = '|gif |jpg |png |jpeg';
		
		$('#fileList'+i).MultiFile({
				accept : fileType
			,	list   : '#detailFileList'+i
			,	max    : maxFileCnt
			,	STRING : {
					remove: '<img src="/images/com/web/btn_remove.png">'
						   +'<input type="hidden" id="fileGb'+i+'" name="fileGb" value="'+i+'" />',
					text  : '<input type="text" id="fileExp'+i+'" class="fileExp" name="fileExp" value="'+i+'" />',
					denied:'$ext는(은) 업로드할 수 없는 파일 확장자입니다.'
				},
		});
		
		/* 첨부한 파일의 출력 순서를 자동으로 샛팅 하기 위한 스크립트 - 공통 스크립트로 빠지면 안됨 .개별 항목으로 처리 필수 */
		$('.MultiFile-wrap').change(function(){
			var fileExpIndex = $(this).closest('.fileWrap').find('.fileList > div').children('div:last-child');
			var	fileExp = fileExpIndex.find('.fileExp');
			fileExp.val(fileExpIndex.index() + 1);
		});
	}
	
	//파일 다운로드
	$('.btnFileDwn').on('click', function(){
		var ATTACH = $(this).data("attach");
		var src = '/attach/fileDownload.do?ATTACH_GRP_NO=${param.ATTACH_GRP_NO}&ATTACH='+ATTACH;
		$("#fileDownFrame").attr("src",src);
	});
	
	$('.icn_fileDel').on('click', function(){
		delModiFile($(this));
	});
	
	$('#addBtn').on('click', function(){
		var fileCnt = $(".MultiFile-label").length;
		$('#fileCnt').val(fileCnt);

		if(fileCnt > 0){
			$('#attachExistYn').val("Y");
		}else{
			$('#attachExistYn').val("N");
		}
		
		fnSubmit("fileFrm", "/attach/fileEdit.do");
	});
	
});

</script>
</head>
<body>
	<div class="wrapper">
		<div id="header" class="pop-header">
			<h3>프로필 사진 업로드</h3>	
		</div>
		<!-- e:pop-header -->
		<div id="main" class="pop-body">
			<section id="section">
				<div id="container">
					<div class="cont-inner">
						
						<!-- 페이지 내용 : s -->
						<div style="padding:3px 10px 0;">
							<form id="fileFrm" name="fileFrm" method="post" enctype="multipart/form-data">
								<input type="hidden" id="MODULE" name="MODULE" value="${param.MODULE }"> <!-- 프로필 : 9 -->
								<input type="hidden" id="ATTACH_GRP_NO" name="ATTACH_GRP_NO" value=""> <!-- 첨부파일 번호 -->
								<input type="hidden" id="DEL_SEQ" name="DEL_SEQ" value=""> <!-- 삭제 -->
								<input type="hidden" id="fileCnt" name="fileCnt" value=""/> <!-- 파일건수 -->
								<input type="hidden" id="attachExistYn" name="attachExistYn" value=""/> <!-- 파일 등록 여부 -->
								<input type="hidden" name="myInfo" value="Y"/>
								<input type="hidden" id="userId" name="userId" value="${userId }"/>
								
									<div class="bxType01 fileWrap">
										<div class="filebox" id="attachFile0"></div>
										<div class="fileList" style="padding:7px 0 5px;">
											<div id="detailFileList0" class="fileDownLst">
												<c:forEach var="item" items="${fileList}" varStatus="idx">
													<div class="MultiFile-label" id="fileLstWrap_${item.ATTACH}">
														<span class="MultiFile-remove">
															<a href="#none" class="icn_fileDel" data-attach="${item.ATTACH}" data-idx="${item.CD_FILE_TYPE }">
																<img src="/images/com/web/btn_remove.png" />
															</a>
														</span> 
														<span class="MultiFile-export">
															${item.SEQ_DSP}
														</span> 
														<span class="MultiFile-title" title="File selected: ${item.NAME}">
														${item.NAME}(${item.FILE_SIZE}kb)
														</span>
														<a href="#none" class="btnFileDwn icon download" data-attach="${item.ATTACH}">다운로드</a>
													</div>		
												</c:forEach>
											</div>
										</div>
									</div>
							</form>
							
							<iframe id="fileDownFrame" style="width:0px;height:0px;border:0px"></iframe>
						</div>
						
						<!-- s: 등록 버튼 -->
						<div class="pop-bottom">
							<a href="#none" class="btn comm st04" id="addBtn">등록</a> 
						</div>
						<!-- s: 등록 버튼 -->
						<!-- 페이지 내용 : e -->
					</div>
				</div>
			</section>
		</div>
		<!-- e:pop-body -->
	</div>
	<!-- e:wrapper -->
</body>
</html>