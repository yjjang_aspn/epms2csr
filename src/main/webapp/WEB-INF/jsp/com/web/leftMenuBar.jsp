<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Class Name : leftMenuBar.jsp
  Description : 왼쪽 메뉴 화면
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.07.06    이영탁              최초 생성

    author   : 이영탁
    since    : 2017.07.06
--%>
<%@ include file="/com/comHeader.jsp"%>

<script type="text/javascript">
	$(document).ready(function(){

		jQuery("#jquery-accordion-menu").jqueryAccordionMenu();
		
		//메뉴 이동
		$('.pg-move').on('click', function(){			
			
			$('#MENU_GROUP').val($(this).data("group"));
			$('#MENU_URL').val($(this).data("url"));
			$('#menuNo').val($(this).data("menu"));
			
			//메뉴가 mail일 경우는 메일 계정 확인이 필요함
			if($(this).data("menu") == "110001"){ // 받는 메일인 경우에만..
				popOpen($('.userId'));
				$('#mailId').val($('#menu_emailId').val());
				$('#mailIdTxt').text($('#menu_emailId').val());
				$('.userId .popWrap').css('height', '172px');
			}else{
				parent.linkLeftMenu($(this).data("url"));
			}
		});
	});
	
	
</script>


<nav id="nav">
	<div id="jquery-accordion-menu" class="jquery-accordion-menu blue">
		<ul class="nav-list" id="nav-list">
			<c:forEach var="item" items="${leftMenuList}">	
				<c:if test="${item.WORK_GRP == param.workGrp }">
				<c:if test="${fn:length(item.UPPR_MENU_CD) == 2 }">
				<li class="lnb" data-menu="${item.MENU_CD}">
					<a class="menu${fn:substring(item.MENU_CD, 0, 3)}" data-menu="${item.MENU_CD}">${item.MENU_NM}</a>
					<ul class="submenu">
						<c:forEach var="item1" items="${leftMenuList}">	
							<c:if test="${fn:length(item1.UPPR_MENU_CD) != 2 }">
								<c:if test="${item.WORK_CD eq item1.WORK_CD}">
									<li class="pg-move" data-group="${item.WORK_CD}" data-url="${item1.MENU_URL}"  data-menu="${item1.MENU_CD}"><a href="#none">- ${item1.MENU_NM}</a></li>
								</c:if>
							</c:if>
						</c:forEach>
					</ul>
				</li>
				</c:if>
				</c:if>
			</c:forEach>
		</ul>
		<!-- e:nav-list -->
	</div>
	<!-- e:jquery-accordion-menu -->
</nav>
<!--e:nav-->