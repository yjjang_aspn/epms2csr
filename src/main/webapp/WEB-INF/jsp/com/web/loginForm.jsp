<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%@ taglib uri="/WEB-INF/tlds/code.tld" prefix="tag" %> 
<%--
  Class Name : loginForm.jsp
  Description : 로그인 화면
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.07.04    이영탁              최초 생성

    author   : 이영탁
    since    : 2017.07.04
--%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>CSR</title>
<!-- <link rel="shortcut icon" type="image/x-icon" href="/images/login/web/epms_icon.ico" /> -->
<link rel="shortcut icon" type="image/x-icon" href="/images/login/web/dunkin_icon.ico" />
<link rel="stylesheet" href="/css/com/web_v2/common.css" type="text/css" />
<link rel="stylesheet" href="/css/com/web/page.css" />
<link rel="stylesheet" href="/css/com/web/modalPopup.css">
<script src="/js/jquery/jquery-1.10.2.min.js" type="text/javascript" ></script>	
<script type="text/javascript" src="/js/com/web/md5.js"></script> 
<script type="text/javascript" src="/js/com/web/sha256.js"></script> 
<script type="text/javascript" src="/js/com/web/common.js"></script> 
<script type="text/javascript" src="/js/com/web/default.js"></script>
<script type="text/javascript" src="/js/com/web/modalPopup.js"></script>
<script type="text/javascript" src="/js/jquery/jquery.cookie.js"></script>
<!-- <script src="/js/com/web/jquery.material.form.js" type="text/javascript" ></script> -->

<script type="text/javascript">
	$(function () {
		if('${resMsg}'!=null  && '${resMsg}'.length>0){
			alert('${resMsg}');
			$('#pwd').focus();
		}
	});
	
	$(document).ready(function(){
		
		//로그인 5회 이상 실패시
		if('${loginFailCnt}' >= 5){
			alert('5회 이상 로그인에 실패 하였습니다.\n시스템 관리자에게 문의 바랍니다.');
		}
		
		//세션이 끊어졋는지 주기적으로 검사하는 로직
		setInterval("sessionCheck()", 3000); // 3000ms(3초)가 경과하면 sessionCheck() 함수를 실행합니다.	
		
		$(".loginInput").focus(function(){
			$(this).parent().parent().removeClass("inputFocusOut");
			$(this).parent().parent().addClass("inputFocusIn");
		}).blur(function(){
			$(this).parent().parent().removeClass("inputFocusIn");
			$(this).parent().parent().addClass("inputFocusOut");
		});
			  
		//LOGIN Button Click
		$('#btnLogin').click(function(){ 	
			fnLogin();
		});
		
		//비밀번호 영역 Key Event
		$('#pwd').on('keyup',function(e){
		     if( e.which == 13){
		    	 fnLogin();
		    }
		});
		
		$('#userId').focus();
		
		var deviceInfo = "${deviceInfo}";	//모바일기기 : mobile, 그외 : null
		if(deviceInfo == "" || deviceInfo == null || deviceInfo == "undefined"){
			$('.app-bg-form').hide();
			$('.loginForm').show();
		}else{
			$('.app-bg-form').show();
			$('.loginForm').show();
// 			$('.loginForm').hide();
		}
	});
	
	function sessionCheck(){	
		$('#sessionCheck').attr('src', "/com/sessionCheck2.jsp");
	}
	
	$(window).load(function() {
		
		if($.cookie("ckSaveYn")){
			$("#userId").val($.cookie("ckUserID")); 
			$("#pwd").val($.cookie("ckPassword")); 
			$('#cookie_save_yn_span').addClass("on");
		}
	});
	
	function fnLogin() {
		//사용자구분 점검, 수정날짜:2016.08.22, 수정자:KYH, 수정사유:사용자구분 Selectbox 추가로 인하여 if문 추가함
		if(!isNull($("#userGubun"), "사용자구분을 선택해주세요.")) {
			return;
		}
	
		//ID 점검
		if(!isNull($("#id"), "아이디를 입력하세요.")) {
			return;
		}
	
		//Password 점검
		if(!isNull($("#pwd"), "패스워드를 입력하세요.")) {
			return;
		}
		
		$('#userId').val($('#id').val());
		
		//공급사의 경우 아직 비밀번호를 암호화하고 있지 않으므로 if else문으로 분기를 나눔
		if($("#userGubun").val() == 3) {
			$('#password').val($('#pwd').val());		
		} else {
			$('#password').val(SHA256(hex_md5($('#pwd').val())));
		}
		
		//id저장
		if($("#cookie_save_yn_span").hasClass("on")){
			// setCookie
			$.cookie("ckUserID", $('#userId').val(), { expires: 365 });
			$.cookie("ckPassword", $('#pwd').val(), { expires: 365 });
			$.cookie("ckSaveYn", $("#cookieSaveYn").is(":checked"), { expires: 365 });
		}else{
			$.cookie("ckUserID", ""); //cookie 삭제
			$.cookie("ckPassword", "");
			$.cookie("ckSaveYn", "");
		} 
		
		fnSubmit("frmLogin", "/webLogin.do");
	}
	
	function accountPop(){
	
		fn_openPop('/sys/member/findIdPassword.do', "", 650, 600);
	}	
	
	$(function(){
		$('form.material').materialForm(); // Apply material
	});
</script>

</head>
<body>
	<form id="frmLogin" name="frmLogin" method="post" class="material">
		<input type="hidden" id="userIp" name="userIp" value="<%=request.getRemoteAddr()%>"/>
		<input type="hidden" id="userId" name="userId" />
		<input type="hidden" id="password" name="password"/>
		<input type="hidden" id="lang" name="lang" value="ko"/>
	</form>
	<div id="wrapper" class="loginWrap">
		<div class="loginForm" style="display:none;">
			<div class="loginBody">
			    <h1 class="loginLogo">CSR</h1>
<!-- 				<h1 class="loginLogo"><img src="/images/login/web/login_logo_epms.png"></h1> -->
<!-- 				<h1 class="loginLogo"><img src="/images/login/web/login_logo_dunkin.png"></h1> -->
				<ul class="frmLogin">
					<%-- <li class="company">
						<tag:combo codeGrp="USER_GUBUN" name="userGubun" />
					</li> --%>
					<li class="id inputFocusOut">
						<div>
							<input type="email" placeholder="ID" title="사용자ID" id="id" name="id" class="loginInput" autofocus value="admin" />
						</div>
					</li>
					<li class="pw inputFocusOut">
						<div>
							<input type="password" placeholder="PASSWORD" title="비밀번호" id="pwd" name="pwd" class="loginInput">
						</div>
					</li>
					<li class="rememberPw">
						<span class="chk-st f-l" id="cookie_save_yn_span">
							<input type="checkbox" id="cookieSaveYn" name="cookieSaveYn" value="Y">
							<label for="cookieSaveYn">로그인 정보 저장</label>
						</span>
						<%-- <span class="f-r">
							<a href="#" onclick="accountPop(); return false;">+ 계정정보 찾기</a>
						</span>  --%>
					</li>
				</ul>
			</div>
			<div class="loginFooter">
				<a href="#none" id="btnLogin">로그인</a>
			</div>
		</div>	
		
<!-- 		<section class="app-bg-form" style="position:absolute; top:40%; left:23%; display:none;"> -->
		<section class="app-bg-form" style="position:absolute; top:70%; left:23%; display:none;">
			<ul class="installBtn">
				<li class="androidBtn">
					<a href="https://pms.dunkindonuts.co.kr:7443/appVer/epms_dunkin.apk">
						<img src="/images/login/web/androidBtn.png" alt="안드로이드 설치">
						<span>ANDROID APP INSTALL</span>
					</a>
				</li>
 				<li class="iosBtn">
	 				<a href="itms-services://?action=download-manifest&url=https://pms.dunkindonuts.co.kr:7443/appVer/EpmsDunkin.plist">
	 					<img src="/images/login/web/iosBtn.png" alt="아이폰 설치">
						<span>IOS(apple) APP INSTALL</span>
					</a>
 				</li>
			</ul>	
		</section>
		
		<p class="copy">
			<a href="#"onclick="fnModalToggle('personalInfo');return false;" class="personalInfo">개인정보 취급방침</a><br>
			이 사이트는 Microsoft Explorer 10.0 이상, Chrome, Firefox에 최적화 되어 있습니다.<br>
			Copyright(c)2018 by <strong>ASPN Mobile</strong>
		</p>
	</div>

<%-- 개인정보 취급방침 팝업 --%>
<%@ include file="/WEB-INF/jsp/com/web/personalInfo.jsp"%>

</body>
</html>


<iframe id="sessionCheck" name="sessionCheck" style="display:none;"></iframe>
