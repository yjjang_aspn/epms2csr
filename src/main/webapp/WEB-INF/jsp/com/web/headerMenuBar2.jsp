<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
	$(document).ready(function() {	
		//세션이 끊어졋는지 주기적으로 검사하는 로직
		setInterval("sessionCheck()", 3000); // 3000ms(3초)가 경과하면 sessionCheck() 함수를 실행합니다.	
	});
	
	function sessionCheck(){	
		$('#sessionCheck').attr('src', "/com/sessionCheck.jsp");
	}
	/* 헤더 메뉴 이동 */
	function linkHeaderMenu(menu_url, menu_group){
		if(menu_url == ''){
			return;
		}
		$("#mainIfrm").attr("src",  menu_url);
// 		$("#leftIfrm").attr("src",  "/sys/menu/leftMenuBar.do?workGrp="+menu_cd);
	}
</script>

<ul id="gnb">			
	<c:forEach var="item" items="${list}">	
		<c:set var="menu_group" value="${item.WORK_CD}"/>
		<li id="gnb_${item.MENU_CD}">	
			<a href="#">${item.MENU_NM}</a>
			<ul>
			<c:forEach var="items" items="${subMenuList}" >
				<c:if test="${menu_group eq items.WORK_CD}">
					<li><a href="#" onclick="linkHeaderMenu('${items.MENU_URL}','${item.WORK_CD}'); return false;" >${items.MENU_NM}</a></li>
				</c:if>
			</c:forEach>
			</ul>
		</li>	
	</c:forEach>
</ul>