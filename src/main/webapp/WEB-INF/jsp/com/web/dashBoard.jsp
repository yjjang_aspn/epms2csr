<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Class Name : dashBoard.jsp
  Description : 대시보드 화면
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.07.06    이영탁              최초 생성

    author   : 이영탁
    since    : 2017.07.06
--%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>

<script type="text/javascript">

</script>
</head>
<body>
	<div id="contents">
	
	</div>
</body>
</html>