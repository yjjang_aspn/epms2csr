<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"    %>
<%--
  Class Name : mainMenu.jsp
  Description : 메인 메뉴 화면
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.07.06    이영탁              최초 생성
   2018.05.18    김영환              정비원 현황 추가
   2018.06.20    김영환              정비요청현황, 실적현황, 예방보전현황 modalPopup
   2018.09.03    김영환              비밀번호 변경 modalPopup

    author   : 이영탁
    since    : 2017.07.06
--%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script type="text/javascript" src="/js/com/web/md5.js"></script> 
<script type="text/javascript" src="/js/com/web/sha256.js"></script> 

<script type="text/javascript">

<%-- 전역변수 --%>
var mainFocusedRow = null;		<%-- Focus Row : [팝업 : 정비실적] --%>
var mainFocusedRow2 = null;		<%-- Focus Row : [팝업 : 정비요청] --%>
var mainFocusedRow3 = null;		<%-- Focus Row : [팝업 : 예방보전] --%>
var counter = 0;
window.setInterval("fnSetDashboardCount()", 300000);

$(document).ready(function(){
	
	<%-- 아이디와 비번이 동일할 경우 --%> 
	if('${pwd_reset}' == 'Y'){ 
		alert("패스워드가 초기화 또는 신규 생성입니다.\n변경 후 사용 하시기 바랍니다.");
		fnModalToggle('popPwdChange');
	}
	
	<%-- 비밀번호 변경 180일 초과할 경우 --%> 
	if('${pwd_change}' == 'Y'){ 
		alert("비밀번호를 변경한지 6개월 이상이 지났습니다.\n변경 후 사용 하시기 바랍니다.");
		fnModalToggle('popPwdChange');
	}
	 
	
	<%-- 계정정보 저장 버튼 클릭  --%>
	$("#chgBtn").on("click", function(){
		fnChgPwd();
	});
	
	<%-- 대시보드 현재 날짜 계산 --%>
	var date = new Date();
	var fullYear = date.getFullYear().toString();
	var month = date.getMonth()+1;
	month = month < 10 ? "0"+ month : month;
	
	$(".statusPeriod_year").html("<span>"+fullYear.substr(0,2)+"</span>"+fullYear.substr(2,4)+"년");
	$(".statusPeriod_month").html(month + "월");
	
	<%-- 전체 loading bar intercept 회피 --%>
	$.ajaxSetup({global: false});
	
	//헤더메뉴 select 표시
	var headMenuCnt =  '${sessionScope.ssHeadMenuCnt}' * 1;
	if(headMenuCnt > 1){
		hearMenuSelect('${MENU_HEADER_GROUP}');
	}else{
		var menu = '${MENU_1}';
		menu = menu.substring(0,4) + "00";
		hearMenuSelect(menu);
	}
	jQuery("#jquery-accordion-menu").jqueryAccordionMenu();
	
	var auth = '${sessionScope.ssAuth}';
	if(headMenuCnt == 1){
			$("#mainIfrm").attr("src", "/epms/main/mainDashBoard.do");
	}else{
		$("#mainIfrm").attr("src", "/dashBoard.do");
	}
	
	// 출/퇴근 버튼
	$('#adcSwitchBtn').on("click", function(){
		var check_inout = $("#check_inout").val()//출퇴근 상태값
		
		// 출퇴근 상태 (1:출근, 2: 퇴근)
		if(check_inout == "02"){
			$('#check_inout').val("01");
			checkInOutAjax($('#check_inout').val());
		}else{
			$('#check_inout').val("02");
	    	checkInOutAjax($('#check_inout').val());
		}
	});

	<%-- 정비원 현황 클릭 이벤트 --%>
	$("#adcStatBtn").on('click', function(){
		fnOpenLayerWithParam('/mem/member/popWorkerStatList.do', 'popWorkerStatList');
	})
	
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function (e) {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>
	});
	
	<%-- 대시보드 카운트 셋팅 --%>
	fnSetDashboardCount();

	<%-- 상태 건수 클릭이벤트 --%>
	$(".statusDtl").on('click', function(){
		
		if($(this).find('.statusCont').text() != 0){
			<%-- 정비원 또는 팀장권한일 경우에만 상세페이지 조회 가능 --%>
			if("${fn:indexOf(sessionScope.ssSubAuth, 'AUTH02')}" > -1 || "${fn:indexOf(sessionScope.ssSubAuth, 'AUTH03')}" > -1){
				var click_tag_id = $(this).attr('id');
				
				if (click_tag_id == "REPAIR_REQUEST_LI"){ <%-- 정비요청 클릭시 --%>
					
					fnOpenLayerWithGrid("popMainRepairApprList", "REPAIR_REQUEST");
					$("#popMainRepairApprList_title").text("정비현황(정비요청)");
					
				}else if (click_tag_id == "REPAIR_APPR_LI") { <%-- 대기 클릭시 --%>
					fnOpenLayerWithGrid("popMainRepairApprList", "REPAIR_APPR");
					$("#popMainRepairApprList_title").text("정비현황(결제대기)");
					
				}else if (click_tag_id == "REPAIR_REJECT_LI") { <%-- 반려 클릭시 --%>
					fnOpenLayerWithGrid("popMainRepairApprList", "REPAIR_REJECT");
					$("#popMainRepairApprList_title").text("정비현황(반려)");
					
				}else if (click_tag_id == "REPAIR_CANCEL_LI") { <%-- 정비취소 클릭시 --%>
					fnOpenLayerWithGrid("popMainRepairApprList", "REPAIR_CANCEL");
					$("#popMainRepairApprList_title").text("정비현황(정비취소)");
					
				}else if (click_tag_id == "REPAIR_COMPLETE_LI") { <%-- 정비실적 정비완료 클릭시 --%>
					fnOpenLayerWithGrid("popMainRepairResultList", "REPAIR_COMPLETE");
					$("#popMainRepairResultList_title").text("정비현황(정비완료)");
					
				}else if (click_tag_id == "REPAIR_ONGOING_LI") { <%-- 정비실적 진행중 클릭시 --%>
					fnOpenLayerWithGrid("popMainRepairResultList", "REPAIR_ONGOING");
					$("#popMainRepairResultList_title").text("정비현황(진행중)");
					
				}else if (click_tag_id == "PREVENTIVE_PLAN_LI") { <%-- 예방보전 계획 --%>
					$('#popMainPreventivePlanList_title').text("예방보전계획현황(예방보전 계획)");
					fnOpenLayerWithGrid("popMainPreventivePlanList", "PREVENTIVE_PLAN");
					
				}else if (click_tag_id == "PREVENTIVE_COMPLETE_LI") { <%-- 예방보전 점검 완료 --%>
					$('#popMainPreventivePlanList_title').text("예방보전계획현황(점검 완료)");
					fnOpenLayerWithGrid("popMainPreventivePlanList", "PREVENTIVE_COMPLETE");
					
				}else if (click_tag_id == "PREVENTIVE_UNCHECK_LI") { <%-- 예방보전 미점검 완료 --%>
					$('#popMainPreventivePlanList_title').text("예방보전계획현황(미점검 완료)");
					fnOpenLayerWithGrid("popMainPreventivePlanList", "PREVENTIVE_UNCHECK");
					
				}else if (click_tag_id == "PREVENTIVE_ONGOING_LI") { <%-- 예방보전 진행중(대기) --%>
					$('#popMainPreventivePlanList_title').text("예방보전계획현황(진행중)");
					fnOpenLayerWithGrid("popMainPreventivePlanList2", "PREVENTIVE_ONGOING");
				}
			} else {
				alert('상세조회가 불가능한 권한입니다.');
			}
		}	
	});
});

<%-- 그리드 클릭 이벤트 --%>
Grids.OnClick = function(grid, row, col){

	if(row.id == "Filter" || row.id == "Header" || row.id == "spanHeader" || row.id == "Toolbar" || col =="Panel"|| row.id == "PAGER" || row.id == "NoData") {
		return;
	}
	
	if(grid.id == "PopMainRepairResultList") { <%-- [팝업 : 정비실적] 클릭시 --%>
		
		<%-- 포커스 --%>
		if(mainFocusedRow != null){
			grid.SetAttribute(mainFocusedRow,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		mainFocusedRow = row;
		
	}
	else if(grid.id == "PopMainRepairApprList") { <%-- [팝업 : 정비요청] 클릭시 --%>
		<%-- 포커스 --%>
		if(mainFocusedRow2 != null){
			grid.SetAttribute(mainFocusedRow2,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		mainFocusedRow2 = row;
	}
	else if(grid.id == "PopMainPreventivePlanList") { <%-- [팝업 : 예방보전] 클릭시 --%>
		
		<%-- 포커스  --%>
		if(mainFocusedRow3 != null){
			grid.SetAttribute(mainFocusedRow3,null,"Color","#FFFFFF",1);	
		}
		grid.SetAttribute(row,null,"Color","#FFFFAA",1);
		
		mainFocusedRow3 = row;
	}
	else if(grid.id == "PopRepairAnalysisList") { <%-- [고장원인분석 불러오기] 클릭시 --%>
	
		REPAIR_RESULT = row.REPAIR_RESULT;
		REPAIR_ANALYSIS = row.REPAIR_ANALYSIS;
		init.getData(row.SAVE_YN, function(result){
			
			init.setData.headData(result.analysisInfo, $("#editForm"));
			init.setData.memberData(result.repairRegMember, $("#editForm"));
			init.setData.itemData(result.analysisItem, $("#editForm"), "edit");
			
		});
		fnModalToggle('popRepairAnalysisList');
	}
}

<%-- 비밀번호 변경  --%>
function fnChgPwd(){
	
	var newPw = $('#user_pwd').val();
	var checkPw = $('#crmChgPwd').val();
	var user_id = $('#user_id').val();
		
	if(newPw == ""){
		alert("신규 비밀번호를 입력하서야 합니다.");
		return;
	}
	
	if(newPw.length < 8){
		alert("비밀번호는 8자 이상으로 변경하셔야 합니다.");
		$('#user_pwd').val("");
		$('#user_pwd').focus();
		return;
	}
	
	if(!fnCheckPwdValid(newPw)){
		alert("비밀번호는 영문, 숫자, 특수문자를 조합하여 생성하셔야 합니다.");
		$('#user_pwd').val("");
		$('#user_pwd').focus();
		return;
	}
	
	
	if(SHA256(hex_md5(newPw)) == SHA256(hex_md5(user_id))){
		alert("비밀번호를 변경 하셔야 합니다.");
		$('#user_pwd').val("");
		$('#user_pwd').focus();
		return;
	}
	
	if(newPw != checkPw){
		alert("입력하신 비밀번호가 서로 다릅니다.");
		$('#crmChgPwd').val("");
		$('#crmChgPwd').focus();
		return;
	}else {
		if(confirm("계정정보를 수정하시겠습니까?")){
			$('#PASSWORD').val(SHA256(hex_md5(newPw)));
			var form = new FormData(document.getElementById('chgForm'));
			/* 계정 정보 수정 */
			$.ajax({
				type     : 'POST',
				url      : '/mem/member/updateProfileAjax.do',
				data     : form,
				dataType : 'json',
				processData : false,
				contentType : false,
				success  : function (data) {
					
					if(data.resultCd == 'T'){
						alert("계정 정보가 수정되었습니다.");
						fnModalToggle('popPwdChange');
					}
					
				},error: function(XMLHttpRequest, textStatus, errorThrown){
					alert('계정 정보 수정에 실패하였습니다. 관리자에게 문의해주세요.\n error : ' + textStatus );
					fnModalToggle('popPwdChange');
				}
			});
			
		}
	}
	
}

<%-- 비밀번호 유효성 체크  --%>
function fnCheckPwdValid(newPw){
	var passwordRules = /^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,16}$/;
	return passwordRules.test(newPw);
}

<%-- 대시보드 카운트 갱신  --%>
function fnSetDashboardCount(){
	
	var user_part = $('#user_part').val();
	
	$.ajax({
		type : 'POST',
		url : '/dashboardInfo.do',
		cache : false,
		dataType : 'json',
		data : {ssPart: user_part},
		async : false,
		error : function() {
			//alert("[main] 처리중 오류가 발생 하였습니다.");
			console.log(' [main.fnSetDashboardCount] 처리중 오류가 발생 하였습니다. ');
		},
		success : function(json) {
			<%-- 예방보전 현황 --%>
			var preventiveInfo = json.preventiveInfo;
			<%-- 정비 현황 --%>
			var repairInfo = json.repairInfo;
			
			var _ul = $("#dashboardInfo");
			
			for( var name in preventiveInfo ) {
            	switch (name) {
	 				default:
	 					_ul.find("[name=" + name + "]").text(preventiveInfo[name]);
	 				break;
 				}
			}
			
			for( var name in repairInfo ) {
            	switch (name) {
	 				default:
	 					_ul.find("[name=" + name + "]").text(repairInfo[name]);
	 				break;
 				}
			}
			
			counter = counter + 1;
			
			if(counter > 1){
				<%-- 메인대시보드일 경우 iframe 새로고침 --%>
				if($('#mainIfrm').attr("src") == "/epms/main/mainDashBoard.do"){
					mainIfrm.location.reload();	
				}
			}
		}		
	}); 
}

<%-- 모달 팝업 호출 --%>
function fnOpenLayerWithGrid(popupUrl, param){	
	
	if(popupUrl=="popMainRepairApprList"){
		Grids.PopMainRepairApprList.Source.Layout.Url = "/epms/main/popMainRepairApprListLayout.do?APPR_STATUS="+param;
 		Grids.PopMainRepairApprList.Source.Data.Url   = "/epms/main/popRepairApprListData.do?APPR_STATUS="+param;

 		Grids.PopMainRepairApprList.Reload();
		fnModalToggle('popMainRepairApprList');
		
	}else if (popupUrl=="popMainRepairResultList") {
		Grids.PopMainRepairResultList.Source.Data.Url = "/epms/main/popRepairResultListData.do?srcType=3&REPAIR_STATUS="+param;
		Grids.PopMainRepairResultList.Reload();
		fnModalToggle('popMainRepairResultList');
		
	}else if (popupUrl=="popMainPreventivePlanList") {<%-- 메인상단 : 예방보전-뱃지: 계획, 완료, 미점검 popup--%>
		Grids.PopMainPreventivePlanList.Source.Layout.Url = "/epms/main/popMainPreventiveResultListLayout.do?LOCATION=${sessionScope.ssLocation}";
		Grids.PopMainPreventivePlanList.Source.Data.Url = "/epms/main/popMainPreventivePlanListData.do?PREVENTIVE_STATUS="+param;
		Grids.PopMainPreventivePlanList.Reload();
		fnModalToggle('popMainPreventivePlanList');
	}else if (popupUrl=="popMainPreventivePlanList2") {<%-- 메인상단 : 예방보전-뱃지: 진행중 popup --%>
		Grids.PopMainPreventivePlanList.Source.Layout.Url = "/epms/main/popMainPreventivePlanListLayout.do?LOCATION=${sessionScope.ssLocation}";
		Grids.PopMainPreventivePlanList.Source.Data.Url = "/epms/main/popMainPreventivePlanListData.do?&PREVENTIVE_STATUS="+param;
		Grids.PopMainPreventivePlanList.Reload();
		fnModalToggle('popMainPreventivePlanList');
	}
	 
}

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

//출퇴근 상태값 변경
function checkInOutAjax(check_inout){
	jQuery.ajax({
		type : 'POST',
		url : '/mem/member/checkInOutAjax.do',
		cache : false,
		dataType : 'json',
		data : {CHECK_INOUT : check_inout},
		async : false,
		error : function() {
			alert("처리중 오류가 발생 하였습니다.");
		},
		success : function(json) {
			if(json.RES_CD != "0000"){ //T: 정상처리
				var check_switch = $("#adcSwitchBtn").is(":checked");
				if(check_switch == true){
					$("#adcSwitchBtn").prop("checked", true);	
				}else{
					$("#adcSwitchBtn").prop("checked", false);		
				}
				alert(json.RES_MSG);
			}
		}
	}); 
}

</script>
</head>
<body>


	
<div id="wrapper" class="wrapper">
	<div class="wrapperIn">
		<!-- 포틀릿 설정 form -->
		<form name="checkboxArea" id="checkboxArea" method="post" action="#"></form>
		<form id="menuFrm" name="menuFrm" method="post">
			<input type="hidden" id="user_id" value="${sessionScope.userId}"/>
			<input type="hidden" id="MENU_GROUP" name="MENU_GROUP" value="" />
			<input type="hidden" id="MENU_URL" name="MENU_URL" value="" />
			<input type="hidden" id="menuNo" name="forMenu" value="" />
			<input type="hidden" id="contents" name="CONTENTS" />
			<input type="hidden" id="module" name="MODULE" />
			<input type="hidden" id="user_part" value="${sessionScope.ssPart}" />
			<input type="hidden" id="MENU" />
			
			<div id="header">
				<h1 class="ci_logo">
					<a href="#none" class="ci"><img src="/images/com/web/ASPN_logo_white.png" alt="" /></a>
<!-- 					<a href="#none" class="ci"><img src="/images/com/web/dunkin_logo.png" alt="" /></a> -->
				</h1>
				
				<div class="gnb-list">
					<c:import url="/sys/menu/headerMenuBar2.do" />
				</div>
				
				<div class="f-r">
					<div class="summaryWrapBtnWrap f-l">
						<a href="#none" class="summaryWrapBtn showBtn" id="summaryWrapBtn"></a>
					</div>
					<div class="dropdownWrap layoutSet f-l">
						<div class="dropdownBtn showBalloon" title="포틀릿 설정">
							<img src="/images/com/web_v2/layout_setting_icon.png"/>
						</div>
						<div class="dropdownCon">
							<div class="portletToggleWrap">
								<!-- id : checkbox 다음 숫자는 panel의 data-inner-id/loipanel-panel 다음 숫자와 동일하게 한다. -->
								<div class="portletSwitchWrap">
									<div class="portletSwitch">
										<input type="checkbox" id="portletCheck1" name="checkbox" class="portletSwitch-checkbox" checked>
										<label for="portletCheck1" class="portletSwitch-label">
									        <span class="portletSwitch-inner"></span>
									        <span class="portletSwitch-switch"></span>
									    </label>
									</div>
									<div class="portletSwitch-copy"><spring:message code='epms.work.perform' /> 현황</div><!-- 정비실적 현황 -->
								</div>
								<div class="portletSwitchWrap">
									<div class="portletSwitch">
										<input type="checkbox" id="portletCheck2" name="checkbox" class="portletSwitch-checkbox" checked>
										<label for="portletCheck2" class="portletSwitch-label">
									        <span class="portletSwitch-inner"></span>
									        <span class="portletSwitch-switch"></span>
									    </label>
									</div>
									<div class="portletSwitch-copy"><spring:message code='epms.work.urgency' /> 현황</div><!-- 돌발정비 현황 -->
								</div>
								<div class="portletSwitchWrap">
									<div class="portletSwitch">
										<input type="checkbox" id="portletCheck3" name="checkbox" class="portletSwitch-checkbox" checked>
										<label for="portletCheck3" class="portletSwitch-label">
									        <span class="portletSwitch-inner"></span>
									        <span class="portletSwitch-switch"></span>
									    </label>
									</div>
									<div class="portletSwitch-copy">MTBF/MTTR</div>
								</div>
<!-- 								<div class="portletSwitchWrap"> -->
<!-- 									<div class="portletSwitch"> -->
<!-- 										<input type="checkbox" id="portletCheck4" name="checkbox" class="portletSwitch-checkbox" checked> -->
<!-- 										<label for="portletCheck4" class="portletSwitch-label"> -->
<!-- 									        <span class="portletSwitch-inner"></span> -->
<!-- 									        <span class="portletSwitch-switch"></span> -->
<!-- 									    </label> -->
<!-- 									</div> -->
<!-- 									<div class="portletSwitch-copy">재고부족 자재</div> -->
<!-- 								</div> -->
<!-- 								<div class="portletSwitchWrap"> -->
<!-- 									<div class="portletSwitch"> -->
<!-- 										<input type="checkbox" id="portletCheck5" name="checkbox" class="portletSwitch-checkbox" checked> -->
<!-- 										<label for="portletCheck5" class="portletSwitch-label"> -->
<!-- 									        <span class="portletSwitch-inner"></span> -->
<!-- 									        <span class="portletSwitch-switch"></span> -->
<!-- 									    </label> -->
<!-- 									</div> -->
<!-- 									<div class="portletSwitch-copy">예방보전 현황</div> -->
<!-- 								</div> -->
								<div class="portletSwitchWrap">
									<div class="portletSwitch">
										<input type="checkbox" id="portletCheck6" name="checkbox" class="portletSwitch-checkbox" checked>
										<label for="portletCheck6" class="portletSwitch-label">
									        <span class="portletSwitch-inner"></span>
									        <span class="portletSwitch-switch"></span>
									    </label>
									</div>
									<div class="portletSwitch-copy">정비원 업무현황</div>
								</div>
<!-- 								<div class="portletSwitchWrap"> -->
<!-- 									<div class="portletSwitch"> -->
<!-- 										<input type="checkbox" id="portletCheck7" name="checkbox" class="portletSwitch-checkbox" checked> -->
<!-- 										<label for="portletCheck7" class="portletSwitch-label"> -->
<!-- 									        <span class="portletSwitch-inner"></span> -->
<!-- 									        <span class="portletSwitch-switch"></span> -->
<!-- 									    </label> -->
<!-- 									</div> -->
<!-- 									<div class="portletSwitch-copy">내 요청 현황</div> -->
<!-- 								</div> -->
<!-- 								<div class="portletSwitchWrap"> -->
<!-- 									<div class="portletSwitch"> -->
<!-- 										<input type="checkbox" id="portletCheck8" name="checkbox" class="portletSwitch-checkbox" checked> -->
<!-- 										<label for="portletCheck8" class="portletSwitch-label"> -->
<!-- 									        <span class="portletSwitch-inner"></span> -->
<!-- 									        <span class="portletSwitch-switch"></span> -->
<!-- 									    </label> -->
<!-- 									</div> -->
<!-- 									<div class="portletSwitch-copy">자재 재고현황</div> -->
<!-- 								</div> -->
								<div class="cl-b mgn-t-15 f-l" style="width: 100%;">
									<a href="#none" id="savePortlet" class="btn comm st01">저장</a>
									<a href="#none" id="cancelPortlet" class="btn comm st02">취소</a>
								</div>
							</div>
							<a href="#none" class="layoutResetBtn">레이아웃 초기화</a>
						</div>
					</div>
					<div class="adcWrap f-l">
						<%-- <c:if test="${fn:indexOf(sessionScope.ssAuth,'AUTH_02') > -1 or fn:indexOf(sessionScope.ssAuth,'AUTH_03') > -1}"> --%>
							<div class="adcSwitchBtn">
								<c:if test="${sessionScope.ssCheckInout ne '01'}"> 
									<input type="checkbox" name="adcSwitchBtn" class="adcSwitchBtn-checkbox" id="adcSwitchBtn"/>
									<label class="adcSwitchBtn-label" for="adcSwitchBtn">
										<span class="adcSwitchBtn-inner"></span>
										<span class="adcSwitchBtn-switch"></span>
										<input type="hidden" id="check_inout" name="check_inout" value="${sessionScope.ssCheckInout}"/>
									</label>
								</c:if>
								<c:if test="${sessionScope.ssCheckInout eq '01'}">
									<input type="checkbox" name="adcSwitchBtn" class="adcSwitchBtn-checkbox" id="adcSwitchBtn" checked/>
									<label class="adcSwitchBtn-label" for="adcSwitchBtn">
										<span class="adcSwitchBtn-inner"></span>
										<span class="adcSwitchBtn-switch"></span>
										<input type="hidden" id="check_inout" name="check_inout" value="${sessionScope.ssCheckInout}"/>
									</label>
								</c:if>
							</div>
						<%-- </c:if> --%>
						<a href="#none" class="adcStatBtn" id="adcStatBtn"><spring:message code='epms.worker.status' /></a><!-- 정비원현황 -->
					</div>
					<div class="dropdownWrap userInfo f-l">
						<div class="dropdownBtn">
							<c:choose>
								<c:when test="${sessionScope.ssUserThumbUrl == '' || sessionScope.ssUserThumbUrl eq null}">
									<div class="user_img"><img src="/images/com/web_v2/userIcon.png" alt="" /></div>
								</c:when>
								<c:otherwise>
									<div class="user_img"><img src="${sessionScope.ssUserThumbUrl}" onerror="this.src='/images/com/web_v2/userIcon.png'" alt="" /></div>
								</c:otherwise>
							</c:choose>
							<div class="user_name">
								<span class="dep">${sessionScope.ssDivisionNm}</span>
								<span class="name">${sessionScope.ssUserNm}&nbsp;${sessionScope.ssJobGradeNm}</span>
							</div>
						</div>
						<ul class="dropdownCon">
							<li><a href="#none" class="info_change_btn user-info">내 정보 변경</a></li>
							<li><a href="#none" class="logout_btn" id="logOutBtn">로그아웃</a></li>
						</ul>
					</div>
				</div>
			</div>
			<!--e:header-->
			
			<!-- start : main -->
			<div id="main" class="main">
				<div class="mainIn">
					<!-- start : 현재페이지 및 결재현황 요약 정보 -->
					<div id="summaryWrap" class="summaryWrap">
						<div class="summaryWrapIn">
							<div class="f-l">
								<div class="statusInfo" id="dashboardInfo">
									<div class="statusPeriod">
										<div class="v-center">
											<div class="statusPeriod_year"></div>
											<div class="statusPeriod_month"></div>
										</div>
									</div>
									
									<!-- s:정비현황 -->
									<div class="statusLst">
										<div class="statusLstTit f-l">
											<div class="v-center"><spring:message code='epms.status' /></div><!-- 정비 -->
										</div>
										<ul class="statusDtlWrap">
											<li class="statusDtl v-center" id="REPAIR_REQUEST_LI">
												<div>
													<p class="statusTit">CSR 요청<p><!-- 정비요청 -->
													<p class="statusCont" name="REPAIR_REQUEST_CNT" id="REPAIR_REQUEST_CNT"></p>
												</div>
											</li>	
											<li class="statusDtl v-center" id="REPAIR_COMPLETE_LI">
												<div>
													<p class="statusTit">CSR 완료<p><!-- 정비완료 -->
													<p class="statusCont" name="REPAIR_COMPLETE_CNT" id="REPAIR_COMPLETE_CNT"></p>
													<p class="stutusWarning"><!-- 돌발정비 -->
													    <spring:message code='epms.work.urgency' /> <span class="fontRed" name="REPAIR_EM_CNT"></span> 건
													</p>
												</div>
											</li>							
											<li class="statusDtl v-center" id="REPAIR_ONGOING_LI">
												<div>
													<p class="statusTit">진행중<p><!-- 진행중 -->
													<p class="statusCont" name="REPAIR_ONGOING_CNT" id="REPAIR_ONGOING_CNT"></p>
													<p class="stutusWarning">지연 <span class="fontRed" name="REPAIR_EXCESS_CNT"></span> 건</p>
												</div>
											</li>
											<li class="statusDtl v-center" id="REPAIR_APPR_LI">
												<div>
													<p class="statusTit">결제대기<p><!-- 결제대기 -->
													<p class="statusCont" name="REPAIR_APPR_CNT" id="REPAIR_APPR_CNT"></p>
												</div>
											</li>
											<li class="statusDtl v-center" id="REPAIR_REJECT_LI">
												<div>
													<p class="statusTit">반려<p><!-- 반려 -->
													<p class="statusCont" name="REPAIR_REJECT_CNT" id="REPAIR_REJECT_CNT"></p>
												</div>
											</li>
											<li class="statusDtl v-center" id="REPAIR_CANCEL_LI">
												<div>
													<p class="statusTit">CSR 취소<p><!-- 정비취소 -->
													<p class="statusCont" name="REPAIR_CANCEL_CNT" id="REPAIR_CANCEL_CNT"></p>
												</div>
											</li>
										</ul>
									</div>
									<!-- e:정비현황 -->
									
									<!-- s:예방보전현황 -->
<!-- 									<div class="statusLst"> -->
<!-- 										<div class="statusLstTit f-l"> -->
<!-- 											<div class="v-center">예방보전</div> -->
<!-- 										</div> -->
<!-- 										<ul class="statusDtlWrap"> -->
<!-- 											<li class="statusDtl v-center" id="PREVENTIVE_PLAN_LI"> -->
<!-- 												<div> -->
<!-- 													<p class="statusTit">예방보전 계획<p> -->
<!-- 													<p class="statusCont" name="PREVENTIVE_PLAN_CNT" id="PREVENTIVE_PLAN_CNT"></p> -->
<!-- 												</div> -->
<!-- 											</li> -->
<!-- 											<li class="statusDtl v-center" id="PREVENTIVE_COMPLETE_LI"> -->
<!-- 												<div> -->
<!-- 													<p class="statusTit">점검 완료<p> -->
<!-- 													<p class="statusCont" name="PREVENTIVE_COMPLETE_CNT" id="PREVENTIVE_COMPLETE_CNT"></p> -->
<!-- 													<p class="stutusWarning">정비필요 <span class="fontRed" name="PREVENTIVE_REPAIR_REQUEST_CNT"></span> 건</p> -->
<!-- 												</div> -->
<!-- 											</li> -->
<!-- 											<li class="statusDtl v-center" id="PREVENTIVE_ONGOING_LI"> -->
<!-- 												<div> -->
<!-- 													<p class="statusTit">진행중<p> -->
<!-- 													<p class="statusCont" name="PREVENTIVE_ONGOING_CNT" id="PREVENTIVE_ONGOING_CNT"></p> -->
<!-- 													<p class="stutusWarning">지연 <span class="fontRed" name="PREVENTIVE_EXCESS_CNT"></span> 건</p> -->
<!-- 												</div> -->
<!-- 											</li> -->
<!-- 											<li class="statusDtl v-center" id="PREVENTIVE_UNCHECK_LI"> -->
<!-- 												<div> -->
<!-- 													<p class="statusTit">미점검<p> -->
<!-- 													<p class="statusCont" name="PREVENTIVE_UNCHECK_CNT" id="PREVENTIVE_UNCHECK_CNT"></p> -->
<!-- 												</div> -->
<!-- 											</li> -->
<!-- 										</ul> -->
<!-- 									</div> -->
									<!-- s:예방보전현황 -->
									
								</div>
							</div>
							<div class="f-r" style="padding:30px 30px 30px 0">
								<a href="javascript:fnSetDashboardCount();"><img src="/images/com/web_v2/refresh.png" alt="" style="width:34px;"></a>
							</div>
						</div>
					</div>
					<!-- end : 현재페이지 및 결재현황 요약 정보 -->
					
					<!-- start : 컨텐츠 내용 -->
					<div id="container" class="container">
						<div class="cont-inner">
							
							<!-- start : mainDashBoard.jsp -->
							<iframe id="mainIfrm" name="mainIfrm" marginwidth="0px" marginheight="0px"
								frameborder="no"  scrolling="no" align="center" style="width:100%;"
								src=""></iframe>
							<!-- end : mainDashBoard.jsp -->
								
						</div>
					</div>
					<!-- end : 컨텐츠 내용 -->
					
				</div>
			</div>
			<!-- end : main -->
		
		</form>
	</div>
</div>

<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<%-- 출퇴근현황 --%>
<div class="modal fade modalFocus" id="popWorkerStatList" data-backdrop="static" data-keyboard="false"></div>

<%-- 출퇴근 이력조회 --%>
<div class="modal fade modalFocus" id="popCheckInOutList" data-backdrop="static" data-keyboard="false"></div>

<script>
	$(document).ready(function() {
		
		$('.summaryWrapBtn').click(function(event) {
		    $('.summaryWrapIn').slideToggle('fast', function() {
		    	$('.summaryWrapBtn').toggleClass('showBtn hideBtn');
		    	
		    	var containerH = $("#container").height();
		    	var summaryWrapH = 95;
		    	if($('.summaryWrapIn').is(':hidden')) {
		        	$("#container, #mainIfrm").height(containerH+summaryWrapH);
		        }else {
					$("#container, #mainIfrm").height(containerH-summaryWrapH);
		        }
				event.preventDefault();
		    });
		});
		
		<%-- 도메인 저장소 값 유무 체크 --%>
		if(localStorage.length > 0){
			$.each(localStorage, function(i, o) {
				var object = "";
				
				if(typeof o === 'string'){

					//console.log('# main.jsp [localStorage]' + localStorage + '(' + o + ') ');
					// 문자열 " " (Double Quotation Marks) 처리  =  Unexpected token # in JSON at position 0
					//object = JSON.parse( '"' + o + '"' );
					
					<%-- onClose된 포틀릿이 있다면 Checkbox 해제 --%>
					if(object.state == "onClose"){
						$("#portletCheck" + i.substr(25)).attr('checked', false);
					}
				}
			});
		}
		
		//포틀릿 설정-저장
		$('#savePortlet').click(function() {
			
			// innerId 저장
			var innerIds = new Array();
			$('input:checkbox[name="checkbox"]').each(function(){
				if(this.checked == true){
					localStorage.removeItem("lobipanel_lobipanel-panel" + $(this).attr("id").substr(12));
				}else{
					localStorage.setItem("lobipanel_lobipanel-panel" + $(this).attr("id").substr(12), '{"state":"onClose"}');
				}
			});
			
			// mainIfm reload
			location.reload();
			// dropdownWrap Close
			$(this).closest('.dropdownWrap').removeClass('is-open');
		});
		
		//포틀릿 설정-취소
		$('#cancelPortlet').click(function() {
			var parent = $(this).closest('.dropdownWrap');
			parent.removeClass('is-open');
		});
								
		//포틀릿 설정-레이아웃 초기화
		$('.layoutResetBtn').click(function() {
			localStorage.clear();
			location.reload();
		});
		
		//드롭다운 메뉴
		$('.dropdownWrap').on('click','.dropdownBtn',function(){
			var parent = $(this).closest('.dropdownWrap');
			if ( ! parent.hasClass('is-open')){
				parent.addClass('is-open');
				$('.dropdownWrap.is-open').not(parent).removeClass('is-open');
			}else{
				parent.removeClass('is-open');
			}
		});
		$(document).mouseup(function (e){
			var dropdownWrap = $(".dropdownWrap");
			if(dropdownWrap.has(e.target).length === 0)
				dropdownWrap.removeClass('is-open');
		});
		
	});
	
	//툴팁
	$(function(){
		var btnBalloon=$('<div class="btnBalloon"></div>').appendTo('body');
		function updateBalloonPosition(x,y){ //마우스 움직임을 따라옴
			btnBalloon.css({left:x + 15, top:y}); //마우스와 겹치지 않도록 x축 값 이동
		}
		
		//사용자정의함수_불러주지 않으면 실행되지 않음
		function showBalloon(){
			btnBalloon.stop();
			btnBalloon.css('opacity',0).show();
			btnBalloon.animate({opacity:1},100);
		}
		function hideBalloon(){
			btnBalloon.stop();
			btnBalloon.animate(
				{opacity:0},100,
				function(){btnBalloon.hide();}
			);
		}
		
		$('.showBalloon').each(function(){
			var element=$(this);
			var text=element.attr('title');
			element.attr('title',''); //default 값이 아니라 내가 디자인한 것으로만 보이기
			element.hover(function(event){
				btnBalloon.text(text);
				updateBalloonPosition(event.pageX,event.pageY);
				showBalloon();
			}, function(){
				hideBalloon();
			});
			
			element.mousemove(function(event){
				updateBalloonPosition(event.pageX,event.pageY); //마우스움직임 감지해서 따라다님
			});
			
		});
	});
</script>

<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<%-- 사용자 프로필 --%>
<div class="modal fade modalFocus" id="popProfile" data-backdrop="static" data-keyboard="false"></div>

<%-- 첨부파일 조회/수정 --%>
<div class="modal fade modalFocus" id="popFileManageForm"   data-backdrop="static" data-keyboard="false"></div>

<%-- 정비실적조회 --%>
<div class="modal fade modalFocus" id="popRepairResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 정비실적등록 --%>
<div class="modal fade modalFocus" id="popRepairResultRegForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 예방보전실적조회 --%>
<div class="modal fade modalFocus" id="popPreventiveResultForm" data-backdrop="static" data-keyboard="false"></div>

<%-- 정비요청/결재 --%>
<%@ include file="/WEB-INF/jsp/epms/main/popMainRepairApprList.jsp"%>

<%-- 정비실적 --%>
<%@ include file="/WEB-INF/jsp/epms/main/popMainRepairResultList.jsp"%>
                                         
<%-- 예방보전 팝업  --%> 
<%@ include file="/WEB-INF/jsp/epms/main/popMainPreventivePlanList.jsp"%>

<%-- 비밀번호변경 팝업  --%> 
<%@ include file="/WEB-INF/jsp/epms/main/popPwdChange.jsp"%>

<%-- 고장원인분석 불러오기 --%>
<div class="modal fade modalFocus" id="popRepairAnalysisList"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog root wd-per-55">		
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">원인분석 목록</h4>
			</div>
		 	<div class="modal-body" >
	           	<div class="modal-bodyIn">
	           		<div class="modal-section">
						<div class="fl-box panel-wrap-modal wd-per-100">
							<div class="panel-body" id="PopRepairAnalysisList">
								<bdo Debug="Error"
									 Data_Url=""
									 Layout_Url="/epms/repair/analysis/popRepairAnalysisListLayout.do"
								 >
								</bdo>
							</div>
						</div>
					</div> <%-- modal-section : e --%>
				</div> <%-- modal-bodyIn : e --%>
			</div> <%-- modal-body : e --%>
		</div> <%-- modal-content : e --%>
	</div> <%-- modal : e --%>
</div>

</body>
</html>