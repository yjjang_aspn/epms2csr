<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Class Name : headerMenuBar.jsp
  Description : 헤더 메뉴 화면
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.07.06    이영탁              최초 생성

    author   : 이영탁
    since    : 2017.07.06
--%>
<script type="text/javascript">
	$(document).ready(function() {	
		//세션이 끊어졋는지 주기적으로 검사하는 로직
		setInterval("sessionCheck()", 3000); // 3000ms(3초)가 경과하면 sessionCheck() 함수를 실행합니다.	
	});
	
	function sessionCheck(){	
		$('#sessionCheck').attr('src', "/com/sessionCheck.jsp");
	}

	/* 헤더 메뉴 이동 */
	function linkHeaderMenu(menu_url, menu_cd){
		if(menu_url == ''){
			return;
		}
		$("#mainIfrm").attr("src",  menu_url);
		$("#leftIfrm").attr("src",  "/sys/menu/leftMenuBar.do?workGrp="+menu_cd);
	}
</script>
<ul id="gnb">
	<c:forEach var="item" items="${headerMenuList}">
		<li id="gnb_${item.WORK_CD}" onclick="linkHeaderMenu('${item.MENU_URL}', '${item.WORK_CD}');">
			<a href="#none">${item.MENU_NM}</a>
		</li>
	</c:forEach>
</ul>
<iframe id="sessionCheck" name="sessionCheck">
</iframe>