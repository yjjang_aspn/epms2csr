<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Class Name : mainMenu.jsp
  Description : 메인 메뉴 화면
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
   2017.07.06    이영탁              최초 생성
   2018.05.18    김영환              정비원 현황 추가

    author   : 이영탁
    since    : 2017.07.06
--%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/com/comHeader.jsp"%>
<script type="text/javascript" src="/js/com/web/md5.js"></script> 
<script type="text/javascript" src="/js/com/web/sha256.js"></script> 

<script type="text/javascript">
	$(document).ready(function(){
		//헤더메뉴 select 표시
		var headMenuCnt =  '${sessionScope.ssHeadMenuCnt}' * 1;
		if(headMenuCnt > 1){
			hearMenuSelect('${MENU_HEADER_GROUP}');
		}else{
			var menu = '${MENU_1}';
			menu = menu.substring(0,4) + "00";
			hearMenuSelect(menu);
		}
		jQuery("#jquery-accordion-menu").jqueryAccordionMenu();
		
		var auth = '${sessionScope.ssAuth}';
		if(headMenuCnt == 1){
// 			$("#mainIfrm").attr("src", "/eacc/main/mainDashBoard.do");
			$("#mainIfrm").attr("src", "/epms/equipment/master/equipmentMasterMng.do");
		}else{
			$("#mainIfrm").attr("src", "/dashBoard.do");
		}
	});
</script>


<script type="text/javascript">
$(document).ready(function(){
	
	hearMenuSelect('HELLO');
	
	// 출/퇴근 버튼
	$('#myonoffswitch').on("click", function(){
		var check_inout = $("#check_inout").val()//출퇴근 상태값
		
		// 출퇴근 상태 (1:출근, 2: 퇴근)
		if(check_inout == "02"){
			$('#check_inout').val("01");
			checkInOutAjax($('#check_inout').val());
		}else{
			$('#check_inout').val("02");
	    	checkInOutAjax($('#check_inout').val());
		}
	});

	// 정비원 현황 클릭 이벤트
	$("#workerStatBtn").on('click', function(){
		fnOpenLayerWithParam('/mem/member/popWorkerStatList.do', 'popWorkerStatList');
	})
	
	<%-- 모달 팝업 focus --%>
	$('.modalFocus').on('shown.bs.modal', function (e) {
		Grids.Focused = null;		<%-- 그리드 focus 해제--%>
		Grids.Active = null;		<%-- 그리드 action 해제(스크롤 해제) --%>
	});
	
	<%-- 모달 hide시 데이터 삭제 --%>
	$('.modalFocus').on('hidden.bs.modal', function (e) {
		$(this).removeData('bs.modal').children().remove()
	});
});

<%-- param 전달용 모달 호출 --%>
function fnOpenLayerWithParam(url, name){
	$('#'+name).load(url, function(responseTxt, statusTxt, xhr){
		$(this).modal();
	});
}

//출퇴근 상태값 변경
function checkInOutAjax(check_inout){
	jQuery.ajax({
		type : 'POST',
		url : '/mem/member/checkInOutAjax.do',
		cache : false,
		dataType : 'json',
		data : {CHECK_INOUT : check_inout},
		async : false,
		error : function() {
			alert("처리중 오류가 발생 하였습니다.");
		},
		success : function(json) {
			if(json.RES_CD != "0000"){ //T: 정상처리
				var check_switch = $("#myonoffswitch").is(":checked")
				if(check_switch == true){
					$("#myonoffswitch").prop("checked", true);	
				}else{
					$("#myonoffswitch").prop("checked", false);		
				}
				alert(json.RES_MSG);
			}
		}
	}); 
}

$(window).on('load', function(){
	var rtnUrl = "http://hello.aspn.co.kr:7070/ws/hellochat/";
	var rtnParam = "?id="+ '${sessionScope.ssUserId}' +"&mainBoard_yn=Y";
	$('.viewFrame').height("500");
});

</script>
</head>
<body>

<form name="pobizFrm" id="pobizFrm" method="post">
    <input type="hidden" name="user_id" value="${sessionScope.encId}" />
    <input type="hidden" name="uri" id="uri" value="/index.do" />
</form>
	
<form name="chatform">
	<input type="hidden" id="chatUserId" name="chatUserId" value="" />
	<input type="hidden" id="chatUserName" name="chatUserName" value="" />
	<input type="hidden" name="mailId" value="${param.mailId}" />
	<input type="hidden" name="mailPwd" value="${param.mailPwd}" />
</form>
	
<div class="wrapper" id="wrapper">
	<form id="menuFrm" name="menuFrm" method="post">
		<input type="hidden" id="user_id" value="${sessionScope.userId}"/>
		<input type="hidden" id="MENU_GROUP" name="MENU_GROUP" value="" />
		<input type="hidden" id="MENU_URL" name="MENU_URL" value="" />
		<input type="hidden" id="menuNo" name="forMenu" value="" />
		<input type="hidden" id="mainBoard_yn" name="mainBoard_yn" value=""/>
		<input type="hidden" id="mainDetail_yn" name="mainDetail_yn" />
		<input type="hidden" id="APPR_MASTER" name="APPR_MASTER" value=""/>
		<input type="hidden" id="APPR_TYPE" name="APPR_TYPE" value=""/>
		<input type="hidden" id="APPR_TARGET" name="APPR_TARGET" value=""/>
		<input type="hidden" id="contents" name="CONTENTS" />
		<input type="hidden" id="module" name="MODULE" />
		<input type="hidden" id="MENU" />
		
		<header id="header">
			<h1 class="ci"><img src="/images/com/web/main_logo.png" alt="ASPN" /></h1>
			<div class="gnb-list">
				<c:choose>
					<c:when test="${sessionScope.ssHeadMenuCnt > 1 }">
						<span class="left-toggle"><span class="skip">레프트메뉴닫기</span></span>
						<c:import url="/sys/menu/headerMenuBar.do" />				
					</c:when>
					<c:otherwise>
						<c:import url="/sys/menu/headerMenuBar2.do" />
					</c:otherwise>
				</c:choose>
			</div>
			<!--e:gnb-list-->
			<div class="gnb-side">
				<ul>
					<li class="user-info">
						<a href="#none">${sessionScope.ssDivisionNm} ${sessionScope.ssUserNm} ${sessionScope.ssJobGradeNm}</a>
					</li>
					
					<li class="work-info" id="work-info">
						<%-- <c:if test="${fn:indexOf(sessionScope.ssAuth,'AUTH_02') > -1 or fn:indexOf(sessionScope.ssAuth,'AUTH_03') > -1}"> --%>
							<div class="workSwitchBtn">
								<c:if test="${sessionScope.ssCheckInout ne '01'}"> 
									<input type="checkbox" name="workSwitchBtn" class="workSwitchBtn-checkbox" id="myonoffswitch"/>
									<label class="workSwitchBtn-label" for="myonoffswitch">
										<span class="workSwitchBtn-inner"></span>
										<span class="workSwitchBtn-switch"></span>
										<input type="hidden" id="check_inout" name="check_inout" value="${sessionScope.ssCheckInout}"/>
									</label>
								</c:if>
								<c:if test="${sessionScope.ssCheckInout eq '01'}">
									<input type="checkbox" name="workSwitchBtn" class="workSwitchBtn-checkbox" id="myonoffswitch" checked/>
									<label class="workSwitchBtn-label" for="myonoffswitch">
										<span class="workSwitchBtn-inner"></span>
										<span class="workSwitchBtn-switch"></span>
										<input type="hidden" id="check_inout" name="check_inout" value="${sessionScope.ssCheckInout}"/>
									</label>
								</c:if>
							</div>
						<%-- </c:if> --%>
						<a href="#none" id="workerStatBtn">정비원현황</a>
					</li>
					
					<li class="logout"><a href="#none" id="logOutBtn">로그아웃
					</a></li>
				</ul>
			</div>
		</header>
		<!--e:header-->
		
		<div id="main">
			<c:if test="${sessionScope.ssHeadMenuCnt > '1' }">
				<aside id="aside">
					<iframe id="leftIfrm" name="leftIfrm"
							marginwidth="0px" marginheight="0px"
							frameborder="no"  scrolling="no" align="center" style="width:100%"
							src="/sys/menu/leftMenuBar.do?workGrp=${HEADER_MENU_HELLOCOMPANY }"
					></iframe>
					<%-- <c:import url="/menu/leftMenuBar.do" >
						<c:param name="workGrp" value="${HEADER_MENU_HELLOCOMPANY }"/>
					</c:import> --%>
				</aside>
			</c:if>
			<!--e:aside-->
			<c:choose>
				<c:when test="${sessionScope.ssHeadMenuCnt > '1' }">
					<section id="section" class="mgn-l-200">
				</c:when>
				<c:otherwise>
					<section id="section" >
				</c:otherwise>
			</c:choose>
				<div id="container" class="">
					<div class="cont-inner">
						<!-- 페이지 내용 : s -->
							
						<iframe id="mainIfrm" name="mainIfrm"
							marginwidth="0px" marginheight="0px"
							frameborder="no"  scrolling="no" align="center" style="width:100%;"
							src=""
						></iframe>
						
						<!-- 페이지 내용 : e -->
						
						
					</div>
					<!--e:container/inner-->		
				</div>
				<!--e:container-->
			</section>
			<!--e:section-->
		</div>
		<!--e:main-->
	
	</form>
</div>
<!-- e:wrapper -->

<%------------------------------------- 레이어팝업 시작 -------------------------------------%>
<%-- 출퇴근현황 --%>
<div class="modal fade modalFocus" id="popWorkerStatList" data-backdrop="static" data-keyboard="false"></div>

<%-- 사용자 프로필 --%>
<div class="modal fade modalFocus" id="popProfile" data-backdrop="static" data-keyboard="false"></div>
</body>
</html>