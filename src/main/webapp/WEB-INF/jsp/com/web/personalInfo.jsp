<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="/js/com/web/common.js" type="text/javascript"></script>
<script type="text/javascript">

</script>

<!-- Layer PopUp Start -->
<div class="modal fade" id="personalInfo">
	<div class="modal-dialog root" style="width:50%; min-width:680px;"> <!-- 원하는 width 값 입력 -->						
		 <div class="modal-content" style="height:80%; min-height:400px;">
			<div class="modal-header">
			    <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">×</button>
			   	<h4 class="modal-title" id="modal_title">개인정보 취급방침</h4>
			</div>
		 	<div class="modal-body">
	           	<div class="modal-bodyIn">
	           	
	           		<div class="privacy_txt" style="height:100%;">
				
						<p class="privacy_Tit">
							㈜ 에이에스피엔(이하”회사”)는 사용자의 개인정보를 중요시하며 "정보통신망 이용촉진 및 정보보호 등에 관한 법률", “개인정보보호법” 등 
							개인정보와 관련한 법령상의 개인정보보호규정 과 행정자치부 및 방송통신위원회 등 유관기관이 제정한 가이드라인을 준수하고 있습니다.
							회사는 사용자의 개인정보를 수집, 이용, 제공하는 경우 반드시 사용자에게 사전에 해당 내용을 알려 동의절차를 거치며, 사용자가 동의하지
							않을 경우 사용자의 개인정보를 수집 이용 제공하지 않을 것임을 알려드립니다.
							단, 동의를 거부하는 경우 통합품질관리시스템의 전부 또는 일부 이용이 제한될 수 있습니다.
							회사는 개인정보취급방침 통해 사용자가 제공하는 개인정보의 처리 목적 및 이용범위를 명확히 하고	처리절차의 투명성과 적법성을 확보하려고 합니다.
						</p>
						
						<br>
						
						<p>회사 서비스의 개인정보취급방침은 다음과 같은 내용을 담고 있습니다. </p>
						
						<br>
						
						<div class="privacy_box">
							<span><a href="#first">1. 수집하는 개인정보의 항목</a></span>
							<span><a href="#second">2. 개인정보 수집 방법</a></span><br>
							<span><a href="#third">3. 개인정보 수집 목적</a></span>
							<span><a href="#fourth">4. 개인정보 보유 및 이용기간</a></span><br>
							<span><a href="#fifth">5. 개인정보 파기절차 및 방법</a></span>
							<span><a href="#sixth">6. 개인정보의 제3자 제공</a></span><br>
							<span><a href="#seventh">7. 개인정보의 취급위탁</a></span>
							<span><a href="#eighth">8. 사용자의 권리와 그 행사 방법</a></span><br>
							<span><a href="#ninth">9. 개인정보관리책임자 및 담당자의 연락처</a></span>
							<span><a href="#tenth">10. 개인정보의 기술적/관리적 보호 대책</a></span><br>
							<span><a href="#eleventh">11. 고지의 의무</a></span>
							<span><a href="#twelfth">12. 기타</a></span><br>
							<span><a href="#thirteenth">13. 시행일자</a></span>
						</div>
						
						<br>
						
						<span class="font_blue" id="first">1. 수집하는 개인정보의 항목</span><br>
						회사는 설비관리를 위한 정보 조회를 위해 적법한 수단을 통하여 아래와 같은 사용자의 정보를 수집하고 있습니다.<br>
						수집 항목 : 사용자 이름, 연락처, 회사 이메일 계정<br><br>
		
						<span class="font_blue" id="second">2. 개인정보 수집 방법</span><br>
						회사는 다음과 같은 방법으로 개인정보를 수집합니다.<br> 
						- 설비관리시스템 사용을 희망하는 자는 개인정보 담당자에게 서면으로 요청할 경우 그 희망자의 이름, 연락처, 회사 이메일 계정을 수집하여
						시스템에 등록 후 별도의 접근 아이디를 부여 합니다.<br><br>
		
						<span class="font_blue" id="third">3. 개인정보 수집 목적</span><br>
						회사는 수집한 개인정보를 다음의 목적을 위해 활용합니다. 사용자가 제공한 모든 정보는 아래 목적에 필요한 용도 이외로는 사용되지 않으며, 변경될 경우 사전 동의를 구하도록 할 것입니다.<br> 
						- 시스템 로그인시에 접근허가 사용자 여부 확인<br>
						- 설비관리시스템의 이용범위(생성, 조회, 변경, 저장, 폐기)의 초기설정 및 변경<br><br>
		
		
						<span class="font_blue" id="fourth">4. 개인정보 보유 및 이용기간</span><br>
						회사는 사용자의 개인정보를 원칙적으로 개인정보의 수집 및 이용목적이 달성하게 되는 경우 즉시 삭제합니다.<br>
						단, 다음과 같은 경우에는 예외적으로 명시한 기간 동안 보관합니다.<br>
						
						회사 내부 방침에 의한 정보 보유 사유<br> 
						- 보관 사유 : 부정 이용 방지 및 불법적 사용자에 대한 수사기관 수사협조<br>
						
						근거 : 정보통신망 이용촉진 및 정보 보호 등에 관한 법률에 따른 개인정보 보관 기간 : 6개월<br>
						- 본인에 관한 기록 : 6개월<br>
						
						근거 : 정보통신망 이용 촉진 및 정보보호 등에 관한 법률 <br>
						- 시스템 방문기록 : 3개월<br>
						
						근거 : 통신비밀보호법 <br><br>
		
						<span class="font_blue" id="fifth">5. 개인정보 파기절차 및 방법</span><br>
						회사는 원칙적으로 사용자의 개인정보 수집 및 이용목적이 달성되면 해당 정보를 지체없이 파기합니다. <br>
						회사의 개인정보 파기절차 및 방법은 다음과 같습니다.<br><br>
		
						<span>◇ 파기절차</span><br>
						사용자가 서비스 이용 등을 위해 입력한 정보는 목적이 달성된 후 별도의 DB로 옮겨져(종이의 경우 별도의 서류함) 내부 방침 및 기타 관련 법령에 의한 정보보호 사유에 따라(보유 및 이용기간 참조) 일정 기간 저장된 후 파기 되어집니다.<br>
						별도 DB로 옮겨진 개인정보는 법률에 의한 경우가 아니고서는 다른 목적으로 이용되지 않습니다. <br><br>
		
						<span>◇ 파기방법</span><br>
						- 종이에 출력된 개인정보 : 분쇄기로 분쇄하거나 소각<br>
						- 전자적 파일 형태로 저장된 개인정보 : 기록을 재생할 수 없는 기술적 방법을 사용하여 삭제<br><br>
		
						<span class="font_blue" id="sixth">6. 개인정보의 제3자 제공</span><br>
						회사는 회원으로부터 수집한 개인정보를 '3. 개인정보 수집 목적' 에 기재된 범위 내에서만 사용하며, 회원의 동의 없이는 범위를 초과하여 이용하거나 제 3자에게 제공하지 않습니다. <br><br>
		
						<span class="font_blue" id="seventh">7. 개인정보의 취급위탁</span><br>
						회사는 수집된 개인정보의 취급 및 관리 등의 업무를 스스로 수행함을 원칙으로 하나, 원활한 업무 처리를 위해 필요한 경우 업무의 일부 또는 전부를 회사가 선정한 업체에 위탁할 수 있습니다. 위탁업체가 추가 또는 변경되는 경우에 회사는 이를 회원님에게 고지합니다.<br><br>
		
						현재 회사의 개인정보처리 수탁자와 그 업무의 내용은 다음과 같습니다. <br><br>
		
						<table border="1" width="100%" class="privacy_ta">
							<colgroup>
								<col width="22%"/>
								<col width="26%"/>
								<col width="26%"/>
								<col width="26%"/>
							</colgroup>
							<tbody>
								<tr>
									<th>수탁자</th>
									<th>이용목적</th>
									<th>제공 정보 항목</th>
									<th>보유 및 이용기간</th>
								</tr>
								<tr>
									<td>㈜에이에스피엔</td>
									<td rowspan="2" style="padding:8px">
										각종 시스템 유지 및 보수 목적<br><br>
										
										개인정보를 보관,저장,이용<br>
										사용자의 시스템<br>
										문의사항 처리,<br>
										신규 기능(사용메뉴)<br><br>
										
										이용권한 설정 또는<br>
										기존 기능(사용메뉴)<br>
										이용권한 변경, 해제
									</td>
									<td rowspan="2">ID, 이름, 회사 이메일 계정</td>
									<td rowspan="2">
										사용자의 퇴사 또는<br>
										위탁관리 계약 종료시까지
									</td>
								</tr>
								<tr>
									<td>(주)에이에스피엔</td>
								</tr>
							</tbody>
						</table>
		 				<br><br>
		
						<span class="font_blue" id="eighth">8. 사용자의 권리와 그 행사 방법</span><br>
						사용자는 자신의 개인정보를 조회하거나 수정할 수 있으며, 회사의 개인정보 처리에 동의하지 않는 경우 동의를 거부하거나 탈퇴를 요청하실 수 있습니다. 동의를 거부하는 경우 설비관리시스템의 전부 또는 일부 이용이 제한될 수 있습니다.<br><br>
						
						사용자의 필요시 개인정보 조회, 수정을 할 수가 있습니다. 개인정보변경'(또는 '사용자 정보수정' 등)을, 동의철회를 위해서는 개인정보 담당자에 본인 확인 절차를 거치신 후 열람, 정정 또는 탈퇴가 가능합니다.<br> 
						개인정보관리책임자에게 서면, 전화 또는 이메일로 연락하시면 지체 없이 조치하겠습니다. <br>
						탈퇴는 개인정보 담당자에게 서면으로 탈퇴 사유 등을 기재하여 요청합니다.<br><br>
						
						회사는 사용자 요청에 의해 동의철회 또는 탈퇴에 의해 삭제된 개인정보는 "개인정보의 보유 및 이용기간"에 명시된 바에 따라 처리하고 그 외의 용도로 열람 또는 이용할 수 없도록 처리하고 있습니다. <br><br>
						
						<span class="font_blue"  id="ninth">9. 개인정보관리책임자 및 담당자의 연락처</span><br>
						회사는 사용자의 개인정보를 보호하고 개인정보와 관련한 불만을 처리하기 위하여 아래와 같이 관련 부서 및 개인정보관리책임자를 지정하고 있습니다.<br><br>
						
						개인정보관리책임자 성명: OOO 전무<br>
						전화번호: 000-0000-0000<br><br>
						
						개인정보관리담당부서: 생산본부 공무팀<br>
						개인정보담당자(정): OOO 부장<br>
						개인정보담당자(부): OOO 과장<br>
						전화번호: 000-0000-0000<br><br>
						
						사용자는 회사의 서비스를 이용하시며 발생하는 모든 개인정보보호 관련 민원을 개인정보관리 책임자 혹은 담당부서로<br>
						신고하실 수 있습니다. 회사는 이용자들의 신고사항에 대해 신속하게 충분한 답변을 드릴 것입니다.<br>
						기타 개인정보침해에 대한 신고나 상담이 필요하신 경우에는 아래 기관에 문의하시기 바랍니다.<br>
						
						1) 개인정보침해신고센터 (privacy.kisa.or.kr / 국번없이 118)<br>
						2) 대검찰청 사이버범죄수사단 (www.spo.go.kr / 02-3480-3571)<br>
						3) 경찰청 사이버안전국 (www.ctrc.go.kr / 국번없이 182)<br><br>
						
						<span class="font_blue" id="tenth">10. 개인정보의 기술적/관리적 보호 대책</span><br>
						회사는 사용자의 개인정보를 취급함에 있어 개인정보가 분실, 도난, 누출, 변조 또는 훼손되지 않도록 안전성 확보를 위하여 다음과 같은 기술적/관리적 대책을 강구하고 있습니다.<br><br>
						
						사용자의 주요 정보를 비롯한 아이디(ID), 비밀번호는 암호화하여 저장 및 관리하고 있습니다.<br><br>
						
						회사는 최신 백신프로그램을 이용하여 사용자들의 개인정보나 자료가 누출되거나 손상되지 않도록 방지하고 있으며, 암호화통신(SSL) 등을 통하여 네트워크상에서 개인정보를 안전하게 전송할 수 있도록 하고 있습니다.<br><br>
						
						회사는 침입차단시스템을 이용하여 외부로부터의 무단 접근을 통제하고 있으며, 기타 시스템적으로 보안성을 확보하기 위한 가능한 모든 기술적 장치를 갖추려 노력하고 있습니다.<br><br>
						
						회사는 웹 사이트 개인정보취급방침의 이행사항 및 담당자의 준수 여부를 확인하여 문제가 발견될 경우 즉시 수정하고 바로 잡을 수 있도록 노력하고 있습니다.<br><br>
						
						회사는 사용자의 개인정보를 안전하게 취하기 위하여, 접근 권한을 최소한의 인원으로 제한하며, 취급 인원에게 보안교육을 시행하고 보안서약서를 받는 등의 관리적 대책을 마련하고 있습니다.<br><br>
						
						사용자가 사용하는 ID와 비밀번호는 원칙적으로 사용자만이 사용하도록 되어 있습니다. 어떠한 경우에도 비밀번호는 타인에게 알려 주지 마시고 로그인 상태에서는 주위의 다른 사람에게 개인 정보가 유출되지 않도록 특별한 주의를 기울여 주시기 바랍니다. <br><br>
						회사는 사용자 본인의 부주의나 인터넷상의 문제로 ID, 비밀번호 등 개인정보가 유출되어 발생한 문제에 대해 회사는 일체의 책임을 지지 않습니다.<br><br>
						
						
						<span class="font_blue" id="eleventh">11. 고지의 의무</span><br>
						현 개인정보취급방침은 2018년 12월 1일부터 적용됩니다. 개인정보 취급방침의 내용은 수시로 변경될 수 있으며, 변경 내용의 확인이 되도록 팀 사전 메일 공유 및 팀 회의시 공지 하도록 하겠습니다.<br><br>
						현 개인정보취급방침 내용의 변경이 있을 경우 변경사항을 최소 7일전부터 홈페이지 ‘공지사항’을 통하여 공지합니다.<br>
						단, 사용자의 권리 또는 의무에 중요한 내용의 변경은 시행일로부터 30일 이전에 고지합니다.<br><br>
						 
						
						<span class="font_blue" id="twelfth">12. 기타</span><br>
						본 사이트에 링크되어 있는 웹사이트들이 개인정보를 수집하는 행위에 대해서는 "본 사이트 개인정보취급방침"이 적용되지 않음을 알려 드립니다.<br><br>
						
						<span class="font_blue" id="Thirteenth">13. 시행일자</span><br>
						공고일 : 2018년 12월 1일<br>
						시행일 : 2018년 12월 1일<br>
					
					</div>
	           	
	           	
				</div>
			</div>	
		</div>		
	</div>
	<!-- 좌측 : e -->
</div>
<!-- Layer PopUp End -->
